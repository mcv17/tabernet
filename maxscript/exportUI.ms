global mcv_prefabs_path = mcv_scripts_path + "/bin/data/prefabs/"

if PrefabExporter != undefined then
	try(destroyDialog PrefabExporter)catch()

utility MyMCVExporter "MCV Exporter"
(
	rollout PrefabExporter "Prefab Exporter" (
		
		label titlePrefab "Prefab directories"
		listbox prefab_directories "Prefab directories:" items:#("None")
		
		label spacing0 ""
		button btn_prefab_export "Export to directory"
		label spacing1 ""
		button btn_reload "Reload directories (Do if you added new ones)"
		label spacing2 ""
		
		
		fn reloadDirectories = (
			local directoriesForPrefabs = #("")
			-- Finally, get all directories in the current path and keep on moving downwards.
			local directories = GetDirectories (mcv_prefabs_path + "/*")

			-- Get relative directories.
			for directory in directories do(
				local basePath = "\\data\\prefabs\\"
				local start_base_dir = findString directory "\\data\\prefabs\\"
				local startAssetDirectory = start_base_dir + basePath.count
				local directory_relative = substring directory startAssetDirectory directory.count
				
				local parsedDirectory = ""
				local wordsToAdd = filterString directory_relative "\\"
				for i = 1 to wordsToAdd.count - 1  do
					parsedDirectory += wordsToAdd[i] + "/"
				parsedDirectory += wordsToAdd[wordsToAdd.count] + "/"
					
				-- Add the directory name.
				append directoriesForPrefabs parsedDirectory
				
				-- Keep moving forward in the directories.
				local newDirectories = GetDirectories (directory + "/*")
				join directories newDirectories
			)
			prefab_directories.items = directoriesForPrefabs
		)
		
		on btn_prefab_export pressed do
		(
			try (
				gc()
				
				local fileName= getFilenameFile maxFileName
				local fullPath = mcv_3DsMaxAssets_path + prefab_directories.selected + fileName
				
				local category_dir = mcv_3DsMaxAssets_path + prefab_directories.selected
				local obj_dir = mcv_3DsMaxAssets_path + prefab_directories.selected + fileName
				local filePath = mcv_3DsMaxAssets_path + prefab_directories.selected + fileName + "/" + fileName -- the full path up to .max
				
				-- Create the dir for the category if it doesn't exist.
				if(not doesFileExist category_dir) then
					makeDir category_dir
				
				-- Create the dir for the obj if it doesn't exist.
				if(not doesFileExist obj_dir) then
					makeDir obj_dir

				local saveFile = true
				if (doesFileExist obj_dir) then
					saveFile = queryBox "Attention, the file already exists. Do you want to overwrite it?"
				
				if not saveFile then
					MessageBox "You decided not to export the file."
				else(
					local exporter = TSceneExporter()
					if (exporter.exportAsPrefab prefab_directories.selected) then(
						saveMaxFile filePath
						
						local metadataFile = obj_dir + "/" + "prefab_object.max_metadata"
						createFile metadataFile
						
						MessageBox "All exported OK"
					)
				)
			) catch (
				MessageBox ("Error Exporting:\n" + getCurrentException())
			)
		)
		
		on btn_reload pressed do(
			reloadDirectories()
		)
		
		on PrefabExporter open do
		(
			reloadDirectories()
		)
	)
	
	rollout exporter "The Exporter"
	(
		button btn_prefab "Export As Prefab" tooltip:"Use this when creating a new prefab."
		button btn_scene "Export As Scene"
		button btn_cameras "Export Scene Cameras"
		button btn_mesh "Export Mesh"
		button btn_skeleton "Export Skeleton & Meshes"
		button btn_skel_anims "Export Animation" tooltip:"Don't use this, use the export as CAF format." 
		button btn_navmesh "Export Navmesh"
		
		on btn_scene pressed do
		(
			try (
				gc()
				local exporter = TSceneExporter()
				if exporter.exportAsScene() then
					MessageBox "All exported OK"
			) catch (
				MessageBox ("Error Exporting:\n" + getCurrentException())
			)
		)
		
		on btn_prefab pressed do
		(
			try(destroyDialog PrefabExporter)catch()
			createDialog PrefabExporter width:300
		)
		
		on btn_cameras pressed do
		(
			try (
				gc()
				local exporter = TSceneExporter()
				if exporter.exportCameras() then
					MessageBox "All exported OK"
			) catch (
				MessageBox ("Error Exporting:\n" + getCurrentException())
			)
		)
		
		-- Exports the current selection mesh to file
		on btn_mesh pressed do
		(
			
			try (
				gc()
				local ofilename = undefined  -- to be defined by you.
				exportMesh $ ofilename undefined
				MessageBox "Single mesh exported OK"
			) catch (
				MessageBox ("Error Exporting Single Mesh:\n" + getCurrentException())
			)
		)

		-- Exports the current selection mesh to file
		on btn_skeleton pressed do
		(
			
			try (
				gc()
				local se = TSkeletonsExporter()
				se.exportSkelAndMeshes()
				MessageBox "Skeleton And Meshes exported OK"
			) catch (
				MessageBox ("Error Exporting Skeleton:\n" + getCurrentException())
			)
		)
		
		-- Exports the current selection mesh to file
		on btn_skel_anims pressed do
		(
			
			try (
				gc()
				local se = TSkeletonsExporter()
				se.exportAnim()
				MessageBox "Skeleton Animation exported OK"
			) catch (
				MessageBox ("Error Exporting Skeleton Animation:\n" + getCurrentException())
			)
		)
		
		on btn_navmesh pressed do
		(
			
			try (
				gc()
				local nv = TNavmeshExporter()
				nv.exportNavmesh()
				MessageBox "Navmesh exported OK"
			) catch (
				MessageBox ("Error Exporting Navmesh:\n" + getCurrentException())
			)
		)
		
	) -- end rollout creator
		
	-- ...
	on MyMCVExporter open do
	(
		addRollout exporter
	)

	on MyMCVExporter close do
	(
		try(destroyDialog PrefabExporter)catch()
		removeRollout exporter
	) 
) -- end utility MyUtil 