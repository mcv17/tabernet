--global mcv_scripts_path = "D:/MCV17/tabernet" -- Manel
--global mcv_scripts_path = "C:/Users/David/MCV/tabernet" 	--David
--global mcv_scripts_path =  "C:/_Dev/tabernet" 	--Alfred
--global mcv_scripts_path = "C:/Users/mcv17_alumne/Google Drive/tabernet-art" -- Startup uni.
global mcv_scripts_path ="C:/Users/Gabi/Documents/Visual Studio 2017/Projects/tabernet"

-- Manager and utils.
filein (mcv_scripts_path + "/maxscript/map.ms")
filein (mcv_scripts_path + "/maxscript/editorAssetManager.ms")
filein (mcv_scripts_path + "/maxscript/editorPrefabManager.ms")

-- Exporter of scene and prefabs.
filein (mcv_scripts_path + "/maxscript/jsonFormatter.ms")
filein (mcv_scripts_path + "/maxscript/mcvCustomAttributes.ms")
filein (mcv_scripts_path + "/maxscript/exportMesh.ms")
filein (mcv_scripts_path + "/maxscript/exportMaterial.ms")
filein (mcv_scripts_path + "/maxscript/exportMesh.ms")
filein (mcv_scripts_path + "/maxscript/exportSkeletons.ms")
filein (mcv_scripts_path + "/maxscript/exportScene.ms")
filein (mcv_scripts_path + "/maxscript/exportNavmesh.ms")
filein (mcv_scripts_path + "/maxscript/exportUI.ms")

-- Editor
filein (mcv_scripts_path + "/maxscript/editorScenePrefabRollout.ms")
filein (mcv_scripts_path + "/maxscript/editorSceneMain.ms") -- Repeated so the ones below know functions.
filein (mcv_scripts_path + "/maxscript/editorScenePrefabRollout.ms")
filein (mcv_scripts_path + "/maxscript/editorSceneSpawnRollout.ms")
filein (mcv_scripts_path + "/maxscript/editorSceneJSONExtraComponentsRollout.ms")
filein (mcv_scripts_path + "/maxscript/editorSceneColliderRollout.ms")
filein (mcv_scripts_path + "/maxscript/editorSceneCullingVolumeRollout.ms")
filein (mcv_scripts_path + "/maxscript/editorSceneMain.ms")
