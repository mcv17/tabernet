-- IMPORTANT - Si da el error: ExportMesh expected 0 args and got 3, teneis que ejecutar primero el 
-- script del ExportMesh para que max lo tenga en cuenta

clearListener()
gc()

----------------------------------------------------------
struct TSceneExporter (
	-- Variables.
	fs = TJsonFormatter(),
	project_path = mcv_scripts_path + "/bin/",
	base_path = "data/",
	current_scene_name,
	scenes_path = base_path + "scenes/",
	prefabs_path = base_path + "prefabs/",
	mesh_path = base_path + "meshes/",
	mats_path = base_path + "materials/",
	
	/* ----------------- TCompName ----------------- */

	-- Export TCompName component.
	fn exportCompName obj = (
		fs.writeKeyValue "name" obj.name
	),
	
	fn isValidName aname = (
		return findString aname " " == undefined 
	),

	/* ----------------- TCompTransform ----------------- */	

	-- Exports a TCompTransform component.
	fn exportTransform obj = (
		fs.writeComma()
		fs.writeKey "transform" 
		
		local max2mcv = rotateXMatrix -90
		local mcv2max = rotateXMatrix 90
		local mcv_position = obj.position * max2mcv
		
		-- From mcv, we will go to max, apply the max transform and go back to mcv coord system
		local mcv_transform = mcv2max * obj.transform * max2mcv

		-- Take just the rotation as quaterion
		local mcv_quat = mcv_transform.rotationPart as quat
		
		fs.beginObj()
			fs.writeKeyValue "pos" mcv_position
			fs.writeComma()
			fs.writeKeyValue "rotation" mcv_quat
		fs.endObj()
	),
	
	/* ----------------- TCompRender ----------------- */
	
	-- Exports a TCompRender component.
	fn exportEditableMesh obj prefabCategory = (
		fs.writeComma()
		fs.writeKey "render" 
		fs.beginObj()
		local mesh_directory = mesh_path + prefabCategory + current_scene_name + "/"
		local mesh_name = mesh_directory + obj.name + ".mesh"
		
		-- Mesh directory.
		if(not doesFileExist mesh_directory) then
			makeDir mesh_directory

		fs.writeKeyValue "mesh" mesh_name
		fs.writeComma()

		fs.writeKeyValue "color" obj.wireColor
		fs.writeComma()
		
		-- Whether the given prefab can be instanced or not, by default we set it to true.
		local can_be_instanced = getUserProp obj "instancing"
		if can_be_instanced == undefined then
			can_be_instanced =  true
		
		fs.writeKeyValue "can_be_instanced" can_be_instanced
		fs.writeComma()
		
		local is_skinned = getUserProp obj "skeleton" != undefined

		-- Export the real mesh
		local full_mesh_filename = project_path + mesh_name
		format "full_mesh_filename is %\n" full_mesh_filename
		exportMesh obj full_mesh_filename undefined
		
		-- Export material(s).
		local me = TMaterialExporter project_path:project_path base_path:base_path
		local exported_materials = me.exportMaterial obj current_scene_name prefabCategory mats_path obj.material is_skinned
		
		fs.writeKey "materials"
		fs.arrayOfStrings exported_materials
			
		fs.endObj()
	),
	
	
	/* ----------------- TCompArmature ----------------- */

	fn isSkeleton obj = (
		return obj != undefined and getUserProp obj "skeleton" != undefined
	),

	fn exportArmature obj = (
		fs.writeComma()
		fs.writeKey "armature"
		local exporter = TSkeletonsExporter()
		local skeletonPath = exporter.exportSkelAndMeshes()
		fs.beginObj()
		fs.writeKeyValue "src" skeletonPath
		fs.endObj()
	),
	
	/* ----------------- TCompTag ----------------- */
	
	fn hasTag obj = (
		return obj != undefined and getUserProp obj "tags" != undefined
	),
	
	fn exportTag obj = (
		fs.writeComma()
		local tags = getUserProp obj "tags"
		fs.writeKeyValueAsArray "tags" tags
	),
	
	/* ----------------- TCompCollider ----------------- */
	
	-- Checks if we have a collider.
	fn isCollider obj = (
		return obj != undefined and getUserProp obj "collider"  != undefined and obj.parent != null
	),
		
	-- Checks if we have added a trigger.
	fn isTrigger obj = (
		return obj != undefined and getUserProp obj "trigger" != undefined
	),
	
	-- Exports a shape for our component.
	fn exportCompShape obj prefabCategory = (
		
		fs.beginObj()
		
		if classof obj == Sphere or classof obj == GeoSphere then (
			fs.writeKeyValue "shape" "sphere"
			fs.writeComma()
			fs.writeKeyValue "radius" obj.radius
			
		) else if classof obj == Box then (
			fs.writeKeyValue "shape" "box"
			fs.writeComma()
			local half_size =  ( [abs obj.width, abs obj.height, abs obj.length] * 0.5)
			-- Warning, order might be incorrect!!!
			fs.writeKeyValue "half_size" half_size
			fs.writeComma()
			fs.writeKey "offset"
			fs.beginObj()
				fs.writeKeyValue "pos" [0, half_size.y, 0]
			fs.endObj()
			
		) else if classof obj == Capsule then (
			fs.writeKeyValue "shape" "capsule"
			fs.writeComma()
			local radius = obj.radius
			local capsuleHeight = obj.height / 2
			local isCharController = (getUserProp obj "collider") == "CharacterController"

			fs.writeKeyValue "radius" radius
			fs.writeComma()
			fs.writeKeyValue "height" capsuleHeight
			fs.writeComma()
			fs.writeKeyValue "controller" isCharController

		) else if classof obj == Editable_Mesh or classof obj == Editable_Poly or classof obj == Cylinder then (
			if classof obj == Editable_Poly or classof obj == Cylinder then(
				convertToMesh obj
			)
			
			fs.writeKeyValue "shape" "trimesh"
			fs.writeComma()
			
			-- Warning, order might be incorrect!!!
			local mesh_directory = mesh_path + prefabCategory + current_scene_name + "/"
			local mesh_name = mesh_directory + obj.name + ".col_mesh"	
			
			-- Mesh directory.
			if(not doesFileExist mesh_directory) then
				makeDir mesh_directory

			local full_mesh_filename = project_path + mesh_name
			exportMesh obj full_mesh_filename "Pos"
			
			fs.writeKeyValue "collision_mesh" mesh_name
		)

		local px_group = getUserProp obj "group"
		if isTrigger obj then (
			fs.writeComma()
			fs.writeKeyValue "group" "trigger"
		)
		else if px_group == undefined then (
			fs.writeComma()
			fs.writeKeyValue "group" "scenario"
		)
		else if px_group != undefined then (
			fs.writeComma()
			fs.writeKeyValue "group" px_group
		)
		
		local px_mask = getUserProp obj "mask"
		if px_mask != undefined then (
			fs.writeComma()
			fs.writeKeyValue "mask" px_mask
		)
		
		if isTrigger obj then (
			fs.writeComma()
			fs.writeKeyValue "trigger" true
		)

		fs.endObj()
	),
	
	-- Collider functions.
	fn exportCompCollider obj candidates prefabCategory = (
		fs.writeComma()
		fs.writeKey "collider" 
		fs.beginObj()
		
		fs.writeKey "shapes"
		fs.beginArray()
			local n = 0
			for child in candidates do (
				if n > 0 then fs.writeComma()
				exportCompShape child prefabCategory
				n = n + 1
			)
		fs.endArray()
		
		-- Add it in the user properties panel of max:     density = 10
		local density = getUserProp obj "density"
		if density != undefined then (
			fs.writeComma()
			fs.writeKeyValue "density" density
		)
		
		local colliderType = getUserProp obj "collider"
		local is_dynamic = (colliderType != undefined and colliderType != "Static");
		if is_dynamic == true then (
			fs.writeComma()
			fs.writeKeyValue "dynamic" true
		)
		
		local is_kinematic = getUserProp obj "kinematic"
		if is_kinematic != undefined then (
			fs.writeComma()
			fs.writeKeyValue "kinematic" true
		)
		
		fs.endObj()
		
		return is_dynamic
	),
	
	-- Collider children.
	fn exportChildrenColliders obj prefabCategory = (
		local candidates = #()
		for child in obj.children do (
			if isCollider child or isTrigger child then append candidates child
		)
		
		if isTrigger obj then candidates = #(obj)
		
		if candidates.count == 0 then return false
		
		format "Candidates are %\n" candidates
		--return true
		
		return exportCompCollider obj candidates prefabCategory
	),
	
	/* Prefabs */
	
	-- This function exports a prefab.
	fn isPrefab obj = (
		local userProp = getUserProp obj "prefab"
		if userProp == undefined then return false
		return true
	),
	
	-- This function exports a prefab.
	fn exportPrefab obj = (
		local valueOfProp = getUserProp obj "prefab"
		local prefabPath = "data/prefabs/" + valueOfProp
		fs.writeComma()
		fs.writeKeyValue "prefab" prefabPath
		
		return true
	),
	
	/* Culling volume */
	
	-- Checks if the entity is a culling volume.
	fn isCullingVolume obj = (
		return obj != undefined and getUserProp obj "scene_culling_volume" != undefined
	),
	
	fn exportCullingVolume obj = (
		fs.writeComma()
		fs.writeKey "scene_culling_volume" 
		fs.beginObj()
		local startingVolume = getUserProp obj "starting_volume"
		if startingVolume != undefined then(
		fs.writeKeyValue "starting_volume" startingVolume
		fs.writeComma()
		)
		
		local max2mcv = rotateXMatrix -90
		local mcv2max = rotateXMatrix 90
		local center = obj.position * max2mcv
		fs.writeKeyValue "center" center
		fs.writeComma()
		
		local half_size =  ( [abs obj.width, abs obj.height, abs obj.length] * 0.5)
		fs.writeKeyValue "half_size" half_size
		fs.writeComma()

		local renderingNeighbors = getUserProp obj "culling_volume_rendering_neighbors"
		fs.writeKeyValueAsArray "culling_volume_rendering_neighbors" renderingNeighbors
		fs.writeComma()
		local logicalNeighbors = getUserProp obj "culling_volume_logic_neighbors"
		fs.writeKeyValueAsArray "culling_volume_logic_neighbors" logicalNeighbors
		fs.endObj()
	),
	
	/* Spawn */
	
	-- Checks if the entity is a culling volume.
	fn isSpawn obj = (
		return obj != undefined and getUserProp obj "spawn" != undefined
	),
	
	fn exportSpawn obj = (
		setUserProp obj "trigger" "trigger"
		fs.writeComma()
		fs.writeKey "spawn" 
		fs.beginObj()
		local enemies = getUserProp obj "spawn"
		fs.writeKeyValueAsArray "enemies_to_spawn" enemies
		fs.endObj()
	),
	
	/* TCompAABB */

	-- This function creates an AABB component, by default, all objs have aabb.
	fn createAABBComp = (
		fs.writeComma()
		fs.writeKey "abs_aabb" 
		fs.beginObj()
		fs.endObj()
	),
	
	fn endEntity = (
		fs.endObj()
		fs.endObj()
	),
	
	/* Camera */
	fn isCamera obj = (
		return obj != undefined and (getUserProp obj "camera" != undefined)
	),
	
	fn isCameraAux obj = (
		return obj != undefined and (getUserProp obj "camera_auxiliar" != undefined)
	),
	
	fn exportCamera obj = (
		fs.writeComma()
		fs.writeKey "camera" 
		fs.beginObj()
		fs.endObj()
		fs.writeComma()is
		fs.writeKey "culling" 
		fs.beginObj()
		fs.endObj()
	),
	
	/* JSON Export */
	
	-- Checks if the entity is a culling volume.
	fn isExtraComponentsJson obj = (
		return obj != undefined and getUserProp obj "json" != undefined
	),
	
	fn exportJSONExtraData obj = (
		fs.writeComma()
		local json_data = getUserProp obj "json"
		fs.writeValue json_data
	),
	
	/* Exporter of entities */
	
	fn hasParent obj = (
		return obj.parent != undefined
	),
	
	fn ignoreEntity obj = (
		return obj != undefined and getUserProp obj "ignore" != undefined
	),
	
	-- Add here all variables that can make an object not be exported as a complete entity in the engine.
	fn isNotExportedAsEntity obj = (
		if isCollider obj then
			return true
		if ignoreEntity obj then
			return true
		return false
	),
	
	/* Forward declaration */
	fn exportEntitiesFromSelection =(),
	
	fn exportEntity obj baseName prefabCategory exportingAsPrefab = (

		-- First we check if there are any children inside this entity.
		local numOfChildrenToExport = obj.children.count
		if numOfChildrenToExport > 0 then(
			for child in obj.children do(
				-- If it is a collider, we won't create another entity, we ignore it.
				if isCollider child or isPrefab child or ignoreEntity child then
					numOfChildrenToExport = numOfChildrenToExport - 1
			)
		)
		
		if isCamera oj and not exportingAsPrefab then(
			return false
		)
		
		-- Start group.
		if numOfChildrenToExport > 0 then(
			fs.beginObj()
			fs.writeKey "group"
			fs.beginArray()
		)
		
		-- Write the parent entity.
		fs.beginObj()
		fs.writeKey "entity"
			fs.beginObj()
			-- Export name component for this entity.
			exportCompName obj
			-- Export a transform component for this entity.
			exportTransform obj
		
			-- Checks if the entity is a prefab, if it is, export it as so.
			-- Right now we don't support (and probably won't support :p) overwritting other components
			-- of a prefab from the 3ds max exporter. So we end the object here if we have a prefab.
			if isSpawn obj then
				exportSpawn obj
			else if isPrefab obj then(
				exportPrefab obj
				if isExtraComponentsJson obj then
					exportJSONExtraData obj
				
				endEntity()
				return true
			)
			
			if isCamera obj then
				exportCamera obj
		
			-- Exports colliders of the object as a TCompCollider. Also checks if the object has
			-- a trigger or not and adds it if necessary.
			exportChildrenColliders obj prefabCategory
			
			if isCullingVolume obj then
				exportCullingVolume obj

			-- Export a TCompRender if it has one.
			local HasMesh = false
			if not isTrigger obj and not isCamera obj then (
				if canConvertTo obj Editable_mesh then(
					exportEditableMesh obj prefabCategory
					HasMesh = true
				)
			)

			-- Exports a TCompTag, used by the mark trigger for now.
			if hasTag obj then
				exportTag obj
			
			-- Export a TCompArmature.
			if isSkeleton obj then
				exportArmature obj
			
			-- Creates an AABB for the entity.
			if HasMesh then
				createAABBComp()
			
			if isExtraComponentsJson obj then
				exportJSONExtraData obj
		endEntity()
		
			
		if numOfChildrenToExport > 0 then(
			fs.writeComma()
		)

		-- Do the same for the children. We only export children if we are not exporting as prefab.
		-- Otherwise, one should select children if they want them instanced.
		if not exportingAsPrefab then
			exportEntitiesFromSelection obj.children true baseName prefabCategory exportingAsPrefab
		
		-- Close the group if we had one open.
		if numOfChildrenToExport > 0 then(
			fs.endArray()
			fs.endObj()
		)
	),
	
	fn exportEntitiesFromSelection selectionObj exportChildren baseName prefabCategory exportingAsPrefab = (
		local nitems = 0
		for obj in selectionObj do (
			-- We don't export children directly, The parent entity exports them.
			if exportChildren == false and hasParent obj then continue
			
			-- Ignore elements that are not supposed to be entities.
			if isNotExportedAsEntity obj then continue
			if isCamera obj or isCameraAux obj then continue
			
			if nitems > 0 then  fs.writeComma()
			exportEntity obj baseName prefabCategory exportingAsPrefab
			nitems = nitems + 1
		)
	),
	
	fn exportCamerasFromSelection selectionObj baseName = (
		local nitems = 0
		for obj in selectionObj do (
			-- We don't export children directly, The parent entity exports them.
			if hasParent obj then continue
			if not isCamera obj and not isCameraAux obj then continue

			-- Ignore elements that are not supposed to be entities.
			if isNotExportedAsEntity obj then continue
			if nitems > 0 then  fs.writeComma()
			exportEntity obj baseName "" false
			nitems = nitems + 1
		)
		
	),
	
	-- Read all materials in the selection and check that none of them have repeated names.
	fn checkAllMaterialsHaveDifferentNames = (

		local matNames = #()
		for obj in getCurrentSelection() do(
			if obj.mat == undefined then
				continue
			
			local isUnique = true
			local matClass = classof obj.mat
			if isExportableMaterialClass matClass then (
				isUnique = appendIfUnique matNames obj.mat.name
				if not isUnique then(
					-- Return the name for the error prompt so we know which material it is that shares names.
					return obj.mat.name
				)
			)
			else if classof obj.mat == MultiMaterial then (
				local multi_mat = obj.mat
				local copyObj = obj
				local copied = false
				if classof copyObj != Editable_mesh then(
					copyObj = copy obj
					copied = true
					convertToMesh copyObj
				)
				
				local materials_of_mesh = getMaterialsUsedByMesh copyObj
				for mat_idx = 1 to materials_of_mesh.count do (
					if materials_of_mesh[ mat_idx ] == undefined then continue
					local mat_of_mesh = multi_mat[ mat_idx ]
					isUnique = appendIfUnique matNames mat_of_mesh.name
					if not isUnique then(
						-- Return the name for the error prompt so we know which material it is that shares names.
						return mat_of_mesh.name
					)
				)
				
				if copied then
					delete copyObj
			)
		)
			
		return undefined
	),
	
	fn exportCameras = (
		local currentSelection = $*
		if currentSelection.count == 0 then(
			messagebox "The scene is empty, add something before trying to export."
			return false
		)
		
		-- Decide output filename based on .max filename
		current_scene_name = getFilenameFile maxFileName
		if current_scene_name == undefined or current_scene_name == "" then(
			messagebox "The name of your max file is empty. You can't export an scene with an empty name. Rename the max file and try to export again."
			return false
		)
		
		local sceneName = current_scene_name
		current_scene_name = "Camera-" + current_scene_name
		
		local full_path = project_path + scenes_path + current_scene_name + ".json"
		format "Exporting to % %\n" full_path  current_scene_name
		fs.begin full_path
		fs.beginArray()
		
		-- Creates a fake empty entity that will act as group for all the scene. This way it showcases as a single entity
		-- inside the engine.
		fs.beginObj()
		fs.writeKey "entity"
		fs.beginObj()
		local sceneName = "Cameras Scene: " + sceneName
		fs.writeKeyValue "name" sceneName
		fs.endObj()
		fs.endObj()
		fs.writeComma()
		
		-- Export all entities inside the scene.
		exportCamerasFromSelection $* current_scene_name

		fs.endArray()
		fs.end()

		return true
	),
	
	fn exportAsPrefab prefabsubfolder = (
		current_scene_name = getFilenameFile maxFileName
		if current_scene_name == undefined or current_scene_name == "" then(
			MessageBox "The name of your max file is empty, save the file with a name before exporting it as a prefab."
			return false
		)
				
		local currentSelection = getCurrentSelection()
		if currentSelection.count == 0 then(
			messagebox "You have no selection to export. Select something before trying to export it as prefab."
			return false
		)
		
		-- Read all materials in the scene and get sure none of them have the same name.
		--local materialWithSameName = checkAllMaterialsHaveDifferentNames()
		--if materialWithSameName != undefined then(
		--	local message = "Attention, you have materials that are sharing the same name: " + materialWithSameName + ".\nChange the name for an specific one for each one please."
		--	messagebox message
		--	return false
		--)

		-- Decide output filename based on .max filename
		local full_path = project_path + prefabs_path + prefabsubfolder + current_scene_name + ".json"
		
		format "Exporting prefab to % %\n" full_path current_scene_name
		fs.begin full_path
		fs.beginArray()

		-- We don't export children directly, the parent entity exports them.
		exportEntitiesFromSelection currentSelection true current_scene_name prefabsubfolder true

		fs.endArray()
		fs.end()
		
		-- Finally, before leaving, set all the materials to relative path, this way the max file will be ready to be imported
		-- and saved in our maxscript folders so all artists can use them off the bat.
		for map in materialsPathToRelative.keys do(
			local mapAndPath = materialsPathToRelative.getItemValue map
			mapAndPath.map.filename = mapAndPath.relativePath
		)
		materialsPathToRelative = Map()
		
		return true
	),
	
	fn exportAsScene = (
		local currentSelection = $*
		if currentSelection.count == 0 then(
			messagebox "The scene is empty, add something before trying to export."
			return false
		)
		
		-- Decide output filename based on .max filename
		current_scene_name = getFilenameFile maxFileName
		if current_scene_name == undefined or current_scene_name == "" then(
			messagebox "The name of your max file is empty. You can't export an scene with an empty name. Rename the max file and try to export again."
			return false
		)
		
		local full_path = project_path + scenes_path + current_scene_name + ".json"
		format "Exporting to % %\n" full_path  current_scene_name
		fs.begin full_path
		fs.beginArray()
		
		-- Creates a fake empty entity that will act as group for all the scene. This way it showcases as a single entity
		-- inside the engine.
		fs.beginObj()
		fs.writeKey "entity"
		fs.beginObj()
		local sceneName = "Scene: " + current_scene_name
		fs.writeKeyValue "name" sceneName
		fs.endObj()
		fs.endObj()
		fs.writeComma()
		
		-- Export all entities inside the scene.
		exportEntitiesFromSelection $* false current_scene_name "" false

		fs.endArray()
		fs.end()

		local command = "cd " + mcv_scripts_path + "/tools/MaxScriptTools/" + " && " + "MaxScriptTools.exe " + " -b " + "\"" + mcv_scripts_path + "\"" +  " \"" + full_path + "\""
		
		format "Command: % \n" command
		local status = DOSCommand command
		
		return true
	)
)
	
--exporter = TSceneExporter()
--exporter.exportAll()
