-- This file holds all the references to the prefabs files ,json. It is used to bind a prefab to it's path on the engine data folder.
global mcv_prefabs_path = mcv_scripts_path + "/bin/data/prefabs/"

global EditorPrefabsManager

struct EditorPrefabsManager(
	-- Path to root asset directory where the folders with prefabs are (i.e folders where .json files are).
	pathToRootPrefabDirectory = mcv_prefabs_path + "/*",

	-- Asset dictionaries are ordered by key representing type of asset (city, interior, decoration, interaction, ...) and the value is
	-- another dictionary holding a key representing the asset name and the path of the .max file.
	prefabNameToRelativePath = Map(),
	
	-- Load asset into the given category.
	fn loadPrefab prefabPath prefab_category = (
		local basePath = "\\data\\prefabs\\"
		local start_base_dir = findString prefabPath "\\data\\prefabs\\"
		local startAssetDirectory = start_base_dir + basePath.count
		
		-- Get sure it's a JSON.
		local prefab_type = getFilenameType prefabPath
		if prefab_type != ".json" then
			return true
	
		-- Get the asset name (i.e the name of the .json file) from the full path.
		local prefab_name = getFilenameFile prefabPath
		local prefab_relative_directory = substring prefabPath startAssetDirectory prefabPath.count
		
		-- Get the asset dictionary for the given asset category.
		local prefabCategoryDict = prefabNameToRelativePath.getItemValue prefab_category
		
		-- If two assets have the same name and are loaded into the same category this error will throw, this is very unlikely to happen
		-- but we load by default all assets in the root assets directory into the root category, if another root category would appear,
		-- this could happen. We leave this here for completion.
		local prefabWasFound = prefabCategoryDict.hasValue prefab_name
		if prefabWasFound then(
			local message = "Two prefabs have the same name (" + prefab_name + ") in the dictionary, this should not happen. Do you have the same file in different folders?"
			messagebox message
			return false
		)
		
		-- Only add prefabs that have a maxscript.
		local isFoundInAssets = EditorAssetsManager.getAssetPath prefab_category prefab_name
		if isFoundInAssets == undefined then
			return true
	
		format "Adding prefab: % with relative_path % to category % \n" prefab_name prefab_relative_directory prefab_category
		
		prefab_relative_directory = substituteString prefab_relative_directory "\\" "/"
		-- Finally, add the item into the dictionary.
		prefabCategoryDict.addItem prefab_name prefab_relative_directory

		return true
	),
	
	
	-- Fetches all the assets from this directory and root ones.
	fn fetchPrefabsFromDirectories current_path category_prefabs isRoot =
	(	
		local loadedSuccessfully = true
		
		-- Get categories.
		local prefab_category = category_prefabs
		if not isRoot then(
			local filenamePath = getFilenamePath current_path
			local basePath = "\\data\\prefabs\\"
			local start_base_dir = findString filenamePath "\\data\\prefabs\\"
			local startAssetDirectory = start_base_dir + basePath.count
			
			prefab_category = substring current_path startAssetDirectory filenamePath.count
			-- The -2 removes the // at the end of the category.
			prefab_category = substring prefab_category 1 (prefab_category.count - 3)
		)
		
		-- Create dictionary for this type of prefab. Add them in it.
		local itemFound = prefabNameToRelativePath.getItemValue prefab_category
		if itemFound == undefined then(
			local newMap = Map()
			prefabNameToRelativePath.addItem prefab_category newMap
		)

		-- For all files in the folder, we load them into their category.
		local files = getFiles current_path
		for prefabPath in files do(
			if (loadPrefab prefabPath prefab_category) == false then
				loadedSuccessfully = false
		)
		
		-- Finally, get all directories in the current path and keep on moving downwards.
		local directories = GetDirectories (current_path)
		for directory in directories do(
			local directoryToFetch = directory + "\\*"
			if (fetchPrefabsFromDirectories directoryToFetch prefab_category false) == false then
				loadedSuccessfully = false
		)

		return loadedSuccessfully
	),
	
	-- Call this function to load assets starting from the root asset directory.
	fn loadPrefabsIntoEditor = (
		prefabNameToRelativePath = Map()
		
		format "Loading prefabs into editor.\n"
		-- We start with the root folder, as category for it we will call it "General".
		if (fetchPrefabsFromDirectories pathToRootPrefabDirectory "General" true) == true then(
			format "Loading finished correctly.\n"
		)
		else(
			prefabNameToRelativePath = Map()
			format "Something went wrong while loading prefabs.\n"
		)
	),
	
	fn getAllPrefabsNamesForTheGivenCategory prefabCategory = (
		local prefabCategoryMap = prefabNameToRelativePath.getItemValue prefabCategory
		if prefabCategoryMap == undefined then
			return undefined
		return prefabCategoryMap.keys
	),
	
	-- Given the name of an prefab, returns the prefab path.
	fn getPrefabPath selectedCategory prefabName = (
		if (selectedCategory == undefined or selectedCategory == "") or (prefabName == undefined or prefabName == "") then
			return undefined
		local prefabCategoryMap = prefabNameToRelativePath.getItemValue selectedCategory
		return prefabCategoryMap.getItemValue prefabName
	)
)


global EditorPrefabsManager = EditorPrefabsManager()
EditorPrefabsManager.loadPrefabsIntoEditor()