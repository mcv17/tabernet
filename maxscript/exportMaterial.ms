global materialsPathToRelative = Map() -- Stores maps and their relative path, can be used to set their filenames to a relative path, we use it at the end of the prefab export for that.

struct TMatMapAndPath (
	map,
	relativePath
)

fn isExportableMaterialClass classMat = (
	return classMat == StandardMaterial or classMat== Physical_Material
)

struct TMaterialExporter (
	
	fs = TJsonFormatter(),
	fsShadows = TJsonFormatter(),
	project_path,                 -- Asigned in the ctor 
	base_path,                 -- Asigned in the ctor 	
	
	fn exportMap map alias default_value sceneName prefabCategory materialName asInstanced = (
		-- Loads undefined textures if no map defined.
		if map == undefined then (
			local json_filename = base_path + "textures/" + default_value + ".dds"
			fs.writeKeyValue alias json_filename
			return false
		)
			
		local map_filename = map.filename
		
		
		if not asInstanced then(
			-- Return if not found.
			if(not doesFileExist map_filename) then(
				local textMat = "Material in path: " + map_filename + " wasn't found. Get sure the path is pointing to a correct location in your computer and not another one."
				MessageBox textMat
				local json_filename = base_path + "textures/" + default_value + ".dds"
				fs.writeKeyValue alias json_filename
				throw "MaterialNotFound"
			)
			
			-- Return if different drives, Tool won't work.
			-- Get drive letter.
			local drive_engine = substring mcv_scripts_path 1 2
			-- Get texture letter.
			local drive_texture = substring map_filename 1 2
			
			if drive_engine != drive_texture then(
				local messageText = "Attention, textures must be in the same drive than the engine when exporting them. Export the asset textures from the same unit disk than the engine. Right now the engine is located at: " + drive_engine + " and the texture at: " + drive_texture
				MessageBox messageText
				throw "DifferentUnitDisks"
			)
			
			-- Add material to change it's path to relative if not found. We will call this from the export as prefab, otherwise we ignore it.
			local matPath = materialName + alias -- materialname + albedo or whatever
			local itemFound = materialsPathToRelative.getItemValue matPath
			if itemFound == undefined then(
				-- Set path relative for the max file. Important for  it to work correctly afterward in the editor as we will be saving the file.
				-- We will go from: C:/users/texturefolder/a.png to mat1/a.png for example.
				local relativePathMaterial = materialName + "/" + filenamefrompath map.filename
				-- We take the AO as undefined. We are using the cutout map as AO, we set it to undefined for when we store the .max file for the editor.
				-- This way, the model won't look weird.
				if alias == "ao" then
					relativePathMaterial = ""
				
				local mapAndPath = TMatMapAndPath()
				mapAndPath.map = map
				mapAndPath.relativePath = relativePathMaterial
				
				materialsPathToRelative.addItem matPath mapAndPath
			)
		)
		
		-- Store in general everything that is not inside a folder.
		if(prefabCategory == "") then
			prefabCategory = "general/"
		
		-- Create texture folder for the prefab if necessary.
		-- Tabernet/data/textures/categoryoftheprefab/folderPrefab/objectFolder/
		local textures_folder_path = mcv_scripts_path + "/bin/" + base_path + "textures/" + prefabCategory + sceneName + "/"

		-- Create the base texture dir if it doesn't exist.
		if(not doesFileExist textures_folder_path) then
			makeDir textures_folder_path
		
		-- Create the dir inside the textures folder path for the given material.
		local textures_material_folder_path = textures_folder_path + materialName + "/"
		if(not doesFileExist textures_material_folder_path) then
			makeDir textures_material_folder_path
			
		-- Save the pngs to the max assets so they can be viewed when the max is used in the editor.
		-- C:/users/texturefolder/bricks.png
		local base_name = getFilenameFile map_filename -- bricks
		local base_type = getFilenameType map_filename -- .png
		m
		
		if not asInstanced then(
			-- C:/users/tabernet//maxscript/assets/props/brick/mat1/bricks.png
			local max_assets_material_path = mcv_scripts_path + "/maxscript/assets/" + prefabCategory + sceneName + "/" + materialName + "/"
			local max_assets_image_resource_path = max_assets_material_path + base_name + base_type
			
			-- Create folder in C:/users/tabernet//maxscript/assets/prefabcat/ for the given material if it doesn't exist.
			if(not doesFileExist max_assets_material_path) then
				makeDir max_assets_material_path
			
			copyFile map_filename max_assets_image_resource_path
			
			-- This command, convertes the texture to DDS.
			local command = "cd " + "\"" + mcv_scripts_path + "/tools/\"" + " && " + "texconv.exe " + "\"" + map_filename +  "\"" + " -o " + "\"" + textures_material_folder_path + "\""
			DOSCommand command
			
			local toolConverstionPath = mcv_scripts_path + "/tools/texconv.exe"
			if not doesFileExist toolConverstionPath then(
				MessageBox "Texture conversion tool wasn't found."
				throw "TextureToolMissing"
			)
		)
		
		-- Save the path of the texture.
		local texture_final_path = base_path + "textures/" + prefabCategory + sceneName + "/" +  materialName + "/" + base_name + ".dds"
		fs.writeKeyValue alias texture_final_path
		
		format "Exported texture % for % to % \n" base_name alias texture_final_path
		
		return texture_final_path
	),
	
	fn isValidName aname = (
		rx = dotNetObject  "System.Text.RegularExpressions.Regex" "[ *\\/�?�'�!\"�@&%()=+\-*�{}`:;,.[\]^�<>�]"
		return not rx.isMatch aname
	),
	
	-- Exports the shadow materials for a given object if it needs to. Only used by objects with alpha, all other materials use the same shadow materials.
	fn exportShadowMaterials mat_category_path nameOfMat albedo_path asInstanced = (
		
		local matName = "shadows_" + nameOfMat + "_alpha"
		if asInstanced then
			matName = matName + "_instanced_gpu"
		matName = matName + ".material"
		
		local mat_full_path = mat_category_path + matName
		format "Exporting shadow material % to path % \n" matName mat_full_path
		

		fsShadows.begin (project_path + mat_full_path)
			fsShadows.beginObj()
				if asInstanced then(
					fsShadows.writeKeyValue "technique" "shadows_instanced_culling_alpha.tech"
					fsShadows.writeComma()
				)
				else(
					fsShadows.writeKeyValue "technique" "shadows_alpha.tech"
					fsShadows.writeComma()
				)

				fsShadows.writeKey "textures" 
				fsShadows.beginObj()
					fsShadows.writeKeyValue "albedo" albedo_path
				fsShadows.endObj()
				fsShadows.writeComma()
				
				fsShadows.writeKeyValue "category" "shadows"
				fsShadows.writeComma()
				
				fsShadows.writeKeyValue "casts_shadows" false
				
			fsShadows.endObj()
		fsShadows.end()
				
		return mat_full_path
	),
	
	-- Exports a single material to a json format
	fn exportSingleMaterial obj mat mat_name mat_category_path mat_full_path sceneName prefabCategory asInstanced casts_shadows hasAlphaText is_skinned = (
		
		format "Exporting material % with class % to path % \n" mat.name (classof mat as string) mat_category_path

		if not (isValidName mat.name) then (
			throw ("Obj " + obj.name + " has a material with an invalid name " + mat.name + " get " +
				"sure to add a name to the material that is not a default one like Material # x" +
				" or has weird characters like �, spaces, etcetera";				)
		)
		
		fs.begin (project_path + mat_full_path )
		fs.beginObj()
			if asInstanced then(
				if hasAlphaText then
					fs.writeKeyValue "technique" "objs_alpha_culled_by_gpu.tech"
				else
					fs.writeKeyValue "technique" "objs_culled_by_gpu.tech"
				fs.writeComma()
			)
			else(
				if hasAlphaText then(
					fs.writeKeyValue "technique" "objs_alpha.tech"
					fs.writeComma()
				)
			)

			fs.writeKey "textures" 
			fs.beginObj()
			
			local albedoPath = ""

			-- Standard material. We should use the PBR one but we leave it here nevertheless.
			if classof mat == Standardmaterial then (
				albedoPath  = exportMap mat.diffusemap "albedo" "null_albedo" sceneName prefabCategory mat_name asInstanced
			    fs.writeComma()
				exportMap mat.bumpMap "normal" "null_normal" sceneName prefabCategory mat_name asInstanced
				fs.writeComma()
				exportMap mat.selfIllumMap "emissive" "null_height" sceneName prefabCategory mat_name asInstanced
				fs.writeComma()
				exportMap mat.displacementMap "height" "null_height" sceneName prefabCategory mat_name asInstanced 
			)
			else if classof mat == Physical_Material then(
				albedoPath = exportMap mat.base_color_map "albedo" "null_albedo" sceneName prefabCategory mat_name asInstanced
			    fs.writeComma()
				exportMap mat.bump_map "normal" "null_normal" sceneName prefabCategory mat_name asInstanced
				fs.writeComma()
				exportMap mat.metalness_map "metallic" "null_metallic" sceneName prefabCategory mat_name asInstanced
				fs.writeComma()
				exportMap mat.roughness_map "roughness" "null_roughness" sceneName prefabCategory mat_name asInstanced
				fs.writeComma()
				exportMap mat.emission_map "emissive" "null_emissive" sceneName prefabCategory mat_name asInstanced
				fs.writeComma()
				exportMap mat.displacement_map "height" "null_height" sceneName prefabCategory mat_name asInstanced
				fs.writeComma()
				exportMap mat.cutout_map "ao" "null_ao" sceneName prefabCategory mat_name asInstanced -- We use the cutout map for this.
			)
			fs.endObj()	
			fs.writeComma()
			
			-- Do we cast shadows.
			fs.writeKeyValue "casts_shadows" casts_shadows
			
			-- Create shadows material if necessary. Only if we are casting shadows and has alpha we should create a shadow mat.
			if casts_shadows then(
				local shadowMatPath = ""
				if hasAlphaText then
					shadowMatPath = exportShadowMaterials mat_category_path mat_name albedoPath asInstanced
				else if asInstanced then
					shadowMatPath = "data/materials/shadows_instanced_gpu.material"
				else if is_skinned then
					shadowMatPath = "data/materials/shadows_skin.material"
				else
					shadowMatPath = "data/materials/shadows.material"

				fs.writeComma()
				fs.writeKeyValue "shadows_material" shadowMatPath
			)
			
		
		fs.endObj()
		fs.end()
			
		format "Exported material successfully\n"
	),
	
	-- Add here all the materials that can be exported by the exportMaterial function.
	fn isExportableMaterial mat = (
		local classMat = classof mat
		return isExportableMaterialClass classMat
	),
	
	-- Will return an array of all the materials names used by obj and exported by us
	fn exportMaterial obj sceneName prefabCategory materials_path mat is_skinned = (
		local exported_material = #()
		
		-- Whether the given obj casts shadows or not, by default we set it to true.
		local casts_shadows = getUserProp obj "cast_shadows"
		if casts_shadows == undefined then
			casts_shadows =  true
		
		local hasAlphaText = getUserProp obj "has_alpha_texture_for_transparency"
		if hasAlphaText == undefined then
			hasAlphaText =  false

		if isExportableMaterial mat then (
			
			-- Get the path of the materials and the name of the material.
			local mat_name = "material_" + mat.name
			local mat_full_name = mat_name
			-- Not very cool but will do if we use it. Probably won't. Not going to rework materials for this.
			if not casts_shadows then
				mat_full_name += "_nocast"
			
			local mat_category_path = materials_path + prefabCategory + sceneName + "/" +  mat_full_name + "/"
			local mat_instanced_category_path = materials_path + prefabCategory + sceneName + "/" + mat_full_name + "/"
			local mat_full_path = mat_category_path + mat_name + ".material"
			local mat_instanced_full_path = mat_instanced_category_path + mat_name + "_instanced_gpu.material"
			
			-- Create the dir if it doesn't exist.
			local mat_folder_path = mcv_scripts_path + "/bin/" + mat_category_path
			if(not doesFileExist mat_folder_path) then
				makeDir mat_folder_path
			
			-- Create the dir if it doesn't exist.
			local mat_instanced_folder_path = mcv_scripts_path + "/bin/" + mat_instanced_category_path
			if(not doesFileExist mat_instanced_folder_path) then
				makeDir mat_instanced_folder_path
			
			append exported_material mat_full_path
			
			exportSingleMaterial obj mat mat_name mat_category_path mat_full_path sceneName prefabCategory false casts_shadows hasAlphaText is_skinned
			exportSingleMaterial obj mat mat_name mat_instanced_category_path mat_instanced_full_path sceneName prefabCategory true casts_shadows hasAlphaText is_skinned -- Instanced version.
			
		) else if classof mat == MultiMaterial then (
			local multi_mat = mat
			if classof obj != Editable_mesh then
				convertToMesh obj

			local materials_of_mesh = getMaterialsUsedByMesh obj
			for mat_idx = 1 to materials_of_mesh.count do (
				if materials_of_mesh[ mat_idx ] == undefined then continue
				local mat_of_mesh = multi_mat[ mat_idx ]
				
				if mat_of_mesh == undefined then throw ("Mesh " + obj.name + " is using a multimaterial in slot " + (mat_idx as string)+ " but the multimat does not have this submat")
				if not isExportableMaterial mat_of_mesh then throw ("Mesh " + obj.name + " is using a multimaterial in slot " + (mat_idx as string) + " but the multimat in this slot is not a exportable material")
				
				-- Get the path of the materials and the name of the material.
				local mat_name = "material_" + mat_of_mesh.name
				local mat_full_name = mat_name
				
				-- Not very cool but will do if we use it. Probably won't. Not going to rework materials for this.
				if not casts_shadows then
					mat_full_name += "_nocast"
				
				local mat_category_path = materials_path + prefabCategory + sceneName + "/" +  mat_full_name + "/"
				local mat_instanced_category_path = materials_path + prefabCategory + sceneName + "/" + mat_full_name + "/"
				local mat_full_path = mat_category_path + mat_name + ".material"
				local mat_instanced_full_path = mat_instanced_category_path + mat_name + "_instanced_gpu.material"
				
				-- Create the dir if it doesn't exist.
				local mat_folder_path = mcv_scripts_path + "/bin/" + mat_category_path
				if(not doesFileExist mat_folder_path) then
					makeDir mat_folder_path
				
				-- Create the dir if it doesn't exist.
				local mat_instanced_folder_path = mcv_scripts_path + "/bin/" + mat_instanced_category_path
				if(not doesFileExist mat_instanced_folder_path) then
					makeDir mat_instanced_folder_path
				
				append exported_material mat_full_path
				exportSingleMaterial obj mat_of_mesh mat_name mat_category_path mat_full_path sceneName prefabCategory false casts_shadows hasAlphaText is_skinned
				exportSingleMaterial obj mat_of_mesh mat_name mat_instanced_category_path mat_instanced_full_path sceneName prefabCategory true casts_shadows hasAlphaText is_skinned -- Instanced version.
			)
		)
		return exported_material
	)

	
)

--gc()
--me = TMaterialExporter project_path:"c:/code/engine/bin/" base_path:"data/"
--me.exportMaterial $.mat "data/materials/" $
