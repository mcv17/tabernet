if CullingVolumeRollout != undefined then
	try(destroyDialog CullingVolumeRollout)catch()

rollout CullingVolumeRollout "Culling Volume Editor"
(	
	local currentlySelectedCullingVolume = undefined
	local previousColorCullingVolume = undefined
	
	local baseColor = color 255 198 87
	local previousColorObj = #()

	local isStartingVolume = false
	local selectedRenderingNeighbors = #()
	local selectedLogicNeighbors = #()
	
	local highlightCullingVolume = color 255 255 0
	local highlightRenderingNeighbor = color 255 0 0
	local highlightLogicNeighbor = color 0 255 0
	local hightlightLogicAndRendering = color 0 0 255
	
	/* Layout */
	label titleCullingVolume "Culling Volume Editor"
	
	label spacing0 ""
	label current_status "Status: None"
	label spacing1 ""
	label info1 "Culling volume being set up: Yellow Colored"
	label info2 "Rendering Neighbors: Red Colored"
	label info3 "Logic Neighbors: Green Colored"
	label info4 "Rendering and Logic Neighbors: Blue Colored"
	label spacing2 ""

	-- Buttons
	button btn_mark_object_as_culling_volume "Mark object as culling volume."
	
	label spacing10 ""
	button btn_mark_starting "Mark object as starting culling."
	button btn_remove_starting "Remove object as starting culling."
	
	label spacing3 ""
	button btn_mark_rendering_neighbor "Mark object as rendering neighbor."
	button btn_remove_rendering_neighbor "Remove object as rendering neighbor."
	
	label spacing4 ""
	button btn_mark_logic_neighbor "Mark object as logic neighbor."
	button btn_remove_logic_neighbor "Remove object as logic neighbor."
	
	label spacing5 ""
	button btn_clear_volumes "Clear culling volume setup." 
	
	label spacing6 ""
	button btn_apply_changes_to_volume "Finish setting up the culling volume."
	
	label spacing7 ""
	listbox rendering_neighbors "Rendering Neighbors:" items:#("No rendering neighbors") readOnly:true
	label spacing8 ""
	listbox logical_neighbors "Logical Neighbors" items:#("No logical neighbors") readOnly:true
	
	-- Close prefab rollout.
	label spacing9 ""
	button btn_close_culling_volume_rollout "Close"
	
	groupBox grouping_actions_box "Culling Volume Actions" pos:[5,25] width:240 height:400
	
	-- Forward declaration.
	local clearCullingVolume
	local colorWithBaseAllScene
	local returnOriginalColors
	
	fn updateRenderingText = (
		neighbors = #()
		for obj in selectedRenderingNeighbors do
			append neighbors obj.name
		rendering_neighbors.items = neighbors
		
		if selectedRenderingNeighbors.count == 0 then
			append rendering_neighbors.items "No rendering neighbors"
	)

	fn updateLogicText = (
		neighbors = #()
		for obj in selectedLogicNeighbors do
			append neighbors obj.name
		logical_neighbors.items = neighbors

		if selectedLogicNeighbors.count == 0 then
			append logical_neighbors.items "No logical neighbors"
	)

	on btn_mark_starting pressed do(
		isStartingVolume = true
	)
	
	on btn_remove_starting pressed do(
		isStartingVolume = false
	)
	
	-- Mark culling volume
	on btn_mark_object_as_culling_volume pressed do(
		local selectedObjects = getCurrentSelection()
		if selectedObjects.count < 1 then(
			messagebox "No object is selected as culling volume, click on one before marking it as a culling volume."
			return false
		)
		if selectedObjects.count > 1 then(
			messagebox "More than one object is selected, you can only edit culling volumes one by one."
			return false
		)
		if selectedObjects[1] == currentlySelectedCullingVolume then(
			messagebox "Object is already marked as culling volume to setup."
			return false
		)
		
		-- We recolor the previously selected culling volume.
		if currentlySelectedCullingVolume != undefined then
			clearCullingVolume()
		
		currentlySelectedCullingVolume = selectedObjects[1]
		previousColorCullingVolume = currentlySelectedCullingVolume.wireColor
		currentlySelectedCullingVolume.wirecolor = highlightCullingVolume
		current_status.text = "Status: Marked obj " + currentlySelectedCullingVolume.name + " as culling box "
	)
	
	-- Mark rendering neighbor
	on btn_mark_rendering_neighbor pressed do(
		if currentlySelectedCullingVolume == undefined then(
			messagebox "No culling volume is selected. Choose one before starting marking neighbors."
			return false
		)
		
		local selectedObjects = getCurrentSelection()
		if selectedObjects.count < 1 then(
			messagebox "No object is selected as rendering neighbor, click on one before marking it as so."
			return false
		)
		
		for obj in selectedObjects do(
			if obj == currentlySelectedCullingVolume then(
				messagebox "There is no need to set the culling volume as a rendering neighbor. By default the culling volume already treats everything inside as renderable and executes the logic in it."
				continue
			)
			
			-- Ignore if already inserted.
			if findItem selectedRenderingNeighbors obj != 0 then(
				local message = "Object " + obj.name + " is already inserted as a rendering neighbor."
				messagebox message
				continue
			)
			
			-- Add to rendering neighbors.
			append selectedRenderingNeighbors obj
			local foundOnLogic = findItem selectedLogicNeighbors obj
			if foundOnLogic != 0 then
				obj.wireColor = hightlightLogicAndRendering
			else
				obj.wirecolor = highlightRenderingNeighbor
		)
		
		updateRenderingText()
	)
	
	-- Remove rendering neighbor
	on btn_remove_rendering_neighbor pressed do(
		if currentlySelectedCullingVolume == undefined then(
			messagebox "No culling volume is selected. Choose one before starting marking neighbors."
			return false
		)
		
		local selectedObjects = getCurrentSelection()
		if selectedObjects.count < 1 then(
			messagebox "No object is selected as rendering neighbor, click on one before removing it as so."
			return false
		)
		
		for obj in selectedObjects do(
			local idxToRemove = findItem selectedRenderingNeighbors obj
			if idxToRemove == 0 then(
				local message = "Object " + obj.name + " is not a rendering neighbor already."
				messagebox message
				continue
			)
			
			local foundOnLogic = findItem selectedLogicNeighbors obj
			if foundOnLogic != 0 then
				obj.wireColor = highlightLogicNeighbor
			else
				obj.wirecolor = baseColor
		
			deleteItem selectedRenderingNeighbors idxToRemove
		)
	)
	
	-- Mark logic neighbor
	on btn_mark_logic_neighbor pressed do(
		if currentlySelectedCullingVolume == undefined then(
			messagebox "No culling volume is selected. Choose one before starting marking neighbors."
			return false
		)
		
		local selectedObjects = getCurrentSelection()
		if selectedObjects.count < 1 then(
			messagebox "No object is selected as logic neighbor, click on one before marking it as so."
			return false
		)
		
		for obj in selectedObjects do(
			if obj == currentlySelectedCullingVolume then(
				messagebox "There is no need to set the culling volume as a logic neighbor by default the culling volume already treats everything inside as renderable and executes the logic in it."
				continue
			)
			
			-- Ignore if already inserted.
			if findItem selectedLogicNeighbors obj != 0 then(
				local message = "Object " + obj.name + " is already inserted as a logic neighbor."
				messagebox message
				continue
			)
				
			append selectedLogicNeighbors obj
			local foundOnRendering = findItem selectedRenderingNeighbors obj
			if foundOnRendering != 0 then
				obj.wirecolor = hightlightLogicAndRendering
			else
				obj.wirecolor = highlightLogicNeighbor
		)
		
		updateLogicText()
	)
	
	-- Remove logic neighbor
	on btn_remove_logic_neighbor pressed do(
		if currentlySelectedCullingVolume == undefined then(
			messagebox "No culling volume is selected. Choose one before starting removing neighbors."
			return false
		)
		
		local selectedObjects = getCurrentSelection()
		if selectedObjects.count < 1 then(
			messagebox "No object is selected as logic neighbor, click on one before marking it as so."
			return false
		)
		
		for obj in selectedObjects do(
			local idxToRemove = findItem selectedLogicNeighbors obj
			if idxToRemove == 0 then(
				local message = "Object " + obj.name + " is not a logic neighbor already."
				messagebox message
				continue
			)
			
			local foundOnRendering = findItem selectedRenderingNeighbors obj
			if foundOnRendering != 0 then
				obj.wireColor = highlightRenderingNeighbor
			else
				obj.wirecolor = baseColor
			
			deleteItem selectedLogicNeighbors idxToRemove
		)
	)
	
	-- Close the rollout.
	on btn_clear_volumes pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Cleared culling volume   Time: %\n\n" timeOfExecution
		
		if currentlySelectedCullingVolume == undefined then(
			messagebox "There is no culling volume setup to clear."
			return false
		)
		
		-- Clear properties and clear variables.
		setUserPropBuffer currentlySelectedCullingVolume ""
		clearCullingVolume()
		current_status.text = "Status: None"
	)
	
	-- Finish setting up the culling volume.
	on btn_apply_changes_to_volume pressed do(
		if currentlySelectedCullingVolume == undefined then(
			messageBox "You have not setup any culling volume. Press this button once your culling volume setup is done. If you want to close the window, press the x or the close button at the bottom."
			return false
		)
		
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Setting up culling volume   Time: %\n\n" timeOfExecution
		
		local clearProperties = true
		
		-- Pass culling parameters as values for the user properties that can be easily read once in the json.
		local renderingNeighborsAsString = "["
		local logicNeighborsAsString = "["
		for obj in selectedRenderingNeighbors do
			renderingNeighborsAsString += obj.name + " "
		renderingNeighborsAsString = substituteString renderingNeighborsAsString " " ", "
		renderingNeighborsAsString = substring renderingNeighborsAsString 1  (renderingNeighborsAsString.count-2)
		renderingNeighborsAsString = renderingNeighborsAsString + "]"
	
		for obj in selectedLogicNeighbors do
			logicNeighborsAsString += obj.name + " "
		logicNeighborsAsString = substituteString logicNeighborsAsString " " ", "
		logicNeighborsAsString = substring logicNeighborsAsString 1  (logicNeighborsAsString.count-2)
		logicNeighborsAsString = logicNeighborsAsString + "]"
		
		SceneEditor.setUserDefinedPropertyForObject currentlySelectedCullingVolume "scene_culling_volume" "culling_volume" clearProperties
		SceneEditor.setUserDefinedPropertyForObject currentlySelectedCullingVolume "starting_volume" isStartingVolume false
		SceneEditor.setUserDefinedPropertyForObject currentlySelectedCullingVolume "culling_volume_rendering_neighbors" renderingNeighborsAsString false
		SceneEditor.setUserDefinedPropertyForObject currentlySelectedCullingVolume "culling_volume_logic_neighbors" logicNeighborsAsString false
		
		-- Make this object the selected object.
		select currentlySelectedCullingVolume
		
		-- Add a trigger tag.
		local selectionD = getCurrentSelection()
		SceneEditor.setUserDefinedPropertyForObjs selectionD "trigger" "trigger" false
		SceneEditor.setUserDefinedPropertyForObjs selectionD "tags" "[trigger]" false
		
		-- Updates the user defined properties for the selected object if we have one showing in the editor.
		SceneEditor.SceneEditorRollout.updateUserDefinedPropertiesForSelectedObject()
		
		-- Clear the volume once done.
		clearCullingVolume()
		messageBox "Setup finished :)"
	)
	
	-- Close the rollout.
	on btn_close_culling_volume_rollout pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Closed culling volumes rollout   Time: %\n\n" timeOfExecution
		destroyDialog CullingVolumeRollout
	)
	
	-- Clears the culling volume control variables
	fn clearCullingVolume = (
		currentlySelectedCullingVolume.wirecolor = previousColorCullingVolume
		currentlySelectedCullingVolume = undefined
		previousColorCullingVolume = undefined
		
		for obj in selectedRenderingNeighbors do(
			local idxToRemove = findItem selectedRenderingNeighbors obj
			obj.wirecolor = baseColor
		)
		selectedRenderingNeighbors = #()
		
		for obj in selectedLogicNeighbors do(
			local idxToRemove = findItem selectedLogicNeighbors obj
			obj.wirecolor = baseColor
		)
		selectedLogicNeighbors = #()
		
		updateRenderingText()
		updateLogicText()
	)
	
	-- Called when the rollout opens.
	on CullingVolumeRollout open do(
		-- Set visualization mode to show colors of the objects.
		displayColor.shaded = #object
		current_status.text = "Status: None"
		
		colorWithBaseAllScene()
	)
	
	-- Called when the rollout closes.
	on CullingVolumeRollout close do(
		-- We recolor the previously selected culling volume.
		if currentlySelectedCullingVolume != undefined then
			clearCullingVolume()
		
		returnOriginalColors()
		previousColorObj = #()
		
		-- Set visualization mode to show materials of the objects.
		displayColor.shaded = #material
	)
	
	fn colorWithBaseAllScene =(
		for obj in $* do(
			append previousColorObj obj.wirecolor
			obj.wirecolor = baseColor
		)
	)
	
	fn returnOriginalColors =(
		local objectsInScene = $*
		for i=1 to objectsInScene.count do
			objectsInScene[i].wirecolor = previousColorObj[i]
		previousColorObj = #()
	)
)