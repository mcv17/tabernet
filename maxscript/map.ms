-- Max doesn't seem to have a dictionary like structure by default so I had to implement it.
struct Map(
	keys = #(),
	values = #(),

	fn addItem key keyVal = (
		local success = appendIfUnique keys key
		if success == false then return success
		values = append values keyVal
		return success
	),
	
	fn removeByKey key = (
		local itemFound = findItem keys key 
		if itemFound == 0 then return false -- Item not found.
			
		-- Remove item.
		keys = deleteItem keys itemFound
		values = deleteItem values itemFound
	),
	
	fn getItemValue key = (
		local itemFound = findItem keys key 
		if itemFound == 0 then return undefined -- Item not found.
		return values[itemFound]
	),
	
	fn hasValue key = (
		local itemFound = findItem keys key 
		if itemFound == 0 then return false -- Item not found.
		return true
	),
	
	-- Sort by key.
	fn sortByKey = (
		local oldKeys = deepcopy keys
		local oldValues = deepcopy values

		-- Sort keys.
		sort keys
		for newValueIdx = 1 to keys.count do(
			local oldValueIdx = findItem oldKeys keys[newValueIdx]
			values[newValueIdx] = oldValues[oldValueIdx]
		)
	),
	
	-- Print functions.
	fn printKeys = (
		for i = 1 to keys.count do(
			format "Key: %.\n" keys[i]
		)
	),
	
	fn printValues = (
		for i = 1 to keys.count do(
			format "Value: %\n" values[i]
		)
	),
	
	fn printContents = (
		for i = 1 to keys.count do(
			format "Key: %. Value: %\n" keys[i] values[i]
		)
	)
)

/*
d = Map()
d.addItem "Hola" 10
d.addItem "Hola" 10
d.addItem "Hola2" 20
d.addItem "Hola4" 30
d.addItem "Hola6" 90
d.addItem "Hola4" 30
d.addItem "Hola5" 40

d.printContents()
d.sortByKey()
d.printContents()

d.removeByKey "Hola5"
d.printContents()

d.getItemValue "Hola2"
*/