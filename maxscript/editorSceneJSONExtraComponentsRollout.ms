if JSONEditor != undefined then
	try(destroyDialog JSONEditor)catch()

rollout JSONEditor "JSON Editor"
(	
	/* Layout */
	label titleSpawn "JSON selector"
	
	-- Text box that shows the user properties of the object.
	edittext prefix_txt "JSON data:" fieldWidth:470 height:600 labelOnTop:true
	
	button btn_create_JSON "Add JSON data"
	label spacing1 ""
	button btn_close_JSON_rollout "Close"
	
		-- Close the rollout.
	on btn_create_JSON pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Create JSON Time: %\n\n" timeOfExecution

		local ParsedText = substituteString  prefix_txt.text "\n" ""
		ParsedText = substituteString  ParsedText "\t" ""
		ParsedText = substituteString  ParsedText "  " ""
		
		local selectionD = getCurrentSelection()
		
		SceneEditor.setUserDefinedPropertyForObjs selectionD "json" ParsedText false
		
		-- Updates the user defined properties for the selected object if we have one showing in the editor.
		SceneEditor.SceneEditorRollout.updateUserDefinedPropertiesForSelectedObject()
		destroyDialog JSONEditor
	)
	
	-- Close the rollout.
	on btn_close_JSON_rollout pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Closed JSON rollout   Time: %\n\n" timeOfExecution
		destroyDialog JSONEditor
	)
	
	-- Called when the rollout opens.
	on JSONEditor open do()
)