if ColliderRollout != undefined then
	try(destroyDialog ColliderRollout)catch()

if GroupRollout != undefined then
	try(destroyDialog GroupRollout)catch()

rollout ColliderRollout "Collider Editor"
(	
	/* Layout */
	label titleCollider "Collider selector"
	
	-- List of collider types.
	listbox collider_list "Collider types:" items:#("Static", "Dynamic", "CharacterController")

	-- Set collider.
	label spacing1 ""
	button btn_make_collider_type "Make collider of selected type"
	label spacing2 ""

	-- Close collider rollout.
	button btn_close_collider_rollout "Close"
	
	-- Mark collider.
	on btn_make_collider_type pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Make Collider Time: %\n\n" timeOfExecution
		local selectionD = getCurrentSelection()
		SceneEditor.setUserDefinedPropertyForObjs selectionD "collider" collider_list.selected false
		-- Updates the user defined properties for the selected object if we have one showing in the editor.
		SceneEditor.SceneEditorRollout.updateUserDefinedPropertiesForSelectedObject()
		destroyDialog ColliderRollout
		createDialog GroupRollout
	)
	
	-- Close the rollout.
	on btn_close_collider_rollout pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Closed collider rollout   Time: %\n\n" timeOfExecution
		destroyDialog ColliderRollout
	)
	
	-- Called when the rollout opens.
	on ColliderRollout open do()
)

rollout GroupRollout "Group Editor"
(	
	/* Layout */
	label titleGroup "Group selector"
	
	-- List of groups. For now hardcoded. Not going to create a file and read it for something we are hardly going to use.
	listbox group_list "Group types:" items:#("scenario", "all", "player", "enemy", "characters", "wall", "floor", "trigger", "ragdoll", "enemyHitboxes", "pickUps", "bullets")

	-- Set group.
	label spacing1 ""
	button btn_make_group_type "Mark collider with group"
	label spacing2 ""

	-- Close prefab rollout.
	button btn_close_group_rollout "Close"
	
	-- Create a prefab button.
	on btn_make_group_type pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Mark Collider with Group Time: %\n\n" timeOfExecution
		local selectionD = getCurrentSelection()
		SceneEditor.setUserDefinedPropertyForObjs selectionD "group" group_list.selected false
		
		-- Updates the user defined properties for the selected object if we have one showing in the editor.
		SceneEditor.SceneEditorRollout.updateUserDefinedPropertiesForSelectedObject()
		destroyDialog GroupRollout
	)
	
	-- Close the rollout.
	on btn_close_group_rollout pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Closed group rollout   Time: %\n\n" timeOfExecution
		destroyDialog GroupRollout
	)
	
	-- Called when the rollout opens.
	on GroupRollout open do()
)