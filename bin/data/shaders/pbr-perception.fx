//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "pbr_common.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
void PS_PERCEPTION(
			VS_OUTPUT input,
      out float4 o_albedo : SV_Target0,
      out float4 o_normal : SV_Target1,
      out float1 o_depth  : SV_Target2,
      out float4 o_self_illumination  : SV_Target3,
      out float1 o_drawn_to  : SV_Target4,
      out float2 o_perception_mask : SV_Target5
  )
{
  float4 albedo_color = txAlbedo.Sample(samLinear, input.Uv);
  o_albedo.xyz = albedo_color.xyz;
  o_albedo.a = txMetallic.Sample(samLinear, input.Uv).r;
	
	// Convert to grayscale.
	float average = (o_albedo.r + o_albedo.g + o_albedo.b)/3.0;
	o_albedo.r = average;
	o_albedo.g = average;
	o_albedo.b = average;
	
	o_albedo.r *= ObjColor.r;
	o_albedo.g *= ObjColor.g;
	o_albedo.b *= ObjColor.b;

	// Normal mapping
  float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
  N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1

  float3 T = input.T.xyz;
  float3 B = cross(T, input.N) * input.T.w;
  float3x3 TBN = float3x3(T,B,input.N);
  float3 N = mul( N_tangent_space.xyz, TBN );   // Normal from NormalMap

  // Save roughness in the alpha coord of the N render target
  float roughness = txRoughness.Sample(samLinear, input.Uv).r;
  o_normal = encodeNormal(N, roughness);

  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  float  linear_depth = dot( cam2obj, CameraFront ) / CameraZFar;

  o_depth = linear_depth;
  o_drawn_to = linear_depth;

  // Self illumination.
  float4 emissive_color = txEmissive.Sample(samLinear, input.Uv);
  o_self_illumination = float4(emissive_color.rgb, 1);

  // Store the depth and the mask value of the perceptive object.
  o_perception_mask = float2(linear_depth, PerceptionMaskValue);
}