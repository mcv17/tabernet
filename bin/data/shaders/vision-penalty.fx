#include "screen_quad.fx"
#include "noise.fx"

float4 PS_CORRUPTION(VS_FULL_OUTPUT input) : SV_TARGET
{
  // Fades the effect.
  float2 uv = input.UV.xy;
  float2 coord = (uv.xy - 0.5);
  float rf = sqrt(dot(coord, coord)) * CorruptionFallOff;
  float rf2_1 = rf * rf + 1.0;
  float mixFactor = 1.0 / (rf2_1 * rf2_1);
  
  // Combines corruption color and screen color based on the fadeout effect.
  float result = warpingExposed(uv, GlobalWorldTime, 0.35, 0.8, 2.0, 0.5, 6);
  float3 color = CorruptionColor/result;
  color = abs(color);
  
  return float4(lerp(txAlbedo.Sample( samLinear, input.UV ).rgb, color.rgb, (1.0 - mixFactor)), 1.0);
}

