#ifndef PARALLAX
#define PARALLAX

#include "pbr.fx" // We are rendering our parallax effect with pbr from a deferred renderer.

//--------------------------------------------------------------------------------------
// Parallax shaders.
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

struct VS_OUTPUT_PARALLAX
{
  float4 Pos      : SV_POSITION;
  float3 N        : NORMAL;
  float2 Uv       : TEXCOORD0;
  float3 WorldPos : TEXCOORD1;
  float4 T        : NORMAL1;
  float3 TangentViewPos : TEXCOORD2;
  float3 TangentFragPos : TEXCOORD3;
};

// VS for parallax.
VS_OUTPUT_PARALLAX VS_Parallax(
  VS_INPUT input
)
{
  VS_OUTPUT_PARALLAX output = (VS_OUTPUT_PARALLAX)0;
  output.Pos = mul(input.Pos, World);
  output.WorldPos = output.Pos.xyz; // Position in ws.
  output.Pos = mul(output.Pos, ViewProjection); // Fragment position projected.
  output.N = normalize(mul(input.N, (float3x3)World)); // Normal rotated in ws.
  output.T = float4( normalize(mul(input.T.xyz, (float3x3)World)), input.T.w); // Tangent rotated in ws.
  output.Uv = input.Uv; // UV coords.

  // Get tanget space view position of the camera 
  // and tangent space world position of the object. 
  float3x3 TBN = transpose(computeTBN( output.N, output.T)); 
  output.TangentViewPos = mul(CameraPosition, TBN);
  output.TangentFragPos = mul(output.WorldPos, TBN);

  return output;
}

//--------------------------------------------------------------------------------------
// Fragment Shaders
//--------------------------------------------------------------------------------------

float2 ParallaxMapping(float2 texCoords, float3 viewDir); // Basic parallax mapping effect.
float2 SteepParallaxMapping(float2 texCoords, float3 viewDir); // Steep parallax mapping effect.
float2 POMParallaxMapping(float2 texCoords, float3 viewDir); // POM parallax mapping effect.

// Pixel shader for basic parallax effect.
void PS_Parallax( 
  VS_OUTPUT_PARALLAX input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float1 o_depth  : SV_Target2,
  out float4 o_self_illumination  : SV_Target3,
  out float1 o_drawn_to  : SV_Target4
){
  // We get the view direction between obj fragment position and camera view position.
  float3 viewDir = normalize(input.TangentViewPos - input.TangentFragPos);
  
  // Calculate the UV we must sample from.
  input.Uv = POMParallaxMapping(input.Uv, viewDir);  
  if(input.Uv.x > 1.0 || input.Uv.y > 1.0 || input.Uv.x < 0.0 || input.Uv.y < 0.0)
    discard;

  VS_OUTPUT inputP = (VS_OUTPUT)0;
  inputP.Pos = input.Pos;
  inputP.N = input.N;
  inputP.Uv = input.Uv;
  inputP.WorldPos = input.WorldPos;
  inputP.T = input.T;

  // Now draw the GBuffer with the new UV coordinates that are affect by the parallax effect.
  PS_Common(inputP, o_albedo, o_normal, o_depth, o_self_illumination, o_drawn_to, false);
}

// Effects adapted from the tutorial: https://learnopengl.com/Advanced-Lighting/Parallax-Mapping

// Basic parallax mapping effect.
float2 ParallaxMapping(float2 texCoords, float3 viewDir){
  const float height_scale = parallax_height_intensity; // Controls the parallax intensity.
  float height = 1.0 - txHeight.Sample(samLinear, texCoords).r; // Sample the height map.
  float2 P = viewDir.xy/viewDir.z * height * height_scale; // Get the P vector for parallax effect.
  // viewDir.xy/viewDir.z, when parallel, z close 0.0, large P, when perpendicular, low P.
  // Therefore, larger or smaller parallax effect.
  // If we remove viewDir.z it's called Parallax Mapping with Offset Limiting,
  // Try both and see which you prefer.

  return texCoords - P;
}

// Steep parallax mapping effect.
float2 SteepParallaxMapping(float2 texCoords, float3 viewDir){
  const float height_scale = parallax_height_intensity; // Controls the parallax intensity.
  
  // Number of depth layers. We use more or less based on the angle with the view dir.
  // When dot is 1 (ie. same dir, we use less samples).
  const float minLayers = parallax_min_layers;
  const float maxLayers = parallax_max_layers;
  float numDepthLayers = lerp(maxLayers, minLayers, abs(dot(float3(0.0, 0.0, 1.0), viewDir)));  

  // Size of each layer.
  float layerDepth = 1.0 / numDepthLayers;
  
  // Depth of the current layer (0.0, starting one).
  float currentLayerDepth = 0.0;
  
  // How much we shift the texture coordinates per layer (from vector P)
  float2 P = viewDir.xy * height_scale; 
  float2 deltaTexCoords = P / numDepthLayers;

  // Iterate through all layers from the top until we hit a depthmap value
  // smaller than the current layer's depth.
  float2 currentTextCoordinates = texCoords;
  float currentDepthMapValue = 1.0 - txHeight.Sample(samLinear, currentTextCoordinates).r; // Sample the height map.
  
  [unroll(32)]
  while(currentLayerDepth < currentDepthMapValue){
    // Shift texture coordinates along direction of P
    currentTextCoordinates -= deltaTexCoords;
    // Sample height.
    currentDepthMapValue = 1.0 - txHeight.Sample(samLinear, currentTextCoordinates).r;  
    // Get depth of next layer
    currentLayerDepth += layerDepth;  
  }

  return currentTextCoordinates;
}

// Steep parallax mapping effect.
float2 POMParallaxMapping(float2 texCoords, float3 viewDir){
  const float height_scale = parallax_height_intensity; // Controls the parallax intensity.

  // number of depth layers
  const float minLayers = parallax_min_layers;
  const float maxLayers = parallax_max_layers;
  float numDepthLayers = lerp(maxLayers, minLayers, abs(dot(float3(0.0, 0.0, 1.0), viewDir)));  

  // calculate the size of each layer
  float layerDepth = 1.0 / numDepthLayers;
  // depth of current layer
  float currentLayerDepth = 0.0;
  // the amount to shift the texture coordinates per layer (from vector P)
  float2 P = viewDir.xy / viewDir.z * height_scale; 
  float2 deltaTexCoords = P / numDepthLayers;
  
  // get initial values
  float2 currentTextCoordinates = texCoords;
  float currentDepthMapValue = 1.0 - txHeight.Sample(samLinear, currentTextCoordinates).r;
  
  [unroll(32)]
  while(currentLayerDepth < currentDepthMapValue)
  {
      // shift texture coordinates along direction of P
      currentTextCoordinates -= deltaTexCoords;
      // get depthmap value at current texture coordinates
      currentDepthMapValue = 1.0 - txHeight.Sample(samLinear, currentTextCoordinates).r;
      // get depth of next layer
      currentLayerDepth += layerDepth;  
  }
  
  // get texture coordinates before collision (reverse operations)
  float2 prevTexCoords = currentTextCoordinates + deltaTexCoords;

  // get depth after and before collision for linear interpolation
  float afterDepth  = currentDepthMapValue - currentLayerDepth;
  float beforeDepth = txHeight.Sample(samLinear, prevTexCoords).r - currentLayerDepth + layerDepth;
  
  // interpolation of texture coordinates
  float weight = afterDepth / (afterDepth - beforeDepth);
  float2 finalTexCoords = prevTexCoords * weight + currentTextCoordinates * (1.0 - weight);

  return finalTexCoords;
}

#endif