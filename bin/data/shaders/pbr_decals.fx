//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "pbr_common.fx"
#include "gpu_culling.fx"

//--------------------------------------------------------------------------------------
struct V2P  // Vertex to Pixel info
{
  float4 Pos:     SV_POSITION;

  float3 decal_center : TEXCOORD0;
  float3 decal_axis_x : TEXCOORD1;
  float3 decal_axis_z : TEXCOORD2;

  float  opacity: TEXCOORD3;
  float2 uv:      TEXCOORD4;
  float4 color:   TEXCOORD5;
  float3 decal_axis_y : TEXCOORD6;
};

//--------------------------------------------------------------------------------------
// Vertex Shader for a decal without instancing. No fade in or out.
//--------------------------------------------------------------------------------------
V2P VS(
    // From stream 0 we read the instanced mesh (the billboard)
    // Stream 0
    in float3 iPos     : POSITION,
    in float2 iTex0    : TEXCOORD0
)
{
  V2P output = (V2P)0;
  // Transform local vertex of the box to the world coordinates
  float4 world_pos = mul(float4(iPos,1), World);

  // Transform it to camera projective space
  output.Pos = mul( world_pos, ViewProjection );

  // From the world matrix, extract the main axis x & z, and the center
  float3 center  = float3( World[3][0], World[3][1], World[3][2] );
  // Use the x axis for the width of the decal.
  float3 decal_x = float3( World[0][0], World[0][1], World[0][2] );
  // Use the y axis for the height of the decal.
  float3 decal_y = float3( World[1][0], World[1][1], World[1][2] );
  // Use the z axis for the depth of the decal.
  float3 decal_z = float3( World[2][0], World[2][1], World[2][2] );

  // Precompute in the vs the axis to compute the local coords from world coords 
  output.decal_center = center;
  float decal_inv_size_x = 1 / ( dot( decal_x, decal_x ) );
  float decal_inv_size_y = 1 / ( dot( decal_y, decal_y ) );
  float decal_inv_size_z = 1 / ( dot( decal_z, decal_z ) );
  output.decal_axis_x = decal_x * decal_inv_size_x;
  output.decal_axis_y = decal_y * decal_inv_size_y;
  output.decal_axis_z = decal_z * decal_inv_size_z;

  output.opacity = 1.0;
  output.color = ObjColor;
  output.uv = iTex0;

  return output;
}

V2P VS_STATIC_GPU_CULLING(
  in VS_INPUT input,
  in uint InstanceID : SV_InstanceID,
  StructuredBuffer<TCulledInstance> culled_instances : register(t0)
)
{
  V2P output = (V2P)0;
  
  // Get world matrix.
  TCulledInstance culled_instance = culled_instances[ instance_base + InstanceID ];
  float4x4 instance_world = culled_instance.world;

  // Transform local vertex of the box to the world coordinates
  float4 world_pos = mul(input.Pos, instance_world);

  // Transform it to camera projective space
  output.Pos = mul( world_pos, ViewProjection );

  // From the world matrix, extract the main axis x & z, and the center
  float3 center  = float3( instance_world[3][0], instance_world[3][1], instance_world[3][2] );
  // Use the x axis for the width of the decal.
  float3 decal_x = float3( instance_world[0][0], instance_world[0][1], instance_world[0][2] );
  // Use the y axis for the height of the decal.
  float3 decal_y = float3( instance_world[1][0], instance_world[1][1], instance_world[1][2] );
  // Use the z axis for the depth of the decal.
  float3 decal_z = float3( instance_world[2][0], instance_world[2][1], instance_world[2][2] );

  // Precompute in the vs the axis to compute the local coords from world coords 
  output.decal_center = center;
  float decal_inv_size_x = 1 / ( dot( decal_x, decal_x ) );
  float decal_inv_size_y = 1 / ( dot( decal_y, decal_y ) );
  float decal_inv_size_z = 1 / ( dot( decal_z, decal_z ) );
  output.decal_axis_x = decal_x * decal_inv_size_x;
  output.decal_axis_y = decal_y * decal_inv_size_y;
  output.decal_axis_z = decal_z * decal_inv_size_z;

  output.opacity = 1.0;
  output.color = culled_instance.objColor;
  output.uv = input.Uv;

  return output;
}

V2P VS_DYNAMIC_INSTANCED(
    // From stream 0 we read the instanced mesh (the billboard)
    // Stream 0
    in float3 iPos     : POSITION,
    in float2 iTex0    : TEXCOORD0,
    
    // From stream 1 we read the instance information 
    in VS_INSTANCE_WORLD instance_data,      // Stream 1
    in float4 InstanceColor   : TEXCOORD6,
    in float InstanceTimeToLife : TEXCOORD7,
    in float InstanceTimeToBlendOut : TEXCOORD8
)
{
  V2P output = (V2P)0;
  
  // Build a World matrix from the instance information
  float4x4 instance_world = getWorldOfInstance(instance_data);

  // Transform local vertex of the box to the world coordinates
  float4 world_pos = mul(float4(iPos,1), instance_world);

  // Transform it to camera projective space
  output.Pos = mul( world_pos, ViewProjection );

  // From the world matrix, extract the main axis x & z, and the center
  float3 center  = float3( instance_world[3][0], instance_world[3][1], instance_world[3][2] );
  // Use the x axis for the width of the decal.
  float3 decal_x = float3( instance_world[0][0], instance_world[0][1], instance_world[0][2] );
  // Use the y axis for the height of the decal.
  float3 decal_y = float3( instance_world[1][0], instance_world[1][1], instance_world[1][2] );
  // Use the z axis for the depth of the decal.
  float3 decal_z = float3( instance_world[2][0], instance_world[2][1], instance_world[2][2] );

  // Precompute in the vs the axis to compute the local coords from world coords 
  output.decal_center = center;
  float decal_inv_size_x = 1 / ( dot( decal_x, decal_x ) );
  float decal_inv_size_y = 1 / ( dot( decal_y, decal_y ) );
  float decal_inv_size_z = 1 / ( dot( decal_z, decal_z ) );
  output.decal_axis_x = decal_x * decal_inv_size_x;
  output.decal_axis_y = decal_y * decal_inv_size_y;
  output.decal_axis_z = decal_z * decal_inv_size_z;

  // Blendout. When time to life is smaller than fade out, it will start to blend.
  output.opacity = smoothstep( 0, InstanceTimeToBlendOut, InstanceTimeToLife);
  output.color = InstanceColor;
  output.uv = iTex0;

  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
void PS(
  in V2P input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float4 o_self_illumination : SV_Target2
)
{
  // Screen coords
  float2 iPosition = input.Pos.xy;

  // As int3
  int3 ss_load_coords = uint3(iPosition, 0);

  // Recover the position of the world for that pixelx
  float  zlinear = txGLinearDepth.Load(ss_load_coords).x;
  float3 wPos = getWorldCoords(iPosition, zlinear);

  // Convert to local unit decal space. We want a value that goes from 0 to 1. and from left to right.
  // So it is correctly mapped. By default, left is positive and 0.5 at the topleft 
  // and right is negative. We use the -0.5 and the final negation to make left start in 0.0 at the edge
  // of the decal and end in 1.0 in the right.
  float3 decal_center_to_wPos = wPos - input.decal_center;
  float amount_of_x = 1.0 - -(dot( decal_center_to_wPos, input.decal_axis_x) - 0.5); 
  float amount_of_y = -(dot( decal_center_to_wPos, input.decal_axis_y) - 0.5); 
  float amount_of_z = dot( decal_center_to_wPos, input.decal_axis_z);
  
  // Clip if outside volume.
  if(amount_of_x < 0.0 || amount_of_x > 1.0)
    clip(-1.0);
  if(amount_of_y < 0.0 || amount_of_y > 1.0)
    clip(-1.0);
  clip(1.0 - abs(amount_of_z * 2));
  
  o_albedo = txAlbedo.Sample(samBorderColor, float2(amount_of_x, amount_of_y));
  o_albedo.a = o_albedo.a * input.opacity;
  //clip(o_albedo.a - 0.1);


  // Convert to local unit decal space. We want a value that goes from 0 to 1. and from left to right.
  // So it is correctly mapped. By default, left is positive and 0.5 at the topleft 
  // and right is negative. We use the -0.5 and the final negation to make left start in 0.0 at the edge
  // of the decal and end in 1.0 in the right. Take into account we are using a unit cube so max coord in
  // any axis will be 0.5
  float vertical_factor = 1 - abs( amount_of_z * 2 );
  o_albedo.a *= vertical_factor;

  // From http://martindevans.me/game-development/2015/02/27/Drawing-Stuff-On-Other-Stuff-With-Deferred-Screenspace-Decals/.
  float3 ddxWp = ddx(wPos);
  float3 ddyWp = ddy(wPos);
  float3 pixelNormal = normalize(cross(ddyWp, ddxWp));
  
  float3 pixelBinormal = normalize(ddxWp);
  float3 pixelTangent = normalize(ddyWp);

  //Create a matrix transforming from tangent space to view space
  float3x3 TBN;
  TBN[0] = pixelTangent;
  TBN[1] = pixelBinormal;
  TBN[2] = pixelNormal;

  float4 decal_normal = txNormal.Sample(samBorderColor, float2(amount_of_x, amount_of_y));
  decal_normal.xyz = decal_normal.xyz * 2.0 - 1.0;
  // Transform normal from tangent space into view space
  decal_normal.xyz = mul(decal_normal.xyz, TBN);
  o_normal = encodeNormal(decal_normal.xyz, decal_normal.a * input.opacity);

  float3 emissive_color = txEmissive.Sample(samBorderColor, float2(amount_of_x, amount_of_y)).rgb;
  o_self_illumination = float4(emissive_color * scalar_emissive, 0.0);

  // Change to true 'see' the boxes
  if( false ) {
    o_albedo.a += 0.3;
   
    if( (input.uv.x < 0.01 || input.uv.x > 0.99 ) || (input.uv.y < 0.01 || input.uv.y > 0.99 ) )
      o_albedo.a = 1.0;
  }
}

void PS_COLORED(
  in V2P input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float4 o_self_illumination : SV_Target2
)
{
  // Screen coords
  float2 iPosition = input.Pos.xy;

  // As int3
  int3 ss_load_coords = uint3(iPosition, 0);

  // Recover the position of the world for that pixelx
  float  zlinear = txGLinearDepth.Load(ss_load_coords).x;
  float3 wPos = getWorldCoords(iPosition, zlinear);


  // Convert to local unit decal space. We want a value that goes from 0 to 1. and from left to right.
  // So it is correctly mapped. By default, left is positive and 0.5 at the topleft 
  // and right is negative. We use the -0.5 and the final negation to make left start in 0.0 at the edge
  // of the decal and end in 1.0 in the right. Take into account we are using a unit cube so max coord in
  // any axis will be 0.5
  float3 decal_center_to_wPos = wPos - input.decal_center;
  float amount_of_x = -(dot( decal_center_to_wPos, input.decal_axis_x) - 0.5); 
  float amount_of_y = -(dot( decal_center_to_wPos, input.decal_axis_y) - 0.5); 
  float amount_of_z = dot( decal_center_to_wPos, input.decal_axis_z);
  
  // Clip if outside volume.
  if(amount_of_x < 0.0 || amount_of_x > 1.0)
    clip(-1.0);
  if(amount_of_y < 0.0 || amount_of_y > 1.0)
    clip(-1.0);
  clip(1.0 - abs(amount_of_z * 2));
  
  float4 decal_color = txAlbedo.Sample(samBorderColor, float2(amount_of_x, amount_of_y));
  o_albedo.xyz = input.color.xyz;
  o_albedo.a = decal_color.a * input.opacity;

  // Fade out the texture as we move away from the oficial plane XZ of the decal.
  // This is specially useful for declas that hare set in steep changes of geometry
  // like blood splatters in walls, it gives a fading effect to the decal that makes
  // it look like dripping blood.
  float vertical_factor = 1 - abs( amount_of_z * 2 );
  o_albedo.a *= vertical_factor;

  // From http://martindevans.me/game-development/2015/02/27/Drawing-Stuff-On-Other-Stuff-With-Deferred-Screenspace-Decals/.
  float3 ddxWp = ddx(wPos);
  float3 ddyWp = ddy(wPos);
  float3 pixelNormal = normalize(cross(ddyWp, ddxWp));
  float3 pixelBinormal = normalize(ddxWp);
  float3 pixelTangent = normalize(ddyWp);

  //Create a matrix transforming from tangent space to view space
  float3x3 TBN;
  TBN[0] = pixelTangent;
  TBN[1] = pixelBinormal;
  TBN[2] = pixelNormal;

  float4 decal_normal = txNormal.Sample(samBorderColor, float2(amount_of_x, amount_of_y));
  decal_normal.xyz = decal_normal.xyz * 2.0 - 1.0;
  // Transform normal from tangent space into view space
  decal_normal.xyz = mul(decal_normal.xyz, TBN);
  o_normal = encodeNormal(decal_normal.xyz, decal_normal.a * input.opacity);

  float3 emissive_color = txEmissive.Sample(samBorderColor, float2(amount_of_x, amount_of_y)).rgb;
  o_self_illumination = float4(emissive_color * scalar_emissive, o_albedo.a);

  // Change to true 'see' the boxes
  if( false ) {
    o_albedo.a += 0.3;
   
    if( (input.uv.x < 0.01 || input.uv.x > 0.99 ) || (input.uv.y < 0.01 || input.uv.y > 0.99 ) )
      o_albedo.a = 1.0;
  }
}

void PS_Totem(
  in V2P input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float4 o_self_illumination : SV_Target2
)
{
  // Screen coords
  float2 iPosition = input.Pos.xy;

  // As int3
  int3 ss_load_coords = uint3(iPosition, 0);

  // Recover the position of the world for that pixelx
  float  zlinear = txGLinearDepth.Load(ss_load_coords).x;
  float3 wPos = getWorldCoords(iPosition, zlinear);

  // Convert to local unit decal space. We want a value that goes from 0 to 1. and from left to right.
  // So it is correctly mapped. By default, left is positive and 0.5 at the topleft 
  // and right is negative. We use the -0.5 and the final negation to make left start in 0.0 at the edge
  // of the decal and end in 1.0 in the right. Take into account we are using a unit cube so max coord in
  // any axis will be 0.5
  float3 decal_center_to_wPos = wPos - input.decal_center;
  float amount_of_x = -(dot( decal_center_to_wPos, input.decal_axis_x) - 0.5); 
  float amount_of_y = -(dot( decal_center_to_wPos, input.decal_axis_y) - 0.5); 
  float amount_of_z = dot( decal_center_to_wPos, input.decal_axis_z);
  
  // Clip if outside volume.
  if(amount_of_x < 0.0 || amount_of_x > 1.0)
    clip(-1.0);
  if(amount_of_y < 0.0 || amount_of_y > 1.0)
    clip(-1.0);
  clip(1.0 - abs(amount_of_z * 2));

  // Apply a rotation only for the totem. We do it directly on the shader because it's cool.
  float rotationSpeed = 0.05;
  float angle = GlobalWorldTime * rotationSpeed;
  float mid = 0.5;
  float2 newAmounts = float2(
    cos(angle) * (amount_of_x - mid) - sin(angle) * (amount_of_y - mid) + mid,
    sin(angle) * (amount_of_x - mid) + cos(angle) * (amount_of_y - mid) + mid);
  amount_of_x = newAmounts.x;
  amount_of_y = newAmounts.y;
  
  float4 decal_color = txAlbedo.Sample(samBorderColor, float2(amount_of_x, amount_of_y));
  o_albedo.xyz = decal_color.xyz;
  o_albedo.a = decal_color.a;

  // Fade out the texture as we move away from the oficial plane XZ of the decal.
  // This is specially useful for declas that hare set in steep changes of geometry
  // like blood splatters in walls, it gives a fading effect to the decal that makes
  // it look like dripping blood.
  float vertical_factor = 1 - abs( amount_of_z * 2 );
  o_albedo.a *= vertical_factor;

  // From http://martindevans.me/game-development/2015/02/27/Drawing-Stuff-On-Other-Stuff-With-Deferred-Screenspace-Decals/.
  float3 ddxWp = ddx(wPos);
  float3 ddyWp = ddy(wPos);
  float3 pixelNormal = normalize(cross(ddyWp, ddxWp));
  float3 pixelBinormal = normalize(ddxWp);
  float3 pixelTangent = normalize(ddyWp);

  //Create a matrix transforming from tangent space to view space
  float3x3 TBN;
  TBN[0] = pixelTangent;
  TBN[1] = pixelBinormal;
  TBN[2] = pixelNormal;

  float4 decal_normal = txNormal.Sample(samBorderColor, float2(amount_of_x, amount_of_y));
  decal_normal.xyz = decal_normal.xyz * 2.0 - 1.0;
  // Transform normal from tangent space into view space
  decal_normal.xyz = mul(decal_normal.xyz, TBN);
  o_normal = encodeNormal(decal_normal.xyz, decal_normal.a * input.opacity);

  float3 emissive_color = decal_color.xyz * decal_color.a;
  float minBlinkTotem = 0.0;
  float maxBlinkTotem = 2.0;
  float blinkFrequency = 1.5;
  float totemBlink = lerp(minBlinkTotem, maxBlinkTotem, (sin(GlobalWorldTime * blinkFrequency) + 1.0) * 0.5);
  o_self_illumination = float4(emissive_color * totemBlink, 1.0);

  // Change to true 'see' the boxes
  if( false ) {
    o_albedo.a += 0.3;
   
    if( (input.uv.x < 0.01 || input.uv.x > 0.99 ) || (input.uv.y < 0.01 || input.uv.y > 0.99 ) )
      o_albedo.a = 1.0;
  }
}