//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "pbr.fx"
#include "noise.fx"

void PS_HEALTH( 
  VS_OUTPUT input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float1 o_depth  : SV_Target2,
  out float4 o_self_illumination  : SV_Target3,
  out float1 o_acc_depth  : SV_Target4
)
{
  input.Uv = input.Uv + float2(GlobalWorldTime * 0.05, GlobalWorldTime * 0.05);

  // Albedo.
  float result = warpingExposed(input.WorldPos, GlobalWorldTime * 3, 0.85, 0.8, 2.0, 0.5, 6);
  float3 color = ObjColor/result;
  o_albedo.rgb = color;
  o_albedo.a = 1.0; // Metallic or not.

  // Normal mapping
  float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
  N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1
  float3x3 TBN = computeTBN( input.N, input.T ); 
  float3 N = mul( N_tangent_space.xyz, TBN );   // Normal from NormalMap

  // Save roughness in the alpha coord of the N render target.
  o_normal = encodeNormal(N, 0.0);

  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  float  linear_depth = dot( cam2obj, CameraFront ) / CameraZFar;
  o_depth = linear_depth;
  o_acc_depth = linear_depth;

  // Make the emissive increase and decrease in frequency.
  float freq = clamp((sin(GlobalWorldTime * 2) + 1.0)/2.0 + 0.25, 0.0, 0.80);
  float3 emissive_color = color * scalar_emissive * freq;
  o_self_illumination = float4(emissive_color, 1.0);
}