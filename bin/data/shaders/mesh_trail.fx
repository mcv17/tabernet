//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Pos : SV_POSITION;
  float2 Uv : TEXCOORD0;
  float4 Color : COLOR0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

VS_OUTPUT VS(
  float3 Pos : POSITION,
  float2 Uv : TEXCOORD,
  float4 Color : COLOR,
  float Thickness : THICKNESS,
  float3 Motion : MOTION
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;

  float3 newPos = float3(0,0,0);
  float3 orient = normalize(cross(CameraPosition - Pos, Motion));
  newPos.x -= Thickness;
  // orient pair according to motion and position relative to camera
  newPos = newPos.x * orient + newPos.y * Motion;
  newPos += Pos;

  output.Pos = mul(float4(newPos, 1), ViewProjection);
  output.Uv = Uv;
  output.Color = Color;
  return output;
}

VS_OUTPUT VS_NO_BILLBOARD(
  float3 Pos : POSITION,
  float2 Uv : TEXCOORD,
  float4 Color : COLOR,
  float Thickness : THICKNESS,
  float3 Motion : MOTION
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;

  float3 newPos;
  if (length(Motion) > 0) {
    newPos = Motion * Thickness;
    newPos += Pos;
  }
  else {
    // vertex is non-existent so result should be NaN
    float zero = 0.0;
    newPos = float3(zero/zero, zero/zero, zero/zero); 
  }

  output.Pos = mul(float4(newPos, 1), ViewProjection);
  output.Uv = Uv;
  output.Color = Color;
  return output;
}

VS_OUTPUT VS_TEST_GABI(
  float3 Pos : POSITION,
  float2 Uv : TEXCOORD
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;

  output.Pos = mul(float4(Pos, 1), World);
  output.Pos = mul(output.Pos, ViewProjection);
  output.Uv = Uv;
  return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
  float4 texture_color = txAlbedo.Sample(samLinear, input.Uv);
  return texture_color * input.Color;
}

// Weird test, probably the best would be to simply move a mesh very fast changing its position.
float4 PS_GABI(VS_OUTPUT input) : SV_Target
{
  float2 Uv = input.Uv;

  float distanceTrail = 4.0; // 2 metros.
  float lengthtrail = 0.50; // 0.50 metros. Height is the one of the mesh scale. The length is the one of the bullet.
  float ratioTrailToMesh = lengthtrail/distanceTrail;
  float currentPosBullet = 0.0;GlobalWorldTime%distanceTrail; // En metros.

  float startingPos = currentPosBullet;
  float finalPos = startingPos + lengthtrail;
  startingPos = startingPos/distanceTrail; // Get it from 0 to 1.0;
  finalPos = finalPos/distanceTrail; // Get it from 0 to 1.0;

  // Don't render outside the trail. Should be a clip but Shader Model 5 doesn't allow me don't kn wwhy.
  // This is just testing though.
  if((Uv.x < startingPos) || (Uv.x > finalPos)){
    Uv.x = 0.0;
  }
  else{ // Adapt uvs.
    Uv.x = clamp((Uv.x - startingPos)/(ratioTrailToMesh), 0.0, 1.0);
  } 
  
  float4 texture_color = txAlbedo.Sample(samBorderColor, Uv);
  return texture_color;
}