#include "common.fx"
#include "pbr.fx"
#include "noise.fx"
#include "gbuffer.inc"

struct VS_OUTPUT_SCREEN
{
  float4 Pos   : SV_POSITION;
  float2 UV    : TEXCOORD0;
};

VS_OUTPUT_SCREEN VS(
	float3 iPos   : POSITION,     // 0..1, 0..1, 0 en la z
	float4 iColor : COLOR0
)
{
  VS_OUTPUT_SCREEN output = (VS_OUTPUT_SCREEN)0;
  output.Pos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 0.5, 1);
  output.UV  = iPos.xy;
  return output;
}

float4 getNormalDepthCombinedForPixel(in float2 uv){
    float3 normal;
    float depth;
    getDepthAndNormalGBuffer(uv, normal, depth);
    return float4(normal, depth);
}

float4 PS_Perception_Colorized(VS_OUTPUT_SCREEN input) : SV_TARGET
{
	float4 inputColor = txAlbedo.Sample( samLinear, input.UV );
	
	/* We can use a look up table. Right now some weird aliasing appears. */
	float3 lutColor = applyLUT(inputColor);

	/* Or this shitty method below that colors everything to the same. */

	// Convert to grayscale.
	float average = (inputColor.r + inputColor.g + inputColor.b)/3.0;
	inputColor.r = average;
	inputColor.g = average;
	inputColor.b = average;
	
	// Colorize.
	inputColor = inputColor * PerceptionTint;
	return inputColor;
}

// Scenario outline applied on perception mode.
float4 PS_Perception_Scenario_Outline( 
  float4 iPosition   : SV_POSITION,
  float2 iUV         : TEXCOORD0
  ) : SV_Target
{
  // Outline parameters.
  int edgeWidth = PerceptionScenarioOutlineEdgeWidth; // How thick the edge will be.
  const float4 edgeColor = float4(PerceptionScenarioOutlineEdgeColor, 1.0);
  float edgeThreshold = PerceptionScenarioOutlineEdgeThreshold; // The bigger, the harder lines are detected.
  const float noiseGrainyController = PerceptionScenarioOutlineNoiseGrainyness;
  const float outlineNoiseExtraWidth = PerceptionScenarioOutlineNoiseExtraWidth;

  // Outline offsets to check. All 8 directions from the given pixel.
  const float2 offsets[8] = {
    float2(-1, -1),
    float2(-1, 0),
    float2(-1, 1),
    float2(0, -1),
    float2(0, 1),
    float2(1, -1),
    float2(1, 0),
    float2(1, 1)
  };
  
  // Fetch the normal color and the normal and depth of the textue.
  int3 ss_load_coords = uint3(iPosition.xy, 0);
  float4 base_color = txAlbedo.Load(ss_load_coords); // Get image color.
  float4 pixelNormalDepthCombined = getNormalDepthCombinedForPixel(iPosition.xy); // Get depth and normal values.

  const float3 worldPos = getWorldCoords(iPosition.xy, pixelNormalDepthCombined.w);
  const float distance = length(PerceptionScenarioOutlinePositionPlayer - worldPos);
  if(pixelNormalDepthCombined.w == 1.0 || distance > PerceptionScenarioOutlineWaveRadius)
    return base_color;

  // Use the camera front and the normal of the pixel.
  // The bigger the angle the bigger the dot product becomes.
  float dotAngle = dot(pixelNormalDepthCombined.xyz, CameraFront);
  const float thresholdAngle = 0.90; // When to start applying normal threshold.
  float thresholdMod = saturate((dotAngle - thresholdAngle) / (1 - thresholdAngle));
  thresholdMod = thresholdMod + 1; // Make it go from 1 onwards.
  edgeThreshold = edgeThreshold * thresholdMod;

  // Get extra noise.
  float outlineNoiseWidth = (snoise(float2(worldPos.x + worldPos.z, worldPos.y + worldPos.z) * noiseGrainyController + GlobalWorldTime) + 1) * outlineNoiseExtraWidth;
  edgeWidth = edgeWidth + outlineNoiseWidth;

  // Now move through all pixels.
  float4 sampledValue = float4(0,0,0,0);
  for(int i = 1; i <= edgeWidth; i++){
    for(int j = 0; j < 8; j++) {
        int2 pixelUVs = iPosition.xy + offsets[j] * i;

		if(pixelUVs.y < 0.0 || pixelUVs.y >= ScreenResolutionVertical){
			sampledValue += pixelNormalDepthCombined;
			continue;
		}

		if(pixelUVs.x < 0.0 || pixelUVs.x >= ScreenResolutionHorizontal){
			sampledValue += pixelNormalDepthCombined;
			continue;
		}
		
        sampledValue += getNormalDepthCombinedForPixel(pixelUVs);
    }
  }

  sampledValue /= (8 * edgeWidth);

  if(step(edgeThreshold, length(pixelNormalDepthCombined - sampledValue)))
    return edgeColor;

  return base_color;
}

void PS_Perception_Objects_Outline( 
  float4 iPosition   : SV_POSITION,
  float2 iUV         : TEXCOORD0,
  out float4 o_albedo : SV_Target0,
  out float1 o_drawn_to  : SV_Target1
  )
{
	// Outline border.
	const int outlineWidth = PerceptionScenarioOutlineEdgeWidth; // How big the outline will be by default.
	const float smoothingNoise = PerceptionScenarioOutlineNoiseGrainyness; // Noise applied to border.
	const float borderNoiseWidth = PerceptionScenarioOutlineNoiseExtraWidth; // Noise border width, how much noise can be added to the base size.

	// Fetch the texture information.
	int3 ss_load_coords = uint3(iPosition.xy, 0);
	const int3 offsets[8] = {
		int3(1,0,0),
		int3(0,1,0),
		int3(-1,0,0),
		int3(0,-1,0),
		int3(1,1,0),
		int3(-1,1,0),
		int3(1,-1,0),
		int3(-1,-1,0)
	};

	// We start calculating the noise of the wave which will vary based on the category we are using.
	// For now I'm simply using the pixel. This makes the effect vary with the screen but it's not that noticeable
	// and it gives a good enough result.
	float outlineNoiseWidth = (snoise(float2(iPosition.x, iPosition.y) * smoothingNoise + GlobalWorldTime) + 1) * borderNoiseWidth;

	// First we have to scan all pixels in the size of our outline. If we find more than a result. We will fetch the closer
	// and paint it if it conincides with the outline category. Otherwise we ignore it. We grab the closer value because we only
	// want to paint the outline of the object that is in front of the others.
	
	// Our starting depth is 1.0 ("the infinite"), and the mask is 0.0. We start asumming nothing is drawn.
	// we will check all pixels around and fetch the value of the closer perceptive object if there is one.
	float2 depthAndPerceptionVal = float2(1.0, 0.0);
	float pixelPerceptionVal = txPerceptionMask.Load(ss_load_coords).y; // We also get the perception value in the pixel before starting.
	for (int i = 1; i <= outlineWidth + outlineNoiseWidth; i++) {
		for (int j = 0; j < 8; j++) {
			ss_load_coords = uint3(iPosition.xy, 0) + offsets[j] * i;
			float2 new_depth_perception_mask = txPerceptionMask.Load(ss_load_coords);
			
			// We check if the value is closer than ours. If it is, it means that there is an object which has perception
			// and is closer and whose outline occupies this pixel.
			if(new_depth_perception_mask.x < depthAndPerceptionVal.x){
				// It's closer to ours. We copy the value.
				depthAndPerceptionVal = new_depth_perception_mask;
			}
		}
	}

	// If this pixel perception value is written and the category is the same as the one of
	// the closer object we clip them. Otherwise we continue. We only want to render the outlines
	// of objects (their borders). If the pixels are of the same category, we shouldn't draw the outline.
	// But if the pixel is of an object of different category, we can write it.
	// We could extend this in the future by saving a third value in the perception mask with an id for each object.
	// This would give us auras for each single enemy. For now i'm leaving it like this as i already like the effect.
	if(pixelPerceptionVal > 0.0 && pixelPerceptionVal == depthAndPerceptionVal.y)
		clip(-1);

	// Before seeing if we draw it, we check the value belongs to an object of our category.
	// If not, we clip.
	if(depthAndPerceptionVal.y != PerceptionScenarioOutlineCategory)
		clip(-1);

	// Now, this is part of our integration with the wave shader.
	// We see if we are inside the radius wave. If outside, we return, no outline is generated.
	const float3 worldPos = getWorldCoords(iPosition.xy, depthAndPerceptionVal.x);
	const float distance = length(PerceptionScenarioOutlinePositionPlayer - worldPos);
	if(distance > PerceptionScenarioOutlineWaveRadius)
		clip(-1);
	
	// If everything went well, return the color of the outline. We are finished :).
	o_albedo = float4(PerceptionScenarioOutlineEdgeColor, 1.0);
	o_drawn_to = depthAndPerceptionVal.x; // also add the outline depth into the depth accumulated texture.
	// this is used by the wave shader to see what to illuminate.
}

float4 PS(VS_OUTPUT_SCREEN input) : SV_TARGET
{
	float4 inputColor = txAlbedo.Sample( samLinear, input.UV );
	
	// Apply fade factor if fading.
	const float fadeFactor = 1.0f - PerceptionFadeWeight;
	if(PerceptionFading) return inputColor * fadeFactor;
	
	// Sonar/wave.

	// Compute the distance to the pixel given the PerceptionPositionPlayer (start of the wave/sonar).
	const int3 ss_load_coords = uint3(input.Pos.xy, 0);
	const float depth = txGLinearDepth.Load(ss_load_coords).x;

	if(depth == 1.0) return inputColor * fadeFactor; 

	const float3 worldPos = getWorldCoords(input.Pos.xy, depth);
	const float distance = length(PerceptionPositionPlayer - worldPos);
	
	// Noise added above to the sonar wave. Gives a more misty texture to the scene.
	// also used for the radius.
	// If you want to see how the noise maps in the world write: return mistFactor.
	const float mistScale = 1.75; // The bigger the scale the more little clouds appear.
	const float mistSpeed = 0.3; // The speed of the mist.
	const float mistFactor = sonarMistNoise(float2(worldPos.x * mistScale + worldPos.y * mistScale +  mistSpeed * GlobalWorldTime,
	worldPos.y * mistScale + worldPos.z * mistScale + mistSpeed * GlobalWorldTime));

	// We use this noise for the radius off the corruption wave,aw
	// gives a more organic look than a perfect circle.wa
	// Value goes from 0 to -1.
	float radius = PerceptionWaveRadius + mistFactor * PerceptionWaveNoiseAmplitude;

	// If inside the circle.
	if(distance <= radius){
		// If it's inside the influence area of the sonar.
		const float minOffset = radius - PerceptionRadiusOffset;
		if(minOffset <= distance){
			// Use the normalized distance from the radius to the radius - PerceptionRadiusOffset.
			// Use it as uv for the gradient texture that has the color and the uv.
			const float uvGradient = (distance - minOffset)/PerceptionRadiusOffset;
			
			// Get the wave color.
			float4 colorGradient = txGradient2.Sample(samLinear, float2(uvGradient, 0.5));
			colorGradient.xyz = colorGradient.xyz;

			// Get the transparency factor. We use a product, gives a more smoother look.
			const float intensityFadeFactor = 0.95; // The higher, the less the fade is visible.
			const float transparent = clamp(colorGradient.a * intensityFadeFactor, 0.0f, 1.0f);
			
			// Return the lerp between the color of the scene and the color of the wave based on the transparency.
			// The fade factor is for the fade effect when transition in or out perception.
			return float4(lerp(inputColor.rgb, colorGradient.rgb, transparent), 1) * fadeFactor;
		}
		// Else return normal color.
		return inputColor * fadeFactor;
	}
	else{
		float intensityFactor = PerceptionNonPercievedAreaColorIntensity;
		// Don't directly fade to non color or the contrast looks terrible.
		// We give a small space lighted with a little linear fade.
		// We could do this fade inside the top part of the wave but the code would be
		// uglier.
		const float PerceptionRadiusOffsetFront = PerceptionRadiusOffset * 0.15;
		const float maxOffset = radius + PerceptionRadiusOffsetFront;
		if(distance <= maxOffset){
			intensityFactor  = clamp((maxOffset - distance)/(PerceptionRadiusOffsetFront),
			PerceptionNonPercievedAreaColorIntensity, 1.0f);
		}
	
		// Paint black.
		return inputColor * intensityFactor * fadeFactor;
	}
}

void PS_PERCEPTION_XRAY(VS_OUTPUT input,
  out float4 o_albedo : SV_Target0,
  out float2 o_perception_mask : SV_Target1
)
{
	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
    float  linear_depth = dot( cam2obj, CameraFront ) / CameraZFar;

	o_albedo = float4(1,1,1,1);
	// Store the depth and the mask value of the perceptive object.
	o_perception_mask = float2(linear_depth, PerceptionMaskValue);
}

// Cool dithering pattern.

/*

	// Outline border.
	const int outlineWidth = PerceptionScenarioOutlineEdgeWidth; // How big the outline will be by default.
	const float smoothingNoise = PerceptionScenarioOutlineNoiseGrainyness; // Noise applied to border.
	const float borderNoiseWidth = PerceptionScenarioOutlineNoiseExtraWidth; // Noise border width, how much noise can be added to the base size.
	// Fetch the albedo.
	int3 ss_load_coords = uint3(iPosition.xy, 0);
	float4 albedo_color = txAlbedo.Load(ss_load_coords);
	const int3 offsets[8] = {
		int3(1,0,0),
		int3(0,1,0),
		int3(-1,0,0),
		int3(0,-1,0),
		int3(1,1,0),
		int3(-1,1,0),
		int3(1,-1,0),
		int3(-1,-1,0)
	};

	// Don't paint things outside the circle.
	float linear_depth = txGLinearDepth.Sample(samLinear, iUV).x;
	const float3 worldPos = getWorldCoords(iPosition.xy, linear_depth);
	const float distance = length(PerceptionScenarioOutlinePositionPlayer - worldPos);
	if((linear_depth != 1.0 && (distance > PerceptionScenarioOutlineWaveRadius)))
		return albedo_color;

	// We will simply use the albedo color for the outlines of characters without
	// considering depth. This is faster than fetching the depth and using it.
	// Also gives nices outer outlines which is what we are interested in.
	if(albedo_color.a == 1.0) {
		float outlineNoiseWidth = (snoise(float2(iPosition.x, iPosition.y) * smoothingNoise + GlobalWorldTime) + 1) * borderNoiseWidth;

		for (int i = 1; i <= outlineWidth + outlineNoiseWidth; i++) {
			for (int j = 0; j < 8; j++) {
				ss_load_coords = uint3(iPosition.xy, 0) + offsets[j] * i;
				albedo_color = txAlbedo.Load(ss_load_coords);
				if (albedo_color.a == 0)
					return float4(PerceptionScenarioOutlineEdgeColor, 1.0);
			}
		}
	}
	
	return albedo_color;
*/

/*
float4 PS_Perception_Objects_Outline( 
  float4 iPosition   : SV_POSITION,
  float2 iUV         : TEXCOORD0
  ) : SV_Target
{
	// Outline border.
	const int outlineWidth = PerceptionScenarioOutlineEdgeWidth; // How big the outline will be by default.
	const float smoothingNoise = PerceptionScenarioOutlineNoiseGrainyness; // Noise applied to border.
	const float borderNoiseWidth = PerceptionScenarioOutlineNoiseExtraWidth; // Noise border width, how much noise can be added to the base size.
	
	// Fetch the albedo.
	int3 ss_load_coords = uint3(iPosition.xy, 0);
	float4 albedo_color = txAlbedo.Load(ss_load_coords);
	const int3 offsets[8] = {
		int3(1,0,0),
		int3(0,1,0),
		int3(-1,0,0),
		int3(0,-1,0),
		int3(1,1,0),
		int3(-1,1,0),
		int3(1,-1,0),
		int3(-1,-1,0)
	};
	
	// We will simply use the albedo color for the outlines of characters without
	// considering depth. This is faster than fetching the depth and using it.
	// Also gives nices outer outlines which is what we are interested in.
	if(albedo_color.a == 0.0) {
		float outlineNoiseWidth = (snoise(float2(iPosition.x, iPosition.y) * smoothingNoise + GlobalWorldTime) + 1) * borderNoiseWidth;

		for (int i = 1; i <= outlineWidth + outlineNoiseWidth; i++) {
			for (int j = 0; j < 8; j++) {
				ss_load_coords = uint3(iPosition.xy, 0) + offsets[j] * i;
				albedo_color = txAlbedo.Load(ss_load_coords);
				if (albedo_color.a > 0.0)
					return float4(PerceptionScenarioOutlineEdgeColor, 1.0);
			}
		}
	}
	
	return albedo_color;
}
*/