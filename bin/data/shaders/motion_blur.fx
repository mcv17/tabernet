#include "screen_quad.fx"

// Motion Blur PS.
// The blurring technique is taken from:
// http://john-chapman-graphics.blogspot.com/2013/01/what-is-motion-blur-motion-pictures-are.html
// Also interesting:
// https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch27.html

// This motion blur works perfect for static objects and dynamic objects but the accuracy of the effect for the
// later could be much improved by creating a velocity texture for them which uses the first two passes of this shader
// to calculate the velocity of each pixel.
float4 PS_MOTION_BLUR(
	VS_FULL_OUTPUT input
) : SV_Target{
	
	/* Get the world position of the pixel. */

	// We sample the depth texture and get the world position.
	float depth = txGLinearDepth.Load(uint3(input.Pos.xy, 0)).x;
	float3 worldPos = getWorldCoords(input.Pos.xy, depth);

	/* Calculate the velocity vector by using the last frame view matrix. */

	// Current viewport position. We convert the UVs going from 0 to 1 to go from -1 to 1.
	//float4 currentPos = float4(input.UV.x * 2 - 1, (1 - input.UV.y) * 2 - 1, depth, 1);

	// Use this frame's position and last frame's to compute the pixel velocity.
	//float2 velocity = 0.15f * (currentPos.xy - previousPos.xy) / (MotionBlurSteps * 2.f);

	// The text coordinate for this fragment.
	float2 texCoord = input.UV;

	// Use the world position, and transform by the previous view-projection matrix.
	float4 previousPos = mul(float4(worldPos, 1.0), MotionBlurCameraLastFrameViewProjectionMatrix);
	// Convert to nonhomogeneous points [-1,1] by dividing by w.
	previousPos /= previousPos.w;
	// Convert to UV coordinates.
	previousPos.xy = (previousPos.xy * 0.5) + 0.5;
	previousPos.y = 1 - previousPos.y;

	// Calculate the velocity vector.
	float2 scaleFactor = MotionBlurIntensity * (MotionBlurCurrentFPS/MotionBlurTargetFPS); 
	float2 velocityVector = (previousPos.xy - texCoord) * scaleFactor;

	/* Apply the motion blur with the velocity vector. */
	
	// To do so we will use the velocity vector to sample across the color of the image.
	// The image that is going to be blurred is stored in the TxAlbedo texture.
	float4 colorToBlur = 0;

	// Number of samplers in the motion blur shader. For each sample, add the velocity vector.
	for (int i = 0; i < MotionBlurSteps; ++i)
	{
		float2 coordToSample = texCoord + velocityVector * (i/float(MotionBlurSteps));
		// Sample the color buffer along the velocity vector. And add the current color to our color sum.
		colorToBlur += txAlbedo.Sample(samClampPoint, coordToSample);
	}

	// Average all of the samples to get the final blur color.
	return colorToBlur / (float)MotionBlurSteps;
}