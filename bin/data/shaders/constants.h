#include "platform.h"

// -------------- Constant buffers --------------

#define CTE_BUFFER_SLOT_CAMERAS      0 // Camera constants.
#define CTE_BUFFER_SLOT_OBJECT       1 // Object constants.
#define CTE_BUFFER_SLOT_SHARED       2 // Shared constants. (World time).
#define CTE_BUFFER_SLOT_DEBUG_LINE   3 // Constants for debugging.
#define CTE_BUFFER_SLOT_SKIN_BONES   4 // Skinning constants.
#define CTE_BUFFER_SLOT_LIGHT        5 // Light constants.
#define CTE_BUFFER_SLOT_MOTION_BLUR  6 // Motion blur constants.
#define CTE_BUFFER_SLOT_BLUR         6 // Blur constants.
#define CTE_BUFFER_SLOT_PERCEPTION   7 // Perception constants.
#define CTE_BUFFER_SLOT_FOG          7 // Fog constants.
#define CTE_BUFFER_SLOT_VIGNETTING   7 // Vignetting constants.
#define CTE_BUFFER_SLOT_CORRUPTION	 7 // Corruption constants.
#define CTE_BUFFER_SLOT_BURNING_FADE 7 // Corruption constants.
#define CTE_BUFFER_SLOT_VANISH       7 // Vanish constants.
#define CTE_BUFFER_SLOT_BLAST        7 // Vanish constants.
#define CTE_BUFFER_SLOT_FOCUS        8 // Depth of field constants.
#define CTE_BUFFER_SLOT_BLOOM        9 // Bloom constants.
//#define CTE_BUFFER_SLOT_DIRECTXTK  9 //DIRECTXTK (UI) IS USING THIS SLOT, TAKE CARE
#define CTE_BUFFER_SLOT_FXAA         10 // FXAA constants.
#define CTE_BUFFER_SLOT_LIGHT_SCATTERING 10 // Light scattering as postfx constants.
#define CTE_BUFFER_SLOT_MATERIAL     10 // Material constants.
#define CTE_BUFFER_SLOT_UI           11 // UI constants.
#define CTE_BUFFER_SLOT_PARTICLES    11 // Particles.
#define CTE_BUFFER_SLOT_MATERIAL_EXTRA 11 // Material constants.

// -------------- Texture slots --------------
// TS = TEXTURE_SLOT

#define TS_ALBEDO            0 // Albedo texture of an object.
#define TS_NORMAL            1 // Normal texture of an object.
#define TS_METALLIC          2 // Metallic texture of an object.
#define TS_ROUGHNESS         3 // Roughness texture of an object.
#define TS_EMISSIVE          4 // Emissive texture of an object.
#define TS_AO                5 // AO map for an object.
#define TS_HEIGHT            6 // Height texture of an object. (Used by parallax effect).

#define TS_PROJECTOR         4 // Texture used as projection for a light.
#define TS_LIGHT_SHADOW_MAP  5 // Stores shadows for a light.
#define TS_ENVIRONMENT_MAP   16 // Used by the HDR skybox.
#define TS_IRRADIANCE_MAP    17 // Used by the HDR skybox.
#define TS_ENVIRONMENT_MAP_2 6
#define TS_IRRADIANCE_MAP_2 7

// Deferred rendering TS --------------
// These textures are used by the deferred rendering.
#define TS_DEFERRED_ALBEDOS		      8
#define TS_DEFERRED_NORMALS		      9
#define TS_DEFERRED_LINEAR_DEPTH      10
#define TS_DEFERRED_SELF_ILLUMINATION 11
#define TS_DEFERRED_FULL_DEPTH	      12
#define TS_DEFERRED_PERCEPTION_MASK   13
#define TS_DEFERRED_ACC_LIGHTS        13
#define TS_DEFERRED_AO                14

// PostFX TS --------------
#define TS_NOISE_MAP			 15 // Used for noise computations.
#define TS_GRADIENT				 15 // Gradient texture used as gradient in the corruption wave of perception and stylistic fog.
#define TS_GRADIENT_2				 16 // Gradient texture used as gradient in the corruption wave of perception and stylistic fog.
#define TS_LUT_COLOR_GRADING	 17 // Lugt color grading texture.
#define TS_TEXTURE_CURRENT_SCREEN 0 // Used by burning shader. Current rt.
#define TS_TEXTURE_SCREEN_TO_FADE_TO 1 // Used by burning shader. Rt to fade to.

// ---------------------------------------------
// Mixing material extra texture slots
#define TS_MIX_BLEND_WEIGHTS          18

#define TS_FIRST_SLOT_MATERIAL_0      TS_ALBEDO
#define TS_FIRST_SLOT_MATERIAL_1      TS_ALBEDO1
#define TS_FIRST_SLOT_MATERIAL_2      TS_ALBEDO2

#define TS_ALBEDO1 20
#define TS_NORMAL1 21
// #define TS_METALLIC1 22
// #define TS_ROUGHNESS1 23
#define TS_ALBEDO2 24
#define TS_NORMAL2 25
// #define TS_METALLIC2 26
// #define TS_ROUGHNESS2 27

// -------------------------------------------------
// Render Outputs. Must be in sync with module_render.cpp
#define RO_COMPLETE           0
#define RO_ALBEDO             1
#define RO_NORMAL             2
#define RO_NORMAL_VIEW_SPACE  3
#define RO_ROUGHNESS          4
#define RO_METALLIC           5
#define RO_WORLD_POS          6
#define RO_LINEAR_DEPTH       7
#define RO_AO                 8

// -------------------------------------------------
#define MAX_SUPPORTED_BONES        128

// Cascade shadow maps constants.
#define NUM_CASCADES_SHADOW_MAP 3
#define NUM_FRUSTUM_CORNERS NUM_CASCADES_SHADOW_MAP * 8

#define PI 3.14159265359f

SHADER_CTE_BUFFER(TCtesCamera, CTE_BUFFER_SLOT_CAMERAS)
{
	matrix Projection;
	matrix View;
	matrix ViewProjection;
	matrix InverseViewProjection;
	matrix CameraScreenToWorld;
	matrix CameraProjWithOffset;
	float3 CameraFront;
	float  CameraZFar;
	float3 CameraPosition;
	float  CameraZNear;
	float  CameraTanHalfFov;
	float  CameraAspectRatio;
	float2 CameraInvResolution;
	float3 CameraLeft;
	float ScreenResolutionVertical;
	float3 CameraUp;
	float ScreenResolutionHorizontal;
};

SHADER_CTE_BUFFER(TCtesObject, CTE_BUFFER_SLOT_OBJECT)
{
	matrix World;
	float4 ObjColor;
};

SHADER_CTE_BUFFER(TCtesShared, CTE_BUFFER_SLOT_SHARED)
{
	float  GlobalWorldTime;
	int    GlobalRenderOutput;
	float  GlobalAmbientBoost;
	float  GlobalExposureAdjustment;

	float  GlobalReflectionIntensity;
	float  GlobalAmbientLightIntensity;
	float  GlobalFXAmount;
	float  GlobalFXVal1;

	float  GlobalFXVal2;
	float  GlobalFXVal3;
	float  GlobalLUTAmount;
	float  GlobalSkyBoxLerp;
};

SHADER_CTE_BUFFER(TCtesDebugLine, CTE_BUFFER_SLOT_DEBUG_LINE)
{
	// The float4 for the positions is to enforce alignment
	float4 DebugSrc;
	float4 DebugDst;
	float4 DebugColor;
};

SHADER_CTE_BUFFER(TCteSkinBones, CTE_BUFFER_SLOT_SKIN_BONES)
{
	matrix Bones[MAX_SUPPORTED_BONES];
};

SHADER_CTE_BUFFER(TCtesLight, CTE_BUFFER_SLOT_LIGHT) {
	float4 LightColor;
	float3 LightPosition;
	float  LightIntensity;

	matrix LightViewProjOffset;
	float4 CascadeEndClipSpace[NUM_CASCADES_SHADOW_MAP]; // Z value of each camera cascade in light view space. Its a float4 because HLSL can't pack an array ok.
	float4 CascadeOffsets[NUM_CASCADES_SHADOW_MAP]; // Used for finding which cascade to sample for. Better use of the boundaries than a simple split.
	float4 CascadeScales[NUM_CASCADES_SHADOW_MAP];

	bool   LightHasShadows;
	float  LightShadowStep;
	float  LightShadowInverseResolution;
	float  LightShadowStepDivResolution;
	float  LightRadius;
	float3 LightFront;      // For the sun
};

SHADER_CTE_BUFFER(TCtesVignetting, CTE_BUFFER_SLOT_VIGNETTING){
	float3 VignettingDummies;
	float  VignettingFallOff;
};

SHADER_CTE_BUFFER(TCtesCorruption, CTE_BUFFER_SLOT_CORRUPTION) {
	float3 CorruptionColor;
	float  CorruptionFallOff;
};

SHADER_CTE_BUFFER(TCtesVanish, CTE_BUFFER_SLOT_VANISH) {
	float3 VanishInnerBorderColor;
	float  VanishCurrentTime;
	float3 VanishOutterBorderColor;
	float  VanishInnerBorderSize;
	float  VanishNoiseScale;
	float  VanishAddedIntensity;
	float  VanishOutterBorderSize;
	float  VanishTotalTime;
};

SHADER_CTE_BUFFER(TCtesBlast, CTE_BUFFER_SLOT_BLAST) {
	float  BlastCurrentTime;
	float  BlastIntensity;
	float  BlastThickness;
	float  BlastSpeed;

	float  BlastScale;
	float  BlastWhiteIntensity;
	float  BlastDispersion;
	float  BlastDummy;
};

SHADER_CTE_BUFFER(TCtesFog, CTE_BUFFER_SLOT_FOG) {
	float3 FogColor;
	float  FogStart;
	float  FogEnd;
	float  FogIntensity;
	float  FogDensity;
	float  FogLerp;
};

SHADER_CTE_BUFFER(TCtesBurningFade, CTE_BUFFER_SLOT_BURNING_FADE) {
	float3 BurningFadeColorEdgeBurning;
	float BurningFadeColorIntensity;
	float BurningFadeCurrentTime;
	float BurningFadeTimeToFinish;
	float BurningFadeEdgeSize;
	float BurningFadeShowBackgroundThreshold;
};

// Used for the postfx effect.
SHADER_CTE_BUFFER(TCtsPerception, CTE_BUFFER_SLOT_PERCEPTION) {
	float4 PerceptionTint;
	float4 PerceptionFadeColor;
	float3 PerceptionPositionPlayer;
	float  PerceptionFadeWeight;

	float PerceptionNonPercievedAreaColorIntensity;
	float PerceptionWaveRadius;
	float PerceptionWaveNoiseAmplitude;
	float PerceptionRadiusOffset;

	float3 PerceptionPadding;
	bool PerceptionFading;
};

// Used for the postfx effect outlining phase.
SHADER_CTE_BUFFER(TCtsPerceptionScenarioOutline, CTE_BUFFER_SLOT_PERCEPTION) {
	float3 PerceptionScenarioOutlinePositionPlayer;
	float PerceptionScenarioOutlineWaveRadius;

	float3 PerceptionScenarioOutlineEdgeColor;
	int PerceptionScenarioOutlineEdgeWidth;

	float PerceptionScenarioOutlineNoiseExtraWidth;
	float PerceptionScenarioOutlineNoiseGrainyness;
	float PerceptionScenarioOutlineEdgeThreshold;
	float PerceptionScenarioOutlineCategory;
};

SHADER_CTE_BUFFER(TCteMotionBlur, CTE_BUFFER_SLOT_MOTION_BLUR)
{
	matrix MotionBlurCameraLastFrameViewProjectionMatrix;
	float MotionBlurSteps;
	float MotionBlurIntensity;
	float MotionBlurCurrentFPS;
	float MotionBlurTargetFPS;
};


SHADER_CTE_BUFFER(TCtesBlur, CTE_BUFFER_SLOT_BLUR)
{
	float4 blur_w;        // weights
	float4 blur_d;        // distances for the 1st, 2nd and 3rd tap
	float2 blur_step;     // Extra modifier
	float2 blur_center; // To keep aligned x4
};

SHADER_CTE_BUFFER(TCtesFocus, CTE_BUFFER_SLOT_FOCUS)
{
	float focus_z_center_in_focus;
	float focus_z_margin_in_focus;
	float focus_transition_distance;
	float focus_modifier;
};

SHADER_CTE_BUFFER(TCtesBloom, CTE_BUFFER_SLOT_BLOOM)
{
	float4 bloom_weights;
	float  bloom_threshold_min;
	float  bloom_threshold_max;
	float  bloom_pad1;
	float  bloom_pad2;
};

SHADER_CTE_BUFFER(TCtesFXAA, CTE_BUFFER_SLOT_FXAA)
{
	float2 fxaaQualityRcpFrame;
	float  fxaaQualitySubpix = 0.75;
	float  fxaaQualityEdgeThreshold = 0.166;
	float3 fxaaPadding;
	float  fxaaQualityEdgeThresholdMin = 0.0833;
};

SHADER_CTE_BUFFER(TCtesLightScattering, CTE_BUFFER_SLOT_LIGHT_SCATTERING) {
	float2 LightScatteringSunPosInScreenSpace;
	float  LightScatteringExposure;
	float  LightScatteringDecay;
	float2 LightScatteringPadding;
	float  LightScatteringDensity;
	float  LightScatteringWeight;
};

SHADER_CTE_BUFFER(TCtesUI, CTE_BUFFER_SLOT_UI)
{
	float2 UIminUV;
	float2 UImaxUV;
	float4 UItint;
	float2 UIframeSize;
	float2 UInframes;
	float UItimeRatio;
	float3 UIpad1;
};

SHADER_CTE_BUFFER(TCtesMaterial, CTE_BUFFER_SLOT_MATERIAL) 
{
	// Base cts.
	float  scalar_roughness;
	float  scalar_metallic;
	float  scalar_emissive;
	float  scalar_ao;
};

SHADER_CTE_BUFFER(TCtesPerceptionMask, CTE_BUFFER_SLOT_MATERIAL_EXTRA)
{
	// Perception cts for material.
	float3 PerceptionMaskPadding;
	float  PerceptionMaskValue; // Used to identify the type of object in perception mode. Depeding on the value different outlines will be applied.
};


SHADER_CTE_BUFFER(TCtesMaterialParallax, CTE_BUFFER_SLOT_MATERIAL_EXTRA)
{
	// Parallax cts.
	float  parallax_height_intensity;
	float  parallax_min_layers;
	float  parallax_max_layers;
	float  material_dummy;
};

SHADER_CTE_BUFFER(TCtesMaterialMix, CTE_BUFFER_SLOT_MATERIAL_EXTRA)
{
	// Mixing cts.
	float  mix_boost_r;
	float  mix_scale_r;
	float  mix_boost_g;
	float  mix_scale_g;
	float2 mix_dummy;
	float  mix_boost_b;
	float  mix_scale_b;
	
};

SHADER_CTE_BUFFER(TCtesBillboarding, CTE_BUFFER_SLOT_MATERIAL_EXTRA)
{
	// Mixing cts.
	float2  BillboardingScale;
	float2  BillboardingPadding;
};

SHADER_CTE_BUFFER(TCtesFresnelPBR, CTE_BUFFER_SLOT_MATERIAL_EXTRA)
{
	// Mixing cts.
	float4  FresnelPBRColor;
	float  FresnelPBRExponent;
	float  FresnelPBRIntensity;
	float2 FresnelPBRDummy;
};

SHADER_CTE_BUFFER(TCtesParticles, CTE_BUFFER_SLOT_PARTICLES)
{
	float2 psystem_frame_size;
	float2 psystem_nframes;
	float4 psystem_min_colors_over_time[12];
	float4 psystem_max_colors_over_time[12];
	float4 psystem_sizes_over_time[12];
	float3 dummy;

	// Particles rotation.
	bool psystem_random_rotation;
	float psystem_rotation_offset;
  
	// Stretched billboards.
	bool psystem_stretched_billboard;
	float psystem_stretch;
	bool psystem_velocity_affects_stretch;
	
	// Particles color.
	int  psystem_random_color = false; // Whether color will be randomly choosen from at start from the ones on the track or not.
	int  psystem_random_dynamic_color = true; // Whether particles that start with a color change them with time.
									   // tracks for controlling alpha blending. (We start at 1 and go downwards based on alpha). Otherwise, colors get spawned with the alpha specified.
	// Particles size.
	int  psystem_random_size = false;  // Whether size is randomly picked for each particle at start or not.
	int  psystem_random_modified_size = true;  // Whether size changes with time or not. When in random size, we use the sizes alpha specified by the tracks as size modifier.
 
	// Particles uvs.
	int  psystem_random_uvs = false; // Whether size is randomly picked for each particle or not.
	int  psystem_random_uvs_change_with_time = false;   // Whether uvs change with time or not if they start randomly.
	int  psystem_uvs_blend = false; // If active, uvs are blended between the two given uvs.
	float psystem_uvs_speed_change = 1.0; // How much uvs distance is changed during the particle lifetime. Also used for color change control.
};

