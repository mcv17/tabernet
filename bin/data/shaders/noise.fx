#ifndef NOISE
#define NOISE

/* This file contains different noise functions that are used by different shaders to produce effects
like corruption or transitions between others. Most of these functions generate random noise or different
variants of perlin noise */

// Hash functions by Morgan McGuire @morgan3d, http://graphicscodex.com
float hash(float n) { return frac(sin(n) * 1e4); }
float hash(float2 p) { return frac(1e4 * sin(17.0 * p.x + p.y * 0.1) * (0.1 + abs(sin(p.y * 13.0 + p.x)))); }

// Random function by https://thebookofshaders.com/13/
float random (in float2 st) {
    return frac(sin(dot(st.xy, float2(12.9898,78.233))) * 43758.5453123);
}

// Noise function by Morgan McGuire @morgan3d, http://graphicscodex.com
float noise(float x) {
    float i = floor(x);
    float f = frac(x);
    float u = f * f * (3.0 - 2.0 * f);
    return lerp(hash(i), hash(i + 1.0), u);
}

// Noise function by Morgan McGuire @morgan3d, http://graphicscodex.com
float noise(float2 x) {
    float2 i = floor(x);
    float2 f = frac(x);

	// Four corners in 2D of a tile
	float a = hash(i);
    float b = hash(i + float2(1.0, 0.0));
    float c = hash(i + float2(0.0, 1.0));
    float d = hash(i + float2(1.0, 1.0));

	// Same code, with the clamps in smoothstep and common subexpressions
	// optimized away.
    float2 u = f * f * (3.0 - 2.0 * f);
	return lerp(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}

// Some useful functions used in signed noise.
float3 mod289(float3 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
float2 mod289(float2 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
float3 permute(float3 x) { return mod289(((x*34.0)+1.0)*x); }

// Description : GLSL 2D simplex noise function
//      Author : Ian McEwan, Ashima Arts
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License :
//  Copyright (C) 2011 Ashima Arts. All rights reserved.
//  Distributed under the MIT License. See LICENSE file.
//  https://github.com/ashima/webgl-noise
float snoise(float2 v) {

    // Precompute values for skewed triangular grid
    const float4 C = float4(0.211324865405187,
                        // (3.0-sqrt(3.0))/6.0
                        0.366025403784439,
                        // 0.5*(sqrt(3.0)-1.0)
                        -0.577350269189626,
                        // -1.0 + 2.0 * C.x
                        0.024390243902439);
                        // 1.0 / 41.0

    // First corner (x0)
    float2 i  = floor(v + dot(v, C.yy));
    float2 x0 = v - i + dot(i, C.xx);

    // Other two corners (x1, x2)
    float2 i1 = float2(0.0, 0.0);
    i1 = (x0.x > x0.y)? float2(1.0, 0.0):float2(0.0, 1.0);
    float2 x1 = x0.xy + C.xx - i1;
    float2 x2 = x0.xy + C.zz;

    // Do some permutations to avoid
    // truncation effects in permutation
    i = mod289(i);
    float3 p = permute(
            permute( i.y + float3(0.0, i1.y, 1.0))
                + i.x + float3(0.0, i1.x, 1.0 ));

    float3 m = max(0.5 - float3(
                        dot(x0,x0),
                        dot(x1,x1),
                        dot(x2,x2)
                        ), 0.0);

    m = m*m ;
    m = m*m ;

    // Gradients:
    //  41 pts uniformly over a line, mapped onto a diamond
    //  The ring size 17*17 = 289 is close to a multiple
    //      of 41 (41*7 = 287)

    float3 x = 2.0 * frac(p * C.www) - 1.0;
    float3 h = abs(x) - 0.5;
    float3 ox = floor(x + 0.5);
    float3 a0 = x - ox;

    // Normalise gradients implicitly by scaling m
    // Approximation of: m *= inversesqrt(a0*a0 + h*h);
    m *= 1.79284291400159 - 0.85373472095314 * (a0*a0+h*h);

    // Compute final noise value at P
    float3 g = float3(0.0, 0.0, 0.0);
    g.x  = a0.x  * x0.x  + h.x  * x0.y;
    g.yz = a0.yz * float2(x1.x,x2.x) + h.yz * float2(x1.y,x2.y);
    return 130.0 * dot(m, g);
}

// Noise from texture for one dimension.
float noiseFromTexture( float x ){return txNoise.Sample(samLinear, float2(x,0)).x;}

// Noise from texture for two dimensions.
float noiseFromTexture( float2 uv ){return txNoise.Sample(samLinear, uv).x;}

// fracal Brownian Motion or fracal noise.
// * Amplitude: Starting amplitude of the wave.
// * Frequency: Starting frequency of the wave.
// * Octaves: Iterations of the noise function per point.
// * Lacunarity: Increment/Decrement in the frequencies for each step.
// * Gain: Increment/Decrement in the amplitude for each step.
float FBM(float x, float amplitude, float frequency, float lacunarity, float gain, int octaves, float phase = 0.0){
    float result = 0.0;
    for(int i = 0; i < octaves; ++i){
        result += amplitude * noise(frequency * x + phase);
        frequency *= lacunarity;
        amplitude *= gain;
    }
    return result;
}

// fracal Brownian Motion for two dimensional input.
// same parameters as one dimensional function.
// Incrementing frequency increases the range of the final image, the bigger the more distance covered.
// Modifying lacunarity affects the 'grainyness' of the final image, softening the result.
// Modifying amplitude makes image whiter or blacker (wither the bigger, blacker the smaller).
// Gain affects similar to amplitude.
// The more octaves the more detail is added. Not that noticiable after 6 octaves to be fair.
float FBM(float2 st, float amplitude, float frequency, float lacunarity, float gain, int octaves, float phase = 0.0){
    float result = 0.0;
    for(int i = 0; i < octaves; ++i){
        result += amplitude * noise(frequency * st + phase);
        frequency *= lacunarity;
        amplitude *= gain;
    }
    return result;
}

/* A version of FBM that uses a signed noise function absolute output. Returns a characteristic noise that looks like turbulences.
an example can be found in the following link: https://www.shadertoy.com/view/ldlXRS */

float Turbulence(float2 st, float amplitude, float frequency, float lacunarity, float gain, int octaves){
    float result = 0.0;
    for(int i = 0; i < octaves; ++i){
        result += amplitude * abs(snoise(frequency * st));
        frequency *= lacunarity;
        amplitude *= gain;
    }
    return result;
}

// Ridged multifractal
// See "Texturing & Modeling, A Procedural Approach", Chapter 12
float ridge(float h, float offset) {
    h = abs(h);     // create creases
    h = offset - h; // invert so creases are at top
    h = h * h;      // sharpen creases
    return h;
}

/* Another type of GBM, similar to the previous Turbulence version but showing ridges instead. Can be seen at the end of:
https://thebookofshaders.com/13/ */
float Ridged(float2 st, float amplitude, float frequency, float lacunarity, float gain, float offset, int octaves) {
    float result = 0.0;
    float prev = 1.0;
    for(int i=0; i < octaves; ++i) {
        float n = ridge(snoise(st*frequency), offset);
        result += n*amplitude;
        result += n*amplitude*prev;  // scale by previous octave
        prev = n;
        frequency *= lacunarity;
        amplitude *= gain;
    }
    return result;
}

// A simple warping function.
float warpingFunc(float2 p, float amplitude, float frequency, float lacunarity, float gain, int octaves){
    float2 warped = float2(FBM(p + float2(0.0,0.0), amplitude, frequency, lacunarity, gain, octaves),
                       FBM(p + float2(5.2,1.3), amplitude, frequency, lacunarity, gain, octaves));
    return FBM(p + 4.0*warped, amplitude, frequency, lacunarity, gain, octaves);
}

// A second warping function that uses two warp vectors and time to create a noise pattern for corruption.
float warpingExposed(float2 p, float time, float amplitude, float frequency, float lacunarity,
                     float gain, int octaves){
    
    float2 q, r;
    q.x = FBM( p + float2(2.0,4.0), amplitude, frequency, lacunarity, gain, octaves );
    q.y = FBM( p + float2(3.2,2.3), amplitude, frequency, lacunarity, gain, octaves );
    
    r.x = FBM( p + 4.0 * q + float2(2,2.2) + time * 0.15, amplitude * 0.5, frequency, lacunarity, gain, octaves );
    r.y = FBM( p + 4.0 * q + float2(3.3,4.8) + time * 0.42, amplitude * 0.2, frequency, lacunarity, gain, octaves );
    
    return Turbulence( p + 4.0*r, amplitude, frequency, lacunarity, gain, octaves );
}

// Basic sinusoidal function.
float sinFunc(float x, float amplitude, float frequency, float phase){
    return amplitude * sin(x * frequency + phase);
}

// Used to compute the noise/mist for the sonar/corruption wave. Returns a perlin noise that looks cool.
float sonarMistNoise ( in float2 _st) {
    float v = 0.0;
    float a = 0.5;
    float2 shift = float2(100.0, 100.0);
    for (int i = 0; i < 6; ++i) {
        v += a * noise(_st);
        _st = _st * 2.0 + shift;
        a *= 0.5;
    }
    return v;
}

/*
Applies a radial shear warping effect similar to a wave to the value of input UV.
The center reference point of the warping effect is defined by input Center and
the overall strength of the effect is defined by the value of input Strength. 
Input Offset can be used to offset the individual channels of the result.
https://docs.unity3d.com/Packages/com.unity.shadergraph@5.3/manual/Radial-Shear-Node.html
*/
void Unity_RadialShear_float(float2 UV, float2 Center, float Strength, float2 Offset, out float2 Out)
{
    float2 delta = UV - Center;
    float delta2 = dot(delta.xy, delta.xy);
    float2 delta_offset = delta2 * Strength;
    Out = UV + float2(delta.y, -delta.x) * delta_offset + Offset;
}

/*
Applies a twirl warping effect similar to a black hole to the value of input UV.
The center reference point of the warping effect is defined by input Center and
the overall strength of the effect is defined by the value of input Strength.
Input Offset can be used to offset the individual channels of the result.
https://docs.unity3d.com/Packages/com.unity.shadergraph@5.3/manual/Twirl-Node.html
*/
void Unity_Twirl_float(float2 UV, float2 Center, float Strength, float2 Offset, out float2 Out)
{
    float2 delta = UV - Center;
    float angle = Strength * length(delta);
    float x = cos(angle) * delta.x - sin(angle) * delta.y;
    float y = sin(angle) * delta.x + cos(angle) * delta.y;
    Out = float2(x + Center.x + Offset.x, y + Center.y + Offset.y);
}

/*
Generates a simple, or Value, noise based on input UV.
The scale of the generated noise is controlled by input Scale.
https://docs.unity3d.com/Packages/com.unity.shadergraph@5.3/manual/Simple-Noise-Node.html
*/
inline float unity_noise_randomValue (float2 uv)
{
    return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453);
}

inline float unity_noise_interpolate (float a, float b, float t)
{
    return (1.0-t)*a + (t*b);
}

inline float unity_valueNoise (float2 uv)
{
    float2 i = floor(uv);
    float2 f = frac(uv);
    f = f * f * (3.0 - 2.0 * f);

    uv = abs(frac(uv) - 0.5);
    float2 c0 = i + float2(0.0, 0.0);
    float2 c1 = i + float2(1.0, 0.0);
    float2 c2 = i + float2(0.0, 1.0);
    float2 c3 = i + float2(1.0, 1.0);
    float r0 = unity_noise_randomValue(c0);
    float r1 = unity_noise_randomValue(c1);
    float r2 = unity_noise_randomValue(c2);
    float r3 = unity_noise_randomValue(c3);

    float bottomOfGrid = unity_noise_interpolate(r0, r1, f.x);
    float topOfGrid = unity_noise_interpolate(r2, r3, f.x);
    float t = unity_noise_interpolate(bottomOfGrid, topOfGrid, f.y);
    return t;
}

void Unity_SimpleNoise_float(float2 UV, float Scale, out float Out)
{
    float t = 0.0;

    float freq = pow(2.0, float(0));
    float amp = pow(0.5, float(3-0));
    t += unity_valueNoise(float2(UV.x*Scale/freq, UV.y*Scale/freq))*amp;

    freq = pow(2.0, float(1));
    amp = pow(0.5, float(3-1));
    t += unity_valueNoise(float2(UV.x*Scale/freq, UV.y*Scale/freq))*amp;

    freq = pow(2.0, float(2));
    amp = pow(0.5, float(3-2));
    t += unity_valueNoise(float2(UV.x*Scale/freq, UV.y*Scale/freq))*amp;

    Out = t;
}

#endif