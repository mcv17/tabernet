//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"

struct VS_OUTPUT
{
  float4 Pos      : SV_POSITION;
  float2 Uv       : TEXCOORD0;
  float3 WorldPos : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  float4 Pos : POSITION,
  float2 Uv: TEXCOORD0
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;
  // orient billboard to camera
  output.Pos = Pos * BlastScale;
  output.Pos = float4(output.Pos.x * CameraLeft + output.Pos.y * CameraUp, 1);
  output.Pos = mul(output.Pos, World);
  output.WorldPos = output.Pos.xyz;
  output.Pos = mul(output.Pos, ViewProjection);
  output.Uv = Uv;
  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
  float ripple = 0;
  float radius = 0;
  float time = BlastCurrentTime * BlastSpeed;
  if (BlastCurrentTime > 0) {
    float minRadius = time; 
    float maxRadius = time + BlastThickness * 2 * PI;
    float2 coords = (input.Uv - float2(0.5, 0.5f)) * 2 * PI;
    radius = length(coords);
    // Only create ripple for the designated area
    if (radius > minRadius && radius < maxRadius && maxRadius < PI) {
      // Function to create the blast effect
      ripple = 0.5 * cos(sqrt(coords.x * coords.x + coords.y * coords.y) / BlastThickness + PI - time / BlastThickness) + 0.5;
      ripple *= 1 - maxRadius / PI;
      ripple *= BlastIntensity;
    }
  }

  float3 noise = float3(normalize(input.Uv) * ripple * 0.1, 0.0);

  // Compute coords in screen space to sample the color behind me
  float3 wPos = input.WorldPos.xyz + noise;
  float4 viewSpace = mul( float4(wPos,1.0), ViewProjection );
  float3 homoSpace = viewSpace.xyz / viewSpace.w;
  float2 uv = float2( ( homoSpace.x + 1.0 ) * 0.5, ( 1.0 - homoSpace.y ) * 0.5 );

  float4 albedo_color = txAlbedo.Sample(samClampLinear, uv);
  if (radius < time + BlastThickness * PI)
    albedo_color += float4(ripple, ripple, ripple, 1) * BlastWhiteIntensity / BlastIntensity;

  return float4(albedo_color.xyz, 1);
}

