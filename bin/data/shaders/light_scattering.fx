#include "common.fx"
#include "gbuffer.inc"
#include "noise.fx"
#include "screen_quad.fx"

float4 PS_SILHOUETTE(
  VS_FULL_OUTPUT input) : SV_Target
{
  // txAlbedo contains the depth buffer texture we uploaded.
  // We will translate it to an occlusion mask texture.
  float depthValue = txAlbedo.Sample(samLinear, input.UV);
  // If not occluded, we return 1.0, else we return 0.0 (occluded).
  return float4(0.0, 0.0, 0.0, depthValue == 1.0);
}

#define NUM_SAMPLES 60

float4 PS_GOD_RAYS(
  VS_FULL_OUTPUT input) : SV_Target
{
  // Get the Uv for the given pixel.
  float2 TexCoord = input.UV;

  // Get the difference between the pixel pos and the sun pos in screen space.
  float2 DeltaTexCoord = (TexCoord.xy - LightScatteringSunPosInScreenSpace);
  DeltaTexCoord *= (1.0 / NUM_SAMPLES) * LightScatteringDensity;
  
  // Fetch the color in the given pixel location. Texture is black for everything
  // unless it's a light source.
  float3 Color = txAlbedo.Sample(samLinear, TexCoord).xyz;
  
  float IlluminationDecay = 1.0;
  
  // TAke NUM_SAMPLEs from the given texture point in the given direction.
  for(int i = 0; i < NUM_SAMPLES; ++i){
    TexCoord -= DeltaTexCoord;
    float3 Sample = txAlbedo.Sample(samClampLinear, TexCoord).xyz;
    Sample *= IlluminationDecay * LightScatteringWeight;
    Color += Sample;
    IlluminationDecay *= LightScatteringDecay;
  }
  return float4(Color * LightScatteringExposure, 1.0);
}