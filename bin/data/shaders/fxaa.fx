#include "screen_quad.fx"

#define FXAA_PC 1
#define FXAA_HLSL_5 1
#define FXAA_QUALITY__PRESET 12
#include "FXAA3_11.h"

float4 PS_FXAA(VS_FULL_OUTPUT input) : SV_TARGET
{
   // Fetch the albedo.
   FxaaTex dataTex;
   dataTex.smpl = samBilinear;
   dataTex.tex = txAlbedo;
  
  return float4(FxaaPixelShader(
    input.UV, // Position of the pixel.
    FxaaFloat4(0.0, 0.0, 0.0, 0.0),  // Used only for FXAA Console, NOT on PC.
    // Input color texture.
    // {rgb_} = color in linear or perceptual color space
    // if (FXAA_GREEN_AS_LUMA == 0)
    //     {___a} = luma in perceptual color space (not linear)
    dataTex,
    // Use the same input here as for "tex" in PC
    dataTex,
    // Use the same input here as for "tex" in PC
    dataTex,
    // Only used on FXAA Quality.
    // {x_} = 1.0/screenWidthInPixels
    // {_y} = 1.0/screenHeightInPixels
    fxaaQualityRcpFrame,
    // Not used on PC version.
    FxaaFloat4(0.0, 0.0, 0.0, 0.0),
    // Not used on PC version.
    FxaaFloat4(0.0, 0.0, 0.0, 0.0),
    // Not used on PC version.
    FxaaFloat4(0.0, 0.0, 0.0, 0.0),
    // Choose the amount of sub-pixel aliasing removal.
    // This can effect sharpness.
    //   1.00 - upper limit (softer)
    //   0.75 - default amount of filtering
    //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
    //   0.25 - almost off
    //   0.00 - completely off
    fxaaQualitySubpix,
    // This used to be the FXAA_QUALITY__EDGE_THRESHOLD define.
    // It is here now to allow easier tuning.
    // The minimum amount of local contrast required to apply algorithm.
    //   0.333 - too little (faster)
    //   0.250 - low quality
    //   0.166 - default
    //   0.125 - high quality 
    //   0.063 - overkill (slower)
    fxaaQualityEdgeThreshold,
    // Only used on FXAA Quality.
    // This used to be the FXAA_QUALITY__EDGE_THRESHOLD_MIN define.
    // It is here now to allow easier tuning.
    // Trims the algorithm from processing darks.
    //   0.0833 - upper limit (default, the start of visible unfiltered edges)
    //   0.0625 - high quality (faster)
    //   0.0312 - visible limit (slower)
    fxaaQualityEdgeThresholdMin,
    // Not used on PC version.
    FxaaFloat4(0.0, 0.0, 0.0, 0.0),
    // Not used on PC version.
    FxaaFloat4(0.0, 0.0, 0.0, 0.0),
    // Not used on PC version.
    FxaaFloat4(0.0, 0.0, 0.0, 0.0),
    // Not used on PC version.
    FxaaFloat4(0.0, 0.0, 0.0, 0.0)).xyz, 1.0);
}