//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Pos      : SV_POSITION;
  float3 N        : NORMAL;
  float2 Uv       : TEXCOORD0;
  float3 WorldPos : TEXCOORD1;
  float4 T        : NORMAL1;
};

float4 calcPhongLighting(float NDotL, float3 V, float3 R )
{
	float4 ambient = 0.1;
	float4 diffuse = saturate( NDotL );
	float4 specular = pow( saturate(dot(R,V)), 32);
	
	return (ambient + diffuse + specular) * LightColor * LightIntensity;
} 

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  float4 Pos : POSITION,
  float3 N : NORMAL,
  float2 Uv: TEXCOORD0
  float4 T : NORMAL1
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;
  output.Pos = mul(Pos, World);
  output.WorldPos = output.Pos.xyz;
  output.Pos = mul(output.Pos, ViewProjection);
  output.N = mul(N, (float3x3)World);
  output.T = float4( mul(T.xyz, (float3x3)World), T.w);
  output.Uv = Uv;
  return output;
}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS_skin(
  float4 Pos	: POSITION,
  float3 N		: NORMAL,
  float2 Uv		: TEXCOORD0,
  float4 T		: NORMAL1,
  uint4  BoneIds: BONES,
  float4 Weights: WEIGHTS
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;
  float4x4 SkinMatrix = getSkinMtx( BoneIds,  Weights );
  float4 skinPos = mul(Pos, SkinMatrix);
  float3 skinN = mul(N, (float3x3)SkinMatrix);
  float3 skinT = mul(T.xyz, (float3x3)SkinMatrix);

  output.WorldPos = skinPos.xyz;
  output.Pos = mul(skinPos, ViewProjection);
  output.T = float4(skinT,T.w);
  output.N = skinN;
  output.Uv = Uv;
  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
	input.N = normalize( input.N );		
	float3 V = normalize( Eye - (float3)input.WorldPos ); // Vector to camera from point.
	float3 L = normalize(LightPosition - input.WorldPos);
	float3 R = reflect( -L, input.N); // Light ray reflecting on surface.
  

  /* Tangent */
  //float3 T = input.T.xyz;
  //float3 B = cross(T, input.N) * input.T.w;
  //float3x3 TBN = float3x3(T,B,input.N);
  float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
  N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;                           // Between -1..1

  float3 N = input.N;//mul( N_tangent_space, TBN );
	float NdotL = saturate(dot(N,L));
  /**/

  /* Reflection, env color */
  float3 E = normalize( CameraPosition - input.WorldPos );
  float3 Er = reflect( -E, input.N );

  float4 reflected_color = txEnvironmentMap.SampleLevel(samLinear, Er, 2);
  float4 env_color = txEnvironmentMap.SampleLevel(samLinear, input.N, 2);

  //return ( LightColor * albedo_color * 0.2 + reflected_color * 0.8 ) * shadow_factor;
  /**/

  /* small tweaks */
  // Half vector 
  float3 H = normalize( E + L );
  float  cos_beta = saturate( dot( H, N ) );
  float  glossiness = 10;
  float  specular_factor = pow( cos_beta, glossiness );

  //float fresnel = 1 - saturate( -dot( N, -E ) );
  //return pow( fresnel, 10 );

  /*return ( specular_factor + 
           LightColor * albedo_color * 0.2 + 
           reflected_color * 0.8 
           ) * shadow_factor  + 0.2 * albedo_color;*/
  /**/
	
	// Calculate color.
	float4 PhongLighting = calcPhongLighting(NdotL, V, R );
	float4 texture_color = txAlbedo.Sample(samLinear, input.Uv);
	//texture_color.xyz *= PhongLighting;
	
	// Shadow factor.
	float clipSpacePositionZ = input.Pos.z * input.Pos.w; // Position of the vertex in clip space, we don't normalize the z.
	float shadow_factor = 1.0;
	
	float4 CascadeIndicator = float4(0.0f, 0.0f, 0.0f, 0.0f);
    for (int i = 0 ; i < NUM_CASCADES_SHADOW_MAP ; i++) {
        if (clipSpacePositionZ <= CascadeEndClipSpace[i].z) {
            shadow_factor = min( 0.2f + NdotL * getShadowFactor( i, input.WorldPos ), 1);
			
			if (i == 0) 
                CascadeIndicator = float4(0.05, 0.0, 0.0, 0.0);
            else if (i == 1)
                CascadeIndicator = float4(0.0, 0.05, 0.0, 0.0);
            else if (i == 2)
                CascadeIndicator = float4(0.0, 0.0, 0.05, 0.0);
			
            break;
        }
    }
	return texture_color * shadow_factor * ObjColor;
	//return shadow_factor + CascadeIndicator;
}


