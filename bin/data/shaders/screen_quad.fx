#ifndef SCREENQUAD
#define SCREENQUAD

#include "common.fx"
#include "gbuffer.inc"
#include "noise.fx"

/// --------------------
struct VS_FULL_OUTPUT {
  float4 Pos   : SV_POSITION;
  float2 UV    : TEXCOORD0;
};

// ----------------------------------------
VS_FULL_OUTPUT VS(
  float3 iPos   : POSITION,     // 0..1, 0..1, 0 en la z
  float4 iColor : COLOR0
  )
{
  VS_FULL_OUTPUT output = (VS_FULL_OUTPUT)0;
  output.Pos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 0.5, 1);
  output.UV  = iPos.xy;
  return output;
}

float4 PS(
  VS_FULL_OUTPUT input
  ) : SV_Target
{
  return txAlbedo.Sample(samLinear, input.UV);
}

/* AFAIK none of the code below is used for anything right now. 
Outlining was reworked and the new algorithm can be found now in perception.
It could be removed but I'm keeping it because I don't know
who wrote it and don't want to delete something that is not mine. */

float4 PS_Combine_XRay( 
  float4 iPosition   : SV_POSITION,
  float2 iUV         : TEXCOORD0
  ) : SV_Target
{
  // Outline border.
  const int outlineWidth = 3; // How big the outline will be by default.
  const float smoothingNoise = 0.14; // Noise applied to border.
  const float borderNoiseWidth = 3; // Noise border width, how much noise can be added to the base size.
  float outlineNoiseWidth = (snoise(float2(iPosition.x, iPosition.y) * smoothingNoise + GlobalWorldTime) + 1) * borderNoiseWidth;

  // Fetch the albedo.
  int3 ss_load_coords = uint3(iPosition.xy, 0);
  float4 albedo_color = txGAlbedo.Load(ss_load_coords);
  const int3 offsets[8] = {
    int3(1,0,0),
    int3(0,1,0),
    int3(-1,0,0),
    int3(0,-1,0),
    int3(1,1,0),
    int3(-1,1,0),
    int3(1,-1,0),
    int3(-1,-1,0)
  };
 
  if(albedo_color.a == 0) {
    for (int i = 1; i <= outlineWidth + outlineNoiseWidth; i++) {
      for (int j = 0; j < 8; j++) {
        ss_load_coords = uint3(iPosition.xy, 0) + offsets[j] * i;
        albedo_color = txGAlbedo.Load(ss_load_coords);
        if (albedo_color.a > 0)
          return float4(0.55, 0, 0.7843, 1);
      }
    }
  }
  else {
    return float4(1, 1, 1, 1);
  }
  
  return albedo_color;
}


float4 PS_Combine_Highlights( 
  float4 iPosition   : SV_POSITION,
  float2 iUV         : TEXCOORD0
  ) : SV_Target
{

  // TODO: apply nicer outline
  const int outlineWidth = 2;
  int3 ss_load_coords = uint3(iPosition.xy, 0);
  float4 albedo_color = txGAlbedo.Load(ss_load_coords);
  int3 offsets[8] = {
    int3(1,0,0),
    int3(0,1,0),
    int3(-1,0,0),
    int3(0,-1,0),
    int3(1,1,0),
    int3(-1,1,0),
    int3(1,-1,0),
    int3(-1,-1,0)
  };
  int i, j;
  // random value
  float r = frac(sin(GlobalWorldTime * iPosition.x * iPosition.y) * 43758.5453);
  if(albedo_color.x == 0) {
    for (i = 1; i <= outlineWidth; i++) {
      for (j = 0; j < 8; j++) {
        ss_load_coords = uint3(iPosition.xy, 0) + offsets[j] * i;
        albedo_color = txGAlbedo.Load(ss_load_coords);
        if (albedo_color.x > 0) {
          r = (r - 0.5) * 0.1;
          return float4(0.5, 0.5, 0.5, 1);
        } 
      }
    }
  }
  
  
  return albedo_color;
}

#endif