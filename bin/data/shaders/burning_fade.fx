#include "screen_quad.fx"
#include "noise.fx"

Texture2D    txCurrentScreen      TEXTURE_SLOT(TS_TEXTURE_CURRENT_SCREEN);
Texture2D    txScreenToFadeTo     TEXTURE_SLOT(TS_TEXTURE_SCREEN_TO_FADE_TO);

// Variation of the shader found at : https://www.shadertoy.com/view/XsVfWz,
// By author: k_kondrak.
float4 PS_BURNING_FADE(VS_FULL_OUTPUT input) : SV_TARGET
{
  float2 uv = input.UV.xy;
  
  float t = fmod(BurningFadeCurrentTime, BurningFadeTimeToFinish);
  
  // Texture fades to black slowly and alpha decreases.
  float4 finalColor = lerp(txCurrentScreen.Sample(samLinear, uv),
  float4(0.0, 0.0, 0.0, 0.0),
  smoothstep(t + 0.1, t - 0.1, FBM(uv * .4, 0.35, 20., 2.0, 0.45, 6)));

  // burning on the edges (when finalColor.a < .1)
  if(finalColor.a < BurningFadeEdgeSize)
    finalColor.rgb = clamp(finalColor.rgb + BurningFadeColorIntensity * BurningFadeColorEdgeBurning * FBM(2000. * uv, 0.5, 3., 2.0, 0.5, 6), 0.0, 1.0);
  
  // fancy background under burned texture
  if(finalColor.a < BurningFadeShowBackgroundThreshold)
    finalColor.rgb = txScreenToFadeTo.Sample(samLinear, uv);

  return finalColor;
}