-- Global values used by LUA for script management. We control publishing of scripts
-- so they can be accessed by other scripts through their identity handle.

-- Interesting information: https://paulwatt526.github.io/wattageTileEngineDocs/luaOopPrimer.html


-- This is the table for the ScriptManager. As it is declared in the global context of LUA.
-- It will be a global variable of all our scripts.
ScriptManager = {}
	
-- This map holds all script instances that have been registered globally.
-- They are indexed by their CHandle value that comes from the engine.
-- Scripts add and remove themselves from this structure when their component
-- gets created or removed. For now, no variables are added to check if the component
-- of the script is disable (it will still work as long as it's not removed).
local registeredScriptInstances = {}

--[[   SCRIPT HANDLE INFORMATION   ]]

-- This is a script Handle. It holds the reference to the instance of the script.
-- Scripts that ask for a reference to another script will get returned this object.
-- They should hold a reference to this object but NOT STORE THE SCRIPT INSTANCE inside of it.
-- Otherwise, the script instance will never go out of scope and it won't be removed.
-- Think of it as the CHandle of the engine where we can store the * to the component because
-- it may change. Different reasons but same procedure.
ScriptHandle = {}
ScriptHandle.__index = ScriptHandle

function ScriptHandle:create(CHandle, ScriptInstance)
   local script = {}             -- our new object
   setmetatable(script, ScriptHandle)  -- make ScriptHandle handle lookup
   
   script._EntityHandle = CHandle -- Handle of the entity.
   script._ScriptInstance = ScriptInstance -- Underscores indicate its supposed to be private.
   script._IsRemoved = false -- I may try making them really private in the future.
 
   return script
end

function ScriptHandle:getEntityHandle()
	return self._EntityHandle
end

-- Returns the script instance or nil if the script is removed.
-- Use it to get the actual instance of the script so you can call
-- its function. DON'T HOLD A POINTER TO IT THOUGH. THIS IS VERY IMPORTANT. If you need to hold a
-- reference, hold a reference to the ScriptHandle obj.
function ScriptHandle:getScriptInstance()
	if self._IsRemoved then
		return nil
	else
		return self._ScriptInstance
	end
end

-- Removes the script by setting the pointer to null.
-- This is called by the ScriptManager removeScriptInstance.
-- This is also supposed to be a private method that should not be
-- called from outside.
function ScriptHandle:_removeScript()
	if not self._IsRemoved then
		self._IsRemoved = true
		self._ScriptInstance = nil
		return true
	end
	return false
end

--[[   SCRIPT REGISTERING INFORMATION   ]]

-- Publish a script instance so it can be viewed, and accessed by other scripts.
-- Do this if you need communication between scripts. Returns true if the script was
-- registered, false if there was another script already registered and not removed.
function ScriptManager.publishScriptInstance(CHandle, ScriptInstance)
	local handleAsNum = CHandle:asUnsigned()
	if ScriptManager.isScriptRegistered(handleAsNum) then
		return false
	end

	registeredScriptInstances[handleAsNum] = ScriptHandle:create(CHandle, ScriptInstance)
	return true
end

-- Return a script handle that has been published so it can be accessed by other scripts.
-- Store the reference and call the methods you need.
function ScriptManager.getRegisteredScriptInstance(CHandle)
	return registeredScriptInstances[CHandle:asUnsigned()]
end


-- Return a script handle that has been published so it can be accessed by other scripts.
-- Store the reference and call the methods you need.
function ScriptManager.isScriptRegistered(handleAsNum)
	if not(registeredScriptInstances[handleAsNum] == nil) then
		return not(registeredScriptInstances[handleAsNum]:getScriptInstance() == nil)
	else
		return false
	end
end

-- Removes a script instance that was published to be accessed by others scripts. From now on,
-- it can not be fetched by others scripts. Returns true if it was removed, false ow.
function ScriptManager.removeScriptInstance(CHandle)
	local handleAsNum = CHandle:asUnsigned()
	if not ScriptManager.isScriptRegistered(handleAsNum) then
		return false
	end

	local result = registeredScriptInstances[handleAsNum]:_removeScript()
	return result
end