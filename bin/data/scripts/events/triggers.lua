function onEnterCamaraTrigger001(entity, trigger)
	dbg("It works! The event system works!")
	dbg(entity:TCompName().name, " entered ", trigger:TCompName().name)
	entity:TCompHealth().immortal = false
end

function onExitCamaraTrigger001(entity, trigger)
	dbg("It works even in here!")
	dbg(entity:TCompName().name, " exited ", trigger:TCompName().name)
end

function onEnterArenaTestTrigger(entity, trigger)
	dbg("ArenaTestTrigger")
	if entity:TCompName().name == "Player" then
		--LogicManager:createEntity("data/prefabs/enemyMelee.json", 10, 0, 10)
		--LogicManager:createEnemyMelee(10, 0, 10)
		--LogicManager:createEnemyRanged(10, 0, 10)
		--LogicManager:createEnemy(Vector3.new(-10, 0, 0))	
		--LogicManager:createEnemyMelee(0, 3, 4, 3.14, 0, 0) --the yaw pitch roll are in pi radians not º
	end
end

function onEnterArena1Trigger(entity, trigger)
	if entity:TCompName().name == "Player" then
		LogicManager:playMusic("Arena")
	end
end
function onExitArena1Trigger(entity, trigger)
	if entity:TCompName().name == "Player" then
		LogicManager:playMusic("Default")
	end
end