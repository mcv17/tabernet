function TeleportPlayer(PositionToTPTo)
	local playerHandle = LogicManager:getEntityByName("Player")
	if playerHandle == nil or not playerHandle:isValid() then return end
	
	local controller = playerHandle:TCompPlayerCharacterController()
	if controller == nil then return end
	controller:teleport(PositionToTPTo)
end