function onPlayerDead()
	dbg("Oh no! Player is dead!")
end

--The following three functions are ordered as their runtime order
--This is called before onEntitiesLoaded because they're loaded but the current gamestate hasn't been processed yet
function onModulesLoaded()
	LogicManager:setActiveMouse(false)
	LogicManager:showCursor(true)
	LogicManager:setCenterMouseActive(false)
end

function onGamestateChange()

end

function onEntitiesLoaded()
	LogicManager:deactivateWidget("black_screen")
	LogicManager:activateWidget("black_screen")
end

--You can create a function with a gamestate name
function gs_gametesting()
	--We configure the default and output camera from the game scene
	LogicManager:setDefaultCamera("Camera-Player-Output")
	LogicManager:setOutputCamera("Camera-Output")

	LogicManager:playMusic("Default");

	--We enable the mouse back
	LogicManager:setActiveMouse(true)
	LogicManager:setCenterMouseActive(true)

	--Disable the loading UI widget
	LogicManager:deactivateWidget("loading_screen")
	LogicManager:activateWidget("hints")
	LogicManager:setEventParameter("event:/UI/GameStart", "LoadingScreenActivated", 0.0)

	-- Add delay 3s. in black screen
	LogicManager:setBlackScreenAlpha(1.0)
	LogicManager:fadeOutBlackScreen("BlackScreenFadeOut3sAlreadyBlack");
end

function gs_main_menu()
	--We configure the default and output camera from the menu scene
	LogicManager:setDefaultCamera("Camera-Main-Menu")
	LogicManager:setOutputCamera("Camera-Output")

	--We disable the mouse
	LogicManager:setActiveMouse(true)
	LogicManager:setCenterMouseActive(false)

	--Disable the loading UI widget
	LogicManager:deactivateWidget("loading_screen")
	LogicManager:deactivateWidget("hud")
	LogicManager:setEventParameter("event:/UI/GameStart", "LoadingScreenActivated", 0.0)
	LogicManager:deactivateWidget("hints")

	--Reset render lights because maybe have been modified
	LogicManager:SetRenderAmbientVariables(1.5, 0.4, 1.1, 1.0, 0)
end

function gs_editor()
	--We configure the default and output camera from the menu scene
	LogicManager:setDefaultCamera("Camera-Player-Output")
	LogicManager:setOutputCamera("Camera-Output")

	LogicManager:setActiveMouse(true)
	LogicManager:setCenterMouseActive(true)

	LogicManager:activateWidget("hud")
end

function gs_shader_test()
	--We configure the default and output camera from the menu scene
	LogicManager:setDefaultCamera("Camera-Debug")
	LogicManager:setOutputCamera("Camera-Output")

	LogicManager:setActiveMouse(true)
	LogicManager:setCenterMouseActive(true)

end