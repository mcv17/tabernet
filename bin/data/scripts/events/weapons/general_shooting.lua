-- This file contains functions that can be called for spawning
-- particles, sounds, and decals when using weapons.
-- In particular. It contains base functions for spawning fx in two different
-- situations that weapons from our game can trigger. This are:
-- * Bullet Hit: When a bullet hit a surfaces (whatever it may be: stone, flesh, ...)
-- * Blood splatter: Called if a blood splatter created by a bullet touches a solid surface.

-- Here we have hardcoded the decals and particles that we will spawn. Change them from here
-- if you want to. We could load them at the beginning from a JSON but that would require either
-- not using scripting and losing the ability to make changes on the fly or implementing json reading
-- inside scripting. Holding them here is good enough and allows us to do perform any extra logic necessary
-- instead of being limited in what we can do when spawning FX by default.

-- Spawns fx on bullet hit surface.
-- touchingPos: position where the hit happens.
-- lookAt : position + normal of the position hit.
-- typeSurface: What type of surface it hits.
function spawnOnHitFXDefaultWeapon(touchingPos, lookAt, typeSurface)
	if isSurfaceBaseEnemy(typeSurface) then
		SpawnDecalsOnBaseEnemy(touchingPos, lookAt, typeSurface)
	elseif isSurfaceEnemyImmortal(typeSurface) then
		SpawnDecalsOnEnemyImmortal(touchingPos, lookAt, typeSurface)
	elseif isSurfaceSand(typeSurface) then
		SpawnFXOnSand(touchingPos, lookAt, typeSurface)
	elseif isSurfaceWood(typeSurface) then
		SpawnFXOnWood(touchingPos, lookAt, typeSurface)
	elseif isSurfaceMetal(typeSurface) then
		SpawnFXOnMetal(touchingPos, lookAt, typeSurface)
	else
		SpawnDecalsOnSolidSurface(touchingPos, lookAt, typeSurface)
	end
end

-- If we hit a base enemy, we will spawn some blood particles.
local defaultBloodParticles = "data/particles/blood_splatters/blood_splatter.particles"
local defaultBloodMistParticles = "data/particles/blood_splatters/blood_splatter_mist.particles"
local fleshHitSoundEvent = "event:/Effects/BulletSurfaceFlesh"
function SpawnDecalsOnBaseEnemy(touchingPos, lookAt, typeSurface)
	local randomAngle = LogicManager:getRandomFloat(-180.0, 180.0)
	randomAngle = randomAngle * (math.pi / 180.0)

	if (fleshHitSoundEvent) then
		LogicManager:playEvent(fleshHitSoundEvent, touchingPos)
	end

	-- Spawn a particle of sparks and one of flash.
	LogicManager:SpawnParticle(defaultBloodParticles, touchingPos, lookAt, randomAngle)
	LogicManager:SpawnParticle(defaultBloodMistParticles, touchingPos, lookAt, randomAngle)
end

-- If we hit a immortal, we spawn spark particles.
local defaultFlashOnHitParticles = "data/particles/blood_splatters/immortal_flash_on_hit.particles"
local defaultSparksParticles = "data/particles/blood_splatters/immortal_sparks.particles"
local immortalHitSoundEvent = "event:/Enemies/Inmortal/InmortalHit"
function SpawnDecalsOnEnemyImmortal(touchingPos, lookAt, typeSurface)
	local randomAngle = LogicManager:getRandomFloat(-180.0, 180.0)
	randomAngle = randomAngle * (math.pi / 180.0)

	if (immortalHitSoundEvent) then
		LogicManager:playEvent(immortalHitSoundEvent, touchingPos)
	end

	-- Spawn a particle of sparks and one of flash.
	LogicManager:SpawnParticle(defaultFlashOnHitParticles, touchingPos, lookAt, randomAngle)
	LogicManager:SpawnParticle(defaultSparksParticles, touchingPos, lookAt, randomAngle)
end

-- Called when hit a sandy or dirt surface
local defaultDustParticle = "data/particles/dust_shoot.particles"
local sandHitSoundEvent = "event:/Effects/BulletSurfaceDirt"
function SpawnFXOnSand(touchingPos, lookAt, typeSurface)
	local randomAngle = LogicManager:getRandomFloat(-180.0, 180.0)
	randomAngle = randomAngle * (math.pi / 180.0)

	if (sandHitSoundEvent) then
		LogicManager:playEvent(sandHitSoundEvent, touchingPos)
	end

	-- Spawn a particle of dust.
	LogicManager:SpawnParticle(defaultDustParticle, touchingPos, lookAt, randomAngle)
end

-- Called when hit a wooden surface
local defaultBulletHoleDecal = "BulletHole"
local numberOfDefaultBulletHoleDecalsPresent = 11
local woodHitSoundEvent = "event:/Effects/BulletSurfaceWood"
function SpawnFXOnWood(touchingPos, lookAt, typeSurface)
	local randomAngle = LogicManager:getRandomFloat(-180.0, 180.0)
	randomAngle = randomAngle * (math.pi / 180.0)

	local randomDecal = LogicManager:getRandomInt(1, numberOfDefaultBulletHoleDecalsPresent)
	local decal = defaultBulletHoleDecal .. tostring(randomDecal)

	if (woodHitSoundEvent) then
		LogicManager:playEvent(woodHitSoundEvent, touchingPos)
	end

	-- Spawn a default bullet decal if no hit.
	LogicManager:SpawnDecal(decal, touchingPos, lookAt, randomAngle)

	-- Spawn a particle of dust.
	LogicManager:SpawnParticle(defaultDustParticle, touchingPos, lookAt, randomAngle)
end

-- Called when hit a metallic surface
local metalHitSoundEvent = "event:/Effects/BulletSurfaceMetallic"
function SpawnFXOnMetal(touchingPos, lookAt, typeSurface)
	local randomAngle = LogicManager:getRandomFloat(-180.0, 180.0)
	randomAngle = randomAngle * (math.pi / 180.0)

	local randomDecal = LogicManager:getRandomInt(1, numberOfDefaultBulletHoleDecalsPresent)
	local decal = defaultBulletHoleDecal .. tostring(randomDecal)

	if (metalHitSoundEvent) then
		LogicManager:playEvent(metalHitSoundEvent, touchingPos)
	end

	-- Spawn a default bullet decal if no hit.
	LogicManager:SpawnDecal(decal, touchingPos, lookAt, randomAngle)

	-- Spawn a particle of sparks and one of flash.
	LogicManager:SpawnParticle(defaultFlashOnHitParticles, touchingPos, lookAt, randomAngle)
	LogicManager:SpawnParticle(defaultSparksParticles, touchingPos, lookAt, randomAngle)
end

-- Called when spawning decals on a solid surface treated as a "default" one if no other specific surface was found.
local concreteHitSoundEvent = "event:/Effects/BulletSurfaceConcrete"
function SpawnDecalsOnSolidSurface(touchingPos, lookAt, typeSurface)
	local randomAngle = LogicManager:getRandomFloat(-180.0, 180.0)
	randomAngle = randomAngle * (math.pi / 180.0)

	local randomDecal = LogicManager:getRandomInt(1, numberOfDefaultBulletHoleDecalsPresent)
	local decal = defaultBulletHoleDecal .. tostring(randomDecal)

	if (concreteHitSoundEvent) then
		LogicManager:playEvent(concreteHitSoundEvent, touchingPos)
	end

	-- Spawn a default bullet decal if no hit.
	LogicManager:SpawnDecal(decal, touchingPos, lookAt, randomAngle)

	-- Spawn a particle of dust.
	LogicManager:SpawnParticle(defaultDustParticle, touchingPos, lookAt, randomAngle)
end

-- Spawns a blood splatter.
-- touchingPos: position where the splatter happens.
-- lookAt : position + normal of the position hit.
-- typeSurface not used right now.
local blood = "Blood"
function spawnBloodSplatterFXDefaultWeapon(touchingPos, lookAt, typeSurface)
	local randomAngle = LogicManager:getRandomFloat(-180.0, 180.0)
	randomAngle = randomAngle * (math.pi / 180.0)
	
	local randomDecal = LogicManager:getRandomInt(1, 6)
	local decal = blood .. tostring(randomDecal)
	
	-- Spawn a blood decal if hit.
	LogicManager:SpawnDecal(decal, touchingPos, lookAt, randomAngle);

	-- Spawn a particle of dust in the place the splatter happened.
	LogicManager:SpawnParticle("data/particles/dust_shoot.particles", touchingPos, lookAt, randomAngle);
end

-- TODO_SOUND: still missing dirt surface, will add it later