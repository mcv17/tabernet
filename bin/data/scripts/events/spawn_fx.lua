function spawnMelee(EntityHandle, alert)
	if not EntityHandle:isValid() then return end
	local SpawnHandle = EntityHandle:TCompSpawn()
	if SpawnHandle == nil then return end
	
	SpawnHandle:SpawnEntity("EnemyMelee", 2.0, alert)
	spawnMeleeFX(SpawnHandle:GetSpawnPosition())
end

-- TODO_SOUND: change value of the next variable to the real event path
local spawnMeleeSoundEvent = "event:/Effects/Spawn"
function spawnMeleeFX(position)
	-- Spawn particles first.
	LogicManager:SpawnParticle("data/particles/fire_melee_spawn.particles", position);
	LogicManager:playEvent("")
	-- Spawn a sound too
	if (spawnMeleeSoundEvent) then
		LogicManager:playEvent(spawnMeleeSoundEvent, position)
	end
end

function spawnMeleeMini(EntityHandle, alert)
	if not EntityHandle:isValid() then return end
	local SpawnHandle = EntityHandle:TCompSpawn()
	if SpawnHandle == nil then return end
	
	SpawnHandle:SpawnEntity("EnemyMeleeMini", 2.0, alert)
	spawnMeleeMiniFX(SpawnHandle:GetSpawnPosition())
end

function spawnMeleeMiniFX(position)
	-- Spawn particles first.
	LogicManager:SpawnParticle("data/particles/fire_melee_spawn.particles", position);
	
	-- Spawn a sound too
	if (spawnMeleeSoundEvent) then
		LogicManager:playEvent(spawnMeleeSoundEvent, position)
	end
end

function spawnRanged(EntityHandle, alert)
	if not EntityHandle:isValid() then return end
	local SpawnHandle = EntityHandle:TCompSpawn()
	if SpawnHandle == nil then return end
	
	SpawnHandle:SpawnEntity("EnemyRanged", 2.0, alert)
	spawnRangedFX(SpawnHandle:GetSpawnPosition())
end

function spawnRangedBuffed(EntityHandle, alert)
	if not EntityHandle:isValid() then return end
	local SpawnHandle = EntityHandle:TCompSpawn()
	if SpawnHandle == nil then return end
	
	SpawnHandle:SpawnEntity("EnemyRangedBuffed", 2.0, alert)
	spawnRangedFX(SpawnHandle:GetSpawnPosition())
end

function spawnRangedSniperGround(EntityHandle, alert)
	if not EntityHandle:isValid() then return end
	local SpawnHandle = EntityHandle:TCompSpawn()
	if SpawnHandle == nil then return end
	
	SpawnHandle:SpawnEntity("EnemyRangedSniperGround", 2.0, alert)
	spawnRangedFX(SpawnHandle:GetSpawnPosition())
end

function spawnRangedSniperRoof(EntityHandle, alert)
	if not EntityHandle:isValid() then return end
	local SpawnHandle = EntityHandle:TCompSpawn()
	if SpawnHandle == nil then return end
	
	SpawnHandle:SpawnEntity("EnemyRangedSniperRoof", 2.0, alert)
	spawnRangedFX(SpawnHandle:GetSpawnPosition())
end

-- TODO_SOUND: change value of the next variable to the real event path
local spawnRangedSoundEvent = "event:/Effects/Spawn2"
function spawnRangedFX(position)
	-- Spawn particles first.
	LogicManager:SpawnParticle("data/particles/fire_ranged_spawn.particles", position);
	
	-- Spawn a sound too
	if (spawnRangedSoundEvent) then
		LogicManager:playEvent(spawnRangedSoundEvent, position)
	end
end

function spawnImmortal(EntityHandle, alert)
	if not EntityHandle:isValid() then return end
	local SpawnHandle = EntityHandle:TCompSpawn()
	if SpawnHandle == nil then return end

	SpawnHandle:SpawnEntity("EnemyImmortal", 2.0, alert)
	spawnImmortalFX(SpawnHandle:GetSpawnPosition())
end

-- TODO_SOUND: change value of the next variable to the real event path
local spawnImmortalSoundEvent = "event:/Effects/Spawn"
function spawnImmortalFX(position)
	-- Spawn particles first.
	LogicManager:SpawnParticle("data/particles/fire_immortal_spawn.particles", position);
	
		-- Spawn a sound too
	if (spawnImmortalSoundEvent) then
		LogicManager:playEvent(spawnImmortalSoundEvent, position)
	end
end