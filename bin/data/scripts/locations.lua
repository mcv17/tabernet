locationPositions = {}
locationNames = {}

curLocation = 1

function addTestLocation(name, pos)
	table.insert(locationPositions, pos)
	locationNames[name] = #locationPositions
end

function teleportTo(place)
	curLocation = locationNames[place]
	LogicManager:teleportPlayer(locationPositions[curLocation])
end

function teleportIterate(iter)
	iter = iter or 1
	if curLocation > 0 and curLocation <= #locationPositions and #locationPositions > 0 then
		curLocation = curLocation + iter
		if curLocation < 1 then
			curLocation = #locationPositions
		elseif curLocation > #locationPositions then
			curLocation = 1
		end
		LogicManager:teleportPlayer(locationPositions[curLocation])
	end
end
