ClassExample = {}
ClassExample.__index = ClassExample

function ClassExample:create(balance)
   local classSpawn = {}             -- Our new object.
   setmetatable(classSpawn, ClassExample)  -- Get sure to add this line and the correct parameters.
   
   classSpawn.balance = balance      -- initialize our object variables
   
   return classSpawn
end

--[[ ClassExample functions ]]
-- Can be called by ClassExample or other scripts with the reference to this.

function ClassExample:update(dt)
	self:show()
end

function ClassExample:show()
	dbg("Money left ", self.balance) 
end

function ClassExample:withdraw(amount)
   self.balance = self.balance - amount
end


--[[ If you are creating the script as an object that can be called by other objects,
don't forget to add an instance of the class and register it in the OnEntityCreated call.
Otherwise, you may very well not register it and not even create an instance of class. ]]

-- create and use an ClassExample
local instance = ClassExample:create(1000)

--[[ onEvent functions. They should have this specific signature, don't change.
This functions are called by the TCompScript when an event is recieved and they
will get sent to the given object. ]]

-- Recieved when the TCompScript is just loaded. Do anything you may need here.
-- Components are still not created though, the entity not being finished creating.
function onStart()
	
end

-- Recieved when the entity is created. Do anything you may want with
-- the instance and then get sure to publish the script if you want it
-- to be accessed from outside this script.
function onEntityCreated(CHandle)
	dbg("onEntityCreated. CHandle recieved: ", CHandle)
	
	-- Example of publishing the script if we want it to be able
	-- to be accesed from outside. If you don't want to publish it
	-- don't add this line. Only publish script that will be used.
	ScriptManager.publishScriptInstance(1, instance)
end

-- Do anything with your instance here if you want.
-- This is recieved when the complete scene is created.
function onSceneCreated()

end

-- Cal from here the update of your object if you have one.
function onUpdate(dt)
	-- For example:
	instance:update(dt)
end

-- This function is called whenever a onTriggerEnter event
-- is recieved by the entity with the given script.
function onTriggerEnter()
	-- Your test code goes here
	dbg("onTriggerEnter called")
end

-- This function is called whenever a onTriggerExit event
-- is recieved by the entity with the given script.
function onTriggerExit()
	-- Your test code goes here
	dbg("onTriggerExit called")
end

-- This function is called whenever a onEnable event
-- is recieved by the entity with the given script.
function onEnable()
	-- Your test code goes here
	dbg("onEnable called")
end

-- This function is called whenever a onDisable event
-- is recieved by the entity with the given script.
function onDisable()
	-- Your test code goes here
	dbg("onDisable called")
end

-- This function is called whenever a onEnd event
-- is recieved by the entity with the given script.
function onEnd(CHandle)
	-- Your test code goes here
	dbg("onEnd called")
	ScriptManager.removeScriptInstance(CHandle)
end