Prueba = {}
Prueba.__index = Prueba

function Prueba:create(balance)
   local acnt = {}             -- our new object
   setmetatable(acnt,Prueba)  -- make Prueba handle lookup
   acnt.balance = balance      -- initialize our object
   return acnt
end

function Prueba:show()
	dbg("Money left ", self.balance) 
end

function Prueba:anotherMethod(amount)
	self.balance = self.balance + amount
	dbg("Método de Prueba")
	dbg("Dinero ", self.balance)
end

-- create and use an Prueba
acc = Prueba:create(1000)


function onStart()
	-- Your test code goes here
	dbg("onStart called")
	local b = ScriptManager.publishScriptInstance(1, acc)
end

function onEntityCreated(CHandle)
	-- Your test code goes here
	dbg("onEntityCreated called", CHandle)
end

function onSceneCreated()
	-- Your test code goes here
	dbg("onSceneCreated called")
end

function onUpdate()
	-- Your test code goes here
	--dbg("Esto es PruebaScript1")
	--acc:show()
	--dbg("---------------------")
end

function onTriggerEnter()
	-- Your test code goes here
	dbg("onTriggerEnter called")
end

function onTriggerExit()
	-- Your test code goes here
	dbg("onTriggerExit called")
end

function onEnable()
	-- Your test code goes here
	dbg("onEnable called")
end

function onDisable()
	-- Your test code goes here
	dbg("onDisable called")
end

function onEnd()
	-- Your test code goes here
	dbg("onEnd called")
	ScriptManager.removeScriptInstance(1)
end

