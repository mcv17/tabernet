
function onbt_retryClicked()
    LogicManager:restorePlayerToLastCheckpoint()
    LogicManager:showCursor(false)
    LogicManager:setCenterMouseActive(true)
end

function onbt_dead_main_menuClicked()
    ModuleGameplay:main_menu()
    LogicManager:SetUIImageAlpha("game_over", "game_over_image", 0.0)
	LogicManager:deactivateWidget("game_over")
	LogicManager:deactivateWidget("game_over_buttons")
end