dofile("data/scripts/locations.lua")

addTestLocation("spawn", Vector3.new(0, 0, 0))
addTestLocation("first_corner", Vector3.new(64, 0, 0))
addTestLocation("first_arena_entrance", Vector3.new(74.166, 0, 37.704))
addTestLocation("first_checkpoint", Vector3.new(-17.54, 0.5, 86.54))
addTestLocation("second_checkpoint", Vector3.new(144, 0, 120))
addTestLocation("sanctuary", Vector3.new(112, 0, 137))
addTestLocation("second_arena_exit", Vector3.new(88, 0, 192))
addTestLocation("final_zone", Vector3.new(85, 0, 300))
addTestLocation("third_checkpoint", Vector3.new(260, 0, 109))
addTestLocation("last_checkpoint", Vector3.new(346.31, 0, 68))

function teleportUtils(default)
	default = default or "spawn"

	if Input["shift"]:isPressed() then
		teleportIterate(-1)
	elseif Input["control"]:isPressed() then
		teleportTo(default)
	else
		teleportIterate()
	end
end

timeControl = false

function toggleTime(valueWhenFalse, valueWhenTrue)
	valueWhenTrue = valueWhenTrue or 1.0
	if (timeControl) then
		LogicManager:setTimeScale(valueWhenTrue)
	else
		LogicManager:setTimeScale(valueWhenFalse)
	end

	timeControl = not timeControl
end

idx = 0
function test()
	-- Your test code goes here
end
