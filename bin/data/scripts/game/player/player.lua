-- Scripted player functions. Used for different types of functions.

-- When one of the feet of the player hits the ground.
function onTouchingGround(touchingPos, touchingNormal)
	LogicManager:SpawnParticle("data/particles/dust_walk.particles", touchingPos);
end

-- When one of the feet of the player leaves the ground.
function onLeavingGround(leavingPos, leavingNormal)
-- Nothing for now, don't know if we will add something.
end

-- YPosFixer.
lastStablePos = Vector3.new()

-- all entities in the game will be loaded when this function is called
function start()
	dbg("YPosFixer script successfully started for ", entity:TCompName().name)
end

function update()
	pos = entity:TCompTransform():getPosition()
	if entity.TCompCharacterController ~= nil then
		controller = entity:TCompCharacterController()
	elseif entity.TCompPlayerCharacterController ~= nil then
		controller = entity:TCompPlayerCharacterController()
	else
		error("Entity does not have a controller")
	end

	if controller:isGrounded() then
		lastStablePos = pos
	elseif pos.y < yThreshold then
		controller:teleport(lastStablePos + Vector3.up * upSpawn)
	end
end