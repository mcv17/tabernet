Arena = {}
Arena.__index = Arena

function Arena:new()
   local arena = {}             -- our new object
   setmetatable(arena, Arena)  -- make Arena handle lookup
   
   -- Spawns.
   arena.ownHandle = nil -- Own handle
   arena.spawnsHandles = {} -- Spawn instances. Holds Script Handles that contain both script instance and handle.
   arena.spawnsFreeSpawns = {}
   
   -- Doors.
   arena.doorsPrefab = ""
   arena.doors = {}
   
   -- Enemies.
   arena.enemies = {}
   
   -- Arena variables.
   arena.currentEnemiesAlive = 0
   arena.enemiesToSpawn = 15
   arena.currentTime = 0.0
   arena.timeBetweenSpawns = 2.0
   arena.maxNumEnemiesAlive = 5
   arena.spawningEnemy = false
   
   arena.arenaActive = false -- When true, it will start spawning enemies.
   
   return arena
end

function Arena:update(dt)
	if not self.arenaActive then return end

	--dbg("Enemies to spawn ", self.enemiesToSpawn)
	--dbg("enemies alive: ", self.currentEnemiesAlive)
	--dbg("max enemies alive: ", self.maxNumEnemiesAlive)
	--dbg("DOOR HANDLE: ", self.doors[1]:asUnsigned())
	--dbg("DOOR HANDLE: ", self.doors[2]:asUnsigned())
	
	-- Erase dead enemies.
	self:checkIfAnyEnemiesDied();
	
	-- On spawn time runs out, spawn.
	if (self.enemiesToSpawn > 0) then
		self.currentTime = self.currentTime + dt;
		if (self.currentTime > self.timeBetweenSpawns) and (self.maxNumEnemiesAlive > self.currentEnemiesAlive) then
			-- If we are not already spawning an enemy.
			if not self.spawningEnemy then
				self:SpawnEnemies();
			end
		end
	elseif (self.currentEnemiesAlive == 0) then -- All enemies are spawned and dead.
		self:endArena();
	end
end

function Arena:createDoors()
	self.doors = LogicManager:CreatePrefab(self.doorsPrefab, 0.0, 0.0, 0.0)
	
	-- ATTENTION, THIS DOESN't WORK, store the vector directly or it may break..
	-- Don't do this as the objects are not kept and so they don't have a reference.
	--local a =  LogicManager:CreatePrefab(self.doorsPrefab, 0.0, 0.0, 0.0)
	--for idx = 1, #a do
	--	table.insert(self.doors, a[idx])
	--end
end

function Arena:onTriggerEnter(entity)
	if self.arenaActive then return end

	local tag = entity:TCompTags()
	if tag and tag:hasTag("player") then
		self.arenaActive = true;
		self:createDoors();
	end
end

function Arena:spawnIsFree(spawnerEntity, isFree)
	if isFree then
		self:addSpawnToFreeOnes(spawnerEntity)
	else
		self:removeSpawnFromFreeOnes(spawnerEntity)
	end
end

function Arena:addSpawnToFreeOnes(spawnerEntity)
	table.insert(self.spawnsFreeSpawns, spawnerEntity);
end

function Arena:removeSpawnFromFreeOnes(spawnerEntity)
	for idx = 1, #self.spawnsFreeSpawns do
		if spawnerEntity == self.spawnsFreeSpawns[idx] then
			table.remove(self.spawnsFreeSpawns, idx)
			break
		end
	end
end

function Arena:registerSpawn(spawnHand)
	table.insert(self.spawnsHandles, spawnHand);
	table.insert(self.spawnsFreeSpawns, spawnHand);
end

function Arena:unregisterSpawn(spawnHand)
	local handleAsNumToRemove = spawnHand:getEntityHandle():asUnsigned()
	for idx = 1, #self.spawnsHandles do
		local handle = self.spawnsHandles[idx]
		local handleAsNum = handle:getEntityHandle():asUnsigned()
		
		if handleAsNumToRemove == handleAsNum then
			table.remove(self.spawnsHandles, idx);
			break
		end
		
	end

	for idx = 1, #self.spawnsFreeSpawns do
		local handle = self.spawnsFreeSpawns[idx]
		local handleAsNum = handle:getEntityHandle():asUnsigned()
		
		if handleAsNumToRemove == handleAsNum then
			table.remove(self.spawnsFreeSpawns, idx);
			break
		end
		
	end
end

function Arena:pickRandomSpawn()
	local numFreeSpawns = #self.spawnsFreeSpawns
	local spawn = nil
	if numFreeSpawns > 0 then
		local spawnIndx = math.random(1, numFreeSpawns)
		spawn = self.spawnsFreeSpawns[spawnIndx]
	end
	return spawn
end

function Arena:SpawnEnemies()
	local spawnH = self:pickRandomSpawn();
	if (spawnH == nil) then return end -- All spawns are occupied or no spawn was found.

	local spawn = spawnH:getScriptInstance()
	
	-- Call the spawn so it fetches a new enemy.
	-- If it returns false, the spawn couldn't create an enemy.
	if spawn:spawnRandomEnemy() == false then
		return
	end

	self.enemiesToSpawn = self.enemiesToSpawn - 1
	self.currentEnemiesAlive = self.currentEnemiesAlive + 1
	-- Set it to true, we have now requested the enemy to the spawn.
	-- Spawn will spawn particles and everything and return a handle to the enemy
	-- once it creates it.
	self.spawningEnemy = true
end

function Arena:AddSpawnedEnemy(handle)
	table.insert(self.enemies, handle);
	-- Once enemy recieved, we can get ready to start waiting for spawning next one.
	self.currentTime = 0.0;
	self.spawningEnemy = false
end

function Arena:checkIfAnyEnemiesDied()
	for idx = 1, #self.enemies do
		local enemyHandle = self.enemies[idx]
		if enemyHandle:isValid() then		
			if self:checkIfEnemyDead(enemyHandle) then
				self.currentEnemiesAlive = self.currentEnemiesAlive - 1
				table.remove(self.enemies, idx)
				break
			end
		else
			self.currentEnemiesAlive = self.currentEnemiesAlive - 1
			table.remove(self.enemies, idx)
			break
		end
	end
end

function Arena:checkIfEnemyDead(enemy)
	return enemy:TCompHealth():isDead()
end

function Arena:endArena()
	-- Destroy doors.
	for idx = 1, #self.doors do
		LogicManager:DestroyEntity(self.doors[idx])
	end

	-- Destroy spawns.
	for idx = 1, #self.spawnsHandles do
		LogicManager:DestroyEntity(self.spawnsHandles[idx]:getEntityHandle())
	end
	
	-- Destroy arena.
	LogicManager:DestroyEntity(self.ownHandle)
end

-- create and use an Spawn
instance = Arena:new()

math.randomseed(os.time())

function onStart(variables)
	instance.doorsPrefab = getValueVariablesArray(variables, "doors", "")
	instance.enemiesToSpawn = getValueVariablesArray(variables, "enemies_to_spawn", instance.enemiesToSpawn)
	instance.timeBetweenSpawns = getValueVariablesArray(variables, "time_between_spawns", instance.timeBetweenSpawns)
	instance.maxNumEnemiesAlive = getValueVariablesArray(variables, "max_enemies_alive", instance.maxNumEnemiesAlive)
end

function onEntityCreated(CHandle)
	-- Get own handle.
	instance.ownHandle = CHandle
	
	-- Set the instance so it can be accessed.
	ScriptManager.publishScriptInstance(CHandle, instance)
end

function onSceneCreated()
end

function onUpdate(dt)
	instance:update(dt)
end

function onTriggerEnter(entity)
	instance:onTriggerEnter(entity)
end

function onTriggerExit(entity)
end

function onEnable()
end

function onDisable()
end

function onEnd()
	-- Set the instance so it can be accessed.
	ScriptManager.removeScriptInstance(instance.ownHandle)
end