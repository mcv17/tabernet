Doors = {}
Doors.__index = Doors

function Doors:new()
   local doors = {}             -- our new object
   setmetatable(doors, Doors)  -- make Doors handle lookup
   
   doors.ownHandle = nil -- Own handle
   doors.isRotating = false -- True when rotating.
   doors.rotationSign = 1.0 -- 1.0 when rotating one side (opening), -1.0 when rotating opposite (closing)
 
   -- Doors.
   doors.RightDoorName = ""
   doors.RightDoorHandle = nil
   doors.LeftDoorName = ""
   doors.LeftDoorHandle = nil
   doors.angularVelocityPerSecond = Vector3:new(0.0, 6.5, 0.0)
   doors.stopRotation = 90.0 * math.pi/180.0 -- Stops after rotating 90 degrees.
   
   return doors
end

function Doors:update(dt)
	if not self.isRotating then return end

	if not self.ownHandle:isValid() then return end

	local forwardVector = LogicManager:transformVector(Vector3:new(0,0,1), self.ownHandle:TCompTransform():getRotation())
	
	local currentYawDoorLeft = self.LeftDoorHandle:TCompTransform():getDeltaYawToAimTo(self.LeftDoorHandle:TCompTransform():getPosition() + forwardVector)
	local currentYawDoorRight = self.RightDoorHandle:TCompTransform():getDeltaYawToAimTo(self.RightDoorHandle:TCompTransform():getPosition() + forwardVector)

	-- Is opening.
	if (self.rotationSign == -1.0 and (currentYawDoorLeft >= self.stopRotation) and ((180.0 * math.pi/180.0  - currentYawDoorRight) >= self.stopRotation)) then
		self:StopDoors()
		return
	end
	
	-- Is closing. Stop when yaw is 0.0 again.
	if (self.rotationSign == 1.0 and (currentYawDoorLeft <= 0.0) and (currentYawDoorRight <= 0.0)) then
		self:StopDoors()
		return
	end
	
	
	self.LeftDoorHandle:TCompRigidBody():setAngularVelocity(self.angularVelocityPerSecond * self.rotationSign * dt, true)
	self.RightDoorHandle:TCompRigidBody():setAngularVelocity(self.angularVelocityPerSecond * self.rotationSign * dt * -1, true)
end

function Doors:fetchDoors()
	local group = self.ownHandle:TCompGroup()
	self.LeftDoorHandle = group:GetEntityInsideGroupByName(self.LeftDoorName)
	self.RightDoorHandle = group:GetEntityInsideGroupByName(self.RightDoorName)
end

function Doors:OpenDoors()
	self.isRotating = true
	self.rotationSign = -1.0
end

function Doors:CloseDoors()
	self.isRotating = true
	self.rotationSign = 1.0
end

function Doors:StopDoors()
	self.isRotating = false
	self.LeftDoorHandle:TCompRigidBody():setAngularVelocity(Vector3:new(0.0, 0.0, 0.0), true)
	self.RightDoorHandle:TCompRigidBody():setAngularVelocity(Vector3:new(0.0, 0.0, 0.0), true)
end

-- create and use an Doors
instance = Doors:new()

math.randomseed(os.time())

function onStart(variables)
	instance.RightDoorName = variables["rightDoorName"]
	instance.LeftDoorName = variables["leftDoorName"]
end

function onEntityCreated(CHandle)
	-- Get own handle.
	instance.ownHandle = CHandle
	
	-- Set the instance so it can be accessed.
	ScriptManager.publishScriptInstance(CHandle, instance)
end

function onSceneCreated()
	-- Get handles of the entities of the other two doors.
	instance:fetchDoors()
end

function onUpdate(dt)
	instance:update(dt)
end

function onEnd()
	-- Set the instance so it can be accessed.
	ScriptManager.removeScriptInstance(instance.ownHandle)
end