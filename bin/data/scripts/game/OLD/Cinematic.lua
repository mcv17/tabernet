
--[[ onEvent functions. They should have this specific signature, don't change.
This functions are called by the TCompScript when an event is recieved and they
will get sent to the given object. ]]

createdScene = false
duration = 15.0

-- Do anything with your instance here if you want.
-- This is recieved when the complete scene is created.
function onSceneCreated()
end

-- For now this.
function onTriggerEnter(entity)
	dbg("onTriggerEnter called")
	if entity:TCompTags():hasTag("player") then
		local handle = LogicManager:getOutputCamera()
		local camName = handle:TCompName().name
		
		-- Activate cinematic bars.
		LogicManager:activateCinematicBars(camName, true)
		LogicManager:setDefaultCamera("Camera-Cinematic")
		
		-- Don't let player move.
		LogicManager:setControllerEntityActive("Player", false)
		
	else
		dbg("Not player enter.")
	end
end

function onUpdate(dt)
	duration = duration - dt
	if duration < 0 then
		finishCinematic()
	end
end

function finishCinematic()
	local handle = LogicManager:getOutputCamera()
	local camName = handle:TCompName().name
	
	-- Activate cinematic bars.
	LogicManager:activateCinematicBars(camName, false)
	LogicManager:setDefaultCamera("Camera-Player-Output")
	
	-- Let player move.
	LogicManager:setControllerEntityActive("Player", true)
end