local ScriptHandle 

function onStart(variables)
end

function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function StartCameras()
	-- Set camera to use.
	LogicManager:setDefaultCamera("CameraThirdArena")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", true)
	
	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Start camera. Listen to event on camera end.
	local Camera = LogicManager:getEntityByName("CameraThirdArena")
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	Camera:TCompTransCurveController():start()
	LogicManager:BindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle, "EndCinematic")
end

function EndCinematic()
	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)
	
	-- Set inactive again.
	local Camera = LogicManager:getEntityByName("CameraThirdArena")
	Camera:TCompTransCurveController():stop()
	
	-- Return player control.
	LogicManager:setActivePlayerInput(true)
	
	-- Activate arena.
	local Arena = LogicManager:getEntityByName("ThirdArena")
	Arena:TCompArena():StartArena()

	-- Activate totems
	local Totem1 = getEntity("Totem004")
	local Totem2 = getEntity("Totem005")
	local Totem3 = getEntity("Totem006")
	local Totem4 = getEntity("Totem007")

	if Totem1:isValid() then
		LogicManager:activateTotem(Totem1)
	end
	if Totem2:isValid() then
		LogicManager:activateTotem(Totem2)
	end
	if Totem3:isValid() then
		LogicManager:activateTotem(Totem3)
	end
	if Totem4:isValid() then
		LogicManager:activateTotem(Totem4)
	end
	
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("CameraThirdArena")
	if Camera == nil or not (Camera:isValid()) then return end
		
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle)
end