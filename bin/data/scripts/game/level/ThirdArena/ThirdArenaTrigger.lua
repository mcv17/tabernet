local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	-- Play cinematic pointing to door opening and wave coming.
	LogicManager:StartCinematic("data/prefabs/cinematics/CinematicFinalArena.json");
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "RemoveDynamic", 5.00)	
end

function RemoveDynamic()
	-- Clear entities in previous volumes if there are any.
	local Entity = LogicManager:getEntityByName("CullingBox006")
	Entity:TCompSceneCullingVolume():RemoveDynamicEntitiesInVolume()
	
	-- Remove trigger.
	component:disableNextFrame()
end