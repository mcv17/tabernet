local S1H, S2H, S3H, S4H, S5H, S6H, S7H, S8H, S9H, S10H, S11H, S12H, S13H, S14H

local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function onSceneCreated()
	-- Fetch spawns.
	S1H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal020")
	S2H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal019")
	S3H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal018")
	S4H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal017")
	S5H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal016")
	S6H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal021")
	
	S7H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal")
	S8H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal002")
	S9H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal012")
	S10H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal014")
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	-- Spawn some enemies here.
	spawnMelee(S1H, true)
	spawnMelee(S2H, true)
	
	spawnRangedSniperRoof(S7H, true)
	spawnRangedSniperRoof(S8H, true)
	spawnRangedSniperRoof(S9H, true)
	spawnRangedSniperRoof(S10H, true)
	
	-- Remove trigger.
	component:disableNextFrame()
end