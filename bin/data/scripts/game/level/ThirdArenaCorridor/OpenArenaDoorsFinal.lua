local S1H, S2H, S3H, S4H, S5H, S6H, S7H, S8H, S9H, S10H

local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onSceneCreated()
	-- Fetch spawns.
	
	-- Enemies.
	S1H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal024")
	S2H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal023")
	S3H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal025")
	S4H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal022")
	
	-- Ranged
	S5H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal004")
	S6H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal008")
	
	-- Immortals.
	S7H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal028")
	S8H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal026")
	S9H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal029")
	S10H = LogicManager:getEntityByName("SpawnWallBeforeArenaFinal027")
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	-- Spawn some enemies here.
	spawnMelee(S1H, true)
	spawnMelee(S2H, true)
	spawnMelee(S3H, true)
	spawnMelee(S4H, true)

	spawnRangedSniperGround(S5H, true)
	spawnRangedSniperGround(S6H, true)

	spawnImmortal(S7H, true)
	spawnImmortal(S8H, true)
	spawnImmortal(S8H, true)
	spawnImmortal(S9H, true)
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "CheckIfImmortalsDied", 15)

	-- Remove trigger.
	component:disableNextFrame()
end

function CheckIfImmortalsDied()
	local DoorHandle = LogicManager:getEntityByName("door_coliseo016")
	if not DoorHandle:isValid() then return end
	
	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:OpenDoors()

	-- Play music
	LogicManager:setEventParameter("event:/Music/MusicExplore", "Explore", 0.0)
	LogicManager:playEvent("event:/Music/MusicArena")
	LogicManager:setEventParameter("event:/Music/MusicArena", "Arena", 1.0)
end