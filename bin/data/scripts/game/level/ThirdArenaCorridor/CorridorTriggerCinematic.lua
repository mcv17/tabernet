local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onTriggerEnter(entity)
	if alreadyActivated then return end
	if not entityHasTag(entity, "player") then return end
	
	local DoorHandle = LogicManager:getEntityByName("door_casadepaso033")
	if not DoorHandle:isValid() then return end
	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:OpenDoors()
	
	-- Don't play for now. Better this way.
	--LogicManager:StartCinematic("data/prefabs/cinematics/CinematicFinalArenaCorridor.json");
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "RemoveDynamic", 5.00)	
end

function RemoveDynamic()
	-- Clear entities in previous volumes if there are any.
	local Entity = LogicManager:getEntityByName("CullingBox005")
	if Entity:isValid() then
		Entity:TCompSceneCullingVolume():RemoveDynamicEntitiesInVolume()
	end
	
	-- Remove trigger.
	ownHandle:destroy()
end