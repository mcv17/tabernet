local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onCheckpointReached()
	LogicManager:setEventParameter("event:/Music/MusicHorde", "Default", 1.0)
end

local entitiesToReset = {
	"TriggerDoorSecondCheckpoint", "EntranceToArena2CombatTrigger", "SecondArenaTriggerIntro"
}
local doorsToClose = {
	"door_casadepaso029", "door_casadepaso030", "door_coliseo012", "door_coliseo013"
}
local totems = {
	"Totem000", "Totem001", "Totem002", "Totem003"
}
local arenaToReset = "EntranceToArena2Combat"
local arena2ToReset = "SecondArena"
function onCheckpointRestorePlayer()
	LogicManager:ClearBindTimedScriptEvents()
	LogicManager:removeEntitiesByTag("pickup")

	arenaEntity = getEntity(arenaToReset)
	arenaEntity:TCompArena():ResetArena()
	arenaEntity = getEntity(arena2ToReset)
	arenaEntity:TCompArena():ResetArena()

	LogicManager:disablePoolingEnemies()
	
	for _, entityToReset in ipairs(entitiesToReset) do
		LogicManager:setEntityActive(entityToReset, false)
		LogicManager:setEntityActive(entityToReset, true)
	end
	for _, doorName in ipairs(doorsToClose) do
		door = getEntity(doorName):TCompDoor()
		door:ResetDoors()
	end
	for _, totemName in ipairs(totems) do
		getEntity(totemName):TCompTotem():reset()
	end
end