function onCheckpointReached()
end

local entitiesToReset = {
	"CinematicSanctuaryExit", "CorridorWaveFirstAppear", "CorridorWaveSecondAppear", "CorridorWaveThirdAppear",
	"TriggerFirstImmortal", "TriggerFirstImmortalCloseDoor", "TriggerImmortalFirstEncounter"
}
local doorsToClose = {
	"door_casadepaso026", "door_casadepaso027", "door_casadepaso028"
}
local arenaToReset = "ArenaIntro"
function onCheckpointRestorePlayer()
	LogicManager:ClearBindTimedScriptEvents()
	LogicManager:removeEntitiesByTag("pickup")

	LogicManager:disablePoolingEnemies()
	
	-- Remove immortal ragdoll
	LogicManager:smartRemoveEntity("ImmortalFirstTriggerToCheck")

	for _, entityToReset in ipairs(entitiesToReset) do
		LogicManager:setEntityActive(entityToReset, false)
		LogicManager:setEntityActive(entityToReset, true)
	end
	for _, doorName in ipairs(doorsToClose) do
		door = getEntity(doorName):TCompDoor()
		door:ResetDoors()
	end
end