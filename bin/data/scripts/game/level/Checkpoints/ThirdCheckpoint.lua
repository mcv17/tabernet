
local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

local spawnNames = {
	"spawn_14_group1_1", "spawn_14_group1_2", "spawn_14_group1_3", "spawn_14_group1_4",
	"spawn_14_group2_1",
	"spawn_14_group3_1", "spawn_14_group3_2", "spawn_14_group3_3"
}
function onTriggerEnter(entity)
	if entityHasTag(entity, "player") then
		for _, spawnName in ipairs(spawnNames) do
			spawn = getEntity(spawnName):TCompSpawn()
			spawn:SpawnEntityInstant(false)
		end
		
		component:disableNextFrame()
	end
end

function onCheckpointReached()
end

local entitiesToReset = {
	"CheckpointThirdDoor", "SecondFightTrigger", "ImmortalsFight", "TriggerImmortalsFight", 
	"SecondFightExitTrigger"
}
local doorsToClose = {
	"door_casadepaso031", "door_casadepaso032", "door_casadepaso033"
}
function onCheckpointRestorePlayer()
	LogicManager:ClearBindTimedScriptEvents()
	LogicManager:removeEntitiesByTag("pickup")

	LogicManager:disablePoolingEnemies()

	LogicManager:setEntityActive(ownHandle, false)
	LogicManager:setEntityActive(ownHandle, true)
	for _, entityToReset in ipairs(entitiesToReset) do
		LogicManager:setEntityActive(entityToReset, false)
		LogicManager:setEntityActive(entityToReset, true)
	end
	for _, doorName in ipairs(doorsToClose) do
		door = getEntity(doorName):TCompDoor()
		door:ResetDoors()
	end
end