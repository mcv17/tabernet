function onCheckpointReached()
	-- Play music
	LogicManager:setEventParameter("event:/Music/MusicBattle", "Battle", 0.0)
	LogicManager:playEvent("event:/Music/MusicExplore")
	LogicManager:setEventParameter("event:/Music/MusicExplore", "Explore", 1.0)

end

local entitiesToReset = {
	"OpenArenaDoorsFinalStart", "OpenArenaDoorsFinalSecond", "OpenArenaDoorsFinal", "ThirdArenaTrigger"
}
local doorsToClose = {
	"door_coliseo016"
}
local arenaToReset = "ThirdArena"
function onCheckpointRestorePlayer()
	LogicManager:ClearBindTimedScriptEvents()
	LogicManager:removeEntitiesByTag("pickup")

	arenaEntity = getEntity(arenaToReset)
	arenaEntity:TCompArena():ResetArena()

	LogicManager:disablePoolingEnemies()

	for _, entityToReset in ipairs(entitiesToReset) do
		LogicManager:setEntityActive(entityToReset, false)
		LogicManager:setEntityActive(entityToReset, true)
	end
	for _, doorName in ipairs(doorsToClose) do
		door = getEntity(doorName):TCompDoor()
		door:ResetDoors()
	end
end