PerceptionTrigger = {}
PerceptionTrigger.__index = PerceptionTrigger

function PerceptionTrigger:new()
   local instance = {}             -- our new object
   setmetatable(instance, PerceptionTrigger)  -- make instance handle lookup
   
   instance.ownHandle = nil -- Own handle
   instance.ScriptHandle = nil -- Script handle
   instance.Activated = false
   instance.HasPerceptionBeenActivated = false
   
   return instance
end

function PerceptionTrigger:OnEntityCreated(CHandle)
	-- Get own handle.
	self.ownHandle = CHandle
	
	-- Get own handle.
	self.ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Set the instance so it can be accessed.
	ScriptManager.publishScriptInstance(CHandle, self)
end

function PerceptionTrigger:onEnable()
	self.Activated = false
	self.HasPerceptionBeenActivated = false
end

function PerceptionTrigger:onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	if not self.Activated then return end

	LogicManager:BindTimedScriptEvent(self.ScriptHandle, "CheckIfPerception", 0.20)
end

function PerceptionTrigger:ActivatePerceptionTrigger()
	self.Activated = true
end

local instance = PerceptionTrigger:new()

function onEntityCreated(CHandle)
	instance:OnEntityCreated(CHandle)
end

function onEnable()
	instance:onEnable()
end

function onTriggerEnter(entity)
	instance:onTriggerEnter(entity)

	-- Remove trigger.
	component:disableNextFrame()
end

function onEnd()
	-- Set the instance so it can be accessed.
	ScriptManager.removeScriptInstance(instance.ownHandle)
end

function CheckIfPerception()
	local Player = LogicManager:getEntityByName("Player")
	if Player:TCompPerception():isPerceptionActive() then
		instance.HasPerceptionBeenActivated = true
		OpenDoor()
		return
	end

	LogicManager:BindTimedScriptEvent(instance.ScriptHandle, "CheckIfPerception", 0.20)
end

function OpenDoor()
	local DoorHandle = LogicManager:getEntityByName("door_casadepaso024")
	if not DoorHandle:isValid() then return end
	
	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:OpenDoors()
end