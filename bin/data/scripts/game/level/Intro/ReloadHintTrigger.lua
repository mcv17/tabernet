ReloadTrigger = {}
ReloadTrigger.__index = ReloadTrigger

function ReloadTrigger:new()
   local instance = {}             -- our new object
   setmetatable(instance, ReloadTrigger)  -- make instance handle lookup
   
   instance.ownHandle = nil -- Own handle
   instance.ScriptHandle = nil -- Script handle
   
   -- Enemies.
   instance.EnemySpawned = nil
   instance.EnemyToKill1 = nil
   instance.EnemyToKill2 = nil
   
   return instance
end

function ReloadTrigger:onEntityCreated(CHandle)
	-- Get own handle.
	self.ownHandle = CHandle
	
	-- Get own handle.
	self.ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Set the instance so it can be accessed.
	ScriptManager.publishScriptInstance(CHandle, self)
end

function ReloadTrigger:onEnable(CHandle)
	self.EnemyToKill1 = nil
	self.EnemyToKill2 = nil
	self.EnemySpawned = false
end

function ReloadTrigger:onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	if self.EnemySpawned then return end
	self.EnemySpawned = true

	LogicManager:BindTimedScriptEvent(self.ScriptHandle, "CheckIfEnemyDead", 0.20)
end

function ReloadTrigger:SpawnEnemies()
	-- Spawn.
	local S1H = LogicManager:getEntityByName("SpawnRangedIntro")
	self.EnemyToKill1 = S1H:TCompSpawn():SpawnEntityInstant("EnemyRanged", false)
	self.EnemyToKill1:TCompHealth():setCurrentHealth(1)
	
	-- Spawn.
	local S2H = LogicManager:getEntityByName("SpawnRangedIntro2")
	self.EnemyToKill2 = S2H:TCompSpawn():SpawnEntityInstant("EnemyRanged", false)
	self.EnemyToKill2:TCompHealth():setCurrentHealth(1)
end

function ReloadTrigger:CheckIfEnemiesDied()
	if self.EnemyToKill1 == nil and self.EnemyToKill2 == nil then
		self:OpenDoor()
		return true
	end

	if not(self.EnemyToKill1 == nil) and self.EnemyToKill1:isValid() then
		if self.EnemyToKill1:TCompHealth():isDead() then
			self.EnemyToKill1 = nil
		end
	end
	
	if not(self.EnemyToKill2 == nil) and self.EnemyToKill2:isValid() then
		if self.EnemyToKill2:TCompHealth():isDead() then
			self.EnemyToKill2 = nil
		end
	end
	
	return false
end

function ReloadTrigger:OpenDoor()
	local DoorHandle = LogicManager:getEntityByName("door_casadepaso023")
	if not DoorHandle:isValid() then return end
	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:OpenDoors()
	
	-- TriggerHint04.
	local TriggerHint04 = LogicManager:getEntityByName("TriggerHint04")
	if not TriggerHint04:isValid() then return end
	local ScytheHintTrigger = ScriptManager.getRegisteredScriptInstance(TriggerHint04)
	ScytheHintTrigger:getScriptInstance():SpawnEnemiesForScytheTutorial()
	
	-- Deactivate trigger.
	LogicManager:setEntityActive(self.ownHandle, false)
end

local instance = ReloadTrigger:new()

function onEntityCreated(CHandle)
	instance:onEntityCreated(CHandle)
end

function onEnable()
	instance:onEnable()
end

function onTriggerEnter(entity)
	instance:onTriggerEnter(entity)
end

function CheckIfEnemyDead()
	local Died = instance:CheckIfEnemiesDied()
	if not Died then
		LogicManager:BindTimedScriptEvent(instance.ScriptHandle, "CheckIfEnemyDead", 0.20)
	end
end

function onEnd()
	-- Set the instance so it can be accessed.
	ScriptManager.removeScriptInstance(instance.ownHandle)
end