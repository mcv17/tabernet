ScytheTrigger = {}
ScytheTrigger.__index = ScytheTrigger

function ScytheTrigger:new()
   local instance = {}             -- our new object
   setmetatable(instance, ScytheTrigger)  -- make instance handle lookup
   
   instance.ownHandle = nil -- Own handle
   instance.ScriptHandle = nil -- Script handle
   
   -- Enemies.
   instance.EnemySpawned = nil
   instance.EnemyToKill1 = nil
   instance.EnemyToKill2 = nil
   
   return instance
end

function ScytheTrigger:OnEntityCreated(CHandle)
	-- Get own handle.
	self.ownHandle = CHandle
	
	-- Get own handle.
	self.ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Set the instance so it can be accessed.
	ScriptManager.publishScriptInstance(CHandle, self)
end

function ScytheTrigger:OnEnable()
	self.EnemySpawned = false
	self.EnemyToKill1 = nil
	self.EnemyToKill2 = nil
end

function ScytheTrigger:onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	if self.EnemySpawned then return end
	self.EnemySpawned = true
	
	LogicManager:BindTimedScriptEvent(self.ScriptHandle, "CheckIfEnemyDead", 0.20)
end

function ScytheTrigger:CheckIfEnemyDead(entity)
	if self.EnemyToKill1 == nil and self.EnemyToKill2 == nil then
		-- TriggerHint05.
		local TriggerHint05 = LogicManager:getEntityByName("TriggerHint05")
		if not TriggerHint05:isValid() then return end
		local PerceptionHintTrigger = ScriptManager.getRegisteredScriptInstance(TriggerHint05)
		PerceptionHintTrigger:getScriptInstance():ActivatePerceptionTrigger()
		return
	end

	if not(self.EnemyToKill1 == nil) and self.EnemyToKill1:isValid() then
		if self.EnemyToKill1:TCompHealth():isDead() then
			self.EnemyToKill1 = nil
		end
	end
	
	if not(self.EnemyToKill2 == nil) and self.EnemyToKill2:isValid() then
		if self.EnemyToKill2:TCompHealth():isDead() then
			self.EnemyToKill2 = nil
		end
	end
	
	LogicManager:BindTimedScriptEvent(self.ScriptHandle, "CheckIfEnemyDead", 0.20)
end

function ScytheTrigger:SpawnEnemiesForScytheTutorial()
	local SpawnScytheEnemy1 = LogicManager:getEntityByName("SpawnEnemyScythe1")
	if SpawnScytheEnemy1:isValid() then
		local STSE1 = SpawnScytheEnemy1:TCompSpawn():GetSpawnTransform()
		local STSE1Pos = STSE1:getPosition()
		local angles = STSE1:getYawPitchRoll()
		
		STSE1Pos.y = STSE1Pos.y - 0.05
		self.EnemyToKill1 = LogicManager:createEntity("data/prefabs/enemies/enemyMeleeTutorialCrouch.json", STSE1Pos, angles)
	end

	local SpawnScytheEnemy2 = LogicManager:getEntityByName("SpawnEnemyScythe2")
	if SpawnScytheEnemy2:isValid() then
		local STSE2 = SpawnScytheEnemy2:TCompSpawn():GetSpawnTransform()
		local STSE2Pos = STSE2:getPosition()
		local angles = STSE2:getYawPitchRoll()
		
		STSE2Pos.y = STSE2Pos.y - 0.05
		self.EnemyToKill2 = LogicManager:createEntity("data/prefabs/enemies/enemyMeleeTutorialCrouch.json", STSE2Pos, angles)
	end
end

local instance = ScytheTrigger:new()

function onEntityCreated(CHandle)
	instance:OnEntityCreated(CHandle)
end

function onEnable()
	instance:OnEnable()
end

function onTriggerEnter(entity)
	instance:onTriggerEnter(entity)
end

function CheckIfEnemyDead()
	instance:CheckIfEnemyDead()
end

function onEnd()
	-- Set the instance so it can be accessed.
	ScriptManager.removeScriptInstance(instance.ownHandle)
end