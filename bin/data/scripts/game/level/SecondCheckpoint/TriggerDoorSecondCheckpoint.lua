local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end

	local DoorHandle = LogicManager:getEntityByName("door_casadepaso029")
	if not DoorHandle:isValid() then return end
	
	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:OpenDoors()
	
	local DoorHandle2 = LogicManager:getEntityByName("door_casadepaso030")
	if not DoorHandle2:isValid() then return end
	
	local DoorComp2 = DoorHandle2:TCompDoor()
	DoorComp2:OpenDoors()
	
	local DoorHandle3 = LogicManager:getEntityByName("door_casadepaso028")
	if not DoorHandle3:isValid() then return end
	
	local DoorComp3 = DoorHandle3:TCompDoor()
	DoorComp3:CloseDoors()
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "RemoveDynamic", 5.00)	
end

function RemoveDynamic()
	-- Clear entities in previous volumes if there are any.
	local Entity = LogicManager:getEntityByName("CullingBoxArea2")
	if Entity:isValid() then
		Entity:TCompSceneCullingVolume():RemoveDynamicEntitiesInVolume()
	end

	-- Remove trigger.
	LogicManager:setEntityActive(ownHandle, false)
end