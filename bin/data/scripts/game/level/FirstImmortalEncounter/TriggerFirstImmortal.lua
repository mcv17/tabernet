local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onTriggerEnter(entity)
	if entityHasTag(entity, "inmortal") then
		-- Can't copy the entity handle because it seems to be null afterwards, we rename the entity as a hack to allow us toa access it.
		entity:TCompName():changeName("ImmortalFirstTriggerToCheck")
		LogicManager:alertEnemy(entity)
		
		LogicManager:BindTimedScriptEvent(ScriptHandle, "CheckIfAlive", 0.25)
	end
end

function CheckIfAlive()
	local ImmortalHandle = LogicManager:getEntityByName("ImmortalFirstTriggerToCheck")
	if not ImmortalHandle:isValid() then return end
	
	if ImmortalHandle:TCompHealth():isDead() then
		OpenDoorToNextArea()
		LogicManager:setEventParameter("event:/Music/MusicExplore", "Explore", 0.0)
		LogicManager:playEvent("event:/Music/MusicDefault")
		LogicManager:setEventParameter("event:/Music/MusicDefault", "Default", 1.0)
	else
		LogicManager:BindTimedScriptEvent(ScriptHandle, "CheckIfAlive", 0.25)
	end
end

function OpenDoorToNextArea()
	local DoorHandle = LogicManager:getEntityByName("door_casadepaso028")
	if not DoorHandle:isValid() then return end
	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:OpenDoors()
	
	-- Remove trigger.
	LogicManager:setEntityActive(ownHandle, false)
end