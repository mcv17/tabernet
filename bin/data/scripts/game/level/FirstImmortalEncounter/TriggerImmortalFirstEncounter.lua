local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	-- Play cinematic pointing to door opening and wave coming.
	LogicManager:StartCinematic("data/prefabs/cinematics/CinematicImmortal.json");

	-- Remove trigger.
	component:disableNextFrame()
end