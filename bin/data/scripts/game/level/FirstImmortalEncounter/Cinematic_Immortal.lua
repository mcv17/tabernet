local ScriptHandle

function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function StartCameras()
	-- Set camera to use.
	LogicManager:setDefaultCamera("Camera-First-Immortal")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", true)
	
	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Listen to event on cinematic end.
	local Camera = LogicManager:getEntityByName("Camera-First-Immortal")
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	
	Camera:TCompTransCurveController():start()
	LogicManager:BindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle, "SpawnImmortal")
end

function SpawnImmortal()
	-- Spawn.
	local S1H = LogicManager:getEntityByName("SpawnFirstImmortal")
	spawnImmortal(S1H)
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "onCinematicEnded", 4.50)
end

function onCinematicEnded()
	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)
	
	-- Set inactive again.
	local Camera = LogicManager:getEntityByName("Camera-First-Immortal")
	Camera:TCompTransCurveController():stop()
	
	-- Return player control.
	LogicManager:setActivePlayerInput(true)
	
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("Camera-First-Immortal")
	if Camera == nil or not (Camera:isValid()) then return end
	
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle)
end