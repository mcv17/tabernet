local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	-- Play cinematic pointing to door opening and wave coming.
	--LogicManager:StartCinematic("data/prefabs/cinematics/CinematicSecondArena.json");

	-- Play music
	LogicManager:setEventParameter("event:/Music/MusicBattle", "Battle", 0.0)
	LogicManager:playEvent("event:/Music/MusicArena")
	LogicManager:setEventParameter("event:/Music/MusicArena", "Arena", 1.0)
	
	-- Activate arena.
	local Arena = LogicManager:getEntityByName("SecondArena")
	Arena:TCompArena():StartArena()

	-- Activate totems
	local Totem1 = getEntity("Totem000")
	local Totem2 = getEntity("Totem001")
	local Totem3 = getEntity("Totem002")
	local Totem4 = getEntity("Totem003")
	if Totem1:isValid() then
		LogicManager:activateTotem(Totem1)
	end
	if Totem2:isValid() then
		LogicManager:activateTotem(Totem2)
	end
	if Totem3:isValid() then
		LogicManager:activateTotem(Totem3)
	end
	if Totem4:isValid() then
		LogicManager:activateTotem(Totem4)
	end
	-- Remove trigger.
	component:disableNextFrame()
end