local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end

	local ArenaHandle = LogicManager:getEntityByName("EntranceToArena2Combat")
	if not ArenaHandle:isValid() then return end
	
	ArenaHandle:TCompArena():StartArena()
	-- Play Music
	LogicManager:setEventParameter("event:/Music/MusicDefault", "Default", 0.0)
	LogicManager:playEvent("event:/Music/MusicBattle")
	LogicManager:setEventParameter("event:/Music/MusicBattle", "Battle", 1.0)
	
	-- Remove trigger.
	component:disableNextFrame()
end