local ScriptHandle 

function onStart(variables)

end

function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function StartCameras()
	-- Set camera to use.
	LogicManager:setDefaultCamera("CameraSecondArena")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", true)
	
	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Start camera. Listen to event on camera end.
	local Camera = LogicManager:getEntityByName("CameraSecondArena")
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	Camera:TCompTransCurveController():start()
	LogicManager:BindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle, "EndCinematic")
end

function EndCinematic()
	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)
	
	-- Set inactive again.
	local CameraSanctuary = LogicManager:getEntityByName("CameraSecondArena")
	CameraSanctuary:TCompTransCurveController():stop()
	
	-- Return player control.
	LogicManager:setActivePlayerInput(true)
	
	-- Activate arena.
	local Arena = LogicManager:getEntityByName("SecondArena")
	Arena:TCompArena():StartArena()
	
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("CameraSecondArena")
	if Camera == nil or not (Camera:isValid()) then return end

	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle)
end