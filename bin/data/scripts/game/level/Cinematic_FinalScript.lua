local ScriptHandle 
function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function StartCameras()
	LogicManager:BindTimedScriptEvent(ScriptHandle, "StartCinematic", 3.00)
end

function StartCinematic()
	-- Set camera to use.
	LogicManager:setDefaultCamera("FinalCameraWithShaker")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", true)
	
	-- Disable hud
	LogicManager:deactivateWidget("hud")

	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Start camera. Listen to event on camera end.
	local Camera = LogicManager:getEntityByName("FinalCamera")
	local CurveHandle = Camera:TCompCameraLookat():asCHandle()
	Camera:TCompCameraLookat():setActive(true)
	LogicManager:BindScriptToComponentEvent(CurveHandle, "OnTargetReached", ScriptHandle, "EndCinematic")
	
	-- Disable arena music
	LogicManager:setEventParameter("event:/Music/MusicArena", "Arena", 0.0)
	-- Play sound cinematic ending.
	local PlayEventPositionEntity = LogicManager:getEntityByName("FinalLookAt")
	if PlayEventPositionEntity and PlayEventPositionEntity:isValid() then
		if PlayEventPositionEntity:TCompTransform() then
			local PlayEventPosition = PlayEventPositionEntity:TCompTransform():getPosition()
			LogicManager:playEvent("event:/Ambience/DemonCinematic", PlayEventPosition)
		end
	end
	
	-- Bind time for the steps of the sound event. 
	LogicManager:BindTimedScriptEvent(ScriptHandle, "StepTrauma", 4.06)
	LogicManager:BindTimedScriptEvent(ScriptHandle, "StepTrauma", 6.7)
	LogicManager:BindTimedScriptEvent(ScriptHandle, "StepTrauma", 8.8)
	LogicManager:BindTimedScriptEvent(ScriptHandle, "StepTrauma", 10.9)
	LogicManager:BindTimedScriptEvent(ScriptHandle, "ShoutTrauma", 13.0)
end

local CurrentStep = 0
function StepTrauma()
	CurrentStep = CurrentStep + 1
	local ShakeDelta = 0.085 * (CurrentStep)
	LogicManager:sendDeltaTrauma("FinalCameraWithShaker", ShakeDelta)
end

function ShoutTrauma()
	LogicManager:sendDeltaTrauma("FinalCameraWithShaker", 0.2)
end

function EndCinematic()
	-- Set inactive again.
	local Camera = LogicManager:getEntityByName("FinalCamera")
	Camera:TCompCameraLookat():setActive(false)
	
	LogicManager:PlayUIAnimation("black_screen", "black_screen_image", "FinalBlackScreenFadeIn", false, 0.0)
	LogicManager:BindTimedScriptEvent(ScriptHandle, "FinishCinematic", 5.00)
end

function FinishCinematic()
	-- Show credits.
	LogicManager:setBlackScreenAlpha(0.0)

	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)
	
	-- Activate the credits widget and start its animation.
	LogicManager:activateWidget("credits")
	LogicManager:PlayUIAnimation("credits", "credits_image", "CreditsAnimation", false, 0.0)
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "FinishCredits", 40)
	-- LogicManager:BindTimedScriptEvent(ScriptHandle, "ActivateCreditsMusic", 3)
	LogicManager:playEvent("event:/Music/MusicBattle")
	LogicManager:setEventParameter("event:/Music/MusicBattle", "Battle", 1.0)
end

function FinishCredits()
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
	LogicManager:deactivateWidget("hud") --EndCinematic enables the hud

	-- Disable credits so loading screen can be seen.
	LogicManager:deactivateWidget("credits")

	LogicManager:stopEvent("event:/Music/MusicBattle")

	-- Finish game.
	LogicManager:ChangeToMenuState()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("FinalCamera")
	if Camera == nil or not (Camera:isValid()) then return end
	
	local CurveHandle = Camera:TCompCameraLookat():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "OnTargetReached", ScriptHandle)
end