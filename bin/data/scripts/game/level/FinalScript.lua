local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function StartCameras()
	-- Play final cinematic.
	LogicManager:StartCinematic("data/prefabs/cinematics/CinematicFinal.json");
end