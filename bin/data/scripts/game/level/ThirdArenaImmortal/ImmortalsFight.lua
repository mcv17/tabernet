
local spawnNamesWave = {
	"spawn_15_group3_1", 
	"spawn_15_group3_2", 
	"spawn_15_group3_3", 
	"spawn_15_group3_4", 
	"spawn_15_group3_5", 
	"spawn_15_group3_6"
}
function onEntityCreated(CHandle)
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	for _, spawnName in ipairs(spawnNamesWave) do
		spawn = getEntity(spawnName):TCompSpawn()
		enemy = spawn:SpawnEntityInstant()
		LogicManager:alertEnemy(enemy)
	end

	component:disableNextFrame()
end