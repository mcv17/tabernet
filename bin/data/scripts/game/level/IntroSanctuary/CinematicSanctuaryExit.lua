local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()

end

local alreadyActivated = false
function onTriggerEnter(entity)
	if alreadyActivated then
		-- Play cinematic pointing to door opening and wave coming.
		LogicManager:StartCinematic("data/prefabs/cinematics/CinematicSanctuaryExit.json");
		component:disableNextFrame()
	else
		if not entityHasTag(entity, "player") then return end
		SanctuaryHandle = LogicManager:getEntityByName("santu")
		if not SanctuaryHandle:isValid() then return end

	
		local SanctuaryComp = SanctuaryHandle:TCompSanctuary()
		if SanctuaryComp == nil then return end

		if SanctuaryComp:HasBeenActivated() then
			-- Play cinematic pointing to door opening and wave coming.
			LogicManager:StartCinematic("data/prefabs/cinematics/CinematicSanctuaryExit.json");

			-- Play music
			LogicManager:setEventParameter("event:/Music/MusicDefault", "Default", 0.0)
			LogicManager:BindTimedScriptEvent(ScriptHandle, "playHordeMusic", 2.0)
			
			-- Remove trigger.
			component:disableNextFrame()

			alreadyActivated = true
		end
	end
end

function playHordeMusic()
	LogicManager:playEvent("event:/Music/MusicHorde")
	LogicManager:setEventParameter("event:/Music/MusicHorde", "Horde", 1.0)
end