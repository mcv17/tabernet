local ScriptHandle

function onStart(variables)

end

function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function onUpdate(dt)
end

function StartCameras()
	-- Set camera to use.
	LogicManager:setDefaultCamera("Camera-Door-Sanctuary")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", true)
	
	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Listen to event on cinematic end.
	local Camera = LogicManager:getEntityByName("Camera-Door-Sanctuary")
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	
	Camera:TCompTransCurveController():start()
	LogicManager:BindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle, "OpenDoor")
	
end

function OpenDoor()
	-- Open the door.
	local WoodHandle = LogicManager:getEntityByName("wooddoor")
	if not WoodHandle:isValid() then return end
	WoodHandle:TCompDoor():OpenDoors()
	
	-- Listen to event on cinematic end.
	LogicManager:BindTimedScriptEvent(ScriptHandle, "onCinematicEnded", 5.0)
end

function onCinematicEnded()
	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)
	
	-- Set inactive again.
	local Camera = LogicManager:getEntityByName("Camera-Door-Sanctuary")
	Camera:TCompTransCurveController():stop()
	
	-- Return player control.
	LogicManager:setActivePlayerInput(true)
		
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("Camera-Door-Sanctuary")
	if Camera == nil or not (Camera:isValid()) then return end
		
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle)
end