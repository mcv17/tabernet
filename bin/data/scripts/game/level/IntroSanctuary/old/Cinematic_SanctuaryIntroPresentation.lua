local ScriptHandle 

function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function StartCameras()
	-- Set camera to use.
	LogicManager:setDefaultCamera("Camera-Sanctuary-Intro")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", true)
	
	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Start camera. Listen to event on camera end.
	local Camera = LogicManager:getEntityByName("Camera-Sanctuary-Intro")
	local CurveHandle = Camera:TCompCameraLookat():asCHandle()
	Camera:TCompCameraLookat():setActive(true)
	LogicManager:BindScriptToComponentEvent(CurveHandle, "OnTargetReached", ScriptHandle, "onCinematicEnded")
	
end

function onCinematicEnded()
	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)
	
	-- Set inactive again.
	local Camera = LogicManager:getEntityByName("Camera-Sanctuary-Intro")
	Camera:TCompCameraLookat():setActive(false)
	
	-- Return player control.
	LogicManager:setActivePlayerInput(true)
	
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("Camera-Sanctuary-Intro")
	if Camera == nil or not (Camera:isValid()) then return end

	local CurveHandle = Camera:TCompCameraLookat():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "OnTargetReached", ScriptHandle)
end