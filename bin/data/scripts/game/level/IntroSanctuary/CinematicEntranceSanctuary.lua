local ownHandle

function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	-- Play cinematic pointing to door opening of sanctuary.
	LogicManager:StartCinematic("data/prefabs/cinematics/CinematicEntranceSanctuary.json");
	
	-- Remove trigger.
	component:disableNextFrame()
end