local ScriptHandle


local S1H, S2H, S3H, S4H, S5H, S6H, S7H, S8H, S9H, S10H, S11H, S12H, S13H, S14H, S15H, S16H, S17H
function onStart()
	-- Fetch spawns.
	S1H = LogicManager:getEntityByName("SpawnWave")
	S2H = LogicManager:getEntityByName("SpawnWave2")
	S3H = LogicManager:getEntityByName("SpawnWave3")
	S4H = LogicManager:getEntityByName("SpawnWave4")
	S5H = LogicManager:getEntityByName("SpawnWave5")
	S6H = LogicManager:getEntityByName("SpawnWave6")
	S7H = LogicManager:getEntityByName("SpawnWave7")
	S8H = LogicManager:getEntityByName("SpawnWave8")
	S9H = LogicManager:getEntityByName("SpawnWave9")
	S10H = LogicManager:getEntityByName("SpawnWave10")
	S11H = LogicManager:getEntityByName("SpawnWave11")
	S12H = LogicManager:getEntityByName("SpawnWave12")
	S13H = LogicManager:getEntityByName("SpawnWave13")
	S14H = LogicManager:getEntityByName("SpawnWave14")
	S15H = LogicManager:getEntityByName("SpawnWave15")
	S16H = LogicManager:getEntityByName("SpawnWave16")
	S17H = LogicManager:getEntityByName("SpawnWave17")
end

function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function StartCameras()
	-- Set camera to use.
	LogicManager:setDefaultCamera("Camera-Door-Wave")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", true)
	
	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Start camera. Listen to event on camera end.
	local Camera = LogicManager:getEntityByName("Camera-Door-Wave")
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	
	Camera:TCompTransCurveController():start()
	LogicManager:BindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle, "onCinematicEnded")
	LogicManager:BindTimedScriptEvent(ScriptHandle, "OpenDoor", 0.50)
	LogicManager:BindTimedScriptEvent(ScriptHandle, "SpawnEnemies", 3.5)
end

function OpenDoor()
	-- Open the door.
	local WoodHandle = LogicManager:getEntityByName("door_casadepaso026")
	if not WoodHandle:isValid() then return end
	WoodHandle:TCompDoor():OpenDoors()
end

function SpawnEnemies()
	-- Spawn wave.
	spawnMelee(S1H, true)
	spawnMelee(S2H, true)
	spawnMelee(S3H, true)
	spawnMelee(S4H, true)
	spawnMelee(S5H, true)
	spawnMelee(S6H, true)
	spawnMelee(S7H, true)
	spawnMelee(S8H, true)
	spawnMelee(S9H, true)
	spawnMelee(S10H, true)
end

function onCinematicEnded()
	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)
	
	-- Set inactive again.
	local Camera = LogicManager:getEntityByName("Camera-Door-Wave")
	Camera:TCompTransCurveController():stop()
	
	-- Return player control.
	LogicManager:setActivePlayerInput(true)
		
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("Camera-Door-Wave")
	if Camera == nil or not (Camera:isValid()) then return end
		
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle)
end