local S1H, S2H, S3H, S4H, S5H, S6H, S7H
local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function onSceneCreated()
	-- Fetch spawns.
	S1H = LogicManager:getEntityByName("SpawnWave30")
	S2H = LogicManager:getEntityByName("SpawnWave31")
	S3H = LogicManager:getEntityByName("SpawnWave32")
	S4H = LogicManager:getEntityByName("SpawnWave29")
	S5H = LogicManager:getEntityByName("SpawnWave28")
	S6H = LogicManager:getEntityByName("SpawnCorridor2")
	S7H = LogicManager:getEntityByName("SpawnCorridor3")
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	-- Spawn some enemies.
	spawnMelee(S1H, true)
	spawnMelee(S2H, true)
	spawnMelee(S3H, true)
	spawnMelee(S4H, true)
	spawnMelee(S5H, true)
	spawnRangedSniperRoof(S6H, true)
	spawnRangedSniperRoof(S7H, true)

	-- Remove trigger.
	component:disableNextFrame()
end