local S1H, S2H, S3H, S4H, S5H, S6H, S7H, S8H, S9H, S10H, S11H, S12H, S13H, S14H, S15H, S16H

local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function onSceneCreated()
	-- Fetch spawns.
	S1H = LogicManager:getEntityByName("SpawnCorridor11")
	S2H = LogicManager:getEntityByName("SpawnCorridor12")
	S3H = LogicManager:getEntityByName("SpawnCorridor13")
	S4H = LogicManager:getEntityByName("SpawnCorridor14")
	S5H = LogicManager:getEntityByName("SpawnCorridor10")
	S6H = LogicManager:getEntityByName("SpawnCorridor20")

	S7H = LogicManager:getEntityByName("SpawnWave13")
	S8H = LogicManager:getEntityByName("SpawnWave12")
	S9H = LogicManager:getEntityByName("SpawnWave14")
	S10H = LogicManager:getEntityByName("SpawnWave16")
	S11H = LogicManager:getEntityByName("SpawnWave15")
	S12H = LogicManager:getEntityByName("SpawnWave20")
	S13H = LogicManager:getEntityByName("SpawnWave21")
	S14H = LogicManager:getEntityByName("SpawnWave11")
	S15H = LogicManager:getEntityByName("SpawnWave22")
	S16H = LogicManager:getEntityByName("SpawnWave24")
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end

	-- Spawn some enemies.
	spawnRangedSniperRoof(S1H, true)
	spawnRangedSniperRoof(S2H, true)
	spawnRangedSniperRoof(S3H, true)
	spawnRangedSniperRoof(S4H, true)
	--spawnRangedSniperRoof(S5H)
	--spawnRangedSniperRoof(S6H)
	
	spawnMelee(S7H, true)
	spawnMelee(S8H, true)
	spawnMelee(S9H, true)
	spawnMelee(S10H, true)
	spawnMelee(S11H, true)
	spawnMelee(S12H, true)
	spawnMelee(S13H, true)
	--spawnMelee(S14H)
	--spawnMelee(S15H)
	--spawnMelee(S16H)
	
	-- Open the door.
	local DoorHandle = LogicManager:getEntityByName("door_casadepaso027")
	if not DoorHandle:isValid() then return end
	
	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:OpenDoors()
	
	-- Remove trigger.
	component:disableNextFrame()
end