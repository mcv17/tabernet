local ownHandle
local ScriptHandle
local Activated = false
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onEnable()
	Activated = false
	ResetSun()
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	if Activated then return end
	Activated = true
	
	-- Get sun variables.
	FetchSun()

	-- Play music
	LogicManager:setEventParameter("event:/Music/MusicDefault", "Default", 0.0)
	LogicManager:playEvent("event:/Music/MusicBattle")
	LogicManager:setEventParameter("event:/Music/MusicBattle", "Battle", 1.0)
	
	-- Open and close doors in the level.
	local DoorHandle = LogicManager:getEntityByName("door_coliseo013")
	if not DoorHandle:isValid() then return end
	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:CloseDoors()
	
	local DoorHandle2 = LogicManager:getEntityByName("door_casadepaso031")
	if not DoorHandle2:isValid() then return end
	local DoorComp2 = DoorHandle2:TCompDoor()
	DoorComp2:OpenDoors()
	
	
	-- Start light interpolation.
	StartLightInterpolation()
	
	-- Remove entities in previous culling volumes.
	LogicManager:BindTimedScriptEvent(ScriptHandle, "RemoveDynamic", 5.00)

	component:disableNextFrame()
end

function RemoveDynamic()
	-- Clear entities in previous volumes if there are any.
	local Entity = LogicManager:getEntityByName("CullingBox004")
	if Entity and Entity:isValid() then
		Entity:TCompSceneCullingVolume():RemoveDynamicEntitiesInVolume()
	end
	
	Entity = LogicManager:getEntityByName("CullingBox003")
	if Entity and Entity:isValid() then
		Entity:TCompSceneCullingVolume():RemoveDynamicEntitiesInVolume()
	end
	
	Entity = LogicManager:getEntityByName("CullingBoxArea2")
	if Entity and Entity:isValid() then
		Entity:TCompSceneCullingVolume():RemoveDynamicEntitiesInVolume()
	end
end

-- Light control.

local SunHandle

local CurrentInterpolationTime = 0.0
local InterpolationTime = 30.0

local SunStartIntensity
local SunFinalIntensity = 2.0

local SunStartColor
local SunFinalColor = Vector4:new(0.5, 0.2706, 0.22, 1.0)

local AmbientStartExposure
local AmbientFinalExposure = 1.0

local AmbientStartLightIntensity
local AmbientFinalLightIntensity = 0.4

local StartGlobalSkybox
local FinalGlobalSkybox = 1.0

local UpdateLightTime = 0.2

function FetchSun()
	SunHandle = LogicManager:getEntityByName("Sun")
	
	SunStartIntensity = SunHandle:TCompLightDir():GetLightIntensity()
	SunStartColor = SunHandle:TCompLightDir():GetLightColor()
	AmbientStartExposure = LogicManager:GetExposureAdjustment()
	AmbientStartLightIntensity = LogicManager:GetAmbientLightIntensity()
	StartGlobalSkybox = LogicManager:GetGlobalSkyboxLerp()
end

function StartLightInterpolation()
	LogicManager:BindTimedScriptEvent(ScriptHandle, "UpdateLighting", UpdateLightTime)	
end

function UpdateLighting()
	CurrentInterpolationTime = CurrentInterpolationTime + UpdateLightTime
	local InterpolationFactor = CurrentInterpolationTime/InterpolationTime
	if InterpolationFactor > 1.0 then InterpolationFactor = 1.0 end

	local NewSunIntensity = lerp(SunStartIntensity, SunFinalIntensity, InterpolationFactor)
	local NewSunColor = lerp(SunStartColor, SunFinalColor, InterpolationFactor)
	local NewAmbientExposure= lerp(AmbientStartExposure, AmbientFinalExposure, InterpolationFactor)
	local NewAmbientLightIntensity = lerp(AmbientStartLightIntensity, AmbientFinalLightIntensity, InterpolationFactor)
	local NewSkyBoxLerp = lerp(StartGlobalSkybox, FinalGlobalSkybox, InterpolationFactor)
	SunHandle:TCompLightDir():SetLightIntensity(NewSunIntensity)
	SunHandle:TCompLightDir():SetLightColor(NewSunColor)
	LogicManager:SetRenderAmbientVariables(-1.0, NewAmbientLightIntensity, NewAmbientExposure, -1.0, NewSkyBoxLerp)
	
	if InterpolationFactor < 1.0 then
		LogicManager:BindTimedScriptEvent(ScriptHandle, "UpdateLighting", UpdateLightTime)
	end
end

function ResetSun()
	CurrentInterpolationTime = 0.0
	SunHandle:TCompLightDir():SetLightIntensity(SunStartIntensity)
	SunHandle:TCompLightDir():SetLightColor(SunStartColor)
	LogicManager:SetRenderAmbientVariables(-1.0, AmbientStartLightIntensity, AmbientFinalExposure, -1.0, StartGlobalSkybox)
end