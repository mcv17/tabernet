-- Scripted enemyRanged functions. Used for different types of functions.

-- Recieved when the entity is created. We use it to activate the perceptive state if necessary.
function onEntityCreated(CHandle)
	-- Activate perceptive state.
	if LogicManager:isPerceptionActive() then
		LogicManager:setStatusPerceptionEntity(CHandle, true)
	end
end

-- When one of the feet of the ranged hits the ground.
function onTouchingGround(touchingPos, touchingNormal)
	LogicManager:SpawnParticle("data/particles/dust_walk.particles", touchingPos);
end

-- When one of the feet of the ranged leaves the ground.
function onLeavingGround(leavingPos, leavingNormal)
-- Nothing for now, don't know if we will add something.
end