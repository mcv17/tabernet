HealthPickUp = {}
HealthPickUp.__index = HealthPickUp

-- Health Pick Up script. When an entity enters, it adds life to it. For now, only the player can access.
function HealthPickUp:new()
   local healthPickUp = {}             -- our new object
   setmetatable(healthPickUp, HealthPickUp)  -- make HealthPickUp handle lookup
   
   -- Pickup.
   healthPickUp.ownHandle = nil -- Own handle
   healthPickUp.lifeRestored = 20 -- How much life will it restore.
   healthPickUp.lifetime = 0.0 -- How long the pickup will exist before it is deleted if nobody takes it.
   
   return healthPickUp
end

-- create and use an HealthPickup
instance = HealthPickUp:new()

function HealthPickUp:addHealthToEntity(entityToHeal)
	-- Heal the entity.
	LogicManager:HealEntity(entityToHeal, self.lifeRestored)
	
	-- Send message to destroy ourselves.
	LogicManager:DestroyEntity(self.ownHandle)
end

function HealthPickUp:onTriggerEnter(entity)
	-- Get tags of the entity that has just entered.
	local tag = entity:TCompTags()
	
	-- If it has tags and has the tag player.
	if tag and tag:hasTag("player") then
		if self.soundEvent then
			local pickupTransform = self.ownHandle:TCompTransform()
			if pickupTransform then
				LogicManager:playEvent(self.soundEvent, pickupTransform:getPosition())
			end
		end
		self:addHealthToEntity(entity)
	end
end

function onStart(variables)
	instance.lifeRestored = getValueVariablesArray(variables, "lifeRestored", instance.lifeRestored)
	instance.lifetime = getValueVariablesArray(variables, "lifetime", instance.lifetime)
	instance.soundEvent = getValueVariablesArray(variables, "soundEvent", instance.soundEvent)
end

function onEntityCreated(CHandle)
	-- Get own handle.
	instance.ownHandle = CHandle
	
	-- Register entity so it gets removed if lifetime turns to 0.0. We use a logic manager function that does this for any entity.
	-- Faster than updating here.
	LogicManager:SetCHandleTimeToLive(CHandle, instance.lifetime)
end

function onSceneCreated()
end

function onTriggerEnter(entity)
	instance:onTriggerEnter(entity)
end

function onTriggerExit(entity)
end

function onEnable()
end

function onDisable()
end

function onEnd()
end