SpecialAmmoPickUp = {}
SpecialAmmoPickUp.__index = SpecialAmmoPickUp

-- Health Pick Up script. When an entity enters, it adds life to it. For now, only the player can access.
function SpecialAmmoPickUp:new()
   local specialAmmoPickUp = {}             -- our new object
   setmetatable(specialAmmoPickUp, SpecialAmmoPickUp)  -- make SpecialAmmoPickUp handle lookup
   
   -- Pickup.
   specialAmmoPickUp.ownHandle = nil -- Own handle
   specialAmmoPickUp.ammoRestored = 1 -- How much ammo will it restore.
   specialAmmoPickUp.lifetime = 0.0 -- How long the pickup will exist before it is deleted if nobody takes it.
   
   return specialAmmoPickUp
end

-- create and use an SpecialAmmoPickUp
instance = SpecialAmmoPickUp:new()

function SpecialAmmoPickUp:addAmmoToEntity(entityToGiveAmmoTo)
	-- Give ammo to the entity.
	LogicManager:addSpecialAmmo(self.ammoRestored)
	
	-- Send message to destroy ourselves.
	LogicManager:DestroyEntity(self.ownHandle)
end

function SpecialAmmoPickUp:onTriggerEnter(entity)
	-- Get tags of the entity that has just entered.
	local tag = entity:TCompTags()
	
	-- If it has tags and has the tag player.
	if tag and tag:hasTag("player") then
		if self.soundEvent then
			local pickupTransform = self.ownHandle:TCompTransform()
			if pickupTransform then
				LogicManager:playEvent(self.soundEvent, pickupTransform:getPosition())
			end
		end
		self:addAmmoToEntity(entity)
	end
end

function onStart(variables)
	instance.ammoRestored = getValueVariablesArray(variables, "ammoRestored", instance.ammoRestored)
	instance.lifetime = getValueVariablesArray(variables, "lifetime", instance.lifetime)
	instance.soundEvent = getValueVariablesArray(variables, "soundEvent", instance.soundEvent)
end

function onEntityCreated(CHandle)
	-- Get own handle.
	instance.ownHandle = CHandle
	
	-- Register entity so it gets removed if lifetime turns to 0.0. We use a logic manager function that does this for any entity.
	-- Faster than updating here.
	LogicManager:SetCHandleTimeToLive(CHandle, instance.lifetime)
end

function onTriggerEnter(entity)
	instance:onTriggerEnter(entity)
end

function onTriggerExit(entity)
end

function onEnable()
end

function onDisable()
end

function onEnd()
end