#pragma once

#include "mcv_platform.h"
#include "particles/particle.h"
#include "PxPhysicsAPI.h"

namespace particles
{
	struct TEmitter;

	// A particle system is composed of an emitter and a vector holding all particles being created.
	// The system manages the emitter and calss it whenever it needs to spawn new particles.
	class CSystem
	{
	public:
		CSystem(TEmitter * emitter);
		CSystem(TEmitter * emitter, CTransform & nTransform);
		~CSystem();
		void launch(); // Launch particles.
		TRenderData* update(float dt, TRenderData* target_render_data); // Returns number of particles written to target_render_data.
		void setOwner(CHandle owner, const Vector3 & offsetOwner = Vector3::Zero) {
			_owner = owner; ownerOffset = offsetOwner; hasOwner = true;
		}
		void followBone(const std::string& boneName);
		void setCanSpawnParticles(bool spawning) { _canSpawnParticles = spawning; }
		void forceEmit(int particlesToEmit); // Emit a # of particles outside of the loop
		CTransform * getOwnerTransform(); // Returns the transform of the owner of the particle system.
		Vector3 getSystemPosition() { return getOwnerTransform()->getPosition(); } // Returns the position of the system. Used for sorting them based on player's position.
		int getPriority() { return _renderPriority; }
		void setPriority(int p) { _renderPriority = p; }
		void setSystemPosition(const Vector3 & position); // Sets the system position, only works if the system position is a vector and not a handle to an owner.
		void setSystemTransform(const CTransform & nTransform);
		void setTimeForDisabling(float timeForDisabling);
		bool isActive() { return isSystemActive; }
		void setActive(bool activeStatus) { isSystemActive = activeStatus; }

		// Particles base velocity
		void setBaseVelocity(Vector3 velocity) { _baseVelocity = velocity; }
		void clearBaseVelocityNextFrame() { _clearBaseVelocity = true; }

	private:
		bool isSystemActive = true; // If not active it won't update.

		TEmitter * _emitter = nullptr; // Each CSystem has a emitter that emits particles.
		std::vector<TParticle> _particles; // Holds all the information about the given particle. Each update, they get computed.

		int _renderPriority = 0;

		physx::PxParticleSystem* _physxParticleSystem = nullptr;
		physx::PxParticleExt::IndexPool* _physxIndexPool = nullptr;

		// Index converters (PhysX index <--> TParticle index)
		std::vector<uint32_t> _physxIndicesToParticlesIndices;
		std::vector<uint32_t> _particlesIndicesToPhysxIndices;

		bool hasOwner = false;
		CHandle _owner; // Owner of the particle system emitter.
		Vector3 ownerOffset = Vector3::Zero; // Offset for the owner.
		CTransform transform; // A transform for the CSystem if no entity owner.
		CHandle _ownerTransform;
		CHandle _ownerSkeleton;
		bool _followsBone = false;
		std::string _boneToFollow = "";
		
		float _particlesLifetime; // Time the system has been running.
		float _systemDuration; // Time the system will be running, taken from the emitter information.
		float _systemStopEmitters; // Time when system will stop spawning particles.

		float _timeForEmit = 0.f; // Time before emitting more particles.
		float _nextEmit = 0.0f; // Time between particles duration.
		bool _canSpawnParticles = true;
		int _particlesToBeEmitted = 0;

		// Light variables for the system.
		int current_number_of_spawned_lights = 0; // Number of particles spawned.

		// Particles base velocity
		Vector3 _baseVelocity = Vector3::Zero;
		bool _clearBaseVelocity = false;

		static const uint32_t invalid_id = ~0;   // All ones
		uint32_t         render_group = invalid_id;
		
		TRenderData * emit(TRenderData * out); // Emit new particles. Called at the start of the system and during the update method.
		VEC3 getRandomDirection(VEC3 dir); // Used for giving a random direction for new particles created.
		TRenderData* spawnParticles(TRenderData * out, CTransform* cTransform, uint32_t numParticles);
		TRenderData* spawnPhysXParticles(TRenderData * out, CTransform* cTransform, uint32_t numParticles);
		
		// Light functions.
		void checkIfSpawnLightInParticle(TParticle & p);
		void addLightToParticle(TParticle & p);

		TRenderData* physXUpdate(float dt, TRenderData* out);
		void createPhysXParticleSystem();
		void releasePhysXParticleSystem();

		// Updates light color, size, and position based on the particle it's following.
		void updateAttachedLight(TParticle & p, float timeRatio);
		float getRandomNumberAsInGPU(float seed);
		Vector4 getLightColorWithTime(float time, float partRandVal);
		float getLightSizeWithTime(float time, float partRandVal);
		
		void removeParticle(uint32_t index, TParticle& p);
		void removeParticle(std::vector<TParticle>::iterator& it, TParticle& p);
		void removeAttachedLightFromParticle(TParticle & p); // remove the particle
		void removeAttachedLightsFromLivingParticles();

		bool removeSystemIfAttachedAndParticleIsPermanent();

		friend class CModuleParticles;
		friend class CParser;
	};
}
