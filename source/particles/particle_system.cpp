#pragma once

#include "mcv_platform.h"
#include "components/common/comp_particles.h"
#include "particles/particle_system.h"
#include "particles/particle_emitter.h"
#include "components/common/comp_transform.h"
#include "components/camera/comp_camera.h"
#include "skeleton/comp_skeleton.h"
#include "entity/entity.h"
#include "engine.h"
#include "render/module_render.h"
#include "render/render_manager.h"
#include <random>
#include <ctime>
#include "module_particles.h"
#include "modules/module_physics.h"

#include "components/lighting/comp_light_point.h"

#include <math.h>       /* modf */

namespace particles
{
	float TRange::getRandom() const
	{
		if (!isRange)
			return minValue;
		static std::mt19937_64 generator(std::time(0));
		static std::uniform_real_distribution<float> uniformDistribution;
		const float unitRandomValue = uniformDistribution(generator);
		return minValue + (maxValue - minValue) * unitRandomValue;
	}

	CSystem::CSystem(TEmitter * emitter) : _emitter(emitter), _systemDuration(emitter->emitterDuration), _systemStopEmitters(emitter->stopNewEmitsAfter){}

	CSystem::CSystem(TEmitter * emitter, CTransform & nTransform) : _emitter(emitter), _systemDuration(emitter->emitterDuration), _systemStopEmitters(emitter->stopNewEmitsAfter), transform(nTransform) {}

	CSystem::~CSystem() {
		if(_emitter->spawn_lights)
			removeAttachedLightsFromLivingParticles();
		if(_emitter->is_physx_based)
			releasePhysXParticleSystem();
	}

	void CSystem::launch()
	{
		if (_emitter->is_physx_based)
			createPhysXParticleSystem();
		emit(nullptr);
	}

	TRenderData * CSystem::emit(TRenderData * out)
	{
		PROFILE_FUNCTION("Emit");
		// If false, stops spawning particles.
		if (!_canSpawnParticles) return out;

		// Get the transform for the given particle emitter.
		CTransform * cTransform = getOwnerTransform();
		if (!cTransform)
			cTransform = &CTransform();

		int count = RNG.i(_emitter->count.maxValue + 1 - _emitter->count.minValue) + _emitter->count.minValue;
		out = spawnParticles(out, cTransform, count);
		out = spawnParticles(out, cTransform, _particlesToBeEmitted);

		_particlesToBeEmitted = 0;

		_nextEmit = _emitter->interval.getRandom();
		return out;
	}

	TRenderData * CSystem::update(float dt, TRenderData * out)
	{
		PROFILE_FUNCTION(_emitter->getName().c_str());
		if (!isSystemActive) return out;

		if (removeSystemIfAttachedAndParticleIsPermanent()) {
			EngineParticles.disableSystem(this);
			return out;
		}

		// Updates the particles informations and adds the data to the out array.
		
		// Check emitter lifetime since enabled.
		_particlesLifetime += dt;
		if (_systemDuration > 0.0f && _particlesLifetime >= _systemDuration) {
			_particlesLifetime = 0.0f;
			
			// Deactivate the emitter if the lifetime has surpassed the duration.
			// Remove the entity if we specified it as so.
			CEntity* e = _owner;
			if (e) {
				if (_emitter->deleteEmitterOwner)
					_owner.destroy();
				TCompParticles* c_part = e->getComponent<TCompParticles>();
				if (c_part)
					c_part->setActive(false);
				else
					EngineParticles.disableSystem(this);
			}
			else {
				EngineParticles.disableSystem(this);
				return out;
			}

			return out;
		}

		if (_emitter->is_physx_based)
			out = physXUpdate(dt, out);
		else {
			const VEC3 kGravity(0.f, -9.8f, 0.f);

			// fetch the owner transform and scale.
			CTransform * cTransform = getOwnerTransform();

			CTransform boneTransform;
			if (_followsBone) {

			}

			// Loop through all particles and update their information.
			for (auto it = _particles.begin(); it != _particles.end(); ) {
				TParticle & p = *it;
				p.lifetime += dt;

				if (p.duration > 0.f && p.lifetime >= p.duration) {
					removeParticle(it, p);
				}
				else {
					const float timeRatio = p.duration != 0.f ? p.lifetime / p.duration : 1.f;

					if (_emitter->localSpace) {
						Vector3 systemPosition = cTransform->getPosition();
						Quaternion systemRotation = cTransform->getRotation();
						//If we are in local space, we need to update the position by the emitter position and rotation
						p.localVelocity += kGravity * p.gravityFactor * dt;

						if (_emitter->gravityPoint != VEC3::Zero) {
							VEC3 kPointGravity;
							kPointGravity = _emitter->gravityPoint - p.localPosition;
							kPointGravity.Normalize();
							kPointGravity *= 9.8f;
							p.localVelocity += kPointGravity * p.gravityPointFactor * dt;
						}

						p.velocity = DirectX::XMVector3Rotate(p.localVelocity, systemRotation);
						p.localPosition += p.localVelocity * dt;
						p.position = DirectX::XMVector3Rotate(p.localPosition, systemRotation);
						p.position += systemPosition;
					}
					else {
						p.velocity += kGravity * p.gravityFactor * dt;
						if (_emitter->gravityPoint != VEC3::Zero) {
							VEC3 kPointGravity;
							if (_emitter->localGravityPoint) {
								CTransform local;
								CTransform world;
								local.fromMatrix(MAT44::Identity);
								local.setPosition(_emitter->gravityPoint);
								world.fromMatrix(local.asMatrix() * cTransform->asMatrix());
								kPointGravity = world.getPosition() - p.position;
							}
							else
								kPointGravity = _emitter->gravityPoint - p.position;
							kPointGravity.Normalize();
							kPointGravity *= 9.8f;
							p.velocity += kPointGravity * p.gravityPointFactor * dt;
						}
						p.position += p.velocity * dt;
					}

					if (_emitter->spawn_lights)
						updateAttachedLight(p, timeRatio);

					++it;

					// Save render information -----------
					out->center = p.position;
					out->time = timeRatio;
					out->id = p.id;
					if (_emitter->particles_type_idx != 0) {
						// Simulate 3D rotation (tumbling) based on velocity
						Vector3 horizontalVelocity = p.velocity;
						Vector3 rotationAxis = Vector3::Up.Cross(horizontalVelocity);
						if (rotationAxis != Vector3::Zero) {
							Quaternion rotationDelta = Quaternion::CreateFromAxisAngle(rotationAxis, dt * horizontalVelocity.Length() * p.rotationSensitivity);
							p.rotation3D *= rotationDelta;
						}
						out->velocity = p.rotation3D;
					}
					else
						out->velocity = float4(p.velocity.x, p.velocity.y, p.velocity.z, 0.0f);
					++out;
				}
			}
		}

		// Time before emitting new particles.
		_timeForEmit += dt;
		if (_timeForEmit >= _nextEmit)
		{
			if (_systemStopEmitters != 0.0f && _particlesLifetime > _systemStopEmitters)
				return out;
			out = emit(out);
			_timeForEmit = 0.0f;
			_nextEmit = _emitter->interval.getRandom();
		}
		if (_clearBaseVelocity)
			_baseVelocity = Vector3::Zero;

		return out;
	}

	void CSystem::followBone(const std::string & boneName) {
		_followsBone = true;
		_boneToFollow = boneName;
	}

	void CSystem::forceEmit(int particlesToEmit) {
		_particlesToBeEmitted += particlesToEmit;
	}

	/* Light functions. */

	void CSystem::checkIfSpawnLightInParticle(TParticle & p) {
		static std::mt19937_64 generator(std::time(0));
		static std::uniform_real_distribution<float> uniformDistribution;
		const float unitRandomValue = uniformDistribution(generator);

		if (unitRandomValue <= _emitter->particles_to_light_ratio)
			if (current_number_of_spawned_lights < _emitter->maximum_lights)
				addLightToParticle(p);
	}

	void CSystem::addLightToParticle(TParticle & p) {
		CHandle light = EngineParticles.spawnLightType(LightType::POINT_LIGHT);
		CEntity * light_entity = light;
		p.light_handle = light_entity->getComponent<TCompLightPoint>();
		TCompLightPoint * pointLight = p.light_handle;
		if (!pointLight) return;
		pointLight->setColor(_emitter->light_base_color);
		pointLight->setIntensity(_emitter->light_base_intensity);
		pointLight->setRange(_emitter->light_base_radius);
		updateAttachedLight(p, 0.0); // Update with the values for the light at the beginning.
		current_number_of_spawned_lights++;
	}

	TRenderData* CSystem::physXUpdate(float dt, TRenderData * out) {
		// lock SDK buffers of *PxParticleSystem* ps for reading
		physx::PxParticleReadData* rd = _physxParticleSystem->lockParticleReadData();
		std::vector<physx::PxU32> removeIndices;
		//physx::PxStrideIterator<const physx::PxU32> indexBuffer(appParticleIndices);

		// access particle data from PxParticleReadData
		assert(rd);
		physx::PxStrideIterator<const physx::PxParticleFlags> flagsIt(rd->flagsBuffer);
		physx::PxStrideIterator<const physx::PxVec3> positionIt(rd->positionBuffer);
		physx::PxStrideIterator<const physx::PxVec3> velocityIt(rd->velocityBuffer);

		for (unsigned pxIndex = 0; pxIndex < rd->validParticleRange; ++pxIndex, ++flagsIt, ++positionIt, ++velocityIt) {
			if (*flagsIt & physx::PxParticleFlag::eVALID) {
				TParticle& p = _particles[_physxIndicesToParticlesIndices[pxIndex]];

				p.lifetime += dt;
				// Check if any collision occurred
				if (*flagsIt & physx::PxParticleFlag::eCOLLISION_WITH_STATIC || *flagsIt & physx::PxParticleFlag::eCOLLISION_WITH_DYNAMIC) {
					if (_emitter->remove_on_contact) {
						p.duration = 1.0f;
						p.lifetime = 1.0f;
					}
				}

				// Remove particle if needed
				if (p.duration > 0.f && p.lifetime >= p.duration) {
					uint32_t lastParticleIndex = _particles.size() - 1;
					removeIndices.push_back(pxIndex);
					_particlesIndicesToPhysxIndices[_physxIndicesToParticlesIndices[pxIndex]] = _particlesIndicesToPhysxIndices[lastParticleIndex];
					_physxIndicesToParticlesIndices[_particlesIndicesToPhysxIndices[lastParticleIndex]] = _physxIndicesToParticlesIndices[pxIndex];
					removeParticle(_physxIndicesToParticlesIndices[pxIndex], p);
				}
				else {
					const float timeRatio = p.duration != 0.f ? p.lifetime / p.duration : 1.f;
					// access particle position
					const physx::PxVec3& position = *positionIt;
					const physx::PxVec3& velocity = *velocityIt;

					// write physx data to particles vector
					p.position = PXVEC3_TO_VEC3(position);
					p.velocity = PXVEC3_TO_VEC3(velocity);

					if (_emitter->spawn_lights)
						updateAttachedLight(p, timeRatio);

					// Save render information -----------
					out->center = p.position;
					out->time = timeRatio;
					out->id = p.id;
					if (_emitter->particles_type_idx != 0) {
						// Simulate 3D rotation (tumbling) based on velocity
						Vector3 horizontalVelocity = p.velocity;
						Vector3 rotationAxis = Vector3::Up.Cross(horizontalVelocity);
						if (rotationAxis != Vector3::Zero) {
							Quaternion rotationDelta = Quaternion::CreateFromAxisAngle(rotationAxis, dt * horizontalVelocity.Length() * p.rotationSensitivity); // Too slow without mult 8
							p.rotation3D *= rotationDelta;
						}
						out->velocity = p.rotation3D;
					}
					else
						out->velocity = float4(p.velocity.x, p.velocity.y, p.velocity.z, 0.0f);
					++out;
				}
			}
		}


		// return ownership of the buffers back to the SDK
		rd->unlock();

		// release physx particles if needed
		if (!removeIndices.empty()) {
			physx::PxStrideIterator<const physx::PxU32> indexBuffer(removeIndices.data());
			_physxParticleSystem->releaseParticles(removeIndices.size(), indexBuffer);
			_physxIndexPool->freeIndices(removeIndices.size(), indexBuffer);
		}

		return out;
	}

	void CSystem::createPhysXParticleSystem() {
		releasePhysXParticleSystem();
		_physxParticleSystem = EnginePhysics.createParticleSystem(_emitter->maxCount, *_emitter);
		_physxIndexPool = physx::PxParticleExt::createIndexPool(_emitter->maxCount);
		_physxIndicesToParticlesIndices.resize(_emitter->maxCount);
		_particlesIndicesToPhysxIndices.resize(_emitter->maxCount);
	}

	void CSystem::releasePhysXParticleSystem() {
		if (_physxParticleSystem) {
			EnginePhysics.releaseParticleSystem(_physxParticleSystem);
			_physxParticleSystem = nullptr;
		}
		if (_physxIndexPool) {
			_physxIndexPool->release();
			_physxIndexPool = nullptr;
		}
		_physxIndicesToParticlesIndices.clear();
		_particlesIndicesToPhysxIndices.clear();
		_particles.clear();
	}

	void CSystem::updateAttachedLight(TParticle & p, float timeRatio) {
		TCompLightPoint * light = p.light_handle;
		if (!light) return;

		float particleRandVal = getRandomNumberAsInGPU(p.id);
		Vector4 color_light = getLightColorWithTime(timeRatio, particleRandVal);
		
		if (_emitter->use_particles_color_for_light) {
			color_light = _emitter->light_base_color * color_light;
			light->setColor(color_light);
		}
		if (_emitter->alpha_affects_intensity)
			light->setIntensity(_emitter->intensity_multiplier * _emitter->light_base_intensity * color_light.w);

		if (_emitter->size_affects_range) {
			float size = getLightSizeWithTime(timeRatio, particleRandVal);;
			light->setRange(_emitter->light_base_radius * _emitter->range_multiplier * size);
		}

		light->setPosition(p.position);
	}

	// Returns a "random", (not actually random) value that is the same used in the gpu shader
	// to calc the particles color. Used for calculating our light color and intensity.
	// We use this method that cast the float to a long and substracts it from the original float.
	// Returns the fraction part and seems to be quite fast.
	// Fraction method based on this answer: https://stackoverflow.com/a/2594953
	float CSystem::getRandomNumberAsInGPU(float seed){
		float value = sin(seed) * 43758.5453f;
		float frac = value - (long)value;
		return frac;
	}

	Vector4 CSystem::getLightColorWithTime(float time, float partRandVal) {
		// Get alpha for color blending, we use the alphas from the colors.
			// Convert range 0..1 to 0 - 11
		float time_in_color_table = time * (12 - 1);
		double color_idx_as_double;
		float color_amount = std::modf(time_in_color_table, &color_idx_as_double);
		int color_idx = (int)color_idx_as_double;
		
		if (_emitter->ctes->psystem_random_color) {
			float color_random = partRandVal;
			if (_emitter->ctes->psystem_random_dynamic_color)
				color_random = fmod(color_random + time, 1.0);

			Vector4 color_min_random = _emitter->ctes->psystem_min_colors_over_time[color_idx] * (1 - color_amount) + _emitter->ctes->psystem_min_colors_over_time[color_idx + 1] * color_amount;
			Vector4 color_max_random = _emitter->ctes->psystem_max_colors_over_time[color_idx] * (1 - color_amount) + _emitter->ctes->psystem_max_colors_over_time[color_idx + 1] * color_amount;
			return color_min_random * (1.0 - color_random) + color_max_random * color_random;
		}
		else
			return _emitter->ctes->psystem_min_colors_over_time[color_idx] * (1 - color_amount) + _emitter->ctes->psystem_min_colors_over_time[color_idx + 1] * color_amount;
	}

	float CSystem::getLightSizeWithTime(float time, float partRandVal){
		if (_emitter->ctes->psystem_random_size) {
			// Convert range 0..1 to 0..11
			float time_in_size_table = time * (12 - 1);

			// Get base particle size.
			// if time_in_size_table = 1.2.      size_entry = 1., size_amount = 0.2
			float size_idx_as_double;
			float size_amount_between_times = std::modf(time_in_size_table, &size_idx_as_double);
			int size_idx = (int)size_idx_as_double;

			Vector4 sizes_in_time = _emitter->ctes->psystem_sizes_over_time[size_idx] * (1 - size_amount_between_times)
				+ _emitter->ctes->psystem_sizes_over_time[size_idx + 1] * (size_amount_between_times);

			// x and y contain the min and max values our particle can go between.
			float size_amount = std::modf(partRandVal, &size_idx_as_double);
			float particle_size = sizes_in_time.x + sizes_in_time.y * (size_amount);

			// Check if we must change size, we use the alpha of the value for it.
			float size_mod = sizes_in_time.w;
			return particle_size * size_mod;
		}
		else {
			// Convert range 0..1 to 0..11
			float time_in_size_table = time * (12 - 1);

			// if time_in_size_table = 1.2.      size_entry = 1., size_amount = 0.2
			double size_entry;
			float size_amount = std::modf(time_in_size_table, &size_entry);
			int sizer_idx = (int)size_entry;

			return _emitter->ctes->psystem_sizes_over_time[sizer_idx].x * (1 - size_amount)
				+ _emitter->ctes->psystem_sizes_over_time[sizer_idx + 1].x * (size_amount);
		}
	}

	void CSystem::removeAttachedLightsFromLivingParticles() {
		for (auto & p : _particles)
			removeAttachedLightFromParticle(p);
	}

	bool CSystem::removeSystemIfAttachedAndParticleIsPermanent() {
		if (hasOwner)
			if (!_owner.isValid() && (_systemDuration == 0))
				return true;
		return false;
	}

	void CSystem::removeParticle(uint32_t index, TParticle & p) {
		if (_emitter->spawn_lights)
			removeAttachedLightFromParticle(p);

		// Remove particle.
		_particles[index] = _particles.back();
		_particles.pop_back();
	}

	void CSystem::removeParticle(std::vector<TParticle>::iterator & it, TParticle& p) {
		if (_emitter->spawn_lights)
			removeAttachedLightFromParticle(p);

		// Remove particle.
		if (it != _particles.end() - 1) {
			*it = _particles.back();
			_particles.pop_back();
		}
		else
			it = _particles.erase(it);
	}

	void CSystem::removeAttachedLightFromParticle(TParticle & p) {
		if (!p.light_handle.isValid()) return; // Don't do anything if this particle doesn't have a light.

		CHandle entityLight = p.light_handle.getOwner();
		entityLight.destroy();
		current_number_of_spawned_lights--;
	}

	VEC3 CSystem::getRandomDirection(VEC3 dir) {
		float apertura = RNG.f(_emitter->coneAngle * M_PI / 180); // Returns an aperture radian between cone angle (in degrees) an 0.0f.
		float circumf = RNG.f(2 * M_PI); // Value between 0 and 2 * M_PI.

		VEC3 dirPart = Vector3(sin(circumf) * sin(apertura), cos(circumf) * sin(apertura), cos(apertura));
		//If it is already oriented return. Particles are spawned in z direction by default.
		if (dir == VEC3(0,0,1))
			return dirPart;

		// Find the rotation axis and rotation angle rot
		dir.Normalize();
		VEC3 z = VEC3(0, 0, 1);

		// Get rotation axis with the cross product and angle with the dot product of the already normalized dir and z vector.
		VEC3 axis = z.Cross(dir);
		float angle = acos(dir.Dot(z));

		//And return the vector rotated by the direction
		return DirectX::XMVector3Rotate(dirPart, QUAT::CreateFromAxisAngle(axis, angle));
	}

	CTransform * CSystem::getOwnerTransform()
	{
		if (!_owner.isValid()) {
			return &transform;
		}

		CEntity * e = _owner;
		if (e) {
			if (_followsBone) {
				if (!_ownerSkeleton.isValid())
					_ownerSkeleton = e->getComponent<TCompSkeleton>();
				TCompSkeleton* skeleton = _ownerSkeleton;
				if (skeleton) {
					CTransform cTransform;
					skeleton->getBoneTransform(_boneToFollow, &cTransform);
					Vector3 offsetRotated = DirectX::XMVector3Rotate(ownerOffset, cTransform.getRotation());
					transform.setPosition(cTransform.getPosition() + offsetRotated);
					transform.setRotation(cTransform.getRotation());
				}
			}
			else {
				TCompTransform * cTransform = nullptr;
				if (!_ownerTransform.isValid())
					_ownerTransform = e->getComponent<TCompTransform>();
				cTransform = _ownerTransform;
				if (cTransform) {
					transform = *cTransform;
					Vector3 offsetRotated = cTransform->getRotation() * ownerOffset;
					transform.setPosition(cTransform->getPosition() + offsetRotated);
				}
			}

		}

		return &transform;
	}

	TRenderData* CSystem::spawnParticles(TRenderData * out, CTransform* cTransform, uint32_t numParticles) {
		int test = numParticles;
		if (_particles.size() + test > _emitter->maxCount)
			test -= _particles.size() + test - _emitter->maxCount;
		if (test == 0) return out;

		if (_emitter->is_physx_based && test > 0)
			out = spawnPhysXParticles(out, cTransform, test);
		else {
			for (size_t i = 0; i < test; i++) {
				float x, y, z;
				TParticle p;
				CTransform local, world;
				local.fromMatrix(MAT44::Identity);

				// Based on the type of spawning volumes, particles are spawned in one way or another.

				//3D Box start location
				if (_emitter->start3DBox) {
					x = RNG.f(_emitter->boxSize.x) - _emitter->boxSize.x / 2.0f;
					y = RNG.f(_emitter->boxSize.y) - _emitter->boxSize.y / 2.0f;
					z = RNG.f(_emitter->boxSize.z) - _emitter->boxSize.z / 2.0f;
					local.setPosition(VEC3(x, y, z));
				}
				//3D Sphere start location
				else if (_emitter->start3DSphere) {
					float x, y, z;
					do {
						x = RNG.f(_emitter->sphereRadius * 2.0f) - _emitter->sphereRadius;
						y = RNG.f(_emitter->sphereRadius * 2.0f) - _emitter->sphereRadius;
						z = RNG.f(_emitter->sphereRadius * 2.0f) - _emitter->sphereRadius;
					} while (powf(x, 2) + powf(y, 2) + powf(z, 2) > powf(_emitter->sphereRadius, 2));
					local.setPosition(VEC3(x, y, z));
				}
				//3D Circle start location
				else if (_emitter->start3DCircle) {
					float r = _emitter->circleRadius.getRandom();
					float theta = RNG.f(1.0f) * M_PI_2 * 180.0f / M_PI;
					x = r * cos(theta);
					z = r * sin(theta);
					local.setPosition(VEC3(x, 0, z));
				}

				VEC3 direction = _emitter->direction;
				float speed = _emitter->speed.getRandom();
				// Cone angle & direction.
				if (_emitter->coneAngle > 0.0f) {
					direction = getRandomDirection(direction);
					p.velocity = DirectX::XMVector3Rotate(direction, cTransform->getRotation());
					p.velocity *= speed;
				}
				else {
					p.velocity = DirectX::XMVector3Rotate(_emitter->direction, cTransform->getRotation());
					p.velocity *= speed;
				}
				//Add base velocity
				p.velocity += _baseVelocity;

				// Convert local position to actual world position.
				// If emitter is set so particles spawn in local space. Set the particle to point to the local position.
				// This will be converted later.
				world.fromMatrix(local.asMatrix() * cTransform->asMatrix());
				p.position = world.getPosition();
				if (_emitter->localSpace) {
					p.localPosition = local.getPosition();
					p.localVelocity = direction * speed;
				}

				// Duration and gravity factors.
				p.duration = _emitter->duration.getRandom();
				p.gravityFactor = _emitter->gravityFactor.getRandom();
				p.gravityPointFactor = _emitter->gravityPointFactor.getRandom();
				p.rotationSensitivity = _emitter->rotationSensitivity.getRandom();

				// Check if we must spawn lights with the particles.
				// Depending of the ratio and the max number of lights.
				// The particle will have a light or not.
				if (_emitter->spawn_lights)
					checkIfSpawnLightInParticle(p);

				// Save render information ----------
				p.id = _emitter->particleCount++;

				if (out) {
					out->center = p.position;
					out->time = 0.f;
					out->id = p.id;
					if (_emitter->particles_type_idx != 0) {
						if (_emitter->random_rotation) {
							p.rotation3D = Quaternion(RNG.f(), RNG.f(), RNG.f(), RNG.f());
							p.rotation3D.Normalize();
						}
						else
							p.rotation3D = Quaternion::Identity;
						out->velocity = p.rotation3D;
					}
					else
						out->velocity = float4(p.velocity.x, p.velocity.y, p.velocity.z, 0.0f);
					++out;
				}

				_particles.emplace_back(std::move(p));
			}
		}

		return out;
	}

	TRenderData * CSystem::spawnPhysXParticles(TRenderData * out, CTransform * cTransform, uint32_t numParticles) {
		std::vector<physx::PxU32> newParticlesIndices;
		newParticlesIndices.resize(numParticles);
		physx::PxU32 numAllocated = _physxIndexPool->allocateIndices(numParticles, physx::PxStrideIterator<physx::PxU32>(newParticlesIndices.data()));
		assert(numParticles >= numAllocated);

		std::vector<physx::PxVec3> newParticlePositions;
		std::vector<physx::PxVec3> newParticleVelocities;
		newParticlePositions.reserve(numParticles);
		newParticleVelocities.reserve(numParticles);
		for (size_t i = 0; i < numParticles; i++) {
			float x, y, z;
			TParticle p;
			CTransform local, world;
			local.fromMatrix(MAT44::Identity);

			// Based on the type of spawning volumes, particles are spawned in one way or another.

			//3D Box start location
			if (_emitter->start3DBox) {
				x = RNG.f(_emitter->boxSize.x) - _emitter->boxSize.x / 2.0f;
				y = RNG.f(_emitter->boxSize.y) - _emitter->boxSize.y / 2.0f;
				z = RNG.f(_emitter->boxSize.z) - _emitter->boxSize.z / 2.0f;
				local.setPosition(VEC3(x, y, z));
			}
			//3D Sphere start location
			else if (_emitter->start3DSphere) {
				float x, y, z;
				do {
					x = RNG.f(_emitter->sphereRadius * 2.0f) - _emitter->sphereRadius;
					y = RNG.f(_emitter->sphereRadius * 2.0f) - _emitter->sphereRadius;
					z = RNG.f(_emitter->sphereRadius * 2.0f) - _emitter->sphereRadius;
				} while (powf(x, 2) + powf(y, 2) + powf(z, 2) > powf(_emitter->sphereRadius, 2));
				local.setPosition(VEC3(x, y, z));
			}
			//3D Circle start location
			else if (_emitter->start3DCircle) {
				float r = _emitter->circleRadius.getRandom();
				float theta = RNG.f(1.0f) * M_PI_2 * 180.0f / M_PI;
				x = r * cos(theta);
				z = r * sin(theta);
				local.setPosition(VEC3(x, 0, z));
			}

			// Convert local position to actual world position.
			// If emitter is set so particles spawn in local space. Set the particle to point to the local position.
			// This will be converted later.
			world.fromMatrix(local.asMatrix() * cTransform->asMatrix());
			p.position = world.getPosition();
			p.rotationSensitivity = _emitter->rotationSensitivity.getRandom();

			// Cone angle & direction.
			if (_emitter->coneAngle > 0.0f) {
				VEC3 direction = getRandomDirection(_emitter->direction);
				p.velocity = DirectX::XMVector3Rotate(direction, cTransform->getRotation());
				float speed = _emitter->speed.getRandom();
				p.velocity *= speed;
			}
			else {
				p.velocity = DirectX::XMVector3Rotate(_emitter->direction, cTransform->getRotation());
				float speed = _emitter->speed.getRandom();
				p.velocity *= speed;
			}

			p.duration = _emitter->duration.getRandom();

			// Check if we must spawn lights with the particles.
			// Depending of the ratio and the max number of lights.
			// The particle will have a light or not.
			if (_emitter->spawn_lights)
				checkIfSpawnLightInParticle(p);

			p.id = _emitter->particleCount++;

			if (out) {
				out->center = p.position;
				out->time = 0.f;
				out->id = p.id;
				if (_emitter->particles_type_idx != 0) {
					p.rotation3D = Quaternion(RNG.f(), RNG.f(), RNG.f(), RNG.f());
					p.rotation3D.Normalize();
					out->velocity = p.rotation3D;
				}
				else
					out->velocity = float4(p.velocity.x, p.velocity.y, p.velocity.z, 0.0f);
					
				++out;
			}

			_particles.emplace_back(std::move(p));

			// register index converters (PhysX index <--> TParticle index)
			_physxIndicesToParticlesIndices[newParticlesIndices[i]] = _particles.size() - 1;
			_particlesIndicesToPhysxIndices[_particles.size() - 1] = newParticlesIndices[i];

			newParticlePositions.push_back(VEC3_TO_PXVEC3(p.position));
			newParticleVelocities.push_back(VEC3_TO_PXVEC3(p.velocity));
		}

		physx::PxParticleCreationData particleCreationData;
		particleCreationData.numParticles = numParticles;
		particleCreationData.indexBuffer = physx::PxStrideIterator<const physx::PxU32>(newParticlesIndices.data());
		particleCreationData.positionBuffer = physx::PxStrideIterator<const physx::PxVec3>(newParticlePositions.data());
		particleCreationData.velocityBuffer = physx::PxStrideIterator<const physx::PxVec3>(newParticleVelocities.data());

		// create particles in *PxParticleSystem*
		bool success = _physxParticleSystem->createParticles(particleCreationData);
		assert(success);

		return out;
	}
	
	void CSystem::setSystemPosition(const Vector3 & position) {
		if (_owner.isValid()) return; // We don't want to set the position if it has a handle. Move it's position from the transform/collider/whatever.

		transform.setPosition(position);
	}

	void CSystem::setSystemTransform(const CTransform & nTransform) {
		if (_owner.isValid()) return; // We don't want to set the position if it has a handle. Move it's position from the transform/collider/whatever.

		transform = nTransform;
	}

	void CSystem::setTimeForDisabling(float timeForDisabling) {
		if (!_emitter)
			EngineParticles.disableSystem(this);
		if (_systemDuration > 0.0f) {
			float timeToSetParticlesTo = Maths::clamp(_systemDuration - timeForDisabling, _systemDuration, timeForDisabling);
			float currentTimeBeforeSystemEnds = _systemDuration - _particlesLifetime;
			_particlesLifetime = (timeToSetParticlesTo > _particlesLifetime) ? _particlesLifetime : timeToSetParticlesTo;
			
			// Stop emitting already if we don't have an stop emitting.
			if (_systemStopEmitters == 0.0f)
				_systemStopEmitters = _particlesLifetime - 1.0f; // -1.0f is an arbitrary value so the particles lifetime is > than stopEmitters var.
		}
		else {
			// Stop emitting already if we don't have an stop emitting.
			if (_systemStopEmitters == 0.0f)
				_systemStopEmitters = _particlesLifetime - 1.0f;// -1.0f is an arbitrary value so the particles lifetime is > than stopEmitters var.
			timeForDisabling = (_emitter->duration.maxValue > timeForDisabling) ? _emitter->duration.maxValue : timeForDisabling;
			_systemDuration = _particlesLifetime + timeForDisabling;
		}
	}
}
