#include "mcv_platform.h"
#include "particles/module_particles.h"
#include "particles/particle_system.h"
#include "particles/particle_emitter.h"
#include "particles/particle_parser.h"
#include "render/meshes/mesh_instanced.h"
#include "render/textures/material.h"

#include "entity/entity_parser.h"

#include "components/common/comp_name.h"
#include "components/common/comp_group.h"
#include "components/common/comp_render.h"
#include "components/common/comp_particles.h"
#include "components/common/comp_transform.h"
#include "components/camera/comp_camera_manager.h"
#include "components/camera/comp_camera.h"
#include "render/render_manager.h"

#include "imgui/imgui_internal.h"
#include "engine.h"

#include <fstream>
#include <iomanip>

namespace particles
{
	CModuleParticles::CModuleParticles(const std::string& name)
		: IModule(name)
	{
	}

	bool CModuleParticles::start()
	{
		auto jTypes = Utils::loadJson("data/particles/particle_types.json");
		assert(jTypes.is_array() && !jTypes.empty());

		TParticlesType * pt = nullptr;
		particles_types.resize(jTypes.size());
		particle_type_names.reserve(jTypes.size());

		// Parse particle types (billboard, sphere, etc...)
		for (size_t i = 0; i < particles_types.size(); i++) {
			auto& j = jTypes[i];
			assert(j.count("name") > 0 && j.count("mesh") > 0);
			pt = &particles_types[i];
			pt->mesh = (CMeshInstanced*)EngineResources.getResource(j["mesh"])->as<CMesh>();
			pt->particles_cpu.reserve(j.value("reserved_particles", 1024));
			particle_type_names[static_cast<std::string>(j["name"])] = i;
		}

		// Create an empty entity with a transform and comp render
		// to be able to register it in the render manager
		h_all_particles_entity.create<CEntity>();
		CEntity * e_all_particles = h_all_particles_entity;
		assert(e_all_particles);

		CHandle h_transform = getObjectManager<TCompTransform>()->createHandle();
		h_render = getObjectManager<TCompRender>()->createHandle();
		CHandle h_name = getObjectManager<TCompName>()->createHandle();

		TCompName* c_name = h_name;
		c_name->setName("AllParticles");

		e_all_particles->setComponent(h_transform.getType(), h_transform);
		e_all_particles->setComponent(h_render.getType(), h_render);
		e_all_particles->setComponent(h_name.getType(), h_name);

		assert(h_render.isValid());
		newName[50] = {};

		// Load all light types that can be spawned together with particles.
		light_types.push_back(Utils::loadJson("data/prefabs/lights/lightPoint.json"));

		return true;
	}

	void CModuleParticles::stop() {
		if (cfgLoaded)
			saveParticlesJson();
	}

	// Spawn light types.
	CHandle CModuleParticles::spawnLightType(LightType lightType){
		assert(light_types.size() > lightType && "Light type is bigger than number of lights stored.");
		TEntityParseContext ctx;
		parseJson(light_types[lightType], ctx);
		assert(ctx.entities_loaded.size() > 0 && "File parsed didn't load any entity.");
		return ctx.entities_loaded[0];
	}
	
	void CModuleParticles::createParticleContainer() {
		// Create a new fresh entity to store the pools
		_particlesEditor.create<CEntity>();

		// Cast to entity object
		CEntity* e = _particlesEditor;

		// Create a new instance of the TCompGroup, TCompName & TCompTransform
		CHandle h_group = getObjectManager<TCompGroup>()->createHandle();
		CHandle h_name = getObjectManager<TCompName>()->createHandle();
		CHandle h_trans = getObjectManager<TCompTransform>()->createHandle();
		TCompName * name = h_name;
		name->setName("Particles Editor");
		TCompTransform* trans = h_trans;
		trans->setPosition(VEC3(0.f, 0.f, 0.f));

		// Add it to the entity
		e->setComponent(h_group.getType(), h_group);
		e->setComponent(h_name.getType(), h_name);
		e->setComponent(h_trans.getType(), h_trans);
	}

	void CModuleParticles::duplicateParticleEmitter(TParticleEditor * pEditor) {
		TParticleEditor* copy = new TParticleEditor;
		copy->name = pEditor->name + "_copy";
		particles::TEmitter* emitter = new particles::TEmitter(*pEditor->emitter);
		std::size_t found = emitter->getName().find_last_of('/');
		std::string newPath = emitter->getName().substr(0, found) + "/" + copy->name + ".particles";

		//Check if in examples
		std::string examples = "/Examples";
		size_t pos = newPath.find(examples);
		if (pos != std::string::npos) {
			newPath.erase(pos, examples.length());
		}

		emitter->replaceName(newPath);
		copy->emitter = emitter;
		copy->system = new CSystem(emitter);

		// Create a new fresh entity to store the emitter
		CHandle particleEntity;
		particleEntity.create<CEntity>();
		copy->entity = particleEntity;

		// Cast to entity object
		CEntity* e = particleEntity;

		// Create a new instance of the TCompGroup, TCompName, TCompTransform, TCompParticles
		CHandle h_group_child = getObjectManager<TCompGroup>()->createHandle();
		CHandle h_name = getObjectManager<TCompName>()->createHandle();
		CHandle h_trans = getObjectManager<TCompTransform>()->createHandle();
		CHandle h_part = getObjectManager<TCompParticles>()->createHandle();
		TCompName* name = h_name;
		name->setName(copy->name.c_str());
		TCompTransform* trans = h_trans;
		trans->setPosition(VEC3(0.f, 0.f, 0.f));
		TCompParticles* part = h_part;
		part->setEmitter(emitter);

		// Add it to the entity
		e->setComponent(h_group_child.getType(), h_group_child);
		e->setComponent(h_name.getType(), h_name);
		e->setComponent(h_trans.getType(), h_trans);
		e->setComponent(h_part.getType(), h_part);

		//Finally add this entity to the root entity of the pooling module
		CEntity* root = _particlesEditor;
		CHandle h_group = root->getComponent<TCompGroup>();
		TCompGroup* c_group = h_group;
		c_group->add(e);

		//We end this by disabling the component so the particles do not render
		TMsgToogleComponent msg;
		msg.active = false;
		e->sendMsgToComp<TCompParticles>(msg);

		particles::CParser parser;
		parser.saveEmitter(*emitter);

		_testingSystems.push_back(copy);
	}

	void CModuleParticles::deleteParticleEmitter(TParticleEditor * pEditor) {
		disableSystem(pEditor->system);
		remove(pEditor->emitter->getName().c_str());
		pEditor->entity.destroy();
		_testingSystems.erase(std::remove(_testingSystems.begin(), _testingSystems.end(), pEditor), _testingSystems.end());
	}


	void CModuleParticles::update(float dt)
	{
		PROFILE_FUNCTION("ModuleParticles_Update");

		sortActiveSystems();

		//Clear the particles type information
		for (auto & pt : particles_types) {
			pt.render_details.clear();
			pt.num_particles = 0;
			pt.num_emitters = 0;
			pt.mesh->getGroups().clear();
		}

		//Clear the parts from the render component
		TCompRender* c_render = h_render;
		assert(c_render);
		c_render->parts.clear();
		CRenderManager::get().clearParticleKeys();

		// Current emitter
		const TEmitter * curr_emitter = nullptr;

		// # particles accumulated for the current emitter, but not yet saved
		uint32_t nparticles = 0;

		// address to write next particle data, and a guard pointer
    	TRenderData * out = nullptr;
		TRenderData * max_out = nullptr;
		TRenderData * limit = nullptr;

		// Loop through all systems.
		for (auto& ps : _activeSystems)
		{
			CEntity* e = ps->_owner;
			std::string name = "UpdateLoop-";
			if (e)
				name += std::string(e->getName());
			PROFILE_FUNCTION(name.c_str());
			registerSystem(ps);

			// (We have our systems sorted by distance)
			const TEmitter * new_emitter = ps->_emitter;
			curr_emitter = new_emitter;


			nparticles = 0;

			// A change in the emitter can trigger a change in the particle type storage
			TParticlesType& pt = particles_types[curr_emitter->particles_type_idx];
			out = pt.top();
			max_out = pt.max_top();
			limit = pt.limit();			
			
			// Do we need to allocate more space as the next system might go beyond our buffer limits?
			if (out >= max_out ) {
				PROFILE_FUNCTION("SpaceAllocation_Pre ps update");

				TParticlesType& pt = particles_types[curr_emitter->particles_type_idx];
				pt.particles_cpu.reserve(pt.particles_cpu.capacity() * 2);
				out = pt.top() + nparticles;
				max_out = pt.max_top();
				limit = pt.limit();
			}

			// Get the render data to draw for the given system.
			TRenderData* new_out = ps->update(dt, out);
			assert(new_out >= out);
			//assert(new_out < limit);

			// Check if we are running out of space be it because we only have one emitter with too many particles or some other reason.
			if (new_out >= max_out) {
				PROFILE_FUNCTION("SpaceAllocation_Post ps update");

				TParticlesType& pt = particles_types[curr_emitter->particles_type_idx];
				pt.particles_cpu.reserve(pt.particles_cpu.capacity() * 2);
				max_out = pt.max_top();
				limit = pt.limit();
			}

			uint32_t particles_added = (uint32_t)(new_out - out);
			nparticles += particles_added;
			out = new_out;

			saveRenderDetailsOfSystem(ps, nparticles);
		}

		// We will request the entity to register all renderpart in the rm
		c_render->addParticlesKeys();

    	uploadRenderDetailsToGPU();

		//We draw the particle editor in the update in order to be drawn always if active
		if (showEditorWindow)
			drawEditorWindow(&showEditorWindow);

		// Free all systems that where removed this frame.
		for (auto systemToFree : systemsToRemove) {
			if (systemToFree != nullptr)
				delete systemToFree;
		}
		systemsToRemove.clear();

	}

 	// Register a call to render nparticles_per_emitter curr_system
	void CModuleParticles::saveRenderDetailsOfSystem(const CSystem* system, uint32_t nparticles) {
		PROFILE_FUNCTION("saveRenderDetailsOfEmitter");

		if (system && nparticles > 0) {
			// register a call to render nparticles_per_emitter curr_emitter
			TParticlesType& pt = particles_types[system->_emitter->particles_type_idx];
			pt.render_details.push_back(TRenderDetail{ system, nparticles, pt.num_particles });
			pt.num_particles += nparticles;
		}
	}

  	void CModuleParticles::uploadRenderDetailsToGPU( ) {
    	PROFILE_FUNCTION("uploadRenderDetailsToGPU");

		// Update the render group of the mesh
		for (auto & pt : particles_types) {
      
			// Update GPU only if there is something to render
			if (pt.num_emitters > 0)
				pt.mesh->setInstancesData(pt.particles_cpu.data(), pt.num_particles, sizeof(TRenderData));

			VMeshGroups & groups = pt.mesh->getGroups();

			// Clear existing groups before updating the new values (only the existing groups are updated)
			for (auto& g : groups)
				g.num_indices = 0;

			// Take the official mesh 
			for (auto& e : pt.render_details) {
				uint32_t render_group = e.system->render_group;
				assert(render_group < groups.size());
				
				// Update the render groups to match the resulting number of particles of each type
				groups[render_group].first_idx = e.base;
				groups[render_group].num_indices = e.num_particles;
			}
		}
	}

	void CModuleParticles::registerSystem(CSystem* system)
	{
		assert(system->_emitter);
		assert(system->_emitter->material);

		// Assign the first one: billboards
		assert(system->_emitter->particles_type_idx < particles_types.size());

		TParticlesType& pt = particles_types[system->_emitter->particles_type_idx];

		// Associate a unique index, which will be associated to a new 
		// render group of our mesh
		uint32_t mesh_group = pt.num_emitters;
		pt.num_emitters++;

		system->render_group = mesh_group;

		// Add an empty group to the mesh. It will store how many particles
		// of this type we are rendering every frame. Right now: zero.
		VMeshGroups& groups = pt.mesh->getGroups();
		groups.push_back(TMeshGroup());

		// We will create a new fake RenderPart
		TCompRender::MeshPart part;
		part.mesh_instances_group = mesh_group;
		part.mesh = pt.mesh;
		part.material = system->_emitter->material;

		TCompRender* c_render = h_render;
		assert(c_render);
		c_render->parts.push_back(part);
	}

	void CModuleParticles::sortActiveSystems()
	{
		PROFILE_FUNCTION("Sort particles systems by distance");
		Vector3 cameraPosition = TCompCameraManager::getCurrentCameraPosition();
		// Sort by pointer of the resource, so sorting by type
		std::sort(_activeSystems.begin(), _activeSystems.end(), [cameraPosition](CSystem* a, CSystem* b) {
			float distA, distB;
			if (a->getSystemPosition() == b->getSystemPosition())
				return a->getPriority() < b->getPriority();
			distA = Vector3::Distance(cameraPosition, a->getSystemPosition());
			distB = Vector3::Distance(cameraPosition, b->getSystemPosition());
			return distA > distB;
			});
	}

	CSystem* CModuleParticles::launchSystem(TEmitter* emitter, CHandle owner, const Vector3 & offsetOwner)
	{
		CSystem* ps = new CSystem(emitter);
		ps->setOwner(owner, offsetOwner);
		ps->launch();
		return addSystem(ps, emitter);
	}

	CSystem * CModuleParticles::launchSystem(TEmitter * emitter, CHandle owner, const std::string & boneToFollow, const Vector3 & offsetOwner) {
		CSystem* ps = new CSystem(emitter);
		ps->setOwner(owner, offsetOwner);
		ps->followBone(boneToFollow);
		ps->launch();
		return addSystem(ps, emitter);
	}

	CSystem * CModuleParticles::launchSystem(TEmitter * emitter, CTransform & nTransform) {
		CSystem* ps = new CSystem(emitter, nTransform);
		ps->launch();
		return addSystem(ps, emitter);
	}

	CSystem * CModuleParticles::addSystem(CSystem * nSystem, TEmitter * emitter) {
		_activeSystems.push_back(nSystem);

		// If the ps.emitter is new... we need to register in the render manager
		//if (!emitter->isRegistered())
			// a render key that renders mesh + material + technique of the emitter
			//registerEmitter(emitter);
		//registerSystem(nSystem);

		//sortActiveSystems();

		return nSystem;
	}

	void CModuleParticles::setEmittingSystemActiveStatus(CSystem * system, bool activeStatus) {
		if (system)
			system->setCanSpawnParticles(activeStatus);
	}

	void CModuleParticles::disableSystem(CSystem* system)
	{
		_activeSystems.erase(std::remove_if(_activeSystems.begin(), _activeSystems.end(), [this, system](const CSystem* sys) {
			if (system == sys) {
				systemsToRemove.push_back(system);
				return true;
			}
			return false;
		}), _activeSystems.end());
	}

	void CModuleParticles::disableSystemDelayed(CSystem * system, float timeForDisabling)
	{
		system->setTimeForDisabling(timeForDisabling);
	}

	void CModuleParticles::loadParticlesJson() {
		cfgLoaded = true;

		CEntity* root = _particlesEditor;
		CHandle h_group = root->getComponent<TCompGroup>();

		//Load the configuration of the module
		json cfg = Utils::loadJson("data/modules/particles.json");

		auto& particles = cfg["particlesEditorList"];
		if (cfg["particlesEditorList"].is_array()) {
			for (auto& p : particles) {
				if (!p.is_null()) {
					TParticleEditor* pEditor = new TParticleEditor;
					pEditor->name = p.value("name", "");
					particles::TEmitter* emitter = EngineResources.getEditableResource(p.value("path", ""))->as<particles::TEmitter>();
					pEditor->emitter = emitter;
					pEditor->system = new CSystem(emitter);

					// Create a new fresh entity to store the emitter
					CHandle particleEntity;
					particleEntity.create<CEntity>();
					pEditor->entity = particleEntity;

					// Cast to entity object
					CEntity* e = particleEntity;

					// Create a new instance of the TCompGroup, TCompName, TCompTransform, TCompParticles
					CHandle h_group_child = getObjectManager<TCompGroup>()->createHandle();
					CHandle h_name = getObjectManager<TCompName>()->createHandle();
					CHandle h_trans = getObjectManager<TCompTransform>()->createHandle();
					CHandle h_part = getObjectManager<TCompParticles>()->createHandle();
					TCompName* name = h_name;
					name->setName(pEditor->name.c_str());
					TCompTransform* trans = h_trans;
					trans->setPosition(VEC3(0.f, 0.f, 0.f));
					TCompParticles* part = h_part;
					part->setEmitter(emitter);

					// Add it to the entity
					e->setComponent(h_group_child.getType(), h_group_child);
					e->setComponent(h_name.getType(), h_name);
					e->setComponent(h_trans.getType(), h_trans);
					e->setComponent(h_part.getType(), h_part);

					//Finally add this entity to the root entity of the pooling module
					TCompGroup* c_group = h_group;
					c_group->add(e);

					//We end this by disabling the component so the particles do not render
					TMsgToogleComponent msg;
					msg.active = false;
					e->sendMsgToComp<TCompParticles>(msg);

					_testingSystems.push_back(pEditor);
				}
			}
		}
	}

	void CModuleParticles::saveParticlesJson() {

		//Load the configuration of the module
		std::string savePath = "data/modules/particles.json";
		json cfg;

		for (auto& pE : _testingSystems) {
			json particle;
			particle["name"] = pE->name;
			particle["path"] = pE->emitter->getName();
			cfg["particlesEditorList"].push_back(particle);
		}

		std::ofstream file(savePath);
		file << std::setw(4) << cfg << std::endl;
	}

	void CModuleParticles::reloadParticlesJson()
	{
		clearParticleEditor();
		createParticleContainer();
		loadParticlesJson();
	}

	void CModuleParticles::resetPhysXForEmitter(TEmitter * emitter) {
		for (auto& system : _activeSystems) {
			if (system->_emitter == emitter)
				system->createPhysXParticleSystem();
		}
	}

	void CModuleParticles::clearParticleEditor() {
		if (_particlesEditor.isValid()) {
			for (auto particleEntity : _testingSystems)
				particleEntity->entity.destroy();
			_testingSystems.clear();
			_particlesEditor.destroy();
			_particlesEditor = CHandle();
		}
	}

	void CModuleParticles::renderInMenu() {
		//Render debug information
		if (ImGui::TreeNode("Particles...")) {
			// Sum total
			uint32_t total_particles = 0;
			uint32_t total_emitters = 0;
			for (auto& pt : particles_types) {
				total_emitters += pt.num_emitters;
				total_particles += pt.num_particles;
			}

			ImGui::Text("%ld active systems", _activeSystems.size());
			ImGui::Text("%d emitters", total_emitters);
			ImGui::Text("%d particles", total_particles);

			for (auto& pt : particles_types) {
				if (ImGui::TreeNode(pt.mesh->getName().c_str())) {
				ImGui::Text("%ld in %ld groups", pt.num_particles, pt.render_details.size());
				ImGui::Text("%d emitters", pt.num_emitters);
				for (auto& e : pt.render_details)
					ImGui::Text("x%d from %d of %s (slot %d)", e.num_particles, e.base, e.system->_emitter->getName().c_str(), e.system->_emitter->render_group );
				ImGui::TreePop();
				}
			}

			if (ImGui::TreeNode("Sorted Systems: (Far to near)")) {
				for (auto ps : _activeSystems) {
					CEntity* e = ps->_owner;
					if (!e)	continue;
					TCompName* c_name = e->getComponent<TCompName>();
					ImGui::Text(c_name->getName());
				}
				ImGui::TreePop();
			}

			ImGui::TreePop();
		}

		if (ImGui::Button("Reload particles list from particles.json")) {
			reloadParticlesJson();
			editingParticle = NULL;
			showEditorWindow = false;
		}
		if (cfgLoaded) {
			if (ImGui::Button("Save particles list to particles.json"))
				saveParticlesJson();
		}

		for (auto& particleEntity : _testingSystems) {
			ImGui::Text(particleEntity->name.c_str());
			ImGui::PushID(particleEntity->name.c_str());
			if (ImGui::Button("Edit")) {
				if (editingParticle) { // Disable the previous emitter if exists
					TMsgToogleComponent msg;
					msg.active = false;
					CEntity* e = editingParticle->entity;
					e->sendMsgToComp<TCompParticles>(msg);
				}
				showEditorWindow = true;
				editingParticle = particleEntity;
			}
			ImGui::SameLine();
			if (ImGui::Button("Duplicate")) {
				duplicateParticleEmitter(particleEntity);
				ImGui::PopID();
				break;
			}
			ImGui::SameLine();
			if (ImGui::Button("Delete"))
				ImGui::OpenPopup("Delete?");

			if (ImGui::BeginPopupModal("Delete?", NULL, ImGuiWindowFlags_AlwaysAutoResize))
			{
				ImGui::Text("All those beautiful particles and their emitter will be deleted.\nThis operation cannot be undone!\n\n");
				ImGui::Separator();

				if (ImGui::Button("OK", ImVec2(120, 0))) { 
					ImGui::CloseCurrentPopup(); 
					deleteParticleEmitter(particleEntity);
					if (showEditorWindow && editingParticle == particleEntity) {
						showEditorWindow = false;
						particleEntity = NULL;
					}
				}
				ImGui::SetItemDefaultFocus();
				ImGui::SameLine();
				if (ImGui::Button("Cancel", ImVec2(120, 0)))
					ImGui::CloseCurrentPopup();
				ImGui::EndPopup();
			}

			ImGui::PopID();
		}
	}

	void CModuleParticles::drawEditorWindow(bool* p_open) {
		PROFILE_FUNCTION("drawEditorWindow");

		assert(editingParticle);
		// New ImGui window
		ImGui::SetNextWindowSize(ImVec2(520, 600), ImGuiCond_FirstUseEver);
		if (!ImGui::Begin((editingParticle->name + " editor").c_str(), p_open))
		{
			ImGui::End();
			return;
		}

		// Particle editor
		particles::TEmitter* e = editingParticle->emitter;
		CEntity* entity = editingParticle->entity;
		TCompTransform* c_trans = entity->getComponent<TCompTransform>();
		TCompParticles* c_part = entity->getComponent<TCompParticles>();

		// Toggle emitter
		ImGui::PushItemFlag(ImGuiItemFlags_Disabled, c_part->isActive());
		ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha);
		if (c_part->isActive()) {
			ImGui::PopStyleVar();
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}
		if (ImGui::Button("Activate the emitter")) {
			TMsgToogleComponent msg;
			msg.active = true;
			editingParticle->entity.sendMsgToComp<TCompParticles>(msg);
		}
		ImGui::PopItemFlag();
		ImGui::PopStyleVar();
		ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha);
		if (!c_part->isActive()) {
			ImGui::PopStyleVar();
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}
		ImGui::PushItemFlag(ImGuiItemFlags_Disabled, !c_part->isActive());
		ImGui::SameLine();
		if (ImGui::Button("Disable the emitter")) {
			TMsgToogleComponent msg;
			msg.active = false;
			editingParticle->entity.sendMsgToComp<TCompParticles>(msg);
		}
		ImGui::PopItemFlag();
		ImGui::PopStyleVar();

		// Save
		if (ImGui::Button("Save current changes to file")) {
			particles::CParser parser;
			parser.saveEmitter(*e);
		}

		// Rename
		ImGui::InputText(" ", newName, sizeof(newName));
		ImGui::SameLine();
		if (ImGui::Button("Rename")) {
			// Delete the old config file from the emitter
			assert(!remove(e->getName().c_str()));
			editingParticle->name = newName;
			CEntity* entity = editingParticle->entity;
			TCompName* c_name = entity->getComponent<TCompName>();
			if (c_name)
				c_name->setName(newName);
			std::size_t found = e->getName().find_last_of('/');
			const std::string newPath = e->getName().substr(0, found) + "/" + newName + ".particles";
			e->replaceName(newPath);
			particles::CParser parser;
			parser.saveEmitter(*e);
		}

		// Transform
		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Transform from entity holding the emitter]");
		ImGui::PopStyleColor();
		ImGui::PushItemWidth(200);
		c_trans->renderInMenu();

		// Parameters editor
		e->renderInMenu();
		ImGui::End();
	}

	void CModuleParticles::disable3DLocation(TParticleEditor* pEditor) {
		pEditor->emitter->start3DBox = false;
		pEditor->emitter->start3DSphere = false;
		pEditor->emitter->start3DCircle = false;
	}

	void CModuleParticles::renderDebug() {
		VEC4 color = VEC4(1.0f, 1.0f, 0.0f, 1.0f);
		for (auto& ps : _activeSystems)
		{
			PROFILE_FUNCTION("particleRenderDebug");
			TEmitter* e = ps->_emitter;
			if (e->start3DBox || e->start3DSphere || e->start3DCircle) {
				CEntity* entity = ps->_owner;
				if (!entity) continue; // Right now, we don't render debug systems that don't have an owner. We may extend this in the future if trully necessary.

				TCompTransform* c_trans = entity->getComponent<TCompTransform>();

				if (!c_trans)
					continue;

				if (e->start3DBox) {
					drawWiredAABB(Vector3(0.0f, 0.0f, 0.0f) , e->boxSize / 2.0f, c_trans->asMatrix(), color);
				}
				else if (e->start3DSphere) {
					drawWiredSphere(c_trans->asMatrix(), e->sphereRadius, color);
				}
				else if (e->start3DCircle) {
					drawCircle(Vector3(0.0f, 0.0f, 0.0f), e->circleRadius.maxValue, c_trans->asMatrix(), color);
				}
			}

			if (e->gravityPoint != VEC3::Zero) {
				VEC3 pos;
				if (e->localGravityPoint) {
					CTransform local;
					CTransform world;
					local.fromMatrix(MAT44::Identity);
					local.setPosition(e->gravityPoint);
					CEntity* e = ps->_owner;
					TCompTransform* c_trans = e->getComponent<TCompTransform>();
					world.fromMatrix(local.asMatrix() * c_trans->asMatrix());
					pos = world.getPosition();
				}
				else
					pos = e->gravityPoint;
				drawWiredSphere(pos, 0.1f, VEC4(0.0f, 1.0f, 1.0f, 1.0f));
			}
		}
	}
}
