#pragma once

#include "mcv_platform.h"

namespace particles
{
	// CPU information (more than what we need to render...)
	struct TParticle
	{
		VEC3 position = VEC3::Zero; // Position where the particle is at.
		VEC3 velocity = VEC3::Zero; // Velocity for the given particle.
		QUAT rotation3D = QUAT::Identity;
		float lifetime = 0.f; // Lifetime of the particle. Will get removed once it goes above duration value.
		float duration = 0.f; // How long before it gets removed.
		float gravityFactor = 1.0f; // How affected by gravity the given particle is.
		float gravityPointFactor = 0.0f;
		float rotationSensitivity;

		// For local space
		VEC3 localPosition = VEC3::Zero; 
		VEC3 localVelocity = VEC3::Zero; 
		float pointGravity = 0.0f;

		// params for random in shader
		unsigned int id = 0;

		// Light params.
		CHandle light_handle;
	};

	// ----------------------------------------------
	// Basic struct to render each particle
	struct TRenderData {
		float3 center;
		float  time;
		unsigned int id;
		float4 velocity;
	};
}
