#pragma once

#include <string>
#include <cassert>

class IResource;

/* Represents a type of resource.
Resource types are used for the management of the creation of a resource given a filepath.
Furthermore, they also hold the name given to the type of resource and the extension it uses
for searches in the engine. */
class CResourceType {
public:
	virtual const char * getExtension(int idx = 0) const = 0;
    virtual int getNumExtensions() const { return 1; }
	virtual const char * getName() const = 0;
	
	// Does all the necessary procedures to open the file, read the resource and create an object of it.
	virtual IResource * create(const std::string & filepath) const = 0;
};

// Forward declaration for a function that given a type will return a CResourceType.
template< typename T > const CResourceType * getResourceTypeFor();

/* An interface representing an abstract resource that can be managed by the resource manager. */
class IResource {
protected:
	std::string          name;
	const CResourceType * resource_type = nullptr;

public:
	virtual ~IResource() { }
	
	/* Renders the resource using img. */
	virtual void renderInMenu() {}

	// If the file gets changed a new filename will be returned, implement here
	// any procedure that may be necessary to update the file.
	virtual void onFileChanged(const std::string & filename) {}

	/* Converts resource to the given template if posible.  */
	template< typename T > const T * as() const {
		assert(resource_type);
		assert(getResourceTypeFor<T>());
		assert(getResourceTypeFor<T>() == resource_type ||
			Utils::fatal("You are trying to convert the resource '%s' of type '%s' to resource type '%s'",
				getName().c_str(), resource_type->getName(), getResourceTypeFor<T>()->getName()));
		return static_cast<const T *>(this);
	}

	/* Converts resource to the given template if posible as non const.  */
	template< typename T > T * as() {
		assert(resource_type);
		assert(getResourceTypeFor<T>());
		assert(getResourceTypeFor<T>() == resource_type ||
					 Utils::fatal("You are trying to convert the resource '%s' of type '%s' to resource type '%s'",
												getName().c_str(), resource_type->getName(), getResourceTypeFor<T>()->getName()));
		return static_cast<T *>(this);
	}

	/* Getters and setters. */
	const std::string & getName() const { return name; }
	const CResourceType * getResourceType() const { return resource_type; }
	void setNameAndType(const std::string & new_name, const CResourceType * new_resource_type);
	void replaceName(const std::string & new_name);
};
