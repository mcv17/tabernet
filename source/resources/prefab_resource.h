#pragma once

#include "resources/resource.h"
#include "utils/json.hpp"

class CPrefab : public IResource {
	std::string filename;
	std::string fileRawText = "";
	json jdata;
	json args;
	bool parsed = false;

	void parseArgs(std::string& prefabStr);

public:
	CPrefab(const std::string & inFilename);

	void parsePrefab(json& inArgs);

	void renderInMenu() override;

	void onFileChanged(const std::string & filename) override;

	void setFilename(const std::string& inFilename) { filename = inFilename; }

	const json& getJson();
	const json& getJson(json& inArgs);
	bool isAreadyParsed() { return parsed; }
};
