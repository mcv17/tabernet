#include "mcv_platform.h"
#include "resource_manager.h"
#include "resource.h"

const CResourceType * CResourcesManager::findResourceTypeByFileExtension(const std::string & filepath) {
	assert(!filepath.empty());

	// Find extension
	auto idx = filepath.find_last_of(".");
	assert(idx != std::string::npos || Utils::fatal("Can't find extension to identify resource in %s", filepath.c_str()));

	// ext = mesh in the case of meshes for example.
	std::string ext = filepath.substr(idx + 1); // Takes the . from ".mesh" so we have "mesh" instead.
	return findResourceTypeByExtension(ext);
}

const CResourceType * CResourcesManager::findResourceTypeByExtension(const std::string & ext) {
	for (auto it : _resource_types) {
		int n = it->getNumExtensions();
		for(int i = 0; i<n; i++){
			if (it->getExtension(i) == ext)
				return it;
		}
	}
	return nullptr;
}

void CResourcesManager::registerResource(IResource * new_resource) {
	// Must exists and have a valid name
	assert(new_resource);
	assert(!new_resource->getName().empty());
	std::string name = new_resource->getName();
	// The resource MUST be unique by name
	assert(!existsResource(name));
	_all_resources[name] = new_resource;
}

void CResourcesManager::registerOrOverwriteResource(IResource * new_resource) {
	// Must exists and have a valid name
	assert(new_resource);
	assert(!new_resource->getName().empty());
	std::string name = new_resource->getName();

	if (_all_resources[name] != nullptr) delete _all_resources[name];

	_all_resources[name] = new_resource;
}

void CResourcesManager::registerResourceType(const CResourceType * res_type) {
	// We get sure the resource type has a valid extension
	for(int i=0; i< res_type->getNumExtensions(); i++){
		const char* ext = res_type->getExtension(i);
		assert(findResourceTypeByExtension(ext) == nullptr);
	}
	_resource_types.push_back(res_type);
}

bool CResourcesManager::existsResource(const std::string & name) const {
	return _all_resources.find(name) != _all_resources.end();
}

IResource* CResourcesManager::getEditableResource(const std::string & filepath) {
	// First we check if the resource is present, if it is, we return it.
	auto it = _all_resources.find(filepath);
	if (it != _all_resources.end())
		return it->second;

	// If the resource is not present, we identify the type by checking the filepath extension.
	auto resource_type = findResourceTypeByFileExtension(filepath);
	assert(resource_type || Utils::fatal("Can't identify resource type %s \n", filepath.c_str()));

	// Create a context entry
	TFileContext fc(filepath);

	// If the resource type is not null we load it by using its resource type.
	std::string profile_text = "CResourcesManager: " + std::string(resource_type->getName());
	PROFILE_FUNCTION_COPY_TEXT(profile_text.c_str());
	IResource* new_resource = resource_type->create(filepath);
	assert(new_resource || Utils::fatal("Failed to create resource %s of type %s\n", filepath.c_str(), resource_type->getName()));

	// Finally, we register the new resource so we can access it without loading the file each time.
	// The memory is managed by this class (the resource manager), and must be freed by it.
	registerResource(new_resource);

	return new_resource;
}

const IResource* CResourcesManager::getResource(const std::string & filepath){
	return getEditableResource(filepath);
}

void CResourcesManager::onFileChanged(const std::string & filename) {
	for (auto it : _all_resources)
		it.second->onFileChanged(filename);
}

void CResourcesManager::renderInMenu() {
	for (auto rt : _resource_types) {
		if (ImGui::TreeNode(rt->getName())) {
			for (auto it : _all_resources) {
				IResource * res = it.second;
				// Is of the current resource type I'm showing...
				if (res->getResourceType() == rt) {
					// Resource name.
					if (ImGui::TreeNode(it.first.c_str())) {
						res->renderInMenu();
						ImGui::TreePop();
					}
				}
			}
			ImGui::TreePop();
		}
	}
}

void CResourcesManager::destroyResource(const std::string & filepath) {
	// First we check if the resource is present, if it is, we return it.
	auto it = _all_resources.find(filepath);
	if (it != _all_resources.end()) {
		delete it->second;
		_all_resources.erase(it->first);
	}
}

void CResourcesManager::destroyAll() {
	// Delete all resources
	for (auto it : _all_resources) 
		delete it.second;
	_all_resources.clear();
}

