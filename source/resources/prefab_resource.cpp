#include "mcv_platform.h"
#include "prefab_resource.h"
#include <regex>

#define ARGS_REGEX		"\\@\\@arg[\\s]*=[\\s]*(.+?)[\\s]*\\@default[\\s]*=[\\s]*([\\S\\s]*?)[\\s]*\\@\\@end"

class CPrefabResourceType : public CResourceType {
public:
	const char* getExtension(int idx) const override { return "prefab"; }

	const char* getName() const override { return "Prefabs"; }

	IResource * create(const std::string & filename) const override {
		CPrefab * new_res = new CPrefab(filename);
		new_res->setNameAndType(filename, this);
		return new_res;
	}
};

void CPrefab::parseArgs(std::string& prefabStr) {
	std::cmatch match;
	std::regex rx(ARGS_REGEX);
	std::string r = "", argName, arg;
	const char* prefabStrPtr = prefabStr.data();
	while (std::regex_search(prefabStrPtr, match, rx)) {
		argName = match.str(1);
		if (args.count(argName) > 0) {
			auto& argValue = args[argName];
			if (argValue.is_object()) {
				arg = args[argName].dump();
			}
			else if (argValue.is_array()) {
				arg = args[argName].dump();
			}
			else if (argValue.is_string())
				arg = args[argName];
			else if (argValue.is_number())
				arg = std::to_string(static_cast<float>(args[argName]));
			else if (argValue.is_boolean())
				arg = (static_cast<bool>(args[argName]) ? "true" : "false");
			else
				assert(false && "Invalid prefab arg");
		}
		else
			arg = match.str(2);

		r += match.prefix().str();
		r += arg;
		prefabStrPtr = match.suffix().first;
		
	}

	if (r != "") {
		prefabStr = "";
		prefabStr += r;
		prefabStr += prefabStrPtr;
	}
}

CPrefab::CPrefab(const std::string & inName) {}

void CPrefab::parsePrefab(json & inArgs) {
	std::string prefabStr;
	{
		PROFILE_FUNCTION_COPY_TEXT(filename.c_str());
		while (true) {
			if (fileRawText.empty()) {
				fileRawText = Utils::loadTextFile(filename);
			}

			prefabStr = fileRawText;
			args = inArgs;

			parseArgs(prefabStr);

#ifdef NDEBUG
			jdata = Utils::loadStringJson(prefabStr);
			if (jdata.is_discarded()) {
				Utils::fatal("Failed to parse json file %s\n", filename.c_str());
				continue;
			}
#else
			try {
				jdata = Utils::loadStringJson(prefabStr);
			}
			catch (json::parse_error& e) {
				// output exception information
				Utils::fatal("Failed to parse json file %s\n%s\nAt offset: %d\n"
										 , filename.c_str(), e.what(), e.byte);
				fileRawText = "";
				continue;
			}
#endif

			break;
		}
	}
	
	parsed = true;
}

void CPrefab::onFileChanged(const std::string& filename) {
	parsed = false;
	fileRawText = "";
}

const json & CPrefab::getJson() {
	return getJson(json());
}

const json & CPrefab::getJson(json & inArgs) {
	if (!parsed)
		parsePrefab(inArgs);

	return jdata;
}

void CPrefab::renderInMenu() {
	std::string jstr = jdata.dump(2);
	ImGui::Text("%s", jstr.c_str());
}

template<> const CResourceType* getResourceTypeFor<CPrefab>() {
	static CPrefabResourceType resource_type;
	return &resource_type;
}



