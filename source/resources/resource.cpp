#include "mcv_platform.h"
#include "resource.h"

void IResource::setNameAndType(const std::string & new_name, const CResourceType * new_resource_type) {
	assert(!new_name.empty());
	assert(name.empty());
	assert(resource_type == nullptr);
	assert(new_resource_type != nullptr);
	name = new_name;
	resource_type = new_resource_type;
}

void IResource::replaceName(const std::string & new_name) {
	assert(!new_name.empty());
	assert(!name.empty());
	name = new_name;
}
