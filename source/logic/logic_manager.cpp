#include "mcv_platform.h"
#include "logic_manager.h"

#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "entity/entity_parser.h"
#include "components/common/comp_name.h"
#include "components/common/comp_render.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_health.h"
#include "components/player/comp_input_player_movement.h"
#include "components/player/comp_input_player_combat.h"
#include "components/animations/comp_player_animator.h"
#include "components/controllers/comp_player_character_controller.h"
#include "components/camera/comp_player_camera_controller.h"
#include "components/powers/comp_perception.h"
#include "components/game/comp_checkpoint.h"
#include "components/scenario/comp_totem.h"

//Devices
#include "input/devices/device_keyboard.h"
#include "input/devices/device_mouse.h"
#include "input/devices/device_pad_xbox.h"

#include "engine.h"
#include "modules/module_camera_mixer.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "geometry/interpolators.h"

#include "imgui/imgui_manager.h"
#include "utils/time.h"
#include "utils/phys_utils.h"

// Rendering.
#include "render/module_render.h"

// Particles.
#include "particles/particle_system.h"
#include "particles/particle_emitter.h"
#include <components\weapons\comp_smg_controller.h>
#include <components\weapons\comp_rifle_controller.h>
#include <components\weapons\comp_shotgun_controller.h>

#include "../components/controllers/comp_rigid_body.h"
#include "components/ai/behaviour_tree/comp_bt_enemy_melee.h"
#include "components/ai/behaviour_tree/comp_bt_enemy_ranged.h"
#include "components/ai/behaviour_tree/comp_bt_enemy_immortal.h"
#include "components/postFX/comp_postFX_fog.h"

using namespace physx;

LogicManager & LogicManager::Instance() {
	static LogicManager instance;
	return instance;
}

void LogicManager::declareInLua() {
	// IMPORTANT: Out of parameters for new_usertype, bind more methods as shown in the bottom of this function
	auto& luaState = EngineScripting.getLuaState();
	luaState.new_usertype<LogicManager>
		(
			"LogicManagerClass",
			"new", sol::no_constructor,
			"getEntityByName", &LogicManager::getEntity,
			"getEntitiesByTag", sol::overload
			(
				sol::resolve<VHandles(const std::string&)>(&LogicManager::getEntitiesByTag),
				sol::resolve<VHandles(uint32_t tag)>(&LogicManager::getEntitiesByTag)
			),
			"removeEntitiesByTag", &LogicManager::removeEntitiesByTag,
			"CreatePrefab", sol::overload
			(
				sol::resolve<std::vector<CHandle>(const std::string&, float, float, float)>(&LogicManager::CreatePrefab),
				sol::resolve<std::vector<CHandle>(const std::string&, const Vector3&)>(&LogicManager::CreatePrefab)
			),

			"createEntity", sol::overload
			(
				sol::resolve<CHandle(const std::string&, float, float, float)>(&LogicManager::CreateEntity),
				sol::resolve<CHandle(const std::string&, const Vector3&)>(&LogicManager::CreateEntity),
				sol::resolve<CHandle(const std::string&, const Vector3&, const Vector3&)>(&LogicManager::CreateEntity)
			),
			"createEntityAndAddImpulse", &LogicManager::CreateEntityAndAddImpulse,
			"createEnemyMelee", sol::overload
			(
				sol::resolve<CHandle(float, float, float)>(&LogicManager::CreateEnemyMelee),
				sol::resolve<CHandle(float, float, float, float, float, float)>(&LogicManager::CreateEnemyMelee),
				sol::resolve<CHandle(const Vector3&)>(&LogicManager::CreateEnemyMelee),
				sol::resolve<CHandle(const Vector3&, const Vector3&)>(&LogicManager::CreateEnemyMelee)
			),
			"createEnemyRanged", sol::overload
			(
				sol::resolve<CHandle(float, float, float)>(&LogicManager::CreateEnemyRanged),
				sol::resolve<CHandle(float, float, float, float, float, float)>(&LogicManager::CreateEnemyRanged),
				sol::resolve<CHandle(const Vector3&)>(&LogicManager::CreateEnemyRanged),
				sol::resolve<CHandle(const Vector3&, const Vector3&)>(&LogicManager::CreateEnemyRanged)
			),
			"createEnemyImmortal", sol::overload
			(
				sol::resolve<CHandle(float, float, float)>(&LogicManager::CreateEnemyImmortal),
				sol::resolve<CHandle(float, float, float, float, float, float)>(&LogicManager::CreateEnemyImmortal),
				sol::resolve<CHandle(const Vector3&)>(&LogicManager::CreateEnemyImmortal),
				sol::resolve<CHandle(const Vector3&, const Vector3&)>(&LogicManager::CreateEnemyImmortal)
			),
			"createEnemy", sol::overload
			(
				sol::resolve<CHandle(float, float, float)>(&LogicManager::CreateEnemy),
				sol::resolve<CHandle(float, float, float, float, float, float)>(&LogicManager::CreateEnemy),
				sol::resolve<CHandle(const Vector3&)>(&LogicManager::CreateEnemy)
			),
			"SetCHandleTimeToLive", &LogicManager::SetCHandleTimeToLive,
			"DestroyEntity", &LogicManager::DestroyEntity,
			"setEntityActive", sol::overload
			(
				sol::resolve<void(const std::string&, bool)>(&LogicManager::SetEntityActive),
				sol::resolve<void(CHandle, bool)>(&LogicManager::SetEntityActive)
			),
			"setTagEntitiesActive", &LogicManager::SetTagEntitiesActive,
			"setControllerEntityActive", sol::overload
			(
				sol::resolve<void(const std::string&, bool)>(&LogicManager::SetControllerEntityActive),
				sol::resolve<void(CHandle, bool)>(&LogicManager::SetControllerEntityActive)
			),
			"HealEntity",
			sol::overload
			(
				sol::resolve<void(const std::string &, float)>(&LogicManager::HealEntity),
				sol::resolve<void(CHandle, float)>(&LogicManager::HealEntity)
			),
			"setEntityHealth",
			sol::overload
			(
				sol::resolve<void(const std::string &, float)>(&LogicManager::SetEntityHealth),
				sol::resolve<void(CHandle, float)>(&LogicManager::SetEntityHealth)
			),
			"setEntityMaxHealth", 
			sol::overload
			(
				sol::resolve<void(const std::string &, float)>(&LogicManager::SetEntityMaxHealth),
				sol::resolve<void(CHandle, float)>(&LogicManager::SetEntityMaxHealth)
			),
			"SetEntityImmortal",
			sol::overload
			(
				sol::resolve<void(const std::string &, bool)>(&LogicManager::SetEntityImmortal),
				sol::resolve<void(CHandle, bool)>(&LogicManager::SetEntityImmortal)
			),
			"setActivePlayerInput", &LogicManager::setActivePlayerInput,
			"restoreFullPlayer", &LogicManager::restoreFullPlayer,
			"teleportPlayer", &LogicManager::teleportPlayer,
			"addSmgCommonAmmo", &LogicManager::addCommonAmmo<TCompSmgController>,
			"addRifleCommonAmmo", &LogicManager::addCommonAmmo<TCompRifleController>,
			"addShotgunCommonAmmo", &LogicManager::addCommonAmmo<TCompShotgunController>,
			"addSpecialAmmo", &LogicManager::addSpecialAmmo,
			"sendDeltaTrauma", sol::overload
			(
				sol::resolve<void(const std::string &, float)>(&LogicManager::SendDeltaTrauma),
				sol::resolve<void(const std::string &, float, float)>(&LogicManager::SendDeltaTrauma),
				sol::resolve<void(CHandle, float)>(&LogicManager::SendDeltaTrauma),
				sol::resolve<void(CHandle, float, float)>(&LogicManager::SendDeltaTrauma)
			),
			"isPerceptionActive", &LogicManager::isPerceptionActive,
			"setStatusPerceptionEntity",
			sol::overload
			(
				sol::resolve<void(const std::string &, bool)>(&LogicManager::setStatusPerceptionEntity),
				sol::resolve<void(CHandle, bool)>(&LogicManager::setStatusPerceptionEntity)
			),
			"blendCamera", sol::resolve<void(const std::string &, float, const std::string &)>(&LogicManager::BlendCamera),
			"getDefaultCamera", &LogicManager::getDefaultCamera,
			"getOutputCamera", &LogicManager::getOutputCamera,
			"setDefaultCamera", sol::overload(
				sol::resolve<void(CHandle)>(&LogicManager::setDefaultCamera),
				sol::resolve<void(const std::string &)>(&LogicManager::setDefaultCamera)
			),
			"setOutputCamera", sol::overload(
				sol::resolve<void(CHandle)>(&LogicManager::setOutputCamera),
				sol::resolve<void(const std::string &)>(&LogicManager::setOutputCamera)
			),
			"SpawnDecal", sol::overload
			(
				sol::resolve<void(const std::string &, const Vector3 &, const Vector3 &, const float)>(&LogicManager::SpawnDecal),
				sol::resolve<void(const std::string &, const Vector3 &, const Vector3 &, const Vector4 &, const float)>(&LogicManager::SpawnDecal),
				sol::resolve<void(const std::string &, const Vector3 &, const Vector3 &, const Vector4 &, const Vector3 &, const float)>(&LogicManager::SpawnDecal),
				sol::resolve<void(int, const Vector3 &, const Vector3 &, const float)>(&LogicManager::SpawnDecal),
				sol::resolve<void(int, const Vector3 &, const Vector3 &, const Vector4 &, const float)>(&LogicManager::SpawnDecal),
				sol::resolve<void(int, const Vector3 &, const Vector3 &, const Vector4 &, const Vector3 &, const float)>(&LogicManager::SpawnDecal)
			),
			"getDynamicDecalID", &LogicManager::getDynamicDecalID
		);

		// IMPORTANT: Out of parameters for new_usertype, bind more methods like this
		auto& userType = luaState["LogicManagerClass"];


		userType["SpawnParticle"] = sol::overload(
			sol::resolve<particles::CSystem * (const std::string&, CHandle, const Vector3&)>(&LogicManager::SpawnParticle),
			sol::resolve<particles::CSystem * (const std::string&, CHandle, const std::string&, const Vector3&)>(&LogicManager::SpawnParticle),
			sol::resolve<particles::CSystem * (const std::string&, const Vector3&)>(&LogicManager::SpawnParticle),
			sol::resolve<particles::CSystem * (const std::string&, const Vector3&)>(&LogicManager::SpawnParticle),
			sol::resolve<particles::CSystem * (const std::string&, const Vector3&, const Vector3&)>(&LogicManager::SpawnParticle),
			sol::resolve<particles::CSystem * (const std::string&, const Vector3&, const Vector3&, const float)>(&LogicManager::SpawnParticle)
		);
		userType["StartCinematic"] = &LogicManager::StartCinematic;
		userType["EndCinematic"] = &LogicManager::EndCinematic;
		userType["PlayLevelSequence"] = &LogicManager::PlayLevelSequence;
		userType["EndLevelSequence"] = &LogicManager::EndLevelSequence;
		
		userType["CallGlobalScriptFunction"] = &LogicManager::CallGlobalScriptFunction;
		userType["BindScriptToComponentEvent"] = &LogicManager::BindScriptToComponentEvent;
		userType["ClearBindTimedScriptEvents"] = &LogicManager::ClearBindTimedScriptEvents;
		userType["UnbindScriptToComponentEvent"] = &LogicManager::UnbindScriptToComponentEvent;
		userType["BindTimedScriptEvent"] = &LogicManager::BindTimedScriptEvent;
		userType["CallEvent"] = &LogicManager::CallEvent;
		userType["CallEventAllScript"] = &LogicManager::CallEventAllScript;

		userType["activateColorGrading"] = &LogicManager::activateColorGrading;
		userType["colorGradingCurrentLUTAmount"] = &LogicManager::colorGradingCurrentLUTAmount;
		userType["colorGradingFallTime"] = &LogicManager::colorGradingFallTime;
		userType["colorGradingLutTexture"] = &LogicManager::colorGradingLutTexture;
		userType["activateVignetting"] = &LogicManager::activateVignetting;
		userType["vignettingAmount"] = &LogicManager::vignettingAmount;
		userType["vignettingFallTime"] = &LogicManager::vignettingFallTime;
		userType["activateBurningFade"] = &LogicManager::activateBurningFade;
		
		userType["activateCinematicBars"] = sol::overload(
			sol::resolve<void (const std::string&, bool)>(&LogicManager::activateCinematicBars),
			sol::resolve<void(const std::string&, bool, float)>(&LogicManager::activateCinematicBars)
		);
		userType["showCinematicBarsFull"] = &LogicManager::showCinematicBarsFull;
		userType["hideCinematicBarsFull"] = &LogicManager::hideCinematicBarsFull;

		userType["setFogLerp"] = &LogicManager::setFogLerp;

		userType["spawnExplosion"] = &LogicManager::SpawnExplosion;
		userType["setActiveModuleInput"] = &LogicManager::setActiveModuleInput;
		userType["setCenterMouseActive"] = &LogicManager::setCenterMouseActive;
		userType["showCursor"] = &LogicManager::showCursor;
		userType["setActiveMouse"] = &LogicManager::setActiveMouse;
		userType["setActiveKeyboard"] = &LogicManager::setActiveKeyboard;
		userType["setActiveGamepad"] = &LogicManager::setActiveGamepad;
		userType["playEvent"] = sol::overload
			(
				sol::resolve<void(const std::string)>(&LogicManager::playEvent),
				sol::resolve<void(const std::string, const VEC3)>(&LogicManager::playEvent)
			);
		userType["setEventParameter"] = &LogicManager::setEventParameter;
		userType["stopEvent"] = &LogicManager::stopEvent;
		userType["playMusic"] = &LogicManager::playMusic;
		userType["stopMusic"] = &LogicManager::stopMusic;
		userType["activateWidget"] = &LogicManager::activateWidget;
		userType["deactivateWidget"] = &LogicManager::deactivateWidget;
		userType["isWidgetActive"] = &LogicManager::isWidgetActive;
		userType["updateRatioFromProgress"] = &LogicManager::updateRatioFromProgress;
		userType["setChildWidgetVisibleByTime"] = &LogicManager::setChildWidgetVisibleByTime;
		userType["setControlUIButtonsByDevice"] = &LogicManager::setControlUIButtonsByDevice;
		userType["getRandomInt"] = &LogicManager::getRandomInt;
		userType["getRandomFloat"] = &LogicManager::getRandomFloat;
		userType["transformVector"] = &LogicManager::TransformVector;
		userType["pauseTime"] = &LogicManager::pauseTime;
		userType["resumeTime"] = &LogicManager::resumeTime;
		userType["toggleTime"] = &LogicManager::toggleTime;
		userType["setTimeScale"] = &LogicManager::setTimeScale;
		userType["getFramerateLimit"] = &LogicManager::getFramerateLimit;
		userType["setFramerateLimit"] = &LogicManager::setFramerateLimit;
		userType["exitApplication"] = &LogicManager::exitApplication;

		userType["GetAmbientBoost"] = &LogicManager::GetAmbientBoost;
		userType["GetAmbientLightIntensity"] = &LogicManager::GetAmbientLightIntensity;
		userType["GetExposureAdjustment"] = &LogicManager::GetExposureAdjustment;
		userType["GetReflectionIntensity"] = &LogicManager::GetReflectionIntensity;
		userType["GetGlobalSkyboxLerp"] = &LogicManager::GetGlobalSkyboxLerp;

		userType["SetRenderAmbientVariables"] = &LogicManager::SetRenderAmbientVariables;
		userType["SetAmbientBoost"] = &LogicManager::SetAmbientBoost;
		userType["SetAmbientLightIntensity"] = &LogicManager::SetAmbientLightIntensity;
		userType["SetExposureAdjustment"] = &LogicManager::SetExposureAdjustment;
		userType["SetReflectionIntensity"] = &LogicManager::SetReflectionIntensity;
		
		userType["ChangeToMenuState"] = &LogicManager::ChangeToMenuState;
		userType["changeToGamestate"] = &LogicManager::changeToGamestate;
		userType["changeToGamestateNonBlocking"] = &LogicManager::changeToGamestateNonBlocking;
		userType["restorePlayerToLastCheckpoint"] = &LogicManager::restorePlayerToLastCheckpoint;
		userType["restorePlayerToSpawnCheckpoint"] = &LogicManager::restorePlayerToSpawnCheckpoint;
		userType["resetCheckpoints"] = &LogicManager::resetCheckpoints;
		userType["getSpawnCheckpoint"] = &LogicManager::getSpawnCheckpointComponent;
		userType["getCurrentCheckpoint"] = &LogicManager::getCurrentCheckpointComponent;
		userType["killEntity"] = sol::overload(
			sol::resolve<void(CHandle)>(&LogicManager::killEntity),
			sol::resolve<void(const std::string&)>(&LogicManager::killEntity)
		);
		userType["alertEnemy"] = &LogicManager::alertEnemy;
		userType["disablePoolingEnemies"] = &LogicManager::disablePoolingEnemies;
		userType["killAllPoolingEntities"] = &LogicManager::killAllPoolingEntities;
		userType["smartRemoveEntity"] = sol::overload(
			sol::resolve<void(CHandle)>(&LogicManager::smartRemoveEntity),
			sol::resolve<void(const std::string&)>(&LogicManager::smartRemoveEntity)
		);
		userType["fadeInBlackScreen"] = sol::overload(
			sol::resolve<void()>(&LogicManager::fadeInBlackScreen),
			sol::resolve<void(std::string)>(&LogicManager::fadeInBlackScreen)
		);
		userType["fadeOutBlackScreen"] = sol::overload(
			sol::resolve<void()>(&LogicManager::fadeOutBlackScreen),
			sol::resolve<void(std::string)>(&LogicManager::fadeOutBlackScreen)
		);
		userType["setBlackScreenAlpha"] = &LogicManager::setBlackScreenAlpha;
		userType["isBlackScreenFadeOver"] = &LogicManager::isBlackScreenFadeOver;
		userType["PlayUIAnimation"] = &LogicManager::PlayUIAnimation;
		userType["SetUIImageAlpha"] = &LogicManager::SetUIImageAlpha;

		userType["activateTotem"] = &LogicManager::activateTotem;
		userType["fillMagazineAmmo"] = &LogicManager::fillMagazineAmmo;

}

void LogicManager::renderInMenu() {
	ImGui::TextColored(ImVec4(1.0f, 0.854f, 0.078f, 1.0f), "Logic Manager");
	ImGui::Spacing();
	ImGui::Separator();
	ImGui::Spacing();
	
	ImGui::Text("Time variables");
	ImGui::DragFloat("Time Factor", &Time.scale_factor, 0.01f, 0.f, 100.0f);
	ImGui::LabelText("Scaled time", "%lf", Time.accumulated_delta);
	ImGui::LabelText("Unscaled time", "%lf", Time.accumulated_delta_unscaled);

	ImGui::Spacing();
	ImGui::Separator();
	ImGui::Spacing();
	
	ImGui::PushID("player");
	ImGui::Text("Player");
	if (ImGui::Button("Teleport to:"))
		teleportPlayer(_playerPos);
	ImGui::SameLine();
	ImGui::DragFloat3("", &_playerPos.x, 0.001f, -10000.0f, 10000.0f);
	ImGui::PopID();


	ImGui::Text("Edit entity");
	//ImGui::InputText("Entity name: ", );

	//ImGui::DragFloat("Entity life: ");
	//ImGui::Checkbox("Is immortal: ", );
	//ImGui::Checkbox("Is active: ", );

	ImGui::Spacing();
	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Edit by tag");
	//ImGui::InputText("Entity name: ", );

	//ImGui::DragFloat("Entity life: ");
	//ImGui::Checkbox("Is immortal: ", );
	//ImGui::Checkbox("Is active: ", );

	ImGui::Spacing();
	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Create entity");
	if (ImGui::Button("Melee"))
		CreateEnemyMelee(_meleePos);
	ImGui::SameLine();
	ImGui::DragFloat3("", &_meleePos.x, 0.001f, -10000.0f, 10000.0f);

	ImGui::Separator();

	ImGui::Text("Weapons: ");
	ImGui::Text("Add ammo ");
	int qt = 30;
	ImGui::DragInt("Quantity: ", &qt, 0.5f, 0, 1000);
	if (ImGui::Button("SMG")) addCommonAmmo<TCompSmgController>(qt);
	ImGui::SameLine();
	if (ImGui::Button("Rifle")) addCommonAmmo<TCompRifleController>(qt);
	ImGui::SameLine();
	if (ImGui::Button("Shotgun")) addCommonAmmo<TCompShotgunController>(qt);
	if (ImGui::Button("Special")) addSpecialAmmo(qt);


}

bool LogicManager::fetchPlayerHandles() {
	if (!player_handle.isValid()) {
		player_handle = getEntityByName("Player");
		if (!player_handle.isValid())
			return false;
	}

	bool transformValid = player_transform.isValid();
	if (!transformValid) {
		CEntity * player_ent = player_handle;
		player_transform = player_ent->getComponent<TCompTransform>();
		if (!player_transform.isValid()) return false;
	}

	return transformValid;
}

/* Entity functions. */

CHandle LogicManager::getEntity(const std::string & name) {
	return getEntityByName(name);
}

VHandles LogicManager::getEntitiesByTag(const std::string & tagName) {
	return CTagsManager::get().getAllEntitiesByTag(Utils::getID(tagName.c_str()));
}

VHandles LogicManager::getEntitiesByTag(uint32_t tag) {
	return CTagsManager::get().getAllEntitiesByTag(tag);
}

void LogicManager::removeEntitiesByTag(const std::string& tagName)
{
	std::vector<CHandle> entities = CTagsManager::get().getAllEntitiesByTag(Utils::getID(tagName.c_str()));
	if (entities.size() > 0) {
		for (CHandle& entity : entities) {
			entity.destroy();
		}
	}
}

std::vector<CHandle> LogicManager::CreatePrefab(const std::string & entityPath, float origX, float origY, float origZ) {
	CTransform entityTransform;
	entityTransform.setPosition(Vector3(origX, origY, origZ));

	TEntityParseContext ctx;
	ctx.root_transform = entityTransform;
	if (!parseScene(entityPath, ctx)) {
		Utils::dbg("Error while parsing entitypath: %s.\n", entityPath.c_str());
		return std::vector<CHandle>();
	}
	return ctx.entities_loaded;
}

std::vector<CHandle> LogicManager::CreatePrefab(const std::string & entityPath, const VEC3 & position) {
	CTransform entityTransform;
	entityTransform.setPosition(position);
	
	TEntityParseContext ctx;
	ctx.root_transform = entityTransform;
	if (!parseScene(entityPath, ctx)) {
		Utils::dbg("Error while parsing entitypath: %s.\n", entityPath.c_str());
		return std::vector<CHandle>();
	}
	return ctx.entities_loaded;
}

// Create entity function.
CHandle LogicManager::CreateEntity(const std::string & entityPath, float origX, float origY, float origZ){
	return CreateEntity(entityPath, Vector3(origX, origY, origZ));
}

CHandle LogicManager::CreateEntity(const std::string & entityPath, const VEC3 & position){
	CTransform entityTransform;
	entityTransform.setPosition(position);

	TEntityParseContext ctx;
	ctx.root_transform = entityTransform;
	if (!parseScene(entityPath, ctx)) {
		Utils::dbg("Error while parsing entitypath: %s.\n", entityPath.c_str());
		return CHandle();
	}
	return ctx.entities_loaded[0];
}

CHandle LogicManager::CreateEntity(const std::string & entityPath, const VEC3 & position, const VEC3 & angles) {
	CTransform entityTransform;
	entityTransform.setPosition(position);
	entityTransform.setRotation(QUAT::CreateFromYawPitchRoll(angles.x, angles.y, angles.z));

	TEntityParseContext ctx;
	ctx.root_transform = entityTransform;
	if (!parseScene(entityPath, ctx)) {
		Utils::dbg("Error while parsing entitypath: %s.\n", entityPath.c_str());
		return CHandle();
	}
	return ctx.entities_loaded[0];
}


CHandle LogicManager::CreateEntityAndAddImpulse(const std::string & entityPath, const VEC3 & position, const VEC3 & impulse) {
	CHandle entityToSpawn = CreateEntity(entityPath, position);
	
	// Get a rigid body if it has one and add impulse to it. For now, only works if it has a rigid body. Didn't bother implementing
	// impulse for character controllers as we are not going to use it.
	CEntity * entity = entityToSpawn;
	if (!entity) return CHandle();
	TCompRigidBody * rb = entity->getComponent<TCompRigidBody>();
	if (!rb) return CHandle();
	rb->addImpulse(impulse);

	return entityToSpawn;
}

CHandle LogicManager::CreateEnemyMelee(float origX, float origY, float origZ) {
	return CreateEnemyMelee(Vector3(origX, origY, origZ));
}

CHandle LogicManager::CreateEnemyMelee(float origX, float origY, float origZ, float yaw, float pitch, float roll) {
	Vector3 position{origX, origY, origZ};
	QUAT rotation = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);

	return EnginePool.spawn(PoolType::Melee, position, rotation);
}

CHandle LogicManager::CreateEnemyMelee(const VEC3 & position) {
	return EnginePool.spawn(PoolType::Melee, position);
}

CHandle LogicManager::CreateEnemyMelee(const VEC3 & position, const VEC3 & angle) {
	QUAT rotation = QUAT::CreateFromYawPitchRoll(angle.x, angle.y, angle.z);
	return EnginePool.spawn(PoolType::Melee, position, rotation);
}

CHandle LogicManager::CreateEnemyRanged(float origX, float origY, float origZ) {
	return CreateEnemyRanged(Vector3(origX, origY, origZ));
}

CHandle LogicManager::CreateEnemyRanged(float origX, float origY, float origZ, float yaw, float pitch, float roll) {
	Vector3 position{ origX, origY, origZ };
	QUAT rotation = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);

	return EnginePool.spawn(PoolType::Ranged, position, rotation);
}

CHandle LogicManager::CreateEnemyRanged(const VEC3 & position) {
	return EnginePool.spawn(PoolType::Ranged, position);
}

CHandle LogicManager::CreateEnemyRanged(const VEC3 & position, const VEC3 & angle) {
	QUAT rotation = QUAT::CreateFromYawPitchRoll(angle.x, angle.y, angle.z);
	return EnginePool.spawn(PoolType::Ranged, position, rotation);
}

CHandle LogicManager::CreateEnemyRangedBuffed(float origX, float origY, float origZ){
	return CreateEnemyRangedBuffed(Vector3(origX, origY, origZ));
}

CHandle LogicManager::CreateEnemyRangedBuffed(float origX, float origY, float origZ, float yaw, float pitch, float roll){
	Vector3 position{ origX, origY, origZ };
	QUAT rotation = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);
	return EnginePool.spawn(PoolType::RangedShooterBuffed, position, rotation);
}

CHandle LogicManager::CreateEnemyRangedBuffed(const VEC3 & position){
	return EnginePool.spawn(PoolType::RangedShooterBuffed, position);
}

CHandle LogicManager::CreateEnemyRangedBuffed(const VEC3 & position, const VEC3 & angle) {
	QUAT rotation = QUAT::CreateFromYawPitchRoll(angle.x, angle.y, angle.z);
	return EnginePool.spawn(PoolType::RangedShooterBuffed, position, rotation);
}

CHandle LogicManager::CreateEnemyRangedSniperGround(float origX, float origY, float origZ){
	return CreateEnemyRangedSniperGround(Vector3(origX, origY, origZ));
}

CHandle LogicManager::CreateEnemyRangedSniperGround(float origX, float origY, float origZ, float yaw, float pitch, float roll){
	Vector3 position{ origX, origY, origZ };
	QUAT rotation = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);
	return EnginePool.spawn(PoolType::RangedShooterSniperGround, position, rotation);
}

CHandle LogicManager::CreateEnemyRangedSniperGround(const VEC3 & position){
	return EnginePool.spawn(PoolType::RangedShooterSniperGround, position);
}

CHandle LogicManager::CreateEnemyRangedSniperGround(const VEC3 & position, const VEC3 & angle) {
	QUAT rotation = QUAT::CreateFromYawPitchRoll(angle.x, angle.y, angle.z);
	return EnginePool.spawn(PoolType::RangedShooterSniperGround, position, rotation);
}

CHandle LogicManager::CreateEnemyRangedSniperRoof(float origX, float origY, float origZ){
	return CreateEnemyRangedSniperRoof(Vector3(origX, origY, origZ));
}

CHandle LogicManager::CreateEnemyRangedSniperRoof(float origX, float origY, float origZ, float yaw, float pitch, float roll){
	Vector3 position{ origX, origY, origZ };
	QUAT rotation = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);
	return EnginePool.spawn(PoolType::RangedShooterSniperRoof, position, rotation);
}

CHandle LogicManager::CreateEnemyRangedSniperRoof(const VEC3 & position) {
	return EnginePool.spawn(PoolType::RangedShooterSniperRoof, position);
}

CHandle LogicManager::CreateEnemyRangedSniperRoof(const VEC3 & position, const VEC3 & angle) {
	QUAT rotation = QUAT::CreateFromYawPitchRoll(angle.x, angle.y, angle.z);
	return EnginePool.spawn(PoolType::RangedShooterSniperRoof, position, rotation);
}

CHandle LogicManager::CreateEnemyImmortal(float origX, float origY, float origZ) {
	return CreateEnemyImmortal(Vector3(origX, origY, origZ));
}

CHandle LogicManager::CreateEnemyImmortal(float origX, float origY, float origZ, float yaw, float pitch, float roll) {
	Vector3 position{ origX, origY, origZ };
	QUAT rotation = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);

	return EnginePool.spawn(PoolType::Immortal, position, rotation);
}

CHandle LogicManager::CreateEnemyImmortal(const VEC3& position) {
	return EnginePool.spawn(PoolType::Immortal, position);
}

CHandle LogicManager::CreateEnemyImmortal(const VEC3 & position, const VEC3 & angle) {
	QUAT rotation = QUAT::CreateFromYawPitchRoll(angle.x, angle.y, angle.z);
	return EnginePool.spawn(PoolType::Immortal, position, rotation);
}

CHandle LogicManager::CreateEnemy(float origX, float origY, float origZ) {
	return CreateEnemy(Vector3(origX, origY, origZ));
}

CHandle LogicManager::CreateEnemy(float origX, float origY, float origZ, float yaw, float pitch, float roll) {
	if (RNG.i(2))
		return CreateEnemyMelee(origX, origY, origZ, yaw, pitch, roll);
	else
		return CreateEnemyRanged(origX, origY, origZ, yaw, pitch, roll);
}

CHandle LogicManager::CreateEnemy(const VEC3 & position) {
	if (RNG.i(2))
		return CreateEnemyMelee(position);
	else
		return CreateEnemyRanged(position);
}

void LogicManager::alertEnemy(CHandle enemy)
{
	if (CEntity* eEnemy = enemy) {
		TMsgAlertAI msg;
		eEnemy->sendMsg(msg);
	}
}

void LogicManager::activateTotem(CHandle totem) {
	if (totem.isValid()) {
		CEntity* totemEntity = totem;
		TCompTotem* totemComponent = totemEntity->getComponent<TCompTotem>();
		if (totemComponent)
			totemComponent->spawn();
	}
}

void LogicManager::SetCHandleTimeToLive(CHandle handle, float time)
{
	EngineEntities.setCHandleTimeToLive(handle, time);
}

void LogicManager::DestroyEntity(CHandle entity) {
	if(entity.isValid())
		entity.destroy();
}

void LogicManager::smartRemoveEntity(const std::string & entityName) {
	smartRemoveEntity(getEntityByName(entityName));
}

void LogicManager::smartRemoveEntity(CHandle entity) {
	if (entity.isValid() && entity.isOfType("entity")) {
		if (!EnginePool.disableEntity(entity))
			DestroyEntity(entity);
	}
}

void LogicManager::disablePoolingEnemies() {
	EnginePool.disableAllPools();
}

void LogicManager::killAllPoolingEntities() {
	EnginePool.killAllActiveEntities();
}

// Sends an entity status msg. Activates or deactivates entity deactivating all components
// that can be deactivated by listening to the TMsgToggleComp.
void LogicManager::SetEntityActive(const std::string & entityName, bool active) {
	SetEntityActive(getEntityByName(entityName), active);
}

void LogicManager::SetEntityActive(CHandle entityHand, bool active) {
	TMsgToogleEntity msg = { active };
	entityHand.sendMsg(msg);
}

// Used for disabling all entities with a tag.
void LogicManager::SetTagEntitiesActive(const std::string & tag, bool active) {
	TMsgToogleEntity msg = { active };
	VHandles entitiesWithTag = CTagsManager::get().getAllEntitiesByTag(Utils::getID(tag.c_str()));
	for (auto & entity : entitiesWithTag)
		entity.sendMsg(msg);
}

// Deactivates controller components (they must be listening for the msh
// TMsgInputControllerStatus).
void LogicManager::SetControllerEntityActive(const std::string & entityName, bool active){
	TMsgInputControllerStatus msg = { active };
	CHandle entityHand = getEntityByName(entityName);
	entityHand.sendMsg(msg);
}

void LogicManager::SetControllerEntityActive(CHandle entityHand, bool active){
	TMsgInputControllerStatus msg = { active };
	entityHand.sendMsg(msg);
}


/* Health functions. */

// Heal entity a given amount.
void LogicManager::HealEntity(CHandle entityH, float health){
	CEntity * entity = entityH;
	TCompHealth * healthComp = entity->getComponent<TCompHealth>();
	if (!healthComp) {
		TCompName * name = entity->getComponent<TCompName>();
		if(name)
			Utils::dbg("Failed finding the health component for entity with name: %s.", name->getName());
		else
			Utils::dbg("Failed finding the health component for entity with handle id: %s.", entityH.asUnsigned());
		return;
	}

	healthComp->addHealth(health);
}

void LogicManager::HealEntity(const std::string & entityName, float health) {
	CHandle entity = getEntityByName(entityName);
	if (!entity.isValid()) {
		Utils::dbg("Failed finding entity with name: %s.", entityName);
		return;
	}
	HealEntity(entity, health);
}

// Set Entity current health.
void LogicManager::SetEntityHealth(CHandle entityH, float health) {
	CEntity * entity = entityH;
	TCompHealth * healthComp = entity->getComponent<TCompHealth>();
	if (!healthComp) {
		TCompName * name = entity->getComponent<TCompName>();
		if (name)
			Utils::dbg("Failed finding the health component for entity with name: %s.", name->getName());
		else
			Utils::dbg("Failed finding the health component for entity with handle id: %s.", entityH.asUnsigned());
		return;
	}

	healthComp->setCurrentHealth(health);
}

void LogicManager::SetEntityHealth(const std::string & entityName, float health){
	CHandle entity = getEntityByName(entityName);
	if (!entity.isValid()) {
		Utils::dbg("Failed finding entity with name: %s.", entityName);
		return;
	}
	SetEntityHealth(entity, health);
}

// Entity maximum health. (Doesn't raise current health).
void LogicManager::SetEntityMaxHealth(CHandle entityH, float maxHealth) {
	CEntity * entity = entityH;
	TCompHealth * healthComp = entity->getComponent<TCompHealth>();
	if (!healthComp) {
		TCompName * name = entity->getComponent<TCompName>();
		if (name)
			Utils::dbg("Failed finding the health component for entity with name: %s.", name->getName());
		else
			Utils::dbg("Failed finding the health component for entity with handle id: %s.", entityH.asUnsigned());
		return;
	}
	healthComp->setCurrentMaxHealth(maxHealth);
}

void LogicManager::SetEntityMaxHealth(const std::string & entityName, float maxHealth) {
	CHandle entity = getEntityByName(entityName);
	if (!entity.isValid()) {
		Utils::dbg("Failed finding entity with name: %s.", entityName);
		return;
	}
	SetEntityMaxHealth(entity, maxHealth);
}

// Set entity to be immortal.
void LogicManager::SetEntityImmortal(CHandle entityH, bool immortal) {
	CEntity * entity = entityH;
	TCompHealth * healthComp = entity->getComponent<TCompHealth>();
	if (!healthComp) {
		TCompName * name = entity->getComponent<TCompName>();
		if (name)
			Utils::dbg("Failed finding the health component for entity with name: %s.", name->getName());
		else
			Utils::dbg("Failed finding the health component for entity with handle id: %s.", entityH.asUnsigned());
		return;
	}
	healthComp->setImmortal(immortal);
}

void LogicManager::SetEntityImmortal(const std::string & entityName, bool immortal) {
	CHandle entity = getEntityByName(entityName);
	if (!entity.isValid()) {
		Utils::dbg("Failed finding entity with name: %s.", entityName);
		return;
	}
	SetEntityImmortal(entity, immortal);
}

void LogicManager::killEntity(CHandle entityHandle) {
	if (entityHandle.isValid()) {
		CEntity* entity = entityHandle;
		TCompHealth* healthComponent = entity->getComponent<TCompHealth>();
		if (healthComponent && !healthComponent->isDead())
			healthComponent->addDamage(healthComponent->getMaxHealth());
	}
}

void LogicManager::killEntity(const std::string & entityName) {
	killEntity(getEntityByName(entityName));
}

/* Player functions. */

// Player walks without considering the camera.
void LogicManager::SetPlayerMovementFreeMode(bool active) {
	// TODO.
}

bool LogicManager::setActivePlayerInput(bool active) {
	TMsgToogleComponent msg = { active };
	if (CEntity* player = getEntityByName("Player")) {
		TCompInputPlayerMovement* pMov = player->getComponent<TCompInputPlayerMovement>();
		TCompInputPlayerCombat* pCom = player->getComponent<TCompInputPlayerCombat>();
		TCompPlayerAnimator* pAni = player->getComponent<TCompPlayerAnimator>();

		if (player) {
			if (pMov && pCom) {
				pMov->sendMsgToComp<TCompInputPlayerMovement>(msg);
				pCom->sendMsgToComp<TCompInputPlayerCombat>(msg);
				if (pAni) {
					pAni->setVariable("xSpeed", 0.0f);
					pAni->setVariable("zSpeed", 0.0f);
				}
				return true;
			}
			else
				return false;
		}
	}
	return false;
}

void LogicManager::restoreFullPlayer() {
	CEntity* player = getEntityByName("Player");
	player->sendMsg(TMsgFullRestore());
}

void LogicManager::teleportPlayer(Vector3 pos) {
	CEntity* player = getEntityByName("Player");
	TCompPlayerCharacterController* controller = player->getComponent<TCompPlayerCharacterController>();
	if (player && controller ) 
		controller->teleport(pos);
}

/* Pick up functions. */
void LogicManager::SpawnPicksUpGivenDropType(const PickUpFunctions::DropTypeID & dropType, const VEC3 & position, const VEC3 & impulse, const VEC3 & minPositionNoise, const VEC3 & maxPositionNoise, const VEC3 & minImpulseNoise, const VEC3 & maxImpulseNoise) {
	PickUpFunctions::SpawnDropsManager::SpawnPickUps(dropType, &LogicManager::CreateEntityAndAddImpulse, position, impulse, minPositionNoise, maxPositionNoise, minImpulseNoise, maxImpulseNoise);
}

std::string LogicManager::getCurrentEquippedWeaponName()
{
	if (CEntity* player = getEntityByName("Player")) {
		if (TCompWeaponManager* wpnManager = player->getComponent<TCompWeaponManager>()) {
			return wpnManager->getCurrentWeapon()->getName();
		}
	}
	return "";
}

void LogicManager::addSpecialAmmo(int quantity){
	if (CEntity* player = getEntityByName("Player")) {
		if (TCompWeaponManager* wpnManager = player->getComponent<TCompWeaponManager>()) {
			wpnManager->getCurrentWeapon()->increaseSpecialAmmo(quantity);
			wpnManager->getCurrentWeapon()->updateAmmoUI();
		}
	}
}

void LogicManager::fillMagazineAmmo() {
	if (CEntity* player = getEntityByName("Player")) {
		if (TCompWeaponManager* wpnManager = player->getComponent<TCompWeaponManager>()) {
			wpnManager->getWeapon(SMG_WEAPON)->setCurrentAmmoToMagCapacity();
			wpnManager->getWeapon(SHOTGUN_WEAPON)->setCurrentAmmoToMagCapacity();
			wpnManager->getWeapon(RIFLE_WEAPON)->setCurrentAmmoToMagCapacity();
			wpnManager->getCurrentWeapon()->updateAmmoUI();
		}
	}
}

/* Dynamic decal variables. */
void LogicManager::SpawnDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const float roll) {
	EngineInstancing.addDynamicDecal(decal_name, position, lookAtPos, roll);
}

void LogicManager::SpawnDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const float roll) {
	EngineInstancing.addDynamicDecal(decal_name, position, lookAtPos, color, roll);
}

void LogicManager::SpawnDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const Vector3 & scaleProj, const float roll) {
	EngineInstancing.addDynamicDecal(decal_name, position, lookAtPos, color, scaleProj, roll);
}

void LogicManager::SpawnDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const float roll) {
	EngineInstancing.addDynamicDecal(decal_ID, position, lookAtPos, roll);
}

void LogicManager::SpawnDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const float roll) {
	EngineInstancing.addDynamicDecal(decal_ID, position, lookAtPos, color, roll);
}

void LogicManager::SpawnDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const Vector3 & scaleProj, const float roll) {
	EngineInstancing.addDynamicDecal(decal_ID, position, lookAtPos, color, scaleProj, roll);
}

int LogicManager::getDynamicDecalID(const std::string & decal_name) {
	return EngineInstancing.getDynamicDecalID(decal_name);
}

/* Player powers. */

bool LogicManager::isPerceptionActive() {
	CHandle entityH = getEntityByName("Player");
	if (!entityH.isValid()) return false;
	CEntity * entity = entityH;
	TCompPerception * perception = entity->getComponent<TCompPerception>();
	if (perception)
		return perception->isPerceptionActive();
	return false;
}

void LogicManager::setStatusPerceptionEntity(const std::string & entityName, bool active){
	CHandle entityH = getEntityByName(entityName);
	if (!entityH.isValid()) return;

	CEntity * entity = entityH;
	TCompRender * render = entity->getComponent<TCompRender>();
	if (render) {
		if (render->hasState("xray"))
			render->setActiveSecondaryState("xray", active); // Activate xray.
		
		// Activate perception. Else normal.
		if(active)
			render->setCurrentState("perception");
		else
			render->setCurrentState(0);
	}
}

void LogicManager::setStatusPerceptionEntity(CHandle entityHand, bool active){
	if (!entityHand.isValid()) return;

	CEntity * entity = entityHand;
	TCompRender * render = entity->getComponent<TCompRender>();
	if (render) {
		if (render->isStateActive("vanish")) return; // ignore if vanish.

		if (render->hasState("xray"))
			render->setActiveSecondaryState("xray", active); // Activate xray.
		
		// Activate perception. Else normal.
		if (active)
			render->setCurrentState("perception");
		else
			render->setCurrentState(0);
	}
}

void LogicManager::setStatusVanishEntity(CHandle entityHand) {
	if (!entityHand.isValid()) return;
	CEntity * entity = entityHand;
	TCompTransform* trans = entity->getComponent<TCompTransform>();

	setStatusVanishEntity(entityHand, trans->getPosition());
}

void LogicManager::setStatusVanishEntity(CHandle entityHand, Vector3 position) {
	if (!entityHand.isValid()) return;

	CEntity * entity = entityHand;
	TCompRender * render = entity->getComponent<TCompRender>();
	if (render) {
		render->setActiveSecondaryState("xray", false);

		// Activate perception. Else normal.
		render->setCurrentState("vanish");
	}
	EngineAudio.playEvent(_vanishEvent, position);
}


/* Trauma variables. */

void LogicManager::SendDeltaTrauma(const std::string & entityName, float traumaDelta) {
	SendDeltaTrauma(getEntityByName(entityName), traumaDelta);
}

void LogicManager::SendDeltaTrauma(CHandle entity, float traumaDelta) {
	if (!entity.isValid()) return;
	TMsgTraumaDelta msg = { traumaDelta };
	entity.sendMsg(msg);
}

void LogicManager::SendDeltaTrauma(const std::string & entityName, float traumaDelta, float maxTraumaReachable) {
	SendDeltaTrauma(getEntityByName(entityName), traumaDelta, maxTraumaReachable);
}

void LogicManager::SendDeltaTrauma(CHandle entity, float traumaDelta, float maxTraumaReachable) {
	if (!entity.isValid()) return;
	TMsgTraumaDelta msg = { traumaDelta, maxTraumaReachable };
	entity.sendMsg(msg);
}

/* Spawner */
void LogicManager::SpawnExplosion(TCompTransform* transform, float innerRadius, float outerRadius, float dmg) {

	//1. Look for candidates
	std::vector<CHandle> hitEntites = PhysUtils::sphericalOverlap(transform, outerRadius);

	//2. Compute the dmg based on distance
	for (CHandle handle : hitEntites) {
		if(CEntity* e = handle)
			if (TCompTransform * eTrans = e->getComponent<TCompTransform>()) {

				VEC3 dir = eTrans->getPosition() + VEC3(0.0f, 0.4f, 0.0f) - transform->getPosition();
				float distance = abs(VEC3::Distance(transform->getPosition(), eTrans->getPosition()));
				float factor = 1;
				if (distance > innerRadius)
					factor -= ((distance - innerRadius) / (outerRadius - innerRadius));
				factor *= factor;

				TMsgExplosion msg;
				msg.dmg = dmg * factor;
				msg.throwDir = dir;
				msg.throwForce = maxThrowForce * factor;

				//3. Send dmg messages
				e->sendMsg(msg);
				alertEnemy(handle);
			}
	}
}

/* Particles */
particles::CSystem * LogicManager::SpawnParticle(const std::string & particle_name, CHandle entityOwner, const Vector3 & offset) {
	particles::TEmitter * emitter = EngineResources.getEditableResource(particle_name)->as<particles::TEmitter>();
	if (emitter)
		return EngineParticles.launchSystem(emitter, entityOwner, offset);
	return nullptr;
}

particles::CSystem * LogicManager::SpawnParticle(const std::string & particle_name, CHandle entityOwner, const std::string & boneToFollow, const Vector3 & offset) {
	particles::TEmitter * emitter = EngineResources.getEditableResource(particle_name)->as<particles::TEmitter>();
	if (emitter)
		return EngineParticles.launchSystem(emitter, entityOwner, boneToFollow, offset);
	return nullptr;
}

particles::CSystem * LogicManager::SpawnParticle(const std::string & particle_name, const Vector3 & spawnPosition) {
	particles::TEmitter * emitter = EngineResources.getEditableResource(particle_name)->as<particles::TEmitter>();
	if (emitter) {
		CTransform transform;
		transform.setPosition(spawnPosition);
		return EngineParticles.launchSystem(emitter, transform);
	}
	return nullptr;
}

particles::CSystem * LogicManager::SpawnParticle(const std::string & particle_name, const CTransform & transform) {
	particles::TEmitter * emitter = EngineResources.getEditableResource(particle_name)->as<particles::TEmitter>();
	if (emitter) {
		CTransform particleTransf = transform;
		return EngineParticles.launchSystem(emitter, particleTransf);
	}
	return nullptr;
}

particles::CSystem * LogicManager::SpawnParticle(const std::string & particle_name, const Vector3 & spawnPosition, const Vector3 & lookAtPos) {
	particles::TEmitter * emitter = EngineResources.getEditableResource(particle_name)->as<particles::TEmitter>();

	if (emitter) {
		CTransform transform;
		transform.lookAt(spawnPosition, lookAtPos);
		return EngineParticles.launchSystem(emitter, transform);
	}
	return nullptr;
}

particles::CSystem * LogicManager::SpawnParticle(const std::string & particle_name, const Vector3 & spawnPosition, const Vector3 & lookAtPos, float roll) {
	particles::TEmitter * emitter = EngineResources.getEditableResource(particle_name)->as<particles::TEmitter>();
	if (emitter) {
		CTransform transform;
		transform.lookAt(spawnPosition, lookAtPos);
		float yaw, pitch, rollToDiscard;
		transform.getAngles(&yaw, &pitch, &rollToDiscard);
		transform.setAngles(yaw, pitch, roll);
		return EngineParticles.launchSystem(emitter, transform);
	}
	return nullptr;
}

void LogicManager::removeSpawnedParticle(particles::CSystem * systemToRemove, float timeBeforeRemoval) {
	EngineParticles.disableSystemDelayed(systemToRemove, timeBeforeRemoval);
}

void LogicManager::spawnFXOnHitPos(const Vector3 & hitPosition, const Vector3 & hitNormal, const std::string & onHitLuaFunction, const std::string & surfaceType){
	Vector3 lookAt = hitPosition + hitNormal;
	EngineScripting.call(onHitLuaFunction, hitPosition, lookAt, surfaceType);
}

void LogicManager::SpawnCriticalHitboxFXOnHit(const Vector3 & hitPosition, const Vector3 & hitNormal, int enemyType){
	Vector3 lookAt = hitPosition + hitNormal;
	EngineScripting.call("onCriticalHiboxHit", hitPosition, lookAt, enemyType);
}

CHandle LogicManager::getHUDUI(){
	return getEntityByName("HUD");
}

/* Camera variables. */
// Camera functions and variables.

// Adds a camera for blending to the camera named Camera-Output.
void LogicManager::BlendCamera(const std::string & cameraName, float blendTime, const std::string & interpolation) {
	EngineCameraMixer.blendCamera(getEntityByName(cameraName), blendTime, Interpolator::InterpolatorManager::Instance().getInterpolator(interpolation));
}

// Adds a camera for blending to the camera named Camera-Output.
void LogicManager::BlendCamera(const std::string & cameraName, float blendTime, Interpolator::IInterpolator * interpolation) {
	EngineCameraMixer.blendCamera(getEntityByName(cameraName), blendTime, interpolation);
}

void LogicManager::setDefaultCamera(CHandle CameraHandle) {
	EngineCameraMixer.setDefaultCamera(CameraHandle);
}

void LogicManager::setDefaultCamera(const std::string& cameraName) {
	EngineCameraMixer.setDefaultCamera(getEntityByName(cameraName));
}

void LogicManager::setOutputCamera(const std::string& cameraName) {
	EngineCameraMixer.setOutputCamera(getEntityByName(cameraName));
}

void LogicManager::setOutputCamera(CHandle CameraHandle){
	EngineCameraMixer.setOutputCamera(CameraHandle);
}

CHandle LogicManager::getDefaultCamera() {
	return EngineCameraMixer.getMixerDefaultCamera();
}

CHandle LogicManager::getOutputCamera() {
	return EngineCameraMixer.getMixerOuputCamera();
}

void LogicManager::StartCinematic(const std::string & CinematicPath){
	EngineCinematicSequencer.StartCinematic(CinematicPath);
}

void LogicManager::EndCinematic(){
	EngineCinematicSequencer.EndCinematic();
}

void LogicManager::PlayLevelSequence(const std::string & LevelSequencePath){
	EngineCinematicSequencer.PlayLevelSequence(LevelSequencePath);
}

void LogicManager::EndLevelSequence(const std::string & LevelSequencePath){
	EngineCinematicSequencer.EndLevelSequence(LevelSequencePath);
}

void LogicManager::CallGlobalScriptFunction(const std::string & FunctionToCall) {
	EngineScripting.call(FunctionToCall);
}

bool LogicManager::BindScriptToComponentEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent, const std::string & FunctionToFireOnEventCall){
	return EngineScripting.BindScriptToComponentEvent(CompHandleFiringEvent, EventID, CompHandleOfScriptToBindToEvent, FunctionToFireOnEventCall);
}

bool LogicManager::UnbindScriptToComponentEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent){
	return EngineScripting.UnbindScriptToComponentEvent(CompHandleFiringEvent, EventID, CompHandleOfScriptToBindToEvent);
}

void LogicManager::BindTimedScriptEvent(CHandle CompHandleOfScriptToBindToEvent, const std::string & FunctionToFireOnEventCall, float TimeBeforeCalling) {
	EngineScripting.BindTimedScriptEvent(CompHandleOfScriptToBindToEvent, FunctionToFireOnEventCall, TimeBeforeCalling);
}

void LogicManager::ClearBindTimedScriptEvents() {
	EngineScripting.ClearTimedScriptEvents();
}

void LogicManager::CallEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent){
	EngineScripting.CallEvent(CompHandleFiringEvent, EventID, CompHandleOfScriptToBindToEvent);
}

void LogicManager::CallEventAllScript(CHandle CompHandleFiringEvent, const std::string & EventID) {
	EngineScripting.CallEventAllScript(CompHandleFiringEvent, EventID);
}

#include "components/postFX/comp_postFX_color_grading.h"
#include "components/postFX/comp_postFX_vignetting.h"
#include "components/postFX/comp_postFX_burningfade.h"
#include "components/postFX/comp_postFX_black_bars.h"

void LogicManager::activateColorGrading(const std::string & cameraName, bool status) {
	if(CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXColorGrading * colorGrading = e->getComponent<TCompPostFXColorGrading>())
			colorGrading->setActive(status);
}

void LogicManager::colorGradingCurrentLUTAmount(const std::string & cameraName, float nCurrentGradAmount){
	if(CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXColorGrading * colorGrading = e->getComponent<TCompPostFXColorGrading>())
			colorGrading->setColorGradingCurrentAmount(nCurrentGradAmount);
}

void LogicManager::colorGradingFallTime(const std::string & cameraName, float nFallTime){
	if(CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXColorGrading * colorGrading = e->getComponent<TCompPostFXColorGrading>())
			colorGrading->setColorGradingFallTime(nFallTime);
}

void LogicManager::colorGradingLutTexture(const std::string & cameraName, const std::string & nLutPath){
	if(CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXColorGrading * colorGrading = e->getComponent<TCompPostFXColorGrading>())
			colorGrading->setColorGradingTextureToLoad(nLutPath);
}

void LogicManager::activateVignetting(const std::string & cameraName, bool status) {
	if(CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXVignetting * vignetting = e->getComponent<TCompPostFXVignetting>())
			vignetting->setActive(status);
}

void LogicManager::vignettingAmount(const std::string & cameraName, float value){
	if(CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXVignetting * vignetting = e->getComponent<TCompPostFXVignetting>())
			vignetting->setVignettingValue(value);
}

void LogicManager::vignettingFallTime(const std::string & cameraName, float nFallTime){
	if(CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXVignetting * vignetting = e->getComponent<TCompPostFXVignetting>())
			vignetting->setVignettingFallTime(nFallTime);
}

void LogicManager::activateBurningFade(const std::string & cameraName, bool status) {
	if (CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXBurningFade * burningFade = e->getComponent<TCompPostFXBurningFade>())
			burningFade->setActive(status);
}

void LogicManager::activateCinematicBars(const std::string & cameraName, bool status) {
	if (CEntity * e = getEntityByName(cameraName))
		if(TCompPostFXBlackBars * blackBars = e->getComponent<TCompPostFXBlackBars>())
			blackBars->setBlackBars(status);
}

void LogicManager::activateCinematicBars(const std::string & cameraName, bool status, float nTimeToGetToGoal){
	if (CEntity * e = getEntityByName(cameraName))
		if (TCompPostFXBlackBars * blackBars = e->getComponent<TCompPostFXBlackBars>())
			blackBars->setBlackBars(status, nTimeToGetToGoal);
}

void LogicManager::showCinematicBarsFull(const std::string & cameraName) {
	if (CEntity * e = getEntityByName(cameraName))
		if (TCompPostFXBlackBars * blackBars = e->getComponent<TCompPostFXBlackBars>())
			blackBars->setBlackBarsFull();
}

void LogicManager::hideCinematicBarsFull(const std::string & cameraName) {
	if (CEntity * e = getEntityByName(cameraName))
		if (TCompPostFXBlackBars * blackBars = e->getComponent<TCompPostFXBlackBars>())
			blackBars->setBlackBarsHidden();
}

void LogicManager::setFogLerp(const std::string & cameraName, float nLerp) {
	if (CEntity * e = getEntityByName(cameraName))
		if (TCompPostFXFog * fog = e->getComponent<TCompPostFXFog>())
			fog->setFogLerp(nLerp);
}

/* Time */

void LogicManager::pauseTime() {
	prevTimeScaleFactor = Time.scale_factor;
	Time.scale_factor = 0.0f;
}

void LogicManager::resumeTime() {
	Time.scale_factor = prevTimeScaleFactor != 0.0f ? prevTimeScaleFactor : 1.0f;
}

void LogicManager::toggleTime() {
	if (Time.scale_factor == 0.0f)
		resumeTime();
	else
		pauseTime();
}

void LogicManager::setTimeScale(float tScale) {
	Time.scale_factor = tScale;
}

void LogicManager::playEvent(std::string path) {
	EngineAudio.playEvent(path);
}

void LogicManager::playEvent(std::string path, VEC3 location)
{
	EngineAudio.playEvent(path, location);
}

void LogicManager::setEventParameter(std::string path, std::string paramName, float paramValue)
{
	EngineAudio.setEventParameter(path, paramName, paramValue);
}

void LogicManager::stopEvent(std::string path)
{
	EngineAudio.stopEvent(path);
}

void LogicManager::stopAllSounds()
{
	EngineAudio.stopAllSounds();
}

void LogicManager::stopMusic()
{
	EngineAudio.setEventParameter(_musicPath, _battleParamName, 0.0f);
	EngineAudio.setEventParameter(_musicPath, _hordeParamName, 0.0f);
	EngineAudio.setEventParameter(_musicPath, _defaultParamName, 0.0f);
	EngineAudio.setEventParameter(_musicPath, _arenaParamName, 0.0f);
}

void LogicManager::playMusic(std::string name)
{
	stopMusic();
	if (name == _arenaParamName) {
		EngineAudio.setEventParameter(_musicPath, _arenaParamName, 1.0f);
	}
	else if (name == _battleParamName) {
		EngineAudio.setEventParameter(_musicPath, _battleParamName, 1.0f);
	}
	else if (name == _defaultParamName) {
		EngineAudio.setEventParameter(_musicPath, _defaultParamName, 1.0f);
	}
	else if (name == _hordeParamName) {
		EngineAudio.setEventParameter(_musicPath, _hordeParamName, 1.0f);
	}
}


/* Input Module */
void LogicManager::setActiveModuleInput(bool active) {
	EngineInput.setActive(active);
}

void LogicManager::setActiveMouse(bool active) {
	dynamic_cast<CDeviceMouse*>(EngineInput.getDevice("mouse"))->setActive(active);
}

void LogicManager::setCenterMouseActive(bool active) {
	EngineInput.mouse().setCenterMouseMode(active);
}

void LogicManager::showCursor(bool active) {
	int displayCount = ShowCursor(active);
	while (!active && displayCount >= 0) {
		displayCount = ShowCursor(active);
	}
	while (active && displayCount < 0) {
		displayCount = ShowCursor(active);
	}
}

void LogicManager::setActiveKeyboard(bool active) {
	dynamic_cast<CDeviceKeyboard*>(EngineInput.getDevice("keyboard"))->setActive(active);
}

void LogicManager::setActiveGamepad(bool active) {
	dynamic_cast<CDevicePadXbox*>(EngineInput.getDevice("gamepad"))->setActive(active);
}

/* UI Module */
void LogicManager::activateWidget(std::string widget_name) {
	Engine.getUI().activateWidget(widget_name);
}
void LogicManager::deactivateWidget(std::string widget_name) {
	Engine.getUI().deactivateWidget(widget_name);
}

bool LogicManager::isWidgetActive(std::string widget_name) {
	UI::CWidget dummy;
	return EngineUI.findWidgetByName(dummy, widget_name);
}

void LogicManager::updateRatioFromProgress(std::string widget_name, float ratio) {
	UI::CProgress* progressBar = dynamic_cast<UI::CProgress*>(Engine.getUI().getWidgetByAlias(widget_name));
	if (progressBar)
		progressBar->setRatio(ratio);
}

void LogicManager::setChildWidgetVisibleByTime(const std::string childName, const std::string parentName, bool status, float time)
{
	Engine.getUI().setChildWidgetVisibleByTime(childName, parentName, status, time);
}

void LogicManager::setControlUIButtonsByDevice(bool isControllerActive)
{
	
	if (isControllerActive) {
		// Perception button ///////////////////////////////////////////////////////////////////////////////////////////////////
		if (UI::CImage* percBtnXbox = EngineUI.getImageWidgetByName("abilities_perception", "perception_button_xbox"))
			percBtnXbox->getParams()->visible = true;
		if (UI::CImage* percBtn = EngineUI.getImageWidgetByName("abilities_perception", "perception_button"))
			percBtn->getParams()->visible = false;
		// Special button //////////////////////////////////////////////////////////////////////////////////////////////////////
		if (UI::CImage * specBtnXbox = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button_xbox"))
			specBtnXbox->getParams()->visible = true;
		if (UI::CImage * specBtn = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button"))
			specBtn->getParams()->visible = false;
		// Special SMG button //////////////////////////////////////////////////////////////////////////////////////////////////////
		if (UI::CImage* specBtnXbox = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button_smg_xbox"))
			specBtnXbox->getParams()->visible = true;
		if (UI::CImage* specBtn = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button_smg"))
			specBtn->getParams()->visible = false;
		// Scythe button //////////////////////////////////////////////////////////////////////////////////////////////////////
		if (UI::CImage* scytBtnXbox = EngineUI.getImageWidgetByName("abilities_scythe", "scythe_button_xbox"))
			scytBtnXbox->getParams()->visible = true;
		if (UI::CImage* scytBtn = EngineUI.getImageWidgetByName("abilities_scythe", "scythe_button"))
			scytBtn->getParams()->visible = false;
	}
	else {
		// Perception button ///////////////////////////////////////////////////////////////////////////////////////////////////
		if (UI::CImage * percBtnXbox = EngineUI.getImageWidgetByName("abilities_perception", "perception_button_xbox"))
			percBtnXbox->getParams()->visible = false;
		if (UI::CImage * percBtn = EngineUI.getImageWidgetByName("abilities_perception", "perception_button"))
			percBtn->getParams()->visible = true;
		// Special button //////////////////////////////////////////////////////////////////////////////////////////////////////
		if (UI::CImage * specBtnXbox = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button_xbox"))
			specBtnXbox->getParams()->visible = false;
		if (UI::CImage * specBtn = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button"))
			specBtn->getParams()->visible = true;
		// Special SMG button //////////////////////////////////////////////////////////////////////////////////////////////////////
		if (UI::CImage* specBtnXbox = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button_smg"))
			specBtnXbox->getParams()->visible = true;
		if (UI::CImage* specBtn = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button_smg_xbox"))
			specBtn->getParams()->visible = false;
		// Scythe button //////////////////////////////////////////////////////////////////////////////////////////////////////
		if (UI::CImage* scytBtnXbox = EngineUI.getImageWidgetByName("abilities_scythe", "scythe_button_xbox"))
			scytBtnXbox->getParams()->visible = false;
		if (UI::CImage* scytBtn = EngineUI.getImageWidgetByName("abilities_scythe", "scythe_button"))
			scytBtn->getParams()->visible = true;
	}
}

void LogicManager::fadeInBlackScreen() {
	auto* image = EngineUI.getImageWidgetByName("black_screen", "black_screen_image");
	image->playAnimation("BlackScreenFadeIn", false);
}

void LogicManager::fadeInBlackScreen(std::string animation)
{
	auto* image = EngineUI.getImageWidgetByName("black_screen", "black_screen_image");
	image->playAnimation(animation, false);
}

void LogicManager::fadeOutBlackScreen() {
	auto* image = EngineUI.getImageWidgetByName("black_screen", "black_screen_image");
	image->playAnimation("BlackScreenFadeOut", false);
}

void LogicManager::fadeOutBlackScreen(std::string animation)
{
	auto* image = EngineUI.getImageWidgetByName("black_screen", "black_screen_image");
	image->playAnimation(animation, false);
}

void LogicManager::setBlackScreenAlpha(float alpha) {
	auto* blackScreen = EngineUI.getImageWidgetByName("black_screen", "black_screen_image");
	if (blackScreen) blackScreen->getImageParams()->color.w = Maths::clamp(alpha, 0.0f, 1.0f);
}

bool LogicManager::isBlackScreenFadeOver() {
	auto* image = EngineUI.getImageWidgetByName("black_screen", "black_screen_image");
	return (image->getCurrentAnimationPercentage() == 0.0f);
}

void LogicManager::PlayUIAnimation(const std::string & ParentName, const std::string & WidgetName, const std::string & Animation, bool Loop, float StartTime) {
	auto* image = EngineUI.getImageWidgetByName(ParentName, WidgetName);
	image->playAnimation(Animation, Loop, StartTime);
}

void LogicManager::SetUIImageAlpha(const std::string& ParentName, const std::string& WidgetName, float alpha) {
	auto* image = EngineUI.getImageWidgetByName(ParentName, WidgetName);
	image->getImageParams()->color.w = Maths::clamp(alpha, 0.0f, 255.0f);
}

void LogicManager::ChangeToMenuState(){
	//Activate the loading screen widget
	Engine.getUI().activateWidget("loading_screen");
	//Disable the main menu widget
	Engine.getUI().deactivateWidget("hud");
	//Load the next gamestate entities
	EngineModules.changeToGamestate("gs_main_menu");
}

float LogicManager::GetAmbientBoost(){
	return EngineRender.GetAmbientBoost();
}

float LogicManager::GetAmbientLightIntensity(){
	return EngineRender.GetAmbientLightIntensity();
}

float LogicManager::GetExposureAdjustment(){
	return EngineRender.GetExposureAdjustment();
}

float LogicManager::GetReflectionIntensity(){
	return EngineRender.GetReflectionIntensity();
}

float LogicManager::GetGlobalSkyboxLerp() {
	return EngineRender.GetGlobalSkyboxLerp();
}

void LogicManager::SetRenderAmbientVariables(float AmbientBoost, float AmbientLightIntensity, float ExposureAdjustment, float ReflectionIntensity, float SkyboxLerp){
	EngineRender.SetRenderAmbientVariables(AmbientBoost, AmbientLightIntensity, ExposureAdjustment, ReflectionIntensity, SkyboxLerp);
}

void LogicManager::SetAmbientBoost(float AmbientBoost){
	EngineRender.SetAmbientBoost(AmbientBoost);
}

void LogicManager::SetAmbientLightIntensity(float AmbientLightIntensity){
	EngineRender.SetAmbientLightIntensity(AmbientLightIntensity);
}

void LogicManager::SetExposureAdjustment(float ExposureAdjustment){
	EngineRender.SetExposureAdjustment(ExposureAdjustment);
}

void LogicManager::SetReflectionIntensity(float ReflectionIntensity){
	EngineRender.SetReflectionIntensity(ReflectionIntensity);
}

void LogicManager::restorePlayerToLastCheckpoint() {
	TCompCheckpoint::restorePlayer();
}

void LogicManager::restorePlayerToSpawnCheckpoint() {
	TCompCheckpoint* spawn = TCompCheckpoint::getSpawnCheckpoint();
	spawn->restorePlayerToCheckpoint();
}

void LogicManager::resetCheckpoints() {
	TCompCheckpoint::reset();
}

CHandle LogicManager::getSpawnCheckpoint() {
	return TCompCheckpoint::getSpawnCheckpoint();
}

CHandle LogicManager::getCurrentCheckpoint() {
	return TCompCheckpoint::getCurrentCheckpoint();
}

TCompCheckpoint * LogicManager::getSpawnCheckpointComponent() {
	return getSpawnCheckpoint();
}

TCompCheckpoint * LogicManager::getCurrentCheckpointComponent() {
	return getCurrentCheckpoint();
}

/* Utils. */
// Returns true if smaller or equal than max distance squared.
bool LogicManager::isPlayerClose(const Vector3 & position, float maxDistanceSquared) {
	if (!fetchPlayerHandles()) return false;

	TCompTransform * transform = player_transform;
	return Vector3::DistanceSquared(transform->getPosition(), position) <= maxDistanceSquared;
}

int LogicManager::getRandomInt(int min, int max) {
	static std::mt19937_64 generator(std::time(0));
	static std::uniform_int_distribution<int> uniformDistribution(min, max);
	return uniformDistribution(generator);
}

float LogicManager::getRandomFloat(float min, float max) {
	static std::mt19937_64 generator(std::time(0));
	static std::uniform_real_distribution<float> uniformDistribution(min, max);
	const float unitRandomValue = uniformDistribution(generator);
	return min + (max - min) * unitRandomValue;
}

Vector3 LogicManager::TransformVector(Vector3 & vectorToTransform, Quaternion & rotationToApply) {
	return Vector3::Transform(vectorToTransform, rotationToApply);
}

void LogicManager::setFramerateLimit(int fps) {
	Application.setFramerateLimit(fps);
}

int LogicManager::getFramerateLimit() {
	return Application.getFramerateLimit();
}

void LogicManager::exitApplication() {
	Application.sendQuitMsg();
}

void LogicManager::changeToGamestate(const std::string & gs) {
	EngineModules.changeToGamestate(gs);
}

void LogicManager::changeToGamestateNonBlocking(const std::string & gs) {
	EngineModules.changeToGamestateNonBlocking(gs);
}
