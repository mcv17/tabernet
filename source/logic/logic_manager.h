#pragma once

/* This class is used for fabricating new entities,
controlling cameras, creating areas of damage, ...
Any call can be added and called from code or scripts,
it will do the necessary work to execute it. */

#include "handle/handle_def.h"
#include "components/common/comp_transform.h"
#include "modules/module_physics.h"
#include "particles/module_particles.h"

// Spawn pickups.
#include "../../components/pickups/PickUpUtils.h"

class TCompCheckpoint;
class Interpolator::IInterpolator;

class LogicManager {
	CHandle player_handle; // Handle for the player.
	CHandle player_transform; // Handle for the player transform.

	float traumaMaxDistanceEvent = 10.f; // How far before events don't add trauma (an explosion far away).

	// Max force applied based on distance to explosion
	float maxThrowForce = 5000.0f;

	float prevTimeScaleFactor = 1.0f;

	bool fetchPlayerHandles();

	// Audio
	const std::string _musicPath        = "event:/Music/AllMusic";
	const std::string _battleParamName  = "Battle";
	const std::string _hordeParamName   = "Horde";
	const std::string _defaultParamName = "Default";
	const std::string _arenaParamName   = "Arena";
	const std::string _vanishEvent      = "event:/Effects/Vanish";
	

public:
	static LogicManager & Instance();
	static void declareInLua();

	void renderInMenu();

	CHandle getEntity(const std::string & name);
	VHandles getEntitiesByTag(const std::string & tagName);
	VHandles getEntitiesByTag(uint32_t tag);
	void removeEntitiesByTag(const std::string& tagName);
	
	/* Create entity functions. */

	std::vector<CHandle> CreatePrefab(const std::string & entityPath, float origX, float origY, float origZ); // Create entity function.
	std::vector<CHandle> CreatePrefab(const std::string & entityPath, const VEC3 & position); // Create entity function.
	CHandle CreateEntity(const std::string & entityPath, float origX, float origY, float origZ); // Create entity function.
	CHandle CreateEntity(const std::string & entityPath, const VEC3 & position); // Create entity function.
	CHandle CreateEntity(const std::string & entityPath, const VEC3 & position, const VEC3 & angles);
	CHandle CreateEntityAndAddImpulse(const std::string & entityPath, const VEC3 & position, const VEC3 & impulse); // Create entity and add impulse to it (only works if it has a rigid body).
	CHandle CreateEnemyMelee(float origX, float origY, float origZ); // Create enemy melee.
	CHandle CreateEnemyMelee(float origX, float origY, float origZ, float yaw, float pitch, float roll); // Create enemy melee with rotation.
	CHandle CreateEnemyMelee(const VEC3 & position); // Create enemy melee.
	CHandle CreateEnemyMelee(const VEC3 & position, const VEC3 & angle); // Create enemy melee.

	CHandle CreateEnemyRanged(float origX, float origY, float origZ); // Create enemy ranged.
	CHandle CreateEnemyRanged(float origX, float origY, float origZ, float yaw, float pitch, float roll); // Create enemy ranged with rotation.
	CHandle CreateEnemyRanged(const VEC3 & position); // Create enemy ranged.
	CHandle CreateEnemyRanged(const VEC3 & position, const VEC3 & angle); // Create enemy melee.

	CHandle CreateEnemyRangedBuffed(float origX, float origY, float origZ); // Create enemy ranged.
	CHandle CreateEnemyRangedBuffed(float origX, float origY, float origZ, float yaw, float pitch, float roll); // Create enemy ranged with rotation.
	CHandle CreateEnemyRangedBuffed(const VEC3 & position); // Create enemy ranged.
	CHandle CreateEnemyRangedBuffed(const VEC3 & position, const VEC3 & angle); // Create enemy melee.

	CHandle CreateEnemyRangedSniperGround(float origX, float origY, float origZ); // Create enemy ranged.
	CHandle CreateEnemyRangedSniperGround(float origX, float origY, float origZ, float yaw, float pitch, float roll); // Create enemy ranged with rotation.
	CHandle CreateEnemyRangedSniperGround(const VEC3 & position); // Create enemy ranged.
	CHandle CreateEnemyRangedSniperGround(const VEC3 & position, const VEC3 & angle); // Create enemy melee.

	CHandle CreateEnemyRangedSniperRoof(float origX, float origY, float origZ); // Create enemy ranged.
	CHandle CreateEnemyRangedSniperRoof(float origX, float origY, float origZ, float yaw, float pitch, float roll); // Create enemy ranged with rotation.
	CHandle CreateEnemyRangedSniperRoof(const VEC3 & position); // Create enemy ranged.
	CHandle CreateEnemyRangedSniperRoof(const VEC3 & position, const VEC3 & angle); // Create enemy melee.

	CHandle CreateEnemyImmortal(float origX, float origY, float origZ); // Create enemy immortal.
	CHandle CreateEnemyImmortal(float origX, float origY, float origZ, float yaw, float pitch, float roll); // Create enemy immortal with rotation.
	CHandle CreateEnemyImmortal(const VEC3& position); // Create enemy immortal.
	CHandle CreateEnemyImmortal(const VEC3 & position, const VEC3 & angle); // Create enemy melee.

	CHandle CreateEnemy(float origX, float origY, float origZ); // Create a random enemy.
	CHandle CreateEnemy(float origX, float origY, float origZ, float yaw, float pitch, float roll); // Create a random enemy with rotation.
	CHandle CreateEnemy(const VEC3 & position); // Create a random enemy.

	void alertEnemy(CHandle enemy);
	void activateTotem(CHandle totem);

	void SetCHandleTimeToLive(CHandle handle, float time); // Sets time to live for a CHandle.

	void DestroyEntity(CHandle entity);
	// Tries to remove the entity via pooling, if fails destroys the entity
	void smartRemoveEntity(const std::string& entityName);
	void smartRemoveEntity(CHandle entity);

	void disablePoolingEnemies();
	void killAllPoolingEntities();

	/* Set entity controllers functions. */

	// Sends an entity status msg. Activates or deactivates entity components listening to the TMsgToggleComp.
	void SetEntityActive(const std::string & entityName, bool active);
	void SetEntityActive(CHandle entityHand, bool active);

	// Used for disabling all entities with a tag.
	void SetTagEntitiesActive(const std::string & tag, bool active);

	// Activates/Deactivates controller components. They must be listening for the msg TMsgInputControllerStatus.
	void SetControllerEntityActive(const std::string & entityName, bool active);
	void SetControllerEntityActive(CHandle entityHand, bool active);

	/* Component functions. */

	// Sets the component of an entity to active.
	//template <class TComp> void SetEntityComponentActive(const std::string & entityName, bool active);
	template <typename TComp> 
	void SetEntityComponentActive(CHandle entity, bool active) {
		CEntity* entityPtr = entity;
		TComp* component = entityPtr->getComponent<TComp>();
		component->setActive(false);
	}
	
	// TODO: I can't seem to figure out why this function isn't compiling
	// Used for disabling all the components of the specific type from the entities with tag
	template <typename TComp> 
	void SetTagEntitiesComponentActive(const std::string & tag, bool active) {
		TMsgToogleComponent msg = { active };
		//VHandles entitiesWithTag = CTagsManager::get().getAllEntitiesByTag(Utils::getID(tag.c_str()));
		//for (auto & entity : entitiesWithTag)
		//	entity.sendMsgToComp<TComp>(msg);
	}

	/* Health functions. */
	void HealEntity(CHandle entityH, float health); // Heal entity a given amount.
	void HealEntity(const std::string & entityName, float health); // Heal entity a given amount.
	
	void SetEntityHealth(CHandle entityH, float health); // Set Entity current health.
	void SetEntityHealth(const std::string & entityName, float health); // Set Entity current health.

	void SetEntityMaxHealth(CHandle entityH, float maxHealth); // Entity maximum health. (Doesn't raise current health)
	void SetEntityMaxHealth(const std::string & entityName, float maxHealth); // Entity maximum health. (Doesn't raise current health)
	
	void SetEntityImmortal(CHandle entityH, bool immortal); // Set entity to be immortal.
	void SetEntityImmortal(const std::string & entityName, bool immortal); // Set entity to be immortal.

	void killEntity(CHandle entityHandle);
	void killEntity(const std::string& entityName);

	/* Player functions. */
	void SetPlayerMovementFreeMode(bool active); // Player walks without considering the camera. TODO.
	bool setActivePlayerInput(bool active);
	void restoreFullPlayer();
	void teleportPlayer(Vector3 pos);

	/* Pick up functions. */

	// Spawns pickups based on the drop type sent as parameter. An impulse vector can be added to send pickups in a given direction.
	void SpawnPicksUpGivenDropType(const PickUpFunctions::DropTypeID & dropType, const VEC3 & position, const VEC3 & impulse = Vector3(),
		const VEC3 & minPositionNoise = Vector3(), const VEC3 & maxPositionNoise = Vector3(), const VEC3 & minImpulseNoise = Vector3(), const VEC3 & maxImpulseNoise = Vector3());

	// Ammo pick up functions.
	template <class TWeapon>
	void addCommonAmmo(int quantity) {
		const char* wpn_name = getWeaponEntityName<TWeapon>();
		if (CEntity * e = getEntityByName(wpn_name)) {
			if (TWeapon* c = e->getComponent<TWeapon>()) {
				c->increaseStoredAmmo(quantity);
				if(getCurrentEquippedWeaponName() == wpn_name){
					c->updateAmmoUI();
				}
			}
		}
	}

	template <class TWeapon>
	const char* getWeaponEntityName() {
		if (typeid(TWeapon) == typeid(TCompSmgController)) return "Submachinegun-Weapon";
		else if (typeid(TWeapon) == typeid(TCompRifleController))return "Rifle-Weapon";
		else if (typeid(TWeapon) == typeid(TCompShotgunController)) return "Shotgun-Weapon";
		else    return nullptr;
	}

	std::string getCurrentEquippedWeaponName();

	void addSpecialAmmo(int quantity);
	void fillMagazineAmmo();

	/* Dynamic decal variables. */
	void SpawnDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const float roll = 0.0f);
	void SpawnDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const float roll = 0.0f);
	void SpawnDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const Vector3 & scaleProj, const float roll = 0.0f);
	void SpawnDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const float roll = 0.0f);
	void SpawnDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const float roll = 0.0f);
	void SpawnDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const Vector3 & scaleProj, const float roll = 0.0f);
	int getDynamicDecalID(const std::string & decal_name);

	/* Player powers. */
	bool isPerceptionActive();
	void setStatusPerceptionEntity(const std::string & entityName, bool active);
	void setStatusPerceptionEntity(CHandle entityHand, bool active);
	void setStatusVanishEntity(CHandle entityHand);
	void setStatusVanishEntity(CHandle entityHand, Vector3 position);

	/* Camera Shake or Trauma variables. */

	// Adds trauma and sends the delta to given entity.
	void SendDeltaTrauma(const std::string & entityName, float traumaDelta);
	void SendDeltaTrauma(CHandle entity, float traumaDelta);
	// Adds trauma and sends delta to given entity. Max trauma reachable allows to tweak how much trauma it can rise this event.
	void SendDeltaTrauma(const std::string & entityName, float traumaDelta, float maxTraumaReachable);
	void SendDeltaTrauma(CHandle entity, float traumaDelta, float maxTraumaReachable);

	/* Spawner */
	// Spawns a explosion, inner radius receives maximum damage, outer radius receives proporcional damage. Outer radius has to be > than inner radius.
	void SpawnExplosion(TCompTransform * transform, float innerRadius, float outerRadius, float dmg);

	/* Particles */
	particles::CSystem * SpawnParticle(const std::string & particle_name, CHandle entityOwner, const Vector3 & offset = Vector3::Zero);
	particles::CSystem * SpawnParticle(const std::string & particle_name, CHandle entityOwner, const std::string& boneToFollow, const Vector3 & offset = Vector3::Zero);
	particles::CSystem * SpawnParticle(const std::string & particle_name, const Vector3 & spawnPosition);
	particles::CSystem * SpawnParticle(const std::string & particle_name, const CTransform & transform);
	particles::CSystem * SpawnParticle(const std::string & particle_name, const Vector3 & spawnPosition, const Vector3 & lookAtPos);
	particles::CSystem * SpawnParticle(const std::string & particle_name, const Vector3 & spawnPosition, const Vector3 & lookAtPos, float roll);
	void removeSpawnedParticle(particles::CSystem * systemToRemove, float timeBeforeRemoval = 0.0f);

	void spawnFXOnHitPos(const Vector3 & hitPosition, const Vector3 & hitNormal, const std::string & onHitLuaFunction, const std::string & surfaceType = std::string());
	void SpawnCriticalHitboxFXOnHit(const Vector3 & hitPosition, const Vector3 & hitNormal, int enemyType = 0);

	/* UI variables. */
	CHandle getHUDUI();

	/* Camera variables. */
	
	// Camera basic functions and variables.
	void BlendCamera(const std::string & cameraName, float blendTime, const std::string & interpolation); // Adds a camera for blending to the camera named Camera-Output.
	void BlendCamera(const std::string & cameraName, float blendTime, Interpolator::IInterpolator * interpolation); // Adds a camera for blending to the camera named Camera-Output.
	void setDefaultCamera(const std::string& cameraName);
	void setDefaultCamera(CHandle CameraHandle);
	void setOutputCamera(const std::string& cameraName);
	void setOutputCamera(CHandle CameraHandle);
	CHandle getDefaultCamera();
	CHandle getOutputCamera();

	/* Cinematic. */
	void StartCinematic(const std::string & CinematicPath);
	void EndCinematic();
	void PlayLevelSequence(const std::string & LevelSequencePath);
	void EndLevelSequence(const std::string & LevelSequencePath);

	/* Scripting. */
	void CallGlobalScriptFunction(const std::string & FunctionToCall);
	bool BindScriptToComponentEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent, const std::string & FunctionToFireOnEventCall);
	bool UnbindScriptToComponentEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent);
	void BindTimedScriptEvent(CHandle CompHandleOfScriptToBindToEvent, const std::string & FunctionToFireOnEventCall, float TimeBeforeCalling);
	void ClearBindTimedScriptEvents();
	void CallEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent);
	void CallEventAllScript(CHandle CompHandleFiringEvent, const std::string & EventID);

	/* PostFX. */

	// Color grading.
	void activateColorGrading(const std::string & cameraName, bool status);
	void colorGradingCurrentLUTAmount(const std::string & cameraName, float nCurrentGradAmount);
	void colorGradingFallTime(const std::string & cameraName, float nFallTime);
	void colorGradingLutTexture(const std::string & cameraName, const std::string & nLutPath);

	// Vignetting.
	void activateVignetting(const std::string & cameraName, bool status);
	void vignettingAmount(const std::string & cameraName, float value);
	void vignettingFallTime(const std::string & cameraName, float nFallTime);
	
	// Burning fade.
	void activateBurningFade(const std::string & cameraName, bool status);

	// Cinematic bars.
	void activateCinematicBars(const std::string & cameraName, bool status);
	void activateCinematicBars(const std::string & cameraName, bool status, float nTimeToGetToGoal);
	void showCinematicBarsFull(const std::string & cameraName);
	void hideCinematicBarsFull(const std::string & cameraName);

	// Fog
	void setFogLerp(const std::string & cameraName, float nLerp);

	/* Time. */

	/* Time */
	void pauseTime();
	void resumeTime();
	void toggleTime();
	void setTimeScale(float tScale);

	/* Audio */
	void playEvent(std::string path);
	void playEvent(std::string path, VEC3 location);
	void setEventParameter(std::string path, std::string paramName, float paramValue);
	void stopEvent(std::string path);
	void stopAllSounds();
	void stopMusic();
	//-------------------------------------------------
	void playMusic(std::string musicName);

	/* Input Module */
	void setActiveModuleInput(bool active);
	void setActiveMouse(bool active);
	void setCenterMouseActive(bool active);
	void showCursor(bool active);
	void setActiveKeyboard(bool active);
	void setActiveGamepad(bool active);

	/* UI Module */
	void activateWidget(std::string widget_name);
	void deactivateWidget(std::string widget_name);
	bool isWidgetActive(std::string widget_name);
	void updateRatioFromProgress(std::string widget_name, float ratio);
	void setChildWidgetVisibleByTime(const std::string childName, const std::string parentName, bool status, float time);
	void setControlUIButtonsByDevice(bool isControllerActive);
	void fadeInBlackScreen();
	void fadeOutBlackScreen();
	void fadeInBlackScreen(std::string animation);
	void fadeOutBlackScreen(std::string animation);
	void setBlackScreenAlpha(float alpha);
	bool isBlackScreenFadeOver();
	void PlayUIAnimation(const std::string& ParentName, const std::string& WidgetName, const std::string& Animation, bool Loop, float StartTime);
	void SetUIImageAlpha(const std::string& ParentName, const std::string& WidgetName, float alpha);

	/* Module manager. */
	void ChangeToMenuState();

	/* Checkpoints */
	// Teleports player to last checkpoint visited & executes scripting logic for when that happens
	void restorePlayerToLastCheckpoint();
	// Teleports player to spawn checkpoint & executes scripting logic for when that happens
	void restorePlayerToSpawnCheckpoint();
	// Resets the current checkpoint to be the spawn
	void resetCheckpoints();
	CHandle getSpawnCheckpoint();
	CHandle getCurrentCheckpoint();
	// Scripting version of the last method so it won't be needed to call "asTCompCheckpoint()"
	TCompCheckpoint* getSpawnCheckpointComponent();
	TCompCheckpoint* getCurrentCheckpointComponent();

	/* Utils. */
	bool isPlayerClose(const Vector3 & position, float maxDistance); // Returns true if smaller or equal than max distance.
	int getRandomInt(int min, int max); // Returns an uniformed distributed integer between the given range [min, max].
	float getRandomFloat(float min, float max); // Returns an uniformed distributed float between the given range [min, max].
	Vector3 TransformVector(Vector3 & vectorToTransform, Quaternion & rotationToApply);
	
	/* ImGui control variables */
	Vector3 _playerPos;
	Vector3 _meleePos;

	/* Application & Render */
	float GetAmbientBoost();
	float GetAmbientLightIntensity();
	float GetExposureAdjustment();
	float GetReflectionIntensity();
	float GetGlobalSkyboxLerp();

	void SetRenderAmbientVariables(float AmbientBoost = -1.0f, float AmbientLightIntensity = -1.0f, float ExposureAdjustment = -1.0f, float ReflectionIntensity = -1.0f, float SkyboxLerp = -1.0f);
	void SetAmbientBoost(float AmbientBoost);
	void SetAmbientLightIntensity(float AmbientLightIntensity);
	void SetExposureAdjustment(float ExposureAdjustment);
	void SetReflectionIntensity(float ReflectionIntensity);
	
	void setFramerateLimit(int fps);
	int getFramerateLimit();
	void exitApplication();
	void changeToGamestate(const std::string& gs);
	void changeToGamestateNonBlocking(const std::string& gs);
	
};