#include "mcv_platform.h"
#include "entity.h"
#include "entity_parser.h"
#include "components/common/comp_name.h"
#include "common_msgs.h"

#include "logic/logic_manager.h"

DECL_OBJ_MANAGER("entity", CEntity);

std::unordered_multimap< uint32_t, TCallbackSlot > all_registered_msgs;
uint32_t getNextUniqueMsgID() {
	static uint32_t unique_msg_id = 0;
  ++unique_msg_id;
  return unique_msg_id;
}

CEntity::~CEntity() {
	// Comp 0 is not valid
  for (uint32_t i = 1; i < CHandleManager::getNumDefinedTypes(); ++i) {
    CHandle h = comps[i];
    if (comps[i].isValid())
      comps[i].destroy();
  }
}

void CEntity::setComponent(uint32_t comp_type, CHandle new_comp) {
  assert(comp_type < CHandle::max_types);
  assert(!comps[comp_type].isValid());
  comps[comp_type] = new_comp;
  new_comp.setOwner(CHandle(this));
}

void CEntity::setComponent(CHandle new_comp) {
  setComponent(new_comp.getType(), new_comp);
}

const char* CEntity::getName() const {
  TCompName* comp_name = getComponent<TCompName>();
  return comp_name->getName();
}

void CEntity::renderDebug() {
  for (uint32_t i = 0; i < CHandleManager::getNumDefinedTypes(); ++i) {
    CHandle h = comps[i];
    if (h.isValid())
      h.renderDebug();
  }
}

void CEntity::debugInMenu() {
	ImGui::PushID(this);
	if (ImGui::TreeNode(getName())) {
		if (ImGui::Checkbox("Is Active", &active))
			LogicManager::Instance().SetEntityActive(CHandle(this), active);
		ImGui::SameLine(0.0f, 25.f);
		
		// Give option to destroy the entity
		if (ImGui::SmallButton("Delete Entity")) {
			CHandle(this).destroy();
			// If destroyed, exit the menu asap
			ImGui::TreePop();
			ImGui::PopID();
			return;
		}

		// Give option to kill the entity
		ImGui::SameLine();
		if (ImGui::SmallButton("Kill by damage")) {
			CHandle(this).sendMsg(TMsgHitDamage{ CHandle{this}, 10000.0f });
		}

		ImGui::Spacing();
		ImGui::Separator();
		ImGui::Spacing();
		
		for (int i = 0; i < CHandle::max_types; ++i) {
			CHandle h = comps[i];
			
			if (h.isValid()) {
				// Open a tree using the name of the component
				if (ImGui::TreeNode(h.getTypeName())) {
					// Do the real show details of the component
					h.debugInMenu();
					ImGui::TreePop();
				}
			}
		}
		
		ImGui::TreePop();
	}

	if (ImGui::IsItemHovered())
		renderDebug();

	ImGui::PopID();
}

void CEntity::load(const json& j, TEntityParseContext& ctx) {

  ctx.current_entity = CHandle(this);

  for (auto it = j.begin(); it != j.end(); ++it) {

    auto& comp_name = it.key();
    auto& comp_json = it.value();

		if (comp_json.is_null())
			continue;

    auto om = CHandleManager::getByName(comp_name.c_str());
    if (!om) {
      if( comp_name != "prefab" )
        Utils::dbg("While parsing file %s. Unknown component named '%s'\n", ctx.filename.c_str(), comp_name.c_str());
      continue;
    }
    assert(om);
    
    int comp_type = om->getType();      // 7

    // This is my current handle of this type of component
    CHandle h_comp = comps[comp_type];
    if (h_comp.isValid()) {
      // Give an option to reconfigure the existing comp with the new json
      h_comp.load(comp_json, ctx);
    }
    else 
    {
      // Get a new fresh component of this type for me
      h_comp = om->createHandle();

      // Initialize the comp from the json. You still don't have an owner
      h_comp.load(comp_json, ctx);

      // Bind it to me
      setComponent(comp_type, h_comp);
    }

  }

  // Send a msg to the entity components to let them know
  // the entity is fully loaded.
  if (!ctx.parsing_prefab)
	sendMsg(TMsgEntityCreated());

}

void CEntity::registerMsgs() {
	DECL_MSG(CEntity, TMsgToogleEntity, onToggleEntity);
}

void CEntity::onToggleEntity(const TMsgToogleEntity & msg) {
	active = msg.active;
	TMsgToogleComponent msgToComp = { msg.active };
	sendMsg(msgToComp);
}

// If we recieve this message we will also block the entity.
template<> void CEntity::sendMsg(const TMsgToogleComponent & msg) {
	active = msg.active;
	auto range = all_registered_msgs.equal_range(TMsgToogleComponent::getMsgID());
	while (range.first != range.second) {
		const auto& slot = range.first->second;

		// Si YO como entidad tengo ese component activo...
		CHandle h_comp = comps[slot.comp_type];
		if (h_comp.isValid())
			slot.callback->sendMsg(h_comp, &msg);
		++range.first;
	}
}

template<> void CEntity::sendMsg(const TMsgToogleEntity & msg) {
	active = msg.active;
	onToggleEntity(msg); // The entity is not stored in the components array, we do this hack.

	auto range = all_registered_msgs.equal_range(TMsgToogleEntity::getMsgID());
	while (range.first != range.second) {
		const auto& slot = range.first->second;

		// Si YO como entidad tengo ese component activo...
		CHandle h_comp = comps[slot.comp_type];
		if (h_comp.isValid())
			slot.callback->sendMsg(h_comp, &msg);
		++range.first;
	}
}