#ifndef INC_COMMON_ENTITY_MSGS_
#define INC_COMMON_ENTITY_MSGS_

#include "msgs.h"
#include "geometry/transform.h"
//#include "components/game/status_effects/status_effect.h"
#include "PxPhysicsAPI.h"

using namespace physx;

/* Sended once an scene is created. */
struct TMsgSceneCreated {
	DECL_MSG_ID();
};

struct TMsgEntityCreated {
	DECL_MSG_ID();
};

struct TMsgToogleEntity {
	bool active;
	DECL_MSG_ID();
};

struct TMsgToogleComponent {
	bool active;
	DECL_MSG_ID();
};

//Used when reusing entities from a pool
struct TMsgResetComponent {
	DECL_MSG_ID();
};

struct TMsgEntityDead {
	DECL_MSG_ID();
	VEC3 hitDirection;
	float hitForce;
};

struct TMsgFloatingDamageText {
	DECL_MSG_ID();
	float damage;
	bool isCritical;
	VEC3 targetLocation;
};

struct TMsgDeleteIAController {
	DECL_MSG_ID();
};

/* Used for controlling the status of the rendering in game. */
struct TMsgRenderingStatus {
	bool activate;
	DECL_MSG_ID();
};

/* Used for controlling the status of the logic in game.
Any component of an entity that gives behaviours to it
should subscribe to this message. */
struct TMsgLogicStatus {
	bool activate;
	DECL_MSG_ID();
};

// Used for disabling or enabling input controllers
struct TMsgInputControllerStatus {
	bool activate;
	DECL_MSG_ID();
};

// Used for shaking.
struct TMsgTraumaDelta {
	float traumaDelta; // Trauma added.
	float maxTraumaReachable = 1.0; // Maximum trauma value this message can reach to.
	// If we have 0.5 trauma, recieve a message with 0.1 and maxTraumaReachable is 0.5, we stay at 0.5.
	// By default set to max = 1.0.
	DECL_MSG_ID();
};

struct TMsgSetMeshState {
	int state;
	DECL_MSG_ID();
};

// Sent when an entity is registered to be rendered through instancing.
struct TMsgSetEntityMeshInstantiated{
	bool active = true;
	DECL_MSG_ID();
};

// Sent when ragdoll is activated, for example to detach AABB from the current collider
struct TMsgRagdollActivated {
	PxArticulationLink* link = nullptr;
	DECL_MSG_ID();
};

struct TMsgAssignBulletOwner {
	CHandle			h_owner;
	CHandle			h_targetTransform;
	VEC3				source;
	float				damageBuff = 1.0f;
	DECL_MSG_ID();
};

/* Three similar messages for damage, used for diferentiation in behaviour. */
// Don't forget to add them if the component has any behaviour for each one of them.

// Enviado al recibir daño de un ataque cuerpo a cuerpo.
struct TMsgHitDamage {
	CHandle h_sender;
	float damage; // Damage made.
	VEC3 damageDirection; 	// Direction the damage came from.
	float damageForce;
	DECL_MSG_ID();
};

// Enviado al recibir daño de un proyectil.
struct TMsgBulletDamage {
	CHandle h_sender;
	float damage; // Damage made.
	VEC3 damageDirection; 	// Direction the damage came from.
	float damageForce;
	void* actorHit; //To check which hitbox was hit
	VEC3 hitPos; // Position where bullet hit.
	VEC3 hitNormal; // Normal of the collider where bullet hit. (not direction of the bullet!)
	DECL_MSG_ID();
};

// Enviado al recibir daño de un clavo.
struct TMsgNailDamage {
	CHandle h_sender;
	float damage;               // Damage made.
	VEC3 damageDirection; 	// Direction the damage came from.
	float damageForce;

	VEC3 hitCoord;
	float hitDistance;	        // Distance between gun and enemy hit
	void* actorHit; //To check which hitbox was hit
	VEC3 hitPos; // Position where bullet hit.
	VEC3 hitNormal; // Normal of the collider where bullet hit. (not direction of the bullet!)
	DECL_MSG_ID();
};

struct TMsgHealing {
	float healedPoints;
	DECL_MSG_ID();
};

struct TMsgFullRestore {
	DECL_MSG_ID();
};

struct TMsgCorruptionAdded {
	CHandle h_sender;
	float addedCorruption;
	DECL_MSG_ID();
};

struct TMsgCorruptionRemoved {
	CHandle h_sender;
	float removedCorruption;
	DECL_MSG_ID();
};

// Indicates if perception is active or not.
struct TMsgPerceptionStatusChanged {
	bool active;
	DECL_MSG_ID();
};

// Indicates if vision penalty is active or not and its current value.
struct TMsgVisionPenaltyStatusChanged {
	float percentage;
	DECL_MSG_ID();
};

// Indicates if the insanity penalty is active or not.
struct TMsgInsanityPenaltyStatusChanged {
	bool active;
	DECL_MSG_ID();
};

// Sent to all entities from a parsed file once all the entities
// in that file has been created. Used to link entities between them
struct TEntityParseContext;
struct TMsgEntitiesGroupCreated {
	const TEntityParseContext& ctx;
	DECL_MSG_ID();
};

struct TMsgDefineAbsAABB {
	AABB* aabb = nullptr;
	DECL_MSG_ID();
};

struct TMsgDefineLocalAABB {
	AABB* aabb = nullptr;
	DECL_MSG_ID();
};

struct TMsgSetVisible {
	bool visible;
	DECL_MSG_ID();
};

//Updates the vortex/teleport powers location indicators from player input
struct TMsgUpdateIndicator {
	VEC3 position;
	VEC3 front; //Is this really necessary?
	DECL_MSG_ID();
};

// Player is currently able to interact with this component
// TODO: name subject to change
struct TMsgPlayerInteractionTriggerEnter {
	DECL_MSG_ID();
};

// Player is no longer able to interact with this component
// TODO: name subject to change
struct TMsgPlayerInteractionTriggerExit {
	DECL_MSG_ID();
};

// Player is currently interacting with this component
struct TMsgPlayerIsInteracting {
	float dt;
	DECL_MSG_ID();
};

//Totem apply buff
struct TMsgApplyBuff {
	float armour;						//Incoming damage resistance
	float regenerationPerSecond;		//Amount of life regenerated per second
	float damage;						//Outgoing damage multiplier
	float bulletSpeed;					//Outgoind projectile speed (Rangeds Only)
	float decisionMakingSpeed;			//The enemies make faster decisions
	float decisionMakingIntelligence;	//The enemies make sharper decisions
	float detectionDistance;			//Vision multiplier
	DECL_MSG_ID();
};

//Totem removes buff
struct TMsgRemoveBuff {
	DECL_MSG_ID();
};

struct TMsgExplode {
	DECL_MSG_ID();
};

struct TMsgExplosion {
	DECL_MSG_ID();
	float dmg;
	float throwForce;
	VEC3  throwDir;
};

struct TMsgMovementSpeedMultiplier {
	DECL_MSG_ID();
	float mult;
};

struct TMsgAlertAI {
	DECL_MSG_ID();
};

struct TMsgClearStatusEffects {
	DECL_MSG_ID();
};
//struct TApplyStatusEffect {
//	DECL_MSG_ID();
//
//	TApplyStatusEffect(CStatusEffect& statusEffect) : effect(statusEffect) {}
//
//private:
//	CStatusEffect& effect;
//
//};

#endif

