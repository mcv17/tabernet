#include "mcv_platform.h"
#include "handle/handle.h"
#include "entity.h"
#include "entity_parser.h"
#include "common_msgs.h"
#include "utils/json_resource.h"
#include "resources/prefab_resource.h"
#include "engine.h"
#include "components/common/comp_group.h"

// Find in the current list of entities created, the first entity matching
// the given name
CHandle TEntityParseContext::findEntityByName(const std::string& name) const {

  // Search linearly in the list of entity currently loaded
  for (auto h : entities_loaded) {
    CEntity* e = h;
    if (e->getName() == name )
      return h;
  }

  // Delegate it to my parent
  if (parent)
    return parent->findEntityByName(name);

  return getEntityByName(name);
}

TEntityParseContext::TEntityParseContext(TEntityParseContext& another_ctx, const CTransform& delta_transform) {
  parent = &another_ctx;
  recursion_level = another_ctx.recursion_level + 1;
  entity_starting_the_parse = another_ctx.entity_starting_the_parse;
  root_transform = another_ctx.root_transform.combineWith(delta_transform);
  //VEC3 p = root_transform.getPosition(); float y, pitch; root_transform.getYawPitchRoll(&y, &pitch);
  //dbg("New root transform is Pos:%f %f %f Yaw: %f\n", p.x, p.y, p.z, rad2deg(y));
}

// IMPORTANT! DO NOT USE THIS METHOD OUTSIDE THIS CPP (DO NOT DECLARE IT IN entity_parser.h)
bool parsePrefab(const std::string & filename, TEntityParseContext & ctx, int depth, float timeLimit, json& args) {
	std::string resourceName = "";
	if (ctx.filename.empty())
		resourceName = filename;
	else {
		resourceName = ctx.filename + "->" + filename;
		ctx.filename += "->" + filename;
	}

	CPrefab* prefab = EngineResources.getEditableResource(resourceName)->as<CPrefab>();
	prefab->setFilename(filename);
	const json& j_scene = prefab->getJson(args);
	ctx.prefab_args = args;
	return parseJson(j_scene, ctx, depth, timeLimit);
}

/* This function parses the contents from a file. */
bool parseJson(const json & j_scene, TEntityParseContext& ctx, int depth, float timeLimit) {
	assert(j_scene.is_array());

	// NonBlocking load time variables
	std::chrono::time_point<std::chrono::steady_clock> start;
	std::chrono::time_point<std::chrono::steady_clock> end;

	// If we are loading without blocking and are at depth 0 we get the current timestamp
	if (depth == 0 && timeLimit != 0.0f) {
		start = std::chrono::steady_clock::now();
	}

	// If we are loading without blocking and are at depth 0 we load entities from where we were
	int i = 0;
	if (depth == 0 && timeLimit != 0.0f)
		i = ctx.entitiesLoadedCount;

	// For each item in the array...
	for (i; i < j_scene.size(); ++i) {
		// If we are loading without blocking and are at depth 0
		if (depth == 0 && timeLimit != 0.0f) {
			// We get the end timestamp and compute the diff from the start
			end = std::chrono::steady_clock::now();
			std::chrono::duration<double> diff = end - start;
			// If we have used all of the frame time allowed we return false
			if (diff.count() > timeLimit) {
				// We save the current progress loading entities and return
				ctx.entitiesLoadedCount = i;
				return false;
			}
		}

		auto& j_item = j_scene[i];

		assert(j_item.is_object());

		if (j_item.count("entity")) {
			auto& j_entity = j_item["entity"];

			CHandle h_e;

			// Do we have the prefab key in the json?
			if (j_entity.count("prefab")) {

				// Get the src/id of the prefab
				auto& jPrefab = j_entity["prefab"];

				std::string prefab_src = jPrefab;
				assert(!prefab_src.empty());


				// Get delta transform where we should instantiate this transform
				CTransform delta_transform;
				if (j_entity.count("transform"))
					delta_transform.load(j_entity["transform"]);

				// Parse the prefab, if any other child is created they will inherit our ctx transform
				TEntityParseContext prefab_ctx(ctx, delta_transform);
				prefab_ctx.parsing_prefab = true;
				if (!parseScene(prefab_src, prefab_ctx, depth + 1, timeLimit))
					return false;

				assert(!prefab_ctx.entities_loaded.empty());

				// For the first entity, we also modify any values if the prefab had extra components.
				h_e = prefab_ctx.entities_loaded[0];
				// Cast to entity object
				CEntity* e = h_e;
				// We give an option to 'reload' the prefab by modifying existing components, 
				// like changing the name, add other components, etc, but we don't want to parse again 
				// the comp_transform, because it was already parsed as part of the root
				// As the json is const as it's a resource, we make a copy of the prefab section and
				// remove the transform
				json j_entity_without_transform = j_entity;
				j_entity_without_transform.erase("transform");

				// Do the parse now outside the 'prefab' context
				e->load(j_entity_without_transform, prefab_ctx);
				prefab_ctx.parsing_prefab = false;

				// add all entities from prefab so they receive the message too.
				ctx.entities_loaded.insert(std::end(ctx.entities_loaded), std::begin(prefab_ctx.entities_loaded), std::end(prefab_ctx.entities_loaded));

				// Finally as it is a prefab and can perform the load method multiple times
				// we send the TMsgEntityCreated here to all the prefab entities (minus the first one, because has already been sent).
				for (auto& entity : prefab_ctx.entities_loaded)
					entity.sendMsg(TMsgEntityCreated());

			}
			else {

				// Create a new fresh entity
				h_e.create< CEntity >();

				// Cast to entity object
				CEntity* e = h_e;

				// Do the parse
				e->load(j_entity, ctx);

				ctx.entities_loaded.push_back(h_e);
			}

		}
		else if (j_item.count("prefab")) {
			auto& jPrefab = j_item["prefab"];
			std::string prefabFile = jPrefab["file"];
			assert(!prefabFile.empty());
			json jArgs;
			if (jPrefab.count("args") > 0)
				jArgs = jPrefab["args"];

			TEntityParseContext prefab_ctx(ctx);
			if (jPrefab.value("forward_args", false)) {
				std::string name;
				for (auto& jElem : ctx.prefab_args.items()) {
					name = jElem.key();
					if (jArgs.count(name) == 0)
						jArgs[name] = jElem.value();
				}
			}

			if (!parsePrefab(prefabFile, prefab_ctx, depth + 1, timeLimit, jArgs))
				return false;

			ctx.entities_loaded.insert(std::end(ctx.entities_loaded), std::begin(prefab_ctx.entities_loaded), std::end(prefab_ctx.entities_loaded));
		}
		else if (j_item.count("group")) {
			TEntityParseContext group_ctx;
			if (!parseJson(j_item["group"], group_ctx, depth + 1, timeLimit))
				return false;
			ctx.entities_loaded.insert(std::end(ctx.entities_loaded), std::begin(group_ctx.entities_loaded), std::end(group_ctx.entities_loaded));
		}
	}

	// Create a comp_group automatically if there is more than one entity
	if (ctx.entities_loaded.size() > 1) {
		// The first entity becomes the head of the group. He is NOT in the group
		CHandle h_root_of_group = ctx.entities_loaded[0];
		CEntity* e_root_of_group = h_root_of_group;

		// If it doesn't have one, create it.
		TCompGroup * c_group = e_root_of_group->getComponent<TCompGroup>();
		if (c_group == nullptr) {
			assert(e_root_of_group);
			// Create a new instance of the TCompGroup
			CHandle h_group = getObjectManager<TCompGroup>()->createHandle();
			// Add it to the entity
			e_root_of_group->setComponent(h_group.getType(), h_group);

			// Now add the rest of entities created to the group, starting at 1 because 0 is the head
			TCompGroup * c_group = h_group;
			for (size_t i = 1; i < ctx.entities_loaded.size(); ++i)
				c_group->add(ctx.entities_loaded[i]);
		}
	}

	// Notify each entity created that we have create a group where they are at.
	TMsgEntitiesGroupCreated msg = { ctx };
	for (auto h : ctx.entities_loaded)
		h.sendMsg(msg);

	// If depth is 0. We send a scene message. This means we have parsed the whole scene, including possible prefabs.
	// We also check for each object if it can be instantiated or not, if it can, we add it.
	if (depth == 0) {
		TMsgSceneCreated msg = {};
		for (auto h : ctx.entities_loaded) {
			h.sendMsg(msg);

			// When registering for instancing, we don't want to add the entity, only register it.
			if (!ctx.is_registering_for_instancing)
				EngineInstancing.addToRender(h); // If it can be instantiated, it will be done.
		}
	}

	return true;
}

/* We open a file and call the parseJSON function to parse the contents. */
bool parseScene(const std::string& filename, TEntityParseContext& ctx, int depth, float timeLimit) {
	auto file = Utils::splitPath(filename);
	if (strcmp(file.extension, ".prefab") == 0) {
		return parsePrefab(filename, ctx, depth, timeLimit, json());
	}

	ctx.filename = filename;
	const json& j_scene = EngineResources.getResource(filename)->as<CJson>()->getJson();
	return parseJson(j_scene, ctx, depth, timeLimit);
}
