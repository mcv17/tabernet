#include "mcv_platform.h"
#include "engine.h"

#include "windows/app.h"

/* ImGui */
#include "imgui/imgui_manager.h"

/* Modules */
#include "modules/module_boot.h"
#include "modules/module_game_data.h"
#include "modules/module_pool.h"

// Rendering module.
#include "render/module_render.h"
#include "render/render.h"
#include "render/meshes/vertex_declarations.h"
#include "render/meshes/mesh.h"
#include "render/textures/texture.h"
#include "render/textures/material.h"
#include "render/meshes/mesh.h"
#include "render/meshes/collision_mesh.h"
#include "render/shaders/technique.h"
#include "render/compute/compute_shader.h"

/* Skeletons */
#include "skeleton/game_core_skeleton.h"

// Physics module.
#include "modules/module_physics.h"

// Entities module.
#include "modules/module_entities.h"

/* FSM module */
#include "fsm/fsm.h"
#include "fsm/module_fsm.h"

/* Particles */
#include "particles/module_particles.h"
#include "particles/particle_emitter.h"

/* Audio module */
#include "modules/module_audio.h"

/* Pooling module */
#include "modules/module_pool.h"

/* Editor module */
#include "modules/module_editor.h"

// Camera mixer module.
#include "modules/module_camera_mixer.h"
#include "geometry/curve.h"

// Input module.
#include "input/input.h"
#include "input/module_input.h"
#include "input/devices/device_keyboard.h"
#include "input/devices/device_mouse.h"
#include "input/devices/device_pad_xbox.h"

// Logic Manager.
#include "logic/logic_manager.h"

/* Scripting module. */
#include "modules/scripting/module_scripting.h"

// Imgui Manager.
#include "imgui/imgui_manager.h"

/* Test modules */
#include "modules/test/module_splash.h"
#include "modules/test/module_main_menu.h"
#include "modules/test/module_gameplay.h"
#include "modules/test/module_sample_objs.h"
#include "ui/module_ui.h"

#include "utils/json_resource.h"
#include "resources/prefab_resource.h"
#include "components/ai/behaviour_tree/bt_resource.h"
#include "modules/scripting/script_resource.h"
#include "../../module_cinematic_sequencer.h"


//Raw mouse input
#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif
#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif

CEngine::CEngine()
{}

CEngine::~CEngine()
{
	_module_manager.clear();
}

CEngine & CEngine::instance()
{
	static CEngine engine;
	return engine;
}

void CEngine::registerResourceTypes() {
	PROFILE_FUNCTION("CEngine::registerResourceTypes");
	_resource_manager.registerResourceType(getResourceTypeFor<CJson>());
	_resource_manager.registerResourceType(getResourceTypeFor<CPrefab>());
	_resource_manager.registerResourceType(getResourceTypeFor<CMesh>());
	_resource_manager.registerResourceType(getResourceTypeFor<CTexture>());
	_resource_manager.registerResourceType(getResourceTypeFor<CTechnique>());
	_resource_manager.registerResourceType(getResourceTypeFor<CMaterial>());
	_resource_manager.registerResourceType(getResourceTypeFor<CCollisionMesh>());
	_resource_manager.registerResourceType(getResourceTypeFor<CCurve>());
	_resource_manager.registerResourceType(getResourceTypeFor<CGameCoreSkeleton>());
	_resource_manager.registerResourceType(getResourceTypeFor<CFSM>());
	_resource_manager.registerResourceType(getResourceTypeFor<CBT>());
	_resource_manager.registerResourceType(getResourceTypeFor<CTransformCurve>());
	_resource_manager.registerResourceType(getResourceTypeFor<CScript>());
	_resource_manager.registerResourceType(getResourceTypeFor<CComputeShader>());
	_resource_manager.registerResourceType(getResourceTypeFor<particles::TEmitter>());
}

void CEngine::init()
{ 
	PROFILE_FUNCTION("CEngine::Init");
	/* Prepare Vertex Declarations */
	CVertexDeclarationManager::instance().initVertexDeclarations();

	/* Register resources in the engine. */
	registerResourceTypes();

	/* Initialize system and game modules. */

	// Register system modules.
	{
		PROFILE_FUNCTION("CEngine::registerSysModules");
		// Render module controls the rendering process communicating with the render class.
		_module_render = new CModuleRender("render");
		_module_manager.registerSystemModule(_module_render);

		// LUA Scripting module. (init before Entities module!!)
		_scripting = new CModuleScripting("scripting");
		_module_manager.registerSystemModule(_scripting);

		// Entities module.
		_module_entities = new CModuleEntities("entities");
		_module_manager.registerSystemModule(_module_entities);

		// Physics module.
		_module_physics = new CModulePhysics("physics");
		_module_manager.registerSystemModule(_module_physics);

		// Camera Mixer module.
		_cameraMixer = new CModuleCameraMixer("camera_mixer");
		_module_manager.registerSystemModule(_cameraMixer);

		// FSM module.
		_module_fsm = new FSM::CModuleFSM("fsm");
		_module_manager.registerSystemModule(_module_fsm);

		// Multithreading module.
		_multithreading = new MultithreadingModule("multithreading");
		_module_manager.registerSystemModule(_multithreading);

		// UI module.
		_ui = new UI::CModuleUI("ui");
		_module_manager.registerSystemModule(_ui);

		// Audio module
		_audio = new CModuleAudio("audio");
		_module_manager.registerSystemModule(_audio);

		// Particles module. (should be updated after module physics for PhysX particles to behave properly
		_particles = new particles::CModuleParticles("particles");
		_module_manager.registerSystemModule(_particles);

		json cfg = Utils::loadJson("data/config.json");
		bool loadEditor = cfg.value("load_editor", false);

		if (loadEditor) {
			// Editor module.
			_editor = new CModuleEditor("editor");
			_module_manager.registerSystemModule(_editor);
		}

		// Instancing module.
		_instancing = new CModuleInstancing("instancing");
		_module_manager.registerSystemModule(_instancing);

		// Boot module.
		_module_boot = new CModuleBoot("boot");
		_module_manager.registerSystemModule(_module_boot);

		// Cinematic sequencer.
		_module_cinematic_sequencer = new CCinematicSequencer("cinematicSequencer");
		_module_manager.registerSystemModule(_module_cinematic_sequencer);

		// Game module.
		_module_gameData = new CModuleGameData("gameData");
		_module_manager.registerSystemModule(_module_gameData);
	}
	
	// Register input module.
	{
		PROFILE_FUNCTION("CEngine::registerInput");
		// Input module.
		_module_input = new Input::CModuleInput("input_1");

		//Raw inputs
		RAWINPUTDEVICE Rid[2];
		//Raw mouse input
		Rid[0].usUsagePage = HID_USAGE_PAGE_GENERIC;
		Rid[0].usUsage = HID_USAGE_GENERIC_MOUSE;
		Rid[0].dwFlags = RIDEV_INPUTSINK;
		Rid[0].hwndTarget = CApplication::instance().getWindowHandler();
		
		//Raw keyboard input
		Rid[1].usUsagePage = HID_USAGE_PAGE_GENERIC;
		Rid[1].usUsage = 0x06;
		Rid[1].dwFlags = RIDEV_INPUTSINK;
		Rid[1].hwndTarget = CApplication::instance().getWindowHandler();

		//Register the total of raw inputs
		if (RegisterRawInputDevices(Rid, 2, sizeof(Rid[0])) == FALSE) {
			DWORD err = GetLastError();
			Utils::fatal("[Module_Input::init] Registering raw inputs failed, error: %i", err);
		}

		_module_input->registerDevice(new CDeviceKeyboard("keyboard"));
		_module_input->registerDevice(new CDeviceMouse("mouse"));
		_module_input->registerDevice(new CDevicePadXbox("gamepad", 0));
		_module_input->assignMapping("data/input/mapping.json");
		_module_input->resetKeyboardData();
		_module_manager.registerSystemModule(_module_input);
	}

	// Register game modules.
	{
		PROFILE_FUNCTION("CEngine::registerGameModules");
		_module_manager.registerGameModule(new CModuleSplash("splash"));
		_module_manager.registerGameModule(new CModuleMainMenu("main_menu"));
		_module_manager.registerGameModule(new CModuleGameplay("gameplay"));
		_module_manager.registerGameModule(new CModuleSampleObjs("sample_objs"));
		_module_pool = new CModulePool("pool");
		_module_manager.registerGameModule(_module_pool);
	}

	// Once all of them are registered, we start them.
	_module_manager.init();

	EngineScripting.call("onModulesLoaded");

	/* Initialize IMGUI Manager. */
	/* Get sure to call it after the other resources passed in init are initialized. */
	/* This will allow the manager to have all the functionality. */
	CEngineImGUIManager::instance();
	_imgui_manager = &CEngineImGUIManager::instance();
	_imgui_manager->init(&_module_manager, &_resource_manager);

	/* Finally, a directory watcher is created. */
	// We create a directory watcher that will send messages if any file get changed inside the directory.
	// These messages will be recieved in the WinProc in the app, we will update the resources present in the data folder.
	dir_watcher_data.start("data", CApplication::instance().getWindowHandler());
}


void CEngine::stop() {
	_module_manager.stop();
}

void CEngine::close() {
	_module_manager.stop();
	_module_manager.clear();
	_imgui_manager->stop();
}

void CEngine::executeFrame() {
	PROFILE_FRAME_BEGINS();
	PROFILE_FUNCTION("Engine::executeFrame");

	// This allow use of Imgui anytime during update/render.
	// We call the ImGui Manager inside the module render render method.
	_imgui_manager->beginImguiFrame();

	float delta = Time.delta;

	// Update the imgui manager.
	_imgui_manager->update(delta); // Each update we check for input from the keyboard.

	// Accumulate delta to know execution time
	Time.accumulated_delta += delta;
	Time.accumulated_delta_unscaled += Time.delta_unscaled;

	// Update the game.
	update(delta);

	// Prepare for rendering.
	_module_render->render();

	// The last thing we do is render imgui menus
	_imgui_manager->endImguiFrame();

	// Move rendered image intro front buffer.
	Render.swapChain();
}

void CEngine::update(float dt)
{
	_module_manager.update(dt);
}

void CEngine::renderDebug()
{
	_module_manager.renderDebug();
}

CModuleManager & CEngine::getModuleManager()
{
	return _module_manager;
}

CResourcesManager & CEngine::getResourceManager() {
	return _resource_manager;
}