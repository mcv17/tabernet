#include "mcv_platform.h"
#include "comp_skeleton_iks.h"

#include "comp_skeleton.h"
#include "bone_correction.h"
#include "game_core_skeleton.h"

#include "components/common/comp_transform.h"

#include "entity/common_msgs.h"

#include "engine.h"

DECL_OBJ_MANAGER("skeleton_iks", TCompSkeletonIKs);

void TCompSkeletonIKs::load(const json& j, TEntityParseContext& ctx) {
	loadedJson = j;
}

void TCompSkeletonIKs::update(float dt) {
	TCompTransform * comp_transform = getComponent<TCompTransform>();
	TCompSkeleton * comp_skel = getComponent<TCompSkeleton>();
	if (!comp_skel) return;

	// For debugging. Should remove in the future :).
	//static bool pressed = false;
	//if (EngineInput[VK_F5].justPressed())
	//	pressed = true;
	//if (EngineInput[VK_F6].justPressed())
	//	pressed = false;

	//if(pressed)
	//	activateIK("LeftArm", a);
	//else
	//	deactivateIK("LeftArm");

	CalModel * model = comp_skel->getModel();

	for (auto & ikbones : ik_bone_resolutions)
		ikbones.second->update(comp_transform, model, dt);
}

void TCompSkeletonIKs::debugInMenu() {
	TCompBase::debugInMenu();

	for (auto & ikbones : ik_bone_resolutions)
	{
		ImGui::Spacing(0, 5);
		ikbones.second->renderInMenu();
		ImGui::Spacing(0, 5);
		ImGui::Separator();
	}

	//ImGui::DragFloat3("Prueba", &a.x, 0.01f, -100.0f, 100.0f);
}

void TCompSkeletonIKs::renderDebug() {
	//drawAxis(Matrix::CreateTranslation(a));

	for (auto & ikbones : ik_bone_resolutions)
		ikbones.second->renderInDebug();

	if (ik_bone_resolutions.size() < 1)
		ImGui::Text("No iks are added to this component.");
	ImGui::Spacing(0, 5);
}

void TCompSkeletonIKs::registerMsgs() {
	DECL_MSG(TCompSkeletonIKs, TMsgEntityCreated, onEntityCreated);
}

void TCompSkeletonIKs::onEntityCreated(const TMsgEntityCreated & msg) {
	TCompSkeleton * comp_skel = getComponent<TCompSkeleton>();
	if (!comp_skel) return;
	CalModel * model = comp_skel->getModel();

	if (loadedJson.count("arithmetic_iks")) {
		for (auto & iks : loadedJson["arithmetic_iks"]) {
			std::string iksName = iks.value("name", "");
			assert(ik_bone_resolutions.find(iksName) == ik_bone_resolutions.end() && "IK is already registered.");
			ik_bone_resolutions[iksName] = new TIKArithmeticBonesResolution();
			ik_bone_resolutions[iksName]->load(iks, model);
		}
	}
	
	if (loadedJson.count("FABRIK_iks")) {
		for (auto & iks : loadedJson["FABRIK_iks"]) {
			std::string iksName = iks.value("name", "");
			assert(ik_bone_resolutions.find(iksName) == ik_bone_resolutions.end() && "IK is already registered.");
			ik_bone_resolutions[iksName] = new TIKFABRIKBonesResolution();
			ik_bone_resolutions[iksName]->load(iks, model);
		}
	}
}

// Activates a ik, which will start affecting the bones it was marked to affect.
void TCompSkeletonIKs::activateIK(const std::string & lookAt) {
	if (ik_bone_resolutions.find(lookAt) == ik_bone_resolutions.end())
		return;
	ik_bone_resolutions[lookAt]->setIKStatus(true);
}

void TCompSkeletonIKs::activateIK(const std::string & lookAt, const Vector3 & nTarget){
	if (ik_bone_resolutions.find(lookAt) == ik_bone_resolutions.end())
		return;
	auto & ik = ik_bone_resolutions[lookAt];
	ik->setIKStatus(true);
	ik->setTargetPosition(nTarget);
}

void TCompSkeletonIKs::activateIK(const std::string & lookAt, CHandle nEntity){
	if (ik_bone_resolutions.find(lookAt) == ik_bone_resolutions.end())
		return;
	auto & ik = ik_bone_resolutions[lookAt];
	ik->setIKStatus(true);
	ik->setTargetEntity(nEntity);
}

// Deactivates an ik, which will stop affecting the bones it was marked to affect.
void  TCompSkeletonIKs::deactivateIK(const std::string & lookAt) {
	if (ik_bone_resolutions.find(lookAt) == ik_bone_resolutions.end())
		return;
	ik_bone_resolutions[lookAt]->setIKStatus(false);
}

// Returns the ik info if you want to modify it's values.
TIKBaseResolver * TCompSkeletonIKs::getIKInfo(const std::string & lookAt) {
	if (ik_bone_resolutions.find(lookAt) == ik_bone_resolutions.end()) return nullptr;
	return ik_bone_resolutions[lookAt];
}