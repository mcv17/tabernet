#include "mcv_platform.h"
#include "comp_skel_lookat.h"
#include "components/common/comp_transform.h"
#include "skeleton/comp_skeleton.h"
#include "cal3d/cal3d.h"
#include "cal3d2engine.h"
#include "entity/entity_parser.h"
#include "game_core_skeleton.h"

#include "engine.h"

DECL_OBJ_MANAGER("skeleton_lookats", TCompSkelLookAt);

void TCompSkelLookAt::load(const json& j, TEntityParseContext& ctx) {
	if (!j.count("lookAts")) return;

	for (auto & lookAt : j["lookAts"]) {
		std::string lookAtName = lookAt.value("name", "");
		assert(tracking_target_and_bones.find(lookAtName) == tracking_target_and_bones.end() && "Look at is already registered.");
		tracking_target_and_bones[lookAtName] = TTrackingTargetAndBones(lookAtName);
		tracking_target_and_bones[lookAtName].load(lookAt);
	}
}

void TCompSkelLookAt::renderDebug() {
	//drawAxis(Matrix::CreateTranslation(a));
}

void TCompSkelLookAt::registerMsgs() {
	DECL_MSG(TCompSkelLookAt, TMsgEntityCreated, onEntityCreated);
}

void TCompSkelLookAt::onEntityCreated(const TMsgEntityCreated & msg) {
	// Fetch skeleton.
	CEntity* e_entity = CHandle(this).getOwner();
	if (!e_entity) return;
	h_skeleton = e_entity->getComponent<TCompSkeleton>();
	TCompSkeleton * skeleton = h_skeleton;

	// Fetch transform.
	h_transform = getComponent<TCompTransform>();
	TCompTransform * transform = h_transform;

	// We should get the angles of the actual bone but their transforms are broken.
	// For now, we use the entity for the angle.
	float  baseYaw, basePitch;
	transform->getAngles(&baseYaw, &basePitch);

	// Finish loading the look at information about the bones.
	for (auto & lookat : tracking_target_and_bones) {
		int bone = 0;
		auto core = (CGameCoreSkeleton *)skeleton->getModel()->getCoreModel();
		for (auto & it : core->getLookAtCorrections(lookat.first)) {
			BoneStatus bs = { baseYaw, basePitch, lookat.second.getSpeedRotation() };
			lookat.second.bonesStatus.push_back(bs);
		}
	}

}

void TCompSkelLookAt::debugInMenu() {
	if (tracking_target_and_bones.size() < 1) {
		ImGui::Text("No look ats are added to this component.");
		ImGui::Spacing(0, 10);
		ImGui::Separator();
		ImGui::Spacing(0, 10);
	}
	else {
		ImGui::Text("Look at variables");
		for (auto & lookat : tracking_target_and_bones)
			lookat.second.debugInMenu();
		ImGui::Spacing(0, 10);
	}

	// For debugging.
	//ImGui::DragFloat3("Prueba", &a.x, 0.01f, -100.0f, 100.0f);

	ImGui::Text("Core skeleton look at bone attributes.");
	ImGui::Spacing(0, 5);

	TCompSkeleton * skeleton = h_skeleton;
	skeleton->getGameCoreSkeleton()->renderLookAtBonesInMenu();
}

void TCompSkelLookAt::update(float dt) {
	// Get the skeleton component
	TCompSkeleton * skeleton = h_skeleton;
	TCompTransform * transform = h_transform;

	// Fetch the skeleton component if we don't have one.
	if (!skeleton) {
		CEntity* e_entity = CHandle(this).getOwner();
		if (!e_entity) return;

		// The entity I'm tracking should have an skeleton component.
		h_skeleton = e_entity->getComponent<TCompSkeleton>();
		if (!h_skeleton.isValid()) return;
		skeleton = h_skeleton;

		if(!h_transform.isValid())
			h_transform = getComponent<TCompTransform>();
		transform = h_transform;
		if (!transform) return;
	}

	// For debugging. Should remove in the future :).
	//static bool pressed = false;
	//if (EngineInput[VK_F5].justPressed())
	//	pressed = true;
	//if (EngineInput[VK_F6].justPressed())
	//	pressed = false;

	//if(pressed)
	//	activateLookAt("UpperArm", a);
	//else
	//	deactivateLookAt("UpperArm");

	// Update lookats.
	for (auto & lookat : tracking_target_and_bones)
		lookat.second.update(transform, skeleton, dt);
}


void TCompSkelLookAt::activateLookAt(const std::string & lookAt) {
	if (tracking_target_and_bones.find(lookAt) == tracking_target_and_bones.end())
		return;
	tracking_target_and_bones[lookAt].setLookAtStatus(true);
}

void TCompSkelLookAt::activateLookAt(const std::string & lookAt, const Vector3 & nTarget){
	if (tracking_target_and_bones.find(lookAt) == tracking_target_and_bones.end())
		return;
	auto & lookat = tracking_target_and_bones[lookAt];
	lookat.setLookAtStatus(true);
	lookat.setTargetPosition(nTarget);
}

void TCompSkelLookAt::activateLookAt(const std::string & lookAt, CHandle nEntity){
	auto & lookat = tracking_target_and_bones[lookAt];
	lookat.setLookAtStatus(true);
	lookat.setTargetEntity(nEntity);
}

void TCompSkelLookAt::deactivateLookAt(const std::string & lookAt) {
	if (tracking_target_and_bones.find(lookAt) == tracking_target_and_bones.end())
		return;
	tracking_target_and_bones[lookAt].setLookAtStatus(false);
}

TCompSkelLookAt::TTrackingTargetAndBones & TCompSkelLookAt::getLookAtInfo(const std::string & lookAt) {
	if (tracking_target_and_bones.find(lookAt) == tracking_target_and_bones.end())
		return TTrackingTargetAndBones();
	return tracking_target_and_bones[lookAt];
}

TCompSkelLookAt::TTrackingTargetAndBones::TTrackingTargetAndBones(const std::string & nLookAtName) : lookAt_name(nLookAtName){}

void TCompSkelLookAt::TTrackingTargetAndBones::fetchEntityTargetting() {
	if (isTargettingEntity && !target_entity_name.empty() && !h_target_entity.isValid())
		h_target_entity = getEntityByName(target_entity_name);
}

void TCompSkelLookAt::TTrackingTargetAndBones::updateEntityTargetPos(float dt) {
	if (isTargettingEntity) {
		CEntity * target_entity = h_target_entity;
		if (!target_entity) return;
		TCompTransform * target_transform = target_entity->getComponent<TCompTransform>();
		target = target_transform->getPosition();
	}
}

void TCompSkelLookAt::TTrackingTargetAndBones::load(const json& j) {
	isActive = j.value("active", false);
	isTargettingEntity = j.find("target_entity_name") != j.end();
	target_entity_name = j.value("target_entity_name", "");
	target = loadVector3(j, "target_pos");

	// Get a weight controller if there is one.
	weightController = WeightController::WeightControllerManager::Instance().createState(j.value("weightControllerType", ""));
	if (weightController)
		weightController->load(j);

	// Targetting speed.
	if (j.count("targetFollowingSpeed"))
		speed_rotation = DEG2RAD(j.value("targetFollowingSpeed", 0.0) >= 0 ? j.value("targetFollowingSpeed", 0.0) : -1.0f);
	else
		speed_rotation = -1.0f;
}

void TCompSkelLookAt::TTrackingTargetAndBones::update(const TCompTransform * transform, TCompSkeleton * skeleton, float dt) {
	if (weightController && !weightController->getWeightControllerStatus()) return;
	else if (!isActive) return;

	// If we are targetting an entity, fetch it.
	fetchEntityTargetting();

	// If we have a target entity, use it to assign a target position
	updateEntityTargetPos(dt);

	// Calculate the amount for the given animation.
	if (weightController)
		weightController->update(dt, amount);
	
	// Update the bones for the given look at.
	// The cal3d skeleton instance
	CalSkeleton * skel = skeleton->getModel()->getSkeleton();
	
	// The set of bones to correct
	int bone = 0;
	auto core = (CGameCoreSkeleton *)skeleton->getModel()->getCoreModel();
	for (auto & it : core->getLookAtCorrections(lookAt_name)) {
		// Instant.
		if (speed_rotation == -1.0f)
			it.applyBoundedInstantly(skel, target, amount, transform, bonesStatus[bone]);
		else
			it.applyBounded(skel, target, amount, transform, bonesStatus[bone]);
	}
}

void TCompSkelLookAt::TTrackingTargetAndBones::debugInMenu() {
	ImGui::PushID(lookAt_name.c_str());

	ImGui::Spacing(0, 10);

	std::string lookAtTitle = "Look At: " + lookAt_name;
	ImGui::Text(lookAtTitle.c_str());

	ImGui::DragFloat3("Target", &target.x, 0.01f, -1.f, 1.f);
	if (isTargettingEntity)
		ImGui::Text("Target Name", "%s", target_entity_name);

	ImGui::Text("Amount control variables");
	ImGui::Spacing();
	if (weightController)
		weightController->renderInMenu();
	ImGui::Spacing();

	ImGui::Spacing(0, 10);
	ImGui::Separator();

	ImGui::PopID();
}

void TCompSkelLookAt::TTrackingTargetAndBones::setLookAtStatus(bool active) {
	// Weight controller controls when to deactivate (for fade ins and outs).
	if (weightController) {
		if (active) isActive = active;
		weightController->setWeightControllerStatus(active);
	}
	else
		isActive = active;
}

void TCompSkelLookAt::TTrackingTargetAndBones::setTargetPosition(const Vector3 & nTarget) {
	isTargettingEntity = false;
	target = nTarget;
}

void TCompSkelLookAt::TTrackingTargetAndBones::setTargetEntity(CHandle nEntity) {
	isTargettingEntity = true;
	h_target_entity = nEntity;
}

void TCompSkelLookAt::TTrackingTargetAndBones::setTargetEntityByName(const std::string & entity_name) {
	isTargettingEntity = true;
	target_entity_name = entity_name;
	h_target_entity = getEntityByName(target_entity_name);
}