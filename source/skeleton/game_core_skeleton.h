#ifndef INC_GAME_CORE_SKELETON_H_
#define INC_GAME_CORE_SKELETON_H_

#include "resources/resource.h"
#include "cal3d/cal3d.h"
#include "bone_correction.h"
#include "skeleton/skeleton_shape_core.h"

typedef int CalAnimationID; // Cal3D animation ID.
typedef float AnimationTimeBeingPlayed;

// Types of animations, either a cycle or an action.
enum class AnimationType { CYCLE = 0, ACTION = 1 };

struct Animation {
	std::string name;
	int id;
	AnimationType isCycle;
};

// This is the CalCoreModel + IResource interface + other metadata
// we might need in the future and is shared between all instances
// of this model
class CGameCoreSkeleton : public IResource, public CalCoreModel {
	/* Variables. */

	// Rooth path of the skeleton.
	std::string root_path;

	// Bones for debugging.
	float              bone_ids_debug_scale = 1.f;
	std::vector< int > bone_ids_to_debug;
	
	// Bones used in look at iks. Key is the given look at type, the value are all the bones that are affected by it.
	// Allows to have multiple separated look ats, like one for the head, the spine, ...
	std::map<std::string, std::vector<TBoneCorrection >> lookat_corrections;

	// Map that tells whether the animations are cyclic or action
	std::vector<Animation> animations;

	float _scale = 1.0f;

	bool convertCalCoreMesh2RenderMesh(CalCoreMesh* cal_mesh, const std::string& ofilename);
public:
	CGameCoreSkeleton( const std::string & aname ) : CalCoreModel(aname) {}
  
	bool create(const std::string & res_name);
	void renderInMenu() override;
	void renderLookAtBonesInMenu();

	/* Getters */
	const std::vector<int> & getBoneIDsToDebug() const { return bone_ids_to_debug; }
	std::vector< TBoneCorrection > & getLookAtCorrections(const std::string & lookAt) {
		if (lookat_corrections.find(lookAt) == lookat_corrections.end())
			return std::vector< TBoneCorrection >();
		return lookat_corrections[lookAt];
	}
	std::vector<Animation> getAnimations() { return animations; }

	/* Variables. */
	TJsonSkeletonShapes ragdoll_core;
	TJsonSkeletonShapes hitboxes_core;

private:
	bool loadSkeleton(const std::string & name);
	bool loadMesh(const std::string & name);
	bool loadAnimations(const std::string & name, const json & json);
	bool loadRagdolls(const json & json);
};

#endif

