#include "mcv_platform.h"
#include "comp_skeleton.h"
#include "components/controllers/comp_character_controller.h"
#include "render/primitives.h" // for the max_skeletons_ctes
#include "Cal3D/animcallback.h" // for on complete callback
#include "engine.h"

#include "components/common/comp_transform.h"

// Changed name from skeleton to force to be parsed before the comp_mesh
DECL_OBJ_MANAGER("armature", TCompSkeleton);

// ---------------------------------------------------------------------------------------
// Cal2DX conversions, VEC3 are the same, QUAT must change the sign of w
CalVector DX2Cal(VEC3 p) {
	return CalVector(p.x, p.y, p.z);
}

CalQuaternion DX2Cal(QUAT q) {
	return CalQuaternion(q.x, q.y, q.z, -q.w);
}

VEC3 Cal2DX(CalVector p) {
	return VEC3(p.x, p.y, p.z);
}

QUAT Cal2DX(CalQuaternion q) {
	return QUAT(q.x, q.y, q.z, -q.w);
}

MAT44 Cal2DX(CalVector trans, CalQuaternion rot) {
	return MAT44::CreateFromQuaternion(Cal2DX(rot)) * MAT44::CreateTranslation(Cal2DX(trans));
}

// ---------------------------------------------------------------------------------------
/* Callbacks */

/* A Cal3D callback that gets called once an animation (cycle or action) is completed. */
struct AnimationCompleteCallback : CalAnimationCallback {
	int animationID = -1;

	AnimationCompleteCallback(int nAnimationID) : animationID(nAnimationID) {}
	virtual ~AnimationCompleteCallback() {}

	// We don't care about animation updates.
	void AnimationUpdate(float anim_time, CalModel *model, void * userData) {}

	void AnimationComplete(CalModel * model, void * userData, CalAnimation::State state, CalAnimation::Type type) {
		CHandle handle;
		handle.fromVoidPtr(userData);
		TCompSkeleton * comp = (TCompSkeleton*)handle;
		if (!comp) return;

		// We don't remove the animation if it is an async animation and is part of the smooth of an
		// animation when weights get changed.
		if (state == CalAnimation::State::STATE_ASYNC) return;

		if (type == CalAnimation::Type::TYPE_ACTION)
			comp->RemoveActionAnimation(animationID, true);
		else if (type == CalAnimation::Type::TYPE_CYCLE)
			comp->RemoveCyclicAnimation(animationID);
	}
};

// ---------------------------------------------------------------------------------------
/* TComp Skeleton. */

TCompSkeleton::TCompSkeleton() : cb_bones(CTE_BUFFER_SLOT_SKIN_BONES)
{
	bool is_ok = cb_bones.create("Bones");
	assert(is_ok);
}

TCompSkeleton::~TCompSkeleton() {
	if (model) delete model;
	cb_bones.destroy();
	model = nullptr;
}

void TCompSkeleton::load(const json& j, TEntityParseContext& ctx) {
	// Loads Cal Skeleton, animations, mesh, ragdolls.
	// Fetches the model and game core skeleton,
	// Adds the handle as data to be used by the model.
	loadGameCoreSkeletonAndFetchCalModel(j);

	// Set callbacks on complete for all animations.
	for (unsigned int animID = 0; animID < skeleton_and_anims->getCoreAnimationCount(); ++animID) {
		auto core_anim = skeleton_and_anims->getCoreAnimation(animID);
		// We register a callback that tells use when an animation ended.
		// These callbacks get remove once the model is removed.
		core_anim->registerCallback(new AnimationCompleteCallback(animID), FLT_MAX);
	}

	// Do a time zero update just to have the bones in a correct place. Make sure the first animation is a default animation
	// like a TPose.
	model->getMixer()->blendCycle(0, 1.0f, 0.f);
	model->update(0.f);
	model->getMixer()->clearCycle(0, 0.f); // Remove animation once updated. The animator will change them.
}

// Loads Cal Skeleton, animations, mesh, ragdolls.
// Fetches the model and game core skeleton,
// Adds the handle as data to be used by the model.
void TCompSkeleton::loadGameCoreSkeletonAndFetchCalModel(const json & j) {
	std::string src = j["src"];
	auto core = EngineResources.getResource(src)->as<CGameCoreSkeleton>();
	model = new CalModel((CalCoreModel*)core);
	skeleton_and_anims = (CGameCoreSkeleton *)model->getCoreModel();
	model->setUserData(CHandle(this).asVoidPtr());

	for (auto anim : skeleton_and_anims->getAnimations())
		registerAnimation(anim.name, anim.id, anim.isCycle);
}

void TCompSkeleton::renderDebug() {
	assert(model);

	VEC3 lines[MAX_SUPPORTED_BONES][2];
	int nrLines = model->getSkeleton()->getBoneLines(&lines[0][0].x);

	TCompTransform* transform = transformH;
	if (!transform) return;

	float scale = transform->getScale().x;
	for (int currLine = 0; currLine < nrLines; currLine++)
		drawLine(lines[currLine][0] * scale, lines[currLine][1] * scale, VEC4(1, 1, 1, 1));

	// Show list of bones
	auto mesh = EngineResources.getResource("axis.mesh")->as<CMesh>();
	auto core = (CGameCoreSkeleton*)model->getCoreModel();
	auto & bones_to_debug = core->getBoneIDsToDebug();
	for (auto it : bones_to_debug) {
		CalBone* cal_bone = model->getSkeleton()->getBone(it);
		QUAT rot = Cal2DX(cal_bone->getRotationAbsolute());
		VEC3 pos = Cal2DX(cal_bone->getTranslationAbsolute());
		MAT44 mtx;
		mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);
		drawMesh(mesh, mtx, VEC4(1, 1, 1, 1));
	}
}

void TCompSkeleton::debugInMenu() {
	TCompBase::debugInMenu();

	static int anim_id = 0;
	static float in_delay = 0.3f;
	static float out_delay = 0.3f;
	static float weight = 0.0f;
	static bool auto_lock = false;

	ImGui::Text("Root animations playing: %d\n", rootAnimationsPlaying);

	// Play action/cycle from the menu
	ImGui::DragInt("Anim Id", &anim_id, 0.1f, 0, model->getCoreModel()->getCoreAnimationCount() - 1);
	auto core_anim = model->getCoreModel()->getCoreAnimation(anim_id);
	if (core_anim)
		ImGui::Text("%s", core_anim->getName().c_str());
	ImGui::DragFloat("In Delay", &in_delay, 0.01f, 0, 1.f);
	ImGui::DragFloat("Out Delay", &out_delay, 0.01f, 0, 1.f);
	ImGui::DragFloat("Weight", &weight, 0.01f, 0, 1.f);
	ImGui::Checkbox("Auto lock", &auto_lock);
	if (ImGui::SmallButton("As Cycle")) {
		model->getMixer()->blendCycle(anim_id, weight, in_delay);
	}
	if (ImGui::SmallButton("As Action")) {
		model->getMixer()->executeAction(anim_id, in_delay, out_delay, weight, auto_lock);
	}

	// Dump Mixer
	auto mixer = model->getMixer();
	auto & animationList = mixer->getAnimationActionList();
	if (!animationList.empty()) {
		for (auto a : animationList) {
			auto core = (CGameCoreSkeleton*)model->getCoreModel();
			ImGui::Text("Action %s S:%d W:%1.2f Time:%1.4f/%1.4f"
				, a->getCoreAnimation()->getName().c_str()
				, a->getState()
				, a->getWeight()
				, a->getTime()
				, a->getCoreAnimation()->getDuration()
			);
			ImGui::SameLine();
			if (ImGui::SmallButton("X")) {
				int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
				if (a->getState() == CalAnimation::State::STATE_STOPPED)
					mixer->removeAction(id);
				else
					a->remove(out_delay);
			}
		}
	}


	for (auto a : mixer->getAnimationCycle()) {
		auto core = (CGameCoreSkeleton*)model->getCoreModel();
		ImGui::PushID(a);
		ImGui::Text("Cycle %s S:%d W:%1.2f Time:%1.4f Percentage: %1.4f Duration:%1.4f"
			, a->getCoreAnimation()->getName().c_str()
			, a->getState()
			, a->getWeight()
			, returnCycleAnimationCurrentTime(core->getCoreAnimationId(a->getCoreAnimation()->getName()))
			, returnCycleAnimationPercentage(core->getCoreAnimationId(a->getCoreAnimation()->getName()))
			, a->getCoreAnimation()->getDuration()
		);
		ImGui::SameLine();
		if (ImGui::SmallButton("X")) {
			int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
			mixer->clearCycle(id, out_delay);
		}
		ImGui::PopID();
	}

	if (mixer->getAnimationCycle().size() == 2) {
		static bool unit_weight = false;
		ImGui::Checkbox("Unit Weight", &unit_weight);
		if (unit_weight) {
			static float w1 = 0.5f;
			auto anim1 = mixer->getAnimationCycle().front();
			auto anim2 = mixer->getAnimationCycle().back();
			float wsmall = 1e-3f;
			if (ImGui::DragFloat("Weight", &w1, 0.005f, wsmall, 1.0f - wsmall)) {
				int anim1_id = model->getCoreModel()->getCoreAnimationId(anim1->getCoreAnimation()->getName());
				int anim2_id = model->getCoreModel()->getCoreAnimationId(anim2->getCoreAnimation()->getName());
				model->getMixer()->blendCycle(anim1_id, w1, 0.1f);
				model->getMixer()->blendCycle(anim2_id, 1.0f - w1, 0.1f);
			}
		}
	}

	// Show Skeleton Resource
	if (ImGui::TreeNode("Core")) {
		auto core_skel = (CGameCoreSkeleton*)model->getCoreModel();
		if (core_skel)
			core_skel->renderInMenu();
		ImGui::TreePop();
	}
}

void TCompSkeleton::registerMsgs() {
	DECL_MSG(TCompSkeleton, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompSkeleton, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompSkeleton, TMsgToogleComponent, onToggleComponent);
}

// Updates the mixer with the current transform position and the dt so the animations play.
void TCompSkeleton::update(float dt) {
	if (!active) return;

	PROFILE_FUNCTION("updateSkel");
	assert(model);

	TCompTransform* tmx = transformH;
	TCompCharacterController* controller = getComponent<TCompCharacterController>();
	if (!tmx) return;

	{
		PROFILE_FUNCTION("checkSkeleton");
		// Adds a dummy animation if no animation is playing.
		checkIfNoAnimations();
		checkIfDummyAnimationIsOn();
	}

	Vector3 pos = tmx->getPosition();
	Quaternion rot = tmx->getRotation();
	model->getMixer()->setWorldTransform(DX2Cal(pos), DX2Cal(rot));

	{
		PROFILE_FUNCTION("updateCal");
		model->update(dt);
	}

	{
		PROFILE_FUNCTION("updateAnimDuration");
		updateAnimationsDurations(dt);
	}
	if (rootAnimationsPlaying > 0) {
		Vector3 root_motion = Cal2DX(model->getMixer()->getAndClearDeltaRootMotion());
    Utils::dbg("root motion: [%f %f %f]\n", root_motion.x, root_motion.y, root_motion.z);
		if (controller != nullptr)
			controller->leap(root_motion);//tmx->setPosition(pos + root_motion);	
	}
}

// Updates the vector of bones with the CAL3D bones new positions then,
// it uploads them to the bones constant buffer.
void TCompSkeleton::updateCtesBones() {
	PROFILE_FUNCTION("updateCtesBones");

	// Pointer to the first float of the array of matrices
	float* fout = &cb_bones.Bones[0]._11;

	CalSkeleton* skel = model->getSkeleton();
	auto& cal_bones = skel->getVectorBone();
	assert(cal_bones.size() < MAX_SUPPORTED_BONES);

	// For each bone from the cal model
	for (size_t bone_idx = 0; bone_idx < cal_bones.size(); ++bone_idx) {
		CalBone* bone = cal_bones[bone_idx];

		const CalMatrix& cal_mtx = bone->getTransformMatrix();
		const CalVector& cal_pos = bone->getTranslationBoneSpace();

		*fout++ = cal_mtx.dxdx;
		*fout++ = cal_mtx.dydx;
		*fout++ = cal_mtx.dzdx;
		*fout++ = 0.f;
		*fout++ = cal_mtx.dxdy;
		*fout++ = cal_mtx.dydy;
		*fout++ = cal_mtx.dzdy;
		*fout++ = 0.f;
		*fout++ = cal_mtx.dxdz;
		*fout++ = cal_mtx.dydz;
		*fout++ = cal_mtx.dzdz;
		*fout++ = 0.f;
		*fout++ = cal_pos.x;
		*fout++ = cal_pos.y;
		*fout++ = cal_pos.z;
		*fout++ = 1.f;
	}

	cb_bones.updateGPU();
}

/* Base getters. */
// Returns the Cal Model pointer.
CalModel * TCompSkeleton::getModel() { return model; }

// Returns the constant buffer with the bones.
CCteBuffer<TCteSkinBones> & TCompSkeleton::getCBBones() { return cb_bones; }

// Returns the game core skeleton. Used by lookat iks debug.
CGameCoreSkeleton * TCompSkeleton::getGameCoreSkeleton() { return skeleton_and_anims; }

// Get bone absolute pos. Used in iks.
Vector3 TCompSkeleton::getBoneAbsPos(const std::string & boneName) const {
	int bone_id = model->getCoreModel()->getCoreSkeleton()->getCoreBoneId(boneName);
	auto cal_bone = model->getSkeleton()->getBone(bone_id);
	return Cal2DX(cal_bone->getTranslationAbsolute());
}

CTransform TCompSkeleton::getBoneTransform(const std::string & boneName) const {
	CTransform rTransf;
	getBoneTransform(boneName, &rTransf);
	return rTransf;
}

void TCompSkeleton::getBoneTransform(const std::string & boneName, CTransform * transform) const {
	int bone_id = model->getCoreModel()->getCoreSkeleton()->getCoreBoneId(boneName);
	auto cal_bone = model->getSkeleton()->getBone(bone_id);
	transform->setPosition(Cal2DX(cal_bone->getTranslationAbsolute()));
	transform->setRotation(Cal2DX(cal_bone->getRotationAbsolute()));
}

// Get bone relative pos. Used in iks.
Vector3 TCompSkeleton::getBoneRelativePos(const std::string & boneName) const {
	int bone_id = model->getCoreModel()->getCoreSkeleton()->getCoreBoneId(boneName);
	auto cal_bone = model->getSkeleton()->getBone(bone_id);
	return Cal2DX(cal_bone->getTranslation());
}

/* Animation functions. */

void TCompSkeleton::registerAnimation(const std::string & cal3DAnimationName, AnimationID animationID, AnimationType animationType) {
	int calAnimationID = getCalAnimationID(cal3DAnimationName);
	assert(calAnimationID != -1 && "The animation to register was not found in the skeleton. "
		"Check the name is correct and the TCompSkeleton is loading it!\n");

	if (animIDToAnimInfo.size() <= animationID)
		animIDToAnimInfo.resize(animationID + 1);

	// Register the animation info.
	animIDToAnimInfo[animationID] = { calAnimationID, animationType };

	if (animationType == AnimationType::ACTION)
		registerActionAnimation(calAnimationID);
	else if (animationType == AnimationType::CYCLE)
		registerCycleAnimation(calAnimationID);

}

// Plays an animation in the animation mixer. Attention, you can't play a same action multiple times in the mixer.
// If you need it, change the TCompSkeleton action method behaviour.
void TCompSkeleton::PlayAnimation(AnimationID animID, float weight, float delayIn, float delayOut, bool playAsRoot, bool autoLock, float startTime) {
	// Check the animation exists.
	assert(animIDToAnimInfo.size() > animID && "The animation requested to be played doesn't exist. Did you declare it?\n");

	if (animIDToAnimInfo[animID].type == AnimationType::CYCLE)
		SetCyclicAnimation(animID, weight, delayIn, playAsRoot);
	else
		SetActionAnimation(animID, weight, delayIn, delayOut, autoLock, playAsRoot, startTime);
}

void TCompSkeleton::PlayAnimation(std::string animationName, float weight, float delayIn, float delayOut, bool playAsRoot, bool autoLock, float startTime) {
	AnimationID animID = getAnimationID(animationName);
	PlayAnimation(animID, weight, delayIn, delayOut, playAsRoot, autoLock, startTime);
}

// Updates a cycle animation with new weights. (You can also use the play animation as both call the same Cal3D function).
void TCompSkeleton::UpdateCycleAnimationWeights(AnimationID animation, float weight, float delayIn, float delayOut, bool playAsRoot) {
	// Check the animation exists and is a cycle.
	assert((animIDToAnimInfo.size() > animation)
		&& (animIDToAnimInfo[animation].type == AnimationType::CYCLE)
		&& "The animation requested to be played doesn't exist or is not a cycle. Did you declare it?\n");

	// It's actually very similar to the play animation but we changed the method name for clarity purposes.
	auto & animationInfo = animIDToAnimInfo[animation];
	if (animationInfo.type == AnimationType::CYCLE && weight > 0.0f)
		SetCyclicAnimation(animationInfo.animationID, weight, delayIn, playAsRoot);
	else
		RemoveCyclicAnimation(animationInfo.animationID, delayOut);
}

void TCompSkeleton::RemoveAnimation(AnimationID animation, float delayOut) {
	auto & animationInfo = animIDToAnimInfo[animation];
	if (animationInfo.type == AnimationType::CYCLE)
		RemoveCyclicAnimation(animationInfo.animationID, delayOut);
	else
		RemoveActionAnimation(animationInfo.animationID, false, delayOut);
}
// Removes an animation from the animation mixer.
void TCompSkeleton::RemoveAnimation(std::string name, float delayOut) {
	RemoveAnimation(getAnimationID(name), delayOut);
}

// Returns the animation id (allows for differentiation between animations that use the
// same cal3d anim but are being played as action or cycles).
AnimationID TCompSkeleton::getAnimationID(const std::string & animationRegisteredName) const {
	for (auto anim : skeleton_and_anims->getAnimations())
		if (anim.name == animationRegisteredName)
			return anim.id;
}

std::string TCompSkeleton::getAnimationName(const AnimationID id) const
{
	for (auto anim : skeleton_and_anims->getAnimations())
		if (anim.id == id)
			return anim.name;
}

// Returns the time of the animation since it was started.
void TCompSkeleton::setAnimatorTimeFactor(float animTimeFactor) {
	model->getMixer()->setTimeFactor(animTimeFactor);
}

// Returns the time of the animation since it was started.
float TCompSkeleton::getAnimationElapsedTime(AnimationID animation) const {
	// Check the animation exists.
	assert(animIDToAnimInfo.size() > animation && "The animation you queried for its elapsed time doesn't exist. Did you declare it?\n");

	auto & animationInfo = animIDToAnimInfo.at(animation);
	if (animationInfo.type == AnimationType::CYCLE)
		return returnCycleAnimationCurrentTime(animationInfo.animationID);
	else
		return returnActionAnimationCurrentTime(animationInfo.animationID);
}

// Returns the percentage of the animation.
float TCompSkeleton::getAnimationPercentage(std::string name) const {
	// Check the animation exists.
	AnimationID animation = getAnimationID(name);
	assert(animIDToAnimInfo.size() > animation && "The animation you queried for its elapsed time doesn't exist. Did you declare it?\n");

	auto & animationInfo = animIDToAnimInfo.at(animation);
	if (animationInfo.type == AnimationType::CYCLE)
		return returnCycleAnimationPercentage(animationInfo.animationID);
	else
		return returnActionAnimationPercentage(animationInfo.animationID);
}

// Returns the duration of the animation.
float TCompSkeleton::getAnimationDuration(AnimationID animation) const {
	// Check the animation exists.
	assert(animIDToAnimInfo.size() > animation && "The animation you queried for its duration doesn't exist. Did you declare it?\n");
	return returnAnimationDuration(animIDToAnimInfo.at(animation).animationID);
}

// Returns true if the animation has just started.
bool TCompSkeleton::isAnimationJustStarted(AnimationID animation) const {
	// Check the animation exists.
	assert(animIDToAnimInfo.size() > animation && "The animation you queried for being just started doesn't exist. Did you declare it?\n");

	const auto & animationInfo = animIDToAnimInfo.at(animation);
	if (animationInfo.type == AnimationType::CYCLE)
		return isCycleJustStarted(animationInfo.animationID);
	else
		return isActionJustStarted(animationInfo.animationID);
}

// Returns true if the animation is executing.
bool TCompSkeleton::isAnimationExecuting(AnimationID animation) const {
	// Check the animation exists.
	assert(animIDToAnimInfo.size() > animation && "The animation you queried for being executed doesn't exist. Did you declare it?\n");

	auto & animationInfo = animIDToAnimInfo.at(animation);
	if (animationInfo.type == AnimationType::CYCLE)
		return isCycleExecuting(animationInfo.animationID);
	else
		return isActionExecuting(animationInfo.animationID);
}

// Returns true if the animation is not playing.
bool TCompSkeleton::isAnimationOver(AnimationID animID) const {
	// Check the animation exists.
	assert(animIDToAnimInfo.size() > animID && "The animation you queried for being over doesn't exist. Did you declare it?\n");

	auto & animationInfo = animIDToAnimInfo.at(animID);
	if (animationInfo.type == AnimationType::CYCLE)
		return isCycleOver(animationInfo.animationID);
	else
		return isActionOver(animationInfo.animationID);
}

bool TCompSkeleton::isAnimationOver(std::string animationName) const {
	AnimationID anim = getAnimationID(animationName);
	return isAnimationOver(anim);
}

// Returns true if the animation is playing as a root animation.
bool TCompSkeleton::isAnimationPlayingAsRoot(AnimationID animation) const {
	// Check the animation exists.
	assert(animIDToAnimInfo.size() > animation && "The animation you queried for being over doesn't exist. Did you declare it?\n");
	auto & animationInfo = animIDToAnimInfo.at(animation);
	if (animationInfo.type == AnimationType::CYCLE)
		return isCyclePlayingAsRoot[animationInfo.animationID];
	else
		return isActionPlayingAsRoot[animationInfo.animationID];
}


/* Skeleton private, internal functions. */

// Called on entity created. Fetches components needed, transform in this case.
void TCompSkeleton::onEntityCreated(const TMsgEntityCreated & msg) {
	transformH = getComponent<TCompTransform>();
}

// Called on logic status message recieved.
void TCompSkeleton::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

// Updates the time animations have been playing.
void TCompSkeleton::updateAnimationsDurations(float dt) {
	auto mixer = model->getMixer();

	// We update the time passed for each animation.
	for (auto a : mixer->getAnimationCycle())
		cyclesPlaying[a->getAnimationID()].second += dt * mixer->getTimeFactor();

	for (auto a : mixer->getAnimationActionList())
		actionsPlaying[a->getAnimationID()].second = a->getTime();
}

// Adds a dummy animation if no animation is playing.
void TCompSkeleton::checkIfNoAnimations() {
	auto mixer = model->getMixer();
	bool isEmpty = mixer->getAnimationActionList().empty();
	isEmpty &= mixer->getAnimationCycle().empty();

	if (isEmpty) {
		model->getMixer()->blendCycle(0, 1.0f, 0.f);
		noAnimationsPlaying = true;
	}
}

// Removes the dummy animation if a new animation is being added.
void TCompSkeleton::checkIfDummyAnimationIsOn() {
	if (noAnimationsPlaying) {
		auto mixer = model->getMixer();
		int size = mixer->getAnimationActionList().size();
		size += mixer->getAnimationCycle().size();
		if (size > 1) {
			model->getMixer()->clearCycle(0, 0.f);
			noAnimationsPlaying = false;
		}
	}
}

void TCompSkeleton::registerCycleAnimation(CalAnimationID animID) {
	if (cyclesPlaying.size() <= animID) {
		cyclesPlaying.resize(animID + 1);
		if (isCyclePlayingAsRoot.size() <= animID)
			isCyclePlayingAsRoot.resize(animID + 1);
	}
	cyclesPlaying[animID] = std::pair<IsAnimationPlaying, AnimationTimeBeingPlayed>(false, 0.0f);
	isCyclePlayingAsRoot[animID] = false;
}

void TCompSkeleton::registerActionAnimation(CalAnimationID animID) {
	if (actionsPlaying.size() <= animID) {
		actionsPlaying.resize(animID + 1);
		if (isActionPlayingAsRoot.size() <= animID)
			isActionPlayingAsRoot.resize(animID + 1);
	}
	actionsPlaying[animID] = std::pair<IsAnimationPlaying, AnimationTimeBeingPlayed>(false, 0.0f);
	isActionPlayingAsRoot[animID] = false;
}

void TCompSkeleton::SetCyclicAnimation(CalAnimationID animID, float weight, float delayIn, bool playAsRoot) {
	model->getMixer()->blendCycle(animID, weight, delayIn);

	// If the cycle was not present, we now start it.
	if (!cyclesPlaying[animID].first && weight > 0.0f) {
		cyclesPlaying[animID].first = true;
		cyclesPlaying[animID].second = 0.0f;
	}

	if (playAsRoot && !isCyclePlayingAsRoot[animID]) {
		isCyclePlayingAsRoot[animID] = true;
		cyclesPlaying[animID].second = 0.0f;
		rootAnimationsPlaying++;
	}
}

void TCompSkeleton::SetActionAnimation(CalAnimationID animID, float weight, float delayIn, float delayOut, bool autoLock, bool playAsRoot, float startTime) {
	// We don't allow playing the same action if it is already playing.
	if (actionsPlaying[animID].first) return;
	model->getMixer()->executeAction(animID, delayIn, delayOut, weight, autoLock, startTime);

	// Playing.
	actionsPlaying[animID].first = true;

	if (playAsRoot && !isActionPlayingAsRoot[animID]) {
		rootAnimationsPlaying++;
		isActionPlayingAsRoot[animID] = true;
	}
}

void TCompSkeleton::RemoveActionAnimation(CalAnimationID animID, bool is_call3d_callback, float delayOut) {
	// If not being played, we ignore.
	if (!actionsPlaying[animID].first) return;

	if (!is_call3d_callback)
		model->getMixer()->removeAction(animID, delayOut);

	// If there is not delayOut we can clear the information
	if (delayOut == 0.0f) {
		// Not playing anymore.
		actionsPlaying[animID].first = false;
		actionsPlaying[animID].second = 0.0f;

		// If it was playing as root we set it to false.
		if (isActionPlayingAsRoot[animID]) {
			rootAnimationsPlaying--;
			isActionPlayingAsRoot[animID] = false;
		}
	}
}

void TCompSkeleton::RemoveCyclicAnimation(CalAnimationID animID, float delayOut) {
	// If not being played, we ignore.
	if (!cyclesPlaying[animID].first) return;

	model->getMixer()->clearCycle(animID, delayOut);

	// Not playing anymore.
	cyclesPlaying[animID].first = false;
	cyclesPlaying[animID].second = 0.0f;

	// If it was playing as root we set it to false.
	if (isCyclePlayingAsRoot[animID]) {
		rootAnimationsPlaying--;
		isCyclePlayingAsRoot[animID] = false;
	}
}

// Returns the Cal3D ID of an animation.
CalAnimationID TCompSkeleton::getCalAnimationID(const std::string & animationName) const {
	return skeleton_and_anims->getCoreAnimationId(animationName);
}

// Returns the time of the animation.
float TCompSkeleton::returnAnimationDuration(CalAnimationID animID) const {
	auto * anim = skeleton_and_anims->getCoreAnimation(animID);
	if (!anim) return -1.0f;
	return anim->getDuration();
}

// Returns the time of the action animation.
float TCompSkeleton::returnActionAnimationCurrentTime(CalAnimationID animID) const {
	if (!actionsPlaying[animID].first) return 0.0f;
	return actionsPlaying[animID].second;
}

// Returns the time of the cycle animation. It will return the time
// since it was started not the time in the animation.
float TCompSkeleton::returnCycleAnimationCurrentTime(CalAnimationID animID) const {
	if (!cyclesPlaying[animID].first) return 0.0f;
	return cyclesPlaying[animID].second;
}

// Returns the percentage of the execution of the action animation.
float TCompSkeleton::returnActionAnimationPercentage(CalAnimationID animID) const {
	if (!actionsPlaying[animID].first) return 0.0f;

	float duration = skeleton_and_anims->getCoreAnimation(animID)->getDuration();
	float elapsed_time = actionsPlaying[animID].second;
	return elapsed_time / duration;
}

// Returns the percentage of the execution of the cycle animation. As cycles are loops
// and do not end by default this percentage will go from 1 to 0 if it reaches the 'end' of the animation.
float TCompSkeleton::returnCycleAnimationPercentage(CalAnimationID animID) const {
	if (!cyclesPlaying[animID].first) return 0.0f;

	float duration = skeleton_and_anims->getCoreAnimation(animID)->getDuration();
	float elapsed_time = cyclesPlaying[animID].second;

	float result = elapsed_time - std::floorf(elapsed_time / duration) * duration;
	return result / duration;
}

// Checks if a cycle animation is just started. Returns true if so, false ow.
bool TCompSkeleton::isCycleJustStarted(CalAnimationID animID) const {
	if (!cyclesPlaying[animID].first) return false;
	float currentDuration = cyclesPlaying[animID].second;
	if (currentDuration == 0.0f) return true;
	return false;
}

// Checks if a cycle animation is executing. Returns true if so, false ow.
bool TCompSkeleton::isCycleExecuting(CalAnimationID animID) const {
	if (!cyclesPlaying[animID].first) return false;
	return true;
}

// Checks if a cycle animation is over. Returns true if so, false ow. It only checks
// if the cycle is not being played (doesn't care if it never was played).
bool TCompSkeleton::isCycleOver(CalAnimationID animID) const {
	return !isCycleExecuting(animID);
}

// Checks if an action animation has just started. Returns true if so, false ow.
bool TCompSkeleton::isActionJustStarted(CalAnimationID animID) const {
	if (!actionsPlaying[animID].first) return false;
	float currentDuration = actionsPlaying[animID].second;
	if (currentDuration == 0.0f) return true;
	return false;
}

// Checks if an action animation is executing. Returns true if so, false ow.
bool TCompSkeleton::isActionExecuting(CalAnimationID animID) const {
	if (!actionsPlaying[animID].first) return false;
	float currentDuration = actionsPlaying[animID].second;
	float animationTotalDuration = skeleton_and_anims->getCoreAnimation(animID)->getDuration();
	if (currentDuration > animationTotalDuration) return false;
	return true;
}

// Checks if an action animation is not executing. Returns true if so, false ow.
// It only checks if the action is not being played (doesn't care if it never was played).
bool TCompSkeleton::isActionOver(CalAnimationID animID) const {
	return !actionsPlaying[animID].first;
}