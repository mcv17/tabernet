#ifndef INC_COMPONENT_SKELETON_H_
#define INC_COMPONENT_SKELETON_H_

#include "geometry/geometry.h"
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "game_core_skeleton.h"

#include "cal3d2engine.h"

class CalModel;
struct TMsgEntityCreated;
struct TMsgLogicStatus;

typedef bool IsAnimationPlaying;
typedef std::string RegisteredAnimationName;
typedef int AnimationID;

class TCompSkeleton : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Cal3D model and skeleton bones. */
	CalModel * model = nullptr;
	CCteBuffer<TCteSkinBones> cb_bones;

	// Handle to the entity's transform.
	CHandle transformH;

	/* Animation variables. */

	// Used for getting the cal ID of an animation and querying info about it like its duration.
	// Also called in iks for render debugging.
	CGameCoreSkeleton * skeleton_and_anims;

	// The information of an animation, the Cal animation ID and the type of animation.
	// The ID of the cal animation allows for easy playing of animations
	// by simply calling the PlayAnimation function. This function used the animation type,
	// to discern what type of animaton to play.
	struct AnimationInfo {
		CalAnimationID animationID;
		AnimationType type;
	};

	// Animation ID to animation info (what type of animation). Used to find type of animation when
	// using animation public functions like PlayAnimation.
	std::vector<AnimationInfo> animIDToAnimInfo; // Indexed by animID, not CalAnimationID.
	std::map <RegisteredAnimationName, AnimationID> registeredNameToAnimation; // From name (string) to animation ID for the given type of animation.

	// These two vectors store the actions and cycles that can be played.
	// The first value stores whether it is currently playing and the second the duration.
	// They are accesed by their animation ID.
	std::vector<std::pair<IsAnimationPlaying, AnimationTimeBeingPlayed>> actionsPlaying;
	std::vector<std::pair<IsAnimationPlaying, AnimationTimeBeingPlayed>> cyclesPlaying;

	// Root motion variables.
	std::vector<IsAnimationPlaying> isCyclePlayingAsRoot;
	std::vector<IsAnimationPlaying> isActionPlayingAsRoot;
	// How many root animations (cycles or vectors) are being played, if more than one, root displacement gets activated.
	int rootAnimationsPlaying = 0;

	// Whether there are any animations being played. If none, an idle anim gets played (the first one loaded in cal).
	bool noAnimationsPlaying = false;

public:
	/* Constructors. */
	TCompSkeleton();
	~TCompSkeleton();



	// Basic functions.
	void load(const json& j, TEntityParseContext& ctx);
	void renderDebug();
	void debugInMenu();
	static void registerMsgs();

	// Updates the mixer with the current transform position and the dt so the animations play.
	void update(float dt);

	// Updates the vector of bones with the CAL3D bones new positions then,
	// it uploads them to the bones constant buffer.
	void updateCtesBones();

	/* Base getters. */
	// Returns the Cal Model pointer.
	CalModel * getModel();
	// Returns the constant buffer with the bones.
	CCteBuffer<TCteSkinBones> & getCBBones();
	// Returns the game core skeleton. Used by lookat iks debug.
	CGameCoreSkeleton * getGameCoreSkeleton();
	// Get bone absolute pos. Used in iks.
	Vector3 getBoneAbsPos(const std::string & boneName) const;
	CTransform getBoneTransform(const std::string & boneName) const;
	void getBoneTransform(const std::string & boneName, CTransform* transform) const;
	// Get bone relative pos. Used in iks.
	Vector3 getBoneRelativePos(const std::string & boneName) const;

	/* Animation functions. */

	// Plays an animation in the animation mixer. Attention, you can't play a same action multiple times in the mixer.
	// If you need it, change the TCompSkeleton action method behaviour.
	//void PlayAnimation(AnimationID animation, float weight, float delayIn, float delayOut = 0.0f, bool playAsRoot = false, bool autoLock = false);
	void PlayAnimation(AnimationID animID, float weight, float delayIn, float delayOut = 0.0f, bool playAsRoot = false, bool autoLock = false, float startTime = 0.0f);
	void PlayAnimation(std::string animationName, float weight, float delayIn, float delayOut = 0.0f, bool playAsRoot = false, bool autoLock = false, float startTime = 0.0f);
		
	// Updates a cycle animation with new weights. (You can also use the play animation as both call the same Cal3D function).
	void UpdateCycleAnimationWeights(AnimationID animation, float weight, float delayIn, float delayOut = 0.0f, bool playAsRoot = false);
	// Removes an animation from the animation mixer.
	void RemoveAnimation(std::string name, float delayOut = 0.0f);
	void RemoveAnimation(AnimationID animation, float delayOut = 0.0f);

	/* Animation queries. */

	// Returns the animation id (allows for differentiation between animations that use the
	// same cal3d anim but are being played as action or cycles).
	AnimationID getAnimationID(const std::string & animationRegisteredName) const;
	// Get animation name by ID
	std::string getAnimationName(const AnimationID id) const;
	// The animation time factor at which animations are run in the mixer.
	void setAnimatorTimeFactor(float animTimeFactor);
	// Returns the time of the animation since it was started.
	float getAnimationElapsedTime(AnimationID animation) const;
	// Returns the percentage of the animation.
	float getAnimationPercentage(std::string name) const;
	// Returns the duration of the animation.
	float getAnimationDuration(AnimationID animation) const;
	// Returns true if the animation has just started.
	bool isAnimationJustStarted(AnimationID animation) const;
	// Returns true if the animation is executing.
	bool isAnimationExecuting(AnimationID animation) const;
	// Returns true if the animation is not playing.
	bool isAnimationOver(AnimationID animID) const;
	bool isAnimationOver(std::string animationName) const;
	// Returns true if the animation is playing as a root animation.
	bool isAnimationPlayingAsRoot(AnimationID animation) const;

	friend class TCompAnimator;
	/* Callbacks can use methods from the skeleton component. */
	friend struct AnimationCompleteCallback;



	// This function registers an animation to be called from the public animation methods.
	// First parameter is the actual name of the animation in the cal3d format, second name is the name that will be used by programmers
	// and scripters to refer to the animation in jsons (or code if they wish so).
	// This allows us to differentiate between two animations that use the same cal3d animation but one acts as an action and the other as a cycle.
	// This way, we don't have to worry about creating multiple methods to play an animation.
	void registerAnimation(const std::string & cal3DAnimationName, AnimationID animEnumToUse, AnimationType animationType);

private:
	// Loads Cal Skeleton, animations, mesh, ragdolls.
	// Fetches the model and game core skeleton,
	// Adds the handle as data to be used by the model.
	void loadGameCoreSkeletonAndFetchCalModel(const json & j);

	// Called on entity created. Fetches components needed, transform in this case.
	void onEntityCreated(const TMsgEntityCreated & msg);
	// Called on logic status message recieved.
	void onLogicStatus(const TMsgLogicStatus & msg);

	// Updates the time animations have been playing.
	void updateAnimationsDurations(float dt);
	// Checks if no animations are playing, adds the dummy animation (a TPose for example) so the model doesn't dissapear.
	void checkIfNoAnimations();
	// Removes the dummy animation if a new animation is being added.
	void checkIfDummyAnimationIsOn();

	/* Animations front-end. */

	/* Cal3D animations functions. */

	// Registers an animation as a cycle. Sets the structures for controlling the animations.
	void registerCycleAnimation(CalAnimationID animID);
	// Registers an animation as an action. Sets the structures for controlling the animations.
	void registerActionAnimation(CalAnimationID animID);
	// Sets an animation to be played as cycle.
	void SetCyclicAnimation(CalAnimationID animID, float weight, float delayIn, bool playAsRoot);
	// Sets an animation to be played as an action.
	void SetActionAnimation(CalAnimationID animID, float weight, float delayIn, float delayOut, bool autoLock, bool playAsRoot, float startTime = 0.0f);
	// Remove an action animation if it is playing.
	void RemoveActionAnimation(CalAnimationID animID, bool is_call3d_callback = false, float delayOut = 0.0f);
	// Remove a cycle animation if it is playing.
	void RemoveCyclicAnimation(CalAnimationID animID, float delayOut = 0.0f);

	/* Cal3D Animation queries. */

	// Returns the Cal3D ID of an animation.
	CalAnimationID getCalAnimationID(const std::string & animationName) const;
	// Returns the time of the animation.
	float returnAnimationDuration(CalAnimationID animID) const;
	// Returns the time of the action animation.
	float returnActionAnimationCurrentTime(CalAnimationID animID) const;
	// Returns the time of the cycle animation. It will return the time
	// since it was started not the time in the animation.
	float returnCycleAnimationCurrentTime(CalAnimationID animID) const;
	// Returns the percentage of the execution of the action animation.
	float returnActionAnimationPercentage(CalAnimationID animID) const;
	// Returns the percentage of the execution of the cycle animation. As cycles are loops
	// and do not end by default this percentage will go from 1 to 0 if it reaches the 'end' of the animation.
	float returnCycleAnimationPercentage(CalAnimationID animID) const;
	// Checks if a cycle animation is just started. Returns true if so, false ow.
	bool isCycleJustStarted(CalAnimationID animID) const;
	// Checks if a cycle animation is executing. Returns true if so, false ow.
	bool isCycleExecuting(CalAnimationID animID) const;
	// Checks if a cycle animation is over. Returns true if so, false ow. It only checks
	// if the cycle is not being played (doesn't care if it never was played).
	bool isCycleOver(CalAnimationID animID) const;
	// Checks if an action animation has just started. Returns true if so, false ow.
	bool isActionJustStarted(CalAnimationID animID) const;
	// Checks if an action animation is executing. Returns true if so, false ow.
	bool isActionExecuting(CalAnimationID animID) const;
	// Checks if an action animation is not executing. Returns true if so, false ow.
	// It only checks if the action is not being played (doesn't care if it never was played).
	bool isActionOver(CalAnimationID animID) const;
};


#endif
