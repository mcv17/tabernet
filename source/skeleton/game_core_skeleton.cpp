#include "mcv_platform.h"
#include "game_core_skeleton.h"
#include "render/meshes/mesh_io.h"
#include "utils/data_saver.h"
#include "cal3d2engine.h"
#include "render/primitives.h"      // for the max_skeletons_ctes

#pragma comment(lib, "cal3d.lib" )

class CGameCoreSkeletonResourceType : public CResourceType {
public:
  const char* getExtension(int idx) const override { return "skeleton"; }
  const char* getName() const override {
    return "Skeletons";
  }
  IResource* create(const std::string& name) const override {
    Utils::dbg("Creating skeleton resource %s\n", name.c_str());
    CGameCoreSkeleton* res = new CGameCoreSkeleton(name);
    bool is_ok = res->create(name);
    res->setNameAndType(name, this);
    assert(is_ok);
    return res;
  }
};

// A specialization of the template defined at the top of this file
// If someone class getResourceClassOf<CGameCoreSkeletonResourceClass>, use this function:
template<>
const CResourceType* getResourceTypeFor<CGameCoreSkeleton>() {
  static CGameCoreSkeletonResourceType resource_type;
  return &resource_type;
}

bool CGameCoreSkeleton::create(const std::string & res_name) {
	PROFILE_FUNCTION("Skel::Create");

	// Load an initial scene
	json json = Utils::loadJson(res_name);

	std::string name = json["name"];
	root_path = "data/skeletons/" + name + "/";

	CalLoader::setLoadingMode(LOADER_ROTATE_X_AXIS | LOADER_INVERT_V_COORD);
	bool loadedSuccessfully = true;

	// Read the core skeleton
	loadedSuccessfully |= loadSkeleton(name);
	if (!loadedSuccessfully) {
		Utils::dbg("Error while loading skeleton %s\n", name);
		return false;
	}

	// Read all anims
	loadedSuccessfully |= loadAnimations(name, json);
	if (!loadedSuccessfully) {
		Utils::dbg("Error while loading skeleton animations %s\n", name);
		return false;
	}

	if (json.count("scale")) {
		_scale = json["scale"];
		scale(_scale);
	}

	// Check if there is already a .mesh.
	loadedSuccessfully |= loadMesh(name);
	if (!loadedSuccessfully) {
		Utils::dbg("Error while loading skeleton mesh %s\n", name);
		return false;
	}

	// Array of bone ids to debug (auto conversion from array of json to array of ints)
	if (json["bone_ids_to_debug"].is_array())
		bone_ids_to_debug = json["bone_ids_to_debug"].get< std::vector< int > >();

	// Read shared lookat correction set of bones
	if (json.count("lookat_corrections")) {
		auto & lookats = json["lookat_corrections"];

		if (!lookats.count("lookAts"))
			Utils::fatal("Error while loading look at correction info from skeleton json. \"lookAts\" field in the json is wrong. %s\n", name);

		for (auto & lookAt : lookats["lookAts"]) {
			std::string lookAtName = lookAt.value("name", "");
			assert(lookAtName != "" && "Look at name is null, you forgot to add a name for the type of lookat");
			assert((lookat_corrections.find(lookAtName) == lookat_corrections.end()) && "Skeleton already has a lookat with the same name");
			
			lookat_corrections[lookAtName] = std::vector<TBoneCorrection>();
			auto & lookAtBonesArray = lookAt["lookat_bones"];
			for (int i = 0; i < lookAtBonesArray.size(); ++i) {
				TBoneCorrection c;
				c.load(lookAtBonesArray[i]);

				// Resolve the bone_name to bone_id here
				c.bone_id = getCoreSkeleton()->getCoreBoneId(c.bone_name);
				lookat_corrections[lookAtName].push_back(c);
			}
		}
	}

	// Load Ragdoll Core
	loadRagdolls(json);

	return true;
}

void showCoreBoneRecursive(CalCoreSkeleton * core_skel, int bone_id) {
	assert(core_skel);
	if (bone_id == -1) return;
	CalCoreBone* cb = core_skel->getCoreBone(bone_id);
	char buf[128];
	sprintf(buf, "%s [Id:%d]", cb->getName().c_str(), bone_id);
	ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Appearing);
	if (ImGui::TreeNode(buf)) {
		auto children = cb->getListChildId();
		for (auto it : children) {
			showCoreBoneRecursive(core_skel, it);
		}
		ImGui::TreePop();
	}
}

void CGameCoreSkeleton::renderInMenu() {
  auto* core_skel = getCoreSkeleton();
  
  if (ImGui::TreeNode("Hierarchy Bones")) {
    auto& core_bones = core_skel->getVectorRootCoreBoneId();
    for (auto& bone_id : core_bones)
      showCoreBoneRecursive(getCoreSkeleton(), bone_id);
    ImGui::TreePop();
  }

  if (ImGui::TreeNode("All Bones")) {
    auto& core_bones = core_skel->getVectorCoreBone();
    for (int bone_id = 0; bone_id < core_bones.size(); ++bone_id) {
      CalCoreBone* cb = core_skel->getCoreBone(bone_id);
      if (ImGui::TreeNode(cb->getName().c_str())) {
        ImGui::LabelText( "ID", "%d", bone_id );
        if (ImGui::SmallButton("Show Axis"))
          bone_ids_to_debug.push_back(bone_id);
        ImGui::TreePop();
      }
    }
    ImGui::TreePop();
  }

  ImGui::DragFloat("Debug Bones Scale", &bone_ids_debug_scale, 0.01f, 0.1f, 15.0f);

  renderLookAtBonesInMenu();
}

void CGameCoreSkeleton::renderLookAtBonesInMenu() {
	if (ImGui::TreeNode("LookAt Corrections")) {
		for (auto & lookat : lookat_corrections) {
			std::string lookAtTitle = "Look at: " + lookat.first;
			ImGui::Text(lookAtTitle.c_str());
			for (auto & it : lookat.second) {
				ImGui::PushID(&it);
				it.debugInMenu();
				ImGui::PopID();
			}
			ImGui::Spacing(0, 5);
		}
		ImGui::TreePop();
	}
}

/* Private functions. */

bool CGameCoreSkeleton::loadSkeleton(const std::string & name) {
	std::string csf = root_path + name + ".csf";
	bool is_ok = loadCoreSkeleton(csf);
	if (!is_ok)
		return false;
	return true;
}

bool CGameCoreSkeleton::loadMesh(const std::string & name) {
	std::string cmf = root_path + name + ".cmf";
	//if (Utils::fileExists(cmf.c_str())) {
		int mesh_id = loadCoreMesh(cmf);

		auto core_mesh = getCoreMesh(mesh_id);
		core_mesh->scale(_scale);

		if (mesh_id < 0) return false;
		std::string skin_mesh_file = root_path + name + ".mesh";
		convertCalCoreMesh2RenderMesh(getCoreMesh(mesh_id), skin_mesh_file);
		// Delete the cmf file
		// std::remove(cmf.c_str());
	//}
	return true;
}

bool CGameCoreSkeleton::loadAnimations(const std::string & name, const json & json) {
	auto& anims = json["anims"];
	for (auto it = anims.begin(); it != anims.end(); ++it) {
		assert(it->is_object());

		auto& anim = *it;

		// Extract the anim name and path.
		std::string anim_path;
		std::string anim_name;
		bool isCycle;

		anim_path = anim["path"];
		isCycle = anim["isCycle"];

		// Extract the name from the path. Remove / if necessary.
		size_t i = anim_path.rfind('/', anim_path.length());
		if (i != std::string::npos)
			anim_name = anim_path.substr(i + 1, anim_path.length());
		else
			anim_name = anim_path;

		std::string caf = root_path + anim_path + ".caf";
		int anim_id = loadCoreAnimation(caf, anim_name);
		if (anim_id < 0) {
			Utils::fatal("Failed to load animation %s in model %s\n", anim_name.c_str(), name.c_str());
			return false;
		}

		Animation a;
		a.id = anim_id;
		a.name = anim_name;
		a.isCycle = isCycle ? AnimationType::CYCLE : AnimationType::ACTION;
		animations.push_back(a);
	}

	return true;
}

bool CGameCoreSkeleton::loadRagdolls(const json & json) {
  // Load Ragdoll Core
  auto& jragdoll_core = json["ragdoll"];
	if (json["ragdoll"].is_array()) {
		PROFILE_FUNCTION("Ragdolls");
		for (auto& j : jragdoll_core) {
			if (!j.is_null()) {
				ragdoll_core.json_skeleton_shapes_core.resize(ragdoll_core.json_skeleton_shapes_core.size() + 1);
				TJsonSkeletonBoneShape& ragdoll_bone_core = ragdoll_core.json_skeleton_shapes_core.back();
				ragdoll_bone_core.bone = j["bone"];
				ragdoll_bone_core.height = j.value("height", 0.0f);
				ragdoll_bone_core.radius = j.value("radius", 0.1f);
				ragdoll_bone_core.parent_bone = j["parent_bone"];
				ragdoll_bone_core.render_debug = j.value("render_debug", false);

				//Articulation joint configuration
				if (j.count("parent_joint")) {
					if (j["parent_joint"].is_object()) {
						auto& parent_joint = j["parent_joint"];
						TJsonArticulationJoint articulationJoint = {};
						ragdoll_bone_core.articulation = articulationJoint;
						ragdoll_bone_core.articulation.joint_parent_local_position = loadVector3(parent_joint, "joint_parent_local_position");
						ragdoll_bone_core.articulation.joint_parent_local_rotation = loadVector3(parent_joint, "joint_parent_local_rotation");
						ragdoll_bone_core.articulation.twist_limit = loadVector2(parent_joint, "twist_limit");
						ragdoll_bone_core.articulation.twist_limit_enabled = parent_joint.value("twist_limit_enabled", false);
						ragdoll_bone_core.articulation.swing_limit = loadVector2(parent_joint, "swing_limit");
						ragdoll_bone_core.articulation.swing_limit_enabled = parent_joint.value("swing_limit_enabled", false);
					}
				}
			}

			for (auto& core_bone : ragdoll_core.json_skeleton_shapes_core) {
				for (auto& parent_core_bone : ragdoll_core.json_skeleton_shapes_core) {
					if (core_bone.parent_bone == parent_core_bone.bone) {
						core_bone.parent_core = &parent_core_bone;
						break;
					}
				}
			}
		}
	}

  // Load Hitboxes Core
  auto& jhitboxes_core = json["hitboxes"];
  if (json["hitboxes"].is_array()) {
	  for (auto& j : jhitboxes_core) {
		  if (!j.is_null()) {
			  hitboxes_core.json_skeleton_shapes_core.resize(hitboxes_core.json_skeleton_shapes_core.size() + 1);
			  TJsonSkeletonBoneShape& hitboxes_bone_core = hitboxes_core.json_skeleton_shapes_core.back();
			  hitboxes_bone_core.bone = j["bone"];
			  hitboxes_bone_core.height = j.value("height", 0.0f);
			  hitboxes_bone_core.radius = j["radius"].get<float>();
			  hitboxes_bone_core.position_offset = loadVector3(j, "position_offset");
			  hitboxes_bone_core.rotation_offset = loadVector3(j, "rotation_offset");
				hitboxes_bone_core.parent_bone = j.value("parent_bone", "");
			  hitboxes_bone_core.render_debug = j.value("render_debug", true);
				if (hitboxes_bone_core.height == 0.0f && hitboxes_bone_core.parent_bone == "")
					hitboxes_bone_core.height = 0.001f;
		  }
	  }

	  for (auto& core_bone : hitboxes_core.json_skeleton_shapes_core) {
		  for (auto& parent_core_bone : hitboxes_core.json_skeleton_shapes_core) {
			  if (core_bone.parent_bone == parent_core_bone.bone) {
				  core_bone.parent_core = &parent_core_bone;
				  break;
			  }
		  }
	  }
  }

	return true;
}

/* Convert CalCore mesh to an engine mesh. */
struct TSkinVertex {
	VEC3 pos;
	VEC3 normal;
	VEC2 uv;
	VEC4 tangent;
	uint8_t bone_ids[4];
	uint8_t bone_weights[4];    // 0.255   -> 0..1
};

bool CGameCoreSkeleton::convertCalCoreMesh2RenderMesh(CalCoreMesh* cal_mesh, const std::string& ofilename) {
	PROFILE_FUNCTION("CalCore2Mesh");

	CMemoryDataSaver mds_vtxs;
	CMemoryDataSaver mds_idxs;

	auto nbones = this->getCoreSkeleton()->getVectorCoreBone().size();

	auto nsubmeshes = cal_mesh->getCoreSubmeshCount();

	VMeshGroups groups;
	groups.resize(nsubmeshes);

	// Compute total vertexs counting all materials
	int  total_vtxs = 0;
	int  total_faces = 0;
	for (int idx_sm = 0; idx_sm < nsubmeshes; ++idx_sm) {
		CalCoreSubmesh* cal_sm = cal_mesh->getCoreSubmesh(idx_sm);

		groups[idx_sm].first_idx = total_faces * 3;
		groups[idx_sm].num_indices = cal_sm->getFaceCount() * 3;
		groups[idx_sm].material_index = idx_sm;
		groups[idx_sm].user_material_id = idx_sm;

		total_vtxs += cal_sm->getVertexCount();
		total_faces += cal_sm->getFaceCount();
	}

	// For each submesh ( each material...);
	int  acc_vtxs = 0;
	for (int idx_sm = 0; idx_sm < nsubmeshes; ++idx_sm) {

		CalCoreSubmesh* cal_sm = cal_mesh->getCoreSubmesh(idx_sm);
		cal_sm->enableTangents(0, true);
		
		// Copy The vertexs
		auto& cal_vtxs = cal_sm->getVectorVertex();

		auto num_vtxs = cal_sm->getVertexCount();
		auto cal_tangents = cal_sm->getVectorVectorTangentSpace()[0];

		// Access to the first texture coordinate set
		auto& cal_all_uvs = cal_sm->getVectorVectorTextureCoordinate();
		if (cal_all_uvs.empty()) {
			Utils::fatal("The skin mesh %s does not have texture coords!\n", ofilename.c_str());
			//return false;
			// Create some fake/empty tex coords
			cal_all_uvs.resize(1);
			cal_all_uvs[0].resize(num_vtxs);
		}
		auto& cal_uvs0 = cal_all_uvs[0];

		// Process the vtxs
		for (int vid = 0; vid < num_vtxs; ++vid) {
			CalCoreSubmesh::Vertex& cal_vtx = cal_vtxs[vid];

			// Prepare a clean vertexs
			TSkinVertex skin_vtx;
			memset(&skin_vtx, 0x00, sizeof(TSkinVertex));

			// Pos & Normal
			skin_vtx.pos = Cal2DX(cal_vtx.position);
			skin_vtx.normal = Cal2DX(cal_vtx.normal);

			VEC3 tVec = Cal2DX(cal_tangents[vid].tangent);
			skin_vtx.tangent = VEC4(tVec.x, tVec.y, tVec.z, cal_tangents[vid].crossFactor);

			// Texture coords
			skin_vtx.uv.x = cal_uvs0[vid].u;
			skin_vtx.uv.y = cal_uvs0[vid].v;

			// Weights...
			int total_weight = 0;
			for (size_t ninfluence = 0; ninfluence < cal_vtx.vectorInfluence.size() && ninfluence < 4; ++ninfluence) {
				auto cal_influence = cal_vtx.vectorInfluence[ninfluence];
				assert(cal_influence.boneId < MAX_SUPPORTED_BONES);
				skin_vtx.bone_ids[ninfluence] = (uint8_t)(cal_influence.boneId);
				assert(skin_vtx.bone_ids[ninfluence] < nbones);

				// Convert cal3d influence from 0..1 to a char from 0..255
				skin_vtx.bone_weights[ninfluence] = (uint8_t)(255.f * cal_influence.weight);

				total_weight += skin_vtx.bone_weights[ninfluence];
			}

			// Confirm the sum of rounded weights are still 1  = > 255
			int err = 255 - total_weight;
			if (err != 0) {
				assert(err > 0);
				skin_vtx.bone_weights[0] += err;
			}

			mds_vtxs.write(skin_vtx);
		}

		// Process the faces
		auto num_faces = cal_sm->getFaceCount();
		const auto& cal_faces = cal_sm->getVectorFace();
		for (auto& face : cal_faces) {
			static int lut[3] = { 0,2,1 };
			for (int i = 0; i < 3; ++i) {
				int my_i = lut[i];      // swap face culling

				uint32_t vertex_id = face.vertexId[my_i];

				// The second ground of vertexs, have the face_id not starting in the zero
				// it starts in the previous count of vertex we have defined
				// VertexSubMesh0... | VertexSubMesh1 ... |...
				vertex_id += acc_vtxs;

				// Cal is defined to be 32 bits per index
				if (total_vtxs > 65535) {
					mds_idxs.write((uint32_t)vertex_id);   // 4 bytes per index
				}
				else {
					mds_idxs.write((uint16_t)vertex_id);   // 2 bytes per index
				}
			}
		}

		// Keep the total accumulated vertexs
		acc_vtxs += num_vtxs;
	}

	TRawMesh raw_mesh = {};
	auto& header = raw_mesh.header;
	header.bytes_per_index = total_vtxs > 65535 ? sizeof(uint32_t) : sizeof(uint16_t);
	header.bytes_per_vertex = sizeof(TSkinVertex);
	header.num_indices = total_faces * 3;
	header.num_vertex = total_vtxs;
	header.num_groups = (uint32_t)groups.size();
	header.primitive_type = CMesh::TRIANGLE_LIST;
	strcpy(header.vertex_type_name, "PosNUvTanSkin");

	raw_mesh.vertices = mds_vtxs.buffer;
	raw_mesh.indices = mds_idxs.buffer;
	raw_mesh.groups = groups;

	CFileDataSaver fds(ofilename.c_str());
	raw_mesh.save(fds);

	return true;
}