#pragma once

// FABRIK algorithm for calculation of IKs. Support more than three bones by using an
// iterative solution.
// Based on the information from: https://developer.roblox.com/articles/Inverse-Kinematics-for-Animation
// And: https://www.youtube.com/watch?v=UNoX65PRehA

class FABRIKSolver {
public:
	enum FABRIKState {
		UNREACHABLE, // Can't reach pos.
		REACHABLE, // Can reach pos.
		REACHABLE_NOT_THRESHOLD // Can reach pos but not meet threshold.
	};

private:
	std::vector<Vector3> jointPositions; // Starting position of each joint.
	std::vector<float> lengthsBetweenJoints; // Length between joints. P0-P1, P1-P2, P2-P3, ...
	
	FABRIKState state;

	Vector3 origin; // Origin.
	Vector3 target; // Point to reach.

	float totalLength = 0.0f; // Sum of all the lengths between joints.
	float tolerance = 0.05; // Error margin of our solution.

	// Checks if the point is too far away, if so, returns the bones fully extended
	// in the direction of the target.
	bool checkIfUnreachable();

	// Calculates the backward phase of the algorithm.
	void backwardPhase();
	// Calculates the forward phase of the algorithm.
	void forwardPhase();

public:
	void init(int numberOfJoints);

	// Adds a new joint to the system, add all the bones positions that should participate in the ik calculation.
	void addJoint(const Vector3 & jointPosition);

	// Call it each frame with the current position of the bone before applying ik.
	void updateJointPosition(int id, const Vector3 & jointPosition);

	// Set the targetting goal.
	void setTarget(const Vector3 & nTarget);

	// Solves the IK problem and returns the final positions for all bones.
	// !Important! The first bone doesn't start position so only the positions for
	// all bones after the starting one are returned.
	const std::vector <Vector3> & solve();

	// Render in menu.
	void renderInMenu();

	// Only call after calling solve, returns the solved iks positions.
	const std::vector <Vector3> & getResults() { return jointPositions;	}

	// Get solver state. Can be unreachable, reachable or reachable but threshold not meet.
	FABRIKState getSolverState() { return state; }

	// How close to the solution must be to the target, the bigger the tolerance, the worse
	// the solution will be but less iterations will be needed.
	void setSolverTolerance(float nTolerance) { tolerance = nTolerance; }

	// Used to get the threshold.
	float getThreshold() { return tolerance;}
};