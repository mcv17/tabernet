#include "mcv_platform.h"
#include "FABRIKSolver.h"


void FABRIKSolver::init(int numberOfJoints) {
	jointPositions.reserve(numberOfJoints);
	lengthsBetweenJoints.reserve(numberOfJoints-1);
}

void FABRIKSolver::addJoint(const Vector3 & jointPosition) {
	jointPositions.push_back(jointPosition);

	// Calc distance with previous joint.
	if (jointPositions.size() > 1) {
		lengthsBetweenJoints.push_back((jointPosition
			- jointPositions[jointPositions.size() - 2]).Length());

		// Add length to the sum of lengths.
		totalLength += lengthsBetweenJoints.back();
	}
}

void FABRIKSolver::updateJointPosition(int id, const Vector3 & jointPosition) {
	jointPositions[id] = jointPosition;
}

void FABRIKSolver::setTarget(const Vector3 & nTarget) {
	target = nTarget;
}

bool FABRIKSolver::checkIfUnreachable() {
	// Get the distance to the target.
	float distanceToTarget = (jointPositions[0] - target).Length();
	
	// Check if unreachable. If so, fully extend the bones.
	if (distanceToTarget > totalLength) {
		// Out of reach.

		// Target is out of reach
		for (int i = 0; i < jointPositions.size() - 1; ++i) {
			// Create direct line to target and travel join distance across it
			float r = (target - jointPositions[i]).Length();
			float l = lengthsBetweenJoints[i] / r;
			// find new joint position
			jointPositions[i + 1] = (1 - l) * jointPositions[i] + l * target;
		}

		return true;
	}
	return false;
}

// Calculates the backward phase of the algorithm.
void FABRIKSolver::backwardPhase() {
	// Backward reaching. Set the end effector as target.
	jointPositions[jointPositions.size() - 1] = target;

	for (int i = jointPositions.size() - 2; i >= 0; --i) {
		float r = (jointPositions[i + 1] - jointPositions[i]).Length();
		float l = lengthsBetweenJoints[i] / r;
		
		// Find new joint position
		Vector3 pos = (1.0 - l) * jointPositions[i + 1] + l * jointPositions[i];
		jointPositions[i] = pos;
	}
}

// Calculates the backward phase of the algorithm.
void FABRIKSolver::forwardPhase() {
	// Forward reaching; set root at initial position
	jointPositions[0] = origin;

	for (int i = 0; i <= jointPositions.size() - 2; --i) {
		float r = (jointPositions[i + 1] - jointPositions[i]).Length();
		float l = lengthsBetweenJoints[i] / r;
		
		// Find new joint position
		Vector3 pos = (1 - l) * jointPositions[i] + l * jointPositions[i + 1];
		jointPositions[i + 1] = pos;
	}
}

const std::vector <Vector3> & FABRIKSolver::solve() {
	origin = jointPositions[0];


	// Check if unreachable. If so, fully extend the bones.
	if (checkIfUnreachable()) {
		state = UNREACHABLE;
		return jointPositions;
	}

	state = REACHABLE;

	// Now we start the with the FABRIK algorithm.
	int iteration = 0;
	float dif = (jointPositions[jointPositions.size()-1] - target).Length();

	while (dif > tolerance) {
		// First phase is called backwards, where we start by setting the end positin,
		// and trying to find an estimate of how the bones should look like.
		backwardPhase();

		// Second phase is called forwards, we now use the calculated points and start
		// by the beggining using them to calculate a final position, it's not a perfect
		// solution but can be iterated for more precision and works okay for our case
		// where perfect precision is not necessary.
		forwardPhase();

		dif = (jointPositions[jointPositions.size() - 1] - target).Length();
		iteration++;

		if (iteration > 10) {
			state = REACHABLE_NOT_THRESHOLD;
			break;
		}
	}

	return jointPositions;
}

void FABRIKSolver::renderInMenu() {
	ImGui::LabelText("IK State", "%d", state);
	ImGui::DragFloat("IK Tolerance.", &tolerance, 0.01f, 0.0f, 10.f);
}