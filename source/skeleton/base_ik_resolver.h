#pragma once

#include "utils/WeightController.h"

class CalModel;
class TCompTransform;

struct TIKBaseResolver {
	bool isActive = false;

	/* Targetting variables. */

	// Entity targetting variables.
	bool isTargettingEntity; // Are we targetting an entity or a position.
	std::string target_entity_name; // Nameof the target entity to track.
	CHandle     h_target_entity; // Handle to the target entity being tracked.

	// Absolute position to move c to. Used if no entity is being targetted.
	Vector3   target;
	Vector3   current_target_pos = current_target_pos;

	// Speed at which the IK will follow changes on the position to be at. 0.0f = no follow,
	// > 0.0f equals a speed to follow changes, < 0.0f equals instant change.
	// By default, instant change.
	float speed_target_following;

	/* Amount variables. */

	// Controls the amount of the ik. Starts working when the ik gets activated.
	// Create your own class inheriting from this to control the IK value.
	// Otherwise, leave it to null and modify the values of this IK directly.
	WeightController::IWeightController * weightController = nullptr;
	float	amount = 1.f; // When 1.0f, we are pointing to the position, when 0.0f, we are not affecting the bone.

	/* Public functions */

	virtual void load(const json & j, CalModel * model) = 0;
	virtual void update(const TCompTransform * transform, CalModel * model, float dt) = 0;
	virtual void renderInMenu() = 0;
	virtual void renderInDebug() = 0;

	void setIKStatus(bool active);
	void setTargetPosition(const Vector3 & nTarget);
	void setTargetEntity(CHandle nEntity);
	void setTargetEntityByName(const std::string & entity_name);
	void setIKAmount(float nAmount) { amount = nAmount; }
	void setSpeedTargetFollowing(float speedTarget) { speed_target_following = speed_target_following; }

protected:

	/* Target functions. */
	// Gets the handle of the entity if we are following one.
	void fetchEntityTargetting();
	// Update the target position if we are following an entity.
	void updateTargetPos(float dt);
};
