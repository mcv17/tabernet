#pragma once

#include "base_ik_resolver.h"
#include "ik_handle.h"

// Struct for arithmetic bones resolution.

struct TIKArithmeticBonesResolution : public TIKBaseResolver {
	/* Bones of the IK. */

	// C is the bone that must be moved to the position (In an arm, the hand).
	// A is the bone that acts as first motor. (The shoulder for example).
	// B is the bone that must be found it's new position and acts as second motor (the ankle in the previous example).

	int	bone_id_c = -1;       // The bone that must be in an specific position.
	int bone_id_b = -1;		  // The parent bone of the bone c, (or a bone specified by the user).
	int bone_id_a = -1;		  // The parent bone of the bone b, (or a bone specified by the user).

	float ABLength; // Distance from bone A to B.
	float BCLength; // Distance from bone B to C.

	/* Public functions */

	void load(const json & j, CalModel * model) override;
	void update(const TCompTransform * transform, CalModel * model, float dt) override;
	void renderInMenu() override;
	void renderInDebug() override;

private:
	/* IK Handle. */

	// This struct resolves the IK.
	TIKHandle ik;

	/* Load functions. */

	// Get the bones ids from the config file.
	void loadBones(const json & j, CalModel * model);
	// Loads the target for the IKs.
	void loadTarget(const json & j);
};