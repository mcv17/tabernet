#include "mcv_platform.h"
#include "fabrik_iks.h"

#include "comp_skeleton_iks.h"
#include "bone_correction.h"
#include "game_core_skeleton.h"
#include "cal3d2engine.h"

#include "components/common/comp_transform.h"

void TIKFABRIKBonesResolution::load(const json & j, CalModel * model) {
	isActive = j.value("active", false);

	// Get the bones used by the IK.
	loadBones(j, model);

	// Get the target or entity that must be followed.
	loadTarget(j);

	// Get a weight controller if there is one.
	weightController = WeightController::WeightControllerManager::Instance().createState(j.value("weightControllerType", ""));
	if (weightController)
		weightController->load(j);

	// Add a tolerance for the solver if necessary.
	if (j.count("solver_tolerance"))
		fabrik.setSolverTolerance(j["solver_tolerance"]);

	// Get targetting speed, by default -1.0f, this equals, no change.
	speed_target_following = j.value("targetFollowingSpeed", -1.0f);
}

void TIKFABRIKBonesResolution::loadBones(const json & j, CalModel * model) {
	// Fetch the ids for the given bone.
	for (auto & boneName : j["bones"])
		boneIds.push_back(model->getSkeleton()->getCoreSkeleton()->getCoreBoneId(boneName));

	// Initiate the FABRIK solver by adding the bones in their core position, this will be used to calculate
	// the distance between joints (or bones).
	fabrik.init(boneIds.size());
	for (int i = 0; i < boneIds.size(); i++)
		fabrik.addJoint(Cal2DX(model->getSkeleton()->getCoreSkeleton()->getCoreBone(boneIds[i])->getTranslationAbsolute()));

	bonesNormalPositions.resize(boneIds.size());
}


void TIKFABRIKBonesResolution::loadTarget(const json & j) {
	isTargettingEntity = j.find("target_entity_name") != j.end();
	std::string targetEntityName = j.value("target_entity_name", "");
	Vector3 targetPos = loadVector3(j, "target_pos");
}

void TIKFABRIKBonesResolution::update(const TCompTransform * transform, CalModel * model, float dt) {
	// If we have a weight controller, we let the weight controller decide if to keep active or not.
	// The weight controller should have recieved the setWeightControllerStatus call and start closing
	// if necessary, once it closes, it will set the is active to false.
	if (weightController && !weightController->getWeightControllerStatus()) return;
	else if (!isActive) return;

	// If we are targetting an entity, fetch it.
	fetchEntityTargetting();

	// If we have a target entity, use it to assign a target positiongit push
	updateTargetPos(dt);

	// Calculate the amount for the given animation.
	if (weightController)
		weightController->update(dt, amount);

	// Send the solver the current position of the bones.
	for (int i = 0; i < boneIds.size(); ++i) {
		Vector3 boneNormalPos = Cal2DX(model->getSkeleton()->getBone(boneIds[i])->getTranslationAbsolute());
		fabrik.updateJointPosition(i, Cal2DX(model->getSkeleton()->getBone(boneIds[i])->getTranslationAbsolute()));
	}

	// Send the solver the target to reach.
	fabrik.setTarget(current_target_pos);

	// Solve the problem and get the new positions for each bone.
	const std::vector<Vector3> & bonesIKPosition = fabrik.solve();


	// Now, update the skeleton considering the amount variable, used for lerping between ik and normal position.
	CalSkeleton * skeleton = model->getSkeleton();
	for (int i = 0; i < bonesIKPosition.size() - 1; ++i) {
		int boneID = boneIds[i];
		int boneToPick = i + 1;

		CalBone * bone = skeleton->getBone(boneID);
		CalBone * nextBone = skeleton->getBone(boneIds[boneToPick]);

		bonesNormalPositions[i] = Cal2DX(nextBone->getTranslationAbsolute());
		float dist = (nextBone->getCoreBone()->getTranslationAbsolute() - bone->getCoreBone()->getTranslationAbsolute()).length();

		// If same pos, get the next bone. This is added due to twist bones in our character.
		// Who share same position with other bones and may crash the corrector otherwise.
		// In the future I may try adding some kind of vector to fetch bone and avoid this hack.
		while (dist < 0.0005f && boneToPick < (bonesIKPosition.size() - 1)) {
			boneToPick++;
			nextBone = skeleton->getBone(boneIds[boneToPick]);
			dist = (nextBone->getCoreBone()->getTranslationAbsolute() - bone->getCoreBone()->getTranslationAbsolute()).length();
		}

		// Ignore. This shouldn't happen.
		assert(!(dist < 0.0005f));
		if (dist < 0.0005f) continue;

		TBoneCorrection corrector = TBoneCorrection();
		corrector.bone_id = boneID;
		corrector.local_axis_to_correct = Vector3(1.0, 0.0, 0.0f);
		corrector.amount = 1.f;
		corrector.apply(skeleton, bonesIKPosition[boneToPick], amount);
	}
}

void TIKFABRIKBonesResolution::renderInMenu() {
	ImGui::DragFloat3("Target", &target.x, 0.01f, -1.f, 1.f);
	if (isTargettingEntity)
		ImGui::Text("Target Name", "%s", target_entity_name);

	ImGui::DragFloat3("Current Target Position", &current_target_pos.x, 0.01f, -1.f, 1.f);
	ImGui::DragFloat("Amount", &amount, 0.01f, 0.f, 1.f);

	ImGui::Text("Amount control variables");
	ImGui::Spacing();
	if (weightController)
		weightController->renderInMenu();
	ImGui::Spacing();

	ImGui::Text("Speed Targetting Following %f", speed_target_following);

	ImGui::Spacing(0, 5);

	ImGui::Text("IK Bones");
	ImGui::Spacing(0, 5);

	const std::vector<Vector3> & bonesIKPosition = fabrik.getResults();

	// Send the solver the current position of the bones.
	for (int i = 0; i < bonesIKPosition.size(); ++i) {
		std::string label = "Bone with ID " + std::to_string(boneIds[i]) + " :";
		Vector3 pos = bonesIKPosition[i];
		ImGui::DragFloat3(label.c_str(), &pos.x, 1.0f, -10000.0f, 10000.0f);
	}

	fabrik.renderInMenu();
}

void TIKFABRIKBonesResolution::renderInDebug() {
	// Green if IK is correct, red otherwise.
	Vector4 ik_color(0, 1, 0, 1);
	if (fabrik.getSolverState() == FABRIKSolver::FABRIKState::UNREACHABLE)
		ik_color = Vector4(1, 0, 0, 1);
	else if(fabrik.getSolverState() == FABRIKSolver::FABRIKState::REACHABLE_NOT_THRESHOLD)
		ik_color = Vector4(1, 0, 1, 1);

	// Send the solver the current position of the bones.
	const std::vector<Vector3> & bonesIKPosition = fabrik.getResults();
	for (int i = 0; i < bonesIKPosition.size(); ++i)
		drawWiredSphere(Matrix::CreateTranslation(bonesIKPosition[i]), 0.025, ik_color);

	for (int i = 0; i < bonesNormalPositions.size(); ++i)
		drawWiredSphere(Matrix::CreateTranslation(bonesNormalPositions[i]), 0.025, Vector4(1,1,0,0));
}

/*

// Now, update the skeleton considering the amount variable, used for lerping between ik and normal position.
	CalSkeleton * skeleton = model->getSkeleton();
	for (int i = 0; i < bonesIKPosition.size()-1; ++i){
		int boneID = boneIds[i];
		int boneToPick = i + 1;

		CalBone * bone = skeleton->getBone(boneID);
		CalBone * nextBone = skeleton->getBone(boneIds[boneToPick]);



		bonesNormalPositions[i] = Cal2DX(nextBone->getTranslationAbsolute());

		//Vector3 newPos = Vector3::Lerp(Cal2DX(nextBone->getTranslationAbsolute()), bonesIKPosition[boneToPick], amount);
		Vector3 newPos = bonesIKPosition[boneToPick];

		float dist = (nextBone->getCoreBone()->getTranslationAbsolute() - bone->getCoreBone()->getTranslationAbsolute()).length();

		// If same pos, get the next bone. This is added due to twist bones in our character.
		// Who share same position with other bones and may crash the corrector otherwise.
		// In the future I may try adding some kind of vector to fetch bone and avoid this hack.
		while(dist < 0.0005f && boneToPick < (bonesIKPosition.size() - 1)) {
			boneToPick++;
			nextBone = skeleton->getBone(boneIds[boneToPick]);
			newPos = bonesIKPosition[boneToPick];
			dist = (nextBone->getCoreBone()->getTranslationAbsolute() - bone->getCoreBone()->getTranslationAbsolute()).length();
		}

		// Ignore. This shouldn't happen.
		assert(!(dist < 0.0005f));
		if (dist < 0.0005f) continue;

		TBoneCorrection corrector = TBoneCorrection();
		corrector.bone_id = boneID;
		corrector.local_axis_to_correct = Vector3(1.0, 0.0, 0.0f);
		corrector.amount = 1.f;
		corrector.apply(skeleton, newPos, 1.0f);
	}

*/


/*

	// Now, update the skeleton considering the amount variable, used for lerping between ik and normal position.
	CalSkeleton * skeleton = model->getSkeleton();
	//float d = (skeleton->getCoreSkeleton()->getCoreBone("Bip001 L UpperArm")->getTranslationAbsolute() - skeleton->getCoreSkeleton()->getCoreBone("Bip001 L Hand")->getTranslationAbsolute()).length();


	for (int i = 0; i < bonesIKPosition.size()-1; ++i){
		int boneID = boneIds[i];
		int boneToPick = i + 1;

		CalBone * bone = skeleton->getBone(boneID);
		CalBone * nextBone = skeleton->getBone(boneIds[boneToPick]);

		bonesNormalPositions[i] = Cal2DX(nextBone->getTranslationAbsolute());
		float dist = (nextBone->getCoreBone()->getTranslationAbsolute() - bone->getCoreBone()->getTranslationAbsolute()).length();

		// If same pos, get the next bone. This is added due to twist bones in our character.
		// Who share same position with other bones and may crash the corrector otherwise.
		// In the future I may try adding some kind of vector to fetch bone and avoid this hack.
		while(dist < 0.0005f && boneToPick < (bonesIKPosition.size() - 1)) {
			boneToPick++;
			nextBone = skeleton->getBone(boneIds[boneToPick]);
			dist = (nextBone->getCoreBone()->getTranslationAbsolute() - bone->getCoreBone()->getTranslationAbsolute()).length();
		}

		// Ignore. This shouldn't happen.
		assert(!(dist < 0.0005f));
		if (dist < 0.0005f) continue;

		TBoneCorrection corrector = TBoneCorrection();
		corrector.bone_id = boneID;
		corrector.local_axis_to_correct = Vector3(1.0, 0.0, 0.0f);
		corrector.amount = 1.f;
		corrector.apply(skeleton, bonesIKPosition[boneToPick], amount);
	}

*/