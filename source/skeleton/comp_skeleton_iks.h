#pragma once

#include "components/common/comp_base.h"
#include "../entity/entity.h"

#include "base_ik_resolver.h"
#include "arithmetic_iks.h"
#include "fabrik_iks.h"

class CalModel;
class TCompTransform;
struct TMsgEntityCreated;

/* Skeleton IK's component. Supports solving bone resolution for IK's of three different bones. */
class TCompSkeletonIKs : public TCompBase {
	DECL_SIBLING_ACCESS();

	json loadedJson;

	// A map holding all ik bone resolutions that are present in the entity, whether active or not.
	std::map<std::string, TIKBaseResolver *> ik_bone_resolutions;

	//Vector3 a = Vector3(2.0f, 2.20f, 0.0f);

	void onEntityCreated(const TMsgEntityCreated & msg);

public:

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();
	void renderDebug();

	static void registerMsgs();

	// Activates a ik, which will start affecting the bones it was marked to affect.
	void activateIK(const std::string & lookAt);
	void activateIK(const std::string & lookAt, const Vector3 & nTarget);
	void activateIK(const std::string & lookAt, CHandle nEntity);

	// Deactivates an ik, which will stop affecting the bones it was marked to affect.
	void deactivateIK(const std::string & lookAt);

	// Returns the ik info if you want to modify it's values.
	TIKBaseResolver * getIKInfo(const std::string & lookAt);
};
