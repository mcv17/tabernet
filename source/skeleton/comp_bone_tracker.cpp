#include "mcv_platform.h"
#include "comp_bone_tracker.h"
#include "components/common/comp_transform.h"
#include "skeleton/comp_skeleton.h"
#include "cal3d/cal3d.h"
#include "cal3d2engine.h"
#include "entity/entity_parser.h"

#include "entity/common_msgs.h"
#include "geometry/geometry.h"

DECL_OBJ_MANAGER("bone_tracker", TCompBoneTracker);

void TCompBoneTracker::load(const json& j, TEntityParseContext& ctx) {
	bone_name   = j.value("bone", "");
	parent_name = j.value("parent", "");

	offset_from_bone = loadVector3(j, "offset_from_bone");

	Vector3 angles = loadVector3(j, "rotation_offset_from_bone");
	yawPitchRollOffset = angles;
	float yaw = DEG2RAD(angles.y);
	float pitch = DEG2RAD(angles.x);
	float roll = DEG2RAD(angles.z);
	rotation_offset_from_bone = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);

	assert(!bone_name.empty());
	assert(!parent_name.empty());

	fetchHandlesOnLoad(ctx);
}

void TCompBoneTracker::update(float dt) {
	if (!fetchHandles()) return;

	// Access to the bone 'bone_id' of the skeleton
	TCompSkeleton * c_skel = h_skeleton;
	auto cal_bone = c_skel->getModel()->getSkeleton()->getBone(bone_id);
	QUAT rot = Cal2DX(cal_bone->getRotationAbsolute());
	VEC3 pos = Cal2DX(cal_bone->getTranslationAbsolute());

	// Apply the cal3d pos&rot to my entity owner
	TCompTransform * tmx = getComponent<TCompTransform>();
	tmx->setRotation(rotation_offset_from_bone * rot);
	Vector3 offset_from_bone_rotated = VEC3::Transform(offset_from_bone, tmx->getRotation());
	tmx->setPosition(pos + offset_from_bone_rotated);
}

void TCompBoneTracker::debugInMenu() {
	ImGui::DragFloat3("Pos offset", &offset_from_bone.x, 0.001f, -50.0f, 50.0f);
	if (ImGui::DragFloat3("Rot offset", &yawPitchRollOffset.x, 0.1f, -360.0f, 360.0f)) {
		float yaw = DEG2RAD(yawPitchRollOffset.y);
		float pitch = DEG2RAD(yawPitchRollOffset.x);
		float roll = DEG2RAD(yawPitchRollOffset.z);
		rotation_offset_from_bone = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);
	}
}

bool TCompBoneTracker::fetchHandles() {
	if (!h_skeleton.isValid()) {
		// Search the parent entity by name
		CEntity * e_entity = getEntityByName(parent_name);
		if (!e_entity) return false;

		// The entity I'm tracking should have an skeleton component.
		h_skeleton = e_entity->getComponent<TCompSkeleton>();
		if (!h_skeleton.isValid()) return false;

		TCompSkeleton * c_skel = h_skeleton;
		if (bone_id == -1) {
			bone_id = c_skel->getModel()->getCoreModel()->getCoreSkeleton()->getCoreBoneId(bone_name);
			assert(bone_id != -1 && "Bone was not found."); // The skeleton don't have the bone with name 'bone_name'
		}
	}
	return true;
}

void TCompBoneTracker::fetchHandlesOnLoad(TEntityParseContext & ctx) {
	if (!h_skeleton.isValid()) {
		// Search the parent entity by name
		CEntity * e_parent = ctx.findEntityByName(parent_name);
		if (!e_parent) return;

		// The entity I'm tracking should have an skeleton component.
		h_skeleton = e_parent->getComponent<TCompSkeleton>();
		assert(h_skeleton.isValid());
		TCompSkeleton * c_skel = h_skeleton;
		if (bone_id == -1) {
			bone_id = c_skel->getModel()->getCoreModel()->getCoreSkeleton()->getCoreBoneId(bone_name);
			assert(bone_id != -1 && "Bone was not found."); // The skeleton don't have the bone with name 'bone_name'
		}
	}
}

void TCompBoneTracker::registerMsgs() {
	DECL_MSG(TCompBoneTracker, TMsgEntitiesGroupCreated, onGroupCreated);
}

void TCompBoneTracker::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	fetchHandles();
}

void TCompBoneTracker::setNewBoneAndParentToTrack(const std::string & parentName, const std::string & boneName){
	bone_name = boneName;
	parent_name = parentName;
	fetchHandles();
}

void TCompBoneTracker::setNewBoneToTrack(const std::string & boneName){
	bone_name = boneName;

	// Access to the bone 'bone_id' of the skeleton
	TCompSkeleton * c_skel = h_skeleton;
	if (!c_skel) return;
	
	bone_id = c_skel->getModel()->getCoreModel()->getCoreSkeleton()->getCoreBoneId(bone_name);
	assert(bone_id != -1 && "Bone was not found."); // The skeleton don't have the bone with name 'bone_name'
}