#pragma once

struct TJsonArticulationJoint {
	VEC3 joint_parent_local_position;
	VEC3 joint_parent_local_rotation;
	VEC2 twist_limit;
	bool twist_limit_enabled;
	VEC2 swing_limit;
	bool swing_limit_enabled;
};

struct TJsonSkeletonBoneShape {
  std::string bone;
  std::string parent_bone;
  float height;
  float radius;
  VEC3 position_offset;
  VEC3 rotation_offset;
  TJsonSkeletonBoneShape* parent_core;
  int instance_idx;
  TJsonArticulationJoint articulation;
  bool render_debug;
};


struct TJsonSkeletonShapes{
  std::vector<TJsonSkeletonBoneShape> json_skeleton_shapes_core;
};