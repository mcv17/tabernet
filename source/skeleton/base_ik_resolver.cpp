#include "mcv_platform.h"
#include "base_ik_resolver.h"

#include "components/common/comp_transform.h"

void TIKBaseResolver::fetchEntityTargetting() {
	if (isTargettingEntity && !target_entity_name.empty() && !h_target_entity.isValid())
		h_target_entity = getEntityByName(target_entity_name);
}

void TIKBaseResolver::updateTargetPos(float dt) {
	if (isTargettingEntity) {
		CEntity * target_entity = h_target_entity;
		if (!target_entity) return;
		TCompTransform * target_transform = target_entity->getComponent<TCompTransform>();
		target = target_transform->getPosition();
	}

	Vector3 directionToTarget = target - current_target_pos;
	directionToTarget.Normalize();
	if (speed_target_following >= 0.0f)
		current_target_pos = current_target_pos + directionToTarget * speed_target_following * dt;
	else
		current_target_pos = target;
}

void TIKBaseResolver::setIKStatus(bool active) {
	// Weight controller controls when to deactivate (for fade ins and outs).
	if (weightController) {
		if (active) isActive = active;
		weightController->setWeightControllerStatus(active);
	}
	else
		isActive = active;
}

void TIKBaseResolver::setTargetPosition(const Vector3 & nTarget) {
	isTargettingEntity = false;
	target = nTarget;
}

void TIKBaseResolver::setTargetEntity(CHandle nEntity) {
	isTargettingEntity = true;
	h_target_entity = nEntity;
}

void TIKBaseResolver::setTargetEntityByName(const std::string & entity_name) {
	isTargettingEntity = true;
	target_entity_name = entity_name;
	h_target_entity = getEntityByName(target_entity_name);
}
