#pragma once

#include "input/button.h"
#include "input/input_enums.h"

namespace Input
{
	struct TGamepadData
	{
		float buttons[BT_GAMEPAD_COUNT] = { };
		bool connected = false;
		float xSensitivity = 0.2f;
		float ySensitivity = 0.2f;
	};

  class CGamepad
  {
  public:

		void update(const TGamepadData& data, float delta);

		TButton _buttons[BT_GAMEPAD_COUNT];
		bool _connected = false;

	private:
		bool _initialized = false;

	};
}
