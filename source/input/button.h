#pragma once

#include "mcv_platform.h"

namespace Input
{
	/* Represents a button from a device. */
	struct TButton
	{
		float value = 0.f;
		float prevValue = 0.f;
    float timePressed = 0.f;

		// Buttons is being pressed.
    bool isPressed() const{	return value != 0.f; }
		// Button was pressed in the last frame. Can be pressed in the current one.
    bool wasPressed() const{ return prevValue != 0.f; }
		// Button was just pressed, wasn't previously.
    bool justPressed() const{ return prevValue == 0.f && value != 0.f; }
		//Button was just released, was pressed before.
    bool justReleased() const{ return prevValue != 0.f && value == 0.f; }
		// Button just changed state
		bool justChanged() const { return prevValue != value; }

    void update(float newValue, float dt)
    {
			prevValue = value;
      value = newValue;

      if (justPressed())
				timePressed = 0.f;
			else if (isPressed())
				timePressed += dt;
		}
		
		operator bool() const{ return isPressed(); }
		
		static const TButton dummy;
  };
}
