#pragma once

#include "mcv_platform.h"
#include "windows/app.h"
#include "input/devices/device_mouse.h"
#include "input/interfaces/mouse.h"
#include "input/input_enums.h"
#include <Windowsx.h>

#include "engine.h"

void CDeviceMouse::update(Input::TMouseData& data)
{
	data.position.x = static_cast<float>(_posX);
	data.position.y = static_cast<float>(_posY);

	data.rawDelta.x = static_cast<float>(_rawDeltaX);
	data.rawDelta.y = static_cast<float>(_rawDeltaY);

	for (int i = 0; i < Input::BT_MOUSE_COUNT; ++i)
	{
		data.buttons[i] = _buttons[i];
	}
	data.wheelDelta = static_cast<float>(_wheelDeltaSteps);

	_wheelDeltaSteps = 0;
	_rawDeltaX = 0;
	_rawDeltaY = 0;
}

void CDeviceMouse::processWindowMsg(UINT message, WPARAM wParam, LPARAM lParam)
{
  if (!_active)
	return;

  switch (message)
  {
    case WM_MOUSEMOVE:
    {
      _posX += GET_X_LPARAM(lParam);
      _posY += GET_Y_LPARAM(lParam);
      break;
    }
	//Raw mouse
	case WM_INPUT:
	{
		UINT dwSize = 48;
		static BYTE lpb[48];

		GetRawInputData((HRAWINPUT)lParam, RID_INPUT,
			lpb, &dwSize, sizeof(RAWINPUTHEADER));

		RAWINPUT* raw = (RAWINPUT*)lpb;

		if (raw->header.dwType == RIM_TYPEMOUSE)
		{
			_rawDeltaX += raw->data.mouse.lLastX;
			_rawDeltaY += raw->data.mouse.lLastY;

			if(_rawDeltaX != 0 || _rawDeltaY != 0)
				EngineInput.setControllerActive(false);
		}
		break;
	}
    case WM_MOUSEWHEEL:
    {
			EngineInput.setControllerActive(false);
      _wheelDeltaSteps += GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA;
      break;
    }
    case WM_MBUTTONDOWN:
    {
			EngineInput.setControllerActive(false);
      _buttons[Input::BT_MOUSE_MIDDLE] = true;
      break;
    }
    case WM_MBUTTONUP:
    {
			EngineInput.setControllerActive(false);
      _buttons[Input::BT_MOUSE_MIDDLE] = false;
      break;
    }
    case WM_LBUTTONDOWN:
    {
			EngineInput.setControllerActive(false);
      _buttons[Input::BT_MOUSE_LEFT] = true;
      break;
    }
    case WM_LBUTTONUP:
    {
			EngineInput.setControllerActive(false);
      _buttons[Input::BT_MOUSE_LEFT] = false;
      break;
    }
    case WM_RBUTTONDOWN:
    {
			EngineInput.setControllerActive(false);
      _buttons[Input::BT_MOUSE_RIGHT] = true;
      break;
    }
    case WM_RBUTTONUP:
    {
			EngineInput.setControllerActive(false);
      _buttons[Input::BT_MOUSE_RIGHT] = false;
      break;
    }
  default:
		break;
  }
}
