#pragma once

#include "input/device.h"

class CDeviceKeyboard : public Input::IDevice
{

	bool _active = true;
	Input::TKeyboardData* kData;

public:
	CDeviceKeyboard(const std::string & name) : Input::IDevice(name) {}
	void update(Input::TKeyboardData & data) override;
	void processWindowMsg(UINT message, WPARAM wParam, LPARAM lParam);
	void setStruct(Input::TKeyboardData* data) { kData = data; }

	void setActive(bool active) { _active = active; }
};

