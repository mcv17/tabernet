#include "windows_utils.h"
#include "mcv_platform.h"

void WindowsUtils::DisplayErrorWindow(const char * errorTitle, const char * errorMessage){
	MessageBox(nullptr, errorMessage, errorTitle, MB_OK | MB_ICONERROR);
}