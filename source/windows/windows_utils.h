#pragma once

/* Util header with utility functions for Windows window communication. Popping error messages for now. */

namespace WindowsUtils {
	void DisplayErrorWindow(const char * errorTitle, const char * errorMessage);
}