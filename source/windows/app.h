#ifndef APP_H
#define APP_H

#include "mcv_platform.h"

class CApplication {
	HWND windowsHandler; // Integer identifying a handler for the created window.
	CTimer           time_since_last_render;

	bool isFocused;
	bool prevFocus = true;
	bool isFullscreen = false;
	VEC4 windowRect;
	VEC2 windowSize;
	VEC2 windowPosition;
	VEC2 prevSystemResolution;

	// Static function called by Windows for recieving messages from the OS for Painting and Destruction between others. 
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	// We register a windows class for its instantiation afterwards by the createWindowFromClass.
	bool registerWindowClass(HINSTANCE hInstance, const std::string & windowsClass);
	// Creates a window from the previously registered class.
	bool createWindowFromClass(HINSTANCE hInstance, int nCmdShow, const std::string & windowsClass, const std::string & windowsTitle, int windowsWidth, int windowsHeight, bool fullscreen);

	void getSystemResolution(float* width, float* height);
	bool changeSystemResolution(const float width, const float height);

	void onWindowFocus();
	void onWindowUnfocus();

	CApplication(){}

	//Framerate
	int framerate = 60;
	long long frame_duration; // In ms
public:
	static CApplication & instance();

	CApplication(CApplication const &) = delete;
	void operator=(CApplication const &) = delete;

	/* Init. */
	bool init(HINSTANCE hInstance, int nCmdShow, const std::string & windowsClass, const std::string & windowsTitle, int windowsWidth, int windowsHeight, bool fullscreen);
	
	/* Close the app, frees windows allocated resources. */
	void close(HINSTANCE hInstance, const std::string & windowsClass);

	/* To stop the main loop in order to close the application */
	void sendQuitMsg();

	// Executes the app main loop. The loop recieves messages from the OS and executes the app.
	void runMainLoop();
	// This function, defined in main calls the executes the app content (our game engine in this case).
	void executeApp();

	/* Getters and setters. */
	HWND getWindowHandler() { return windowsHandler; }

	float getWindowsWidth() { return windowSize.x; }
	float getWindowsHeight() { return windowSize.y; }

	void setFocus(bool focus) { 
		prevFocus = isFocused;
		isFocused = focus; 
	}
	bool getFocus() { return isFocused; }
	bool justFocused() { return !prevFocus && isFocused; }
	bool justUnfocused() { return prevFocus && !isFocused;}

	void setWindowRect(float left, float top, float right, float bottom) {
		windowRect = VEC4(left, top, right, bottom);
	}	
	VEC4 getWindowRect() { return windowRect; }

	void setWindowSize(float width, float heigth) {
		windowSize = VEC2(width, heigth);
	}
	VEC2 getWindowSize() { return windowSize; }

	void setWindowPosition(float left, float top) {
		windowPosition = VEC2(left, top);
	}
	VEC2 getWindowPosition() { return windowPosition; }

	int getFramerateLimit() { return framerate; }

	void setFramerateLimit(int fps);

	void resetTimeSinceLastRender();
};

#define Application CApplication::instance()

#endif
