#include "mcv_platform.h"
#include "app.h"
#include "engine.h"
#include "utils/directory_watcher.h"
#include "input/input.h"
#include "input/devices/device_mouse.h"
#include "input/devices/device_keyboard.h"

//Framerate limiter, Time
#include <chrono>
#include <iostream>
#include <thread>

// Function that will allow ImGui to interact with our app.
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

CApplication & CApplication::instance() {
	static CApplication instance;
	return instance;
}

bool CApplication::init(HINSTANCE hInstance, int nCmdShow, const std::string & windowsClass, const std::string & windowsTitle, int windowsWidth, int windowsHeight, bool fullscreen) {
	PROFILE_FUNCTION("CApplication::init");
	//Load framerate limiter configuration
	json cfg = Utils::loadJson("data/config.json");
	framerate = cfg.value("framerate", 144);
	frame_duration = 1.0 / double(framerate) * 1000000000.0;
	isFullscreen = fullscreen;

	if (!registerWindowClass(hInstance, windowsClass))
		return false;
	if (!createWindowFromClass(hInstance, nCmdShow, windowsClass, windowsTitle, windowsWidth, windowsHeight, fullscreen))
		return false;
	setWindowSize((float)windowsWidth, (float)windowsHeight);
	return true;
}

void CApplication::close(HINSTANCE hInstance, const std::string & windowsClass) {
	if (windowSize != prevSystemResolution)
		changeSystemResolution(prevSystemResolution.x, prevSystemResolution.y);
	UnregisterClass(windowsClass.c_str(), hInstance);
}

void CApplication::sendQuitMsg() {
	PostMessage(windowsHandler, WM_QUIT, 0, 0);
}

void CApplication::runMainLoop() {
	// this struct holds Windows event messages
	MSG msg = { 0 };

	using clock = std::chrono::steady_clock;
	auto next_frame = clock::now();

	// wait for the next message in the queue, store the result in 'msg'
	while (WM_QUIT != msg.message)
	{

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg); // translate keystroke messages into the right format
			DispatchMessage(&msg); // send the message to the WindowProc function
		}
		else {
			executeApp();

			next_frame += std::chrono::nanoseconds(frame_duration);

			// wait for end of frame
			std::this_thread::sleep_until(next_frame);
		}		
	}
}

LRESULT CALLBACK CApplication::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	// Allows imgui to work with our app, allowing it to recieve messages from it, like the mouse input.
	if (ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam))
		return true;

	switch (message)
	{
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case CDirectoryWatcher::WM_FILE_CHANGED: {
		const char* filename = (const char*)(lParam);
		assert(filename);
		std::string strfilename(filename);
		Utils::dbg("File %s has changed!!\n", filename);
		CEngine::instance().getResourceManager().onFileChanged(strfilename);
		delete[] filename;
		break;
	}

	case WM_SETFOCUS:
		CApplication::instance().setFocus(true);
		CApplication::instance().onWindowFocus();
		break;
	case WM_KILLFOCUS:
		CApplication::instance().setFocus(false);
		CApplication::instance().onWindowUnfocus();
		break;
	case WM_SIZE: //Sent when a window is resized, use WM_SIZING if you need it RT
		if (Render.device != NULL && wParam != SIZE_MINIMIZED)
			Render.resizeBackBuffer((UINT)LOWORD(lParam), (UINT)HIWORD(lParam));
		return 0;
	case WM_MOVE: //Sent when a window is moved, use WM_MOVING if you need it rt
		RECT rect;
		GetClientRect(hWnd, &rect);

		CApplication::instance().setWindowRect		(float(rect.left), float(rect.top), float(rect.right), float(rect.bottom));
		CApplication::instance().setWindowSize		(float(rect.right - rect.left), float(rect.bottom - rect.top));
		CApplication::instance().setWindowPosition	(float(rect.left), float(rect.top));
		break;
	case WM_MOUSEMOVE:	//Normal mouse coordinates (windows)
	case WM_MOUSEWHEEL:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_INPUT:		//Raw relative mouse input (already delta, not coordinates)
	{
		CDeviceMouse * mouse = dynamic_cast<CDeviceMouse*>(EngineInput.getDevice("mouse"));
		if (mouse)
			mouse->processWindowMsg(message, wParam, lParam);

		if (message == WM_INPUT) {
			CDeviceKeyboard * keyboard = dynamic_cast<CDeviceKeyboard*>(EngineInput.getDevice("keyboard"));
			if (keyboard)
				keyboard->processWindowMsg(message, wParam, lParam);
		}

		break;
	}

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

bool CApplication::registerWindowClass(HINSTANCE hInstance, const std::string & windowsClass) {

	PROFILE_FUNCTION("App::registerWindowClass 1/2");
	// Struct that holds windows class information.
	WNDCLASSEX wc = {};
	// fill in the struct with the needed information
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance; // Windows instance.
	wc.hIcon = LoadIcon(hInstance, "MAINICON");
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = windowsClass.c_str();
	wc.hIconSm = LoadIcon( wc.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	// Once class struct is filled we register it.
	if (!RegisterClassEx(&wc))
		return false;
	return true;
}

// Here we instantiate a window.
bool CApplication::createWindowFromClass(HINSTANCE hInstance, int nCmdShow, const std::string & windowsClass, const std::string & windowsTitle, int windowsWidth, int windowsHeight, bool fullscreen) {

	PROFILE_FUNCTION("App::createWindowFromClass 2/2");
	// Windows dimensions, width and height resolution in pixels (not direct size in the actual screen).
	RECT rc = { 0, 0, windowsWidth, windowsHeight };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE); // Adapt windows rectangle to windows.

	// Now we create the window with all the information given.
	if (fullscreen) {
		getSystemResolution(&prevSystemResolution.x, &prevSystemResolution.y);
		if (prevSystemResolution.x != windowsWidth || prevSystemResolution.y != windowsHeight)
			if (!changeSystemResolution(windowsWidth, windowsHeight))
				Utils::error("Could not change system resolution to %d x %d.\nIs your monitor compatible with this resolution?", windowsWidth, windowsHeight);
		windowsHandler = CreateWindow(windowsClass.c_str(), windowsTitle.c_str(), WS_POPUP,
											CW_USEDEFAULT, CW_USEDEFAULT, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), NULL, NULL, hInstance, NULL);
	}
	else
		windowsHandler = CreateWindow(windowsClass.c_str(), windowsTitle.c_str(), WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, NULL);

	if (!windowsHandler)
		return false;

	// Display window if everything went ok.
	ShowWindow(windowsHandler, nCmdShow);
	return true;
}

void CApplication::getSystemResolution(float * width, float * height) {
	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	*width = desktop.right;
	*height = desktop.bottom;
}

bool CApplication::changeSystemResolution(const float width, const float height) {
	DEVMODEA lpDevMode;
	lpDevMode.dmSize = sizeof(DEVMODEA);
	lpDevMode.dmPelsWidth = width;
	lpDevMode.dmPelsHeight = height;
	lpDevMode.dmFields = DM_PELSHEIGHT | DM_PELSWIDTH;
	HRESULT hr = ChangeDisplaySettingsA(&lpDevMode, CDS_UPDATEREGISTRY | CDS_GLOBAL | CDS_RESET);

	return hr == S_OK;
}

void CApplication::onWindowFocus() {
	if (isFullscreen && windowSize != prevSystemResolution)
		changeSystemResolution(windowSize.x, windowSize.y);
}

void CApplication::onWindowUnfocus() {
	if (isFullscreen && windowSize != prevSystemResolution)
		changeSystemResolution(prevSystemResolution.x, prevSystemResolution.y);
}

void CApplication::setFramerateLimit(int fps) {
	framerate = fps;
	frame_duration = 1.0 / double(framerate) * 1000000000.0;
}

void CApplication::resetTimeSinceLastRender() {
	time_since_last_render.reset();
}