#pragma once

#include "mcv_platform.h"
#include "fsm/transition.h"
#include "skeleton/comp_skeleton.h"

/* We have to create multiple different transitions. This is due to the engine
not supporting retrieval of base components so we cannot know which component to
get from a basic handler. This is the easiest way. */

/* This transition returns true when the animation is about to end. It also considers a fade out. */

class CPlayerAnimationEnd : public ITransition
{
public:
	void load(const json & jData) override;
	bool check(const CFSMContext & ctx) const override;

private:
	CHandle animator;
	AnimationID animation;
	AnimationType animType;
	std::string stateToTransitionTo;

	float fadeIn;
	float fadeOut;
};

class CMeleeAnimationEnd : public ITransition
{
public:
	void load(const json & jData) override;
	bool check(const CFSMContext & ctx) const override;

private:
	CHandle animator;
	AnimationID animation;
	AnimationType animType;
	std::string stateToTransitionTo;

	float fadeIn;
	float fadeOut;
};

class CRangedAnimationEnd : public ITransition
{
public:
	void load(const json & jData) override;
	bool check(const CFSMContext & ctx) const override;

private:
	CHandle animator;
	AnimationID animation;
	AnimationType animType;
	std::string stateToTransitionTo;

	float fadeIn;
	float fadeOut;
};

class CDemonAnimationEnd : public ITransition
{
public:
	void load(const json & jData) override;
	bool check(const CFSMContext & ctx) const override;

private:
	CHandle animator;
	AnimationID animation;
	AnimationType animType;
	std::string stateToTransitionTo;

	float fadeIn;
	float fadeOut;
};
