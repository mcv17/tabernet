#include "mcv_platform.h"
#include "AnimationEnd.h"

#include "fsm/state.h"
#include "fsm/context.h"
#include "components/animations/player/comp_player_animator.h"

void CPlayerAnimationEnd::load(const json & jData, CHandle nOwner)
{
	CEntity * entity = nOwner;
	animator = entity->getComponent<TCompSkeleton>();
	TCompSkeleton * animatorComp = animator;
	
	animation = animatorComp->getAnimationID(jData.value("animation_name", ""));
	animType = jData.value("animType", "cycle") == "cycle" ? AnimationType::CYCLE : AnimationType::ACTION;
	stateToTransitionTo = jData.value("target", "");
	fadeOut = jData.value<float>("fadeOut", 0.f);
	fadeIn = jData.value<float>("fadeIn", fadeOut);
}

bool CPlayerAnimationEnd::check(const CFSMContext& ctx) const
{
	TCompSkeleton * animatorComp = animator;
	float timeToFinish = animatorComp->getAnimationDuration(animation, animType) - animatorComp->getAnimationElapsedTime(animation, animType);
	
	if (timeToFinish <= fadeOut) {
		IAnimationState * transitionState = (IAnimationState *)ctx.getState(stateToTransitionTo);
		transitionState->setFadeIn(fadeIn);
		return true;
	}
	return false;
}

void CMeleeAnimationEnd::load(const json & jData, CHandle nOwner)
{
	/* TODO: CHANGE TO ACTUAL MELEE COMPONENT. */

	CEntity * entity = nOwner;
	animator = entity->getComponent<TCompSkeleton>();
	TCompSkeleton * animatorComp = animator;
	
	animation = animatorComp->getAnimationID(jData.value("animation_name", ""));
	animType = jData.value("animType", "cycle") == "cycle" ? AnimationType::CYCLE : AnimationType::ACTION;
	stateToTransitionTo = jData.value("target", "");
	fadeOut = jData.value<float>("fadeOut", 0.f);
	fadeIn = jData.value<float>("fadeIn", fadeOut);
}

bool CMeleeAnimationEnd::check(const CFSMContext& ctx) const
{
	TCompSkeleton * animatorComp = animator;
	float timeToFinish = animatorComp->getAnimationDuration(animation, animType) - animatorComp->getAnimationElapsedTime(animation, animType);

	if (timeToFinish <= fadeOut) {
		IAnimationState * transitionState = (IAnimationState *)ctx.getState(stateToTransitionTo);
		transitionState->setFadeIn(fadeIn);
		return true;
	}
	return false;
}

void CRangedAnimationEnd::load(const json & jData, CHandle nOwner)
{
	/* TODO: CHANGE TO ACTUAL RANGED COMPONENT. */

	CEntity * entity = nOwner;
	animator = entity->getComponent<TCompSkeleton>();
	TCompSkeleton * animatorComp = animator;

	animation = animatorComp->getAnimationID(jData.value("animation_name", ""));
	animType = jData.value("animType", "cycle") == "cycle" ? AnimationType::CYCLE : AnimationType::ACTION;
	stateToTransitionTo = jData.value("target", "");
	fadeOut = jData.value<float>("fadeOut", 0.f);
	fadeIn = jData.value<float>("fadeIn", fadeOut);
}

bool CRangedAnimationEnd::check(const CFSMContext& ctx) const
{
	TCompSkeleton * animatorComp = animator;
	float timeToFinish = animatorComp->getAnimationDuration(animation, animType) - animatorComp->getAnimationElapsedTime(animation, animType);

	if (timeToFinish <= fadeOut) {
		IAnimationState * transitionState = (IAnimationState *)ctx.getState(stateToTransitionTo);
		transitionState->setFadeIn(fadeIn);
		return true;
	}
	return false;
}

void CDemonAnimationEnd::load(const json & jData, CHandle nOwner)
{
	/* TODO: CHANGE TO ACTUAL DEMON COMPONENT. */

	CEntity * entity = nOwner;
	animator = entity->getComponent<TCompPlayerAnimator>();
	TCompSkeleton * animatorComp = animator;

	animation = animatorComp->getAnimationID(jData.value("animation_name", ""));
	animType = jData.value("animType", "cycle") == "cycle" ? AnimationType::CYCLE : AnimationType::ACTION;
	stateToTransitionTo = jData.value("target", "");
	fadeOut = jData.value<float>("fadeOut", 0.f);
	fadeIn = jData.value<float>("fadeIn", fadeOut);
}

bool CDemonAnimationEnd::check(const CFSMContext& ctx) const
{
	TCompSkeleton * animatorComp = animator;
	float timeToFinish = animatorComp->getAnimationDuration(animation, animType) - animatorComp->getAnimationElapsedTime(animation, animType);

	if (timeToFinish <= fadeOut) {
		IAnimationState * transitionState = (IAnimationState *)ctx.getState(stateToTransitionTo);
		transitionState->setFadeIn(fadeIn);
		return true;
	}
	return false;
}