#pragma once

#include "mcv_platform.h"
#include "fsm/transition.h"

/* If no transition condition is defined in YED files, this transition is used. */
class CEmptyTransition : public ITransition
{
public:
  void load(const json& jData) override;
  bool check(const CFSMContext& ctx) const override;
};
