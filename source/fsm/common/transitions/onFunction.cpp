#include "mcv_platform.h"
#include "fsm/common/transitions/onFunction.h"

#include "fsm/state.h"
#include "fsm/context.h"

void COnFunctionAnimation::load(const json& jData){
	stateToTransitionTo = jData.value("target", "");
	condModif = jData.value("negate", false) ? -1 : 1;
	fadeInNextState = jData.value<float>("fadeInNextState", 0.f);
}

bool COnFunctionAnimation::check(const CFSMContext& ctx) const
{
	bool finished = (*function)() * condModif;

	// Send message to set fadeIn next state.
	if (finished) {
		/*IAnimationState * transitionState = (IAnimationState *)ctx.getState(stateToTransitionTo);
		transitionState->setFadeIn(fadeInNextState);*/
	}

	return (*function)();
}
