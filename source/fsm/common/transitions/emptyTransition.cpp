#include "mcv_platform.h"
#include "fsm/common/transitions/emptyTransition.h"
#include "fsm/context.h"

void CEmptyTransition::load(const json& jData)
{
}

bool CEmptyTransition::check(const CFSMContext& ctx) const
{
  return true;
}
