#include "mcv_platform.h"
#include "ei_movement.h"

void CEIMovement::update(CFSMContext& ctx, float dt) {
		float speed = std::get<float>(ctx.getVariable("speed")->_value);
	_blenders[0].setValueBlendParameter(speed);
	
	float fade = std::max(_fadeIn -  ctx.getTimeInState(), _weight);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), fade);
}

void CEIMovement::onEnter(CFSMContext& ctx) {
	_blenders = ctx.getStateBlenders(_name);

	float speed = std::get<float>(ctx.getVariable("speed")->_value);

	
	_blenders[0].setValueBlendParameter(1.0f);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), _weight);
}

void CEIMovement::onExit(CFSMContext& ctx) {
	_blenders.at(0).endMotions(ctx.getSkeletonComp(), _fadeOut);
}