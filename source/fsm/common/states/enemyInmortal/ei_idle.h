#pragma once

#include "fsm/state.h"
#include "fsm/context.h"

class CFSMContext;

class CEIIdle : public IState
{

public:

	void load(const json & jData) override;
  void update(CFSMContext& ctx, float dt) override;
  void onEnter(CFSMContext& ctx)  override;
  void onExit(CFSMContext& ctx)  override;
};
