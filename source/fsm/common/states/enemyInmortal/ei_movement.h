#pragma once

#include "fsm/context.h"
#include "fsm/state.h"

class CFSMContext;

class CEIMovement : public IState
{

	char _rand_movement = 0;
public:

  void update(CFSMContext& ctx, float dt) override;
  void onEnter(CFSMContext& ctx)  override;
  void onExit(CFSMContext& ctx)  override;

};
