#pragma once

#include "fsm/context.h"
#include "fsm/state.h"

class CFSMContext;

class CERInjured : public IState
{

public:

	void update(CFSMContext& ctx, float dt) override;
	void onEnter(CFSMContext& ctx)  override;
	void onExit(CFSMContext& ctx)  override;
	bool isAnimationOver(CFSMContext& ctx);
};
