#include "mcv_platform.h"
#include "engine.h"
#include "fsm/module_fsm.h"
#include "er_injured.h"

void CERInjured::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx))
		ctx.setVariable(TVariable("isInjured", false));
}

void CERInjured::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn, _fadeOut);
}

void CERInjured::onExit(CFSMContext& ctx) {
	if (isAnimationOver(ctx))
		ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CERInjured::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}
