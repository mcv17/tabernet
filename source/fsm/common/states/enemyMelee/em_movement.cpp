#include "mcv_platform.h"
#include "em_movement.h"


void CEMMovement::update(CFSMContext& ctx, float dt) {
	
	float speed = std::get<float>(ctx.getVariable("speed")->_value);
	_blenders[0].setValueBlendParameter(speed);
	
	float fade = std::max(_fadeIn -  ctx.getTimeInState(), _weight);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), fade);
	
}

void CEMMovement::onEnter(CFSMContext& ctx) {


	_blenders = ctx.getStateBlenders(_name);

	float speed = std::get<float>(ctx.getVariable("speed")->_value);

	
	_blenders[0].setValueBlendParameter(1.0f);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), _weight);

}

void CEMMovement::onExit(CFSMContext& ctx) {
	_blenders.at(0).endMotions(ctx.getSkeletonComp(), _fadeOut);
}