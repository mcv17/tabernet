#pragma once

#include "fsm/context.h"
#include "fsm/state.h"

class CFSMContext;

class CEMKnockback: public IState
{
	
	KnockbackAnim _knockbackId = KnockbackAnim::NONE;
public:

  void update(CFSMContext& ctx, float dt) override;
  void onEnter(CFSMContext& ctx)  override;
  void onExit(CFSMContext& ctx)  override;
  bool isAnimationOver(CFSMContext& ctx);
};
