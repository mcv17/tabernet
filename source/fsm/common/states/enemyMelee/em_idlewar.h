#pragma once

#include "fsm/state.h"
#include "fsm/context.h"

class CFSMContext;

class CEMIdleWar : public IState
{

public:

  void update(CFSMContext& ctx, float dt) override;
  void onEnter(CFSMContext& ctx)  override;
  void onExit(CFSMContext& ctx)  override;
};
