#pragma once

#include "fsm/state.h"
#include "fsm/context.h"
#include "components/weapons/comp_scythe_controller.h"
#include "components/weapons/comp_weapons_manager.h"

class CFSMContext;

// Class Player FullBody Scythe
class CPFBScythe : public IState
{

	CHandle _animator;

	DECL_TCOMP_ACCESS("Scythe-Weapon", TCompScytheController, ScytheController);
	DECL_TCOMP_ACCESS("Player", TCompWeaponManager, WeaponManager);

public:

	void load(const json & jData) override;
  void update(CFSMContext& ctx, float dt) override;
  void onEnter(CFSMContext& ctx)  override;
  void onExit(CFSMContext& ctx)  override;
  bool isAnimationOver(CFSMContext& ctx);
};
