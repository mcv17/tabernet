#include "mcv_platform.h"
#include "pfb_interact.h"
#include "components/animations/comp_player_animator.h"


void CPFBInteract::load(const json & jData)
{
}

void CPFBInteract::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("stop_fullbody", true);
		a->setVariable("interacting", false);
	}
}

void CPFBInteract::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();
}

void CPFBInteract::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
	
}

bool CPFBInteract::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}
