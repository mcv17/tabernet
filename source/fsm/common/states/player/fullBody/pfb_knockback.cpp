#include "mcv_platform.h"
#include "pfb_knockback.h"
#include "components/animations/comp_player_animator.h"



void CPFBKnockback::load(const json & jData)
{
}

void CPFBKnockback::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("stop_fullbody", true);
		a->setVariable("knockback", false);
	}
}

void CPFBKnockback::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();


}

void CPFBKnockback::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
	
}

bool CPFBKnockback::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}