#include "mcv_platform.h"
#include "pfb_scythe.h"
#include "components/animations/comp_player_animator.h"



void CPFBScythe::load(const json & jData)
{
}

void CPFBScythe::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("stop_fullbody", true);
		a->setVariable("scytheAttack", false);
	}
}

void CPFBScythe::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();

	TCompScytheController* scytheController = getScytheController();
	if (scytheController)
		scytheController->attachWeaponToHand();
	TCompWeaponManager* weaponManager = getWeaponManager();
	if (weaponManager)
		weaponManager->getCurrentWeapon()->attachWeaponToHolder();
}

void CPFBScythe::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
	
	TCompScytheController* scytheController = getScytheController();
	if (scytheController)
		scytheController->attachWeaponToHolder();
	TCompWeaponManager* weaponManager = getWeaponManager();
	if (weaponManager)
		weaponManager->getCurrentWeapon()->attachWeaponToHand();
}

bool CPFBScythe::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}
