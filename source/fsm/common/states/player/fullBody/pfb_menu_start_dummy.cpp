#include "mcv_platform.h"
#include "pfb_menu_start_dummy.h"
#include "components/animations/comp_player_menu_animator.h"



void CPFBMenuStartDummy::load(const json & jData)
{
}

void CPFBMenuStartDummy::update(CFSMContext& ctx, float dt) {}

void CPFBMenuStartDummy::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerMenuAnimator>();

}

void CPFBMenuStartDummy::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}
