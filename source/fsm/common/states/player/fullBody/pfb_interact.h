#pragma once

#include "fsm/state.h"
#include "fsm/context.h"

class CFSMContext;

// Class Player FullBody Interact
class CPFBInteract : public IState
{
	CHandle _animator;
public:

	void load(const json & jData) override;
  void update(CFSMContext& ctx, float dt) override;
  void onEnter(CFSMContext& ctx)  override;
  void onExit(CFSMContext& ctx)  override;
  bool isAnimationOver(CFSMContext& ctx);
};
