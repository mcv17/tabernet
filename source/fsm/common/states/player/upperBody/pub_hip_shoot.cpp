#include "mcv_platform.h"
#include "pub_hip_shoot.h"
#include "components/animations/comp_player_animator.h"


void CPUBHipShoot::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("isHipShooting", -1);
	}
}

void CPUBHipShoot::onEnter(CFSMContext& ctx) {
	_weaponShootId = WeaponAnim(std::get<int>(ctx.getVariable("isHipShooting")->_value));

	ctx.getSkeletonComp()->PlayAnimation(_animations[_weaponShootId].filename, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();
}

void CPUBHipShoot::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CPUBHipShoot::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	AnimationID animID = c_skel->getAnimationID(_animations[_weaponShootId].filename);
	return c_skel->getAnimationElapsedTime(animID) >= (c_skel->getAnimationDuration(animID) - _fadeOut) || c_skel->isAnimationOver(animID);
}