#include "mcv_platform.h"
#include "pub_reload_fail.h"
#include "components/animations/comp_player_animator.h"


void CPUBReloadFail::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("isReloadingFail", -1);
	}
}

void CPUBReloadFail::onEnter(CFSMContext& ctx) {
	_weaponReloadId = WeaponAnim(std::get<int>(ctx.getVariable("isReloadingFail")->_value));
	float startTime = std::get<float>(ctx.getVariable("currentReloadElapsedTime")->_value);
	ctx.getSkeletonComp()->PlayAnimation(_animations[_weaponReloadId].filename, _weight, _fadeIn, _fadeOut, false, false, startTime);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();
}

void CPUBReloadFail::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CPUBReloadFail::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	AnimationID animID = c_skel->getAnimationID(_animations[_weaponReloadId].filename);
	return c_skel->getAnimationElapsedTime(animID) >= (c_skel->getAnimationDuration(animID) - _fadeOut) || c_skel->isAnimationOver(animID);
}