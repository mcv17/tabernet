#pragma once

#include "fsm/context.h"
#include "fsm/state.h"

class CFSMContext;
// Class Player UpperBody Aim
class CPUBHipShoot : public IState
{
	
	CHandle _animator;
	WeaponAnim _weaponShootId = WeaponAnim::WEAPON_NONE;

public:

  void update(CFSMContext& ctx, float dt) override;
  void onEnter(CFSMContext& ctx)  override;
  void onExit(CFSMContext& ctx)  override;
  bool isAnimationOver(CFSMContext& ctx);

};
