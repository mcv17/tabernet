#include "mcv_platform.h"
#include "pub_power_aim.h"


void CPUBPowerAim::update(CFSMContext& ctx, float dt) {
}

void CPUBPowerAim::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn);
}

void CPUBPowerAim::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);

}
