#include "mcv_platform.h"
#include "pub_aim_shoot.h"
#include "components/animations/comp_player_animator.h"


void CPUBAimShoot::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("isAimShooting", -1);
	}

	float aimAngle = std::get<float>(ctx.getVariable("aimAngle")->_value);
	_blenders[0].setValueBlendParameter(aimAngle);

	float fade = std::max(_fadeIn - ctx.getTimeInState(), _weight);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), fade);
}

void CPUBAimShoot::onEnter(CFSMContext& ctx) {
	// Shooting animation
	_weaponShootId = WeaponAnim(std::get<int>(ctx.getVariable("isAimShooting")->_value));

	ctx.getSkeletonComp()->PlayAnimation(_animations[_weaponShootId].filename, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();

	// Shooting angle
	_blenders = ctx.getStateBlenders(_name);

	float aimAngle = std::get<float>(ctx.getVariable("aimAngle")->_value);

	_blenders[0].setValueBlendParameter(aimAngle);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), _weight);
}

void CPUBAimShoot::onExit(CFSMContext& ctx) {
	// Shooting
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
	// Angle pose
	_blenders[0].endMotions(ctx.getSkeletonComp(), _fadeOut);
}

bool CPUBAimShoot::isAnimationOver(CFSMContext& ctx) {
	if (_weaponShootId == 0) return false; //SMG is a cycle so it never ends
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	AnimationID animID = c_skel->getAnimationID(_animations[_weaponShootId].filename);
	return c_skel->getAnimationElapsedTime(animID) >= (c_skel->getAnimationDuration(animID) - _fadeOut) || c_skel->isAnimationOver(animID);
}