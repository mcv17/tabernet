#include "mcv_platform.h"
#include "pub_reload.h"
#include "components/animations/comp_player_animator.h"


void CPUBReload::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("isReloadingNormal", -1);
	}
}

void CPUBReload::onEnter(CFSMContext& ctx) {
	_weaponReloadId = WeaponAnim(std::get<int>(ctx.getVariable("isReloadingNormal")->_value));

	ctx.getSkeletonComp()->PlayAnimation(_animations[_weaponReloadId].filename, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();
}

void CPUBReload::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CPUBReload::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	AnimationID animID = c_skel->getAnimationID(_animations[_weaponReloadId].filename);
	return c_skel->getAnimationElapsedTime(animID) >= (c_skel->getAnimationDuration(animID) - _fadeOut) || c_skel->isAnimationOver(animID);
}