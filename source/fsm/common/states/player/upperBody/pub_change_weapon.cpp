#include "mcv_platform.h"
#include "pub_change_weapon.h"
#include "components/animations/comp_player_animator.h"


void CPUBChangeWeapon::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("isChangingWeapon", -1);
	}
}

void CPUBChangeWeapon::onEnter(CFSMContext& ctx) {
	_weaponChangeId = WeaponAnim(std::get<int>(ctx.getVariable("isChangingWeapon")->_value));

	ctx.getSkeletonComp()->PlayAnimation(_animations[_weaponChangeId].filename, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();
}

void CPUBChangeWeapon::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CPUBChangeWeapon::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	AnimationID animID = c_skel->getAnimationID(_animations[_weaponChangeId].filename);
	return c_skel->getAnimationElapsedTime(animID) >= (c_skel->getAnimationDuration(animID) - _fadeOut) || c_skel->isAnimationOver(animID);
}