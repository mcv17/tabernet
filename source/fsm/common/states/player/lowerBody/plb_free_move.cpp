#include "mcv_platform.h"
#include "plb_free_move.h"


void CPLBFreeMove::update(CFSMContext& ctx, float dt) {
	
	float fade = std::max(_fadeIn -  ctx.getTimeInState(), _weight);
	
	float speed = std::get<float>(ctx.getVariable("zSpeed")->_value);
	_blenders[0].setValueBlendParameter(speed);
	
	_blenders[0].updateWeights(ctx.getSkeletonComp(), fade);
	
}

void CPLBFreeMove::onEnter(CFSMContext& ctx) {


	_blenders = ctx.getStateBlenders(_name);

	float speed = std::get<float>(ctx.getVariable("zSpeed")->_value);

	
	_blenders[0].setValueBlendParameter(speed);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), _weight);

}

void CPLBFreeMove::onExit(CFSMContext& ctx) {
	_blenders[0].endMotions(ctx.getSkeletonComp(), _fadeOut);
}
