#pragma once

#include <variant>

enum class EVariableOperator
{
  Equal = 0,
  NotEqual,
  Greater,
  Less,
  GreaterEqual,
  LessEqual
};

using TVariableValue = std::variant<bool, int, float, std::string, CHandle, Vector2>;

//struct TVariableValue
//{
//  enum EType { Bool = 0, Int, Float};
//  
//  union
//  {
//    bool bValue;
//    int iValue;
//    float fValue;
//  };
//
//  EType type;
//
//  void set(bool value)
//  {
//    type = Bool;
//    bValue = value;
//  }
//};

struct TVariable
{
	TVariable() {}
	TVariable(std::string name, TVariableValue value) : _name(name), _value(value) {}

  static EVariableOperator parseOperator(const std::string& type);
  static TVariable parseVariable(const json& jData);

  std::string _name;
  TVariableValue _value;

  const std::string& getType() const;
  std::string toString() const;
	template <typename T>
	T as() const {
		return std::get<T>(_value);
	}
};
