#include "mcv_platform.h"
#include "fsm/module_fsm.h"
#include "fsm/state.h"
#include "fsm/transition.h"

// EnemyMelee
#include "fsm/common/states/enemyMelee/em_attack.h"
#include "fsm/common/states/enemyMelee/em_movement.h"
#include "fsm/common/states/enemyMelee/em_knockback.h"
#include "fsm/common/states/enemyMelee/em_idlewar.h"
#include "fsm/common/states/enemyMelee/em_taunt.h"
#include "fsm/common/states/enemyMelee/em_leap.h"

// EnemyRanged
#include "fsm/common/states/enemyRanged/er_attack.h"
#include "fsm/common/states/enemyRanged/er_movement.h"
#include "fsm/common/states/enemyRanged/er_idle_war.h"
#include "fsm/common/states/enemyRanged/er_injured.h"

// EnemyInmortal
#include "fsm/common/states/enemyInmortal/ei_attack.h"
#include "fsm/common/states/enemyInmortal/ei_movement.h"
#include "fsm/common/states/enemyInmortal/ei_knockback.h"
#include "fsm/common/states/enemyInmortal/ei_idlewar.h"

// Player
// fullbody
#include "fsm/common/states/player/fullBody/pfb_idle.h"
#include "fsm/common/states/player/fullBody/pfb_interact.h"
#include "fsm/common/states/player/fullBody/pfb_scythe.h"
#include "fsm/common/states/player/fullBody/pfb_knockback.h"
#include "fsm/common/states/player/fullBody/pfb_dead.h"
#include "fsm/common/states/player/fullBody/pfb_menu_idle.h"
#include "fsm/common/states/player/fullBody/pfb_menu_start.h"
#include "fsm/common/states/player/fullBody/pfb_menu_start_dummy.h"
#include "fsm/common/states/player/fullBody/pfb_none.h"
// lowerbody
#include "fsm/common/states/player/lowerBody/plb_free_move.h"
#include "fsm/common/states/player/lowerBody/plb_strafing.h"
// upperbody
#include "fsm/common/states/player/upperBody/pub_aim.h"
#include "fsm/common/states/player/upperBody/pub_power_aim.h"
#include "fsm/common/states/player/upperBody/pub_power_activated.h"
#include "fsm/common/states/player/upperBody/pub_weapon_down.h"
#include "fsm/common/states/player/upperBody/pub_weapon_up.h"
#include "fsm/common/states/player/upperBody/pub_reload.h"
#include "fsm/common/states/player/upperBody/pub_reload_fail.h"
#include "fsm/common/states/player/upperBody/pub_reload_fast.h"
#include "fsm/common/states/player/upperBody/pub_change_weapon.h"
#include "fsm/common/states/player/upperBody/pub_hip_shoot.h"
#include "fsm/common/states/player/upperBody/pub_aim_shoot.h"


#include "fsm/common/transitions/emptyTransition.h"
#include "fsm/common/transitions/checkVariable.h"
#include "fsm/common/transitions/checkTime.h"
#include "fsm/common/transitions/onFunction.h"
#include "fsm/common/transitions/AnimationEnd.h"

namespace FSM {
	// It doesn't execute for now but once we release modules we won't have a memory leak
	CModuleFSM::~CModuleFSM() {
		for (auto factory : _stateFactories)
			delete factory.second;

		for (auto factory : _transitionFactories)
			delete factory.second;
	}

	CModuleFSM::CModuleFSM(const std::string& name)
		: IModule(name) {}

	bool CModuleFSM::start() {
		
		/* Register animation "types" which is the name of the state */
		/* EnemyMelee */
		/*_stateFactories["EMIdle"] = new StateFactory<CEMIdle>();
		_stateFactories["EMRun"] = new StateFactory<CEMRun>();*/
		_stateFactories["EMMovement"] = new StateFactory<CEMMovement>();
		_stateFactories["EMAttack"] = new StateFactory<CEMAttack>();
		_stateFactories["EMLeap"] = new StateFactory<CEMLeap>();
		_stateFactories["EMTaunt"] = new StateFactory<CEMTaunt>();
		_stateFactories["EMIdleWar"] = new StateFactory<CEMIdleWar>();
		_stateFactories["EMKnockback"] = new StateFactory<CEMKnockback>();

		/* Enemy Ranged*/
		_stateFactories["ERMovement"] = new StateFactory<CERMovement>();
		_stateFactories["ERIdleWar"] = new StateFactory<CERIdleWar>();
		_stateFactories["ERAttack"] = new StateFactory<CERAttack>();
		_stateFactories["ERInjured"] = new StateFactory<CERInjured>();

		/* Enemy Immortal */
		_stateFactories["EIMovement"] = new StateFactory<CEIMovement>();
		_stateFactories["EIAttack"] = new StateFactory<CEIAttack>();
		_stateFactories["EIIdleWar"] = new StateFactory<CEIIdleWar>();
		_stateFactories["EIKnockback"] = new StateFactory<CEIKnockback>();
		
		/* Player */
		// fullbody
		_stateFactories["PFBIdle"] = new StateFactory<CPFBIdle>();
		_stateFactories["PFBInteract"] = new StateFactory<CPFBInteract>();
		_stateFactories["PFBScythe"] = new StateFactory<CPFBScythe>();
		_stateFactories["PFBKnockback"] = new StateFactory<CPFBKnockback>();
		_stateFactories["PFBDead"] = new StateFactory<CPFBDead>();
		_stateFactories["PFBMenuIdle"] = new StateFactory<CPFBMenuIdle>();
		_stateFactories["PFBMenuStart"] = new StateFactory<CPFBMenuStart>();
		_stateFactories["PFBMenuStartDummy"] = new StateFactory<CPFBMenuStartDummy>();
		_stateFactories["PFBNone"] = new StateFactory<CPFBNone>();

		// lowerbody
		_stateFactories["PLBFreeMove"] = new StateFactory<CPLBFreeMove>();
		_stateFactories["PLBStrafing"] = new StateFactory<CPLBStrafing>();

		// upperbody
		_stateFactories["PUBWeaponDown"] = new StateFactory<CPUBWeaponDown>();
		_stateFactories["PUBWeaponUp"] = new StateFactory<CPUBWeaponUp>();
		_stateFactories["PUBAiming"] = new StateFactory<CPUBAim>();
		_stateFactories["PUBPowerAiming"] = new StateFactory<CPUBPowerAim>();
		_stateFactories["PUBPowerActivated"] = new StateFactory<CPUBPowerActivated>();
		_stateFactories["PUBReload"] = new StateFactory<CPUBReload>();
		_stateFactories["PUBReloadFail"] = new StateFactory<CPUBReloadFail>();
		_stateFactories["PUBReloadFast"] = new StateFactory<CPUBReloadFast>();
		_stateFactories["PUBChangeWeapon"] = new StateFactory<CPUBChangeWeapon>();
		_stateFactories["PUBHipShoot"] = new StateFactory<CPUBHipShoot>();
		_stateFactories["PUBAimShoot"] = new StateFactory<CPUBAimShoot>();


		/* Transitions */
		_transitionFactories["checkVariable"] = new TransitionFactory<CCheckVariable>();
		_transitionFactories["checkTime"] = new TransitionFactory<CCheckTime>();
		_transitionFactories["empty"] = new TransitionFactory<CEmptyTransition>();
		_transitionFactories["checkFunction"] = new TransitionFactory<COnFunctionAnimation>();

		return true;
	}

	IState* CModuleFSM::createState(const std::string& type) {
		const auto& factory = _stateFactories.find(type);
		if (factory == _stateFactories.end()) {
			return nullptr;
		}
		IState* st = factory->second->create();
		st->_type = factory->first;

		return st;
	}

	ITransition* CModuleFSM::createTransition(const std::string& type) {
		const auto& factory = _transitionFactories.find(type);
		if (factory == _transitionFactories.end()) {
			return nullptr;
		}
		ITransition* tr = factory->second->create();
		tr->_type = factory->first;

		return tr;
	}

	void CModuleFSM::renderInMenu() {
		/*const auto printButton = [](const TButton& button, const char* label, bool centerValue = false)
		{
			float value = centerValue ? 0.5f + button.value * 0.5f : button.value;
			ImGui::ProgressBar(value, ImVec2(-1, 0), label);
		};

		if (ImGui::TreeNode("Input"))
		{
			if (ImGui::TreeNode("keyboard"))
			{
				for(int i = 0; i< Input::BT_KEYBOARD_COUNT; ++i)
				{
					printButton(_keyboard._buttons[i], getButtonName(INTERFACE_KEYBOARD, i).c_str());
				}
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("mouse"))
			{
				ImGui::Text("Current position: %.0f %.0f", _mouse._currPosition.x, _mouse._currPosition.y);
				ImGui::Text("Previous position: %.0f %.0f", _mouse._prevPosition.x, _mouse._prevPosition.y);
				for (int i = 0; i< Input::BT_MOUSE_COUNT; ++i)
				{
					printButton(_mouse._buttons[i], getButtonName(INTERFACE_MOUSE, i).c_str());
				}
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("pad"))
			{
				ImGui::Text("Connected: %s", _gamepad._connected ? "YES" : "no");
				for (int i = 0; i< Input::BT_GAMEPAD_COUNT; ++i)
				{
					bool isAnalog = i == Input::BT_LANALOG_X || i == Input::BT_LANALOG_Y || i == Input::BT_RANALOG_X || i == Input::BT_RANALOG_Y;
					printButton(_gamepad._buttons[i], getButtonName(INTERFACE_GAMEPAD, i).c_str(), isAnalog);
				}
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("Mapping"))
			{
				for (auto& bt : _mapping._buttons)
				{
					printButton(bt.second.result, bt.first.c_str());
				}
				ImGui::TreePop();
			}
			ImGui::TreePop();
		}*/
	}
}
