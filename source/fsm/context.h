#pragma once

#include "fsm/variable.h"
#include "skeleton/comp_skeleton.h"

class CFSM;
class IState;
class OneDimensionalBlender;

/* A FSMContext runs a fsm. Multiple contexts can run the same fsm but be careful,
states are shared between context and so, multiple contexts could modify the same state.
To avoid this, create two different CFSMs. */

class CFSMContext
{
public:
	// Initiates the fsm machine.
	void init(CFSM * fsm, CHandle owner);

	// Updates the context for the given delta time.
	void update(float dt);

	// Displays the state of the fsm and other information in IMGUI.
	void debugInMenu();

	/* Getters. */
	float getTimeInState() const { return _timeInState; }
	TVariable * getVariable(const std::string & name);
	const TVariable* getVariable(const std::string & name) const;
	CHandle getOwner() const { return _owner; }
	IState * getState(const std::string & name) const;
	TCompSkeleton* getSkeletonComp() const;
	IState * getCurrentState() const;
	std::vector<OneDimensionalBlender> getStateBlenders(std::string state);

	/* Setters. */
	void setOwner(const CHandle nOwner) { _owner = nOwner; }
	void setVariable(const TVariable & var);

private:
	void start();
	void stop();
	void changeToState(IState * newState);

	// Pointer to the fsm we are running.
	CFSM * _fsm = nullptr;
	// The current state we are at.
	IState * _currentState = nullptr;
	// Variables of the fsm.
	std::map<std::string, TVariable> _variables;
	// Time the fsm has been in the current state.
	float _timeInState = 0.f;
	// CHandle for the owner of the state machine.
	CHandle _owner;

	// Skeleton component
	CHandle _skeleton;
	// Using IKs ?
	bool _usingIks = false;
	// Loaded blender info
	std::map<std::string, std::vector<OneDimensionalBlender>> _blenderInfo;
	
};
