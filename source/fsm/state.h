#pragma once

#include "mcv_platform.h"
#include "components/animations/animation_blenders.h"

class CFSMContext;
class ITransition;

enum KnockbackAnim {
	HEAD,
	TORSO,
	SHOULDER_LEFT,
	SHOULDER_RIGHT,
	NONE = -1
};

enum WeaponAnim {
	SMG,
	SHOTGUN,
	RIFLE,
	WEAPON_NONE = -1
};

struct TAnimation {
	TAnimation(std::string filename, bool isCycle) 
		: filename(filename), isCycle(isCycle){}

	std::string filename;
	bool isCycle;
};

class IState {
public:
	std::string _name;
	std::string_view _type;
	int animationID;
	float _fadeIn;
	float _fadeOut;
	float _weight;
	std::vector<const ITransition*> _transitions;
	std::vector<TAnimation> _animations;
	std::vector<OneDimensionalBlender> _blenders;


	virtual void load(const json & jData) {}

	virtual void update(CFSMContext & ctx, float dt) {};
	virtual void onEnter(CFSMContext & ctx)  {}
	virtual void onExit(CFSMContext & ctx) {}
	
};