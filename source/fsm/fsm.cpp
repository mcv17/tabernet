#include "mcv_platform.h"
#include "fsm/fsm.h"
#include "fsm/state.h"
#include "fsm/transition.h"
#include "fsm/variable.h"
#include "engine.h"
#include "fsm/module_fsm.h"

class CFSMResourceType : public CResourceType {
public:
  const char* getExtension(int idx = 0) const override { return "fsm"; }
  const char* getName() const override { return "FSM's"; }
  IResource * create(const std::string & name) const override {
	Utils::dbg("Creating fsm resource %s\n", name.c_str());
    CFSM* newFsm = new CFSM();
    bool is_ok = newFsm->load(name);
    assert(is_ok);
    newFsm->setNameAndType(name, this);
    return newFsm;
  }
};

template<>
const CResourceType* getResourceTypeFor<CFSM>() {
  static CFSMResourceType resource_type;
  return &resource_type;
}

CFSM::~CFSM() {
	for (auto state : _states)
		if(state)	delete state;
	for (auto transition : _transitions)
		if(transition) delete transition;
}

bool CFSM::load(const std::string & file)
{
  FSM::CModuleFSM & factories = Engine.getModuleFSM();

  json jData = Utils::loadJson(file);

  const json & jStates = jData["states"];
  const json & jTransitions = jData["transitions"];
  const json & jVariables = jData["variables"];

  for (const auto & jState : jStates)
  {
    const std::string stName = jState["name"];
    const std::string stType = jState["type"];
	const float stFadeIn = jState["fadeIn"];
	const float stFadeOut = jState["fadeOut"];
	const float stWeight = jState["weight"];
	const json & stAnimations = jState["animations"];

    IState * newState = factories.createState(stType);
	newState->_name = stName;
	newState->_fadeIn = stFadeIn;
	newState->_fadeOut = stFadeOut;
	newState->_weight= stWeight;
	
	// Load animations
	for (auto anim : stAnimations) {
		std::string filename = anim["filename"];
		std::string name;

		// Remove folder paths if it has
		int pos = filename.find('/');
		if (pos != -1)
			name = filename.substr(pos + 1, filename.size());
		else
			name = filename;

		bool isCycle = anim["isCycle"];
		// If the json has a name parameter, will use that as name, if not, it will use the filename
		newState->_animations.push_back(TAnimation(name.empty() ? filename : name, isCycle));
	}

	// Load blenders


    newState->load(jState);

    _states.push_back(newState);
  }

  for (const auto& jTransition : jTransitions)
  {
	  const std::string trSource = jTransition["source"];
	  const std::string trTarget = jTransition["target"];
	  const std::string trType = jTransition["type"];

	  IState* stSource = getState(trSource);

	  ITransition* newTransition = factories.createTransition(trType);

	  newTransition->_source = stSource;
	  newTransition->_target = getState(trTarget);
	  newTransition->load(jTransition);

	  _transitions.push_back(newTransition);

	  if (stSource)
	  {
		  stSource->_transitions.push_back(newTransition);
	  }
  }

  for (const auto& jVariable : jVariables)
  {
    TVariable newVariable = TVariable::parseVariable(jVariable);
    _variables.push_back(std::move(newVariable));
  }

  const std::string startingState = jData["start"];
  _startState = getState(startingState);

  return true;
}

void CFSM::registerTransitionFunction(const std::string & functionName, transitionFunc nFunc) {
	assert(_functionTransitions.count(functionName) > 0 && "Function name was not found.\n");
	for(auto & transition : _functionTransitions[functionName])
		transition->setFunction(nFunc);
}

IState* CFSM::getState(const std::string& name) const
{
  auto it = std::find_if(_states.begin(), _states.end(), [&name](const IState* state)
  {
    return state->_name == name;
  });
  return it != _states.end() ? *it : nullptr;
}

void CFSM::renderInMenu()
{
  ImGui::Text("Start state: %s", _startState ? _startState->_name.c_str() : "...");
  if (ImGui::TreeNode("States"))
  {
    for (auto& st : _states)
    {
      if (ImGui::TreeNode("%s [%s]", st->_name.c_str(), st->_type.data()))
      {
        for (auto& tr : st->_transitions)
        {
          ImGui::Text("-> %s [%s]", tr->_target ? tr->_target->_name.c_str() : "...", tr->_type.data());
        }

        ImGui::TreePop();
      }
    }

    ImGui::TreePop();
  }

  if (ImGui::TreeNode("Variables"))
  {
    for (auto& var : _variables)
    {
      ImGui::Text("%s [%s] : %s", var._name.c_str(), var.getType().c_str(), var.toString().c_str());
    }

    ImGui::TreePop();
  }
}
