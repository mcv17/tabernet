#pragma once

#include "modules/module_manager.h"
#include "modules/module_entities.h"
#include "modules/module_physics.h"
#include "modules/module_multithreading.h"
#include "modules/scripting/module_scripting.h"
#include "modules/module_pool.h"
#include "modules/module_audio.h"
#include "modules/module_instancing.h"
#include "modules/module_boot.h"
#include "ui/module_ui.h"
#include "resources/resource_manager.h"
#include "input/input.h"
#include "logic/logic_manager.h"
#include "imgui/imgui_manager.h"

#include "utils/directory_watcher.h"
#include "../../module_cinematic_sequencer.h"

/* This class will represent our engine, contained inside the CApplication class.
It will hold all the necessary data for the engine execution.
Will be implemented as a singleton. */

#define DECL_MODULE(__CLASS__, __MEMBER__, __GETTER__) \
  public: __CLASS__& __GETTER__() const { return *__MEMBER__; } \
  private: __CLASS__* __MEMBER__ = nullptr;

class CModuleGameData;
namespace Input{ class CModuleInput; }
class CModuleRender;
class CModuleGPUCullingRender;
class CModuleEntities;
class CEngineImGUIManager;
class CModuleCameraMixer;
namespace FSM {
	class CModuleFSM;
}
namespace UI {
  class CModuleUI;
}
namespace particles {
  class CModuleParticles;
}
class CModuleEditor;

class CEngine
{
	CModuleManager _module_manager; // The module manager of our instance, controls all the modules that are present in the engine.
	CResourcesManager _resource_manager; // The resource manager of our engine. These are textures, meshes, jsons, ...

	CDirectoryWatcher dir_watcher_data; // Used for watching for changes in directories and making hot reloads.

	// Constructor and destructor are set to private for this singleton class.
	CEngine();
	~CEngine();

	// Updates the engine, updating all the components.
	void update(float dt);

	// Called for render debugging.
	void renderDebug();
	
	// Here all posible resource types suported by the engine are registered.
	void registerResourceTypes();

public:
	// Instance of the singleton class of the engine.
	static CEngine & instance();
	CEngine(CEngine const&) = delete;
	void operator=(CEngine const&) = delete;
	
	// Initiates the engine starting all the different components.
	void init();
	// Stops modules from executing.
	void stop();
	// Call each frame, will execute the engine for a frame.
	void executeFrame();
	// Closes the engine and cleans all allocated resources.
	void close();

	// Returns a pointer to the module manager for module manipulation.
	CModuleManager & getModuleManager();
	CResourcesManager & getResourceManager();
	DECL_MODULE(CEngineImGUIManager, _imgui_manager, getImGuiManager);
	DECL_MODULE(CModuleRender, _module_render, getModuleRender);
	DECL_MODULE(CModuleEntities, _module_entities, getModuleEntities);
	DECL_MODULE(CModulePhysics, _module_physics, getPhysics);
	DECL_MODULE(Input::CModuleInput, _module_input, getModuleInput);
	DECL_MODULE(CModuleCameraMixer, _cameraMixer, getCameraMixer);
	DECL_MODULE(MultithreadingModule, _multithreading, getMultithreading);
	DECL_MODULE(CModuleScripting, _scripting, getModuleScripting);
	DECL_MODULE(FSM::CModuleFSM, _module_fsm, getModuleFSM);
	DECL_MODULE(CModulePool, _module_pool, getModulePool);
	DECL_MODULE(UI::CModuleUI, _ui, getUI);
	DECL_MODULE(CModuleAudio , _audio, getAudio);
	DECL_MODULE(particles::CModuleParticles, _particles, getParticles);
	DECL_MODULE(CModuleEditor, _editor, getEditor);
	DECL_MODULE(CModuleInstancing, _instancing, getInstancing);
	DECL_MODULE(CModuleBoot, _module_boot, getBoot);
	DECL_MODULE(CCinematicSequencer, _module_cinematic_sequencer, getCinematicSequencer);
	DECL_MODULE(CModuleGameData, _module_gameData, getGameData);
};

#define Engine CEngine::instance()
#define EngineModules CEngine::instance().getModuleManager()
#define EngineImGui CEngine::instance().getImGuiManager()
#define EngineRender CEngine::instance().getModuleRender()
#define EngineEntities CEngine::instance().getModuleEntities()
#define EnginePhysics CEngine::instance().getPhysics()
#define EngineInput CEngine::instance().getModuleInput()
#define EngineResources CEngine::instance().getResourceManager()
#define EngineCameraMixer CEngine::instance().getCameraMixer()
#define EngineScripting CEngine::instance().getModuleScripting()
#define EnginePool CEngine::instance().getModulePool()
#define EngineLogicManager LogicManager::Instance()
#define EngineUI CEngine::instance().getUI()
#define EngineAudio CEngine::instance().getAudio()
#define EngineParticles CEngine::instance().getParticles()
#define EngineInstancing CEngine::instance().getInstancing()
#define EngineBoot CEngine::instance().getBoot()
#define EngineCinematicSequencer CEngine::instance().getCinematicSequencer()