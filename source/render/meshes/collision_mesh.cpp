#include "mcv_platform.h"
#include "collision_mesh.h"
#include "utils/data_provider.h"

class CCollisionMeshResourceType : public CResourceType {
public:
  const char* getExtension(int idx) const override { return "col_mesh"; }
  const char* getName() const override {
    return "Collision Meshes";
  }
  IResource* create(const std::string& name) const override {
    CFileDataProvider fdp(name.c_str());
    size_t file_size = fdp.fileSize();

    CCollisionMesh* new_mesh = new CCollisionMesh();
    bool is_ok = new_mesh->load(fdp);

    assert(is_ok);
    new_mesh->setNameAndType(name, this);

    // Check if there is more data after our mesh
    size_t curr_offset = fdp.current();
    if (curr_offset < file_size) {
      size_t extra_bytes = file_size - curr_offset;
      new_mesh->cooked_data.resize(extra_bytes);
      fdp.read(new_mesh->cooked_data.data(), extra_bytes);
    }

    return new_mesh;
  }
};

template<>
const CResourceType* getResourceTypeFor<CCollisionMesh>() {
  static CCollisionMeshResourceType resource_type;
  return &resource_type;
}
