#pragma once

#include <string>
#include <map>
#include <utility>      // std::pair

/* Struct holding a vertex declaration. */
struct CVertexDeclaration {
  std::string name;
  uint32_t    bytes_per_vertex = 0;
  std::vector<D3D11_INPUT_ELEMENT_DESC> layout;
  UINT        numElements = 0;
  bool        instancing = false;
  bool isCompatibleWith(const CVertexDeclaration* other) const;
};

/* This class manages different possible vertex declarations. */
class CVertexDeclarationManager {
private:
	// All CVertexDeclarations. Used for retrieval purposes.
	std::map<std::string, CVertexDeclaration> all_vertex_declarations;
	bool initiated = false;

	CVertexDeclarationManager(){}
	bool isVertexDeclarationAlreadyAdded(const std::string & vdName);

public:
	// We define a singleton class.
	static CVertexDeclarationManager & instance();
	CVertexDeclarationManager(CVertexDeclarationManager const&) = delete;
	void operator=(CVertexDeclarationManager const&) = delete; 

	// Initializes vertex declarations
	void initVertexDeclarations();

	// Adds a new vertex declaration through code instead of file.
	void addNewVertexDeclaration(const std::string & vdName, uint32_t bytes_per_vertex, const D3D11_INPUT_ELEMENT_DESC new_layout[], UINT layoutSize, bool instancing = false);

	// Adds a new new vertex declaration for instances
	const CVertexDeclaration* createInstancedVertexDecl(const std::string& name, size_t idx);

	// Returns vertex declaration.
	const CVertexDeclaration * getVertexDeclByName(const std::string & name);
};

