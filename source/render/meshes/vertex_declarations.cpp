#include "mcv_platform.h"
#include "vertex_declarations.h"

// Returns the instance of the class.
CVertexDeclarationManager & CVertexDeclarationManager::instance()
{
	static CVertexDeclarationManager instance;
	return instance;
}

// Initializes vertex declarations
void CVertexDeclarationManager::initVertexDeclarations() {
	if (initiated)
		return;

	D3D11_INPUT_ELEMENT_DESC layout_Pos[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("Pos", 12, layout_Pos, ARRAYSIZE(layout_Pos));

	D3D11_INPUT_ELEMENT_DESC layout_PosUv[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	addNewVertexDeclaration("PosUv", 20, layout_PosUv, ARRAYSIZE(layout_PosUv));

	D3D11_INPUT_ELEMENT_DESC layout_PosColor[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("PosColor", 28, layout_PosColor, ARRAYSIZE(layout_PosColor));

	D3D11_INPUT_ELEMENT_DESC layout_PosUvColor[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("PosUvColor", 36, layout_PosUvColor, ARRAYSIZE(layout_PosUvColor));

	D3D11_INPUT_ELEMENT_DESC layout_PosNUv[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("PosNUv", 32, layout_PosNUv, ARRAYSIZE(layout_PosNUv));

	D3D11_INPUT_ELEMENT_DESC layout_PosNUvT[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("PosNUvT", 48, layout_PosNUvT, ARRAYSIZE(layout_PosNUvT));

	D3D11_INPUT_ELEMENT_DESC layout_PosNUvColor[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("PosNUvColor", 44, layout_PosNUvColor, ARRAYSIZE(layout_PosNUvColor));

	D3D11_INPUT_ELEMENT_DESC layout_PosNUvTanSkin[] = 
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES",    0, DXGI_FORMAT_R8G8B8A8_UINT,      0, 48, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS",  0, DXGI_FORMAT_R8G8B8A8_UNORM,     0, 52, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("PosNUvTanSkin", 56, layout_PosNUvTanSkin, ARRAYSIZE(layout_PosNUvTanSkin));

  D3D11_INPUT_ELEMENT_DESC layout_Instance[] = {
		{ "TEXCOORD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world0
		{ "TEXCOORD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world1
		{ "TEXCOORD", 4, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world2
		{ "TEXCOORD", 5, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 48, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world3
  };
  addNewVertexDeclaration("Instance", 64, layout_Instance, ARRAYSIZE(layout_Instance));

	D3D11_INPUT_ELEMENT_DESC layout_InstanceColor[] = {
		{ "TEXCOORD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world0
		{ "TEXCOORD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world1
		{ "TEXCOORD", 4, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world2
		{ "TEXCOORD", 5, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 48, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world3
		{ "TEXCOORD", 6, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 64, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // color
	};
	addNewVertexDeclaration("InstanceColor", 80, layout_InstanceColor, ARRAYSIZE(layout_InstanceColor));

	D3D11_INPUT_ELEMENT_DESC layout_InstanceColorFadeTime[] = {
	{ "TEXCOORD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world0
	{ "TEXCOORD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world1
	{ "TEXCOORD", 4, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world2
	{ "TEXCOORD", 5, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 48, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // world3
	{ "TEXCOORD", 6, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 64, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // color
	{ "TEXCOORD", 7, DXGI_FORMAT_R32_UINT, 0, 80, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // timetoLife
	{ "TEXCOORD", 8, DXGI_FORMAT_R32_UINT, 0, 84, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // timeToStartBlendingOut

	};
	addNewVertexDeclaration("InstanceColorFadeTime", 88, layout_InstanceColorFadeTime, ARRAYSIZE(layout_InstanceColorFadeTime));

	D3D11_INPUT_ELEMENT_DESC layout_ParticleRenderData[] = {
		{ "TEXCOORD"			, 2, DXGI_FORMAT_R32G32B32_FLOAT,    0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // pos
		{ "TEXCOORD"			, 3, DXGI_FORMAT_R32_FLOAT,          0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // time
		{ "TEXCOORD"			, 4, DXGI_FORMAT_R32_UINT,					 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 },    // particle identifier
		{ "TEXCOORD"			, 5, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("ParticleRenderData", 36, layout_ParticleRenderData, ARRAYSIZE(layout_ParticleRenderData));

	D3D11_INPUT_ELEMENT_DESC layout_MeshTrailRenderData[] = {
		{ "POSITION"			, 0, DXGI_FORMAT_R32G32B32_FLOAT,			0, 0,		D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD"			, 0, DXGI_FORMAT_R32G32_FLOAT,				0, 12,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR"					, 0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, 20,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "THICKNESS"			, 0, DXGI_FORMAT_R32_FLOAT,						0, 36,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "MOTION"				, 0, DXGI_FORMAT_R32G32B32_FLOAT,			0, 40,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	addNewVertexDeclaration("PosUvColorTrail", 52, layout_MeshTrailRenderData, ARRAYSIZE(layout_MeshTrailRenderData));

	initiated = true;
}

const CVertexDeclaration* CVertexDeclarationManager::createInstancedVertexDecl(const std::string& name, size_t idx){
  // Check if it's an instance vertex declaration
  std::string first = name.substr(0, idx);
  std::string second = name.substr(idx + 3);

  auto a = getVertexDeclByName(first);      // instanced_decl
  auto b = getVertexDeclByName(second);     // instances_decl
  assert(a && b);
  auto num_elems = a->numElements + b->numElements;
  // Concatenate layouts from a and b as:
  //  -- LAYOUT A AS NORMAL    --
  //  -- LAYOUT B AS INSTANCED --
  std::vector<D3D11_INPUT_ELEMENT_DESC> layout;
  for(auto input : a->layout)
    layout.push_back(input);

  // Change the layout B to be 'instanced data' with rate = 1
  std::vector<D3D11_INPUT_ELEMENT_DESC> layout_b = b->layout;
  for (UINT i = 0; i < b->numElements; i++) {
	  layout_b[i].InputSlotClass = D3D11_INPUT_CLASSIFICATION::D3D11_INPUT_PER_INSTANCE_DATA;
	  layout_b[i].InstanceDataStepRate = 1;
	  layout_b[i].InputSlot = 1;    // Comes from stream 1
	  layout.push_back(layout_b[i]);
  }

  uint32_t nbytes = a->bytes_per_vertex + b->bytes_per_vertex;
  auto decl = new CVertexDeclaration{
    name, nbytes, layout, num_elems
  };
  decl->instancing = true;

  // Register it
  // layour.data returs de memory pointer for layout
  addNewVertexDeclaration(name, nbytes, layout.data(), num_elems, decl->instancing);

  return decl;
}

void CVertexDeclarationManager::addNewVertexDeclaration(const std::string & vdName, uint32_t bytes_per_vertex, const D3D11_INPUT_ELEMENT_DESC new_layout[], UINT layoutSize, bool instancing) {
	if (isVertexDeclarationAlreadyAdded(vdName))
		return;

	CVertexDeclaration newVertexDeclaration = { vdName, bytes_per_vertex, std::vector<D3D11_INPUT_ELEMENT_DESC>(new_layout, new_layout + layoutSize), layoutSize, instancing};
	all_vertex_declarations.insert(std::pair<std::string, CVertexDeclaration>(newVertexDeclaration.name, newVertexDeclaration));
}

const CVertexDeclaration * CVertexDeclarationManager::getVertexDeclByName(const std::string & name) {
	auto it = all_vertex_declarations.find(name);
	if (it == all_vertex_declarations.end()){
		// Check if it's an instanced mesh vertex decl
		auto idx = name.find("_x_");
		if (idx != std::string::npos)
			return createInstancedVertexDecl(name, idx);
		else
			return nullptr;
	}
	return &it->second;
}

bool CVertexDeclarationManager::isVertexDeclarationAlreadyAdded(const std::string & vdName) {
	auto it = all_vertex_declarations.find(vdName);
	if (it == all_vertex_declarations.end())
		return false;
	return true;
}

// If this == PosNUv and other is Pos, yes PosNUv is compatible with Pos
bool CVertexDeclaration::isCompatibleWith(const CVertexDeclaration* other) const {
  return name.substr(0, other->name.length()) == other->name;
}

// D3D11_INPUT_ELEMENT_DESC element description.
// * Semantic name: Maps elements in the vertex structure to elements in the vertex shader.
// * Semantic index: Used as index of reference in the case multiple elements with same name exist.
// By default if semantic name is position and semantic index is set to 0, the name will be position0.
// * Format: Format of component in our vertex structure. In the case of a position we set the DXGI_FORMAT_R32G32B32_FLOAT.
// * Input slot: up to 16 (0-15) input slots to put vertex data through. Values can be input through the same slot.
// * AlignedByteOffset: Byte offset of the element being described, depends on how many values are in each input slot.
// For example, if pos (vec3) and color (vec4) are in same slot, the value for vertex would be 0 and 12 (bytes) for the color.
// * InputSlotClass: Used for instancing, we have still not seen it. We will leave it with D3D11_INPUT_PER_VERTEX_DATA for now.
// * InstanceDataStepRate: Also used for instancing, we will set it to 0 for now.

/*
Macro de Juan que est� guay pero ya no sirve:
#define MAKE_VERTEX_DECLARATION(name, nbytes) \
  { \
	#name, nbytes, layout_##name, ARRAYSIZE(layout_##name) \
  }
*/