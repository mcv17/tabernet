#pragma once

#include "resources/resource.h"
#include "mesh_io.h"

class CCollisionMesh : public TRawMesh, public IResource {
public:
  std::vector< uint8_t > cooked_data;
};
