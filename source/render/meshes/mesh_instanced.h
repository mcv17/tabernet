#ifndef INC_RENDER_MESHES_INSTANCED_MESH_H_
#define INC_RENDER_MESHES_INSTANCED_MESH_H_

#include "mesh.h"

// Holds all meshes that are being instanced.
// Independent of the type of instance of the mesh, which has
// been moved to a .cpp file.
class CMeshInstanced : public CMesh {
protected:
	const CMesh * instanced_mesh = nullptr;    // Pointer to the instanced mesh.
	size_t       num_instances_allocated_in_gpu = 0; // Number of instances in the GPU for the given mesh.

public:
	// Reads a Json file with data about the mesh to instance, type of instance and the number of instances to reserve.
	void configure(const json & j);

	void renderGroup(uint16_t group_idx, uint16_t instanced_group_idx) const override;
	void reserveGPUInstances(size_t new_max_instances);
	void setInstancesData(const void* data, size_t total_instances, size_t bytes_per_instance );
	void setGroupSize(uint32_t num_subgroup, uint32_t new_size_for_subgroup);

	size_t getNumInstancesAllocated() { return num_instances_allocated_in_gpu; }
};

#endif
