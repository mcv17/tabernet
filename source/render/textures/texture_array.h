#pragma once

#include "render/textures/texture.h"

/* A class representing a texture array. */
class CTextureArray : public CTexture {
protected:
	// Number of textures in the array.
	int size_texture_array = 0;

public:
	~CTextureArray() { destroy(); }
	
	// Creates an array of dds textures from filenames
	bool create(const std::vector<std::string> filenames, bool load_textures_as_individual_resources = false);
	// Creates an array of textures with the given size.
	bool create(int arraySize, int new_xres, int new_yres, DXGI_FORMAT format, eCreateOptions options = CREATE_STATIC);

	// Creates a DDS texture from a file whose filename is sent as argument.
	bool create(const std::string & filename) = delete;
	// Creates a texture with the given resolution, color format and type of texture.
	bool create(int new_xres, int new_yres, DXGI_FORMAT new_color_format, eCreateOptions create_options = CREATE_STATIC) = delete;

	void destroy();

	void setDXParams(int size_array, ID3D11Texture2D* new_texture, ID3D11ShaderResourceView* new_srv);

private:
	bool SetTextureArrayResource(const D3D11_TEXTURE2D_DESC & textureDescription, int numberOfTextures);
	bool CopyTexturesToTextureArray(const std::vector<ID3D11Resource*> & texturesToAdd, const D3D11_TEXTURE2D_DESC & textureDescription, int numberOfTextures);
	bool SetTextureArrayShaderView(const D3D11_TEXTURE2D_DESC & texArrayDesc, int numberOfTextures);
};
