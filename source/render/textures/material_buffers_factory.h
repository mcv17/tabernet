#pragma once

#include "material.h"
#include "material_buffers.h"


// Material cts factory. Used to create constant buffers for materials with extra variables.
// For now only used by parallax.
class CMaterialBuffersFactory {
public:
	static CMaterialBuffersFactory & Instance() {
		static CMaterialBuffersFactory instance;
		return instance;
	}

	MaterialConstantBuffers * create(const std::string & constant, const json & constantData);
};