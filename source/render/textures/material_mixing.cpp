#include "mcv_platform.h"
#include "material_mixing.h"
#include "engine.h"

CMaterialMixing::CMaterialMixing() : ctes_mixing(CTE_BUFFER_SLOT_MATERIAL_EXTRA){}

void CMaterialMixing::activate() const {
	// Upload base ctes if it has some.
	// We are not using them for now so we don't upload them.
	//ctes_material.activate();

	// Upload ctes mixing.
	ctes_mixing.activate();

	// Tech
	tech->activate();

	// 3 materials sets
	mats[0]->activateTextures(TS_FIRST_SLOT_MATERIAL_0);
	mats[1]->activateTextures(TS_FIRST_SLOT_MATERIAL_1);
	mats[2]->activateTextures(TS_FIRST_SLOT_MATERIAL_2);

	mix_blend_weights->activate(TS_MIX_BLEND_WEIGHTS);
}

void CMaterialMixing::destroy() {
	ctes_mixing.destroy();
}

bool CMaterialMixing::create(const json& j) {
	if (!CMaterial::create(j)) return false;

	bool is_ok = ctes_mixing.create("Mixing");
	assert(is_ok);

	// Load materials.
	for (int i = 0; i < 3; ++i) {
		std::string mat_name = j["mats"][i];
		mats[i] = EngineResources.getResource(mat_name)->as< CMaterial >();
		assert(mats[i]);
	}

	// Load blending weights textures.
	mix_blend_weights = EngineResources.getResource(j.value("mix_blend_weights", "data/textures/black.dds"))->as<CTexture>();
	assert(mix_blend_weights);

	// Load mixing ctes.
	if (j.count("ctes")) {
		const json & ctes = j["ctes"];
		ctes_mixing.mix_boost_r = ctes.value("mix_boost_r", 1.0);
		ctes_mixing.mix_scale_r = ctes.value("mix_scale_r", 2.0);
		ctes_mixing.mix_boost_g = ctes.value("mix_boost_g", 1.0);
		ctes_mixing.mix_scale_g = ctes.value("mix_scale_g", 2.0);
		ctes_mixing.mix_boost_b = ctes.value("mix_boost_b", 1.0);
		ctes_mixing.mix_scale_b = ctes.value("mix_scale_b", 2.0);
	}
	else {
		ctes_mixing.mix_boost_r = 1.0;
		ctes_mixing.mix_scale_r = 2.0;
		ctes_mixing.mix_boost_g = 1.0;
		ctes_mixing.mix_scale_g = 2.0;
		ctes_mixing.mix_boost_b = 1.0;
		ctes_mixing.mix_scale_b = 2.0;
	}

	ctes_mixing.updateGPU();

	return true;
}

void CMaterialMixing::renderInMenu() {
	((CTechnique*)tech)->renderInMenu();
	
	ImGui::Spacing(0, 5);
	bool changed = false;
	changed |= ImGui::DragFloat("Boost R", &ctes_mixing.mix_boost_r, 0.01f, 0.f, 1.f);
	changed |= ImGui::DragFloat("Scale R", &ctes_mixing.mix_scale_r, 0.01f, 0.f, 20.f);
	changed |= ImGui::DragFloat("Boost G", &ctes_mixing.mix_boost_g, 0.01f, 0.f, 1.f);
	changed |= ImGui::DragFloat("Scale G", &ctes_mixing.mix_scale_g, 0.01f, 0.f, 20.f);
	changed |= ImGui::DragFloat("Boost B", &ctes_mixing.mix_boost_b, 0.01f, 0.f, 1.f);
	changed |= ImGui::DragFloat("Scale B", &ctes_mixing.mix_scale_b, 0.01f, 0.f, 20.f);
	
	if (changed)
		ctes_mixing.updateGPU();

	ImGui::Spacing(0, 5);
	if (ImGui::CollapsingHeader("Materials Mix")) {
		for (int i = 0; i < 3; ++i)
			((CMaterial*)mats[i])->renderInMenu();
	}

	ImGui::Spacing(0, 5);
}