#include "mcv_platform.h"
#include "render_to_texture_array.h"
#include "render\render.h"

#include "engine.h"

CRenderToTextureArray * CRenderToTextureArray::current_rt = nullptr;

bool CRenderToTextureArray::create(const char * new_name, int array_size, int tex_width, int tex_height,
	DXGI_FORMAT new_color_format, DXGI_FORMAT new_depth_format, bool uses_depth_of_backbuffer) {
	size_texture_array = array_size;
	width = tex_width;
	height = tex_height;

	// Resource information
	setNameAndType(new_name, getResourceTypeFor<CTexture>());

	// Here we allocate the color texture2D array.
	color_format = new_color_format;
	if (color_format != DXGI_FORMAT_UNKNOWN) {
		if (!CTextureArray::create(array_size, width, height, color_format, CREATE_RENDER_TARGET))
			return false;
		// We also create the render target view necessary for rendering to this texture.
		if (!setRenderTargetViews())	return false;
	}
	else {
		// If we didn't create the color buffer, we must register the resource ourselves,
		// otherwise, the call to the create method from CTexture does it.
		EngineResources.registerResource(this);
	}

	// Create the ZBuffer if necessary.
	depth_format = new_depth_format;
	if (depth_format != DXGI_FORMAT_UNKNOWN) {
		if (!setDepthStencils(getName(), width, height, new_depth_format))
			return false;
	}
	else {
		// Create can have the option to use the ZBuffer of the backbuffer
		if (uses_depth_of_backbuffer) {
			assert(width == Render.width);
			assert(height == Render.height);
			for (int i = 0; i < size_texture_array; ++i) {
				depth_stencil_views[i] = Render.getDepthStencilView();
				depth_stencil_views[i]->AddRef();
			}
		}
	}

	return true;
}

void CRenderToTextureArray::destroy() {
	for(auto & render_target_view : render_target_views)
		SAFE_RELEASE(render_target_view);
	for (auto & depth_stencil_view : depth_stencil_views)
		SAFE_RELEASE(depth_stencil_view);
	
	CTexture::destroy(); // Frees the color buffer.
	z_textures_array->destroy(); // Frees the depth buffer.
}

void CRenderToTextureArray::renderInMenu() {
	// If we have a color buffer...
	if (render_target_views.size() > 0) {
		CTexture::renderInMenu();
	}

	// Show the Depth Buffer if it exists
	if (render_target_views.size() > 0 && getZTextureArray())
		z_textures_array->renderInMenu();
}

CRenderToTextureArray * CRenderToTextureArray::activateRT(int slot, const Vector4 & clear_color){
	CRenderToTextureArray * prev_rt = current_rt;
	Render.activateRenderTarget(&render_target_views[slot], depth_stencil_views[slot]);
	activateViewport();
	current_rt = this;
	return prev_rt;
}

CRenderToTextureArray * CRenderToTextureArray::activateRTAndClear(int slot, const Vector4 & clear_color) {
	CRenderToTextureArray * prev_rt = current_rt;

	Render.activateRenderTarget(&render_target_views[slot], depth_stencil_views[slot]);
	activateViewport();
	current_rt = this;
	Render.clearRenderingTargetView(render_target_views[slot], clear_color);
	if (depth_stencil_views.size() > 0)
		Render.clearDepthStencilView(depth_stencil_views[slot], clear_color);

	return prev_rt;
}

CRenderToTextureArray * CRenderToTextureArray::activateRTNoZClear(int slot, const Vector4 & clear_color) {
	CRenderToTextureArray * prev_rt = current_rt;

	Render.activateRenderTarget(&render_target_views[slot], depth_stencil_views[slot]);
	activateViewport();
	Render.clearRenderingTargetView(render_target_views[slot], clear_color);

	return prev_rt;
}

void CRenderToTextureArray::clearRenderTargetView(int slot, const Vector4 & clear_color){
	assert(render_target_views.size() > 0);
	Render.clearRenderingTargetView(render_target_views[slot], clear_color);
}

void CRenderToTextureArray::clearDepthBuffer(int slot, const Vector4 & clear_color){
	if (depth_stencil_views.size() > 0)
		Render.clearDepthStencilView(depth_stencil_views[slot], clear_color);
}

void CRenderToTextureArray::renderToScreen() const {
	const CMesh * screen_space_mesh = EngineResources.getResource("screen_space_mesh.mesh")->as<CMesh>();
	screen_space_mesh->activateAndRender();
}

void CRenderToTextureArray::activateViewport() {
	D3D11_VIEWPORT vp;
	vp.Width = (float)width;
	vp.Height = (float)height;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.MinDepth = 0.f;
	vp.MaxDepth = 1.f;
	Render.setViewport(vp);
}

bool CRenderToTextureArray::setRenderTargetViews() {
	D3D11_RENDER_TARGET_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	desc.Texture2DArray.MipSlice = 0;
	desc.Texture2DArray.ArraySize = 1;

	for (int i = 0; i < size_texture_array; i++) {
		// Change slot RTV desc to choose correct slice from array
		desc.Texture2DArray.FirstArraySlice = D3D11CalcSubresource(0, i, 1);
		// Create the RTV for the slot in m_renderSlotsTexArray
		ID3D11RenderTargetView * slotRtv;
		HRESULT hr = Render.device->CreateRenderTargetView(texture, &desc, &slotRtv);
		if (FAILED(hr)) return false;

		// Add the RenderTargetView to a list
		render_target_views.push_back(slotRtv);
	}
	
	return true;
}

bool CRenderToTextureArray::setDepthStencils(const std::string & aname, int width, int height, DXGI_FORMAT format)
{
	assert(format == DXGI_FORMAT_R32_TYPELESS || format == DXGI_FORMAT_R24G8_TYPELESS
		|| format == DXGI_FORMAT_R16_TYPELESS || format == DXGI_FORMAT_D24_UNORM_S8_UINT
		|| format == DXGI_FORMAT_R8_TYPELESS);

	// Crear un ZBuffer de la resolucion de mi backbuffer.
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = size_texture_array;
	desc.Format = format;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;

	// The format 'DXGI_FORMAT_D24_UNORM_S8_UINT' can't be binded to shader resource
	if (format != DXGI_FORMAT_D24_UNORM_S8_UINT)
		desc.BindFlags |= D3D11_BIND_SHADER_RESOURCE;

	// SRV = Shader Resource View
	// DSV = Depth Stencil View
	DXGI_FORMAT texturefmt = DXGI_FORMAT_R32_TYPELESS;
	DXGI_FORMAT SRVfmt = DXGI_FORMAT_R32_FLOAT;       // Stencil format
	DXGI_FORMAT DSVfmt = DXGI_FORMAT_D32_FLOAT;       // Depth format

	switch (format) {
	case DXGI_FORMAT_R32_TYPELESS:
		SRVfmt = DXGI_FORMAT_R32_FLOAT;
		DSVfmt = DXGI_FORMAT_D32_FLOAT;
		break;
	case DXGI_FORMAT_R24G8_TYPELESS:
		SRVfmt = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
		DSVfmt = DXGI_FORMAT_D24_UNORM_S8_UINT;
		break;
	case DXGI_FORMAT_R16_TYPELESS:
		SRVfmt = DXGI_FORMAT_R16_UNORM;
		DSVfmt = DXGI_FORMAT_D16_UNORM;
		break;
	case DXGI_FORMAT_R8_TYPELESS:
		SRVfmt = DXGI_FORMAT_R8_UNORM;
		DSVfmt = DXGI_FORMAT_R8_UNORM;
		break;
	case DXGI_FORMAT_D24_UNORM_S8_UINT:
		SRVfmt = desc.Format;
		DSVfmt = desc.Format;
		break;
	default:
		Utils::fatal("Unsupported format creating depth buffer\n");
	}

	ID3D11Texture2D * depth_stencil_resource = nullptr;
	HRESULT hr = Render.device->CreateTexture2D(&desc, NULL, &depth_stencil_resource);
	if (FAILED(hr)) return false;

	// Setup the description of the shader resource view.
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = SRVfmt;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	shaderResourceViewDesc.Texture2DArray.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2DArray.MipLevels = desc.MipLevels;
	shaderResourceViewDesc.Texture2DArray.FirstArraySlice = 0;
	shaderResourceViewDesc.Texture2DArray.ArraySize = size_texture_array;

	// Create the shader resource view.
	ID3D11ShaderResourceView * depth_resource_view = nullptr;
	hr = Render.device->CreateShaderResourceView(depth_stencil_resource, &shaderResourceViewDesc, &depth_resource_view);
	if (FAILED(hr))
		return false;

	z_textures_array = new CTextureArray();
	z_textures_array->setDXParams(size_texture_array, depth_stencil_resource, depth_resource_view);
	z_textures_array->setNameAndType("Z" + aname, getResourceTypeFor<CTexture>());
	EngineResources.registerResource(z_textures_array);
	setDXName(depth_stencil_resource, (z_textures_array->getName() + "_DSR").c_str());
	setDXName(depth_resource_view, (z_textures_array->getName() + "_DRV").c_str());

	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DSVfmt;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
	descDSV.Texture2DArray.MipSlice = 0;
	descDSV.Texture2DArray.ArraySize = 1;

	for (int i = 0; i < size_texture_array; i++) {
		// Change slot RTV desc to choose correct slice from array
		descDSV.Texture2DArray.FirstArraySlice = D3D11CalcSubresource(0, i, 1);
		
		// Create the RTV for the slot in m_renderSlotsTexArray
		ID3D11DepthStencilView * slotDSV;
		HRESULT hr = Render.device->CreateDepthStencilView(depth_stencil_resource, &descDSV, &slotDSV);
		if (FAILED(hr)) return false;

		// Add the RenderTargetView to a list
		depth_stencil_views.push_back(slotDSV);
	}

	return true;
}