#pragma once

#include "material.h"

// Parallax material. Has the cts for a parallax material.
class CMaterialParallax : public CMaterial {

	CCteBuffer<TCtesMaterialParallax> ctes_material;

public:
	CMaterialParallax();
	virtual ~CMaterialParallax() {}

	bool create(const json & j) override;
	void destroy() override;
	void activate() const override;
	void renderInMenu() override;
};
