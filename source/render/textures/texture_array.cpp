#include "mcv_platform.h"
#include "texture_array.h"
#include "DDSTextureLoader.h"
#include "render/render.h"

#include "engine.h"

bool CTextureArray::create(const std::vector<std::string> filenames, bool load_textures_as_individual_resources) {
	assert(filenames.size() > 0 && "Vector with texture filepaths was empty");

	int numberOfTextures = 0;
	std::vector<ID3D11Resource *> texturesToAdd;
	texturesToAdd.reserve(filenames.size());

	for (auto & textureFileName : filenames) {
		int index = 0;
		wchar_t wFilename[MAX_PATH];
		mbstowcs(wFilename, textureFileName.c_str(), textureFileName.length() + 1);

		if (load_textures_as_individual_resources) {
			CTexture * texture = nullptr;
			texture = new CTexture();
			if (!texture->create(textureFileName)) return false;
			EngineResources.registerResource(texture);
			texturesToAdd.push_back(texture->getTextureResource());
		}
		else {
			texturesToAdd.push_back(nullptr);
			HRESULT hr = DirectX::CreateDDSTextureFromFile(Render.device, wFilename,
				&texturesToAdd[index], nullptr);
			if (FAILED(hr)) return false;
		}

		index++;
		numberOfTextures++;
	}
	
	assert((texturesToAdd.size() > 0) && "Vector with texture resources was empty");
	
	// Here we create our texture array.
	// For setting up the array resource we send the information of a texture.
	// All textures should have the same properties or the array would not load.
	D3D11_TEXTURE2D_DESC textureDescription;
	((ID3D11Texture2D*)texturesToAdd[0])->GetDesc(&textureDescription);
	
	if (!SetTextureArrayResource(textureDescription, numberOfTextures)) return false;
	
	if (load_textures_as_individual_resources) {
		if (!CopyTexturesToTextureArray(texturesToAdd, textureDescription, numberOfTextures)) return false;
	}
	
	if (!SetTextureArrayShaderView(textureDescription, numberOfTextures)) return false;

	if (!load_textures_as_individual_resources){
		for (ID3D11Resource * textureResource : texturesToAdd)
			SAFE_RELEASE(textureResource);
	}

	size_texture_array = numberOfTextures;

	return true;
}

// Creates a texture with the given resolution, color format and type of texture.
bool CTextureArray::create(int arraySize, int new_xres, int new_yres, DXGI_FORMAT nformat, eCreateOptions options) {
	size_texture_array = arraySize;

	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = new_xres;
	desc.Height = new_yres;
	desc.MipLevels = 1;
	desc.ArraySize = arraySize;
	desc.Format = nformat;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;


	if (options == CREATE_DYNAMIC) {
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else if (options == CREATE_RENDER_TARGET) {
		desc.BindFlags |= D3D11_BIND_RENDER_TARGET;
	}
	else {
		assert(options == CREATE_STATIC);
	}

	ID3D11Texture2D* tex2d = nullptr;
	HRESULT hr = Render.device->CreateTexture2D(&desc, nullptr, &tex2d);
	if (FAILED(hr))
		return false;
	texture = tex2d;
	setDXName(texture, getName().c_str());

	// Create a resource view so we can use the data in a shader
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = nformat;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	shaderResourceViewDesc.Texture2DArray.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2DArray.MipLevels = desc.MipLevels;
	shaderResourceViewDesc.Texture2DArray.FirstArraySlice = 0;
	shaderResourceViewDesc.Texture2DArray.ArraySize = size_texture_array;
	
	hr = Render.device->CreateShaderResourceView(texture, &shaderResourceViewDesc, &shader_resource_view);
	if (FAILED(hr)) return false;
	setDXName(shader_resource_view, getName().c_str());

	//setNameAndType(getName(), getResourceTypeFor<CTexture>());
	EngineResources.registerResource(this);

	return true;
}

void CTextureArray::destroy() {
	CTexture::destroy();
}

void CTextureArray::setDXParams(int size_array, ID3D11Texture2D* new_texture, ID3D11ShaderResourceView* new_srv) {
	size_texture_array = size_array;
	texture = new_texture;
	shader_resource_view = new_srv;
	new_texture->AddRef();
	new_srv->AddRef();
}

bool CTextureArray::SetTextureArrayResource(const D3D11_TEXTURE2D_DESC & textureDescription, int numberOfTextures) {
	// Prepare the texture array description witht the data of the type of texture it will hold.
	D3D11_TEXTURE2D_DESC texArrayDesc;
	texArrayDesc.Width = textureDescription.Width;
	texArrayDesc.Height = textureDescription.Height;
	texArrayDesc.MipLevels = textureDescription.MipLevels;
	texArrayDesc.ArraySize = numberOfTextures;
	texArrayDesc.Format = textureDescription.Format;
	texArrayDesc.SampleDesc.Count = 1;
	texArrayDesc.SampleDesc.Quality = 0;
	texArrayDesc.Usage = D3D11_USAGE_DEFAULT;
	texArrayDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texArrayDesc.CPUAccessFlags = 0;
	texArrayDesc.MiscFlags = 0;

	// Create the texture array resource.
	ID3D11Texture2D * texArray = 0;
	HRESULT hr = Render.device->CreateTexture2D(&texArrayDesc, 0, &texArray);
	if (FAILED(hr)) return false;
	texture = texArray;

	return true;
}

bool CTextureArray::CopyTexturesToTextureArray(const std::vector<ID3D11Resource *> & texturesToAdd, const D3D11_TEXTURE2D_DESC & textureDescription, int numberOfTextures) {
	// Now bind all the textures into the array so they can be accesed from it.
	for (unsigned int texElement = 0; texElement < numberOfTextures; ++texElement) {
		// for each mipmap level...
		for (unsigned int mipLevel = 0; mipLevel < textureDescription.MipLevels; ++mipLevel) {
			D3D11_MAPPED_SUBRESOURCE mappedTex2D;
			HRESULT hr = Render.ctx->Map(texturesToAdd[texElement], mipLevel, D3D11_MAP_READ, 0, &mappedTex2D);
			if (FAILED(hr)) return false;

			Render.ctx->UpdateSubresource(texture, D3D11CalcSubresource(mipLevel, texElement,
				textureDescription.MipLevels), 0, mappedTex2D.pData, mappedTex2D.RowPitch, mappedTex2D.DepthPitch);

			Render.ctx->Unmap(texturesToAdd[texElement], mipLevel);
		}
	}
	return true;
}

bool CTextureArray::SetTextureArrayShaderView(const D3D11_TEXTURE2D_DESC & textureDescription, int numberOfTextures) {
	// Create a resource view to the texture array.
	D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
	viewDesc.Format = textureDescription.Format;
	viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	viewDesc.Texture2DArray.MostDetailedMip = 0;
	viewDesc.Texture2DArray.MipLevels = textureDescription.MipLevels;
	viewDesc.Texture2DArray.FirstArraySlice = 0;
	viewDesc.Texture2DArray.ArraySize = numberOfTextures;

	HRESULT hr = Render.device->CreateShaderResourceView(texture, &viewDesc, &shader_resource_view);
	if (FAILED(hr)) return false;
	return true;
}