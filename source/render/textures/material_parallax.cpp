#include "mcv_platform.h"
#include "material_parallax.h"

CMaterialParallax::CMaterialParallax() : ctes_material(CTE_BUFFER_SLOT_MATERIAL){}

bool CMaterialParallax::create(const json & j){
	CMaterial::create(j);

	bool is_ok = ctes_material.create("Material");
	// Read initial values...
	assert(is_ok);

	if (j.count("constants_mat")) {
		const json & cts_mat = j["constants_mat"];

		// Base cts.
		ctes_material.scalar_parallax_roughness = cts_mat.value("roughness_intensity", 1.0);
		ctes_material.scalar_parallax_metallic = cts_mat.value("metallic_intensity", 1.0);
		ctes_material.scalar_parallax_emissive = cts_mat.value("emissive_intensity", 1.0);
		ctes_material.scalar_parallax_ao = cts_mat.value("ao_intensity", 1.0);

		// Parallax constants.
		ctes_material.parallax_height_intensity = cts_mat.value("parallax_intensity", 0.09);
		ctes_material.parallax_min_layers = cts_mat.value("parallax_min_layers", 8);
		ctes_material.parallax_max_layers = cts_mat.value("parallax_max_layers", 50);
	}
	{
		ctes_material.scalar_parallax_roughness = 1.0;
		ctes_material.scalar_parallax_metallic = 1.0;
		ctes_material.scalar_parallax_emissive = 1.0;
		ctes_material.scalar_parallax_ao = 1.0;
		ctes_material.parallax_height_intensity = 0.09;
		ctes_material.parallax_min_layers = 8.0;
		ctes_material.parallax_max_layers = 50.0;
	}

	ctes_material.updateGPU();
	return true;
}

void CMaterialParallax::destroy() {
	ctes_material.destroy();
}

void CMaterialParallax::activate() const {
	CMaterial::activate();
	ctes_material.activate();
}

void CMaterialParallax::renderInMenu(){
	CMaterial::renderInMenu();

	ImGui::Text("Constants STD material.");
	bool modified = ImGui::DragFloat("Roughness Intensity", &ctes_material.scalar_parallax_roughness);
	modified |= ImGui::DragFloat("Metallic Intensity", &ctes_material.scalar_parallax_metallic);
	modified |= ImGui::DragFloat("Emissive Intensity", &ctes_material.scalar_parallax_emissive);
	modified |= ImGui::DragFloat("AO Intensity", &ctes_material.scalar_parallax_ao);

	ImGui::Text("Constants Parallax material.");
	modified = ImGui::DragFloat("Parallax Intensity", &ctes_material.parallax_height_intensity);
	modified |= ImGui::DragFloat("Parallax Min Layers", &ctes_material.parallax_min_layers);
	modified |= ImGui::DragFloat("Parallax Max Layers", &ctes_material.parallax_max_layers);

	if (modified)
		ctes_material.updateGPU();
}