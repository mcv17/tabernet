#include "mcv_platform.h"
#include "material_buffers.h"
#include "engine.h"
#include "components/postFX/comp_postFX_perception.h"

// Buffer for perception materials.
void PerceptionConstantBuffers::load(const json & dataToLoad){
	name = "Perception";

	CCteBuffer<TCtesPerceptionMask> * cts = new CCteBuffer<TCtesPerceptionMask>(CTE_BUFFER_SLOT_MATERIAL_EXTRA);

	std::string PerceptionMaskString = dataToLoad.value<std::string>("PerceptionMask", "perception");
	float PerceptionMask = PostFXPerceptionCategory::MASK_OBJECTS_CATEGORY;
	if (PerceptionMaskString == "perception")
		PerceptionMask = PostFXPerceptionCategory::MASK_PLAYER_CATEGORY;
	else if (PerceptionMaskString == "perception_enemies")
		PerceptionMask = PostFXPerceptionCategory::MASK_ENEMY_CATEGORY;

	cts->create("Perception");
	cts->PerceptionMaskValue = PerceptionMask;
	ct_buffer = cts;
}

void PerceptionConstantBuffers::debugInMenu(){
	CCteBuffer<TCtesPerceptionMask> * cts = (CCteBuffer<TCtesPerceptionMask> *)ct_buffer;
	if (ImGui::DragFloat("Perception Mask", &cts->PerceptionMaskValue, 1.0f, 0.0f, 10.f))
		cts->updateGPU();
}


// Parallax material buffer.
void ParallaxConstantBuffers::load(const json & dataToLoad){
	name = "Parallax";
	
	CCteBuffer<TCtesMaterialParallax> * cts = new CCteBuffer<TCtesMaterialParallax>(CTE_BUFFER_SLOT_MATERIAL_EXTRA);
	cts->create("Parallax");
	cts->parallax_height_intensity = dataToLoad.value("parallax_height_intensity", 0.09);
	cts->parallax_min_layers = dataToLoad.value("parallax_min_layers", 8);
	cts->parallax_max_layers = dataToLoad.value("parallax_max_layers", 50);
	ct_buffer = cts;
}

void ParallaxConstantBuffers::debugInMenu() {
	CCteBuffer<TCtesMaterialParallax> * cts = (CCteBuffer<TCtesMaterialParallax> *)ct_buffer;

	bool changed = false;
	changed |= ImGui::DragFloat("Parallax Intensity", &cts->parallax_height_intensity, 0.001f, 0.0f, 10.f);
	changed |= ImGui::DragFloat("Parallax Min Layers", &cts->parallax_min_layers, 1.0, 0.0, 300.0);
	changed |= ImGui::DragFloat("Parallax Max Layers", &cts->parallax_max_layers, 1.0, 0.0, 300.0);
	if (changed)
		cts->updateGPU();
}

void VanishConstantBuffers::load(const json & dataToLoad) {
	name = "Vanish";

	CCteBuffer<TCtesVanish> * cts = new CCteBuffer<TCtesVanish>(CTE_BUFFER_SLOT_PERCEPTION);
	cts->create("Vanish");
	
	// Load here all your data.
	cts->VanishInnerBorderColor = loadVector3(dataToLoad, "VanishInnerBorderColor");
	cts->VanishInnerBorderSize = dataToLoad.value("VanishInnerBorderSize", 0.0f);
	cts->VanishOutterBorderColor = loadVector3(dataToLoad, "VanishOutterBorderColor");
	cts->VanishOutterBorderSize = dataToLoad.value("VanishOutterBorderSize", 0.0f);
	cts->VanishTotalTime = std::max(dataToLoad.value("VanishTotalTime", 1.0f), 0.0f);
	cts->VanishNoiseScale = dataToLoad.value("VanishNoiseScale", 1.0f);
	cts->VanishAddedIntensity = dataToLoad.value("VanishAddedIntensity", 1.0f);
	cts->VanishCurrentTime = cts->VanishTotalTime;

	_defaultTime = cts->VanishTotalTime;

	cts->updateGPU();
	ct_buffer = cts;
}

void VanishConstantBuffers::debugInMenu() {
	CCteBuffer<TCtesVanish> * cts = (CCteBuffer<TCtesVanish> *)ct_buffer;
	bool flag = false;
	
	if (ImGui::ColorEdit3("Inner Border Color: ", &cts->VanishInnerBorderColor.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR))
		flag = true;
	if (ImGui::ColorEdit3("Outter Border Color: ", &cts->VanishOutterBorderColor.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR))
		flag = true;
	if (ImGui::DragFloat("Inner Border Size: ", &cts->VanishInnerBorderSize, 0.0001, 0.0f, 5.0f)) 
		flag = true;
	if (ImGui::DragFloat("Outter Border Size:", &cts->VanishOutterBorderSize, 0.0001, 0.0f, 5.0f)) 
		flag = true;
	if (ImGui::DragFloat("Noise Vanish: ", &cts->VanishNoiseScale, 0.01, 0.0f, 10.0f))
		flag = true;
	if (ImGui::DragFloat("Intensity Multiplier: ", &cts->VanishAddedIntensity, 0.01, 0.0f, 10.0f))
		flag = true;
	if (ImGui::DragFloat("Current time: ", &cts->VanishCurrentTime, 0.01, 0.0f, 10.0f))
		flag = true;
	if (ImGui::DragFloat("Total time: ", &cts->VanishTotalTime, 0.01, 0.0f, 10.0f)) 
		flag = true;
	
	if(flag)  cts->updateGPU();
}

void VanishConstantBuffers::reset()
{
	CCteBuffer<TCtesVanish>* cts = (CCteBuffer<TCtesVanish>*)ct_buffer;
	cts->VanishCurrentTime = _defaultTime;
}

void VanishConstantBuffers::update(float dt) {
	CCteBuffer<TCtesVanish> * cts = (CCteBuffer<TCtesVanish> *)ct_buffer;
	if(cts->VanishCurrentTime > 0)
		cts->VanishCurrentTime -= dt;
	cts->updateGPU();
}

void BillboardsConstantBuffers::load(const json & dataToLoad){
	name = "Billboarding";

	CCteBuffer<TCtesBillboarding> * cts = new CCteBuffer<TCtesBillboarding>(CTE_BUFFER_SLOT_MATERIAL_EXTRA);
	cts->create("Billboarding");
	ct_buffer = cts;
}

void BillboardsConstantBuffers::onEntityLoaded(CHandle entityOwner) {
	CEntity * owner = entityOwner.getOwner();
	if (!owner) return;
	transformH = owner->getComponent<TCompTransform>();
}

void BillboardsConstantBuffers::update(float dt){
	// We update it each frame in case the scale may change constantly.
	if (transformH.isValid()) {
		TCompTransform * transform = transformH;
		Vector2 transformScaleForBillboarding = transform->getScale();
		CCteBuffer<TCtesBillboarding> * cts = (CCteBuffer<TCtesBillboarding> *)ct_buffer;
		cts->BillboardingScale = transformScaleForBillboarding;
		cts->updateGPU();
	}
}

void BillboardsConstantBuffers::debugInMenu(){
	CCteBuffer<TCtesBillboarding> * cts = (CCteBuffer<TCtesBillboarding> *)ct_buffer;
	ImGui::DragFloat2("Billboard scale: ", &cts->BillboardingScale.x, 0.01, 0.0f, 100.0f);
}

void BlastConstantBuffers::load(const json & dataToLoad) {
	CCteBuffer<TCtesBlast> * cts = new CCteBuffer<TCtesBlast>(CTE_BUFFER_SLOT_BLAST);
	cts->create("Explosion blast");

	cts->BlastScale = dataToLoad.value("BlastScale", 12.0f);
	cts->BlastThickness = dataToLoad.value("BlastThickness", 0.15f);
	cts->BlastIntensity = dataToLoad.value("BlastIntensity", 1.0f);
	cts->BlastWhiteIntensity = dataToLoad.value("BlastWhiteIntensity", 0.15f);
	cts->BlastSpeed = dataToLoad.value("BlastSpeed", 6.0f);
	cts->BlastCurrentTime = 0.0f;

	cts->updateGPU();
	ct_buffer = cts;
}

void BlastConstantBuffers::update(float dt) {
	CCteBuffer<TCtesBlast> * cts = (CCteBuffer<TCtesBlast> *)ct_buffer;
	cts->BlastCurrentTime += dt;
	cts->updateGPU();
}

void BlastConstantBuffers::debugInMenu() {
	CCteBuffer<TCtesBlast> * cts = (CCteBuffer<TCtesBlast> *)ct_buffer;
	bool updateRequired = false;

	updateRequired |= ImGui::DragFloat("Scale: ", &cts->BlastScale, 0.0001, 0.0f, 20.0f);
	updateRequired |= ImGui::DragFloat("Blast thickness: ", &cts->BlastThickness, 0.0001, 0.0f, 5.0f);
	updateRequired |= ImGui::DragFloat("Blast intensity: ", &cts->BlastIntensity, 0.0001, 0.0f, 1.0f);
	updateRequired |= ImGui::DragFloat("Blast white intensity: ", &cts->BlastWhiteIntensity, 0.0001, 0.0f, 1.0f);
	updateRequired |= ImGui::DragFloat("Speed: ", &cts->BlastSpeed, 0.0001, 0.1f, 5.0f);
	ImGui::LabelText("Current time: ", "%f", cts->BlastCurrentTime);

	if (ImGui::Button("Reset"))
		cts->BlastCurrentTime = 0.0f;

	if (updateRequired)  cts->updateGPU();
}

void BlastConstantBuffers::reset() {
	CCteBuffer<TCtesBlast> * cts = (CCteBuffer<TCtesBlast> *)ct_buffer;
	cts->BlastCurrentTime = 0.0f;
	cts->updateGPU();
}

void InteractEffectConstantBuffers::load(const json & dataToLoad) {
	CCteBuffer<TCtesFresnelPBR> * cts = new CCteBuffer<TCtesFresnelPBR>(CTE_BUFFER_SLOT_MATERIAL_EXTRA);
	cts->create("Interact effect");

	cts->FresnelPBRColor = loadVector4(dataToLoad, "color");
	_speed = dataToLoad.value("speed", _speed);
	_exponent = dataToLoad.value("exponent", _exponent);
	_intensity = dataToLoad.value("intensity", _intensity);
	_fadeIntensity = dataToLoad.value("fade_intensity", _fadeIntensity);
	if (_fadeIntensity)
		cts->FresnelPBRExponent = _exponent;
	else
		cts->FresnelPBRIntensity = _intensity;
	_exponentRange = loadVector2(dataToLoad, "exponent_range", Vector2(1.0f, 8.0f));
	if (_exponentRange.x > _exponentRange.y)
		_exponentRange.x = _exponentRange.y;
	_intensityRange = loadVector2(dataToLoad, "intensity_range", Vector2(0.0f, 1.0f));
	if (_intensityRange.x > _intensityRange.y)
		_intensityRange.x = _intensityRange.y;

	cts->updateGPU();
	ct_buffer = cts;
}

void InteractEffectConstantBuffers::update(float dt) {
	CCteBuffer<TCtesFresnelPBR> * cts = (CCteBuffer<TCtesFresnelPBR> *)ct_buffer;
	float delta = dt * _speed;
	
	if (_fadeIntensity)
		cts->FresnelPBRIntensity = (_intensityRange.y - _intensityRange.x) * (cosf(_currentTime + M_PI) + 1.0f) + _intensityRange.x;
	else
		cts->FresnelPBRExponent = (_exponentRange.y - _exponentRange.x) * (cosf(_currentTime + M_PI) + 1.0f) + _exponentRange.x;

	_currentTime = fmodf(_currentTime + delta * 2.0f * M_PI, 2.0f * M_PI);
	cts->updateGPU();
}

void InteractEffectConstantBuffers::debugInMenu() {
	CCteBuffer<TCtesFresnelPBR> * cts = (CCteBuffer<TCtesFresnelPBR> *)ct_buffer;
	bool updateRequired = false;

	updateRequired |= ImGui::ColorEdit4("Color", &cts->FresnelPBRColor.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
	ImGui::DragFloat("Speed", &_speed, 0.01f, 0.0f, 8.0f);
	if (ImGui::Checkbox("Fade intensity", &_fadeIntensity)) {
		if (_fadeIntensity)
			cts->FresnelPBRExponent = _exponent;
		else
			cts->FresnelPBRIntensity = _intensity;
		updateRequired = true;
	}
	if (_fadeIntensity) {
		updateRequired = (ImGui::DragFloat("Exponent", &cts->FresnelPBRExponent, 0.01f, 0.0f, 32.0f));
		if (ImGui::DragFloat2("Intensity range", &_intensityRange.x, 0.001f, 0.0f, 10.0f)) {
			if (_intensityRange.x > _intensityRange.y)
				_intensityRange.x = _intensityRange.y;
			if (_intensityRange.x < 0.0f)
				_intensityRange.x = 0.0f;
			if (_intensityRange.y < 0.0f)
				_intensityRange.y = 0.0f;
		}
	}
	else {
		if (ImGui::DragFloat2("Exponent range", &_exponentRange.x, 0.01f, 1.0f, 32.0f)) {
			if (_exponentRange.x > _exponentRange.y)
				_exponentRange.x = _exponentRange.y;
		}
		updateRequired = (ImGui::DragFloat("Intensity", &cts->FresnelPBRIntensity, 0.001f, 0.0f, 10.0f));
	}


	if (updateRequired)  cts->updateGPU();
}
