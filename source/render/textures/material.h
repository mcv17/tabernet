#pragma once

#include "resources/resource.h"

class CTechnique;
class CTexture;
class MaterialConstantBuffers;
struct TCompBuffers;

class CMaterial : public IResource {
protected:
	static const int max_textures = 7;
	const CTexture*  textures[max_textures];
	bool             casts_shadows = true;
	const CMaterial* shadows_material = nullptr;

	// To be able to upload all textures at once
	const ID3D11ShaderResourceView* srvs[max_textures];
	void cacheSRVs();

	CCteBuffer<TCtesMaterial> ctes_material;
	MaterialConstantBuffers *  ctes_material_extra = nullptr; // Free to be defined by someone else. If set it will be activated
public:
	CMaterial();
	virtual ~CMaterial() {}

	virtual bool create(const json& j);
	virtual void destroy();
	virtual void activate() const;
	void activateTextures(int slot0) const;
	void activateCompBuffers(TCompBuffers* c_buffer) const;
	void renderInMenu() override;
	void onFileChanged(const std::string& filename);

	const CTechnique * tech = nullptr;
	eRenderCategory   category;
	int               priority = 100;

	const char* getCategoryName() const;
	bool castsShadows() const { return casts_shadows; }
	const CMaterial* getShadowsMaterial() const { return shadows_material; }
	const CTexture* getTexture(int slot) const { return textures[slot]; }
	void setCtesMaterialExtra(MaterialConstantBuffers * ct) { ctes_material_extra = ct; }
};

// --------------------------
using RenderCategoryNames = NamedValues<eRenderCategory>;
extern RenderCategoryNames category_names;