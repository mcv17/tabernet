#include "mcv_platform.h"
#include "material_buffers_factory.h"
#include "material_buffers.h"

MaterialConstantBuffers * CMaterialBuffersFactory::create(const std::string & constant, const json & constantData) {
	if (constant == "parallax") {
		MaterialConstantBuffers * cts = new ParallaxConstantBuffers();
		cts->load(constantData);
		return cts;
	}
	else if (constant == "vanish") {
		MaterialConstantBuffers * cts = new VanishConstantBuffers();
		cts->load(constantData);
		return cts;
	}
	else if (constant == "Perception"){
		MaterialConstantBuffers * cts = new PerceptionConstantBuffers();
		cts->load(constantData);
		return cts;
	}
	else if (constant == "Billboarding") {
		MaterialConstantBuffers * cts = new BillboardsConstantBuffers();
		cts->load(constantData);
		return cts;
	}
	else if (constant == "blast") {
		MaterialConstantBuffers * cts = new BlastConstantBuffers();
		cts->load(constantData);
		return cts;
	}
	else if (constant == "interact_effect") {
		MaterialConstantBuffers * cts = new InteractEffectConstantBuffers();
		cts->load(constantData);
		return cts;
	}
	return nullptr;
}