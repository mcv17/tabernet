#pragma once

#include "handle/handle_def.h"

class CCteBufferBase;
struct TEmitter;

// This header includes most of the material constant buffers declared.
// Add them header specially if you want to add them to the material factory
// so they can be instantiated from a JSON.

class MaterialConstantBuffers {
protected:
	CCteBufferBase * ct_buffer;
	std::string name;

public:
	~MaterialConstantBuffers() {
		if (ct_buffer) {
			ct_buffer->destroy();
			delete ct_buffer;
		}
	}

	virtual void load(const json & dataToLoad) = 0; // Called when creating the buffer, fetch information from a json. First function called.
	virtual void onEntityLoaded(CHandle entityOwner){} // Called when the entity that owns the material constant buffer is created. Use it to fetch any necessary information.
	virtual void debugInMenu(){}
	virtual void update(float dt){} // This method is used to update values, store them in their class and change the activate method to upload them to the gpu before activating.
	virtual void activate(){ ct_buffer->activate(); }
	virtual void reset() {}

	std::string getName() { return name; }
	CCteBufferBase * getCtBuffer() { return ct_buffer; }
};

// Buffer for perception materials.
class PerceptionConstantBuffers : public MaterialConstantBuffers {
	void load(const json & dataToLoad);
	void debugInMenu();
};


// Buffer for parallax materials.
class ParallaxConstantBuffers : public MaterialConstantBuffers {
	void load(const json & dataToLoad);
	void debugInMenu();
};

// Buffer for vanish materials.
class VanishConstantBuffers : public MaterialConstantBuffers {
	void load(const json & dataToLoad);
	void update(float dt);
	void debugInMenu();
	void reset();

private:
	float _defaultTime;
};

// Buffer for explosion blast materials.
class BlastConstantBuffers : public MaterialConstantBuffers {
	void load(const json & dataToLoad);
	void update(float dt);
	void debugInMenu();
	void reset();

private:
	float _defaultTime;
};

// Buffer for explosion blast materials.
class InteractEffectConstantBuffers : public MaterialConstantBuffers {
	void load(const json & dataToLoad);
	void update(float dt);
	void debugInMenu();

private:
	float _speed = 1.0f;
	Vector2 _intensityRange, _exponentRange;
	float _intensity = 1.0f, _exponent = 4.0f;
	bool _fadeIntensity = false;
	float _currentTime;
};

// Buffer for billboards materials.
/* Use this buffer together with one of the billboard shaders provided
if you want a mesh to show billboarding behaviour. Right now two methods are supported:

* Spherical Billboard: Objects will always face the camera no matter where the position
of the camera is (above the object, at the sides, ...). This is useful for objects like a far away
sun for example.

* Cilindrical Billboard: Objects will rotate to face the camera when the camera moves to the right or left.
If the camera goes above the billboard, it won't face the camera. This is the most common billboard and can be
used for trees, grass, characters like in DukeNukem3D or the original DOOM.

In this moment we don't support painting instanced billboards and probably won't as we are only using them to render
the light of the sun. We could extend billboarding and in such case avoid using this constants instead registering
directly into the module_instancing and using and instanced shader that uses the position, scale and any other values. */
class BillboardsConstantBuffers : public MaterialConstantBuffers {
	void load(const json & dataToLoad);
	void onEntityLoaded(CHandle entityOwner) override;
	void update(float dt);
	void debugInMenu();

private:
	CHandle transformH;
};