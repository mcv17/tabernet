#include "mcv_platform.h"
#include "engine.h"
#include "material.h"
#include "texture.h"
#include "utils/json_resource.h"

#include "components/common/comp_buffers.h"
#include "render/compute/gpu_buffer.h"

#include "material_buffers_factory.h"
#include "material_mixing.h"

class CMaterialResourceType : public CResourceType {
public:
	const char* getExtension(int idx) const override { return "material"; }
	
	const char* getName() const override { return "Materials"; }
	
	IResource* create(const std::string& name) const override {
		// This is the factory of all materials... This time will 
		// check the type attribute looking for custom types
		auto j = Utils::loadJson(name);
		
		std::string mat_type = j.value("type", "std");
		
		CMaterial* new_res = nullptr;
		if (mat_type == "std")
			new_res = new CMaterial();
		else if (mat_type == "mix")
			new_res = new CMaterialMixing();
		else
			Utils::fatal("Invalid material type %s found at", mat_type.c_str(), name.c_str());

		bool is_ok = new_res->create(j);
		if (!is_ok) return nullptr;

		new_res->setNameAndType(name, this);
		return new_res;
	}
};

template<> const CResourceType * getResourceTypeFor<CMaterial>() {
	static CMaterialResourceType resource_type;
	return &resource_type;
}

using RenderCategoryNames = NamedValues<eRenderCategory>;
RenderCategoryNames::TEntry render_category_names[] = {
	{eRenderCategory::CATEGORY_NORMAL_SOLID, "solid"},
	{eRenderCategory::CATEGORY_PERCEPTION, "perception"},
	{eRenderCategory::CATEGORY_PERCEPTION_ENEMIES, "perception_enemies"},
	{eRenderCategory::CATEGORY_PERCEPTION_OBJECTS, "perception_objects"},
	{eRenderCategory::CATEGORY_UI, "ui"},
	{eRenderCategory::CATEGORY_DISTORSIONS, "distorsions"},
	{eRenderCategory::CATEGORY_DECALS, "decals"},
	{eRenderCategory::CATEGORY_DECALS_PERCEPTION, "decals_perception"},
	{eRenderCategory::CATEGORY_TRANSPARENTS, "transparent"},
	{eRenderCategory::CATEGORY_XRAY, "xray"},
	{eRenderCategory::CATEGORY_SHADOWS, "shadows"},
	{eRenderCategory::CATEGORY_PARTICLES, "particles"},
	{eRenderCategory::CATEGORY_PARTICLES_PERCEPTION, "particles_perception"},
	{eRenderCategory::CATEGORY_NO_LIGHTING, "no_lighting"},
	{eRenderCategory::CATEGORY_SKYBOX_LIGHTS, "skybox_light"}
};
RenderCategoryNames category_names(render_category_names, sizeof(render_category_names) / sizeof(RenderCategoryNames::TEntry));

// -------------------------------------------------------
// I'm assuming the normal = albedo + 1, metallic = normal + 1, roughness = metallic + 1
using TextureSlotNames = NamedValues<int>;
TextureSlotNames::TEntry texture_slot_names_data[] = {
	{TS_ALBEDO, "albedo"},
	{TS_NORMAL, "normal"},
	{TS_METALLIC, "metallic"},
	{TS_ROUGHNESS, "roughness"},
	{TS_EMISSIVE, "emissive"},
	{TS_AO, "ao"},
	{TS_HEIGHT, "height"},
	{TS_ENVIRONMENT_MAP, "envmap"},
	{TS_IRRADIANCE_MAP, "irradiance"},
	{TS_ENVIRONMENT_MAP_2, "envmap2"},
	{TS_IRRADIANCE_MAP_2, "irradiance2"},
	{TS_NOISE_MAP, "noise"},
};
TextureSlotNames texture_slot_names(texture_slot_names_data, sizeof(texture_slot_names_data) / sizeof(TextureSlotNames::TEntry));

const char * CMaterial::getCategoryName() const {
	return category_names.nameOf(category);
}

CMaterial::CMaterial() : ctes_material(CTE_BUFFER_SLOT_MATERIAL){}

void CMaterial::destroy() {
	ctes_material.destroy();
	if (ctes_material_extra) {
		delete ctes_material_extra;
		ctes_material_extra = nullptr;
	}
}

bool CMaterial::create(const json& j) {
	assert(j.count("textures") || Utils::fatal("material %s requires attribute textures\n", name.c_str()));

	// Set all textures to a null value
	for (int i = 0; i < max_textures; ++i) {
		textures[i] = nullptr;
		srvs[i] = nullptr;
	}

	// Load textures.
	if (j.count("textures")) {
		const json& jtextures = j["textures"];
		for (auto it : jtextures.items()) {
			std::string slot = it.key();
			std::string texture_name = it.value();
			int ts = texture_slot_names.valueOf(slot.c_str());

			assert((ts >= 0 && ts < max_textures)
				|| Utils::fatal("Material %s has an invalid texture slot %s\n", name.c_str(), slot.c_str()));
			textures[ts] = EngineResources.getResource(texture_name)->as<CTexture>();
		}
	}

	// Fill gaps
	if (!textures[TS_ALBEDO])
		textures[TS_ALBEDO] = EngineResources.getResource("data/textures/null_albedo.dds")->as<CTexture>();
	if (!textures[TS_NORMAL])
		textures[TS_NORMAL] = EngineResources.getResource("data/textures/null_normal.dds")->as<CTexture>();
	if (!textures[TS_METALLIC])
		textures[TS_METALLIC] = EngineResources.getResource("data/textures/white.dds")->as<CTexture>();
	if (!textures[TS_ROUGHNESS])
		textures[TS_ROUGHNESS] = EngineResources.getResource("data/textures/white.dds")->as<CTexture>();
	if (!textures[TS_EMISSIVE])
		textures[TS_EMISSIVE] = EngineResources.getResource("data/textures/null_emissive.dds")->as<CTexture>();
	if (!textures[TS_AO])
		textures[TS_AO] = EngineResources.getResource("data/textures/null_ao.dds")->as<CTexture>();
	if (!textures[TS_HEIGHT])
		textures[TS_HEIGHT] = EngineResources.getResource("data/textures/null_height.dds")->as<CTexture>();

	std::string tech_name = j.value("technique", "objs.tech");
	tech = EngineResources.getResource(tech_name)->as<CTechnique>();

	// Category of the material.
	category = category_names.valueOf(j.value("category", "solid").c_str());

	// Material priority.
	priority = j.value("priority", priority);

	// Shadows.
	casts_shadows = j.value("casts_shadows", casts_shadows);
	if (casts_shadows) {
		std::string shadows_mat_name = j.value("shadows_material", "data/materials/shadows.material");
		if (tech->usesSkin())
			shadows_mat_name = j.value("shadows_material", "data/materials/shadows_skin.material");
		shadows_material = EngineResources.getResource(shadows_mat_name)->as<CMaterial>();
	}

	// Constants materials.
	bool is_ok = ctes_material.create("BaseMaterial");
	assert(is_ok);

	// Upload constant material.
	if (j.count("ctes")) {
		const json & ctes = j["ctes"];

		ctes_material.scalar_roughness = ctes.value("scalar_roughness", 1.0);
		ctes_material.scalar_metallic = ctes.value("scalar_metallic", 1.0);
		ctes_material.scalar_emissive = ctes.value("scalar_emissive", 1.0);
		ctes_material.scalar_ao = ctes.value("scalar_ao", 1.0);

		// Load any constant added.
		if (ctes.count("extra_constant")) {
			const json & extra_ctes = ctes["extra_constant"];
			const json & data = (extra_ctes.count("data") > 0) ? extra_ctes["data"] : extra_ctes;
			ctes_material_extra = CMaterialBuffersFactory::Instance().create(extra_ctes.value("name", ""), data);
		}
	}
	else {
		ctes_material.scalar_roughness = 1.0;
		ctes_material.scalar_metallic = 1.0;
		ctes_material.scalar_emissive = 1.0;
		ctes_material.scalar_ao = 1.0;
	}

	cacheSRVs();

	ctes_material.updateGPU();
	if (ctes_material_extra)
		ctes_material_extra->getCtBuffer()->updateGPU();

	return true;
}

void CMaterial::onFileChanged(const std::string& filename) {
	if (filename == getName())
		create(filename);
	else
		// Maybe a texture has been updated, get the new shader resource view
		cacheSRVs();
}

void CMaterial::cacheSRVs() {
	// Maybe a texture has been updated, get the new shader resource view
	for (int i = 0; i < max_textures; ++i)
		srvs[i] = textures[i] ? textures[i]->getShaderResourceView() : nullptr;
}

void CMaterial::activate() const {
	tech->activate();

	// Activate basic constants.
	ctes_material.activate();

	// Upload extra ctes if there are any.
	if (ctes_material_extra)
		ctes_material_extra->activate();
	
	activateTextures(TS_ALBEDO);
}

void CMaterial::activateTextures(int slot0) const {
	Render.ctx->PSSetShaderResources(slot0, max_textures, (ID3D11ShaderResourceView**)srvs);
}

void CMaterial::renderInMenu() {
	ImGui::Spacing(0, 10);

	category_names.debugInMenu("Category", category);
	ImGui::DragInt("Priority", &priority, 0.2f, 1, 200);
	((CTechnique*)tech)->renderInMenu();

	ImGui::Spacing(0, 5);
	ImGui::LabelText("Tech", "%s", tech->getName().c_str());


	ImGui::Spacing(0, 5);
	if (ImGui::CollapsingHeader("Material textures.")) {
		for (int i = 0; i < max_textures; ++i) {
			const CTexture* t = textures[i];
			ImGui::LabelText(texture_slot_names.nth(i)->name, "%s", t->getName().c_str());
			if (t)
				((CTexture*)t)->renderInMenu();
		}
	}

	ImGui::Spacing(0, 10);
	if (ImGui::CollapsingHeader("Shadow material")) {
		if (casts_shadows && shadows_material)
			((CMaterial*)shadows_material)->renderInMenu();
	}

	ImGui::Spacing(0, 10);
	if (ImGui::CollapsingHeader("Constants material")) {
		bool changed = false;
		ImGui::Text("Base constants");
		changed |= ImGui::DragFloat("Metallic Intensity", &ctes_material.scalar_metallic, 0.01f, 0.f, 1.f);
		changed |= ImGui::DragFloat("Roughness Intensity", &ctes_material.scalar_roughness, 0.01f, 0.f, 1.f);
		changed |= ImGui::DragFloat("Emissive Intensiy", &ctes_material.scalar_emissive, 0.01f, 0.f, 1.f);
		changed |= ImGui::DragFloat("AO Intensity", &ctes_material.scalar_ao, 0.01f, 0.f, 1.f);

		ImGui::Spacing(0, 10);
		ImGui::Text("Extra constants");
		if (changed)
			ctes_material.updateGPU();

		if (ctes_material_extra)
			ctes_material_extra->debugInMenu();
	}

	ImGui::Spacing(0, 10);
}

void CMaterial::activateCompBuffers(TCompBuffers* c_buffers) const {
	const json& jdef = tech->getDefinition();
	for (auto it : jdef["gpu_buffers"].items()) {
		assert(c_buffers);
		const std::string& key = it.key();
		const json& jvalue = it.value();
		auto gpu_buffer = c_buffers->getBufferByName(key.c_str());
		assert(gpu_buffer);
		int slot = jvalue.get<int>();
		Render.ctx->VSSetShaderResources(slot, 1, &gpu_buffer->srv);
	}
}