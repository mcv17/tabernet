#pragma once

#include "render/textures/texture.h"

/* This class represents a texture that can be used for rendering data to it. 
The CTexture it inherits from serves as the color buffer. This is attached
to the render_target_view allowing for rendering data on it.
A second texture is created holding the information of the depth buffer. 
It is named z_texture. This texture is attached to the depth_stencil_view. */
class CRenderToTexture : public CTexture {
	// Width and height of the render to texture, texture.
	int width, height;

	// Format of both the color and depth buffer.
	DXGI_FORMAT              color_format = DXGI_FORMAT_UNKNOWN;
	DXGI_FORMAT              depth_format = DXGI_FORMAT_UNKNOWN;
	
	// Render Target View and Depth Stencil View pointers.
	ID3D11RenderTargetView * render_target_view = nullptr;
	ID3D11DepthStencilView * depth_stencil_view = nullptr;
	
	// Pointer to the depth texture in the GPU.
	ID3D11Texture2D * depth_resource = nullptr;
	// To be able to use the ZBuffer as a texture from our material system.
	// We could use its destroy instead but we hold pointer to the depth stencil view
	// and resource and free them ourselves.
	CTexture * ztexture = nullptr;

	static CRenderToTexture * current_rt;

public:
	/* Create and destroy RT. */
	bool create(const char * new_name, int tex_width, int tex_height, DXGI_FORMAT new_color_format, DXGI_FORMAT new_depth_format = DXGI_FORMAT_UNKNOWN, bool uses_depth_of_backbuffer = false);
	bool create(const std::string & filename) = delete; 	// Methods of creation of a normal texture can't be called from RenderToTexture by the user.
	bool create(int new_xres, int new_yres, DXGI_FORMAT new_color_format, eCreateOptions create_options = CREATE_STATIC) = delete;
	void destroy();

	/* Activate render to texture to be rendered to. */
	// Activates render texture target and depth view to be rendered to. No clear is done.
	CRenderToTexture * activateRT(const Vector4 & clear_color = Vector4(0, 0, 0, 0));
	// Activates render texture target and used depth view passed as parameter. No clear is done.
	CRenderToTexture * activateRTWithDepth(ID3D11DepthStencilView * nDepth_stencil_view, const Vector4 & clear_color = Vector4(0, 0, 0, 0));
	// Activates render texture target and used depth view passed as parameter. No clear is done. Viewport must be given.
	CRenderToTexture * activateRTWithDepthAndViewport(ID3D11DepthStencilView * nDepth_stencil_view, float width, float height, const Vector4 & clear_color = Vector4(0, 0, 0, 0));
	// Activates render to texture target and depth view and clears both views.
	CRenderToTexture * activateRTAndClear(const Vector4 & clear_color = Vector4(0, 0, 0, 0));
	// Activates render to texture target and depth view but doesn't clear depth buffer.
	CRenderToTexture * activateRTNoZClear(const Vector4 & clear_color = Vector4(0, 0, 0, 0));
	
	void activateViewport();
	
	// Clears the render target view.
	void clearRenderTargetView(const Vector4 & clear_color = Vector4(0, 0, 0, 0));
	// Clears the depth buffer.
	void clearDepthBuffer(const Vector4 & clear_color = Vector4(0, 0, 0, 0));

	void renderInMenu() override;

	/* Getters and setters. */
	int getWidth() const { return width; }
	int getHeight() const { return height; }
	ID3D11RenderTargetView * getRenderTargetView() { return render_target_view; }
	ID3D11DepthStencilView * getDepthStencilView() { return depth_stencil_view; }
	CTexture * getZTexture() { return ztexture; }
	static CRenderToTexture * getCurrentRT() { return current_rt; }

private:
	bool setRenderTargetView(const char * new_name);
};
