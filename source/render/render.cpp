#include "mcv_platform.h"
#include "render.h"

#pragma comment(lib,"d3d11")
#pragma comment(lib,"dxguid.lib")   // WKPDID_D3DDebugObjectName in release
#pragma comment(lib,"d3d9")         // For the perf funcs

CRenderD11 Render;

bool CRenderD11::init(HWND hWnd, int renderWidth, int renderHeight) {
	
	PROFILE_FUNCTION("CRenderD11::init");
	width = renderWidth;
	height = renderHeight;

	Utils::dbg("Creating render device %dx%d\n", width, height);

	// We start by creating a struct that holds the information about the swap chain.
	DXGI_SWAP_CHAIN_DESC sd;
	setupSwapChainStruct(hWnd, sd);

	// Now we create our device, device context and swap chain with the information from the struct.	
	if (!setupDevice(sd)) {
		Utils::dbg("Failed while setting up the render device.\n");
		return false;
	}

	// Now we create the render target view that will text Direct3D where to render.
	if (!setupRenderTargetView()) {
		Utils::dbg("Failed while setting up the render target view.\n");
		return false;
	}

	// Create a ZBuffer with the same resolution as our backbuffer. The ZBuffer
	// will hold the depth values of our rendered image.
	if (!setupZBuffer()) {
		Utils::dbg("Failed while setting up the zbuffer memory.\n");
		return false;
	}

	if (!setupDepthStencilView()) {
		Utils::dbg("Failed while setting up the depth stencil view with the zbuffer allocated memory.\n");
		return false;
	}

	if(!setupShaderResourceView()){
		Utils::dbg("Failed while setting up the shader resource view.\n");
		return false;
	}

	ID3D11SamplerState* samplerstate;
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	HRESULT hr = Render.device->CreateSamplerState(&sampDesc, &samplerstate);
	if (FAILED(hr))
		return false;
	Render.ctx->PSSetSamplers(0, 1, &samplerstate);

	// Finally we set the render target view to be used and setup the viewport.
	setRenderTargetView(&render_target_view, depth_stencil_view);
	setViewport();

  rmt_BindD3D11(Render.device, Render.ctx);
	return true;
}

void CRenderD11::setupSwapChainStruct(HWND hWnd, DXGI_SWAP_CHAIN_DESC & sd) {
	ZeroMemory(&sd, sizeof(sd));

	// We fill the swap chain struct contents.
	sd.BufferCount = 1; // A single back buffer will be used for our rendering. The front buffer is already presupossed.
	sd.BufferDesc.Width = width;
	sd.BufferDesc.Height = height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //Use 32-bit color for rendering.
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; // Graphics will be drawn into the back buffer.
	sd.OutputWindow = hWnd; // Window where the output will be shown.
	sd.SampleDesc.Count = 1; // Tells Direct3D how to perform multisample anti-aliased (MSAA) rendering. Maximum guaranteed to support is up to 4 in D3D11 cards.
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE; // Whether we are rendering windowed or fullscreen. Extra changes needed for fullscreen, not only this value.
}

// Creates our device, device context and swap chain with the information from the struct.
bool CRenderD11::setupDevice(const DXGI_SWAP_CHAIN_DESC & sd) {
  UINT createDeviceFlags = 0;	// We will run Direct3D normally.
#ifdef DEBUG
  createDeviceFlags = D3D11_CREATE_DEVICE_DEBUG;
#endif
	D3D_FEATURE_LEVEL  highestFeatureLevelFound = D3D_FEATURE_LEVEL_11_0; // Will return us the highest feature level found.
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	HRESULT hr = D3D11CreateDeviceAndSwapChain(
		nullptr, // Leaving it in null will let DXGI choose the default graphic adapter for rendering.
		D3D_DRIVER_TYPE_HARDWARE, // Whether we should use hardware or software rendering. Different flags possible, hardware is the best for our game.
		nullptr, // Used with the DRIVER_TYPE_SOFTWARE flag.
		createDeviceFlags, // Flags for altering Direct3D rendering normally for debugging purposes. By default set to no flags.
		featureLevels, // Tells Direct3D what features to be expected the program to work with. By default only Direct3D 11.
		numFeatureLevels, // How many feature leves we have in our list.
		D3D11_SDK_VERSION, // Always same parameter, tells user which version of D3D the game is developed for. Avoids executing newer versions of Direct with potential different behaviour.
		&sd, // Pointer to swap chain structure.
		&swap_chain, // Will return us the pointer to the swap chain pointing to the allocated memory.
		&device, // Pointer to the device object. Same functionality as with the swap chain.
		&highestFeatureLevelFound, // Pointer to feature level variable. Filled with flag of the highest feature found, allows programmers to know what kind of hardware the user is running with.
		&ctx // Pointer to pointer to device context object. Same functionality as with swapchain and device.
	);

	// Get sure our hardware support D3D11.
	assert(highestFeatureLevelFound == D3D_FEATURE_LEVEL_11_0);

	if (FAILED(hr))
		return false;
	return true;
}

bool CRenderD11::setupRenderTargetView() {
	// Get the address of the back buffer in memory. We only have one so we access position 0.
	ID3D11Texture2D * pBackBuffer = nullptr;
	HRESULT hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(hr)) return false;

	// Create render target from the back buffer address. Our render target renders into the back buffer.
	hr = device->CreateRenderTargetView(pBackBuffer, nullptr, &render_target_view);
	pBackBuffer->Release(); // We close the texture object used for getting the address of the back buffer.
	// Note that the memory is not freed.
	if (FAILED(hr)) return false;
	return true;
}

bool CRenderD11::setupZBuffer() {
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R24G8_TYPELESS; // was DXGI_FORMAT_D24_UNORM_S8_UINT;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	// Line below indicates that we will use it as an stencil and be able to access it in shaders.
	desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;

	HRESULT hr = Render.device->CreateTexture2D(&desc, NULL, &depth_texture);
	if (FAILED(hr)) return false;
	setDXName(depth_texture, "SwapChain.Z");
	return true;
}

bool CRenderD11::setupDepthStencilView() {
	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	HRESULT hr = Render.device->CreateDepthStencilView(depth_texture, &descDSV, &depth_stencil_view);
	if (FAILED(hr)) return false;
	setDXName(depth_stencil_view, "SwapChain.Z.dsv");
	return true;
}

bool CRenderD11::setupShaderResourceView() {
	// Create a resource view so we can use the data in a shader
	D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
	ZeroMemory(&srv_desc, sizeof(srv_desc));
	srv_desc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srv_desc.Texture2D.MipLevels = 1;
	HRESULT hr = Render.device->CreateShaderResourceView(depth_texture, &srv_desc, &depth_srv);
	if (FAILED(hr)) return false;
	setDXName(depth_srv, "SwapChain.Z.srv");
	return true;
}

void CRenderD11::startRenderingBackBuffer() {
	assert(width > 0);
	assert(height > 0);

	setRenderTargetView(&render_target_view, depth_stencil_view);

	// Setup the viewport
	setViewport();
}

// Clears back buffer backfround setting it to color.
void CRenderD11::clearBackBufferRenderTargetView(const Vector4 & backgroundColor) {
	float ClearColor[4] = { backgroundColor.x, backgroundColor.y, backgroundColor.z, backgroundColor.w }; // red,green,blue,alpha
	ctx->ClearRenderTargetView(render_target_view, ClearColor);
}

// Clears the depth buffer setting it all to the requested background color.
void CRenderD11::clearBackBufferDepthStencil(const Vector4 & backgroundColor) {
	float ClearColor[4] = { backgroundColor.x, backgroundColor.y, backgroundColor.z, backgroundColor.w }; // red,green,blue,alpha
	ctx->ClearDepthStencilView(depth_stencil_view, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void CRenderD11::setViewport() {
	// Setup the viewport
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(viewport));
	viewport.Width = (FLOAT)width;
	viewport.Height = (FLOAT)height;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;

	// Activates the new viewport. First parameter is #viewports, second reference to viewports.
	ctx->RSSetViewports(1, &viewport);
}

// Performs swap in swap chain. Front buffer becomes the new back buffer and viceversa (if only one back buffer was set).
void CRenderD11::swapChain() {
	PROFILE_FUNCTION("CRender::swapChain");
	swap_chain->Present(0, 0);
}

// Activates the back buffer setting the render target view and viewport.  
void CRenderD11::activateRenderTarget(ID3D11RenderTargetView ** render_target_view, ID3D11DepthStencilView * depth_stencil_view){
	assert(width > 0);
	assert(height > 0);

	setRenderTargetView(render_target_view, depth_stencil_view);
}

void CRenderD11::setRenderTargetView(ID3D11RenderTargetView ** render_target_view, ID3D11DepthStencilView * depth_stencil_view) {
	// Set render target view memory where data from graphic pipeline will be stored.
	ctx->OMSetRenderTargets(1, render_target_view, depth_stencil_view);
}

void CRenderD11::destroyRenderTarget() {
	SAFE_RELEASE(render_target_view);
}

void CRenderD11::clearRenderingTargetView(ID3D11RenderTargetView * render_target_view, const Vector4 & backgroundColor) {
	float ClearColor[4] = { backgroundColor.x, backgroundColor.y, backgroundColor.z, backgroundColor.w }; // red,green,blue,alpha
	ctx->ClearRenderTargetView(render_target_view, ClearColor);
}

// Clears the depth buffer memory setting it all to the requested background color.
void CRenderD11::clearDepthStencilView(ID3D11DepthStencilView * depth_stencil_view, const Vector4 & backgroundColor, UINT depthStencilFlags) {
	float ClearColor[4] = { backgroundColor.x, backgroundColor.y, backgroundColor.z, backgroundColor.w }; // red,green,blue,alpha
	ctx->ClearDepthStencilView(depth_stencil_view, depthStencilFlags, 1.0f, 0);
}

// Sets a viewport given as parameter for rendering.
void CRenderD11::setViewport(D3D11_VIEWPORT & vp) {
	// Activates the new viewport. First parameter is #viewports, second reference to viewports.
	ctx->RSSetViewports(1, &vp);
}

bool CRenderD11::resizeBackBuffer(int new_width, int new_height) {
	width = new_width;
	height = new_height;
	
	destroyRenderTarget();

	Utils::dbg("Resizing backbuffer to %dx%d\n", width, height);
	HRESULT hr = swap_chain->ResizeBuffers(0, (UINT)width, (UINT)height, DXGI_FORMAT_UNKNOWN, 0);
	if (FAILED(hr)) return false;

	setupRenderTargetView();
	setupZBuffer();
	setupDepthStencilView();
	setupShaderResourceView();
	return true;
}

// Call before closing the program. Deallocates resources from the GPU for Windows to reuse.
// Otherwise a reboot will be necessary.
void CRenderD11::close() {
	rmt_UnbindD3D11();
	if (ctx) ctx->ClearState();
	SAFE_RELEASE(depth_stencil_view);
	SAFE_RELEASE(depth_texture);
	SAFE_RELEASE(depth_srv);
	destroyRenderTarget();
	SAFE_RELEASE(swap_chain);
	SAFE_RELEASE(device);
	SAFE_RELEASE(ctx);
}
