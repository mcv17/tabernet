#pragma once


class CModuleRender;

class CForwardRenderer{
private:
	/* Width and height of the screen and pointers to the different textures
	used by the deferred renderer. */
	int               xres = 0, yres = 0;

public:

	CForwardRenderer();
		
	void renderCategoryTransparent(CRenderToTexture * rt_destination, CRenderToTexture * rt_auxiliar);
};


