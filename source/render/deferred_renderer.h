#ifndef INC_RENDER_DEFERRED_RENDERER_H_
#define INC_RENDER_DEFERRED_RENDERER_H_

#include "forward_renderer.h" // Used for transparencies.

#include "render/textures/render_to_texture.h"
#include "intel/ASSAO.h"

class CModuleRender;

class CDeferredRenderer {
protected:
	/* Width and height of the screen and pointers to the different textures
	used by the deferred renderer. */
	int               xres = 0, yres = 0;
	CRenderToTexture * rt_albedos = nullptr;
	CRenderToTexture * rt_normals = nullptr;
	CRenderToTexture * rt_depth = nullptr;
	CRenderToTexture * rt_acc_light = nullptr;
	CRenderToTexture * rt_self_illumination = nullptr;
	CRenderToTexture * rt_acc_depth = nullptr;
	CRenderToTexture * rt_perception_mask = nullptr;
	CRenderToTexture * rt_distortions = nullptr;

	ASSAO_Effect *     assao_fx = nullptr;
	
	// And auxiliar texture, used for combining textures when rendering perception.
	CRenderToTexture * rt_auxiliar;
	CRenderToTexture * rt_auxiliar_depth;
	
	void renderGBuffer(eRenderCategory categoryToRender, bool clearDepth = true);
	void renderGBufferPerception(eRenderCategory categoryToRender, bool clearDepth = true);
	void renderGBufferDecals(eRenderCategory decalCategoryToRender, bool clearRts = false);
	void renderAccLight();
	void renderAccLightMinusSkyBox();
	void renderAmbientPass();
	void renderPointLights();
	void renderSpotLights();
	void renderDirectionalLights();
	void renderSkyBox() const;
	void renderAO(CHandle h_camera, bool deactivate) const;

	void renderNormalCategory(CRenderToTexture * rt_destination, CHandle h_camera, eRenderCategory categoryToRender, bool perceptionMode, bool clearDepth = true);
	void renderTransparentCategory(CRenderToTexture * rt_destination, CHandle h_camera);
	void renderPerceptionCategory(CRenderToTexture* rt_destination, CHandle h_camera, eRenderCategory categoryToRender, CModuleRender * renderModule, bool clearDepth = false);
	void renderPerceptionParticles(CRenderToTexture * rt_destination, CModuleRender * renderModule);
	void renderCategoryNoSkyBox(CRenderToTexture * rt_destination, CHandle h_camera, eRenderCategory categoryToRender, bool clearDepth = true);
	void renderPerceptionLightedCategory(CRenderToTexture * rt_destination, CHandle h_camera, eRenderCategory categoryToRender, bool clearDepth = true);
	void renderDecalsPerception(CRenderToTexture * rt_destination);

	friend class TCompPostFXLightScattering;

public:
	CDeferredRenderer();
	
	bool create( int xres, int yres );
	void destroy();
	void renderDefault(CModuleRender * renderModule, CRenderToTexture* rt_destination, CHandle h_camera );
	void renderPerception(CModuleRender * renderModule, CRenderToTexture* rt_destination, CHandle h_camera);
};


#endif

