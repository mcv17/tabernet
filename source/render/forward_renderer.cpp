#include "mcv_platform.h"
#include "forward_renderer.h"

#include "render_manager.h"
#include "render_utils.h"
#include "resources/resource.h"
#include "render/primitives.h"
#include "components/lighting/comp_light_dir.h"

#include "render/module_render.h"

#include "utils/data_provider.h"

#include "engine.h"

CForwardRenderer::CForwardRenderer() {
	Utils::dbg("Creating CForwardRenderer\n");
}

void CForwardRenderer::renderCategoryTransparent(CRenderToTexture * rt_destination, CRenderToTexture * rt_auxiliar) {
	CGpuScope gpu_scope("Forward Transparent");
	rt_auxiliar->activateRT();
	//CRenderManager::get().render(eRenderCategory::CATEGORY_FORWARD_TRANSPARENTS);

	{
		CGpuScope gpu_scope("Combine with the rest.");
		rt_destination->activateRT();
		drawFullScreenQuad("combine_textures.tech", rt_auxiliar);
	}
}