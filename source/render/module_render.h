#pragma once

#include "modules/module.h"
#include "render/deferred_renderer.h"

#include "render/render.h"
#include "render/meshes/mesh.h"
#include "render/textures/texture.h"
#include "render/textures/material.h"
#include "render/primitives.h"
#include "render/textures/render_to_texture.h"

class CEntity;
class CTechnique;
class TCompCamera;
class TCompCulling;
class TCompPlayerCameraPostProcessEffects;

class CModuleRender : public IModule
{
	/* Rendering variables. */
	float currentFrameRate = 0.0f, targetFrameRate = 0.0f;

	CHandle h_entity_camera; // Handle to the camera entity used in rendering.
	TCompCamera * renderCamera; // Pointer to the camera object.
	TCompCulling * cullingCamera; // Pointer to the culling.
	CCamera renderCameraOrtho; // Used for rendering the UI.

	Vector4 clear_color = Vector4(0.142f, 0.142f, 0.142f, 1);

	CDeferredRenderer* deferred = nullptr;
	CRenderToTexture* deferred_output = nullptr;

	bool renderDebugActive = false;

	/* Rendering functions. */

	// Prepares the deferred output rt.
	bool setupDeferredOutput();

	// Fetch render camera.
	bool fetchRenderCamera();

	// Skinning methods.
	void uploadSkinMatricesToGPU();

	// Rendering methods.
	void defaultRendering();
	void perceptionRendering();

	//Render the 3D world
	void renderWorld();
	// Prepares the rendering camera to render UI elements.
	void renderUI();
	
	// Parse techniques with compute shaders
	void parsePipelines(const std::string& filename);

public:
	CModuleRender(const std::string & name) : IModule(name){}
	
	// Starts the render module. Parses techniques, creates render primitives,
	// start render textures and uploads global constant values.
	bool start() override;
	
	// Stop the render module. Frees any memory the object is responsible of.
	void stop() override;

	// Updates the rendering of the engine.
	void render();

	void update(float dt) override {};
	void renderDebug() override;
	void renderInMenu() override;

	// We render to the back buffer for showing in the screen.
	// We don't clear by default as it's done by the deferred rendering
	// at the beginning.
	void activateRenderToBackBuffer(bool clearDepthStencil = false);
	// Same as above but with a new clear color.
	void activateRenderToBackBuffer(bool clearDepthStencil, const Vector4 & new_clear_color);

	// Post processing methods.
	CRenderToTexture * applyPostProcessing(CRenderToTexture * deferred_output, Vector4 & clearColorBackBuffer);
	// Applies post processing methods to the complete image.
	CRenderToTexture * applyFinalPostProcessing(CDeferredRenderer * renderer, CRenderToTexture * deferred_output, CRenderToTexture * rt_acc_depth, Vector4 & clearColorBackBuffer);

	// Applies post processing methods to the base image.
	CRenderToTexture * applyPostProcessingPerceptionNormalObjects(CRenderToTexture * deferred_output);
	// Applies final post processing methods to the base image.
	void applyFinalPostProcessingPerceptionNormalObjects(CRenderToTexture * curr_rt, const std::string & bloomToApply);
	// Applies post processing methods to a perception category. Applies bloom to the elements of the given category.
	CRenderToTexture * applyPostProcessingPerceptionCategory(CRenderToTexture * deferred_output, eRenderCategory perceptionCategory);
	// This is the part where the bloom is applied.
	void applyFinalPostProcessingPerceptionCategory(const std::string & bloomToApply);
	// Applies post processing methods for perceptive particles. Generates the bloom.
	CRenderToTexture * applyPostProcessingPerceptionParticles(CRenderToTexture * deferred_output);
	// Applies post processing methods for perceptive particles. Applies the bloom.
	CRenderToTexture * applyFinalPostProcessingPerceptionParticles(CRenderToTexture * deferred_output, const std::string & bloomToApply);
	// Applies post processing methods for generating the outlines of each category.
	CRenderToTexture * applyPostProcessingPerceptionOutlines(CRenderToTexture * deferred_output, CRenderToTexture * rt_acc_depth, CRenderToTexture * rt_perception_mask, Vector4 & clearColorBackBuffer);
	// Applies post processing methods to the complete image, including previous post processing methods.
	CRenderToTexture * applyFinalPostProcessingPerception(CDeferredRenderer * renderer, CRenderToTexture * deferred_output, CRenderToTexture * rt_acc_depth, CRenderToTexture * rt_perception_mask, Vector4 & clearColorBackBuffer);
	// Apply antialiasing.
	CRenderToTexture * applyAntialiasing(CRenderToTexture * deferred_output);



	// Get the render camera component.
	TCompCamera * getRenderCamera() { return renderCamera; }

	float getCurrentFPS() { return currentFrameRate; }
	float getTargetFPS() { return targetFrameRate; }
	CHandle getCamera() { return h_entity_camera; }
	const Vector4 & getBackBufferClearColor() { return clear_color; }
	float GetAmbientBoost() { return ctes_shared.GlobalAmbientBoost; }
	float GetAmbientLightIntensity() { return ctes_shared.GlobalAmbientLightIntensity; }
	float GetExposureAdjustment() { return ctes_shared.GlobalExposureAdjustment; }
	float GetReflectionIntensity() { return ctes_shared.GlobalReflectionIntensity; }
	float GetGlobalSkyboxLerp() { return ctes_shared.GlobalSkyBoxLerp; }

	bool getRenderDebugActive() { return renderDebugActive; }
	void toggleRenderDebug() { renderDebugActive = !renderDebugActive; }
	void setRenderDebug(bool active) { renderDebugActive = active; }

	/* Setters */
	void SetRenderAmbientVariables(float AmbientBoost = -1.0f, float AmbientLightIntensity = -1.0f, float ExposureAdjustment = -1.0f, float ReflectionIntensity = -1.0f, float SkyboxLerp = -1.0f) {
		if(AmbientBoost >= 0.0f)
			ctes_shared.GlobalAmbientBoost = AmbientBoost;
		if (AmbientLightIntensity >= 0.0f)
			ctes_shared.GlobalAmbientLightIntensity = AmbientLightIntensity;
		if(ExposureAdjustment >= 0.0f)
			ctes_shared.GlobalExposureAdjustment = ExposureAdjustment;
		if (ReflectionIntensity >= 0.0f)
			ctes_shared.GlobalReflectionIntensity = ReflectionIntensity;
		if(SkyboxLerp >= 0.0f)
			ctes_shared.GlobalSkyBoxLerp = SkyboxLerp;
		ctes_shared.updateGPU();
	}

	void SetAmbientBoost(float nAmbientBoost) {
		ctes_shared.GlobalAmbientBoost = nAmbientBoost;
		ctes_shared.updateGPU();
	}
	
	void SetAmbientLightIntensity(float nAmbientLightIntensity) {
		ctes_shared.GlobalAmbientLightIntensity = nAmbientLightIntensity;
		ctes_shared.updateGPU();
	}

	void SetExposureAdjustment(float ExposureAdjustment) {
		ctes_shared.GlobalExposureAdjustment = ExposureAdjustment;
		ctes_shared.updateGPU();
	}

	void SetReflectionIntensity(float ReflectionIntensity) {
		ctes_shared.GlobalReflectionIntensity = ReflectionIntensity;
		ctes_shared.updateGPU();
	}

	void SetGlobalSkyBoxLerp(float nSkyboxLerp) {
		ctes_shared.GlobalSkyBoxLerp = nSkyboxLerp;
		ctes_shared.updateGPU();
	}
};
