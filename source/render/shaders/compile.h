#pragma once

/* This function allows for the compilation of a shader file. */

extern HRESULT compileShaderFromFile(const std::string& filePath, const std::string& szEntryPoint, LPCSTR szShaderModel, ID3DBlob** compiledShaderCode);
