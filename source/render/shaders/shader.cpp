#include "mcv_platform.h"
#include "render/render.h"
#include "render/meshes/vertex_declarations.h"
#include "compile.h"
#include "shader.h"

bool CVertexShader::create(const std::string& source, const std::string& entry_fn, const std::string& vertex_decl_name) {
	HRESULT hr;
	
	// Compile the vertex shader.
	ID3DBlob* pVSBlob = nullptr;
	hr = compileShaderFromFile(source, entry_fn, "vs_5_0", &pVSBlob);
	if (FAILED(hr))
	{
		WindowsUtils::DisplayErrorWindow("Error", "The VS FX file cannot be compiled. Please run this executable from the directory that contains the FX file.");
		return hr;
	}
	
	// Once compiled, we now create the vertex shader object from the compiled code and save the reference to the object in vs.
	hr = Render.device->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &vs);
	if (FAILED(hr))
	{
		pVSBlob->Release();
		return false;
	}
	
	// Now, after the shader object has been successfully created, we need to create the input-layout object for it which will
	// allow Direct3D to describe the input-buffer data for the input assembler (i.e what is what in the GPU buffer for the vs shader).
	vertex_decl = CVertexDeclarationManager::instance().getVertexDeclByName(vertex_decl_name);
	assert(vertex_decl);
	
	// We must add to the render the new input layout.
	hr = Render.device->CreateInputLayout(&vertex_decl->layout.front(), vertex_decl->numElements, pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), &vertex_layout);
	pVSBlob->Release(); // Free from memory the compiled code once the shader object and layout are set up.
	if (FAILED(hr))
		return false;

	setDXName(vs, (entry_fn + "@" + source).c_str());
	setDXName(vertex_layout, vertex_decl_name.c_str());

  return true;
}

// Free the vertex shader resources.
void CVertexShader::destroy() {
	SAFE_RELEASE(vertex_layout);
	SAFE_RELEASE(vs);
}

// Activates the shader.
void CVertexShader::activate() const {
	assert(vs);

	// Set the input layout and set the vs shader of the pipeline to this one.
	Render.ctx->IASetInputLayout(vertex_layout);
	Render.ctx->VSSetShader(vs, nullptr, 0);
}

void CVertexShader::deactivateResources() {
  // Can't update a buffer if it's still bound in a vs. So unbound it.
  ID3D11ShaderResourceView* srv_nulls[3] = { nullptr, nullptr, nullptr };
  Render.ctx->VSSetShaderResources(0, 3, srv_nulls);
}

bool CPixelShader::create(const std::string& source, const std::string& entry_fn) {
	HRESULT hr;
  
	// Compile the pixel shader.
	ID3DBlob* pPSBlob = nullptr;
	hr = compileShaderFromFile(source, entry_fn, "ps_5_0", &pPSBlob);
	if (FAILED(hr))
	{
		WindowsUtils::DisplayErrorWindow("Error", "The PS FX file cannot be compiled. Please run this executable from the directory that contains the FX file.");
		return hr;
	}

	// Create the pixel shader obj from the compiled code.
	hr = Render.device->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &ps);
	pPSBlob->Release(); // Free from memory the compiled code once the shader object and layout are set up.
	if (FAILED(hr))
		return false;
	
	return true;
}

void CPixelShader::destroy() {
	SAFE_RELEASE(ps);
}

void CPixelShader::activate() const {
	assert(ps);
	
	// Send the PS to the pipeline. Substituting previous one.
	Render.ctx->PSSetShader(ps, nullptr, 0);
}

