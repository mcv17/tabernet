#include "mcv_platform.h"
#include "render/render.h"
#include <d3dcompiler.h>

#ifdef _DEBUG
#define SHADER_BLOB_PATH				"shaders/debug/"
#else
#define SHADER_BLOB_PATH				"shaders/"
#endif
#define SHADER_BLOB_EXTENSION		".cso"	

#pragma comment(lib, "d3dcompiler")

bool fetchCompiledShader(const std::string& fileName, ID3DBlob** compiledShaderCode) {
	WCHAR szFilename[MAX_PATH];
	mbstowcs(szFilename, fileName.c_str(), MAX_PATH);
	HRESULT hr = D3DReadFileToBlob(szFilename, compiledShaderCode);
	return !FAILED(hr);
}

//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
HRESULT compileShaderFromFile(const std::string& filePath, const std::string& szEntryPoint, LPCSTR szShaderModel, ID3DBlob** compiledShaderCode)
{
  HRESULT hr = S_OK;
  TFileContext fc(filePath);

  /* We start by setting the shader flags. */

  DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
  // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
  // Setting this flag improves the shader debugging experience, but still allows 
  // the shaders to be optimized and to run exactly the way they will run in 
  // the release configuration of this program.
#if defined( DEBUG ) || defined( _DEBUG )
  dwShaderFlags |= D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
  // Avoids transposing matrices when sending them to the cte.
  dwShaderFlags |= D3DCOMPILE_PACK_MATRIX_ROW_MAJOR;
  
  /* Load into memory the shader file. */

  // Get filename of shader and store it into wchar array.
  WCHAR szFilename[MAX_PATH];
  mbstowcs(szFilename, filePath.c_str(), MAX_PATH);

	auto filePathData = Utils::splitPath(filePath);
	std::string filename = filePathData.filename;

	std::string blobFilename = filename + "_" + szEntryPoint;
	blobFilename = SHADER_BLOB_PATH + blobFilename + SHADER_BLOB_EXTENSION;
	struct _stat result;
	struct _stat resultBlob;
	_stat(filePath.c_str(), &result);
	if (_stat(blobFilename.c_str(), &resultBlob) != 0 || resultBlob.st_mtime < result.st_mtime) {
		while (true) {
			ID3DBlob* pErrorBlob = NULL; // Returns us any possible errors during the compilation process.

		// Compile our shader.
			hr = D3DCompileFromFile(szFilename, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, szEntryPoint.c_str(),
															szShaderModel, dwShaderFlags, 0, compiledShaderCode, &pErrorBlob);


			if (FAILED(hr)) {
				if (hr == HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND)) {
					Utils::fatal("Missing shader input file '%s'\n", filePath.c_str());
				}
				if (pErrorBlob != NULL) {
					const char* err_msg = (char*)pErrorBlob->GetBufferPointer();
					Utils::fatal(err_msg);
				}
				if (pErrorBlob)
					pErrorBlob->Release();
				continue;
			}
			if (pErrorBlob)
				pErrorBlob->Release();
			break;
		}

		mbstowcs(szFilename, blobFilename.c_str(), MAX_PATH);
		hr = D3DWriteBlobToFile(*compiledShaderCode, szFilename, TRUE);
	}
	else
		fetchCompiledShader(blobFilename, compiledShaderCode);


  return S_OK;
}


