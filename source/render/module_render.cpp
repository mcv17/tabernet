#include "mcv_platform.h"
#include "module_render.h"

#include "geometry/camera.h"

#include "entity/entity.h"
#include "components/common/comp_render.h"
#include "render/render_manager.h"
#include "components/common/comp_transform.h"
#include "components/camera/comp_camera.h"
#include "components/camera/comp_camera_manager.h"
#include "components/common/comp_culling.h"
#include "components/common/comp_tags.h"
#include "skeleton/comp_skeleton.h"
#include "render/render.h"
#include "render/meshes/mesh.h"
#include "render/textures/material.h"
#include "render/render_manager.h"
#include "render/compute/compute_shader.h"

#include "components/lighting/comp_light_dir.h"
#include "components/lighting/comp_light_spot.h"

#include "components/postfx/comp_postFX_perception.h"
#include "components/postfx/comp_postFX_blur.h"
#include "components/postFX/comp_postFX_motion_blur.h"
#include "components/postfx/comp_postFX_focus.h"
#include "components/postfx/comp_postFX_bloom.h"
#include "components/postFX/comp_postFX_fog.h"
#include "components/postFX/comp_postFX_vignetting.h"
#include "components/postfx/comp_postFX_color_grading.h"
#include "components/postFX/comp_postFX_burningfade.h"
#include "components/postFX/comp_postFX_black_bars.h"
#include "components/postFX/comp_postFX_antialiasing.h"
#include "components/postFX/comp_postFX_LightScattering.h"

#include "ui/module_ui.h"

#include "engine.h"

#include <filesystem>

bool CModuleRender::start() {

	// Start deferred rendering.
	deferred = new CDeferredRenderer();
	deferred_output = new CRenderToTexture();

	setupDeferredOutput();
	deferred->create(Render.width, Render.height);

	// Initiate render utils, parse all the techniques and create all render primitives.
	createRenderUtils();
	parsePipelines("data/shaders/techniques.json");
	createRenderPrimitives();

	// Load the json config for the module.
	json renderJson = Utils::loadJson("data/modules/render.json");

	// Upload initial rendering module variables.
	ctes_shared.GlobalRenderOutput = RO_COMPLETE;
	ctes_shared.GlobalAmbientBoost = renderJson.value("GlobalAmbientBoost", 1.5f);
	ctes_shared.GlobalExposureAdjustment = renderJson.value("GlobalExposureAdjustment", 1.1f);
	ctes_shared.GlobalReflectionIntensity = renderJson.value("GlobalReflectionIntensity", 1.0f);
	ctes_shared.GlobalAmbientLightIntensity = renderJson.value("GlobalAmbientLightIntensity", 1.0f);
	
	targetFrameRate = renderJson.value("TargetFPS", 60);
	
	return true;
}

void CModuleRender::parsePipelines(const std::string& filename) {
  PROFILE_FUNCTION("CModuleRender::parsePipelines");
  TFileContext fc(filename);

	std::string shadersFolder = "shaders";
	std::string shadersDebugFolder = shadersFolder + "/debug";
	
	namespace fs = std::filesystem;

	if (!fs::exists(shadersFolder))
		fs::create_directory(shadersFolder);
	if (!fs::exists(shadersDebugFolder))
		fs::create_directory(shadersDebugFolder);

  json j = Utils::loadJson(filename);

  for (auto it : j.items()) {
    PROFILE_FUNCTION_COPY_TEXT(it.key().c_str());
    TFileContext fce(it.key());

    const json& j = it.value();

    const CResourceType* res_type = nullptr;
    IResource* new_res = nullptr;
    if (j.count("cs")) {
      res_type = getResourceTypeFor<CComputeShader>();
      CComputeShader* cs = new CComputeShader();
      if (cs->create(j)) 
        new_res = cs;
    }
    else {
      assert(j.count("vdecl") > 0);
      res_type = getResourceTypeFor<CTechnique>();
      CTechnique* tech = new CTechnique();
      if (tech->create(j))
        new_res = tech;
    }

    std::string suffix = std::string(".") + res_type->getExtension();
    std::string name = it.key() + suffix;

    if (new_res == nullptr) {
      Utils::fatal("Failed to create %s %s\n", res_type->getName(), name.c_str());
      continue;
    }

    // Create a new tech, configure it and register it as resource
    new_res->setNameAndType(name, res_type);
    Utils::dbg("Registering %s %s\n", res_type->getName(), name.c_str());
    EngineResources.registerResource(new_res);
  }
}

void CModuleRender::stop() {
	// Render targets are owned by the Resources obj
	deferred = nullptr;
	deferred_output = nullptr;

	destroyRenderPrimitives();
	destroyRenderUtils();
}

// After this function is called in the engine, we are ready
// to start rendering all the modules in the rendering sections.
void CModuleRender::activateRenderToBackBuffer(bool clearDepthStencil) {
	Render.startRenderingBackBuffer();
	Render.clearBackBufferRenderTargetView(clear_color);
	if(clearDepthStencil)
		Render.clearBackBufferDepthStencil(clear_color);
}

void CModuleRender::activateRenderToBackBuffer(bool clearDepthStencil, const Vector4 & new_clear_color) {
	Render.startRenderingBackBuffer();
	Render.clearBackBufferRenderTargetView(new_clear_color);
	if (clearDepthStencil)
		Render.clearBackBufferDepthStencil(new_clear_color);
}

void CModuleRender::renderDebug() {
	getObjectManager<TCompRender>()->renderDebugAll();
}

void CModuleRender::renderInMenu() {
	ImGui::PushID("Render-Module");

	if (ImGui::BeginTabBar("Render - Sections", ImGuiTabBarFlags_None))
	{

		if (ImGui::BeginTabItem("Statistics"))
		{
			ImGui::Spacing(0, 10);

			// FPS graph.
			const int samples = 100;
			static int currentVal = 0;
			static float values[samples] = { 0 };
			static float offsetFrames = 25.0f; // How many frames up and down in the statistics.

			values[currentVal] = ImGui::GetIO().Framerate;
			currentVal = (currentVal + 1) % IM_ARRAYSIZE(values);
			ImGui::PlotLines(" ", values, IM_ARRAYSIZE(values), currentVal, "Frames per second",
				Maths::clamp(ImGui::GetIO().Framerate - offsetFrames, 0.0f, 1000.f), ImGui::GetIO().Framerate + offsetFrames, ImVec2(0, 80));

			ImGui::Spacing(0, 5);
			ImGui::Text("Frames per second: %.1f FPS", ImGui::GetIO().Framerate);
			ImGui::Text("Miliseconds per frame: %.3f ms", 1000.0f / ImGui::GetIO().Framerate);
			ImGui::Text("Time elapsed %f", Time.current);

			ImGui::Spacing(0, 15);
			ImGui::ColorEdit3("Buffer clear color", (float*)&clear_color.x); // Edit 3 floats representing a color
			
			ImGui::Spacing(0, 15);
			ImGui::Separator();
			ImGui::Spacing(0, 15);

			ImGui::TextColored(ImVec4(1.0f, 0.2f, 1.0f, 1.0f), "Profiling");
			ImGui::Spacing(0, 10);

			static int nframes = 5;
			ImGui::Text("Number of frames to capture");
			ImGui::DragInt("Number of frames", &nframes, 0.1f, 1, 20);
			ImGui::Spacing(0, 5);
			if (ImGui::SmallButton("Start CPU Trace Capturing"))
				PROFILE_SET_NFRAMES(nframes);

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Render variables"))
		{
			ImGui::Spacing(0, 10);
			ImGui::DragFloat("Exposure Adjustment", &ctes_shared.GlobalExposureAdjustment, 0.01f, 0.0f, 1000.f);
			ImGui::DragFloat("Ambient Boost", &ctes_shared.GlobalAmbientBoost, 0.01f, 0.0f, 1000.f);
			ImGui::DragFloat("Ambient Light Intensity", &ctes_shared.GlobalAmbientLightIntensity, 0.01f, 0.0f, 1000.f);
			ImGui::DragFloat("Reflections Intensity", &ctes_shared.GlobalReflectionIntensity, 0.01f, 0.0f, 1000.f);

			
			// Must be in the same order as the RO_* ctes
			static const char* render_output_str =
				"Complete\0"
				"Albedo\0"        // RO_ALBEDO
				"Normals\0"
				"Normals ViewSpace\0"
				"Roughness\0"
				"Metallic\0"
				"World Pos\0"
				"Linear Depth\0"
				"AO\0"
				"\0";
			ImGui::Combo("Output", &ctes_shared.GlobalRenderOutput, render_output_str);
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Render Manager"))
		{
			ImGui::Spacing(0, 15);
			CRenderManager::get().renderInMenu();
			ImGui::EndTabItem();
		}

		ImGui::EndTabBar();
	}

	ImGui::Spacing(0, 15);
	EngineImGui.showModuleInWindowButton(this);
	ImGui::Spacing(0, 15);
	ImGui::PopID();
}

void CModuleRender::render() {
	CGpuScope gpu_trace("Module::Render");
	PROFILE_FUNCTION("CModuleRender::render");	
	renderWorld();
	renderUI();
}

void CModuleRender::renderWorld() {
	PROFILE_FUNCTION("CModuleRender::renderWorld");

	// Get the current framerate.
	currentFrameRate = ImGui::GetIO().Framerate;

	// Fetch the camera we will use to render the scene.
	if (!fetchRenderCamera()) return;

	// Update global constants.
	ctes_shared.GlobalWorldTime = (float)Time.current;
	ctes_shared.updateGPU();

	// Clear constants objects.
	activateObject(MAT44::Identity, VEC4(1, 1, 1, 1));

	// Upload skin matrices to the GPU.
	uploadSkinMatricesToGPU(); // Before starting our render, we update the bones of the animations.

	/* -------------------------- Shadow generations -------------------------- */

	// Generate the shadow maps for each light in this scene that casts shadows
	// and is active.
	{
		PROFILE_FUNCTION("Generate Shadow Maps");
		{
			PROFILE_FUNCTION("Generate CSM");
			CGpuScope gpu_scope("Light Dir shadow maps");
			getObjectManager<TCompLightDir>()->forEach([this](TCompLightDir * cs) {
				cs->generateShadowMap(*renderCamera);
			});
		}
		{
			PROFILE_FUNCTION("Generate Spot Lights SM");
			CGpuScope gpu_scope("Light Spot shadow maps");
			getObjectManager<TCompLightSpot>()->forEach([this](TCompLightSpot * cs) {
				cs->generateShadowMap();
			});
		}
	}

	/* -------------------------- Main render -------------------------- */

	// As we rendered the scene from shadows, we must reactivate this camera.
	if (!fetchRenderCamera()) return;
	
	// Updates the render camera viewport.
	renderCamera->activateViewport();

	// Do the culling for this camera. (We don't call it on update, we want to have the information
	// of the GPU Culling for the rendering cam).
	if(cullingCamera)
		cullingCamera->update(0.0f);

	// Now start rendering of the scene.

	// Activate the camera.
	activateCamera(*renderCamera, Render.width, Render.height);

	// Setup the deffered output creating a bigger texture if necessary.
	setupDeferredOutput();

	// Set scissor rect to all image by default.
	D3D11_RECT rects[1] = { 0, 0, Render.width, Render.height };
	Render.ctx->RSSetScissorRects(1, rects);

	// Choose the render method for rendering the scene.
	CEntity * camera = h_entity_camera;
	TCompPostFXPerception * postPEff = camera->getComponent<TCompPostFXPerception>();
	if (!postPEff || !postPEff->isActive())
		defaultRendering();
	else
		perceptionRendering();

	// Draw the render debug methods.
	if (renderDebugActive)
		Engine.getModuleManager().renderDebug();

	Engine.getImGuiManager().renderImguiEngineWindow();

}

// Get the camera we have to render from.
bool CModuleRender::fetchRenderCamera() {
	CHandle cameraHandle = TCompCameraManager::getCurrentCamera();
	if (!cameraHandle.isValid()) return false;
	
	// Fetch the entity.
	h_entity_camera = cameraHandle.getOwner();

	// Copy current state of the given camera to the render camera object.
	renderCamera = cameraHandle;
	
	// Get also the culling of this camera, we will execute it.
	CEntity * ent = h_entity_camera;
	cullingCamera = ent->getComponent<TCompCulling>();

	// Here we are updating the Viewport of the given camera
	renderCamera->setViewport(0, 0, Render.width, Render.height);

	// Set the camera to be used by the render manager. It will use it to use it's cull component if it has one.
	// It's important to call activate camera before calling the renderManager render method.
	CRenderManager::get().setEntityCamera(h_entity_camera);

	return true;
}

// Update bones values to their respective constant buffer.
void CModuleRender::uploadSkinMatricesToGPU() {
	PROFILE_FUNCTION("uploadSkinMatricesToGPU");
  getObjectManager<TCompSkeleton>()->forEach([](TCompSkeleton* cs) {
    cs->updateCtesBones();
	});
}

// Sets the defferred output. If the width changes, it will also update the defferred texture to adapt to it.
bool CModuleRender::setupDeferredOutput() {
	assert(deferred_output);
	if (deferred_output->getWidth() != Render.width || deferred_output->getHeight() != Render.height) {
		if (!deferred_output->create("g_deferred_output.dds", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN, true))
			return false;
	}
	return true;
}

// Default rendering.
void CModuleRender::defaultRendering() {
	PROFILE_FUNCTION("Render Default");
	CGpuScope gpu_scope("Render default");

	deferred->renderDefault(this, deferred_output, h_entity_camera);
}

// Change for something better in the future once we start
// working on a better rendering, not very optimized.
void CModuleRender::perceptionRendering() {
	PROFILE_FUNCTION("Render Perception");
	CGpuScope gpu_scope("Render perception");

	deferred->renderPerception(this, deferred_output, h_entity_camera);
}

CRenderToTexture * CModuleRender::applyPostProcessing(CRenderToTexture * deferred_output, Vector4 & clearColorBackBuffer) {
	CRenderToTexture * curr_rt = deferred_output;
	CEntity * cameraEntity = h_entity_camera;

	TCompPostFXBlur * render_blur = cameraEntity->getComponent<TCompPostFXBlur>();
	if (render_blur)
		curr_rt = render_blur->apply(curr_rt);

	// Render focus requires render blur to be enabled also
	TCompPostFXFocus * render_focus = cameraEntity->getComponent<TCompPostFXFocus>();
	if (render_focus && render_blur && render_blur->isActive() && render_focus->isActive())
		curr_rt = render_focus->apply(deferred_output, curr_rt);

	return curr_rt;
}

CRenderToTexture * CModuleRender::applyFinalPostProcessing(CDeferredRenderer * renderer, CRenderToTexture * deferred_output, CRenderToTexture * rt_acc_depth, Vector4 & clearColorBackBuffer) {
	CRenderToTexture * curr_rt = deferred_output;
	CEntity * cameraEntity = h_entity_camera;

	// Light scattering.
	TCompPostFXLightScattering * light_scattering = cameraEntity->getComponent<TCompPostFXLightScattering>();
	if (light_scattering)
		curr_rt = light_scattering->apply(renderer, curr_rt, rt_acc_depth);

	TCompPostFXBloom * render_bloom = cameraEntity->getComponent<TCompPostFXBloom>();
	if (render_bloom)
		render_bloom->generateHighlights(curr_rt);

	TCompPostFXMotionBlur * motion_blur = cameraEntity->getComponent<TCompPostFXMotionBlur>();
	if (motion_blur)
		curr_rt = motion_blur->apply(curr_rt);

	// Apply the bloom.
	if (render_bloom)
		render_bloom->apply("default");

	// Fog post processing.
	TCompPostFXFog * postFXFog = cameraEntity->getComponent<TCompPostFXFog>();
	if (postFXFog)
		curr_rt = postFXFog->apply(curr_rt);

	TCompPostFXColorGrading * color_grading = cameraEntity->getComponent<TCompPostFXColorGrading>();
	if (color_grading)
		color_grading->activate();
	else {
		ctes_shared.GlobalLUTAmount = 0.f;
		ctes_shared.updateGPU();
	}

	//Vignetting post processing.
	TCompPostFXVignetting * postFXVignetting = cameraEntity->getComponent<TCompPostFXVignetting>();
	if (postFXVignetting)
		curr_rt = postFXVignetting->apply(curr_rt);

	// Burning post processing.
	TCompPostFXBurningFade * postFXBurningFade = cameraEntity->getComponent<TCompPostFXBurningFade>();
	if (postFXBurningFade)
		curr_rt = postFXBurningFade->apply(curr_rt);

	// Black bars.
	TCompPostFXBlackBars * blackScreenBars = cameraEntity->getComponent<TCompPostFXBlackBars>();
	if (blackScreenBars) {
		blackScreenBars->applyScissor(Render.width, Render.height);
		clearColorBackBuffer = blackScreenBars->getBarColors();
	}

	return curr_rt;
}

// Applies post processing methods to the base image.
CRenderToTexture * CModuleRender::applyPostProcessingPerceptionNormalObjects(CRenderToTexture * deferred_output){
	CRenderToTexture * curr_rt = deferred_output;

	CEntity * cameraEntity = h_entity_camera;

	// Blur post processing.
	TCompPostFXBlur * render_blur = cameraEntity->getComponent<TCompPostFXBlur>();
	if (render_blur)
		curr_rt = render_blur->apply(curr_rt);

	// Render focus requires render blur to be enabled also
	TCompPostFXFocus * render_focus = cameraEntity->getComponent<TCompPostFXFocus>();
	if (render_focus && render_blur && render_blur->isActive() && render_focus->isActive())
		curr_rt = render_focus->apply(deferred_output, curr_rt);

	// Perception post processing.
	TCompPostFXPerception * postFXPerception = cameraEntity->getComponent<TCompPostFXPerception>();
	if (postFXPerception)
		curr_rt = postFXPerception->applyColoring(curr_rt);

	// Fog post processing.
	TCompPostFXFog * postFXFog = cameraEntity->getComponent<TCompPostFXFog>();
	if (postFXFog)
		curr_rt = postFXFog->apply(curr_rt);

	// Outlines.
	if (postFXPerception)
		curr_rt = postFXPerception->applyOutlineScenario(curr_rt);

	return curr_rt;
}

// Applies post processing methods to the base image.
void CModuleRender::applyFinalPostProcessingPerceptionNormalObjects(CRenderToTexture * curr_rt, const std::string & bloomToApply) {
	CEntity * cameraEntity = h_entity_camera;

	// Bloom postprocessing.
	TCompPostFXBloom * render_bloom = cameraEntity->getComponent<TCompPostFXBloom>();
	if (render_bloom)
		render_bloom->generateHighlights(curr_rt); // Only get the highlights.

	// Bloom postprocessing.
	if (render_bloom)
		render_bloom->apply(bloomToApply); // Apply the highlights now.
}

// Applies post processing methods to a perception category.
CRenderToTexture * CModuleRender::applyPostProcessingPerceptionCategory(CRenderToTexture * deferred_output, eRenderCategory perceptionCategory) {
	CRenderToTexture * curr_rt = deferred_output;
	CEntity * cameraEntity = h_entity_camera;

	// Bloom postprocessing.
	TCompPostFXBloom * render_bloom = cameraEntity->getComponent<TCompPostFXBloom>();
	if (render_bloom)
		render_bloom->generateHighlights(curr_rt); // Only get the highlights.

	return curr_rt;
}

void CModuleRender::applyFinalPostProcessingPerceptionCategory(const std::string & bloomToApply) {
	CEntity * cameraEntity = h_entity_camera;
	
	// Bloom postprocessing.
	TCompPostFXBloom * render_bloom = cameraEntity->getComponent<TCompPostFXBloom>();
	if (render_bloom)
		render_bloom->apply(bloomToApply); // Apply the highlights now.
}

// Applies post processing methods to a particle
CRenderToTexture * CModuleRender::applyPostProcessingPerceptionParticles(CRenderToTexture * deferred_output) {
	CRenderToTexture * curr_rt = deferred_output;
	CEntity * cameraEntity = h_entity_camera;

	// Bloom postprocessing.
	TCompPostFXBloom * render_bloom = cameraEntity->getComponent<TCompPostFXBloom>();
	if (render_bloom) {
		render_bloom->generateHighlights(curr_rt); // Only get the highlights.
	}

	return curr_rt;
}

CRenderToTexture * CModuleRender::applyFinalPostProcessingPerceptionParticles(CRenderToTexture * deferred_output, const std::string & bloomToApply) {
	CRenderToTexture * curr_rt = deferred_output;
	CEntity * cameraEntity = h_entity_camera;

	// Bloom postprocessing.
	TCompPostFXBloom * render_bloom = cameraEntity->getComponent<TCompPostFXBloom>();
	if (render_bloom)
		render_bloom->apply(bloomToApply); // Apply the highlights now.

	return curr_rt;
}

// Applies post processing methods for generating the outlines of each category.
CRenderToTexture * CModuleRender::applyPostProcessingPerceptionOutlines(CRenderToTexture * deferred_output, CRenderToTexture * rt_acc_depth, CRenderToTexture * rt_perception_mask, Vector4 & clearColorBackBuffer){
	CRenderToTexture * curr_rt = deferred_output;
	CEntity * cameraEntity = h_entity_camera;

	// Perception category outlines.
	TCompPostFXBloom * bloom = cameraEntity->getComponent<TCompPostFXBloom>();
	TCompPostFXPerception * perception = cameraEntity->getComponent<TCompPostFXPerception>();
	if (perception) {
		{
			PROFILE_FUNCTION("PostFx - Outlines categories");
			CGpuScope gpu_scope("Outlines categories rendering");

			CRenderToTexture * outlines = nullptr;

			// Activate the perception mask where we have all the information of the rendered objects.
			rt_perception_mask->activate(TS_DEFERRED_PERCEPTION_MASK);

			{
				PROFILE_FUNCTION("PostFx - Outlines categories player");
				CGpuScope gpu_scope("Outlines categories player rendering");
				outlines = perception->applyOutlineCategory(curr_rt, rt_acc_depth, eRenderCategory::CATEGORY_PERCEPTION);
				if (outlines && bloom) {
					bloom->generateHighlights(outlines);
					bloom->apply("perception_player");
				}
			}

			{
				PROFILE_FUNCTION("PostFx - Outlines categories objects");
				CGpuScope gpu_scope("Outlines categories objects rendering");
				outlines = perception->applyOutlineCategory(curr_rt, rt_acc_depth, eRenderCategory::CATEGORY_PERCEPTION_OBJECTS);
				if (outlines && bloom) {
					bloom->generateHighlights(outlines);
					bloom->apply("perception_objects");
				}
			}

			{
				PROFILE_FUNCTION("PostFx - Outlines categories enemies");
				CGpuScope gpu_scope("Outlines categories enemies rendering");
				outlines = perception->applyOutlineCategory(curr_rt, rt_acc_depth, eRenderCategory::CATEGORY_PERCEPTION_ENEMIES);
				if (outlines && bloom) {
					bloom->generateHighlights(outlines);
					bloom->apply("perception_enemies");
				}
			}
		}
	}

	return curr_rt;
}

// Applies post processing methods to the complete image, including previous post processing methods.
CRenderToTexture * CModuleRender::applyFinalPostProcessingPerception(CDeferredRenderer * renderer, CRenderToTexture * deferred_output, CRenderToTexture * rt_acc_depth, CRenderToTexture * rt_perception_mask, Vector4 & clearColorBackBuffer) {
	CRenderToTexture * curr_rt = deferred_output;
	CEntity * cameraEntity = h_entity_camera;

	// Use the full depth with post processing objects.
	rt_acc_depth->activate(TS_DEFERRED_LINEAR_DEPTH);

	// Motion blur.
	TCompPostFXMotionBlur * motion_blur = cameraEntity->getComponent<TCompPostFXMotionBlur>();
	if (motion_blur)
		curr_rt = motion_blur->apply(curr_rt);

	// Light scattering.
	TCompPostFXLightScattering * light_scattering = cameraEntity->getComponent<TCompPostFXLightScattering>();
	if (light_scattering)
		curr_rt = light_scattering->apply(renderer, curr_rt, rt_acc_depth);

	// Perception post processing wave.
	TCompPostFXPerception * perception = cameraEntity->getComponent<TCompPostFXPerception>();
	if (perception)
		curr_rt = perception->apply(curr_rt);

	//Vignetting post processing.
	TCompPostFXVignetting * postFXVignetting = cameraEntity->getComponent<TCompPostFXVignetting>();
	if (postFXVignetting)
		curr_rt = postFXVignetting->apply(curr_rt);

	// Color grading.
	TCompPostFXColorGrading * color_grading = cameraEntity->getComponent<TCompPostFXColorGrading>();
	if (color_grading)
		color_grading->activate();
	else {
		ctes_shared.GlobalLUTAmount = 0.f;
		ctes_shared.updateGPU();
	}

	// Burning post processing.
	TCompPostFXBurningFade * postFXBurningFade = cameraEntity->getComponent<TCompPostFXBurningFade>();
	if (postFXBurningFade)
		curr_rt = postFXBurningFade->apply(curr_rt);

	// Black bars.
	TCompPostFXBlackBars * blackScreenBars = cameraEntity->getComponent<TCompPostFXBlackBars>();
	if (blackScreenBars) {
		blackScreenBars->applyScissor(Render.width, Render.height);
		clearColorBackBuffer = blackScreenBars->getBarColors();
	}

	return curr_rt;
}

// Apply antialiasing.
CRenderToTexture * CModuleRender::applyAntialiasing(CRenderToTexture * deferred_output) {
	CRenderToTexture * curr_rt = deferred_output;
	CEntity * cameraEntity = h_entity_camera;

	// Apply antialiasing.
	TCompPostFXAA * fxaa = cameraEntity->getComponent<TCompPostFXAA>();
	if (fxaa)
		curr_rt = fxaa->apply(curr_rt);

	return curr_rt;
}

// Render UI section using an ortho camera
// Vertical size is 1
// Horizontal size is aspect ratio. Top/left is 0,0
void CModuleRender::renderUI() {
	CGpuScope gpu_trace("UI Rendering");
	PROFILE_FUNCTION("CModuleRender::renderUI");
	renderCameraOrtho.setUIProjection(false, 0.0f, (float)Render.width / (float)Render.height, 0.f, 1.0f, -1.0f, 1.0f);
	activateCamera(renderCameraOrtho, Render.width, Render.height);
	activateUICts();
	CRenderManager::get().render(eRenderCategory::CATEGORY_UI);
	EngineUI.render();
}