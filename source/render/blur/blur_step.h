#ifndef INC_RENDER_BLUR_STEP_H_
#define INC_RENDER_BLUR_STEP_H_

class CRenderToTexture;
class CTechnique;
class CMesh;
class CTexture;

/* This struct performs a step of the blur process by downsampling the texture
by half and applying a blur to it. */
class CBlurStep {
private:
	CRenderToTexture* rt_half_y = nullptr;
	CRenderToTexture* rt_output = nullptr;    // half x & y
	
	int   xres = 0, yres = 0;
	const CTechnique* tech = nullptr;
	const CMesh*      mesh = nullptr;

	// Renders the image with the given weights.
	void applyBlur(float dx, float dy);

public:
	bool create(const char * name, int in_xres, int in_yres, DXGI_FORMAT fmt);
	// Returns a blurred texture of the original input with half the resolution.
	CRenderToTexture * apply(CRenderToTexture * input, float global_distance, const Vector4 & distances, const Vector4 & weights);
	CRenderToTexture * getRTOutput() { return rt_output; }
};

#endif
