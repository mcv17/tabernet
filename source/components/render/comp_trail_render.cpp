#include "mcv_platform.h"
#include "comp_trail_render.h"
#include "components/common/comp_render.h"
#include "engine.h"
#include "render/textures/material.h"
#include "render/render_manager.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("trail_render", TCompTrailRender);

TCompTrailRender::~TCompTrailRender() {
	if (_mesh) {
		_mesh->destroy();
		delete _mesh;
	}
}

void TCompTrailRender::registerMsgs() {
	DECL_MSG(TCompTrailRender, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompTrailRender, TMsgEntitiesGroupCreated, onGroupCreated);
}

void TCompTrailRender::declareInLua() {
	EngineScripting.declareComponent<TCompTrailRender>
		(
		"TCompTrailRender",
		"lockPosition", sol::overload
		(
			sol::resolve<void()>(&TCompTrailRender::lockPosistion),
			sol::resolve<void(const Vector3&)>(&TCompTrailRender::lockPosistion)
		),
		"unlockPosition", &TCompTrailRender::unlockPosistion,
		"attatchToEntity", sol::overload
			(
				sol::resolve<void(CHandle)>(&TCompTrailRender::attachToEntity),
				sol::resolve<void(CHandle, bool)>(&TCompTrailRender::attachToEntity)
			)
		);
}

void TCompTrailRender::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Text("Vertices start: %d", _vertsArrayStart);
	ImGui::Text("Vertices end: %d", _vertsArrayEnd);
	ImGui::Text("Keyframe start: %d", _keyFrameArrayStart);
	ImGui::Text("Keyframe end: %d", _keyFrameArrayEnd);

	if (ImGui::TreeNode("Vertices thickness")) {
		for (auto& vert : _vertsToGPU) {
			ImGui::Text("%f", vert.thickness);
		}

		ImGui::TreePop();
	}
}

void TCompTrailRender::load(const json & j, TEntityParseContext & ctx) {
	if (j.count("entity"))
		_attachedEntityName = j["entity"];
	else
		setActive(false);
	_material = j.value("material", _material);
	_minVertexDistance = j.value("minimum_vertex_distance", _minVertexDistance);
	_minDistanceTime = j.value("minimum_vertex_time", _minDistanceTime);
	_lifetime = j.value("lifetime", _lifetime);
	_offset = loadVector3(j, "offset");
	_minDistanceTimer.setTime(_minDistanceTime);
	_isBillboard = j.value("billboard", _isBillboard);
	_dieWithEntity = j.value("die_with_entity", _dieWithEntity);
	_waitTrailToEndAfterDeath = j.value("wait_trail_to_end_after_death", _waitTrailToEndAfterDeath);
	_fadeOutTimer.setTime(j.value("fade_out_time", _fadeOutTimer.initialTime()));
	if (j.count("locked") > 0 && j["locked"] == true)
		lockPosistion();

	auto& interpolators = Interpolator::InterpolatorManager::Instance();
	_uvInterpolator = interpolators.getInterpolatorAsLambda(j.value("uv_interpolator", "linear"));

	// Color
	if (j.count("color")) {
		auto jColor = j["color"];
		if (jColor.is_string()) {
			Vector4 color = loadVector4(j, "color");
			_colorGradient.push_back(std::pair(0.0f, color));
			_colorGradient.push_back(std::pair(1.0f, color));
		}
		else if (jColor.is_array()) {
			float pct;
			Vector4 color;
			for (auto& jElem : jColor) {
				pct = jElem["percentage"];
				color = loadVector4(jElem, "color");
				_colorGradient.push_back(std::pair(pct, color));
			}
			std::sort(_colorGradient.begin(), _colorGradient.end(), [](const std::pair<float, Vector4>& i1, const std::pair<float, Vector4>& i2) { return (i1.first > i2.first); });
			if (_colorGradient.back().first > 0.0f)
				_colorGradient.push_back(std::pair(0.0f, _colorGradient.back().second));
			if (_colorGradient[0].first < 1.0f)
				_colorGradient.insert(_colorGradient.begin(), std::pair(1.0f, _colorGradient[0].second));
		}
	}
	else {
		_colorGradient.push_back(std::pair(1.0f, Vector4(1)));
		_colorGradient.push_back(std::pair(0.0f, Vector4(1)));
	}

	// Tickness
	if (j.count("thickness")) {
		auto jThickness = j["thickness"];
		float thickness;
		TThicknessInterpolator interp;
		if (jThickness.is_number()) {
			thickness = jThickness;
			interp.interpolator = interpolators.getInterpolatorAsLambda("linear");
			interp.start = 0.0f;
			interp.end = 1.0f;
			interp.startValue = thickness / 2.0f;
			interp.endValue = interp.startValue;
			_thicknessGradient.push_back(interp);
		}
		else if (jThickness.is_array()) {
			float startValue, endValue;
			for (auto& jElem : jThickness) {
				interp.interpolator = interpolators.getInterpolatorAsLambda(jElem["interpolator"]);
				interp.start = jElem["start"];
				interp.end = jElem["end"];
				interp.startValue = jElem["start_value"];
				interp.endValue = jElem["end_value"];
				interp.startValue /= 2.0f;
				interp.endValue /= 2.0f;
				_thicknessGradient.push_back(interp);
			}
			std::sort(_thicknessGradient.begin(), _thicknessGradient.end(), [](const TThicknessInterpolator& i1, const TThicknessInterpolator& i2) { return (i1.start > i2.start); });
			if (_thicknessGradient.back().start > 0.0f) {
				interp.interpolator = interpolators.getInterpolatorAsLambda("linear");
				interp.start = 0.0f;
				interp.end = _thicknessGradient.back().start;
				interp.startValue = _thicknessGradient.back().startValue;
				interp.endValue = interp.startValue;
				_thicknessGradient.push_back(interp);
			}
			if (_thicknessGradient[0].end < 1.0f) {
				interp.interpolator = interpolators.getInterpolatorAsLambda("linear");
				interp.start = _thicknessGradient[0].end;
				interp.end = 1.0f;
				interp.startValue = _thicknessGradient[0].endValue;
				interp.endValue = interp.startValue;
				_thicknessGradient.insert(_thicknessGradient.begin(), interp);
			}
		}
	}
	else
		assert(false && "Trail needs thickness config");

	// Calculate internals
	_maxVertCount = std::max(20, static_cast<int>((_lifetime / _minDistanceTime) * 3.0f));
	if (_maxVertCount % 2 != 0) _maxVertCount++;
	_vertsInCPU.resize(_maxVertCount);
	_vertsToGPU.resize(_maxVertCount);
	_keyFrameArrayMaxSize = _maxVertCount / 2 + static_cast<int>((_lifetime / _keyFrameFreqTime) * 2.5f);
	_removalKeyFrames.resize(_keyFrameArrayMaxSize);
}

void TCompTrailRender::update(float dt) {
	// Initialize attached transform reference
	Vector3 pos;
	Quaternion rotation;
	TCompTransform* transform = nullptr;

	if (_isFadingOut) {
		_globalAlpha = Maths::clamp(_fadeOutTimer.remaining() / _fadeOutTimer.initialTime(), 0.0f, 1.0f);
	}

	// We execute getTransform even when transform locked to look if entity is dead
	if (!getTransform(&transform)) return;

	if (_isTransformLocked) {
		pos = _storedPosition;
		rotation = Quaternion::Identity;
	}
	else {
		pos = transform->getPosition();
		rotation = transform->getRotation();
		pos += DirectX::XMVector3Rotate(_offset, rotation);
	}

	int numVerts = getVertCount();

	// Update last vertices data (increase the trail)
	if (_prevPosition != pos) {
		Vector3 motion;
		if (_isBillboard) {
			motion = pos - _prevPairPosition;
			motion.Normalize();
		}
		else {
			motion = DirectX::XMVector3Rotate(Vector3::Down, rotation);
		}

		// Create new vertices if needed
		if (_minDistanceTimer.isFinished() && Vector3::Distance(pos, _prevPairPosition) > _minVertexDistance) {
			addRemovalKeyFrame(_vertsInCPU[getLastVerticesIndex(1)].pos, true);
			createVertexPair(pos);
			numVerts += 2;

			_minDistanceTimer.start();
		}

		// Update last vertices positions (increase the trail)
		if (numVerts == 4) {
			// If only a quad update all the vertices
			for (size_t i = 0; i < 4; i++) {
				auto& vert = _vertsInCPU[getFirstVerticesIndex(i)];
				if (i > 1)
					vert.pos = pos;
				else
					vert.pos = _prevPairPosition;
				vert.motion = motion;
			}
		}
		else {
			// Update only last two vertices
			auto& vert1 = _vertsInCPU[getLastVerticesIndex(1)];
			auto& vert2 = _vertsInCPU[getLastVerticesIndex()];
			vert1.pos = pos;
			vert1.motion = motion;
			vert2.pos = pos;
			vert2.motion = motion;
		}

		_prevPosition = pos;
	}

	if (_keyFrameFreqTimer.isFinished())
		addRemovalKeyFrame(pos, false);

	// Update first vertices data (reduce the trail)
	if (_keyFrameArrayStart != _keyFrameArrayEnd) {
		const auto& keyFrame = _removalKeyFrames[getFirstKeyFramesIndex()];
		auto& curVert = _vertsInCPU[getFirstVerticesIndex()];
		Vector3 newPos = curVert.pos;
		// Look if next keyframe needs to be processed
		if (keyFrame.timestamp <= Time.accumulated_delta) {
			if (keyFrame.isVertex) {
				// Delete vertices if necessary
				removeVertexPair();
				numVerts -= 2;
			}
			else {
				// Force vertex pair to the position defined in the keyframe
				newPos = keyFrame.pos;
				curVert.pos = newPos;
				_vertsInCPU[getFirstVerticesIndex(1)].pos = newPos;
			}
			increaseKeyFrameArrayStart();

		}
		else {
			// Progressive trail remove
			Vector3 removePos = keyFrame.pos - curVert.pos;
			Vector3 removeVelocity = keyFrame.pos - curVert.pos;
			removeVelocity.Normalize();
			removeVelocity *= keyFrame.speed * dt;
			if (removePos.Length() > removeVelocity.Length()) {
				newPos = removeVelocity;
				newPos += curVert.pos;
			}
			else {
				// Correct if removed more than it needs to
				newPos = keyFrame.pos;
			}
			curVert.pos = newPos;
			_vertsInCPU[getFirstVerticesIndex(1)].pos = newPos;

			if (numVerts == 4)
				_prevPairPosition = curVert.pos;
		}
	}

	if (_vertsArrayStart != _vertsArrayEnd) {
		copyVertsToGPUBuffer();

		// Calculate trail total distance from start to finish so we can interpolate color & thickness values
		float totalDistance = 0.0f;
		for (size_t i = 0; i < numVerts - 2; i += 2) {
			totalDistance += Vector3::Distance(_vertsToGPU[i].pos, _vertsToGPU[i + 2].pos);
		}

		// Update to all vertices (UVs, color, thickness etc...)
		size_t colorIdx = 1, thicknessIdx = 0;
		float distance = totalDistance;
		if (totalDistance == 0.0f)
			totalDistance = 1.0f;

		float progression, ratio;
		for (size_t i = 0; i < numVerts; i += 2) {
			progression = distance / totalDistance;

			/* Color */
			if (progression < _colorGradient[colorIdx].first)
				colorIdx++;
			ratio = (progression - _colorGradient[colorIdx].first) / (_colorGradient[colorIdx - 1].first - _colorGradient[colorIdx].first);
			Vector4::Lerp(_colorGradient[colorIdx].second, _colorGradient[colorIdx - 1].second, ratio, _vertsToGPU[i].color);
			_vertsToGPU[i].color.w *= _globalAlpha;
			_vertsToGPU[i + 1].color = _vertsToGPU[i].color;

			/* Thickness */
			if (progression < _thicknessGradient[thicknessIdx].start)
				thicknessIdx++;
			ratio = (progression - _thicknessGradient[thicknessIdx].start) / (_thicknessGradient[thicknessIdx].end - _thicknessGradient[thicknessIdx].start);
			_vertsToGPU[i].thickness = _thicknessGradient[thicknessIdx].interpolate(ratio);
			_vertsToGPU[i + 1].thickness = -_vertsToGPU[i].thickness;

			distance = std::max(0.0f, distance - Vector3::Distance(_vertsToGPU[i].pos, _vertsToGPU[i + 2].pos));

			/* UVs */
			float uvProgression = _uvInterpolator(0.0f, 1.0f, 1 - progression);
			_vertsToGPU[i].uv.y = uvProgression;
			_vertsToGPU[i + 1].uv.x = 1.0f;
			_vertsToGPU[i + 1].uv.y = uvProgression;
		}

		_prevRotation = rotation;

		uploadToGPU();
	}
}

void TCompTrailRender::lockPosistion() {
	TCompTransform* transform = nullptr;
	// we store dieWithEntity so it isn't removed while locking position
	bool dieWithEntity = _dieWithEntity;
	_dieWithEntity = false;
	if (getTransform(&transform)) {
		_storedPosition = transform->getPosition();
		_storedPosition += DirectX::XMVector3Rotate(_offset, transform->getRotation());
	}
	else {
		_storedPosition = Vector3::Zero;
	}
	_dieWithEntity = dieWithEntity;
	_isTransformLocked = true;

}

void TCompTrailRender::lockPosistion(const Vector3 & pos) {
	_storedPosition = pos;
	_isTransformLocked = true;
}

void TCompTrailRender::unlockPosistion() {
	_isTransformLocked = false;
	TCompTransform* transform = nullptr;
	if (!getTransform(&transform)) return;
	size_t idx = getFirstVerticesIndex();
	_vertsInCPU[idx].pos = transform->getPosition();
	_vertsInCPU[idx + 1].pos = transform->getPosition();
	idx = getFirstVerticesIndex(2);
	_vertsInCPU[idx].pos = transform->getPosition();
	_vertsInCPU[idx + 1].pos = transform->getPosition();
	_prevPosition = transform->getPosition();
}

void TCompTrailRender::fadeOut() {
	_fadeOutTimer.start();
	_isFadingOut = true;
}

void TCompTrailRender::attachToEntity(CHandle e) {
	if (e.isValid()) {
		_attachedTransform = CHandle();
		TCompTransform* dummy;
		getTransform(&dummy, e);
	}
}

void TCompTrailRender::attachToEntity(CHandle e, bool dieWithEntity) {
	if (e.isValid()) {
		TCompTransform* dummy;
		_dieWithEntity = false;
		_attachedTransform = CHandle();
		getTransform(&dummy, e);
		_dieWithEntity = dieWithEntity;
	}
}

void TCompTrailRender::createVertexPair(const Vector3& pos) {
	increaseVertArrayEnd();
	increaseVertArrayEnd();
	_prevPairPosition = pos;
}

void TCompTrailRender::removeVertexPair() {
	increaseVertArrayStart(2);
}

void TCompTrailRender::addRemovalKeyFrame(const Vector3 & point, bool isVertex) {
	TRemovalKeyFrame kf;
	kf.pos = point;
	kf.speed = Vector3::Distance(point, _lastKeyFramePos) / _keyFrameFreqTimer.elapsed();
	kf.timestamp = Time.accumulated_delta + _lifetime;
	kf.isVertex = isVertex;
	if (kf.speed > 0 || isVertex) {
		_removalKeyFrames[_keyFrameArrayEnd] = kf;
		increaseKeyFrameArrayEnd();

		_keyFrameFreqTimer.start();
		_lastKeyFramePos = point;
	}
}

void TCompTrailRender::copyVertsToGPUBuffer() {
	memset(&_vertsToGPU[0], 0, sizeof(TMeshTrailRenderData) * _maxVertCount);
	if (_vertsArrayStart < _vertsArrayEnd) {
		std::copy_n(&_vertsInCPU[_vertsArrayStart], _vertsArrayEnd - _vertsArrayStart, &_vertsToGPU[0]);
	}
	else {
		int backSize = _maxVertCount - _vertsArrayStart;
		std::copy_n(&_vertsInCPU[_vertsArrayStart], backSize, &_vertsToGPU[0]);
		if (_vertsArrayEnd > 0)
			std::copy_n(&_vertsInCPU[0], _vertsArrayEnd, &_vertsToGPU[backSize]);
	}
}

void TCompTrailRender::uploadToGPU() {
	size_t numVerts = getVertCount();
	_mesh->destroy();
	_mesh->create(&_vertsToGPU[0], numVerts, sizeof(TMeshTrailRenderData), "PosUvColorTrail", nullptr, 0U, 0U, CMesh::TRIANGLE_STRIP, true, &AABB());
	_mesh->updateVertsFromCPU(_vertsToGPU.data(), sizeof(TMeshTrailRenderData) * numVerts);
}

void TCompTrailRender::increaseVertArrayStart(const size_t amount) {
	_vertsArrayStart += amount;
	_vertsArrayStart = _vertsArrayStart % _maxVertCount;
}

void TCompTrailRender::increaseVertArrayEnd(const size_t amount) {
	_vertsArrayEnd += amount;
	_vertsArrayEnd = _vertsArrayEnd % _maxVertCount;
}

void TCompTrailRender::increaseKeyFrameArrayEnd(const size_t amount) {
	_keyFrameArrayEnd += amount;
	_keyFrameArrayEnd = _keyFrameArrayEnd % _keyFrameArrayMaxSize;
}

void TCompTrailRender::increaseKeyFrameArrayStart(const size_t amount) {
	_keyFrameArrayStart += amount;
	_keyFrameArrayStart = _keyFrameArrayStart % _keyFrameArrayMaxSize;
}

bool TCompTrailRender::getTransform(TCompTransform** transf) {
	if (!_attachedTransform.isValid()) {
		CEntity* attachedEntity = nullptr;
		if (_attachedEntity.isValid())
			attachedEntity = _attachedEntity;
		else
			attachedEntity = getEntityByName(_attachedEntityName);
		
		if (attachedEntity) {
			_attachedTransform = attachedEntity->getComponent<TCompTransform>();
			if (_attachedTransform.isValid()) {
				*transf = _attachedTransform;
				// If found reference for the first time, initialize some variables
				_prevPosition = (*transf)->getPosition() + _offset;
				_prevPairPosition = _prevPosition;
				_lastKeyFramePos = _prevPosition;
			}
			else return false;
		}
		else {
			if (_dieWithEntity) {
				if (_waitTrailToEndAfterDeath) {
					EngineEntities.setCHandleTimeToLive(getEntity(), _lifetime);
					Vector3 pos = _prevPosition;
					pos += DirectX::XMVector3Rotate(_offset, _prevRotation);
					lockPosistion(pos);
					_fadeOutTimer.start();
					_isFadingOut = true;
				}
				else
					getEntity().destroy();
			}
			return false;
		}
	}
	else
		*transf = _attachedTransform;
	return true;
}

bool TCompTrailRender::getTransform(TCompTransform ** transf, CEntity * entity) {
	if (!_attachedTransform.isValid()) {
		if (entity) {
			_attachedTransform = entity->getComponent<TCompTransform>();
			if (_attachedTransform.isValid()) {
				*transf = _attachedTransform;
				// If found reference for the first time, initialize some variables
				_prevPosition = (*transf)->getPosition() + _offset;
				_prevPairPosition = _prevPosition;
				_lastKeyFramePos = _prevPosition;
			}
			else return false;
		}
		else {
			if (_dieWithEntity) {
				if (_waitTrailToEndAfterDeath) {
					if (_isFadingOut) {
						_globalAlpha = Maths::lerp(1.0f, 0.0f, _fadeOutTimer.elapsed() / _fadeOutTimer.initialTime());
					}
					else {
						EngineEntities.setCHandleTimeToLive(getEntity(), _lifetime);
						Vector3 pos = _prevPosition;
						pos += DirectX::XMVector3Rotate(_offset, _prevRotation);
						lockPosistion(pos);
						_fadeOutTimer.start();
						_isFadingOut = true;
					}
				}
				else
					getEntity().destroy();
			}
			return false;
		}
	}
	else
		*transf = _attachedTransform;
	return true;
}

size_t TCompTrailRender::getVertCount() {
	if (_vertsArrayStart <= _vertsArrayEnd)
		return _vertsArrayEnd - _vertsArrayStart;
	else
		return (_maxVertCount - _vertsArrayStart) + _vertsArrayEnd;
}

size_t TCompTrailRender::getKeyFrameCount() {
	if (_keyFrameArrayStart <= _keyFrameArrayEnd)
		return _keyFrameArrayEnd - _keyFrameArrayStart;
	else
		return (_keyFrameArrayMaxSize - _keyFrameArrayStart) + _keyFrameArrayEnd;
}

size_t TCompTrailRender::getFirstVerticesIndex(const size_t offset) {
	return (_vertsArrayStart + offset) % _maxVertCount;
}

size_t TCompTrailRender::getLastVerticesIndex(const size_t offset) {
	int idx = _vertsArrayEnd - offset - 1;
	if (idx < 0)
		idx += _maxVertCount;
	return idx;
}

size_t TCompTrailRender::getFirstKeyFramesIndex(const size_t offset) {
	return (_keyFrameArrayStart + offset) % _keyFrameArrayMaxSize;
}

size_t TCompTrailRender::getLastKeyFramesIndex(const size_t offset) {
	int idx = _keyFrameArrayEnd - offset;
	if (idx < 0)
		idx += _keyFrameArrayMaxSize;
	return idx;
}

void TCompTrailRender::onEntityCreated(const TMsgEntityCreated & msg) {
	_minDistanceTimer.start();
	_keyFrameFreqTimer.start();

	assert(getComponent<TCompTransform>().isValid() && "Trail render needs a transform");

	_render = getComponent<TCompRender>();

	// Create vertices data
	memset(&_vertsToGPU[0], 0, sizeof(TMeshTrailRenderData) * _maxVertCount);
	TMeshTrailRenderData rd = {};
	for (size_t i = 0; i < 4; i++) {
		_vertsInCPU[i] = rd;
		_vertsToGPU[i] = rd;
	}
	increaseVertArrayEnd(4);

	// Create mesh and setup render
	if (TCompRender* render = _render) {
		auto& parts = render->parts;
		if (parts.empty()) {
			_mesh = new CMesh();
			_mesh->create(&_vertsToGPU[0], _maxVertCount, sizeof(TMeshTrailRenderData), "PosUvColorTrail", nullptr, 0U, 0U, CMesh::TRIANGLE_STRIP, true);
			auto* resourceType = getResourceTypeFor<CMesh>();
			_mesh->setNameAndType(resourceType->getName(), resourceType);

			TCompRender::MeshPart meshPart;
			meshPart.mesh = _mesh;
			meshPart.material = EngineResources.getResource(_material)->as<CMaterial>();
			parts.push_back(meshPart);
			render->unregisterFromInstancing();

			render->updateRenderManager();

			_mesh->updateVertsFromCPU(_vertsToGPU.data(), _maxVertCount * sizeof(TMeshTrailRenderData));
		}
		else setActive(false);
	}
	
}

void TCompTrailRender::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	if (_attachedEntityName != "") {
		_attachedEntity = msg.ctx.findEntityByName(_attachedEntityName);
	}
}
