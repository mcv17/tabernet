#pragma once
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"

class TCompLineRender : public TCompBase {

public:

	struct TLineRenderData {
		Vector3 pos;
		Vector2 uv;
		Vector4 color;
		float thickness;
		Vector3 motion;
	};

	~TCompLineRender();

	static void registerMsgs();

	DECL_SIBLING_ACCESS();

	void renderDebug() {}
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	size_t drawLine(const Vector3& start, const Vector3& end, const float thickness, const Vector4& color = Vector4(1));
	size_t drawLine(const Vector3& start, const Vector3& end, const float thickness, const Vector4& colorStart, const Vector4& colorEnd);
	void removeLine(size_t id);
	void setLinePosition(size_t id, const Vector3& start, const Vector3& end);
	void setLineColor(size_t id, const Vector4& color);
	void setLineColor(size_t id, const Vector4& colorStart, const Vector4& colorEnd);
	void setLineThickness(size_t id, float thickness);

	const TLineRenderData& getLineVertex(size_t id, uint8_t vertex = 0);

private:

	std::vector<TLineRenderData> _vertsToGPU;
	std::vector<TLineRenderData> _vertsInCPU;
	CMesh* _mesh;
	CHandle _render;
	std::string _material = "data/materials/mesh_trail.material";

	size_t _maxLines = 128;
	size_t _vertsUsed = 0;
	bool _hasMeshChanged = false;

	std::list<size_t> _availableQueue;
	size_t _lastAvailable = 0;

	void copyVertsToGPUBuffer();
	void uploadToGPU();

	void onEntityCreated(const TMsgEntityCreated& msg);

};