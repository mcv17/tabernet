#pragma once
#include "components/common/comp_base.h"
#include "utils/time.h"
#include "entity/common_msgs.h"
#include "components/common/comp_transform.h"
#include "geometry/interpolators.h"

class TCompTrailRender : public TCompBase {

public:
	
	~TCompTrailRender();

	static void registerMsgs();
	static void declareInLua();

	DECL_SIBLING_ACCESS();

	void renderDebug() {}
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	void lockPosistion();
	void lockPosistion(const Vector3& pos);
	void unlockPosistion();
	void fadeOut();

	void attachToEntity(CHandle e);
	void attachToEntity(CHandle e, bool dieWithEntity);

private:
	
	struct TMeshTrailRenderData {
		Vector3 pos;
		Vector2 uv;
		Vector4 color;
		float thickness;
		Vector3 motion;
	};

	struct TRemovalKeyFrame {
		Vector3 pos;
		float speed;
		double timestamp;
		bool isVertex;
	};

	struct TThicknessInterpolator {
		std::function<float(float, float, float)> interpolator;
		float start, end;
		float startValue, endValue;

		float interpolate(float ratio) {
			return interpolator(startValue, endValue, ratio);
		}
	};

	std::vector<TMeshTrailRenderData> _vertsToGPU;
	std::vector<TMeshTrailRenderData> _vertsInCPU;
	CMesh* _mesh;
	std::string _material = "data/materials/mesh_trail.material";
	std::string _attachedEntityName;
	CHandle _render, _attachedTransform, _attachedEntity;
	Vector3 _prevPosition;
	Quaternion _prevRotation;
	bool _dieWithEntity = false;
	bool _waitTrailToEndAfterDeath = true;

	float _minVertexDistance = 1.0f;
	float _lifetime = 1.0f;
	int _maxVertCount;

	size_t _vertsArrayStart = 0, _vertsArrayEnd = 0;
	CClock _minDistanceTimer;
	float _minDistanceTime = 0.1f;
	bool _isBillboard = true;

	std::vector<std::pair<float, Vector4>> _colorGradient;
	std::vector<TThicknessInterpolator> _thicknessGradient;
	std::function<float(float, float, float)> _uvInterpolator;

	Vector3 _offset, _prevPairPosition;
	Vector3 _storedPosition;
	bool _isTransformLocked = false;
	float _globalAlpha = 1.0f;
	CClock _fadeOutTimer = CClock(1.0f);
	bool _isFadingOut = false;

	std::vector<TRemovalKeyFrame> _removalKeyFrames;		// First for point, second for timestamp
	size_t _keyFrameArrayStart = 0, _keyFrameArrayEnd = 0;
	size_t _keyFrameArrayMaxSize;
	Vector3 _lastKeyFramePos;
	const float _keyFrameFreqTime = 0.1f;
	CClock _keyFrameFreqTimer = CClock(_keyFrameFreqTime);

	void createVertexPair(const Vector3& pos);
	void removeVertexPair();
	void addRemovalKeyFrame(const Vector3& point, bool isVertex);
	void copyVertsToGPUBuffer();
	void uploadToGPU();

	void increaseVertArrayStart(const size_t amount = 1);
	void increaseVertArrayEnd(const size_t amount = 1);
	void increaseKeyFrameArrayStart(const size_t amount = 1);
	void increaseKeyFrameArrayEnd(const size_t amount = 1);

	bool getTransform(TCompTransform** transf);
	bool getTransform(TCompTransform** transf, CEntity* entity);
	size_t getVertCount();
	size_t getKeyFrameCount();
	size_t getFirstVerticesIndex(const size_t offset = 0);
	size_t getLastVerticesIndex(const size_t offset = 0);
	size_t getFirstKeyFramesIndex(const size_t offset = 0);
	size_t getLastKeyFramesIndex(const size_t offset = 0);

	void onEntityCreated(const TMsgEntityCreated& msg);
	void onGroupCreated(const TMsgEntitiesGroupCreated& msg);

};