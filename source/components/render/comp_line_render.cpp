#include "mcv_platform.h"
#include "comp_line_render.h"
#include "components/common/comp_render.h"
#include "engine.h"
#include "render/textures/material.h"

DECL_OBJ_MANAGER("line_render", TCompLineRender);

TCompLineRender::~TCompLineRender() {
	if (_mesh)
		delete _mesh;
}

void TCompLineRender::registerMsgs() {
	DECL_MSG(TCompLineRender, TMsgEntityCreated, onEntityCreated);
}

void TCompLineRender::copyVertsToGPUBuffer() {
	size_t gpuSize = 0;
	for (size_t i = 0; i < _vertsUsed; i += 4) {
		std::copy_n(&_vertsInCPU[i], 4, &_vertsToGPU[gpuSize]);
		gpuSize += 4;
		memset(&_vertsToGPU[gpuSize], 0, 2 * sizeof(TLineRenderData));
		gpuSize += 2;
	}
}

void TCompLineRender::uploadToGPU() {
	_mesh->updateVertsFromCPU(_vertsToGPU.data(), _vertsToGPU.size() * sizeof(TLineRenderData));

}

void TCompLineRender::onEntityCreated(const TMsgEntityCreated & msg) {
	_render = getComponent<TCompRender>();

	assert(getComponent<TCompTransform>().isValid() && "Line render needs a transform");

	// Create mesh and setup render
	if (TCompRender* render = _render) {
		auto& parts = render->parts;
		if (parts.empty()) {
			_mesh = new CMesh();
			_mesh->create(&_vertsToGPU[0], (_maxLines) * 6 + 4, sizeof(TLineRenderData), "PosUvColorTrail", nullptr, 0U, 0U, CMesh::TRIANGLE_STRIP, true);
			auto* resourceType = getResourceTypeFor<CMesh>();
			_mesh->setNameAndType(resourceType->getName(), resourceType);

			TCompRender::MeshPart meshPart;
			meshPart.mesh = _mesh;
			meshPart.material = EngineResources.getResource(_material)->as<CMaterial>();
			parts.push_back(meshPart);
			render->unregisterFromInstancing();

			render->updateRenderManager();
		}
		else setActive(false);
	}
}

void TCompLineRender::debugInMenu() {}

void TCompLineRender::load(const json & j, TEntityParseContext & ctx) {
	_material = j.value("material", _material);
	_maxLines = j.value("max_lines", _maxLines);
	_vertsInCPU.resize(_maxLines * 4);
	_vertsToGPU.resize(_maxLines * 6 + 4);
}

void TCompLineRender::update(float dt) {
	if (_hasMeshChanged) {
		copyVertsToGPUBuffer();
		uploadToGPU();
		_hasMeshChanged = false;
	}
}

size_t TCompLineRender::drawLine(const Vector3 & start, const Vector3 & end, const float thickness, const Vector4 & color) {
	return drawLine(start, end, thickness, color, color);
}

size_t TCompLineRender::drawLine(const Vector3 & start, const Vector3 & end, const float thickness, const Vector4 & colorStart, const Vector4 & colorEnd) {
	size_t idx;
	if (_availableQueue.empty()) {
		idx = _vertsUsed;
		_vertsUsed += 4;
		assert(_vertsUsed <= _maxLines * 4 && "Number of lines exceeded for this component");
	}
	else {
		idx = _availableQueue.front();
		_availableQueue.pop_front();
	}

	Vector3 motion = end - start;
	motion.Normalize();

	auto* vert = &_vertsInCPU[idx];
	vert->pos = start;
	vert->thickness = thickness;
	vert->color = colorStart;
	vert->motion = motion;
	vert->uv = Vector2(0, 0);
	vert++;
	vert->pos = start;
	vert->thickness = -thickness;
	vert->color = colorStart;
	vert->motion = motion;
	vert->uv = Vector2(1, 0);
	vert++;
	vert->pos = end;
	vert->thickness = thickness;
	vert->color = colorEnd;
	vert->motion = motion;
	vert->uv = Vector2(0, 1);
	vert++;
	vert->pos = end;
	vert->thickness = -thickness;
	vert->color = colorEnd;
	vert->motion = motion;
	vert->uv = Vector2(1, 1);

	_hasMeshChanged = true;
	return idx;
}


void TCompLineRender::removeLine(size_t id) {
	memset(&_vertsInCPU[id], 0, sizeof(TLineRenderData) * 4);
	if (_vertsUsed - 4 == id)
		_vertsUsed -= 4;
	else
		_availableQueue.push_back(id);
	_hasMeshChanged = true;
}

void TCompLineRender::setLinePosition(size_t id, const Vector3 & start, const Vector3 & end) {
	auto* vert = &_vertsInCPU[id];
	vert->pos = start;
	vert++;
	vert->pos = start;
	vert++;
	vert->pos = end;
	vert++;
	vert->pos = end;
	_hasMeshChanged = true;
}

void TCompLineRender::setLineColor(size_t id, const Vector4 & color) {
	setLineColor(id, color, color);
}

void TCompLineRender::setLineColor(size_t id, const Vector4 & colorStart, const Vector4 & colorEnd) {
	auto* vert = &_vertsInCPU[id];
	vert->color = colorStart;
	vert++;
	vert->color = colorStart;
	vert++;
	vert->color = colorEnd;
	vert++;
	vert->color = colorEnd;
	_hasMeshChanged = true;
}

void TCompLineRender::setLineThickness(size_t id, float thickness) {
	auto* vert = &_vertsInCPU[id];
	vert->thickness = thickness;
	vert++;
	vert->thickness = thickness;
	vert++;
	vert->thickness = thickness;
	vert++;
	vert->thickness = thickness;
	_hasMeshChanged = true;
}

const TCompLineRender::TLineRenderData & TCompLineRender::getLineVertex(size_t id, uint8_t vertex) {
	assert(vertex < 4);
	return _vertsInCPU[id + vertex];
}
