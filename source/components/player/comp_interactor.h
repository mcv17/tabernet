#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_health.h"
#include "entity/common_msgs.h"

class TCompInteractor : public TCompBase {

public:
	const static char * INTERACTIBLE_TAG;

	DECL_SIBLING_ACCESS();

	static void registerMsgs();

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void onEntityCreated(const TMsgEntityCreated& msg);

	Vector3 getPosition();
	Vector3 getHalfSize();
	Quaternion getRotation();

	void moveToTarget(const CTransform & target);
	bool interact(float dt);

private:
	physx::PxRigidDynamic * _actor;
	CHandle _interactEntity;	
	Vector4 _tempColor; // TODO: Remove after 1st milestone
	// Button
	bool _showButton = false;
	float _textTargetScale = 2.0f;
	bool _isControllerActive = false;
	std::string _parentWidgetName = "interact";
	std::string _interactWidgetName = "interact_widget";
	std::string _interactXboxWidgetName = "interact_widget_xbox";

	physx::PxRigidDynamic * getActor();

	void onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter);
	void onTriggerExit(const TMsgEntityTriggerExit& trigger_exit);

};