#pragma once

#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/states/state_machine_controller.h"

class TCompTransform;
class TCompPlayerCameraController;
class TCompPlayerCharacterController;
class TCompWeaponManager;
class TCompWeaponsController;
class TCompScythePlayer;
class TCompPerception;
class TCompInputPlayerMovement;
class TCompPlayerAnimator;
class TCompSkeleton;
class TCompInteractor;

class TCompInputPlayerCombat : public StateMachineController{
	DECL_SIBLING_ACCESS();

	//Handles
	std::string playerCamera;
	CHandle cameraTransformHandle;
	CHandle cameraControllerHandle;
	CHandle playerControllerHandle;
	CHandle weaponManagerHandle;
	CHandle scythePlayerHandle;
	CHandle perceptionPlayerHandle;
	CHandle inputPlayerMovementHandle;
	CHandle playerAnimatorHandle;
	CHandle playerSkeletonHandle;

	//Components
	TCompTransform * cameraTransform;
	TCompPlayerCameraController* playerCamController;
	TCompPlayerCharacterController* playerController;
	TCompWeaponManager* weaponManager;
	TCompWeaponsController* wp_contr;
	TCompScythePlayer* scythePlayer;
	TCompPerception* perceptionPlayer;
	TCompInputPlayerMovement* inputPlayerMovement;
	TCompPlayerAnimator * playerAnimator;
	TCompSkeleton* playerSkeleton;

	enum PowerState {NONE, VORTEX, TELEPORT};
	PowerState powerState = NONE;
	//WeaponUp state when shoot, time countdown when not shooting to go back to WeaponDown (Normal)
	float timeToMoveFreely = 2.0f;
	float currentTimeToMoveFreely = 0.0f;

	// Max and min values for camera pitch in different states
	float maxNormalPitch = 1.0f;
	float minNormalPitch = -1.0f;
	float maxCombatPitch = 0.9f;
	float minCombatPitch = -0.6f;

	//We fetch these value from the input_player_movement
	bool isFacingCamera; 
	bool isMoving; 

	//Animation vars - Reloading
	std::string _reloadNormalVarName = "isReloadingNormal";
	std::string _reloadFastVarName = "isReloadingFast";
	std::string _reloadFailVarName = "isReloadingFail";
	std::string _currentReloadVarName = "isReloadingNormal";
	bool _hasTriedActiveReload = false;

	//Animation vars - Change Weapon
	std::string _changeWeaponVarName = "isChangingWeapon";
	std::string _changeWeaponAnimNames[3] = { "ub_change_smg", "ub_change_shotgun", "ub_change_rifle" };
	bool _changedWeaponMidAnimation = false;

	//Animation vars - Scythe
	std::string _scytheVarName = "scytheAttack";
	
	//Animation vars - Knockback
	std::string _knockbackVarName = "knockback";
	
	//Animation vars - Interact
	std::string _interactVarName = "interacting";

	//Functions
	void fetchComponents();

	void handlePerception(float dt);
	bool handleScythe(float dt);
	//Checks if player requests to change weapon. If player requests to change weapon, and is able to, return true.
	bool handleChangeWeapon(float dt);
	//Checks if player requests to reload. If player requests to reload, and is able to, return true and handle the animations + changestate
	bool handleReload(float dt);
	//Checks if the player requests to interact. If it requests to interact, and is able to, return true.
	bool handleInteract(float dt);
	void checkTimeToMoveFreely(float dt);

	//States
	void normal(float dt);
	void weaponUp(float dt);
	void aiming(float dt);
	void scythe(float dt);
	void changeWeapon(float dt);
	void reloading(float dt);
	void interacting(float dt);
	void dead(float dt);

	void checkStateJumpAfterAnimation();

	//Msgs
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onDeadChangeMsg(const TMsgEntityDead & msg);
	void onInputActiveMsg(const TMsgInputControllerStatus & msg);
	void onResurrectionMsg(const TMsgFullRestore & msg); // Used during this milestone to resurrect player on dead. Remove once finished.

	DECL_TCOMP_ACCESS("Player_interactor", TCompInteractor, PlayerInteractor);

public:
	void Init() override;
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void update(float dt);	
	static void registerMsgs();
};