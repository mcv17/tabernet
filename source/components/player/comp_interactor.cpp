#include "mcv_platform.h"
#include "comp_interactor.h"
#include "components/common/comp_render.h"
#include "components/common/comp_tags.h"
#include "engine.h"
#include "components/common/comp_aabb.h"
#include "components/camera/comp_camera.h"
#include "modules/module_camera_mixer.h"

using namespace physx;

const char * TCompInteractor::INTERACTIBLE_TAG = "interactible";

DECL_OBJ_MANAGER("interactor", TCompInteractor)

void TCompInteractor::registerMsgs() {
	DECL_MSG(TCompInteractor, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompInteractor, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompInteractor, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompInteractor, TMsgEntityTriggerExit, onTriggerExit);
}

void TCompInteractor::update(float dt) {

	if (_showButton) {
		CEntity* iEntity = _interactEntity;
		TCompTransform* eTrans = iEntity->getComponent<TCompTransform>();
		TCompTransform* trans = getComponent<TCompTransform>();
		CTransform* cameraT = EngineCameraMixer.getMixerOutputCameraTransform();
		VEC3 targetPos = eTrans->getPosition();
		VEC3 playerPos = trans->getPosition();

		targetPos.y += 1.5f; // height 
		float distance = VEC3::Distance(targetPos, cameraT->getPosition());
		VEC3* screenSpace = nullptr; // Get screen coords from interact prop
		if (CEntity* eCamera = EngineLogicManager.getOutputCamera()) {
			if (TCompCamera* cameraComp = eCamera->getComponent<TCompCamera>()) {
				screenSpace = new VEC3;
				cameraComp->getScreenCoordsOfWorldCoord(targetPos, screenSpace);
			}
		}
		if (EngineInput.isControllerActive()) {
			if (!_isControllerActive) {
				UI::CImage* widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactWidgetName);
				widget->getParams()->visible = false;
				widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactXboxWidgetName);
				widget->getParams()->visible = true;
				_isControllerActive = true;
			}
		}
		else {
			if (_isControllerActive) {
				UI::CImage* widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactWidgetName);
				widget->getParams()->visible = true;
				widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactXboxWidgetName);
				widget->getParams()->visible = false;
				_isControllerActive = false;
			}
		}
		std::string currentWidgetName = _isControllerActive ? _interactXboxWidgetName : _interactWidgetName;
		UI::CImage* widget = EngineUI.getImageWidgetByName(_parentWidgetName, currentWidgetName);
		widget->getParams()->scale.x = (_textTargetScale / distance);
		widget->getParams()->scale.y = widget->getParams()->scale.x;	
		widget->getParams()->position.x = screenSpace->x - 50.f; // ui offset
		widget->getParams()->position.y = screenSpace->y - 50.f;
	}
}

void TCompInteractor::load(const json& j, TEntityParseContext& ctx) {}

void TCompInteractor::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Text("text target scale: ");
	ImGui::SameLine();
	ImGui::DragFloat("%%a", &_textTargetScale, 0.01f, 0.0f, 10.0f);
}

void TCompInteractor::onEntityCreated(const TMsgEntityCreated& msg)
{
	EngineUI.activateWidget(_parentWidgetName);
	UI::CImage* widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactWidgetName);
	widget->getParams()->visible = false;
	widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactXboxWidgetName);
	widget->getParams()->visible = false;
}

Vector3 TCompInteractor::getPosition() {
	PxRigidDynamic * actor = getActor();
	PxTransform pTransf = actor->getGlobalPose();
	return PXVEC3_TO_VEC3(pTransf.p);
}

Vector3 TCompInteractor::getHalfSize() {
	PxRigidDynamic * actor = getActor();
	std::vector<PxShape*> shapes;
	shapes.resize(1);
	actor->getShapes(&shapes[0], 1);
	PxBoxGeometry box;
	shapes[0]->getBoxGeometry(box);
	return PXVEC3_TO_VEC3(box.halfExtents);
}

Quaternion TCompInteractor::getRotation() {
	PxRigidDynamic * actor = getActor();
	PxTransform pTransf = actor->getGlobalPose();
	return PXQUAT_TO_QUAT(pTransf.q);
}

// This method is used to move the interactor kinematic trigger to a position and rotation
void TCompInteractor::moveToTarget(const CTransform & target) {
	PxRigidDynamic * actor = getActor();
	if (!actor) return;
	PxTransform pTransf = actor->getGlobalPose();
	pTransf.p = VEC3_TO_PXVEC3(target.getPosition());
	pTransf.q = QUAT_TO_PXQUAT(target.getRotation());
	actor->setKinematicTarget(pTransf);
}

PxRigidDynamic * TCompInteractor::getActor() {
	if (!_actor) {
		TCompCollider * col = getComponent<TCompCollider>();
		if (!col) return nullptr;
		_actor = static_cast<PxRigidDynamic *>(col->actor);
	}
	return _actor;
}

bool TCompInteractor::interact(float dt) {
	if (_interactEntity.isValid()) {
		CEntity * entity = _interactEntity;
		TCompTags * tags = entity->getComponent<TCompTags>();

		if (tags && tags->hasTag(Utils::getID(INTERACTIBLE_TAG))) {
			TMsgPlayerIsInteracting msg;
			msg.dt = dt;
			CEntity * entity = _interactEntity;
			entity->sendMsg(msg);

			TCompRender* render = entity->getComponent<TCompRender>();
			if (render && render->hasState("normal"))
				render->setCurrentState("normal");

			_showButton = false;

			UI::CImage* widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactWidgetName);
			widget->getParams()->visible = false;
			widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactXboxWidgetName);
			widget->getParams()->visible = false;

			return true;
		}
		else 
			_interactEntity = CHandle();
	}

	return false;
}

void TCompInteractor::onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter) {
	CEntity * entity = trigger_enter.h_entity;
	if (!entity) return;

	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags && tags->hasTag(Utils::getID(INTERACTIBLE_TAG))) {
		entity->sendMsg(TMsgPlayerInteractionTriggerEnter());
		_interactEntity = entity;
		_showButton = true;

		std::string currentWidgetName = EngineInput.isControllerActive() ? _interactXboxWidgetName : _interactWidgetName;
		UI::CImage* widget = EngineUI.getImageWidgetByName(_parentWidgetName, currentWidgetName);
		widget->getParams()->visible = true;
	}
}

void TCompInteractor::onTriggerExit(const TMsgEntityTriggerExit& trigger_exit) {
	CEntity * entity = trigger_exit.h_entity;
	if (!entity) return;

	TCompTags * tags = entity->getComponent<TCompTags>();

	if (tags && tags->hasTag(Utils::getID(INTERACTIBLE_TAG))) {
		entity->sendMsg(TMsgPlayerInteractionTriggerExit());
		_interactEntity = CHandle();
		_showButton = false;

		UI::CImage* widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactWidgetName);
		widget->getParams()->visible = false;
		widget = EngineUI.getImageWidgetByName(_parentWidgetName, _interactXboxWidgetName);
		widget->getParams()->visible = false;

	}
}