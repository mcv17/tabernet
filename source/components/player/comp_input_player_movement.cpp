#include "mcv_platform.h"
#include "comp_input_player_movement.h"

#include "components/controllers/comp_player_character_controller.h"

#include "components/common/comp_transform.h"
#include "components/common/comp_render.h"

#include "components/camera/comp_camera.h"
#include "components/camera/comp_player_camera_controller.h"

#include "components/animations/comp_player_animator.h"

#include "engine.h"
#include "entity/entity_parser.h"

#include "components/ui/comp_lose_ui.h"

DECL_OBJ_MANAGER("input_player_movement", TCompInputPlayerMovement);

//General functions
void TCompInputPlayerMovement::load(const json & j, TEntityParseContext& ctx) {
	playerCamera = j.value("cameraFollowing", playerCamera);
	facingCameraThreshold = DEG2RAD(j.value("facingCamerathreshold", facingCameraThreshold));
	Init();
}

void TCompInputPlayerMovement::registerMsgs() {
	DECL_MSG(TCompInputPlayerMovement, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompInputPlayerMovement, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompInputPlayerMovement, TMsgFullRestore, onResurrectionMsg);  // Used during this milestone to resurrect player on dead. Remove once finished.
	DECL_MSG(TCompInputPlayerMovement, TMsgEntityDead, onDeadChangeMsg);
	DECL_MSG(TCompInputPlayerMovement, TMsgInputControllerStatus, onInputActiveMsg);
}

void TCompInputPlayerMovement::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Spacing();
	ImGui::Text("Current state: ");
	ImGui::Text(state.c_str());
	ImGui::Spacing();

	float thresholdCamShoot = RAD2DEG(facingCameraThreshold);
	ImGui::DragFloat("Shoot threshold", &thresholdCamShoot, 0.001f, 0.f, 360.f);
	if (thresholdCamShoot != RAD2DEG(facingCameraThreshold))
		facingCameraThreshold = DEG2RAD(thresholdCamShoot);
}

void TCompInputPlayerMovement::update(float dt)
{
	fetchComponents();
	
	facingCamera = isFacingCamera(playerTransform, cameraTransform, facingCameraThreshold);
	moving = false;
	isSprinting = false;

	// TEMP_ADDED - updating the change between states
	assert(!state.empty());
	assert(statemap.find(state) != statemap.end());

	if (locked) {
		if (lockedDuration != -1.0f) {
			lockedDuration -= dt;
			if (lockedDuration <= 0.0f) {
				locked = false;
				lockedDuration = -1.0f;
			}
		}
		return;
	}

	// this is a trusted jump as we've tested for coherence in ChangeState
	(this->*statemap[state])(dt);
}

void TCompInputPlayerMovement::Init() {
	AddState("Normal", (statehandler)&TCompInputPlayerMovement::normal);
	AddState("Sprint", (statehandler)&TCompInputPlayerMovement::sprint);
	AddState("Dead", (statehandler)&TCompInputPlayerMovement::dead);
	ChangeState("Normal");
}

//States
void TCompInputPlayerMovement::normal(float dt) {
	handleMovement(dt);
}

void TCompInputPlayerMovement::sprint(float dt) {
	isSprinting = true;

	// Movement
	bool wantToSprint = false;
	VEC3 movementDelta;
	if (EngineInput["forward"].value > deadZone || currentTimeBeforeSprintingEnds >= 0.0f) {
		// Only sprint if moving forward. Once animations are done,
		// we should check if the value is true at the end of the sprinting.
		if (EngineInput["sprint"] && EngineInput["forward"].value > deadZone) {
			wantToSprint = true;
			currentTimeBeforeSprintingEnds = timeBeforeSprintingEnds;
		}

		movementDelta += VEC3::Forward * abs(EngineInput["forward"].value);
		moving = true;
	}

	// Set the movement variable in the animator.
	playerAnimator->setVariable("xSpeed", movementDelta.x);
	playerAnimator->setVariable("zSpeed", -(movementDelta.z - 0.5f));
	playerAnimator->setVariable("weaponUp", false);


	currentTimeBeforeSprintingEnds -= dt;
	if (!wantToSprint && currentTimeBeforeSprintingEnds < 0.0f) { // This controls for both, not moving and not sprinting but moving.
		currentTimeToStartSprintingAgain = timeToStartSprintingAgain;
		ChangeState(lastState);
		playerCamController->ChangeState("Normal");
		return;
	}
	playerController->rotateAndMoveInDir(cameraTransform, movementDelta * playerController->getPlayerSpeedSprinting(), playerController->getRotationSpeedSprinting(), dt);
}

void TCompInputPlayerMovement::dead(float dt) {
	if (deadButtonsClock.isStarted() && deadButtonsClock.isFinished()) {
		deadButtonsClock.stop();
		EngineUI.activateWidget("game_over_buttons");
	}
}

//Private functions
void TCompInputPlayerMovement::fetchComponents() {
	// checkPlayerCamera
	if (!cameraTransformHandle.isValid()) {
		CEntity * ent = getEntityByName(playerCamera.c_str());
		cameraTransformHandle = ent->getComponent<TCompTransform>();
	}
	if (!cameraControllerHandle.isValid()) {
		CEntity * ent = getEntityByName(playerCamera.c_str());
		cameraControllerHandle = ent->getComponent<TCompPlayerCameraController>();
	}
	if (!cameraHandle.isValid()) {
		CEntity * ent = getEntityByName(playerCamera.c_str());
		cameraHandle = ent->getComponent<TCompCamera>();
	}

	cameraTransform		= cameraTransformHandle; // Get the camera's transform.
	playerCamController	= cameraControllerHandle; // Get the camera that follows the player.
	playerController	= playerControllerHandle;
	playerRender		= playerRenderHandle;
	playerTransform		= playerTransformHandle;
	playerAnimator = playerAnimatorHandle;
}

void TCompInputPlayerMovement::fetchAliveColour() {
	if (aliveColour == VEC4(0, 0, 0, 0)) {
		if (playerRender) {
			VEC4 renderColour = playerRender->getColor();
			aliveColour = renderColour;
		}
	}
}

bool TCompInputPlayerMovement::isFacingCamera(TCompTransform * playerTransform, TCompTransform * cameraTransform, float threshold) {
	float yawObj = playerTransform->getDeltaYawToAimTo(playerTransform->getPosition() + cameraTransform->getFront());
	if (abs(yawObj) <= threshold)
		return true;
	return false;
}

void TCompInputPlayerMovement::handleMovement(float dt) {
	if (!playerTransform || !cameraTransform) return;

	VEC3 playerPosition = playerTransform->getPosition();
	VEC3 playerFront = playerTransform->getFront();

	// Movement
	bool wantToSprint = false;
	VEC3 movementDelta;
	// Only sprint if moving forward.
	if (EngineInput["forward"].value > deadZone && EngineInput["sprint"])
		wantToSprint = true;

	if (EngineInput.isControllerActive()) {
		movementDelta.x = EngineInput["right"].value;
		movementDelta.z = -EngineInput["backwards"].value;
		movementDelta.Normalize();
		if (combatState != NORMAL && movementDelta.z > 0.0f)
			movementDelta.z *= playerController->getPlayerSpeedMultiplierBackwards();
	}
	else {
		VEC2 input;
		input.x = -EngineInput["left"].value + EngineInput["right"].value;
		input.y = EngineInput["backwards"].value;
		if (combatState != NORMAL)
			input.y *= playerController->getPlayerSpeedMultiplierBackwards();
		input.y -= EngineInput["forward"].value;

		float angle = atan2f(input.y, input.x);
		movementDelta.x = cosf(angle) * abs(input.x);
		movementDelta.z = sinf(angle) * abs(input.y);
	}

	if (movementDelta.Length() > deadZone)
		moving = true;

	//Normal
	if (moving && combatState == NORMAL && !wantToSprint)
		playerController->rotateAndMoveFreely(cameraTransform, movementDelta * playerController->getPlayerSpeedNormal(), playerController->getRotationSpeedNormal(), dt);

	//Normal but wanting to sprint
	if (moving && combatState == NORMAL && wantToSprint)
		playerController->rotateAndMoveInDir(cameraTransform, movementDelta * playerController->getPlayerSpeedNormal(), playerController->getRotationSpeedNormal(), dt);

	//WeaponUp
	if (moving && combatState == WEAPONUP)
		playerController->rotateAndMoveInDir(cameraTransform, movementDelta * playerController->getPlayerSpeedNormal(), playerController->getRotationSpeedNormal(), dt);
	else if (!moving && combatState == WEAPONUP)
		playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedAiming(), dt);
	
	//Aiming
	if (moving && combatState == AIMING)
		playerController->rotateAndMoveInDir(cameraTransform, movementDelta * playerController->getPlayerSpeedAiming(), playerController->getRotationSpeedAiming(), dt);
	else if (!moving && combatState == AIMING)
		playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedAiming(), dt);

	if (combatState == NORMAL) {
		float dist = Vector2(movementDelta.x, -movementDelta.z).Length();
		playerAnimator->setVariable("xSpeed", 0.0f);
		playerAnimator->setVariable("zSpeed", Maths::clamp(dist, 0.0f, 1.0f) + (wantToSprint ? -0.5f : 0.0f));
	}
	else {
		playerAnimator->setVariable("xSpeed", movementDelta.x);
		playerAnimator->setVariable("zSpeed", -movementDelta.z + (wantToSprint ? -0.5f : 0.0f));
	}

	// Small hack to simulate time before we can sprint again. This should consider animation blending.
	if (currentTimeToStartSprintingAgain >= 0.0f)
		currentTimeToStartSprintingAgain -= dt;

	// Here we check if we must sprint.
	if (wantToSprint) {
		if (currentTimeToStartSprintingAgain <= 0.0f) { // If we are already facing the sprinting direction, sprint.
			ChangeState("Sprint");
			playerCamController->ChangeState("Sprint");
			return;
		}
	}
}

void TCompInputPlayerMovement::lockMovement(float duration) {
	locked = true;
	lockedDuration = duration;
	ChangeState("Normal");
	playerCamController->ChangeState("Normal");
	playerAnimator->setVariable("xSpeed", 0.0f);
	playerAnimator->setVariable("zSpeed", 0.0f);

}

//Msg handlers
void TCompInputPlayerMovement::onInputActiveMsg(const TMsgInputControllerStatus & msg) {
	active = msg.activate;
}

void TCompInputPlayerMovement::onEntityCreated(const TMsgEntityCreated & msg) {
	playerControllerHandle	= getComponent<TCompPlayerCharacterController>();
	playerRenderHandle		= getComponent<TCompRender>();
	playerTransformHandle	= getComponent<TCompTransform>();
	playerAnimatorHandle = getComponent<TCompPlayerAnimator>();
	fetchAliveColour();
}

void TCompInputPlayerMovement::onDeadChangeMsg(const TMsgEntityDead & msg) {
	ChangeState("Dead");

	// Play UI FadeIn of game over plus disable mouse center mode	
	EngineUI.activateWidget("game_over");
	EngineLogicManager.PlayUIAnimation("game_over", "game_over_image", "GameOverAnimation", false, 0.0f);
	EngineLogicManager.setCenterMouseActive(false);

	deadButtonsClock.start();

	// Set normal camera.
	playerCamController->ChangeState("Dead");

	// Make player stay on the ground.
	playerTransform->setRotation(Quaternion::CreateFromAxisAngle(VEC3(1, 0, 0), DEG2RAD(-90.f)));

	// Paint player red.
	VEC4 renderColour = playerRender->getColor();
	if (renderColour != Vector4(1, 0, 0, 1))
		aliveColour = renderColour;
	playerRender->setColor(Vector4(1, 0, 0, 1));

	// Remove in the future.
	EngineLogicManager.SetEntityActive(std::string("Submachinegun-Weapon"), false);
	EngineLogicManager.SetEntityActive(std::string("Shotgun-Weapon"), false);
	EngineLogicManager.SetEntityActive(std::string("Rifle-Weapon"), false);
	EngineLogicManager.SetEntityActive(std::string("Scythe-Weapon"), false);
}

// Used during this milestone to resurrect player on dead. Remove once finished.
void TCompInputPlayerMovement::onResurrectionMsg(const TMsgFullRestore & msg) {
	if (state == "Dead")
		ChangeState("Normal");

	// Remove in the future.
	EngineLogicManager.SetEntityActive(std::string("Submachinegun-Weapon"), true);
	EngineLogicManager.SetEntityActive(std::string("Shotgun-Weapon"), true);
	EngineLogicManager.SetEntityActive(std::string("Rifle-Weapon"), true);
	EngineLogicManager.SetEntityActive(std::string("Scythe-Weapon"), true);

	// Set normal camera.
	playerCamController->ChangeState("Normal");

	// Make player stay normal
	playerTransform->setRotation(Quaternion::CreateFromAxisAngle(Vector3(1, 0, 0), DEG2RAD(0.f)));

	// Paint player normal
	playerRender->setColor(aliveColour);
}