#include "mcv_platform.h"
#include "comp_input_player_combat.h"
#include "entity/entity_parser.h"

#include "components/common/comp_transform.h"
#include "components/controllers/comp_player_character_controller.h"
#include "components/camera/comp_camera.h"
#include "components/camera/comp_player_camera_controller.h"
#include "components/weapons/comp_weapons_manager.h"
#include "components/powers/comp_scythe_player.h"
#include "components/powers/comp_perception.h"
#include "components/player/comp_input_player_movement.h"
#include "components/animations/comp_player_animator.h"
#include "ui/ui_widget.h"
#include "engine.h"
#include "ui/widgets/ui_image.h"
#include "skeleton/comp_skeleton.h"

DECL_OBJ_MANAGER("input_player_combat", TCompInputPlayerCombat);

//General functions
void TCompInputPlayerCombat::load(const json & j, TEntityParseContext& ctx) {
	playerCamera = j.value("cameraFollowing", playerCamera);
	maxNormalPitch = j.value("maxNormalPitch", maxNormalPitch);
	minNormalPitch = j.value("minNormalPitch", minNormalPitch);
	maxCombatPitch = j.value("maxCombatPitch", maxCombatPitch);
	minCombatPitch = j.value("minCombatPitch", minCombatPitch);
	Init();
}

void TCompInputPlayerCombat::registerMsgs() {
	DECL_MSG(TCompInputPlayerCombat, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompInputPlayerCombat, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompInputPlayerCombat, TMsgFullRestore, onResurrectionMsg);  // Used during this milestone to resurrect player on dead. Remove once finished.
	DECL_MSG(TCompInputPlayerCombat, TMsgEntityDead, onDeadChangeMsg);
	DECL_MSG(TCompInputPlayerCombat, TMsgInputControllerStatus, onInputActiveMsg);
}

void TCompInputPlayerCombat::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Spacing();
	ImGui::Text("Current state: ");
	ImGui::Text(state.c_str());
}

void TCompInputPlayerCombat::update(float dt) {
	fetchComponents();
	wp_contr = weaponManager->getCurrentWeapon();
	isFacingCamera = inputPlayerMovement->getIsFacingCamera();
	isMoving = inputPlayerMovement->getIsMoving();

	//Checks if the player is sprinting
	if (inputPlayerMovement->getIsSprinting() && state != "ChangeWeapon") { //Fix bug not handling weapon change becase sprint returns to normal state
		// Bug sometimes when sprinting it stays with aiming, so we reset the combat input
		if (state != "Normal") {
			ChangeState("Normal");
			playerCamController->checkAndChangeState("Normal");
			inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::WEAPONUP);
			playerAnimator->setVariable("isAiming", false);
		}
		checkTimeToMoveFreely(dt); //If we are sprinting we count the time to get out of combat too
		return; //We can't do nothing if the player is sprinting
	}

	// TEMP_ADDED - updating the change between states
	assert(!state.empty());
	assert(statemap.find(state) != statemap.end());
	// this is a trusted jump as we've tested for coherence in ChangeState
	(this->*statemap[state])(dt);
}

void TCompInputPlayerCombat::Init() {
	AddState("Normal", (statehandler)&TCompInputPlayerCombat::normal);
	AddState("WeaponUp", (statehandler)&TCompInputPlayerCombat::weaponUp);
	AddState("Aim", (statehandler)& TCompInputPlayerCombat::aiming);
	AddState("Scythe", (statehandler)& TCompInputPlayerCombat::scythe);
	AddState("ChangeWeapon", (statehandler)& TCompInputPlayerCombat::changeWeapon);
	AddState("Reloading", (statehandler)& TCompInputPlayerCombat::reloading);
	AddState("Interacting", (statehandler)& TCompInputPlayerCombat::interacting);
	AddState("Dead", (statehandler)&TCompInputPlayerCombat::dead);
	ChangeState("Normal");
	lastState = "Normal";
}

//States
void TCompInputPlayerCombat::normal(float dt) {
	playerAnimator->setVariable("weaponUp", false);
	playerAnimator->setVariable("stop_fullbody", false);
	inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::NORMAL);
	
	//Handle reload and change weapon, if player wants, return
	if (handleReload(dt)) {
		playerAnimator->setVariable("weaponUp", true);
		return;
	}
	if (handleChangeWeapon(dt))
		return;

	// Powers
	handlePerception(dt);
	if (handleScythe(dt))
		return;

	if (handleInteract(dt))
		return;

	/* Weapons */
	if (EngineInput["shoot"].justPressed()) {
		weaponManager->showCroshair();
		ChangeState("WeaponUp");
		currentTimeToMoveFreely = timeToMoveFreely;
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::WEAPONUP);
		playerCamController->setMaxUpperPitch(maxCombatPitch);
		playerCamController->setMinLowerPitch(minCombatPitch);
	}
	else
		playerAnimator->clearShooting();

	if (EngineInput["altfire"].justPressed())
		wp_contr->switchSpecialMode();

	if (EngineInput["aim"].justPressed()) {
		weaponManager->showCroshair();
		ChangeState("Aim");
		playerCamController->checkAndChangeState("Aim");
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::AIMING);
		playerCamController->setMaxUpperPitch(maxCombatPitch);
		playerCamController->setMinLowerPitch(minCombatPitch);
		return;
	}	
}

void TCompInputPlayerCombat::weaponUp(float dt) {
	checkTimeToMoveFreely(dt);

	playerAnimator->setVariable("weaponUp", true);
	playerAnimator->setVariable("isAiming", false);
	playerAnimator->setVariable("aimAngle", playerCamController->getAimingDegreesPlayer());
	playerAnimator->setVariable("stop_fullbody", true);

	if (handleReload(dt))
		return;
	if (handleChangeWeapon(dt))
		return;

	// Powers
	handlePerception(dt);
	if (handleScythe(dt))
		return;

	if (handleInteract(dt))
		return;

	/* Weapons */
	if (EngineInput["shoot"]) {
		playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		currentTimeToMoveFreely = timeToMoveFreely;

		// Once we are facing, shoot.
		if (isFacingCamera && playerAnimator->isWeaponUpReadyToShoot())
			if (wp_contr->shoot())
				playerAnimator->setAimShootingWeapon(weaponManager->getCurrentWeaponID());
			else if (wp_contr->getCurrentAmmo() == 0)
				playerAnimator->clearShooting();
	}
	else
		playerAnimator->clearShooting();

	if (EngineInput["altfire"].justPressed())
		wp_contr->switchSpecialMode();

	if (EngineInput["aim"].justPressed()) {
		ChangeState("Aim");
		weaponManager->showCroshair();
		playerCamController->checkAndChangeState("Aim");
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::AIMING);
		return;
	}
}

void TCompInputPlayerCombat::aiming(float dt) {
	
	playerAnimator->setVariable("weaponUp", true);
	playerAnimator->setVariable("isAiming", true);
	playerAnimator->setVariable("aimAngle", playerCamController->getAimingDegreesPlayer());
	playerAnimator->setVariable("stop_fullbody", true);

	weaponManager->showCroshair();

	if (handleReload(dt))
		return;
	if (handleChangeWeapon(dt))
		return;

	handlePerception(dt);
	if (handleScythe(dt))
		return;

	if (handleInteract(dt))
		return;

	/* Weapons */
	if (EngineInput["shoot"] && playerAnimator->isWeaponUpReadyToShoot()) {
		if (wp_contr->shoot()) {
			currentTimeToMoveFreely = timeToMoveFreely;
			playerAnimator->setAimShootingWeapon(weaponManager->getCurrentWeaponID());
		}
		else if (wp_contr->getCurrentAmmo() == 0)
			playerAnimator->clearShooting();
	}
	else
		playerAnimator->clearShooting();

	if (EngineInput["altfire"].justPressed())
		wp_contr->switchSpecialMode();

	if (EngineInput["aim"].justReleased()) {
		ChangeState("WeaponUp");
		playerAnimator->setVariable("isAiming", false);
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::WEAPONUP);
		playerCamController->checkAndChangeState("Normal");
		return;
	}	
}

void TCompInputPlayerCombat::scythe(float dt) {
	handlePerception(dt);

	//When the variable is -1 the animation will be over and we will handle to which state we need to jump
	bool isUsingScythe = std::get<bool>(playerAnimator->getVariable(_scytheVarName, PlayerFSM::FULLBODY)->_value);
	if (!isUsingScythe) {
		checkStateJumpAfterAnimation();
		weaponManager->showCroshair(true);
		currentTimeToMoveFreely = timeToMoveFreely;
		return;
	}

	//Handle if aiming or normal for camera and movement restrictions
	if (EngineInput["aim"].justPressed()) {
		playerCamController->checkAndChangeState("Aim");
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::AIMING);
	}
	if (EngineInput["aim"].justReleased()) {
		playerCamController->checkAndChangeState("Normal");
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::WEAPONUP);
	}
}

void TCompInputPlayerCombat::changeWeapon(float dt) {
	handlePerception(dt);

	if (!_changedWeaponMidAnimation) {
		//Check for animation % to change the weapon from the back to the hand and viceversa
		float animationPercentage = playerSkeleton->getAnimationPercentage(_changeWeaponAnimNames[weaponManager->getPreviousWeaponID()]);
		//Change the meshes from the back to the hand
		if (animationPercentage > 0.5f) {
			weaponManager->setActiveWeaponSlotInternal(weaponManager->getCurrentWeaponID());
			_changedWeaponMidAnimation = true;
		}
	}

	//When the variable is -1 the animation will be over and we will handle to which state we need to jump
	int isChangingWeapon = std::get<int>(playerAnimator->getVariable(_changeWeaponVarName, PlayerFSM::UPPER_BODDY)->_value);
	if (isChangingWeapon == -1) {
		checkStateJumpAfterAnimation();
		_changedWeaponMidAnimation = false;
		return;
	}

	//Handle if aiming or normal for camera and movement restrictions
	if (EngineInput["aim"].justPressed()) {
		playerCamController->checkAndChangeState("Aim");
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::AIMING);
	}
	if (EngineInput["aim"].justReleased()) {
		playerCamController->checkAndChangeState("Normal");
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::WEAPONUP);
	}
}

void TCompInputPlayerCombat::reloading(float dt)
{
	handlePerception(dt);

	//This variable will ignore the frame we set the reload fast or the reload fail, 
	//because the animator needs one update loop to set the var in the context
	bool ignoreThisFrame = false;

	if (EngineInput["reload"].justPressed() && !_hasTriedActiveReload) {

		//We need to check for the active reload
		//If true, active reload okay, else bad timing
		if (wp_contr->reload()) {
			//Play the fast animation with the same time and clears the normal one
			playerAnimator->setReloadFast(playerAnimator->getReloadElapsedTime());

			_currentReloadVarName = _reloadFastVarName;
			ignoreThisFrame = true;
		}
		else {
			//Play the fast animation with the same time and clears the normal one
			playerAnimator->setReloadFail(playerAnimator->getReloadElapsedTime());

			_currentReloadVarName = _reloadFailVarName;
			ignoreThisFrame = true;
		}
		_hasTriedActiveReload = true;
	}

	if (!ignoreThisFrame) {
		//When the variable is -1 the animation will be over and we will handle to which state we need to jump
		int isReloading = std::get<int>(playerAnimator->getVariable(_currentReloadVarName, PlayerFSM::UPPER_BODDY)->_value);
		if (isReloading == -1) {
			//Handle to which state we need to jump
			if (EngineInput["aim"].isPressed()) {
				ChangeState("Aim");
				playerCamController->checkAndChangeState("Aim");
				playerAnimator->setVariable("weaponUp", true);
				playerAnimator->setVariable("isAiming", true);
				inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::AIMING);
			}
			else {
				ChangeState("WeaponUp");
				playerCamController->checkAndChangeState("Normal");
				playerAnimator->setVariable("weaponUp", true);
				playerAnimator->setVariable("isAiming", false);
				inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::WEAPONUP);
			}
			//Set the time for the weaponUp state before going back to normal.
			currentTimeToMoveFreely = timeToMoveFreely;

			//Clear the reload information and control in the player animator as we have finished reloading
			playerAnimator->clearReload();
			return;
		}

		//Handle if aiming or normal for camera and movement restrictions
		if (EngineInput["aim"].justPressed()) {
			playerCamController->checkAndChangeState("Aim");
			inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::AIMING);
		}
		if (EngineInput["aim"].justReleased()) {
			playerCamController->checkAndChangeState("Normal");
			inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::WEAPONUP);
		}
	}
}

void TCompInputPlayerCombat::interacting(float dt) {
	handlePerception(dt);

	//When the variable is -1 the animation will be over and we will handle to which state we need to jump
	bool isinteracting = std::get<bool>(playerAnimator->getVariable(_interactVarName, PlayerFSM::FULLBODY)->_value);
	if (!isinteracting) {
		checkStateJumpAfterAnimation();
		inputPlayerMovement->unlockMovement();
		//Move the current weapon back to the hand
		weaponManager->getCurrentWeapon()->attachWeaponToHand();
		return;
	}
}

void TCompInputPlayerCombat::dead(float dt) {
	playerAnimator->setVariable("dead", true);
	playerAnimator->setVariable("stop_fullbody", false);
}


void TCompInputPlayerCombat::checkStateJumpAfterAnimation()
{
	if (state == "ChangeWeapon" && inputPlayerMovement->getIsSprinting()) {
		playerCamController->ChangeState("Sprint");
		ChangeState("Normal");
		playerAnimator->setVariable("weaponUp", false);
		playerAnimator->setVariable("isAiming", false);
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::NORMAL);
		playerCamController->setMaxUpperPitch(maxNormalPitch);
		playerCamController->setMinLowerPitch(minNormalPitch);
		return;
	}

	//Handle to which state we need to jump
	if (EngineInput["aim"].isPressed()) {
		ChangeState("Aim");
		playerCamController->checkAndChangeState("Aim");
		playerAnimator->setVariable("weaponUp", true);
		playerAnimator->setVariable("isAiming", true);
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::AIMING);
	}
	else if (currentTimeToMoveFreely > 0.0f) {
		ChangeState("WeaponUp");
		playerCamController->checkAndChangeState("Normal");
		playerAnimator->setVariable("weaponUp", true);
		playerAnimator->setVariable("isAiming", false);
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::WEAPONUP);
	}
	else {
		ChangeState("Normal");
		playerCamController->checkAndChangeState("Normal");
		playerAnimator->setVariable("weaponUp", false);
		playerAnimator->setVariable("isAiming", false);
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::NORMAL);
		playerCamController->setMaxUpperPitch(maxNormalPitch);
		playerCamController->setMinLowerPitch(minNormalPitch);
	}
}

//Private functions
void TCompInputPlayerCombat::fetchComponents() {
	// checkPlayerCamera
	if (!cameraTransformHandle.isValid()) {
		CEntity * ent = getEntityByName(playerCamera.c_str());
		cameraTransformHandle = ent->getComponent<TCompTransform>();
	}
	if (!cameraControllerHandle.isValid()) {
		CEntity * ent = getEntityByName(playerCamera.c_str());
		cameraControllerHandle = ent->getComponent<TCompPlayerCameraController>();
	}

	cameraTransform		= cameraTransformHandle; // Get the camera's transform.
	playerCamController = cameraControllerHandle; // Get the camera that follows the player.
	playerController	= playerControllerHandle;
	weaponManager		= weaponManagerHandle;
	scythePlayer		= scythePlayerHandle;
	perceptionPlayer	= perceptionPlayerHandle;
	inputPlayerMovement	= inputPlayerMovementHandle;
	playerAnimator		= playerAnimatorHandle;
	playerSkeleton		= playerSkeletonHandle;
}

void TCompInputPlayerCombat::handlePerception(float dt) {
	if (EngineInput["perceptionPower"].justPressed()) 
		perceptionPlayer->Use();
}

bool TCompInputPlayerCombat::handleScythe(float dt) {
	bool knockbackReceived = std::get<bool>(playerAnimator->getVariable(_knockbackVarName, PlayerFSM::FULLBODY)->_value);
	if (EngineInput["scythePower"].justPressed() && scythePlayer->isCooldownOver() && !knockbackReceived) {
		//First we clear shooting values because maybe we were shooting
		playerAnimator->clearShooting();

		scythePlayer->activatePower();
		weaponManager->showCroshair(false);
		playerAnimator->setVariable(_scytheVarName, true);
		playerAnimator->setVariable("stop_fullbody", false);
		ChangeState("Scythe");
		currentTimeToMoveFreely = timeToMoveFreely;

		// UI 
		UI::CWidget iconScythe;
		if (EngineUI.findWidgetByName(iconScythe, "abilities_scythe")) {
			for (auto& child : iconScythe.getChildren()) {
				if (UI::CImage * imgChild = dynamic_cast<UI::CImage*>(child)) {
					imgChild->playAnimation("AbilityCD", false);
					imgChild->playAnimation("AbilityCDButton", false);
				}
			}
		}

		return true;
	}

	return false;
}

bool TCompInputPlayerCombat::handleChangeWeapon(float dt) {
	bool wantToChangeWeapon = false;
	//Smg
	if (EngineInput["weapon1"].justPressed()) {
		if (weaponManager->setActiveWeaponSlot(0))
			wantToChangeWeapon = true;
	}
	//Shotgun
	if (EngineInput["weapon2"].justPressed()) {
		if (weaponManager->setActiveWeaponSlot(1))
			wantToChangeWeapon = true;
	}
	//Rifle
	if (EngineInput["weapon3"].justPressed()) {
		if (weaponManager->setActiveWeaponSlot(2))
			wantToChangeWeapon = true;
	}

	if (EngineInput.mouse()._wheelDelta > 0.f) {
		weaponManager->changeNextWeapon();
		wantToChangeWeapon = true;
	}
	if (EngineInput.mouse()._wheelDelta < 0.f) {
		weaponManager->changePrevWeapon();
		wantToChangeWeapon = true;
	}

	bool knockbackReceived = std::get<bool>(playerAnimator->getVariable(_knockbackVarName, PlayerFSM::FULLBODY)->_value);
	if (wantToChangeWeapon && !knockbackReceived) {
		//First we clear shooting values because maybe we were shooting and changed the weapon
		playerAnimator->clearShooting();

		playerAnimator->setChangeWeapon(weaponManager->getPreviousWeaponID());
		ChangeState("ChangeWeapon");
		return true;
	}

	return false;
}

bool TCompInputPlayerCombat::handleReload(float dt) {
	bool knockbackReceived = std::get<bool>(playerAnimator->getVariable(_knockbackVarName, PlayerFSM::FULLBODY)->_value);
	if (EngineInput["reload"].justPressed() && !knockbackReceived) {
		// Start reloading the weapon and the animation if the weapon can be reloaded
		if (wp_contr->reload()) {
			//First we clear shooting values because maybe we were shooting and reloaded
			playerAnimator->clearShooting();
			
			playerAnimator->setReload(weaponManager->getCurrentWeaponID());

			ChangeState("Reloading");
			_currentReloadVarName = _reloadNormalVarName;
			_hasTriedActiveReload = false;

			return true;
		}
	}

	return false;
}

bool TCompInputPlayerCombat::handleInteract(float dt) {
	bool knockbackReceived = std::get<bool>(playerAnimator->getVariable(_knockbackVarName, PlayerFSM::FULLBODY)->_value);
	TCompInteractor* c_interactor = getPlayerInteractor();
	if (EngineInput["interact"].justPressed() && !knockbackReceived && c_interactor) {
		// Check if the component is able to interact
		if (c_interactor->interact(dt)) {
			//First we clear shooting values because maybe we were shooting
			playerAnimator->clearShooting();

			//Move the current weapon to the back of the player
			weaponManager->getCurrentWeapon()->attachWeaponToHolder();

			playerAnimator->setVariable("interacting", true);
			playerAnimator->setVariable("stop_fullbody", false);

			playerCamController->checkAndChangeState("Normal");
			inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::NORMAL);
			inputPlayerMovement->lockMovement();

			ChangeState("Interacting");

			return true;
		}
	}

	return false;
}

void TCompInputPlayerCombat::checkTimeToMoveFreely(float dt) {
	currentTimeToMoveFreely -= dt;
	if (currentTimeToMoveFreely <= 0) {
		weaponManager->showCroshair(false);
		ChangeState("Normal");
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::NORMAL);
		playerCamController->setMaxUpperPitch(maxNormalPitch);
		playerCamController->setMinLowerPitch(minNormalPitch);
		return;
	}
}

//Msgs handlers
void TCompInputPlayerCombat::onInputActiveMsg(const TMsgInputControllerStatus & msg) {
	active = msg.activate;
	//Normal combat status if disabled
	if (!active) {
		inputPlayerMovement = inputPlayerMovementHandle;
		inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::NORMAL);
		if(playerCamController)
		playerCamController->checkAndChangeState("Normal");
	}
}

void TCompInputPlayerCombat::onEntityCreated(const TMsgEntityCreated & msg) {
	playerControllerHandle		= getComponent<TCompPlayerCharacterController>();
	weaponManagerHandle			= getComponent<TCompWeaponManager>(); 
	scythePlayerHandle			= getComponent<TCompScythePlayer>();
	perceptionPlayerHandle		= getComponent<TCompPerception>();
	inputPlayerMovementHandle	= getComponent<TCompInputPlayerMovement>();
	playerAnimatorHandle		= getComponent<TCompPlayerAnimator>();
	playerSkeletonHandle		= getComponent<TCompSkeleton>();
}

void TCompInputPlayerCombat::onDeadChangeMsg(const TMsgEntityDead & msg) {
	weaponManager->showCroshair(false);
	ChangeState("Dead");
	//Normal combat status if dead
	inputPlayerMovement->setCombatState(TCompInputPlayerMovement::CombatState::NORMAL);
}

// Used during this milestone to resurrect player on dead. Remove once finished.
void TCompInputPlayerCombat::onResurrectionMsg(const TMsgFullRestore & msg) {
	if (state == "Dead") {
		ChangeState("Normal");
		playerCamController->setMaxUpperPitch(maxNormalPitch);
		playerCamController->setMinLowerPitch(minNormalPitch);
	}
}