#pragma once

#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/states/state_machine_controller.h"

class TCompTransform;
class TCompPlayerCameraController;
class TCompPlayerCharacterController;
class TCompRender;
class TCompPlayerAnimator;

class TCompInputPlayerMovement : public StateMachineController{
public:
	enum CombatState { NORMAL, WEAPONUP, AIMING };
private:
	DECL_SIBLING_ACCESS();

	//Handles
	std::string playerCamera;
	CHandle cameraTransformHandle;
	CHandle cameraControllerHandle;
	CHandle cameraHandle; // We use this handle for debug. If the camara is not active, the player won't move.
	CHandle playerControllerHandle;
	CHandle playerRenderHandle;
	CHandle playerTransformHandle;
	CHandle playerAnimatorHandle;

	//Components
	TCompTransform * cameraTransform; 
	TCompPlayerCameraController * playerCamController; 
	TCompPlayerCharacterController * playerController;
	TCompRender * playerRender;
	TCompTransform * playerTransform;
	TCompPlayerAnimator * playerAnimator;

	// In degrees, threshold for considering when the player is facing the camera.
	// Once achieved, the player is able to start firing.
	float facingCameraThreshold = 10.0f;

	// This simulates a sprinting animation that must be played. A small delay in input.
	float timeToStartSprintingAgain = 0.25f;
	float currentTimeToStartSprintingAgain = timeToStartSprintingAgain;
	float timeBeforeSprintingEnds = 0.1f;
	float currentTimeBeforeSprintingEnds = timeBeforeSprintingEnds;

	bool justDead = false; // Used for sending a message to the UI to show the player is dead.
	VEC4 aliveColour = VEC4(0, 0, 0, 0);

	bool facingCamera;
	bool moving;
	bool isSprinting;
	bool locked = false; //Locks the movement
	float lockedDuration = 0.0f;
	CombatState combatState  = NORMAL;
	float deadZone = 0.2;

	float deadButtonsTime = 6.0f;
	CClock deadButtonsClock = CClock(deadButtonsTime);

	//Functions
	void fetchComponents();
	void fetchAliveColour();

	bool isFacingCamera(TCompTransform * playerTransform, TCompTransform * cameraTransform, float threshold);
	void handleMovement(float dt);
	
	//States
	void normal(float dt);
	void sprint(float dt);
	void dead(float dt);

	//Msgs
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onDeadChangeMsg(const TMsgEntityDead & msg);
	void onInputActiveMsg(const TMsgInputControllerStatus & msg);
	void onResurrectionMsg(const TMsgFullRestore & msg); // Used during this milestone to resurrect player on dead. Remove once finished.

public:
	void Init() override;
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void update(float dt);	
	static void registerMsgs();

	bool getIsFacingCamera() { return facingCamera; }
	bool getIsMoving() { return moving; }
	bool getIsSprinting() { return isSprinting; }
	void setCombatState(CombatState state) { combatState = state; }
	void lockMovement(float duration = -1.0f);
	void unlockMovement() { locked = false; }
};