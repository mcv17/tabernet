#pragma once


/* Contains functions used for managing PickUps dropping rates and
how this are selected */

class CHandle;
class LogicManager;

namespace PickUpFunctions {
	typedef std::string PickUpID;
	typedef std::string DropTypeID;

	// Struct holding some data about a pickup. For now only name and path of the entity
	// that represents it.
	struct PickUp {
	private:
		PickUpID PickUpNameID;
		std::string PathToPickUpEntity;
	public:
		void loadPickup(const json & pickUpData);
		void renderInMenu() const;

		const std::string & getPickUpNameId() const { return PickUpNameID; }
		const std::string & getPickUpPathToEntity() const { return PathToPickUpEntity; }
	};

	// Struct representing a drop rate. Holds a pickup that will be spawned together with it's
	// probability to spawn.
	struct DropRate {
	private:
		PickUp & pickupToSpawn;
		float dropRate;
	public:
		DropRate & operator =(const DropRate & a)
		{
			pickupToSpawn = a.pickupToSpawn;
			dropRate = a.dropRate;
			return *this;
		}
		
		DropRate(PickUp & nPickUpToSpawn, float nDropRate) : pickupToSpawn(nPickUpToSpawn), dropRate(nDropRate){}

		const PickUp & getPickUpInfo() const { return pickupToSpawn; }
		float getDropRate() const{ return dropRate; }
		void setDropRate(float nDropRate) { dropRate = nDropRate; }
	};

	typedef std::vector<DropRate> DropRatesArray;

	// Holds information about the percentages and min and max number of drops
	// that can fall for the given drop type.
	struct DropRateForDropType {

	private:
		std::string ratesName;
		int minNumberOfDrops;
		int maxNumberOfDrops;
		DropRatesArray pickUpDropRates;

	public:
		void loadRates(const json & ratesData);
		void renderInMenu();

		/* Getters. */
		const int getMinNumberOfDrops() const { return minNumberOfDrops; }
		const int getMaxNumberOfDrops() const { return maxNumberOfDrops; }
		const DropRatesArray & GetDropRatesForDropType() const { return pickUpDropRates; }
	};

	typedef CHandle (LogicManager::*LogicManagerSpawnPickUpFunction)(const std::string &, const Vector3 &, const Vector3 &);

	// Manages the spawning of drops. Gets loaded at the beginning, reads information
	// from a json file to configure dropping rates.
	class SpawnDropsManager {
		static bool SpawnDropsManagerLoaded;
		static std::map<PickUpID, PickUp> existingPickupTypes;
		static std::map<DropTypeID, DropRateForDropType> dropRateType;

		SpawnDropsManager();
	public:
		static void Init(const std::string & spawnsDropsDataFilePath);

		// Functions that compute the drop rate for different types of enemies.
		static void SpawnPickUps(DropTypeID dropType, LogicManagerSpawnPickUpFunction functionToSpawnPickUp, const Vector3 & positionToSpawnAt, const Vector3 & impulse = Vector3(),
			const VEC3 & minPositionNoise = Vector3(), const VEC3 & maxPositionNoise = Vector3(), const VEC3 & minImpulseNoise = Vector3(), const VEC3 & maxImpulseNoise = Vector3(),
			int numOfPickUpsToSpawnFromParameter = -1);

		static void renderInMenu();

		// Just in case somebody may want them I give them here.
		static const DropRateForDropType * getDropTypeRates(DropTypeID dropType);
		static const PickUp * getPickUpInfo(PickUpID pickUp);

		friend struct DropRateForDropType;
	private:
		static void loadPickUps(const std::string & spawnsDropsDataFilePath, const json & dataToLoad);
		static void loadDropRateTypes(const std::string & spawnsDropsDataFilePath, const json & dataToLoad);
	};
}