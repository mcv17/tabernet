#include "mcv_platform.h"
#include "PickUpUtils.h"

#include "../../engine.h"
#include "../../utils/json_resource.h"
#include "../../logic/logic_manager.h"

#include "components/render/comp_trail_render.h"

namespace PickUpFunctions {
	
	void PickUp::loadPickup(const json & pickUpData) {
		PickUpNameID = pickUpData.value("pickup_name_id", "");
		PathToPickUpEntity = pickUpData.value("path_to_pickup_entity", "");
	}

	void PickUp::renderInMenu() const {
		ImGui::Text("Pick Up Name ID: %s", PickUpNameID.c_str());
		ImGui::Text("Path to Pick Up Entity: %s", PathToPickUpEntity.c_str());
	}

	void DropRateForDropType::loadRates(const json & ratesData) {
		ratesName = ratesData.value("drop_type_id", "");

		// Minimum and maximum drops that can drop for the given entity.
		minNumberOfDrops = std::max(ratesData.value<int>("min_number_of_drops", 0), 0);
		maxNumberOfDrops = std::max(ratesData.value<int>("max_number_of_drops", 0), 0);
		if (minNumberOfDrops > maxNumberOfDrops)
			std::swap(minNumberOfDrops, maxNumberOfDrops);

		if (ratesData.count("pickups_to_drop") == 0) {
			Utils::dbg("Attention drop rate %s didn't have any pickups to drop. Is this correct?", ratesName);
			return;
		}

		if (ratesData["pickups_to_drop"].is_array() == false) {
			Utils::dbg("Attention pick ups to drop for drop type %s is not an array. Make it one", ratesName);
			return;
		}

		// Load drop rates information.
		for (auto pickUpToLoad : ratesData["pickups_to_drop"]) {
			std::string PickUpId = pickUpToLoad.value("pickup_name_id", "");
			if(SpawnDropsManager::existingPickupTypes.count(PickUpId))
				pickUpDropRates.push_back(DropRate(SpawnDropsManager::existingPickupTypes[PickUpId], pickUpToLoad.value<float>("drop_rate", 0.0f)));
			else
				Utils::dbg("Attention pick up with name %s wasn't found. �Did you add it to the pickups definition array?", PickUpId);
		}

		// Calculate maximum drop rate (should be 1.0 in total but we allow to define it above 1.0 if you want).
		float total_drop_rate = 0.0f;
		for (int i = 0; i < (int)pickUpDropRates.size(); ++i)
			total_drop_rate += pickUpDropRates[i].getDropRate();

		// Adapt the drop rate so it is between 0.0 - 1.0. It will be in the 1.0f range.
		for (int i = 0; i < (int)pickUpDropRates.size(); ++i)
			pickUpDropRates[i].setDropRate(pickUpDropRates[i].getDropRate()/total_drop_rate);
	}

	void DropRateForDropType::renderInMenu() {
		ImGui::Spacing(0, 5);
		if (ImGui::CollapsingHeader(ratesName.c_str())) {
			ImGui::Spacing(0, 5);
			ImGui::PushID(ratesName.c_str());

			ImGui::Text("Drop rates for: %s", ratesName.c_str());

			ImGui::DragInt("Minimum number of drops: ", &minNumberOfDrops, 1.0, 0, 10000000);
			ImGui::DragInt("Maximum number of drops: ", &maxNumberOfDrops, 1.0, 0, 10000000);
			ImGui::Spacing(0, 5);

			ImGui::Text("Pickup drop rates");
			ImGui::Spacing(0, 5);

			bool dropRateChanged = false;
			for (int i = 0; i < pickUpDropRates.size(); ++i) {
				pickUpDropRates[i].getPickUpInfo().renderInMenu();

				float dropRatePickUp = pickUpDropRates[i].getDropRate();
				if (ImGui::DragFloat("PickUp DropRate", &dropRatePickUp, 0.01f, 0.0f, 1.0f))
					pickUpDropRates[i].setDropRate(dropRatePickUp);
				ImGui::Spacing(0, 5);
			}

			if (dropRateChanged) {
				// Calculate maximum drop rate (should be 1.0 in total but we allow to define it above 1.0 if you want).
				float total_drop_rate = 0.0f;
				for (int i = 0; i < (int)pickUpDropRates.size(); ++i)
					total_drop_rate += pickUpDropRates[i].getDropRate();

				// Adapt the drop rate so it is between 0.0 - 1.0. It will be in the 1.0f range.
				for (int i = 0; i < (int)pickUpDropRates.size(); ++i)
					pickUpDropRates[i].setDropRate(pickUpDropRates[i].getDropRate() / total_drop_rate);
			}

			ImGui::PopID();
			ImGui::Spacing(0, 5);
			ImGui::Separator();
			ImGui::Spacing(0, 5);
		}
	}

	bool SpawnDropsManager::SpawnDropsManagerLoaded = false;
	std::map<PickUpID, PickUp> SpawnDropsManager::existingPickupTypes;
	std::map<DropTypeID, DropRateForDropType> SpawnDropsManager::dropRateType;


	void SpawnDropsManager::Init(const std::string & spawnsDropsDataFilePath) {
		if (SpawnDropsManagerLoaded) return;
		SpawnDropsManagerLoaded = true;

		json & data = Utils::loadJson(spawnsDropsDataFilePath);

		loadPickUps(spawnsDropsDataFilePath, data);
		loadDropRateTypes(spawnsDropsDataFilePath, data);
	}


	void SpawnDropsManager::loadPickUps(const std::string & spawnsDropsDataFilePath, const json & dataToLoad){
		if (dataToLoad.count("pickups") == 0) {
			Utils::dbg("Pickup data was not found in the game_data file with path: %s", spawnsDropsDataFilePath);
			return;
		}
		if (dataToLoad["pickups"].is_array() == false) {
			Utils::dbg("Pickup data is not an array. Make it an array.");
			return;
		}

		const json & pickUps = dataToLoad["pickups"];
		for (auto & pickUp : pickUps) {
			const std::string pickUpNameId = pickUp["pickup_name_id"];
			if (existingPickupTypes.count(pickUpNameId) == 0) {
				PickUp newPickUp;
				newPickUp.loadPickup(pickUp);
				existingPickupTypes[pickUpNameId] = newPickUp;
			}
			else
				Utils::dbg("Pickup with same pickup ID %s was repeated, change pickup ids to add it to the game.", pickUpNameId);
		}
	}

	void SpawnDropsManager::loadDropRateTypes(const std::string & spawnsDropsDataFilePath, const json & dataToLoad){
		if (dataToLoad.count("dropTypes") == 0) {
			Utils::dbg("Drop types data was not found in the game_data file with path: %s", spawnsDropsDataFilePath);
			return;
		}
		if (dataToLoad["dropTypes"].is_array() == false) {
			Utils::dbg("Drop types data is not an array. Make it an array.");
			return;
		}

		const json & dropTypes = dataToLoad["dropTypes"];
		for (auto & dropType : dropTypes) {
			const DropTypeID dropTypeID = dropType["drop_type_id"];
			if (dropRateType.count(dropTypeID) == 0) {
				DropRateForDropType dropRateForDropType;
				dropRateForDropType.loadRates(dropType);
				dropRateType[dropTypeID] = dropRateForDropType;
			}
			else
				Utils::dbg("Drop type with same drop type ID %s was repeated, change ids to add it to the game.", dropTypeID);
		}
	}

	void SpawnDropsManager::SpawnPickUps(DropTypeID dropType, LogicManagerSpawnPickUpFunction functionToSpawnPickUp, const Vector3 & positionToSpawnAt, const Vector3 & impulse,
		const VEC3 & minPositionNoise, const VEC3 & maxPositionNoise, const VEC3 & minImpulseNoise, const VEC3 & maxImpulseNoise, int numOfPickUpsToSpawnFromParameter) {
		const DropRateForDropType & dropRate = *getDropTypeRates(dropType);
		const DropRatesArray & dropRates = dropRate.GetDropRatesForDropType();
		int numOfProbsToSpawn = (numOfPickUpsToSpawnFromParameter == -1) ? (int)randomFloat(dropRate.getMinNumberOfDrops(), dropRate.getMaxNumberOfDrops() + 1) : numOfPickUpsToSpawnFromParameter;

		for (int z = 0; z < numOfProbsToSpawn; ++z) {
			float randomProbPickUpToSpawn = randomFloat(0.0f + 0.000000000001f, 1.0f); // Between 0.0f + very small value to avoid bug of spawning pickup with 0.0f and 1.0f.

			// Loop through all pickups, check if it will spawn.
			float accumulatedProbUntilNow = 0.0f;
			for (int i = 0; i < (int)dropRates.size(); ++i) {
				auto & DropRateInfo = dropRates[i];

				// If we are in the prob range, we spawn it, otherwise, we add the probability until now,
				// and move to next drop rate if there is one.
				float pickUpRate = DropRateInfo.getDropRate();
				if (randomProbPickUpToSpawn <= (accumulatedProbUntilNow + pickUpRate)) {
					Vector3 finalPosition = positionToSpawnAt + Vector3(randomFloat(minPositionNoise.x, maxPositionNoise.x), randomFloat(minPositionNoise.y, maxPositionNoise.y), randomFloat(minPositionNoise.z, maxPositionNoise.z));
					Vector3 finalImpulse = impulse + Vector3(randomFloat(minImpulseNoise.x, maxImpulseNoise.x), randomFloat(minImpulseNoise.y, maxImpulseNoise.y), randomFloat(minImpulseNoise.z, maxImpulseNoise.z));
					CHandle pickupEntity = (LogicManager::Instance().*functionToSpawnPickUp)(DropRateInfo.getPickUpInfo().getPickUpPathToEntity(), finalPosition, finalImpulse);
					break;
				}
				accumulatedProbUntilNow += pickUpRate;
			}
		}
	}

	void SpawnDropsManager::renderInMenu() {
		if (ImGui::CollapsingHeader("Pickups")) {
			for (auto & pickUp : existingPickupTypes)
				pickUp.second.renderInMenu();
		}

		if (ImGui::CollapsingHeader("Drop types")) {
			for (auto & dropType : dropRateType)
				dropType.second.renderInMenu();
		}
	}

	const DropRateForDropType * SpawnDropsManager::getDropTypeRates(DropTypeID dropType){
		auto dropTypeIt = dropRateType.find(dropType);
		return (dropTypeIt != dropRateType.end()) ? &(dropTypeIt->second) : nullptr;
	}

	const PickUp * SpawnDropsManager::getPickUpInfo(PickUpID pickUp) {
		auto pickUpIt = existingPickupTypes.find(pickUp);
		return (pickUpIt != existingPickupTypes.end()) ? &(pickUpIt->second) : nullptr;
	}
}