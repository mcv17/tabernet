#include "mcv_platform.h"
#include "components\states\state_machine_controller.h"


void StateMachineController::update(float dt)
{
  assert(!state.empty());
  assert(statemap.find(state) != statemap.end());
  // this is a trusted jump as we've tested for coherence in ChangeState
  (this->*statemap[state])(dt);
}

void StateMachineController::ChangeState(const std::string newstate)
{
  // try to find a state with the suitable name
  if (statemap.find(newstate) == statemap.end())
  {
    // the state we wish to jump to does not exist. we abort
    Utils::fatal("Invalid ChangeState(%s)\n", newstate.c_str());
  }
	lastState = state;
  state = newstate;
}


void StateMachineController::AddState(const std::string& name, statehandler sh)
{
  // try to find a state with the suitable name
  if (statemap.find(name) != statemap.end())
  {
    // the state we wish to jump to does exist. we abort
		Utils::fatal("Invalid AddState(%s). Already defined.\n", name.c_str());
  }
  statemap[name] = sh;
}
