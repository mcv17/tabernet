#ifndef _AICONTROLLER_H
#define _AICONTROLLER_H

// ai controllers using maps to function pointers for easy access and scalability. 

// we put this here so you can assert from all controllers without including everywhere
#include <assert.h>	
#include <string>
#include <map>
#include "entity\entity.h"
#include "components\common\comp_base.h"

// states are a map to member function pointers, to 
// be defined on a derived class. 
class StateMachineController;

typedef void (StateMachineController::*statehandler)(float);

class StateMachineController : public TCompBase {
public:

protected:
	std::string                         state;
	std::string													lastState;
	// the states, as maps to functions
  std::map<std::string, statehandler> statemap;

public:
	void ChangeState(const std::string);	// state we wish to go to
	virtual void Init() { }// resets the controller
  void update(float dt);	// recompute behaviour
  void AddState(const std::string&, statehandler);

	void load(const json & j, TEntityParseContext& ctx) {}
};

#endif