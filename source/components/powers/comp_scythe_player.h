#pragma once
#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/weapons/comp_scythe_controller.h"
#include "components/render/comp_trail_render.h"

class TCompScythePlayer : public TCompBase {

	float _powerCooldown;
	float _remainingCooldown;
	float _damage = 80.0f;

	std::string _swingSoundEvent = "";

	std::string _weaponEntity, _trailEntity;
	CClock _trailStartClock = CClock(0.1f), _trailDurationClock = CClock(1.2f);

	DECL_TCOMP_ACCESS(_weaponEntity, TCompScytheController, ScytheController);
	DECL_TCOMP_ACCESS(_trailEntity, TCompTrailRender, TrailRender);

public:
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();
	static void declareInLua();

	void activatePower();
	bool isCooldownOver();

	DECL_SIBLING_ACCESS();

};