#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_collider.h"

class TCompScythe : public TCompBase {

	float _timeToLive;
	float _damage;
	float _offsetY;
	std::vector<CHandle> _enemies;
	
	TMsgHitDamage _msg;

	void onTriggerEnter(const TMsgEntityTriggerEnter & trigger_enter);
	void onCreatedUpdatePositon(const TMsgUpdateIndicator & msg);

	void updateRotation(float dt);

public:
	
	void load(const json& j, TEntityParseContext& ctx);
	static void registerMsgs();
	
	void update(float dt);
	void debugInMenu();
	void renderDebug();

	DECL_SIBLING_ACCESS();
	
};