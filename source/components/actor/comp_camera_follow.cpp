#include "mcv_platform.h"
#include "comp_camera_follow.h"
#include "components/common/comp_transform.h"
#include "utils/utils.h"
#include "engine.h"
#include "input/input.h"
#include "input/module_input.h"

DECL_OBJ_MANAGER("camera_follow", TCompCameraFollow);

inline TCompTransform* getPlayerTransform() {
	CEntity* player = getEntityByName("Player");
	if (!player) return nullptr;
	else return player->getComponent<TCompTransform>();
}

void TCompCameraFollow::debugInMenu() {
}

void TCompCameraFollow::load(const json& j, TEntityParseContext& ctx) {
	if (j.count("key")) {
		std::string k = j["key"];
		_key_toggle_enabled = k[0];
	}

}

void TCompCameraFollow::update(float scaled_dt)
{
	if (_enabled) {
		TCompTransform* c_transform = getComponent<TCompTransform>();
		if (!c_transform) return;

		float orbitSpeed = 3.0f;
		if (EngineInput["cam_right"].isPressed()) orbitAngle += scaled_dt * orbitSpeed;
		if (EngineInput["cam_left"].isPressed()) orbitAngle -= scaled_dt * orbitSpeed;
		VEC3 back = VEC3(cosf(orbitAngle), 0.0f, sinf(orbitAngle));

		TCompTransform* playerTransform = getPlayerTransform();
		if (playerTransform) {
			c_transform->setPosition(playerTransform->getPosition() - back * _distanceToPlayer + 3 * VEC3::Up);
			c_transform->lookAt(c_transform->getPosition() - VEC3::Up, playerTransform->getPosition());
		}
	}
	if (EngineInput[_key_toggle_enabled].justPressed()) {
		_enabled = !_enabled;
	}
}