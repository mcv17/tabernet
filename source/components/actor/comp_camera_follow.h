#pragma once

#include "mcv_platform.h"
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompCameraFollow : public TCompBase
{
	DECL_SIBLING_ACCESS();

public:
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

private:
	bool  _enabled = false;
	std::string _key_toggle_enabled = "f1";
	float _distanceToPlayer = 4.0f;
	float orbitAngle;

};