#include "mcv_platform.h"
#include "comp_circular_controller.h"
#include "components/common/comp_transform.h"
#include "entity/entity_parser.h"
#include "engine.h"

DECL_OBJ_MANAGER("circular_controller", TCompCircularController);

void TCompCircularController::debugInMenu() {
  ImGui::DragFloat("Speed", &speed, 0.1f, 0.f, 10.f);
  ImGui::DragFloat("Radius", &radius, 0.1f, 0.f, 10.f);
  ImGui::LabelText("Yaw", "%f", current_yaw);
  ImGui::DragFloat3("Center", &center.x, 0.01f, -10.0f, 10.0f);
} 

void TCompCircularController::load(const json& j, TEntityParseContext& ctx) {
  speed = j.value("speed", speed);
  radius = j.value("radius", radius);
  current_yaw = j.value("yaw", current_yaw);
  center = loadVector3(j, "center");
}

void TCompCircularController::registerMsgs() {
	DECL_MSG(TCompCircularController, TMsgEntityCreated, onEntityCreated);
}

void TCompCircularController::onEntityCreated(const TMsgEntityCreated & msg) {
	Utils::dbg("Hi from onEntityCreated\n");
}

void TCompCircularController::update(float delta) {
	current_yaw += delta * speed;
  VEC3 new_pos = yawToVector(current_yaw) * radius + center;

  TCompTransform * c_trans = getComponent<TCompTransform>();
  c_trans->setPosition(new_pos);

	if (EngineInput['T'].justPressed()) {
		TEntityParseContext ctx;
		ctx.root_transform = *c_trans;
		parseScene("data/prefabs/bullet.json", ctx);

		TMsgAssignBulletOwner msg;
		// Commented due to this msg changed and this code is not used
		//msg.h_owner = CHandle(this).getOwner();
		//msg.source = c_trans->getPosition();
		//msg.destination = c_trans->getFront();

		ctx.entities_loaded[0].sendMsg(msg);
	}
  
  // destroy my entity
  // CHandle(this).getOwner().destroy();
  
  // destroy myself
  // CHandle(this).destroy();

}


