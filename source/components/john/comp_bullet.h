#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompBulletController : public TCompBase {
	CHandle h_owner;
  
  VEC3  front;
  float speed = 1.0f; // Bullet speed per second.
	float damage = 20.0f; // Bullet damage on hit.
 
	float collision_radius = 1.0f; // Bullet collision radius. Soon to dissapear in a component.
	float timeToLive = 10.f; // Time before the entity component containing the bullet is destroyed.  
	

  DECL_SIBLING_ACCESS();
  void onBulletInfoMsg(const TMsgAssignBulletOwner& msg);

public:
  void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  static void registerMsgs();
};