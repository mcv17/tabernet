#include "mcv_platform.h"
#include "comp_john_character_controller.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_transform.h"
#include "modules/module_physics.h"
#include "engine.h"
#include "input/module_input.h"

using namespace physx;

DECL_OBJ_MANAGER("john_player_controller", TCompJohnCharacterController);

void TCompJohnCharacterController::debugInMenu() {
  ImGui::Checkbox("Enabled", &enabled);
  ImGui::Checkbox("Gravity Enabled", &gravity_enabled);
  ImGui::DragFloat("Gravity", &gravity, 0.01f, -10.0f, 10.0f);
  ImGui::DragFloat("Rotation Sensibility", &rotation_sensibility, 0.01f, 0.0f, 1.0f);
  ImGui::DragFloat("Movement Sensibility", &movement_sensibility, 0.01f, 0.0f, 1.0f);
}

void TCompJohnCharacterController::load(const json& j, TEntityParseContext& ctx) {
  enabled = j.value("enabled", enabled);
  gravity_enabled = j.value("gravity_enabled", gravity_enabled);
  gravity = j.value("gravity", gravity);
  rotation_sensibility = j.value("rotation_sensibility", rotation_sensibility);
  movement_sensibility = j.value("movement_sensibility", movement_sensibility);
}

void TCompJohnCharacterController::update(float scaled_dt) {

  // Because we are moving the camera...
  const auto& but_right = EngineInput[Input::BT_MOUSE_RIGHT];
  if (but_right.isPressed())
    return;

  if (!enabled)
    return;

  TCompCollider* comp_collider = getComponent<TCompCollider>();
  if (!comp_collider || !comp_collider->controller)
    return;

  // Guardo mi transform
  TCompTransform *c_my_transform = getComponent<TCompTransform>();
  assert(c_my_transform);

  // Rotation
  float current_yaw = 0.f;
  float current_pitch = 0.f;
  c_my_transform->getAngles(&current_yaw, &current_pitch);

  float amount_rotated = rotation_sensibility * scaled_dt * 3.0f;
  if (Utils::isPressed('A'))
    current_yaw += amount_rotated;
  if (Utils::isPressed('D'))
    current_yaw -= amount_rotated;

  // Movement
  VEC3 local_mov = VEC3::Zero;
  if (Utils::isPressed('W'))
    local_mov.z = 1.f;
  if (Utils::isPressed('S'))
    local_mov.z = -1.f;
  if (Utils::isPressed('Q'))
    local_mov.x = 1.f;
  if (Utils::isPressed('E'))
    local_mov.x = -1.f;
  local_mov *= movement_sensibility * 5.0f;

  // Beware of pitch, I'm assuming it's zero.
  c_my_transform->setAngles(current_yaw, current_pitch);

  VEC3 world_mov = c_my_transform->getFront() * local_mov.z
    + c_my_transform->getLeft() * local_mov.x;

  if (gravity_enabled)
    world_mov.y = gravity;

  // disp is the displacement vector for current frame
  world_mov *= scaled_dt;
  
  comp_collider->controller->move(VEC3_TO_PXVEC3(world_mov), 0.0f, scaled_dt, PxControllerFilters());
}
