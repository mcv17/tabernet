#include "mcv_platform.h"
#include "comp_bullet.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_name.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "input/input.h"
#include "input/module_input.h"

DECL_OBJ_MANAGER("bullet_controller", TCompBulletController);

void TCompBulletController::load(const json& j, TEntityParseContext& ctx) {
	speed = j.value("speed", speed);
	damage = j.value("damage", damage);
}

void TCompBulletController::debugInMenu() {
  ImGui::DragFloat("Speed", &speed, 0.1f, 0.f, 10000.f);
	ImGui::DragFloat("Damage", &damage, 0.1f, 0.f, 10000.f);
}

void TCompBulletController::registerMsgs() {
	DECL_MSG(TCompBulletController, TMsgAssignBulletOwner, onBulletInfoMsg);
}

void TCompBulletController::onBulletInfoMsg(const TMsgAssignBulletOwner & msg) {
  TCompTransform* c_trans = getComponent<TCompTransform>();
	// Commented due to this msg changed and this code is not used
	//c_trans->lookAt(msg.source, msg.destination + msg.source);
	h_owner = msg.h_owner; // Save who generated me.
}

void TCompBulletController::update(float dt) {
	TCompTransform* c_trans = getComponent<TCompTransform>();
	if (!c_trans) return;

	// Move the bullet to its new position.
  VEC3 new_pos = c_trans->getPosition() + c_trans->getFront() * speed * dt;
  c_trans->setPosition(new_pos);

	/* Check for collisions. */
	// This will probably end up changed.
  auto om_tmx = getObjectManager<TCompTransform>();

  // This will store the handle close enough to me
  CHandle h_near;
  om_tmx->forEach([this, &h_near, c_trans](TCompTransform* c) {
		// Discard myself
		if (c_trans == c) return;

	  float distance_to_c = VEC3::Distance(c->getPosition(), c_trans->getPosition());
	  if (distance_to_c < this->collision_radius) {
		  CHandle h_candidate = CHandle(c).getOwner();

		  // Discard my sender (the teapot)
		  if (h_candidate != h_owner) 
				h_near = h_candidate;
	  }
  });

	// If a hit was detected.
  if (h_near.isValid()) {
	  CEntity *e_near = h_near;
	  TCompTransform* c_trans = getComponent<TCompTransform>();
		VEC3 direction = c_trans->getFront();
	  direction.Normalize();

	  Utils::dbg("Entity %s has been hit\n", e_near->getName());

	  // Notify the entity that he has been hit
		TMsgBulletDamage msg;
	  msg.h_sender = h_owner;      // Who send this bullet
	  msg.damage = damage;
	  msg.damageDirection = direction;
	  e_near->sendMsg(msg);

	  // Queue my owner entity as 'destroyed'
	  CHandle(this).getOwner().destroy();
	}
}