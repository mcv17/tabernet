#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "geometry/curve.h"

class TCompCamera3rdPerson : public TCompBase
{
  DECL_SIBLING_ACCESS();

public:
  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void renderDebug();

private:
  const CCurve* _curve = nullptr;
  CHandle _target;
  VEC3 _targetOffset;
  std::string _targetName;
  float _ratio = 0.f;
  float _pitchSensitivity = 1.f;
  float _yawSensitivity = 1.f;

  MAT44 _curveTransform;
};

