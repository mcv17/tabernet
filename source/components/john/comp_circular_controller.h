#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompCircularController : public TCompBase {
	float speed = 1.0f;
	float radius = 1.0f;
  float current_yaw = 0.f;
  VEC3  center;

	// Always add entity.h and this macro for fast access to the
	// getHandle and getEntity methods.
  DECL_SIBLING_ACCESS();
	void onEntityCreated(const TMsgEntityCreated & msg);

public:
  void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
	static void registerMsgs();
};