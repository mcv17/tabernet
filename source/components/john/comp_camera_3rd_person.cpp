#include "mcv_platform.h"
#include "comp_camera_3rd_person.h"
#include "components/common/comp_transform.h"
#include "engine.h"
#include "input/input.h"

DECL_OBJ_MANAGER("camera_3rd_person", TCompCamera3rdPerson);

void TCompCamera3rdPerson::debugInMenu()
{
  ImGui::Text("Target: %.s", _target.isValid() ? ((CEntity*)_target)->getName() : "...");
}

void TCompCamera3rdPerson::load(const json& j, TEntityParseContext& ctx) {
  _targetName = j.value("target", "");
  _targetOffset = loadVEC3(j, "offset");
  _ratio = j.value("ratio", _ratio);
  _curve = Resources.get(j.value("curve", ""))->as<CCurve>();
  _pitchSensitivity = j.value("pitchSensitivity", _pitchSensitivity);
  _yawSensitivity = j.value("yawSensitivity", _yawSensitivity);
}

void TCompCamera3rdPerson::update(float scaled_dt)
{
  if (!_target.isValid())
  {
    _target = getEntityByName(_targetName);

    if (!_target.isValid())
      return;
  }

  TCompTransform* cTransform = get<TCompTransform>();
  if (!cTransform)
    return;

  CEntity* eTarget = _target;
  TCompTransform* cTargetTransform = eTarget->get<TCompTransform>();
  if (!cTargetTransform)
    return;

  // pitch update
  const float ratioOffset = (EngineInput[VK_UP].value - EngineInput[VK_DOWN].value) * _pitchSensitivity * scaled_dt;
  _ratio = clamp(_ratio + ratioOffset, 0.f, 0.99999f);

  //_curveTransform = cTargetTransform->asMatrix() * cTransform->asMatrix();
  _curveTransform = cTargetTransform->asMatrix();

  const VEC3 posInCurve = _curve->evaluate(_ratio);
  const VEC3 newPos = VEC3::Transform(posInCurve, _curveTransform);
  
  const VEC3 targetPos = cTargetTransform->getPosition() + _targetOffset;

  cTransform->lookAt(newPos, targetPos);

  // yaw update
  float yaw, pitch;
  cTransform->getAngles(&yaw, &pitch);
  const float yawOffset = (EngineInput[VK_LEFT].value - EngineInput[VK_RIGHT].value) * _yawSensitivity * scaled_dt;
  yaw += yawOffset;
  cTransform->setAngles(yaw, pitch);
}

void TCompCamera3rdPerson::renderDebug()
{
  if (_curve)
  {
    CTransform tr;
    tr.fromMatrix(_curveTransform);
    _curve->renderDebug(tr);
  }
}