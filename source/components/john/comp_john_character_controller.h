#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"

class TCompJohnCharacterController : public TCompBase
{
  DECL_SIBLING_ACCESS();

public:
  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);

private:

  float rotation_sensibility = 1.0f;
  float movement_sensibility = 1.0f;
  float gravity = -9.8f;
  bool enabled = true;
  bool gravity_enabled = true;
};

