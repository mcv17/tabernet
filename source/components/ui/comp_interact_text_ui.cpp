#include "mcv_platform.h"
#include "comp_interact_text_ui.h"

DECL_OBJ_MANAGER("interact_text_ui", TCompInteractTextUI);

void TCompInteractTextUI::update(float dt) {
	if (!active) return;

	auto imGuiFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_AlwaysAutoResize;
	ImGui::SetNextWindowPos(ImVec2(_position.x * Render.getWidth(), _position.y * Render.getHeight()), 0);
	if (!ImGui::Begin("GUI_window_interact", &active, imGuiFlags)) {
		ImGui::End();
		return;
	}

	ImGui::Text(interactionText.c_str());
	ImGui::End();
}

void TCompInteractTextUI::load(const json& j, TEntityParseContext& ctx) {
	_position.x = j.value("x_coord", _position.x);
	_position.y = j.value("y_coord", _position.y);
}

void TCompInteractTextUI::debugInMenu() {}

void TCompInteractTextUI::registerMsgs() {}
