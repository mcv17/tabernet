#include "mcv_platform.h"
#include "components\ui\comp_win_ui.h"
#include "entity/entity_parser.h"
#include "engine.h"

DECL_OBJ_MANAGER("win_message_ui", TCompWinMessageUI);

void TCompWinMessageUI::update(float dt) {
	if (!active) return;

	auto imGuiFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_AlwaysAutoResize;
	ImGui::SetNextWindowPos(ImVec2(_position.x * Render.getWidth(), _position.y * Render.getHeight()), 0);
	if (!ImGui::Begin("GUI_window_win_message", &active, imGuiFlags)) {
		ImGui::End();
		return;
	}

	ImGui::Text(message.c_str());
	ImGui::Button(endText.c_str());
	if (EngineInput["interact"].justPressed())
		active = false;

	ImGui::End();
}

void TCompWinMessageUI::load(const json& j, TEntityParseContext& ctx) {
	message = j.value("message", message);
	endText = j.value("button-text", endText);
	_position.x = j.value("x_coord", _position.x);
	_position.y = j.value("y_coord", _position.y);
}

void TCompWinMessageUI::activateOnMsg(const TMsgWinMessageUI & msg) {
	active = msg.activate;
}

void TCompWinMessageUI::registerMsgs() {
	DECL_MSG(TCompWinMessageUI, TMsgWinMessageUI, activateOnMsg);
}
