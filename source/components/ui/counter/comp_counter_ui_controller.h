#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"

class TCompCounterUIController : public TCompBase {

public:

	virtual void update(float dt);
	virtual void load(const json& j, TEntityParseContext& ctx);
	virtual void debugInMenu();

	void setFontColor(Vector4 color) { _fontColor = color; }

protected:
	std::string _attribute;
	int _curValue, _maxValue, _storedValue = -1;
	Vector4 _fontColor, _fgColor;
	VEC2 _position;
};