#include "mcv_platform.h"
#include "comp_ammo_ui_controller.h"
#include "entity/entity_parser.h"
#include "components\weapons\comp_weapons_controller.h"

DECL_OBJ_MANAGER("ammo_ui_controller", TCompAmmoUIController);

void TCompAmmoUIController::update(float dt) {
	TCompWeaponManager * weaponManager = getPlayerWeaponManager();
	if (!weaponManager) return;
	TCompWeaponsController * current_weapon = weaponManager->getCurrentWeapon();

	_attribute = "Ammo (";
	_attribute += weaponManager->getCurrentWeapon()->getName() + ")";
	_curValue = current_weapon->getCurrentAmmo();
	_maxValue = current_weapon->getCapacity();
	_storedValue = current_weapon->getStoredAmmo();

	if (current_weapon->isSpecialModeActivated() == 0)
		setFontColor(Vector4(1.0f, 0.0f, 0.0f, 1.0f));
	else
		setFontColor(Vector4(0.6f, 0.6f, 0.6f, 1.0f));

	TCompCounterUIController::update(dt);
}

void TCompAmmoUIController::load(const json& j, TEntityParseContext& ctx) {
	TCompCounterUIController::load(j, ctx);
}

void TCompAmmoUIController::debugInMenu() {
	TCompCounterUIController::debugInMenu();
}