#pragma once

#include "comp_counter_ui_controller.h"
#include "components/common/comp_base.h"
#include "components\weapons\comp_weapons_manager.h"
#include "entity/entity.h"

class TCompSpecialAmmoUIController : public TCompCounterUIController {

public:

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();

private:
	DECL_TCOMP_ACCESS("Player", TCompWeaponManager, PlayerWeaponManager);
};