#include "mcv_platform.h"
#include "comp_special_ammo_ui_controller.h"
#include "entity/entity_parser.h"
#include "components\weapons\comp_weapons_controller.h"

DECL_OBJ_MANAGER("special_ammo_ui_controller", TCompSpecialAmmoUIController);

void TCompSpecialAmmoUIController::update(float dt) {
	TCompWeaponManager * weaponManager = getPlayerWeaponManager();
	if (!weaponManager) return;
	TCompWeaponsController * current_weapon = weaponManager->getCurrentWeapon();

	_attribute = "Special ammo";
	_curValue = current_weapon->getCurrentAltAmmo();
	_maxValue = current_weapon->getMaxAltCapacity();

	if (current_weapon->isSpecialModeActivated())
		setFontColor(Vector4(1.0f, 0.0f, 0.0f, 1.0f));
	else
		setFontColor(Vector4(0.6f, 0.6f, 0.6f, 1.0f));

	TCompCounterUIController::update(dt);
}

void TCompSpecialAmmoUIController::load(const json& j, TEntityParseContext& ctx) {
	TCompCounterUIController::load(j, ctx);
}

void TCompSpecialAmmoUIController::debugInMenu() {
	TCompCounterUIController::debugInMenu();
}