#include "mcv_platform.h"
#include "comp_holywater_ui_controller.h"
#include "entity/entity_parser.h"
#include "components/player/comp_holy_water.h"

DECL_OBJ_MANAGER("holywater_ui_controller", TCompHolyWaterUIController);


void TCompHolyWaterUIController::update(float dt) {
	TCompHolyWater* holyWaterComponent = getPlayerHolyWaterComponent();
	if (!holyWaterComponent) return;

	_attribute = "Holy water";
	_curValue = holyWaterComponent->getCurrentCharges();
	_maxValue = holyWaterComponent->getMaxCharges();

	TCompCounterUIController::update(dt);
}

void TCompHolyWaterUIController::load(const json& j, TEntityParseContext& ctx) {
	TCompCounterUIController::load(j, ctx);
}

void TCompHolyWaterUIController::debugInMenu() {
	TCompCounterUIController::debugInMenu();
}