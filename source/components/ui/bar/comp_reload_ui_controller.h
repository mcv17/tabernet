#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/weapons/comp_weapons_manager.h"

class TCompReloadUIController : public TCompBase {
public:
	DECL_SIBLING_ACCESS();

	void update(float dt);
	void debugInMenu();
	void startReload(float time, float activeReloadOffset, float penaltyTime);
	void addPenaltyTime(float time);
	void abortReload();
	void finishReload();
	bool checkIfSuccess();
	void changeProgressColor(VEC4 color);
	void resetUIAlpha();
	void setActivePressed(bool status);

private:
	DECL_TCOMP_ACCESS("Player", TCompWeaponManager, WeaponManager);
	bool _started = false;
	bool _pressed = false;
	float _curValue = 0.0f;
	float _maxValue = 0.0f;
	float _activeTargetValue = 0.0f;
	float _penaltyTime = 0.0f;
	// Helper offset before and after target that gives success if inside
	float _offSet = 0.05f;
	// Offset in pixels 
	float _screenOffset = 7.0f; 
	float _upperScreenLimit = 225.0f; //0-255 px of screen that can go the target widget
	float _targetWidgetOffset = 10.0f;
	// If you are spying on this code and have doubts, send an email to manutheking1991@gmail.com
	float _magicRelation = 1.062f;  

	std::string _fadeOutAnimation = "ActiveReloadFadeOut";

	VEC4 _defaultColor = VEC4(1.0f, 1.0f, 1.0f, 1.0f);
	VEC4 _failColor = VEC4(0.75f, 0.0f, 0.0f, 1.0f);
	VEC4 _successColor = VEC4(0.0f, 0.75f, 0.0f, 1.0f);

	std::string _activeUIParent = "active_reload";
	std::string _activeUIPortrait = "active_portrait";
	std::string _activeUIProgress = "active_progress";
	std::string _activeUITarget = "active_success";
	
	std::string _activeReloadSuccessSound = "event:/Weapons/ActiveReloadSuccess";
	std::string _activeReloadFailedSound = "event:/Weapons/ActiveReloadFail";


	/* DEBUG */
	bool _activateDebug = false;
};