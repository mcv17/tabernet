#include "mcv_platform.h"
#include "comp_reload_ui_controller.h"
#include "entity/entity_parser.h"
#include "components/weapons/comp_weapons_controller.h"
#include "components/weapons/comp_smg_controller.h"

DECL_OBJ_MANAGER("reload_ui_controller", TCompReloadUIController);

void TCompReloadUIController::update(float dt) {
	if (_activateDebug) {
		resetUIAlpha();
		if (UI::CImage* target = EngineUI.getImageWidgetByName(_activeUIParent, _activeUITarget))
			target->getParams()->position.y = 0.85f * _upperScreenLimit + _screenOffset;
		if (UI::CImage* progress = EngineUI.getImageWidgetByName(_activeUIParent, _activeUIProgress)) {
			float targetValueUI = _curValue / _maxValue;

			progress->getParams()->scale.y = targetValueUI;
			progress->getImageParams()->maxUV.y = targetValueUI;
		}
	}	

	if (_started) {
		_curValue += dt;
		if (_curValue <= _maxValue) {

			float targetValue = _curValue + dt;
			if (targetValue > _maxValue) targetValue = _maxValue;

			float targetValueUI = targetValue / _maxValue ;

			if (UI::CImage* progress = EngineUI.getImageWidgetByName(_activeUIParent, _activeUIProgress)) {
				progress->getParams()->scale.y = targetValueUI;
				progress->getImageParams()->maxUV.y = targetValueUI;
			}

		}
		else {
			auto weaponManager = getWeaponManager();
			if (weaponManager) {
				TCompWeaponsController * weapon = weaponManager->getCurrentWeapon();
				weapon->resetAmmo();
				weapon->resetReload();
			}
			changeProgressColor(_successColor);
			finishReload();
			EngineAudio.playEvent(_activeReloadSuccessSound);
		}
	}
}

void TCompReloadUIController::debugInMenu()
{
	ImGui::Text("Activate debug");
	ImGui::SameLine();
	ImGui::Checkbox("##debg", &_activateDebug);
	ImGui::Text("Current value: ");
	ImGui::SameLine();
	ImGui::DragFloat("##floatdrag", &_curValue, 0.001f, 0.0f, 3.0f);
	ImGui::Text("Max value: ");
	ImGui::SameLine();
	ImGui::DragFloat("##floatdrag2aa", &_maxValue, 0.001f, 0.0f, 3.0f);
}

void TCompReloadUIController::startReload(float time, float activeReloadOffset, float penaltyTime) {
	_started = true;
	_pressed = false;
	_curValue = 0.0f;
	_maxValue = time;
	_activeTargetValue = activeReloadOffset;
	_penaltyTime = penaltyTime;
	

	if (UI::CImage* target = EngineUI.getImageWidgetByName(_activeUIParent, _activeUITarget))
		target->getParams()->position.y = activeReloadOffset * _upperScreenLimit;

	resetUIAlpha();
}

void TCompReloadUIController::addPenaltyTime(float time)
{
	_maxValue += time;
	changeProgressColor(_failColor);
	EngineAudio.playEvent(_activeReloadFailedSound);
}

void TCompReloadUIController::finishReload()
{
	_maxValue = 0;
	_started = false;
	if (UI::CImage* portrait = EngineUI.getImageWidgetByName(_activeUIParent, _activeUIPortrait))
		portrait->playAnimation("ActiveReloadFadeOut", false);
	if (UI::CImage* progress = EngineUI.getImageWidgetByName(_activeUIParent, _activeUIProgress))
		progress->playAnimation("ActiveReloadFadeOut", false);
	if (UI::CImage* target = EngineUI.getImageWidgetByName(_activeUIParent, _activeUITarget))
		target->playAnimation("ActiveReloadFadeOut", false);
}

void TCompReloadUIController::abortReload() {
	finishReload();
}

bool TCompReloadUIController::checkIfSuccess()
{
	float fromTargetPos = _activeTargetValue - (_offSet * _magicRelation);
	float toTargetPos   = _activeTargetValue + (_offSet * _magicRelation);
	float curValueUI = _curValue / _maxValue * _magicRelation;//  +(_targetWidgetOffset / _upperScreenLimit);

	bool success = curValueUI >= fromTargetPos && curValueUI <= toTargetPos;

	if (success) {
		auto weaponManager = getWeaponManager();
		if (weaponManager) {
			TCompWeaponsController* weapon = weaponManager->getCurrentWeapon();
			weapon->resetAmmo();
			weapon->resetReload();
		}
		finishReload();
		changeProgressColor(_successColor);
		EngineAudio.playEvent(_activeReloadSuccessSound);
	}
	else
		addPenaltyTime(_penaltyTime);

	return success;
}

void TCompReloadUIController::changeProgressColor(VEC4 color)
{
	if (UI::CImage* progress = EngineUI.getImageWidgetByName(_activeUIParent, _activeUIProgress))
		progress->getImageParams()->color = color;
}

void TCompReloadUIController::resetUIAlpha()
{
	if (UI::CImage* portrait = EngineUI.getImageWidgetByName(_activeUIParent, _activeUIPortrait))
		portrait->getImageParams()->color = _defaultColor;
	if (UI::CImage* progress = EngineUI.getImageWidgetByName(_activeUIParent, _activeUIProgress))
		progress->getImageParams()->color = _defaultColor;
	if (UI::CImage* target = EngineUI.getImageWidgetByName(_activeUIParent, _activeUITarget))
		target->getImageParams()->color = _defaultColor;
}

void TCompReloadUIController::setActivePressed(bool status)
{
	_pressed = status;
}

