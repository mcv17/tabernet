#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"

class TCompInteractTextUI : public TCompBase {
	bool active = false;

	CHandle interactedObject;
	std::string interactionText = nullptr;
	VEC2 _position;

public:

	void update(float dt);
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
};