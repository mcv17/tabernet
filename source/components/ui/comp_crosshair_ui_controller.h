#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity\common_msgs.h"
#include "ui/ui_widget.h"
#include "ui/widgets/ui_image.h"

class TCompCrosshairUIController : public TCompBase {

public:
	~TCompCrosshairUIController();
	void update(float dt) {}
	void load(const json& j, TEntityParseContext& ctx) {}
	void debugInMenu();
	void onEntityCreated(const TMsgEntityCreated& msg);
	static void registerMsgs();


	void draw(float outerRadius, float innerRadius, float size = 2.0f);

	DECL_SIBLING_ACCESS();

protected:
	Vector4 _outerColor = Vector4(1.0f, 1.0f, 1.0f, 1.0f), _innerColor = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	

	UI::CWidget crosshairWidget;
	std::vector<UI::CImage*> bars;
	std::vector<VEC2> barPosition;
	std::vector<VEC2> initialBarPosition;
private:
	bool getCrosshairBars();
};