#pragma once

#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_base.h"
#include "ui/widgets/ui_text_font.h"

class TCompFloatingDamage : public TCompBase {

private:

	std::string _fontName = "DINCondensed_26_bold";
	std::string _lastPlayedAnim = "";
	VEC4 _textColor = VEC4(1, 1, 1, 1);
	VEC4 _outlineColor = VEC4(0, 0, 0, 1);
	VEC4 _textColorCritical = VEC4(0, 0, 1, 1);
	VEC4 _outlineColorCritical = VEC4(1, 1, 1, 1);
	VEC2 _offset = VEC2();
	VEC3 _lastPosition;
	float _textTargetScale = 12.0f;
	float _scaleMultiplier = 1.0f;
	float _leaveDistance = -100.0f;
	float _aliveTime = 1.0f;
	float _timeToStartVanish = 0.15f;
	std::vector<UI::CTextFont*> _widgets;

	void onHitDamage(const TMsgFloatingDamageText& msg);

public:
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
	static void declareInLua() {}

	DECL_SIBLING_ACCESS();
};