#include "mcv_platform.h"
#include "components\ui\comp_lose_ui.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "components/game/comp_checkpoint.h"

DECL_OBJ_MANAGER("lose_message_ui", TCompLoseMessageUI);

void TCompLoseMessageUI::update(float dt) {
	if (!active) return;

	/*
	auto imGuiFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_AlwaysAutoResize;
	ImGui::SetNextWindowPos(ImVec2(_position.x * Render.getWidth(), _position.y * Render.getHeight()), 0);
	if (!ImGui::Begin("GUI_window_lose_message", &active, imGuiFlags)) {
		ImGui::End();
		return;
	}

	ImGui::Text(message.c_str());
	ImGui::Button(endText.c_str());
	if (EngineInput["interact"].justPressed()) {
		// maybe we'll use this method in a script for the next UI
		TCompCheckpoint::restorePlayer();

		active = false;
	}
	
	ImGui::End();
	*/
}

void TCompLoseMessageUI::load(const json& j, TEntityParseContext& ctx) {
	message = j.value("message", message);
	endText = j.value("button-text", endText);
	_position.x = j.value("x_coord", _position.x);
	_position.y = j.value("y_coord", _position.y);
}

void TCompLoseMessageUI::activateOnMsg(const TMsgLoseMessageUI & msg) {
	active = msg.activate;
}

void TCompLoseMessageUI::registerMsgs() {
	DECL_MSG(TCompLoseMessageUI, TMsgLoseMessageUI, activateOnMsg);
}
