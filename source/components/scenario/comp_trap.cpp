#include "mcv_platform.h"
#include "components/scenario/comp_trap.h"
#include "modules/module_physics.h"
#include "components/common/comp_render.h"
#include "components/common/comp_tags.h"
#include "components/controllers/comp_character_controller.h"
#include "components/player/comp_input_player_movement.h"

DECL_OBJ_MANAGER("trap", TCompTrap);

void TCompTrap::load(const json& j, TEntityParseContext& ctx)
{
	//Check poison
	if (j.count("poison")) {
		poison = true;
		auto& poisonObject = j["poison"];
		poisonTickDmg = poisonObject.value("poisonTickDmg", 30.0f);
		posionTickInterval = poisonObject.value("posionTickInterval", 30.0f);
		poisonTickCount = poisonObject.value("poisonTickCount", 30.0f);
	}
	//Check movement
	if (j.count("movement")) {
		movement = true;
		auto& movementObject = j["movement"];
		movementDuration = movementObject.value("movementDuration", 30.0f);
	}
}

void TCompTrap::update(float dt) {
}

void TCompTrap::onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter) {
	//Check if enemy
	CEntity* e = trigger_enter.h_entity;
	TCompTags* e_tags = e->getComponent<TCompTags>();

	if (!e_tags || !e_tags->hasTag(Utils::getID("player")))
		return;

	if (poison) {
		TCompHealth* c_health = e->getComponent<TCompHealth>();
		c_health->setTickDamage(poisonTickDmg, posionTickInterval, poisonTickCount);
	}

	if (movement) {
		TCompInputPlayerMovement* c_input = e->getComponent<TCompInputPlayerMovement>();
		c_input->lockMovement(movementDuration);
	}
}

void TCompTrap::onEntityDead(const TMsgEntityDead& msg) {
	this->getEntity().destroy();
}

void TCompTrap::registerMsgs() {
	DECL_MSG(TCompTrap, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompTrap, TMsgEntityDead, onEntityDead);
}

void TCompTrap::debugInMenu()
{

}

void TCompTrap::renderDebug() {
}