#include "mcv_platform.h"
#include "comp_totem.h"
#include "modules/module_physics.h"
#include "components/common/comp_render.h"
#include "components/common/comp_tags.h"
#include "components/controllers/comp_character_controller.h"
#include "../common/comp_particles.h"
#include "../common/comp_name.h"
#include "logic/logic_manager.h"
#include "particles/particle_system.h"
#include "particles/module_particles.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("totem", TCompTotem);

TCompTotem::~TCompTotem()
{
	TMsgRemoveBuff msg;
	for (auto value : _enemies) {
		if (value.isValid()) {
			CEntity* e = value;
			e->sendMsg(msg);
		}		
	}
}

void TCompTotem::load(const json& j, TEntityParseContext& ctx) 
{
	_armour = j.value("armour", 1.05f);
	_regenerationPerSecond = j.value("regenerationPerSecond", 1.05f);
	_damage = j.value("damage", 1.05f);
	_bulletSpeed = j.value("bulletSpeed", 1.05f);
	_decisionMakingSpeed = j.value("decisionMakingSpeed", 1.05f);
	_decisionMakingIntelligence = j.value("decisionMakingIntelligence", 1.05f);
	_detectionDistance = j.value("detectionDistance", 1.05f);
	_timeToDie = j.value("timeToDie", 3.0f);
	_dyingTimer = CClock(_timeToDie);

	_hiddenYLevel = j.value("hiddenYLevel", _hiddenYLevel);
	_hiddenAngle = j.value("hiddenAngle", _hiddenAngle);
	_spawnTranslationInterpolator = j.value("spawnTranslationInterpolator", _spawnTranslationInterpolator);
	_spawnRotationInterpolator = j.value("spawnRotationInterpolator", _spawnRotationInterpolator);
	
	_spawningTimer.setTime(j.value("timeToShow", 1.5f));
	_spawnParticleDust = j.value("dustParticles", "data/particles/totem_dust.particles");
	_spawnParticleFlint = j.value("flintParticles", "data/particles/totem_flint.particles");
}

void TCompTotem::update(float dt) {
	if (!active) return;

	switch (_currentState) {
	case ETotemState::HIDDEN:
		break;
	case ETotemState::SPAWNING:
		updateIfSpawning();
		break;
	case ETotemState::VISIBLE:
		updateIfNormal();
		break;
	case ETotemState::DIYNG:
		updateIfDying();
		break;
	case ETotemState::DEAD:
		break;
	}
}


void TCompTotem::updateIfSpawning() {
	TCompTransform* transform = getComponent<TCompTransform>();
	Vector3 position = transform->getPosition();
	if (_spawningTimer.isStarted() && _spawningTimer.isFinished()) {
		_spawningTimer.stop();

		// Spawn active particles
		if (compParticles.isValid()) {
			TCompParticles* particles = compParticles;
			particles->setActive(true);
		}

		EngineLogicManager.removeSpawnedParticle(_spawnParticleDustSystem);
		EngineLogicManager.removeSpawnedParticle(_spawnParticleFlintSystem);

		_currentState = ETotemState::VISIBLE;
	}
	else {
		float ratioOfSpawning = _spawningTimer.elapsed() / _spawningTimer.initialTime();
		float yaw, pitch;
		transform->getAngles(&yaw, &pitch);
		_spawnTranslationInterpolatorFunction = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(_spawnTranslationInterpolator);
		_spawnRotationInterpolatorFunction = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(_spawnRotationInterpolator);
		position.y = _spawnRotationInterpolatorFunction(_initialYLevel - _hiddenYLevel, _initialYLevel, ratioOfSpawning);
		yaw = _spawnRotationInterpolatorFunction(_initialYaw - _hiddenAngle, _initialYaw, ratioOfSpawning);
		
		transform->setPosition(position);
		transform->setAngles(yaw, pitch);
	}

}

void TCompTotem::updateIfNormal() {
	for (auto it = _enemies.begin(); it != _enemies.end(); it++) {
		if ((*it).isValid()) {
			if (checkIfEnemyDead((*it))) {
				_enemies.erase(it);
				break;
			}
		}
		else {
			_enemies.erase(it);
			break;
		}
	}
}

void TCompTotem::updateIfDying() {
	if (checkIfTotemIsDeadToRemove())
		destroyTotem();
}

void TCompTotem::destroyTotem() {
	TCompTransform* transform = _transform;
	Vector3 position = transform->getPosition();
	position.y -= _hiddenYLevel;
	transform->setPosition(position);
	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	transform->setAngles(_initialYaw - _hiddenAngle, pitch);

	getEntity().sendMsgToComp<TCompCollider>(TMsgToogleComponent{ false });
	if (compParticles.isValid()) {
		TCompParticles* particles = compParticles;
		particles->setActive(false);
	}
	TCompRender* render = getComponent<TCompRender>();
	render->setCurrentState(0);

	_currentState = ETotemState::DEAD;
}

void TCompTotem::reset() {
	if (_currentState != ETotemState::HIDDEN) {
		if (_currentState != ETotemState::DEAD)
			destroyTotem();

		_dyingTimer.stop();
		_spawningTimer.stop();
		_spawnParticleDustSystem = nullptr;
		_spawnParticleFlintSystem = nullptr;
		_enemies.clear();
		dynamicEntitiesInside = 0;
		_currentState = ETotemState::HIDDEN;
	}
}

void TCompTotem::onEntityCreated(const TMsgEntityCreated & msg) {
	getEntity().sendMsgToComp<TCompCollider>(TMsgToogleComponent{ false });
	compParticles = getComponent<TCompParticles>();
	if (compParticles.isValid()) {
		TCompParticles* particles = compParticles;
		particles->setActive(false);
	}
	_transform = getComponent<TCompTransform>();

	TCompTransform* transform = _transform;
	Vector3 position = transform->getPosition();
	_initialYLevel = position.y;
	position.y -= _hiddenYLevel;
	transform->setPosition(position);
	float pitch;
	transform->getAngles(&_initialYaw, &pitch);
	transform->setAngles(_initialYaw - _hiddenAngle, pitch);
}

void TCompTotem::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompTotem::onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter) {
	//Check if enemy
	CEntity* e = trigger_enter.h_entity;
	TCompTags * e_tags = e->getComponent<TCompTags>();
	if (!e_tags) return;

	TCompName * name = e->getComponent<TCompName>();

	bool isPlayer = e_tags->hasTag(Utils::getID("player"));
	bool isEnemy = e_tags->hasTag(Utils::getID("enemy"));

	if (isPlayer || isEnemy) {
		if (dynamicEntitiesInside == 0) {
			TCompParticles * particles = compParticles;
			if (particles)
				particles->setSystemActive(true);
		}
		dynamicEntitiesInside++;
	}

	// Only do this if enemy.
	if (!isEnemy) return;

	TCompCharacterController * c_con = e->getComponent<TCompCharacterController>();
	if (!c_con) return;

	if (c_con->isDead()) //Flying enemy due to the railgun is already dead
		return;
	if (std::find(_enemies.begin(), _enemies.end(), trigger_enter.h_entity) != _enemies.end()) { //else check if new to the vector and needs to be added
		Utils::dbg("[TCompTotem::onTriggerEnter] Entity already on the enemies list wtf.\n");
	}
	else {
		_enemies.push_back(trigger_enter.h_entity);

		TMsgApplyBuff msg;
		msg.armour = _armour;
		msg.regenerationPerSecond = _regenerationPerSecond;
		msg.damage = _damage;
		msg.bulletSpeed = _bulletSpeed;
		msg.decisionMakingSpeed = _decisionMakingSpeed;
		msg.decisionMakingIntelligence = _decisionMakingIntelligence;
		msg.detectionDistance = _detectionDistance;

		CEntity* e = trigger_enter.h_entity;
		e->sendMsg(msg);
	}
}

void TCompTotem::onTriggerExit(const TMsgEntityTriggerExit& trigger_exit) {
	TMsgRemoveBuff msg;
	CEntity* e = trigger_exit.h_entity;
	e->sendMsg(msg);

	auto endIt = std::remove_if(_enemies.begin(), _enemies.end(), [trigger_exit](const CHandle& chandle) {
		return chandle.asVoidPtr() == trigger_exit.h_entity.asVoidPtr();
	});
	_enemies.erase(endIt, _enemies.end());

	dynamicEntitiesInside--;
	if (dynamicEntitiesInside == 0) {
		TCompParticles * particles = compParticles;
		if (particles)
			particles->setSystemActive(false);
	}
}

void TCompTotem::onEntityDead(const TMsgEntityDead& msg) {
	// Set totem dying.
	_currentState = ETotemState::DIYNG;
	_dyingTimer.start();
	
	// Activate the vanish mesh state.
	LogicManager::Instance().setStatusVanishEntity(CHandle(this).getOwner());

	// Activate particles on destruction.
	TCompTransform* c_trans = _transform;
	LogicManager::Instance().SpawnParticle("data/particles/vanish_totem.particles", c_trans->getPosition());
}

void TCompTotem::registerMsgs() {
	DECL_MSG(TCompTotem, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompTotem, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompTotem, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompTotem, TMsgEntityTriggerExit, onTriggerExit);
	DECL_MSG(TCompTotem, TMsgEntityDead, onEntityDead);
}

void TCompTotem::declareInLua() {
	EngineScripting.declareComponent<TCompTotem>(
		"TCompTotem",
		"spawn", &TCompTotem::spawn,
		"reset", &TCompTotem::reset
		);
}

bool TCompTotem::checkIfEnemyDead(CEntity * e) {
	TCompHealth * health = e->getComponent<TCompHealth>();
	if (health)
		return health->isDead();
	return false;
}

void TCompTotem::debugInMenu()
{
	ImGui::DragFloat("Power cooldown", &_armour, 1.05f, 0.05f, 5.f);

	if (ImGui::Button("Spawn totem"))
		_currentState = ETotemState::SPAWNING;
}

void TCompTotem::renderDebug()
{
	TCompTransform* totem_trans = getComponent<TCompTransform>();
	for (auto enemy : _enemies) {
		if (!enemy.isValid())
			continue;
		CEntity* e = enemy;
		TCompTransform* enemy_trans = e->getComponent<TCompTransform>();
		if (!enemy_trans)
			continue;
		drawLine(totem_trans->getPosition() + VEC3(0.f, 2.f, 0.f), enemy_trans->getPosition() + VEC3(0.f, 2.f, 0.f), Vector4(0, 1, 0, 1));
	}
}

void TCompTotem::spawn() {
	if (_currentState == ETotemState::HIDDEN) {
		_spawningTimer.start();
		_currentState = ETotemState::SPAWNING;

		TCompTransform* transform = _transform;
		Vector3 position = transform->getPosition() + Vector3::Up * _hiddenYLevel;
		_spawnParticleDustSystem = EngineLogicManager.SpawnParticle(_spawnParticleDust, position);
		_spawnParticleFlintSystem = EngineLogicManager.SpawnParticle(_spawnParticleFlint, position + Vector3::Up * 0.2f);
		getEntity().sendMsgToComp<TCompCollider>(TMsgToogleComponent{ true });
	}
}

bool TCompTotem::checkIfTotemIsDeadToRemove() {
	if (_dyingTimer.isStarted() && _dyingTimer.isFinished()) {
		_dyingTimer.stop();
		return true;
	}
	return false;
}