#pragma once

#include "entity\entity.h"
#include "entity\common_msgs.h"
#include "..\common\comp_base.h"

class TEntityParseContext;
class TCompTransform;
class TCompRigidBody;

class TCompDoor : public TCompBase {
	DECL_SIBLING_ACCESS();

	enum DoorState {
		STOPPED,
		OPENING,
		CLOSING
	};

	std::string RightDoorName;
	std::string LeftDoorName;
	CHandle ownTransform;
	CHandle RightDoorTransformComponent;
	CHandle RightDoorRbComponent;
	CHandle LeftDoorTransformComponent;
	CHandle LeftDoorRbComponent;
	Quaternion LeftDoorInitialRotation;
	Quaternion RightDoorInitialRotation;

	DoorState CurrentState = STOPPED;
	bool isDoorOpened = false;
	int RotationSign = 1;
	bool UsesMeshRotatedForRightDoor = true;

	Vector3 AngularVelocityPerSecond = Vector3(0.0, 0.4, 0.0);
	float StopRotationAngle = DEG2RAD(90.0f);

	std::string openDoorSoundEvent = "";
	std::string closeDoorSoundEvent = "";

	/* Callbacks. */
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);

	void RotateRightDoor(const Vector3 & doorForwardVector, TCompTransform * transform, TCompRigidBody * rigidBody);
	void RotateLeftDoor(const Vector3 & doorForwardVector, TCompTransform * transform, TCompRigidBody * rigidBody);
	void CheckDoorStop(TCompRigidBody * rRigidBody, TCompRigidBody * lRigidBody);

public:

	void update(float dt);
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
	static void declareInLua();

	void OpenDoors();
	void CloseDoors();
	void StopDoors();
	void ResetDoors();
};