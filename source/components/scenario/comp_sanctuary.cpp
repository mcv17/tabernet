#include "mcv_platform.h"
#include "comp_sanctuary.h"
#include "components/common/comp_render.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_collider.h"

DECL_OBJ_MANAGER("sanctuary", TCompSanctuary);

void TCompSanctuary::registerMsgs() {
	DECL_MSG(TCompSanctuary, TMsgPlayerInteractionTriggerEnter, showInteractionFeedback);
	DECL_MSG(TCompSanctuary, TMsgPlayerInteractionTriggerExit, hideInteractionFeedback);
	DECL_MSG(TCompSanctuary, TMsgPlayerIsInteracting, onInteract);
}

void TCompSanctuary::declareInLua() {
	EngineScripting.declareComponent<TCompSanctuary>
	(
		// name in LUA
		"TCompSanctuary",
		"HasBeenActivated", &TCompSanctuary::HasBeenActivated
	);
}

void TCompSanctuary::update(float dt) {}

void TCompSanctuary::load(const json & j, TEntityParseContext& ctx) {}

void TCompSanctuary::debugInMenu() {}

void TCompSanctuary::onInteract(const TMsgPlayerIsInteracting & msg) {
	EngineAudio.playEvent(AUDIO_FOUNTAIN);
	CEntity * player = getEntityByName("Player");
	player->sendMsg(TMsgFullRestore());
	deactivate();
}

void TCompSanctuary::deactivate() {
	TCompRender * render = getComponent<TCompRender>();
	TCompTags * tags = getComponent<TCompTags>();

	if (render) render->setColor(Vector4(0.5f, 0.5f, 0.5f, 1.0f));
	tags->delTag(Utils::getID(TCompInteractor::INTERACTIBLE_TAG));
}

bool TCompSanctuary::HasBeenActivated() {
	TCompTags * tags = getComponent<TCompTags>();
	if (!tags) return false;
	return tags->hasTag(Utils::getID(TCompInteractor::INTERACTIBLE_TAG)) == false;
}

void TCompSanctuary::showInteractionFeedback(const TMsgPlayerInteractionTriggerEnter & msg) {
	TCompRender * render = getComponent<TCompRender>();
	if (!render) return;

	// TODO: Change after 1st milestone
	_tempColor = render->getColor();
	render->setColor(Vector4(1.0f, 1.0f, 0.0f, 1.0f));
}

void TCompSanctuary::hideInteractionFeedback(const TMsgPlayerInteractionTriggerExit & msg) {
	TCompRender * render = getComponent<TCompRender>();
	if (!render) return;

	render->setColor(_tempColor); // TODO: Change after 1st milestone
}