#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/common/comp_collider.h"

class TCompTrap : public TCompBase {

public:

	DECL_SIBLING_ACCESS();

	static void registerMsgs();

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();


private:
	//Poison Trap
	bool poison = false;
	float poisonTickDmg;
	float posionTickInterval;
	int poisonTickCount;

	//Movement Trap
	bool movement = false;
	float movementDuration;
	
	void onTriggerEnter(const TMsgEntityTriggerEnter & trigger_enter);
	void onEntityDead(const TMsgEntityDead& msg);
};