#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/common/comp_collider.h"
#include "utils/time.h"
#include "particles/particle_system.h"

class TCompTotem : public TCompBase {

public:
	~TCompTotem();

	DECL_SIBLING_ACCESS();

	static void registerMsgs();
	static void declareInLua();

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();

	void spawn();
	void reset();

private:
	enum class ETotemState {
		HIDDEN, SPAWNING, VISIBLE, DIYNG, DEAD
	} _currentState = ETotemState::HIDDEN;

	CHandle compParticles, _transform;

	float _armour;
	float _regenerationPerSecond;
	float _damage;
	float _bulletSpeed;
	float _decisionMakingSpeed;
	float _decisionMakingIntelligence;
	float _detectionDistance;

	int dynamicEntitiesInside = 0;
	std::vector<CHandle> _enemies;

	float _timeToDie;
	CClock _dyingTimer, _spawningTimer;
	float _hiddenYLevel = 1.0f, _initialYLevel;
	float _hiddenAngle = 2.0f * M_PI, _initialYaw;
	std::string _spawnTranslationInterpolator = "linear";
	std::string _spawnRotationInterpolator = "linear";
	std::string _spawnParticleDust = "", _spawnParticleFlint = "";
	particles::CSystem* _spawnParticleDustSystem = nullptr;
	particles::CSystem* _spawnParticleFlintSystem = nullptr;
	std::function<float(float, float, float)> _spawnTranslationInterpolatorFunction, _spawnRotationInterpolatorFunction;

	void updateIfSpawning();
	void updateIfNormal();
	void updateIfDying();
	void destroyTotem();

	bool checkIfEnemyDead(CEntity * e);
	bool checkIfTotemIsDeadToRemove();

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onLogicStatus(const TMsgLogicStatus & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & trigger_enter);
	void onTriggerExit(const TMsgEntityTriggerExit & trigger_exit);
	void onEntityDead(const TMsgEntityDead& msg);
};