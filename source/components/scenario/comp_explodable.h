#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/entity_parser.h"
#include "components/common/comp_collider.h"

#define AUDIO_RIFLE_GRENADE_HIT "event:/RifleSpecialHit"

class TCompExplodable : public TCompBase {

public:
	DECL_SIBLING_ACCESS();

	static void registerMsgs();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();

	void setRadius(float innerRadius, float outerRadius) { _innerRadius = innerRadius; _outerRadius = outerRadius; }
	void setPower(float power) { _power = power; }
	void setPosition(VEC3 pos) { _position = pos; }

	void explode();
	void onExplode(const TMsgExplode &msg);

private:
	/** FUNCTIONS **/
	void generateDebris();

	/** VARIABLES **/
	// Data
	float _innerRadius = 2.0f;
	float _outerRadius = 2.0f;
	float _power = 10.0f;
	VEC3 _position = VEC3(0, 0, 0);
	bool _isDead = false;
	bool _alreadyExploded = false;
	std::string _debrisPath = "";
	std::string _blastPath = "";
	std::string _onExplosionParticles = "";
	std::vector<std::pair<std::string, int>> _particles;
	CHandle _materialBuffer;
	std::string _explosionSoundEvent = "";

	void onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter);
	void onEntityCreated(const TMsgEntityCreated& msg);
	void onResetComponent(const TMsgResetComponent& msg);
	void onBulletDamage(const TMsgBulletDamage& msg);

};