#include "mcv_platform.h"
#include "status_effect.h"

CStatusEffectFactory CStatusEffect::_factory;

CStatusEffect* CStatusEffect::getStatusEffectByName(const std::string& name) {
	return _factory.getStatusEffect(name);
}

void CStatusEffect::loadTime(const json & j) {
	if (j.count("time")) {
		auto& jTime = j["time"];
		if (jTime == "infinite")
			setInfiniteTime(true);
		else
			_timer.setTime(jTime);
	}
}

void CStatusEffect::loadCustomSpecifier(const json & j) {
	if (j.count("specifier"))
		customSpecifier = j["specifier"];
}

void CStatusEffect::load(const json & j) {
	loadTime(j);
	loadCustomSpecifier(j);
}

void CStatusEffect::init(CHandle entity) {
	_timer.start();
	_entity = entity;
	onEffectAcquired();
}

void CStatusEffect::update(float dt) {
	onEffectUpdate(dt);
}

void CStatusEffect::remove() {
	onEffectRemoved();
}

bool CStatusEffect::exitCondition() {
	return timeUp();
}

std::string CStatusEffect::getName() {
	return _name; 
}

std::string CStatusEffect::getId() const {
	if (customSpecifier == "")
		return _name + "_" + getIdSpecifier();
	else
		return _name + "_" + customSpecifier;
}

bool CStatusEffect::isOfType(const std::string_view& type) const {
	return _name._Starts_with(type);
}

void CStatusEffect::setTime(float time) {
	_timer.setTime(time);
}

void CStatusEffect::addTime(float time) {
	_timer.setTime(_timer.remaining() + time);
}

void CStatusEffect::setInfiniteTime(bool isInfinite) {
	_isInfinite = isInfinite;
}

CStatusEffect::StaticConstructor CStatusEffect::_staticConstr;