#include "mcv_platform.h"
#include "comp_status_effects.h"

DECL_OBJ_MANAGER("status_effects", TCompStatusEffects)

TCompStatusEffects::~TCompStatusEffects() {
	clearStatusEffects();
}

void TCompStatusEffects::update(float dt) {
	std::unordered_map<std::string, CStatusEffect*>::iterator it = _statusEffects.begin();
	while (it != _statusEffects.end()) {
		CStatusEffect& eff = (*it->second);
		// Remove status effect if needed
		if (!eff.isInfiniteTime() && eff.exitCondition()) {
			eff.remove();
			delete it->second;
			it = _statusEffects.erase(it);
		}
		else {
			eff.update(dt);
			it++;
		}
	}
}

void TCompStatusEffects::registerMsgs()
{
	DECL_MSG(TCompStatusEffects, TMsgClearStatusEffects, onClearStatusEffects);
	DECL_MSG(TCompStatusEffects, TMsgResetComponent, onResetComponent);
}

bool TCompStatusEffects::hasStatusEffect(const std::string & name) const {
	return _statusEffects.size() > 0 && _statusEffects.count(name) > 0;
}

bool TCompStatusEffects::hasStatusEffect(const CStatusEffect & eff) const {
	return hasStatusEffect(eff.getId());
}

bool TCompStatusEffects::hasStatusEffectType(const std::string & name) const {
	return _statusEffectTypes.size() > 0 && _statusEffectTypes.count(name) > 0;
}

void TCompStatusEffects::addStatusEffect(CStatusEffect* eff) {
	const std::string effId = eff->getId();
	if (hasStatusEffect(*eff)) {
		_statusEffects[effId]->overrideEffect(*eff);
	}
	else {
		eff->init(getEntity());
		// Add status effect
		_statusEffects[eff->getId()] = eff;
		// Add status effect name to active types map
		const std::string& name = eff->getName();
		if (_statusEffectTypes.count(name) > 0)
			_statusEffectTypes[name] ++;
		else
			_statusEffectTypes[name] = 1;
	}
}

void TCompStatusEffects::parseStatusEffects(const json & j) {
	if (j.is_array()) {
		for (auto& jElem : j) {
			for (auto& jEffect : jElem.items()) {
				CStatusEffect* eff = CStatusEffect::getStatusEffectByName(jEffect.key());
				eff->load(jEffect.value());
				addStatusEffect(eff);
			}
		}
	}
}

void TCompStatusEffects::removeStatusEffect(const std::string& name) {
	if (hasStatusEffect(name)) {
		CStatusEffect* eff = _statusEffects[name];
		eff->remove();
		delete eff;
		_statusEffects.erase(name);
		// Remove status effect name from active types map
		int stCounter = --_statusEffectTypes[name];
		if (stCounter <= 0)
			_statusEffectTypes.erase(name);
	}
}

void TCompStatusEffects::removeStatusEffect(const CStatusEffect & eff) {
	removeStatusEffect(eff.getId());
}

void TCompStatusEffects::clearStatusEffects() {
	std::unordered_map<std::string, CStatusEffect*>::iterator it = _statusEffects.begin();
	while (it != _statusEffects.end()) {
		(*it->second).remove();
		delete it->second;
		it = _statusEffects.erase(it);
	}
}

void TCompStatusEffects::onResetComponent(const TMsgResetComponent& msg) {
	clearStatusEffects();
}

void TCompStatusEffects::onClearStatusEffects(const TMsgClearStatusEffects& msg)
{
	clearStatusEffects();
}
