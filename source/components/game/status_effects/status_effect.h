#pragma once
#include "mcv_platform.h"
#include "utils/time.h"

class CStatusEffect;

class CStatusEffectFactory {

public:

	template<typename EffectType>
	void registerStatusEffect(const std::string& name) {
		if (_factories.count(name) == 0) {
			_factories[name] = []() { return new EffectType(); };
		}
	}

	CStatusEffect* getStatusEffect(const std::string& name) {
		if (_factories.count(name) > 0)
			return _factories[name]();
	}

private:
	std::unordered_map<std::string, std::function<CStatusEffect* ()>> _factories;

};

// Pure virtual needs getIdSpecifier() implemented (as const function!!!)
class CStatusEffect {

public:

	CStatusEffect(const std::string& name) : _name(name) {}

	std::string customSpecifier;

	static CStatusEffect* getStatusEffectByName(const std::string& name);

	template<typename EffectType>
	static void registerStatusEffect(const std::string& name) {
		_factory.registerStatusEffect<EffectType>(name);
	}

	virtual void load(const json& j);

	void init(CHandle entity);
	void update(float dt);
	void remove();
	virtual bool exitCondition();

	std::string getName();
	std::string getId() const;

	bool isOfType(const std::string_view& type) const;

	void setTime(float time);
	void addTime(float time);
	bool isInfiniteTime() { return _isInfinite; }
	void setInfiniteTime(bool isInfinite);

	virtual void overrideEffect(const CStatusEffect& eff) {
		if (*this < eff)
			setTime(eff._timer.remaining());
	}

	friend bool operator<(const CStatusEffect& l, const CStatusEffect& r) {
		if (l._timer.isPaused()) return false;
		return (l._timer.remaining() < r._timer.remaining());
	}

	friend bool operator>(const CStatusEffect& l, const CStatusEffect& r) {
		if (l._timer.isPaused()) return true;
		return (l._timer.remaining() > r._timer.remaining());
	}

protected:
	CClock _timer = CClock(1.0f);
	CHandle _entity;
	const std::string _name;

	virtual std::string getIdSpecifier() const = 0;
	void loadTime(const json& j);
	void loadCustomSpecifier(const json& j);

	virtual void onEffectAcquired() {}
	virtual void onEffectUpdate(float dt) {}
	virtual void onEffectRemoved() {}

	bool timeUp() {
		return (_timer.isFinished());
	}

private:
	static CStatusEffectFactory _factory;
	bool _isInfinite = false;

	static void registerAllStatusEffects();

	friend class StaticConstructor;
	struct StaticConstructor {
		StaticConstructor() {
			CStatusEffect::registerAllStatusEffects();
		}
	};
	static StaticConstructor _staticConstr;

};
