#pragma once
#include "status_effect.h"
#include "entity/common_msgs.h"
#include "components/common/comp_health.h"

struct TDamageOverTimeStatusEffect : public CStatusEffect {

	TDamageOverTimeStatusEffect() : CStatusEffect("damage_over_time") {}

	float damagePerSecond = 1.0f;

	void load(const json& j) {
		CStatusEffect::load(j);
		damagePerSecond = j.value("damage_per_second", damagePerSecond);
	}

	std::string getIdSpecifier() const {
		return std::to_string(damagePerSecond);
	}

	void onEffectAcquired() {
		CEntity* entity = _entity;
		_healthComponent = entity->getComponent<TCompHealth>();
	}

	void onEffectUpdate(float dt) {
		if (_healthComponent.isValid()) {
			TCompHealth* pHealthComponent = _healthComponent;
			pHealthComponent->addDamage(damagePerSecond * dt);
		}
	}

protected:
	CHandle _healthComponent;

	TDamageOverTimeStatusEffect(const char* name) : CStatusEffect(name) {}

};

struct THealthOverTimeStatusEffect : public CStatusEffect {

	THealthOverTimeStatusEffect() : CStatusEffect("health_over_time") {}

	float healthPerSecond = 1.0f;

	void load(const json& j) {
		CStatusEffect::load(j);
		healthPerSecond = j.value("heal_per_second", healthPerSecond);
	}

	std::string getIdSpecifier() const {
		return std::to_string(healthPerSecond);
	}

	void onEffectAcquired() {
		CEntity* entity = _entity;
		_healthComponent = entity->getComponent<TCompHealth>();
	}

	void onEffectUpdate(float dt) {
		if (_healthComponent.isValid()) {
			TCompHealth* pHealthComponent = _healthComponent;
			pHealthComponent->setCurrentHealth(pHealthComponent->getCurrentHealth() + healthPerSecond * dt);
		}
	}

protected:
	CHandle _healthComponent;

	THealthOverTimeStatusEffect(const char* name) : CStatusEffect(name) {}

};

struct TMovementSpeedStatusEffect : public CStatusEffect {

	TMovementSpeedStatusEffect() : CStatusEffect("movement_speed") {}

	float speedMult = 1.2f;

	void load(const json& j) {
		CStatusEffect::load(j);
		speedMult = j.value("speed_multiplier", speedMult);
	}

	std::string getIdSpecifier() const {
		return std::to_string(speedMult);
	}

	void onEffectAcquired() {
		TMsgMovementSpeedMultiplier msg;
		msg.mult = speedMult;
		_entity.sendMsg(msg);
	}
	
	void onEffectRemoved() {
		TMsgMovementSpeedMultiplier msg;
		msg.mult = 1.0f / speedMult;
		_entity.sendMsg(msg);
	}

protected:
	TMovementSpeedStatusEffect(const char* name) : CStatusEffect(name) {}

};

struct TTotemBuffsEffect : public CStatusEffect {

	float armor;
	float regenerationPerSecond;
	float damageBuff;
	float bulletSpeed;
	float decisionMakingSpeed;

	//void onEffectAcquired() {
	//	setInfiniteTime(true);
	//	// Send msg to render to change materials.
	//	TMsgSetMeshState msgRender = { 4 }; // 4 is used for active totem buff state.
	//	_entity.sendMsg(msgRender);

	//	_damageBuff = msg.damage;

	//	TCompHealth * cHealth = getComponent<TCompHealth>();
	//	if (cHealth)
	//		cHealth->setArmour(msg.armour);
	//}

	//void onEffectUpdate(float dt) {
	//	if (_corruptionComponent.isValid()) {
	//		TCompCorruptionControllerPlayer* pCorruptionComponent = _corruptionComponent;
	//		pCorruptionComponent->addCorruption(corruptionPerSecond * dt);
	//	}
	//}

};