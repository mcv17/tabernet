#include "mcv_platform.h"
#include "common_status_effects.h"

void CStatusEffect::registerAllStatusEffects() {
	registerStatusEffect<TDamageOverTimeStatusEffect>("damage_over_time");
	registerStatusEffect<THealthOverTimeStatusEffect>("health_over_time");
	registerStatusEffect<TMovementSpeedStatusEffect>("movement_speed");
}