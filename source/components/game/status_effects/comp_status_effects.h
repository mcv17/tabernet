#pragma once
#include "components/common/comp_base.h"
#include "status_effect.h"
#include "entity/common_msgs.h"


class TCompStatusEffects : public TCompBase {

public:
	~TCompStatusEffects();

	DECL_SIBLING_ACCESS();

	void debugInMenu() {}
	void update(float dt);
	static void registerMsgs();

	static void declareInLua() {};

	bool hasStatusEffect(const std::string& name) const;
	bool hasStatusEffect(const CStatusEffect& eff) const;
	bool hasStatusEffectType(const std::string& name) const;

	template<typename EffectType>
	void addStatusEffect(EffectType& eff) {
		if (hasStatusEffect(eff)) {
			_statusEffects[eff.getId()]->overrideEffect(eff);
		}
		else {
			// Copy status effect
			EffectType* pEffect = new EffectType(std::move(eff));
			pEffect->init(getEntity());
			// Add status effect
			_statusEffects[pEffect->getId()] = pEffect;
			// Add status effect name to active types map
			const std::string& name = eff.getName();
			if (_statusEffectTypes.count(name) > 0)
				_statusEffectTypes[name] ++;
			else
				_statusEffectTypes[name] = 1;
		}
	}
	void addStatusEffect(CStatusEffect* eff);
	void parseStatusEffects(const json& j);

	void removeStatusEffect(const std::string& name);
	void removeStatusEffect(const CStatusEffect& eff);
	void clearStatusEffects();
	void onClearStatusEffects(const TMsgClearStatusEffects& msg);

private:
	std::unordered_map<std::string, CStatusEffect*> _statusEffects;
	std::unordered_map<std::string, int> _statusEffectTypes;

	void onResetComponent(const TMsgResetComponent& msg);
};