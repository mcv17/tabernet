#include "mcv_platform.h"
#include "comp_checkpoint.h"
#include "engine.h"
#include "components/controllers/comp_player_character_controller.h"
#include "components/common/comp_script.h"

DECL_OBJ_MANAGER("checkpoint", TCompCheckpoint);

CHandle TCompCheckpoint::_current;
CHandle TCompCheckpoint::_spawn;

bool TCompCheckpoint::isAfter(TCompCheckpoint * checkpoint) {
	if (!checkpoint) return true;

	return _order >= checkpoint->_order;
}

void TCompCheckpoint::setAsCurrent() {
	_current = CHandle(this);
}

void TCompCheckpoint::restorePlayer() {
	// If no checkpoint found go to what's supposed to be the spawn point
	if (_current.isValid()) {
		TCompCheckpoint* current = _current;
		current->restorePlayerToCheckpoint();
	}
	else {
		CEntity* player = getEntityByName("Player");
		TCompPlayerCharacterController* playerController = player->getComponent<TCompPlayerCharacterController>();
		if (playerController) {
			player->sendMsg(TMsgResetComponent());
			player->sendMsg(TMsgFullRestore());
			playerController->teleport(Vector3::Zero);
			EngineLogicManager.fillMagazineAmmo();
		}
	}
}

CHandle TCompCheckpoint::getSpawnCheckpoint() {
	return _spawn;
}

CHandle TCompCheckpoint::getCurrentCheckpoint() {
	return _current;
}

void TCompCheckpoint::reset() {
	_current = _spawn;
}

void TCompCheckpoint::onEntityCreated(const TMsgEntityCreated & msg) {
	_transform = getComponent<TCompTransform>();
	_script = getComponent<TCompScript>();
}

void TCompCheckpoint::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	CEntity* entity = msg.h_entity;
	std::string entityName = entity->getName();
	if (entityName == "Player") {
		if (isAfter(_current) && _current != CHandle(this)) {
			setAsCurrent();
			launchUIAnimation();
			TCompScript* script = _script;
			if (script)
				script->executeFunction("onCheckpointReached");
		}
	}
}

void TCompCheckpoint::restorePlayerToCheckpoint() {
	_currentCheckpointState = ERestorePlayerState::START;
}

int TCompCheckpoint::getOrder() {
	return _order;
}

void TCompCheckpoint::setOrder(int order) {
	_order = order;
}

void TCompCheckpoint::launchUIAnimation()
{
	if (UI::CImage* widget = EngineUI.getImageWidgetByName(_parentUIWidget, _iconUIWidget)) {
		widget->getParams()->visible = true;
		widget->playAnimation(_UIAnimationName, false);
	}
	else {
		Utils::dbg("[TCompCheckpoint::launchUIAnimation] widget not found\n");
	}
}

void TCompCheckpoint::registerMsgs() {
	DECL_MSG(TCompCheckpoint, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCheckpoint, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompCheckpoint::declareInLua() {
	EngineScripting.declareComponent<TCompCheckpoint>(
		"TCompCheckpoint",

		"restorePlayerToCheckpoint", &TCompCheckpoint::restorePlayerToCheckpoint,
		"setAsCurrent", &TCompCheckpoint::setAsCurrent,
		"isAfter", &TCompCheckpoint::isAfter,
		"getOrder", &TCompCheckpoint::getOrder,
		"setOrder", &TCompCheckpoint::setOrder
		);
}

void TCompCheckpoint::update(float dt) {
	switch (_currentCheckpointState) {
	case ERestorePlayerState::NOT_RESTORING:
		break;
	case ERestorePlayerState::START:
		EngineLogicManager.fadeInBlackScreen();
		_currentCheckpointState = ERestorePlayerState::FADING_IN;
		break;
	case ERestorePlayerState::FADING_IN:
		if (EngineLogicManager.isBlackScreenFadeOver())
			_currentCheckpointState = ERestorePlayerState::PLAYER_RESET;
		break;
	case ERestorePlayerState::PLAYER_RESET: 
		{
			//Set alpha of game over to 0, and disable the widget.
			EngineLogicManager.SetUIImageAlpha("game_over", "game_over_image", 0.0f);
			EngineUI.deactivateWidget("game_over");
			EngineUI.deactivateWidget("game_over_buttons");
			CHandle playerControllerHandle = getPlayerController();
			CEntity* player = playerControllerHandle.getOwner();
			TCompPlayerCharacterController* playerController = playerControllerHandle;
			TCompTransform* transform = _transform;
			if (player && transform && playerController) {
				player->sendMsg(TMsgResetComponent());
				player->sendMsg(TMsgFullRestore());
				playerController->teleport(transform->getPosition());
				EngineLogicManager.fillMagazineAmmo();
			}
			_currentCheckpointState = ERestorePlayerState::SCRIPT_CALL;
		}
		break;
	case ERestorePlayerState::SCRIPT_CALL:
		{
			TCompScript* script = _script;
			if (script)
				script->executeFunction("onCheckpointRestorePlayer");
			EngineLogicManager.fadeOutBlackScreen();
			_currentCheckpointState = ERestorePlayerState::FADING_OUT;
		}
		break;
	case ERestorePlayerState::FADING_OUT:
		if (EngineLogicManager.isBlackScreenFadeOver())
			_currentCheckpointState = ERestorePlayerState::NOT_RESTORING;
		break;
	}
}

void TCompCheckpoint::load(const json & j, TEntityParseContext & ctx) {
	assert(j.count("order") && "Checkpoint MUST have an order");

	_order = j["order"];
	if (j.value("is_spawn", false)) {
		setAsCurrent();
		_spawn = CHandle(this);
	}
}

void TCompCheckpoint::debugInMenu() {
	
}
