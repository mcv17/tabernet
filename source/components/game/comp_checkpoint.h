#pragma once
#include "components/common/comp_base.h"
#include "components/common/comp_collider.h"
#include "components/controllers/comp_player_character_controller.h"

class TCompCheckpoint : public TCompBase {
	
public:
	static void restorePlayer();
	static CHandle getSpawnCheckpoint();
	static CHandle getCurrentCheckpoint();
	static void reset();

	static void registerMsgs();
	static void declareInLua();

	DECL_SIBLING_ACCESS();
	bool isAfter(TCompCheckpoint* checkpoint);
	// Forces the checkpoint to be the current one regardless of order
	void setAsCurrent();
	// Forces restoration of player to this checkpoint without changing current
	void restorePlayerToCheckpoint();

	int getOrder();
	void setOrder(int order);

	void launchUIAnimation();

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();

	void onEntityCreated(const TMsgEntityCreated& msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter& msg);

private:
	static CHandle _current;
	static CHandle _spawn;

	std::string _parentUIWidget = "checkpoint";
	std::string _iconUIWidget = "checkpoint_widget";
	std::string _UIAnimationName = "Checkpoint";

	enum class ERestorePlayerState {
		NOT_RESTORING, START, FADING_IN, PLAYER_RESET, SCRIPT_CALL, FADING_OUT
	};

	DECL_TCOMP_ACCESS("Player", TCompPlayerCharacterController, PlayerController);

	int _order;
	CHandle _transform, _script;

	ERestorePlayerState _currentCheckpointState = ERestorePlayerState::NOT_RESTORING;

};