#include "mcv_platform.h"
#include "comp_mouse_manager_debug.h"
#include "engine.h"

DECL_OBJ_MANAGER("comp_mouse_manager_debug", TCompMouseManagerDebug);

void TCompMouseManagerDebug::load(const json & j, TEntityParseContext& ctx) {
	EngineInput.mouse().setCenterMouseMode(true);
}

void TCompMouseManagerDebug::update(float dt) {
	if (EngineInput[VK_F7].justPressed() || EngineInput["subtract"].justPressed()) {
		bool activate = !EngineInput.mouse()._isCenterMouseModeActive;
		EngineInput.mouse().setCenterMouseMode(activate);
	}
}