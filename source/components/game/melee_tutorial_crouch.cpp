#include "mcv_platform.h"
#include "melee_tutorial_crouch.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_render.h"
#include "components/common/comp_aabb.h"

DECL_OBJ_MANAGER("melee_tutorial_crouch", TCompMeleeTutorialCrouch);

void TCompMeleeTutorialCrouch::load(const json& j, TEntityParseContext& ctx) {
	_deathTime = j.value("death_time", _deathTime);
}

void TCompMeleeTutorialCrouch::registerMsgs() {
	DECL_MSG(TCompMeleeTutorialCrouch, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompMeleeTutorialCrouch, TMsgEntityDead, onEntityDead);
}

void TCompMeleeTutorialCrouch::onEntityCreated(const TMsgEntityCreated & msg) {
	_deathTimer = CClock(_deathTime);
}

void TCompMeleeTutorialCrouch::onEntityDead(const TMsgEntityDead& msg)
{
	_deathTimer.start();
	// Activate the vanish mesh state and the cte data
	TCompRender* render = getComponent<TCompRender>();
	if (render)
		EngineLogicManager.setStatusVanishEntity(CHandle(this).getOwner());

	// Attach AABB to ragdoll
	TCompRagdoll* c_ragdoll = getComponent<TCompRagdoll>();;
	if (c_ragdoll) {
		TMsgRagdollActivated msg;
		if (c_ragdoll->ragdoll.num_bones > 0)
			msg.link = c_ragdoll->ragdoll.bones[0].link;
		if (msg.link != nullptr)
			getEntity().sendMsgToComp<TCompLocalAABB>(msg);
	}

	// Delete collider
	if (CEntity* entity = getEntity()) {
		TMsgToogleComponent msg;
		msg.active = false;
		entity->sendMsgToComp<TCompCollider>(msg);
	}
}

void TCompMeleeTutorialCrouch::update(float dt) {

	if (_deathTimer.isFinished()) {
		CHandle eHandle = getEntity();
		eHandle.destroy();
	}
}

void TCompMeleeTutorialCrouch::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Text("Death time: ");
	ImGui::SameLine();
	ImGui::InputFloat("%%floatid", &_deathTime, 0.1f, 1.0f);
}
