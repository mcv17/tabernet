#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "geometry/curve.h"
#include "../module_scripting.h"

class TCompCurveController : public TCompBase
{
	DECL_SIBLING_ACCESS();

public:
	~TCompCurveController();

	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void renderDebug();
	
	void setRatio(float ratio);

private:
	void applyRatio();
	
	// Handle target.
	CHandle _target;
	std::string _targetName;

	// Variables.
	float _speed = 10.f;
	float _ratio = 0.f;
	bool  active = true;

	const CCurve * _curve = nullptr;

	// Handle.
	ComponentEventHandle OnCurveEnded;
	bool FinishCalled = false;
	void OnFinishTransition();
};

