#include "mcv_platform.h"

#include "comp_camera_manager.h"
#include "comp_camera.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_name.h"

#include "modules/module_camera_mixer.h"

#include "entity/common_msgs.h"
#include "engine.h"

DECL_OBJ_MANAGER("camera_manager", TCompCameraManager);

int TCompCameraManager::currentCamera = 0; // ID of the camera.
bool TCompCameraManager::firstFrame = true;
std::map< int, CHandle > TCompCameraManager::all_cameras; 	// Stores handles to all cameras, retrieval is done by their ID.
std::vector< int > TCompCameraManager::cameras_indexes; // Used for travel between cameras. Sorted.

TCompCameraManager::~TCompCameraManager() {
	currentCamera = 0;
	firstFrame = true;
}

void TCompCameraManager::onStart() {
	if (firstFrame) {
		firstFrame = false;
		setStatusOutputCamera(true);
	}
}

void TCompCameraManager::load(const json & j, TEntityParseContext & ctx) {
	currentCamera = j.value("starting-camera-index", 0);
}

void TCompCameraManager::debugInMenu() {
	if (ImGui::DragInt("Current Camera", &currentCamera, 1, 0, cameras_indexes.back()))
		setActiveCamera(currentCamera);

	CHandle camH = TCompCameraManager::all_cameras[currentCamera];
	CEntity * ent = camH.getOwner();
	TCompName * name = ent->getComponent<TCompName>();
	std::string cameraName = name->getName();
	std::string textCameraName = "The name of the current camera is: " + cameraName;
	ImGui::Text(textCameraName.c_str());
}

void TCompCameraManager::update(float dt) {
	onStart();

	if (EngineInput[VK_F1].justPressed())
		setActiveCamera(0);
	if (EngineInput[VK_F2].justPressed())
		setActiveCamera(1);
	if (EngineInput[VK_F3].justPressed())
		setActiveCamera((currentCamera+1) %((int)cameras_indexes.size()));
	if (EngineInput[VK_F4].justPressed()) {
		int nVIndx = currentCamera - 1;
		nVIndx = (nVIndx < 0) ? cameras_indexes.size() - 1 : nVIndx;
		setActiveCamera((nVIndx) % ((int)cameras_indexes.size()));
	}
}

void TCompCameraManager::registerCamera(int cameraIndex, TCompCamera * camera) {
	CHandle camHand = CHandle(camera);
	if(all_cameras.find(cameraIndex) != all_cameras.end())
		Utils::fatal("A valid camera was found in the requested index: %d\n", cameraIndex);

	all_cameras[cameraIndex] = camHand;
	cameras_indexes.push_back(cameraIndex);
	std::sort(cameras_indexes.begin(), cameras_indexes.end());

	setStatusNormalCamera(camHand, false);
}

int TCompCameraManager::registerCamera(TCompCamera * camera) {
	int cameraIndex = -1;
	for (int i = 0; i < cameras_indexes.size() - 1; i++) {
		int diff = cameras_indexes[i+1] - cameras_indexes[i];
		if (diff > 1)
			cameraIndex = cameras_indexes[i] + 1;
	}

	if (cameraIndex == -1)
		cameraIndex = cameras_indexes.size();

	CHandle camHand = CHandle(camera);
	if (all_cameras.find(cameraIndex) != all_cameras.end())
		Utils::fatal("A valid camera was found in the requested index: %d\n", cameraIndex);

	all_cameras[cameraIndex] = camHand;
	cameras_indexes.push_back(cameraIndex);
	std::sort(cameras_indexes.begin(), cameras_indexes.end());

	setStatusNormalCamera(camHand, false);
	return cameraIndex;
}

void TCompCameraManager::unRegisterCamera(int cameraIndex) {
	if (all_cameras.find(cameraIndex) == all_cameras.end()) return;
	
	//Delete it from map
	all_cameras.erase(cameraIndex);

	//Delete it from vector
	cameras_indexes.erase(std::remove(cameras_indexes.begin(), cameras_indexes.end(), cameraIndex), cameras_indexes.end());

	//Sort back the vector
	std::sort(cameras_indexes.begin(), cameras_indexes.end());
}

CHandle TCompCameraManager::getCamera(int cameraID) {
	if (all_cameras.find(cameraID) != all_cameras.end())
		return TCompCameraManager::all_cameras[cameraID];

#ifndef NDEBUG
	std::string text = "[WARNING]: A camera with index: " + std::to_string(cameraID) + " was not found.";
	Utils::fatal(text.c_str());
#endif

	return CHandle();
}

CHandle TCompCameraManager::getCurrentCamera() {
	if (all_cameras.find(currentCamera) != all_cameras.end())
		return TCompCameraManager::all_cameras[currentCamera];
	return CHandle();
}

Vector3 TCompCameraManager::getCurrentCameraPosition()
{
	CEntity* camera = getCurrentCamera().getOwner();
	if (camera) {
		TCompTransform* c_trans = camera->getComponent<TCompTransform>();
		if (c_trans)
			return c_trans->getPosition();
	}

	return Vector3();
}

void TCompCameraManager::setActiveCamera(int nVectorIndx){
	TMsgToogleEntity entityToggleMsg = { false };

	if (currentCamera == (int)Final_Camera)
		setStatusOutputCamera(false);
	else
		setStatusNormalCamera(all_cameras[currentCamera], false);

	currentCamera = cameras_indexes[nVectorIndx];

	if (currentCamera == (int)Final_Camera)
		setStatusOutputCamera(true);
	else
		setStatusNormalCamera(all_cameras[currentCamera], true);
}

void TCompCameraManager::setStatusOutputCamera(bool active) {
	TMsgToogleEntity entityToggleMsg = { active };

	// We activate all cameras in the mixer, in case we deactivated any of
	// them while changing cameras.
	VHandles entities = EngineCameraMixer.getCamerasEntitiesMixing();
	for (auto & entityH : entities) {
		CEntity * entity = entityH;
		entity->sendMsg(entityToggleMsg);
	}
}

void TCompCameraManager::setStatusNormalCamera(CHandle camera, bool active) {
	TMsgToogleEntity entityToggleMsg = { active };
	CEntity * entity = camera.getOwner();
	entity->sendMsg(entityToggleMsg);
}