#include "mcv_platform.h"
#include "comp_blender_camera.h"

#include "components/common/comp_transform.h"
#include "components/common/comp_collider.h"
#include "modules/module_physics.h"
using namespace physx;

DECL_OBJ_MANAGER("comp_camera_blender", TCompBlenderCamera);

void TCompBlenderCamera::load(const json& j, TEntityParseContext & ctx){}

void TCompBlenderCamera::registerMsgs(){
	DECL_MSG(TCompBlenderCamera, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompBlenderCamera, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompBlenderCamera, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompBlenderCamera, TMsgEntityTriggerExit, onTriggerExit);
	DECL_MSG(TCompBlenderCamera, TMsgBlenderStatus, onStatusChange);
}

void TCompBlenderCamera::renderDebug(){}

void TCompBlenderCamera::debugInMenu(){
	TCompBase::debugInMenu();
	ImGui::Checkbox("Blending", &activateBlending);
}

void TCompBlenderCamera::update(float dt) {
	TCompTransform * transform = transformH;
	TCompCollider * collider = colliderH;
	if (transform == nullptr || collider == nullptr) return;
	PxRigidDynamic * actor = static_cast<PxRigidDynamic *>(collider->actor);
	if (!actor) return;

	Vector3 newColPos = transform->getPosition();
	Quaternion newColRot = transform->getRotation();
	PxTransform pTransf;
	pTransf.p = VEC3_TO_PXVEC3(newColPos);
	pTransf.q = QUAT_TO_PXQUAT(newColRot);
	actor->setKinematicTarget(pTransf);
}

void TCompBlenderCamera::onEntityCreated(const TMsgEntityCreated & msg) {
	transformH = getComponent<TCompTransform>();
	colliderH = getComponent<TCompCollider>();
	assert(transformH.isValid() && colliderH.isValid());

	TCompCollider * collider = colliderH;
	PxRigidDynamic * actor = static_cast<PxRigidDynamic *>(collider->actor);
	assert(actor);

	// Get the trigger shape and get its offset.
	std::vector<PxShape*> shapes;
	shapes.resize(1);
	actor->getShapes(&shapes[0], 1);
	PxTransform transform = shapes[0]->getLocalPose();
	colliderOffset = PXVEC3_TO_VEC3(transform.p);
}

void TCompBlenderCamera::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	if (!activateBlending) return;
	
	CHandle handle = msg.h_entity;
	CEntity * entity = handle;

	// Blend entity.
	TMsgBlendEntity msgBlend = { true };
	entitiesBeingBlended[handle.getExternalIndex()] = handle;
	handle.sendMsg(msgBlend);
}

void TCompBlenderCamera::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	if (!activateBlending) return;
	
	CHandle handle = msg.h_entity;
	CEntity * entity = handle;

	// Unblend entity. Remove.
	TMsgBlendEntity msgBlend = { false };
	entitiesBeingBlended.erase(handle.getExternalIndex());
	handle.sendMsg(msgBlend);
}

void TCompBlenderCamera::onStatusChange(const TMsgBlenderStatus & msg) {
	activateBlending = msg.activate;

	// If we are not blending anymore, unblend entities.
	// Then remove all of them from the vector.
	if (!activateBlending) {
		TMsgBlendEntity msgBlend = { false };
		for (auto & handle : entitiesBeingBlended)
			handle.second.sendMsg(msgBlend);
		entitiesBeingBlended.clear();
	}
}