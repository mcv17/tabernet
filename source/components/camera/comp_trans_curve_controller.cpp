#include "mcv_platform.h"
#include "comp_trans_curve_controller.h"
#include "components/common/comp_transform.h"
#include "entity/common_msgs.h"

#include "engine.h"

DECL_OBJ_MANAGER("trans_curve_controller", TCompTransCurveController);

TCompTransCurveController::~TCompTransCurveController() {
	EngineScripting.UnregisterNewComponentEventForScripts(CHandle(this), "onKnotReached");
}

void TCompTransCurveController::load(const json& j, TEntityParseContext& ctx) {
	_curve = EngineResources.getResource(j.value("curve", ""))->as<CTransformCurve>();
	active = j.value("enabled", active);
	_loop = j.value("loop", _loop);
	_ratio = j.value("ratio", _ratio);
	_speed = j.value("speed", _speed);
	_timeForCompleteRatio = j.value("timeForCompleteRatio", _timeForCompleteRatio);

	// Create scripting events so scripts can bind to this component.
	OnKnotReached = EngineScripting.RegisterNewComponentEventForScripts(CHandle(this), "onKnotReached");
	OnEndReached = EngineScripting.RegisterNewComponentEventForScripts(CHandle(this), "onEndReached");
}

void TCompTransCurveController::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Checkbox("Loops", &_loop);
	if (ImGui::DragFloat("Ratio", &_ratio, 0.01f, 0.f, 1.0f))
		setRatio(_ratio);
	ImGui::DragFloat("Speed", &_speed, 0.01f, 0.f, 10.0f);
	ImGui::DragFloat("Time to complete ratio.", &_timeForCompleteRatio, 0.01f, 0.f, 10.0f);
	
	if (!_curve)
		ImGui::Text("Curve is not valid!");
}

void TCompTransCurveController::registerMsgs() {
	DECL_MSG(TCompTransCurveController, TMsgEntityCreated, onEntityCreated);
}

void TCompTransCurveController::declareInLua() {
	EngineScripting.declareComponent<TCompTransCurveController>
	(
		"TCompTransCurveController",
		"update", &TCompTransCurveController::update,
		"setRatio", &TCompTransCurveController::setRatio,
		"start", &TCompTransCurveController::start,
		"stop", &TCompTransCurveController::stop,
		"setLooping", &TCompTransCurveController::setLooping,
		"setTimeForCompleteRatio", &TCompTransCurveController::setTimeForCompleteRatio
	);
}

void TCompTransCurveController::onEntityCreated(const TMsgEntityCreated& msg) {
	transformH = getComponent<TCompTransform>();
}

void TCompTransCurveController::update(float delta) {
	float new_ratio = _ratio + delta * _speed/(_timeForCompleteRatio);
	setRatio(new_ratio);

	// If ended and not looping, send event.
	if (new_ratio >= 1.0f && !_loop) {
		OnEndReached.CallEventForAllScripts();
		active = false;
	}
}

void TCompTransCurveController::start() {
	active = true;
	_ratio = 0.0f;
}

void TCompTransCurveController::stop() {
	active = false;
}

void TCompTransCurveController::setRatio(float ratio)
{
	if (_loop) {
		// Wrap in the range 0..1
		ratio = fmodf(ratio, 1.0f);
		if (ratio < 0)
			ratio += 1;
	}
	else {
		// Keep ratio in valid range
		ratio = Maths::clamp(ratio, 0.f, 1.f);
	}
	
	_ratio = ratio;
	if (!_curve) return;
	
	// delta curve
	int previousSection = currentSectionInCurve;
	CTransform t = _curve->evaluate(_ratio, currentSectionInCurve);
	if (previousSection != currentSectionInCurve)
		OnKnotReached.CallEventForAllScripts();
	
	// Destination component
	TCompTransform* c_transform = transformH;
	if (!c_transform) return;
	
	// transform = base + delta
	c_transform->set(t);
}

void TCompTransCurveController::renderDebug() {
	if (!_curve) return;
	
	TCompTransform* c_transform = transformH;
	if (!c_transform) return;

	_curve->renderDebug();
}
