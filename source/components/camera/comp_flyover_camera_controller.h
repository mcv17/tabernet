#pragma once

#include "entity\entity.h"
#include "entity/common_msgs.h"

/* Cambiar por uno actualizado. Esto fue antes del input de albert. */

class TCompCameraFlyOverController : public TCompBase {
	// Always add entity.h and this macro for fast access to the
	// getHandle and getEntity methods.
	DECL_SIBLING_ACCESS();

	void onEntityCreated(const TMsgEntityCreated & msg);
	void OnInputStatus(const TMsgInputControllerStatus & msg);

	float camera_speed = 5.f;
	float camera_sprint_speed = 10.f;
	float horizontal_sensitivity = 60.f; // Max degrees if mouse goes from center to edge of the screen.
	float vertical_sensitivity = 60.f; // Max degrees if mouse goes from center to edge of the screen.
	const float maxPitch = (float)M_PI_2 - 1e-4f;
public:

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void update(float dt);
	static void registerMsgs();
};