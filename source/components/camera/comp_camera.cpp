#include "mcv_platform.h"
#include "comp_camera.h"
#include "components/common/comp_transform.h"
#include "entity/entity_parser.h"
#include "render/render.h"

#include "entity/common_msgs.h"

DECL_OBJ_MANAGER("camera", TCompCamera);

TCompCamera::~TCompCamera() {
	if (visible_in_debug)
		TCompCameraManager::unRegisterCamera(cameraIndex);
}

void TCompCamera::registerMsgs() {
	DECL_MSG(TCompCamera, TMsgEntitiesGroupCreated, onGroupCreated);
	DECL_MSG(TCompCamera, TMsgToogleComponent, onToggleComponent);
}

// We wait for group creation in case another entity could have changed some of our values.
// For example, lights that also use cameras.
void TCompCamera::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	if (visible_in_debug) {
		if(cameraIndex > -1)
			TCompCameraManager::registerCamera(cameraIndex, this);
		else
			TCompCameraManager::registerCamera(this);
	}
}

void TCompCamera::load(const json & j, TEntityParseContext& ctx) {
	float znear = j.value("near", getNear());
	float zfar = j.value("far", getFar());
	cameraIndex = j.value("camera_index", cameraIndex);
	visible_in_debug = j.value("visible_in_debug", visible_in_debug); // Set to false by default.
	// If we have a camera index set, we expect it to be shown in debug, so we set it to true.
	if (cameraIndex > -1)
		visible_in_debug = true;
	bool ortho = j.value("is_ortho", false);

	if (ortho) {
		float new_width = j.value("ortho_width", ortho_width);
		float new_height = j.value("ortho_height", ortho_height);
		float new_left = j.value("ortho_left", ortho_left);
		float new_top = j.value("ortho_top", ortho_top);
		bool centered = j.value("ortho_centered", ortho_centered);
		setOrthoParams(centered, new_left, new_width, new_top, new_height, znear, zfar);
	}
	else {
		float fov = DEG2RAD(j.value("fov", RAD2DEG(getFov())));;
		setProjectionParams(fov, znear, zfar);
	}
}

void TCompCamera::update(float dt) {
	TCompTransform * c_trans = getComponent<TCompTransform>();
	if (!c_trans) return;
	lookAt(c_trans->getPosition(), (c_trans->getPosition() + c_trans->getFront()), c_trans->getUp());
}

void TCompCamera::renderDebug() {
	MAT44 inv_view_proj = getViewProjection().Invert();
	// xy between -1..1 and z betwee 0 and 1
	auto mesh = EngineResources.getResource("unit_frustum.mesh")->as<CMesh>();

	// Sample several times to 'view' the z distribution along the 3d space
	const int nsamples = 10;
	for (int i = 1; i < nsamples; ++i) {
		float f = (float)i / (float)(nsamples - 1);
		MAT44 world = MAT44::CreateScale(1.f, 1.f, f) * inv_view_proj;
		drawMesh(mesh, world, VEC4(1, 1, 1, 1));
	}
	
	// Draw frustum points.
	for (int i = 0; i < NUM_FRUSTUM_CORNERS; i++)
		drawWiredSphere(Matrix::CreateTranslation(cascade_frustas_corners[i].x, cascade_frustas_corners[i].y, cascade_frustas_corners[i].z), 0.10f);
}

void TCompCamera::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::LabelText("Camera Index: ", "%d", cameraIndex);

	float new_znear = z_near;
	float new_zfar = z_far;
	float fovInDeg = RAD2DEG(fov_radians);
	float cfovInDeg = RAD2DEG(cascade_fov);

	ImGui::LabelText("ViewPort", "@(%d,%d) %dx%d", viewport.x0, viewport.y0, viewport.width, viewport.height);
	ImGui::LabelText("Aspect Ratio", "%f", aspect_ratio);

	bool changed = ImGui::Checkbox("Is Ortho", &is_ortho);
	if (is_ortho) {
		bool keep_aspect_ratio = true;
		ImGui::Checkbox("Keep Ratio", &keep_aspect_ratio);
		changed |= ImGui::DragFloat("Width", &ortho_width, 0.1f, 0.1f, 512.0f);
		if (keep_aspect_ratio && changed) {
			ortho_height = ortho_width / aspect_ratio;
			ImGui::Text("Height: %f", ortho_height);
		}
		else
			changed |= ImGui::DragFloat("Height", &ortho_height, 0.1f, 0.1f, 512.0f);
		changed |= ImGui::Checkbox("Centered", &ortho_centered);
		if (!ortho_centered) {
			changed |= ImGui::DragFloat("Left", &ortho_left, 0.1f, 0.1f, 512.0f);
			changed |= ImGui::DragFloat("Top", &ortho_top, 0.1f, 0.1f, 512.0f);
		}
	}
	else {
		changed |= ImGui::DragFloat("Fov", &fovInDeg, 0.1f, 30.f, 175.f);
		if (ImGui::DragFloat("Cascade Fov", &cfovInDeg, 0.1f, 30.f, 175.f)) {
			changed |= true;
			cascade_fov = DEG2RAD(cfovInDeg);
		}
	}
	changed |= ImGui::DragFloat("Z Near", &new_znear, 0.001f, 0.01f, 10000.f);
	changed |= ImGui::DragFloat("Z Far", &new_zfar, 1.0f, 2.0f, 100000.f);

	ImGui::LabelText("Eye", "%f %f %f", eye.x, eye.y, eye.z);
	ImGui::LabelText("Target", "%f %f %f", target.x, target.y, target.z);
	ImGui::LabelText("Front", "%f %f %f", front.x, front.y, front.z);
	ImGui::LabelText("Up", "%f %f %f", up.x, up.y, up.z);
	ImGui::LabelText("Left", "%f %f %f", left.x, left.y, left.z);

	if (changed && new_znear > 0.f && new_znear < new_zfar) {
		if (is_ortho)
			setOrthoParams(ortho_centered, ortho_left, ortho_width, ortho_top, ortho_height, new_znear, new_zfar);
		else
			setProjectionParams(DEG2RAD(fovInDeg), new_znear, new_zfar);
	}

	ImGui::Separator();
	ImGui::TextColored(ImVec4(0, 1, 0, 1), "Cascade shadow maps - General variables");
	bool csm_changed = false;
	csm_changed |= ImGui::DragFloat("Cascade start %: ", &CCamera::cascade_start_percentage, 0.0f, 0.001f, 1.f);
	csm_changed |= ImGui::DragFloat("Cascade end %: ", &CCamera::cascade_max_percentage, 0.0f, 0.001f, 1.f);
	csm_changed |= ImGui::DragFloat("Cascade 1 End %: ", &CCamera::perc_c1, 0.0f, 0.001f, 1.f);
	csm_changed |= ImGui::DragFloat("Cascade 2 End %: ", &CCamera::perc_c2, 0.0f, 0.001f, 1.f);
	csm_changed |= ImGui::DragFloat("Cascade 3 End %: ", &CCamera::perc_c3, 0.0f, 0.001f, 1.f);

	if (csm_changed)
		setCascadeFrustaZ();
}