#include "mcv_platform.h"
#include "comp_shaker.h"

#include "components/common/comp_transform.h"
#include "components/common/comp_tags.h"

DECL_OBJ_MANAGER("comp_shaker", TCompShaker);

void TCompShaker::load(const json& j, TEntityParseContext & ctx){
	traumaDecrease = j.value("trauma_decrease_per_second", traumaDecrease);

	translationShake = j.value("enable_translational_shake", translationShake);
	maxOffsetTranslation = loadVector3(j, "max_offset_translation");

	rotationShake = j.value("enable_rotational_shake", rotationShake);
	maxOffsetRotation = loadVector3(j, "max_offset_rotation");
	maxOffsetRotation.x = DEG2RAD(maxOffsetRotation.x);
	maxOffsetRotation.y = DEG2RAD(maxOffsetRotation.y);
	maxOffsetRotation.z = DEG2RAD(maxOffsetRotation.z);
}

void TCompShaker::registerMsgs(){
	DECL_MSG(TCompShaker, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompShaker, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompShaker, TMsgTraumaDelta, onTraumaDelta);
}

void TCompShaker::renderDebug(){}

void TCompShaker::debugInMenu(){
	TCompBase::debugInMenu();

	// Trauma variables.
	ImGui::Text("Trauma");
	ImGui::DragFloat("Trauma: ", &trauma, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("Trauma decrease per second: ", &traumaDecrease, 0.01f, 0.0f, 20.0f);

	ImGui::Spacing();
	ImGui::Separator();
	ImGui::Spacing();

	// Translational shake.
	ImGui::Text("Translational shake");
	ImGui::Checkbox("Activate translation: ", &translationShake);
	ImGui::DragFloat3("Translational offset: ", &maxOffsetTranslation.x, 0.01f, 0.0f, 40.0f);

	ImGui::Spacing();
	ImGui::Separator();
	ImGui::Spacing();

	// Rotational shake.
	ImGui::Text("Rotational shake (Pitch, Yaw, Roll)");
	ImGui::Checkbox("Activate rotation: ", &rotationShake);
	Vector3 offsetInDeg = Vector3(RAD2DEG(maxOffsetRotation.x), RAD2DEG(maxOffsetRotation.y), RAD2DEG(maxOffsetRotation.z));
	if (ImGui::DragFloat3("Rotational offset: ", &offsetInDeg.x, 0.01f, 0.0f, 10000.0f)) {
		maxOffsetRotation.x = DEG2RAD(offsetInDeg.x);
		maxOffsetRotation.y = DEG2RAD(offsetInDeg.y);
		maxOffsetRotation.z = DEG2RAD(offsetInDeg.z);
	}
}

void TCompShaker::update(float dt) {
	TCompTransform * transform = transformH;
	if (!transform) return;

	updateTrauma(dt);
	if(translationShake) translationalShake(transform);
	if(rotationShake) rotationalShake(transform);
}

void TCompShaker::updateTrauma(float dt) {
	trauma = std::max(trauma - traumaDecrease * dt, 0.0f);
	shake = std::pow(trauma, 2);
}

void TCompShaker::translationalShake(TCompTransform * transform) {
	float offsetX = maxOffsetTranslation.y * shake * getRandomFloatNegOneToOne();
	float offsetY = maxOffsetTranslation.x * shake * getRandomFloatNegOneToOne();
	float offsetZ = maxOffsetTranslation.z * shake * getRandomFloatNegOneToOne();
	Vector3 newPosition = transform->getPosition();
	newPosition.x += offsetX;
	newPosition.y += offsetY;
	newPosition.z += offsetZ;
	transform->setPosition(newPosition);
}

void TCompShaker::rotationalShake(TCompTransform * transform) {
	float yaw = maxOffsetRotation.y * shake * getRandomFloatNegOneToOne();
	float pitch = maxOffsetRotation.x * shake * getRandomFloatNegOneToOne();
	float roll = maxOffsetRotation.z * shake * getRandomFloatNegOneToOne();
	float currentYaw, currentPitch, currentRoll;
	transform->getAngles(&currentYaw, &currentPitch, &currentRoll);
	transform->setAngles(currentYaw + yaw, currentPitch + pitch, currentRoll + roll);
}

float TCompShaker::getRandomFloatNegOneToOne() {
	return static_cast <float>(rand())/static_cast <float>(RAND_MAX) * 2.0f - 1.0f;
}

void TCompShaker::onEntityCreated(const TMsgEntityCreated & msg) {
	transformH = getComponent<TCompTransform>();
}

void TCompShaker::onTraumaDelta(const TMsgTraumaDelta & msg) {
	trauma = std::max(std::min(trauma + msg.traumaDelta, msg.maxTraumaReachable), 0.0f);
}