#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"

/* This component tracks a camera.
Similar to the hierarchy component
but copies the values from a camera
(including FOV and Zs). */

struct TMsgEntitiesGroupCreated;

class TCompCameraTracker : public TCompBase {
	DECL_SIBLING_ACCESS();

	std::string cameraToTrack;
	CHandle cameraToTrackH;
	CHandle cameraH;


	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);
	bool fetchCameraToTrack();
public:
	void load(const json & j, TEntityParseContext & ctx);
	static void registerMsgs();
	void renderDebug();
	void debugInMenu();
	void update(float dt);
};