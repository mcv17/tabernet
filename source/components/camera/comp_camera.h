#pragma once

#include "components\common\comp_base.h"
#include "comp_camera_manager.h"
#include "entity/entity.h"
#include "geometry/camera.h"
#include "engine.h"

struct TMsgEntitiesGroupCreated;

/* This component represents a camera used for rendering. */

class TCompCamera : public CCamera, public TCompBase {
	// Always add entity.h and this macro for fast access to the
	// getHandle and getEntity methods.
	DECL_SIBLING_ACCESS();

	int cameraIndex = -1; // Used by the CameraManager for moving between cameras.
	bool visible_in_debug = false; // Whether the camera can be viewed from the CameraManager.

	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);
public:
	~TCompCamera();

	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void renderDebug();
	void debugInMenu();

	bool isLocked() { return EngineInput.mouse()._isCenterMouseModeActive; }
	void setFov(float nFov) { fov_radians = nFov; updateProjection(); }
};