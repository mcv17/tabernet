#include "mcv_platform.h"
#include "comp_camera_player_postprocess_effects.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "render/module_render.h"

DECL_OBJ_MANAGER("player_camera_post_process_effects", TCompPlayerCameraPostProcessEffects);

void TCompPlayerCameraPostProcessEffects::load(const json & j, TEntityParseContext& ctx) {
	// Corruption technique.
	std::string tech_filename = j.value("vision_penalty_techn", "vision_penalty.tech");
	vision_penalty_techn = EngineResources.getResource(tech_filename)->as<CTechnique>();
	
	maxVisionPenaltyShaderEffect = j.value("max_vision_penalty_shader_value", maxVisionPenaltyShaderEffect);
	timeForCompleteEffect = j.value("time_for_complete_fall_off", timeForCompleteEffect);
	deltaEffectPerSecond = maxVisionPenaltyShaderEffect / timeForCompleteEffect;
	
	if (!cts_vision_shader.create("Corruption")) Utils::fatal("Failed while creating the cts_vision_shader.");
	cts_vision_shader.CorruptionColor = Vector3(.2, 0.1, 0.4);
	cts_vision_shader.CorruptionFallOff = 0.0;

	// Insanity technique.
	tech_filename = j.value("insanity_penalty_techn", "texture_to_screen.tech");
	insanity_penalty_techn = EngineResources.getResource(tech_filename)->as<CTechnique>();

	// Perception technique.
	tech_filename = j.value("perception_penalty_techn", "perception_screen.tech");
	perception_penalty_techn = EngineResources.getResource(tech_filename)->as<CTechnique>();

	createOrResizeRT(Render.getWidth(), Render.getHeight());
}


void TCompPlayerCameraPostProcessEffects::debugInMenu() {
	ImGui::Checkbox("Vision penalty activated:", &vision_penalty_active);
	
	bool changed = ImGui::DragFloat("Vision penalty max effect: ", &maxVisionPenaltyShaderEffect, 0.01f, 0.0f, 10.f);
	changed |= ImGui::DragFloat("Corruption time for complete effect: ", &timeForCompleteEffect, 0.01f, 0.0f, 10.f);
	if(changed)
		deltaEffectPerSecond = maxVisionPenaltyShaderEffect / timeForCompleteEffect;


	ImGui::Checkbox("Insanity penalty activated:", &insanity_penalty_active);
	ImGui::Checkbox("Perception activated:", &perception_active);
}

bool TCompPlayerCameraPostProcessEffects::createOrResizeRT(int width, int height) {
	if (!rt)
		rt = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {
		char rt_fog[64];
		sprintf(rt_fog, "PostProcess-Texture", CHandle(this).asUnsigned());
		return rt->create(rt_fog, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	}
	return true;
}

CRenderToTexture * TCompPlayerCameraPostProcessEffects::apply(CRenderToTexture * textureToApplyPPE) {
	if (!active) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	CRenderToTexture * current_rt = rt;
	CRenderToTexture * current_use_as_texture = textureToApplyPPE;

	{
		CGpuScope gpu_scope("Corruption Penalty");
		
		// Increase as time goes by.
		if (currentFallOffValue <= goalFallOffValue)
			currentFallOffValue = Maths::clamp(currentFallOffValue + deltaEffectPerSecond * Time.delta, 0.0f, maxVisionPenaltyShaderEffect);
		else
			currentFallOffValue = Maths::clamp(currentFallOffValue - deltaEffectPerSecond * Time.delta, 0.0f, maxVisionPenaltyShaderEffect);
		cts_vision_shader.CorruptionFallOff = currentFallOffValue;

		// Apply to screen texture effect in the postprocess texture.
		current_rt->activateRTAndClear();
		cts_vision_shader.activate();
		cts_vision_shader.updateGPU();
		drawFullScreenQuad(vision_penalty_techn, current_use_as_texture);
			
		current_use_as_texture = current_rt;
		current_rt = textureToApplyPPE;
	}

	{
		CGpuScope gpu_scope("insanity-penalty-fx");
		if (insanity_penalty_active) {
			// Apply effect to screen texture in the postprocess texture.
			current_rt->activateRTAndClear();
			drawFullScreenQuad(insanity_penalty_techn, current_use_as_texture);

			current_use_as_texture = current_rt;
			current_rt = textureToApplyPPE;
		}
	}

	{
		CGpuScope gpu_scope("perception-fx");
		if (perception_active) {
			// Apply effect to screen texture in the postprocess texture.
			current_rt->activateRTAndClear();
			drawFullScreenQuad(perception_penalty_techn, current_use_as_texture);

			current_use_as_texture = current_rt;
			current_rt = textureToApplyPPE;
		}
	}

	return current_use_as_texture;
}

void TCompPlayerCameraPostProcessEffects::registerMsgs() {
	DECL_MSG(TCompPlayerCameraPostProcessEffects, TMsgPerceptionStatusChanged, OnPerceptionChanged);
	DECL_MSG(TCompPlayerCameraPostProcessEffects, TMsgVisionPenaltyStatusChanged, OnVisionPenaltyChanged);
	DECL_MSG(TCompPlayerCameraPostProcessEffects, TMsgInsanityPenaltyStatusChanged, OnInsanityPenaltyChanged);
}

void TCompPlayerCameraPostProcessEffects::OnPerceptionChanged(const TMsgPerceptionStatusChanged & msg) {
	perception_active = msg.active;
}

void TCompPlayerCameraPostProcessEffects::OnVisionPenaltyChanged(const TMsgVisionPenaltyStatusChanged & msg) {
	goalFallOffValue = msg.percentage * maxVisionPenaltyShaderEffect;
}

void TCompPlayerCameraPostProcessEffects::OnInsanityPenaltyChanged(const TMsgInsanityPenaltyStatusChanged & msg) {
	insanity_penalty_active = msg.active;
}