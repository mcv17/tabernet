#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "render/shaders/cte_buffer.h"
#include "render/textures/render_to_texture.h"

// DISCONTINUED.

/* This component applies post processing effects */
class TCompPlayerCameraPostProcessEffects : public TCompBase {
	DECL_SIBLING_ACCESS();

	CRenderToTexture * rt;

	// Corruption penalty technique.
	const CTechnique * vision_penalty_techn;
	bool vision_penalty_active = false;
	
	float currentFallOffValue = 0.0f;
	float goalFallOffValue = 0.0f;
	float maxVisionPenaltyShaderEffect = 1.0f;
	float timeForCompleteEffect = 2.0f;
	float deltaEffectPerSecond = maxVisionPenaltyShaderEffect / timeForCompleteEffect;

	CCteBuffer<TCtesCorruption> cts_vision_shader = CCteBuffer<TCtesCorruption>(CTE_BUFFER_SLOT_CORRUPTION);
	
	// Insanity penalty technique.
	const CTechnique * insanity_penalty_techn;
	bool insanity_penalty_active = false;

	// Perception penalty technique.
	const CTechnique * perception_penalty_techn;
	bool perception_active = false; // Used by the module render to choose rendering path.

	// Messages.
	void OnPerceptionChanged(const TMsgPerceptionStatusChanged & msg);
	void OnVisionPenaltyChanged(const TMsgVisionPenaltyStatusChanged & msg);
	void OnInsanityPenaltyChanged(const TMsgInsanityPenaltyStatusChanged & msg);

	bool createOrResizeRT(int width, int height);
public:
	~TCompPlayerCameraPostProcessEffects() { 
		if (rt) rt->destroy();		
		cts_vision_shader.destroy();
	}

	CRenderToTexture * apply(CRenderToTexture * textureToApplyPPE);

	void load(const json& j, TEntityParseContext & ctx);
	void debugInMenu();

	static void registerMsgs();

	bool isVisionPenaltyActive() { return vision_penalty_active; }
	bool isInsanityActive() { return insanity_penalty_active; }
	bool isPerceptionActive() { return perception_active; }
};