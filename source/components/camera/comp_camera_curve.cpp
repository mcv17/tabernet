#include "mcv_platform.h"
#include "comp_camera_curve.h"
#include "entity/common_msgs.h"

#include "engine.h"

#include "components/camera/comp_camera.h"

DECL_OBJ_MANAGER("camera_curve_controller", TCompCameraCurveController);

TCompCameraCurveController::~TCompCameraCurveController() {
	EngineScripting.UnregisterNewComponentEventForScripts(CHandle(this), "onKnotReached");
}

void TCompCameraCurveController::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", active);
	_curve = EngineResources.getResource(j.value("curve", ""))->as<CCurve>();
	_loop = j.value("loop", _loop);
	_ratio = j.value("ratio", _ratio);
	_currentSpeedMod = j.value("speed", _currentSpeedMod);
	_timeForCompleteRatio = j.value("timeForCompleteRatio", _timeForCompleteRatio);

	// Create scripting events so scripts can bind to this component.
	OnKnotReached = EngineScripting.RegisterNewComponentEventForScripts(CHandle(this), "onKnotReached");
	OnEndReached = EngineScripting.RegisterNewComponentEventForScripts(CHandle(this), "onEndReached");
}

void TCompCameraCurveController::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Checkbox("Loops", &_loop);

	if (ImGui::DragFloat("Ratio", &_ratio, 0.01f, 0.f, 1.0f))
		setRatio(_ratio);

	ImGui::DragFloat("Time to complete ratio.", &_timeForCompleteRatio, 0.01f, 0.f, 10.0f);

	if (!_curve)
		ImGui::Text("Curve is not valid!");
}

void TCompCameraCurveController::renderDebug() {
	if (!_curve) return;
	_curve->renderDebug();
}

void TCompCameraCurveController::registerMsgs() {
	DECL_MSG(TCompCameraCurveController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCameraCurveController, TMsgToogleComponent, onToggleComponent);
}

void TCompCameraCurveController::onEntityCreated(const TMsgEntityCreated& msg) {
	transformH = getComponent<TCompTransform>();
}

void TCompCameraCurveController::declareInLua() {
	EngineScripting.declareComponent<TCompCameraCurveController>
		(
			"TCompCameraCurveController",
			"setActive", &TCompCameraCurveController::start,
			"setRatio", &TCompCameraCurveController::setRatio,
			"setTimeForCompleteRatio", &TCompCameraCurveController::setTimeForCompleteRatio,
			"setLooping", &TCompCameraCurveController::setLooping
			);
}

void TCompCameraCurveController::update(float delta) {
	float new_ratio = _ratio + delta * _currentSpeedMod /(_timeForCompleteRatio);
	setRatio(new_ratio);

	// If ended and not looping, send event.
	if (new_ratio >= 1.0f && !_loop) {
		OnEndReached.CallEventForAllScripts();
		active = false;
	}
}

void TCompCameraCurveController::start() {
	active = true;
	_ratio = 0.0f;
}

void TCompCameraCurveController::setRatio(float ratio)
{
	if (_loop) {
		ratio = fmodf(ratio, 1.0f); // Wrap in the range 0..1
		if (ratio < 0)
			ratio += 1;
	}
	else
		ratio = Maths::clamp(ratio, 0.f, 1.f); // Keep ratio in valid range.

	_ratio = ratio;
	if (!_curve) return;

	// delta curve
	int previousSection = currentSectionInCurve;
	Vector3 pos = _curve->evaluate(_ratio, currentSectionInCurve);
	if (previousSection != currentSectionInCurve)
		OnKnotReached.CallEventForAllScripts();

	// Get camera.
	TCompTransform * transform = transformH;
	if (transform)
		transform->setPosition(pos);
}