#pragma once

#include "entity\entity.h"
#include "components\states\state_machine_controller.h"
#include "components/common/comp_transform.h"

class TCompCamera;

class TCompPlayerCameraController : public StateMachineController {
	//Fast access to the getHandle and getEntity methods.
	DECL_SIBLING_ACCESS();
	
	/* Handles. */
	CHandle targetTransformHandle;
	CHandle camaraTransformHandle;
	CHandle cameraHandle;

	/* Target variables. */
	std::string targetName; // Name to search the player entity, remove once we have second pass load.
	VEC3 targetOffset; // Offset from the entity transform of the target.

	/* General variables. */
	const float pitchReference = (float)M_PI_2 - 1e-4f;
	float maxPitch = pitchReference;
	float minPitch = -pitchReference;
	
	/* Camera controller states. */

	/* Transition variables. */
	std::unordered_map<std::string, float> transitionTimes;
	bool calledTransition = false; // Indicates if we must change state and start transition.
	std::string stateToTransitionTo;
	bool transition = false; // Indicates if we are transitioning from state to state.
	float currentTimeTransition = 0.0f;
	// This is the starting position and fov. We could have implemented the controller with multiple
	// entities always updating but decided to do it this way.
	VEC3 startingTransitionPos;
	float startingTransitionFov;
	float timeForTransition;

	/* Collision variables. */
	bool transitionFromCollision = false;
	float timeToGetToCollisionToRealPos = 0.15f;
	float distanceToKeepFromCollision = 0.25f; // How far away from a collision we will stay.

	/* Clipping variables. */
	bool isClipping = false;

	/* Base state. */
	// Variables.
	float horizontal_sensitivity_base = 200.f;
	float vertical_sensitivity_base = 200.f;
	VEC3 offsetFromOrbitCenter;
	float baseFOV = 60.00f;
	float timeToGetToBasePos = 0.5f; // In seconds.

	// Methods.
	void onBase(float dt);
	Vector3 getCameraPositionOnBase(const Vector3 & targetPosition, TCompTransform * camaraTransform);

	/* Aim state. */
	// Variables.
	float horizontal_sensitivity_aiming = 30.f;
	float vertical_sensitivity_aiming = 30.f;
	VEC3 offsetAiming; // Distance the camera will keep from the target.
	float aimingFOV = 50.00f;
	float timeToGetToAimingPos = 0.5f; // In seconds

	// Methods.
	void onAim(float dt);
	Vector3 getCameraPositionOnAim(const Vector3 & targetPosition, TCompTransform * camaraTransform);

	/* Sprint state. */
	// Variables.
	float horizontal_sensitivity_sprint = 60.f;
	float vertical_sensitivity_sprint = 60.f;
	VEC3 offsetSprinting; // Distance the camera will keep from the target.
	float sprintingFOV = 65.f;
	float timeToGetToSprintPos = 0.5f; // In seconds

	// Methods.
	void onSprint(float dt);
	Vector3 getCameraPositionOnSprint(const Vector3 & targetPosition, TCompTransform * camaraTransform);

	/* Dead state. */
	// Variables.
	float horizontal_sensitivity_dead = 60.f;
	float vertical_sensitivity_dead = 60.f;
	VEC3 offsetDead; // Distance the camera will keep from the target.
	float deadFOV = 60.f;
	float timeToGetToDeadPos = 1.f; // In seconds
																		 
	// Methods.
	void onDead(float dt);
	Vector3 getCameraPositionOnDead(const Vector3 & targetPosition, TCompTransform * camaraTransform);

	void onEntityCreated(const TMsgEntityCreated & msg);
	// Activates or deactivates this component.
	void OnInputStatus(const TMsgInputControllerStatus & msg);

public:
	void Init() override; // Sets the initial camera data.
	void load(const json & j, TEntityParseContext& ctx);
	void renderDebug();
	void debugInMenu();
	void update(float dt);
	void ChangeState(const std::string & stateToTransitionTo);
	//Checks if the new state is not the current one, if it isn't it transitions into
	void checkAndChangeState(const std::string& stateToTransitionTo);
	static void registerMsgs();
	void setTarget(const std::string & nTargetName); // Sets a new target.

	// Returns the degrees the player is aiming at. 0.0 being straight. Used for
	// setting the animator.
	float getAimingDegreesPlayer();

	void setMaxUpperPitch(float pitch);
	void setMinLowerPitch(float pitch);

private:
	/* General functions. */
	
	// Check all the handles used by the camera.
	bool checkHandles();
	// Sets the base starting pos of the camera.
	void setBaseStartingPos();
	// Returns the target position to point to. Applies an offset to the
	// transform of the entity which normally is at ground level.
	VEC3 getTargetPosition(TCompTransform * targetTransform);
	
	/* Transition between camera states methods. */
	
	// Lerps the camara to move to the next transition.
	void lerpIfTransitionOrCollision(TCompTransform * camaraTransform, TCompCamera * camera, Vector3 & finalPosition,
		float currentYaw, float currentPitch, float finalYaw, float finalPitch, float finalFov, float dt);
	// Checks if a change state transition was called, sets the starting values
	//of the transition (where the camera is right now).
	void checkIfTransitionToAnotherState(TCompTransform * camaraTransform, TCompCamera * camera);
	// Sets the starting transition values.
	void setTransitionValues(const Vector3 & startingPosition, float startingFov, float nTimeTransition);
	// Returns the transition time for the next state.
	float getTransitionTimeForNextState();
	// Computes the pitch from mouse and gamepad
	float handleCameraVerticalMovement(float sensitivity);
	// Computes the roll from mouse and gamepad
	float handleCameraHorizontalMovement(float sensitivity);

	/* Checks for collisions. */
	bool checkForCollision(Vector3 & finalPosition, const Vector3 & targetPosition);

	/* Set the cascade fov of the camera. */
	void setCameraCascadeFOV();
};