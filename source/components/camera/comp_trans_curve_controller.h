#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "geometry/curve.h"
#include "../module_scripting.h"

struct TMsgEntityCreated;

class TCompTransCurveController : public TCompBase
{
	DECL_SIBLING_ACCESS();

	void onEntityCreated(const TMsgEntityCreated& msg);

public:
	~TCompTransCurveController();

	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void renderDebug();
	static void registerMsgs();
	static void declareInLua();

	void update(float delta);
	void start();
	void stop();
	void setRatio(float ratio);
	void setTimeForCompleteRatio(float nTime) { _timeForCompleteRatio = nTime; }
	void setLooping(bool looping) { _loop = looping; }
	
private:
	ComponentEventHandle OnKnotReached;
	ComponentEventHandle OnEndReached;
	CHandle        transformH;

	const CTransformCurve * _curve = nullptr;
	int   currentSectionInCurve = -1;
	float _timeForCompleteRatio = 1.f; // How long it takes to get to the end of the curve if the curve has default speed modifiers.
	float _speed = 1.f;
	float _ratio = 0.f;
	bool  _loop = true;
	bool  _base_defined = false;
};