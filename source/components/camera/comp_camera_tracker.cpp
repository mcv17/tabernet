#include "mcv_platform.h"
#include "comp_camera_tracker.h"
#include "comp_camera.h"
#include "entity/common_msgs.h"

DECL_OBJ_MANAGER("camera_tracker", TCompCameraTracker);

void TCompCameraTracker::load(const json& j, TEntityParseContext & ctx){
	cameraToTrack = j.value("cam_to_track", cameraToTrack);
}

void TCompCameraTracker::registerMsgs(){
	DECL_MSG(TCompCameraTracker, TMsgEntitiesGroupCreated, onGroupCreated);
	DECL_MSG(TCompCameraTracker, TMsgToogleComponent, onToggleComponent);
}

void TCompCameraTracker::renderDebug(){}

void TCompCameraTracker::debugInMenu(){
	TCompBase::debugInMenu();

	// Trauma variables.
	ImGui::LabelText("Camera tracking", "%s", cameraToTrack.c_str());
}

void TCompCameraTracker::update(float dt) {
	if (!cameraH.isValid() || !cameraToTrackH.isValid()) {
		if (!fetchCameraToTrack()) return;
	}

	TCompCamera * cameraToTrack = cameraToTrackH;
	TCompCamera * camera = cameraH;
	CCamera & cameraData = *camera;
	CCamera & cameraToTrackData = *cameraToTrack;
	cameraData = cameraToTrackData;
}

void TCompCameraTracker::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	fetchCameraToTrack();
}

bool TCompCameraTracker::fetchCameraToTrack(){
	CEntity * ent = getEntityByName(cameraToTrack);
	cameraToTrackH = ent->getComponent<TCompCamera>();
	cameraH = getComponent<TCompCamera>();

	return cameraH.isValid() && cameraToTrackH.isValid();
}