#include "mcv_platform.h"
#include "components\common\comp_transform.h"
#include"comp_camera.h"
#include "comp_player_camera_controller.h"
#include "engine.h"

#include "geometry\geometry.h"
#include "utils/phys_utils.h"

DECL_OBJ_MANAGER("comp_player_camera_controller", TCompPlayerCameraController);

void TCompPlayerCameraController::load(const json& j, TEntityParseContext& ctx) {
	targetOffset = loadVector3(j, "targetOffset");

	// Base variables.
	vertical_sensitivity_base = DEG2RAD(j.value("v_sensitivity", vertical_sensitivity_base));
	horizontal_sensitivity_base = DEG2RAD(j.value("h_sensitivity", horizontal_sensitivity_base));
	offsetFromOrbitCenter = loadVector3(j, "offsetFromOrbitCenter");
	baseFOV = DEG2RAD((float)j.value("baseFOV", baseFOV));
	timeToGetToBasePos = (float)j.value("timeToGetToBasePos", timeToGetToBasePos);

	// Sprint variables.
	horizontal_sensitivity_sprint = DEG2RAD(j.value("h_sensitivity_sprint", horizontal_sensitivity_sprint));
	vertical_sensitivity_sprint = DEG2RAD(j.value("v_sensitivity_sprint", vertical_sensitivity_sprint));
	offsetSprinting = loadVector3(j, "offsetSprinting");
	sprintingFOV = DEG2RAD((float)j.value("sprintingFOV", sprintingFOV));
	timeToGetToSprintPos = (float)j.value("timeToGetToSprintPos", timeToGetToSprintPos);

	// Aim variables.
	horizontal_sensitivity_aiming = DEG2RAD(j.value("h_sensitivity_aiming", horizontal_sensitivity_aiming));
	vertical_sensitivity_aiming = DEG2RAD(j.value("v_sensitivity_aiming", vertical_sensitivity_aiming));
	offsetAiming = loadVector3(j, "offsetAiming");
	aimingFOV = DEG2RAD((float)j.value("aimingFOV", aimingFOV));
	timeToGetToAimingPos = (float)j.value("timeToGetToAimingPos", timeToGetToAimingPos);

	// Dead variables.
	horizontal_sensitivity_dead = DEG2RAD(j.value("h_sensitivity_dead", horizontal_sensitivity_dead));
	vertical_sensitivity_dead = DEG2RAD(j.value("v_sensitivity_dead", vertical_sensitivity_dead));
	offsetDead = loadVector3(j, "offsetDead");
	deadFOV = DEG2RAD((float)j.value("deadFOV", deadFOV));
	timeToGetToDeadPos = (float)j.value("timeToGetToDeadPos", timeToGetToDeadPos);

	// Collision variables.
	timeToGetToCollisionToRealPos = j.value("time_to_get_to_real_pos_on_collision", timeToGetToCollisionToRealPos);
	distanceToKeepFromCollision = j.value("distance_to_keep_from_collision", distanceToKeepFromCollision);

	// Target.
	targetName = j.value("target_name", targetName);
	Init();
}

void TCompPlayerCameraController::Init() {
	AddState("Normal", (statehandler)&TCompPlayerCameraController::onBase);
	transitionTimes["Normal"] = timeToGetToBasePos;
	AddState("Aim", (statehandler)&TCompPlayerCameraController::onAim);
	transitionTimes["Aim"] = timeToGetToAimingPos;
	AddState("Sprint", (statehandler)&TCompPlayerCameraController::onSprint);
	transitionTimes["Sprint"] = timeToGetToSprintPos;
	AddState("Dead", (statehandler)&TCompPlayerCameraController::onDead);
	transitionTimes["Dead"] = timeToGetToDeadPos;
	StateMachineController::ChangeState("Normal");
}

void TCompPlayerCameraController::debugInMenu() {
	TCompBase::debugInMenu();

	bool fov_changed = false;

	ImGui::Text("General");
	ImGui::Spacing();
	ImGui::Text("Current state: ");
	ImGui::Text(state.c_str());
	ImGui::Spacing();
	ImGui::DragFloat3("Target offset", &targetOffset.x, 0.1f, -100.f, 100.f);

	ImGui::Spacing();
	ImGui::Separator();

	ImGui::Text("Base State");

	float hsDeg = RAD2DEG(horizontal_sensitivity_base);
	if (ImGui::DragFloat("Horizontal Sensitivity Base", &hsDeg, 0.001f, 0.f, 7200.f))
		horizontal_sensitivity_base = DEG2RAD(hsDeg);
	float vsDeg = RAD2DEG(vertical_sensitivity_base);
	if (ImGui::DragFloat("Vertical Sensitivity Base", &vsDeg, 0.001f, 0.f, 7200.f))
		vertical_sensitivity_base = DEG2RAD(vsDeg);
	
	ImGui::DragFloat3("Camera offset from orbit center", &offsetFromOrbitCenter.x, 0.1f, -100.f, 100.f);
	float bFov = RAD2DEG(baseFOV);
	if (ImGui::DragFloat("Base FOV", &bFov, 0.1f, 0.01f, 360.f)) {
		baseFOV = DEG2RAD(bFov);
		fov_changed = true;
	}
	ImGui::DragFloat("Time to transition to base pos: ", &timeToGetToBasePos, 0.01f, 0.f, 100.f);

	ImGui::Spacing();
	ImGui::Separator();

	ImGui::Text("Sprint State");

	hsDeg = RAD2DEG(horizontal_sensitivity_sprint);
	if (ImGui::DragFloat("Horizontal Sensitivity Sprint", &hsDeg, 0.001f, 0.f, 7200.f))
		horizontal_sensitivity_sprint = DEG2RAD(hsDeg);
	vsDeg = RAD2DEG(vertical_sensitivity_sprint);
	if (ImGui::DragFloat("Vertical Sensitivity Sprint", &vsDeg, 0.001f, 0.f, 7200.f))
		vertical_sensitivity_sprint = DEG2RAD(vsDeg);

	ImGui::DragFloat3("Sprinting offset from player", &offsetSprinting.x, 0.1f, -100.f, 100.f);
	float sprintFov = RAD2DEG(sprintingFOV);
	if (ImGui::DragFloat("Sprinting FOV", &sprintFov, 0.1f, 0.01f, 360.f)) {
		sprintingFOV = DEG2RAD(sprintFov);
		fov_changed = true;
	}
	ImGui::DragFloat("Time to transition to sprinting pos: ", &timeToGetToSprintPos, 0.01f, 0.f, 100.f);

	ImGui::Spacing();
	ImGui::Separator();

	ImGui::Text("Aiming State");
	
	hsDeg = RAD2DEG(horizontal_sensitivity_aiming);
	if (ImGui::DragFloat("Horizontal Sensitivity Aiming", &hsDeg, 0.001f, 0.f, 7200.f))
		horizontal_sensitivity_aiming = DEG2RAD(hsDeg);
	vsDeg = RAD2DEG(vertical_sensitivity_aiming);
	if(ImGui::DragFloat("Vertical Sensitivity Aiming", &vsDeg, 0.001f, 0.f, 7200.f))
		vertical_sensitivity_aiming = DEG2RAD(vsDeg);
	
	ImGui::DragFloat3("Aiming offset from player", &offsetAiming.x, 0.1f, -100.f, 100.f);
	float aimFov = RAD2DEG(aimingFOV);
	if (ImGui::DragFloat("Aiming FOV", &aimFov, 0.1f, 0.01f, 360.f)) {
		aimingFOV = DEG2RAD(aimFov);
		fov_changed = true;
	}
	ImGui::DragFloat("Time to transition to aiming pos: ", &timeToGetToAimingPos, 0.01f, 0.f, 100.f);


	ImGui::Spacing();
	ImGui::Separator();

	ImGui::Text("Dead State");

	hsDeg = RAD2DEG(horizontal_sensitivity_dead);
	if(ImGui::DragFloat("Horizontal Sensitivity Dead", &hsDeg, 0.001f, 0.f, 7200.f))
		horizontal_sensitivity_dead = DEG2RAD(hsDeg);
	vsDeg = RAD2DEG(vertical_sensitivity_dead);
	if (ImGui::DragFloat("Vertical Sensitivity Dead", &vsDeg, 0.001f, 0.f, 7200.f))
		vertical_sensitivity_dead = DEG2RAD(vsDeg);
	
	ImGui::DragFloat3("Dead offset from player", &offsetDead.x, 0.1f, -100.f, 100.f);
	float deathFov = RAD2DEG(deadFOV);
	if (ImGui::DragFloat("Dead FOV", &deathFov, 0.1f, 0.01f, 360.f)) {
		deadFOV = DEG2RAD(deathFov);
		fov_changed = true;
	}
	ImGui::DragFloat("Time to transition to dead pos: ", &timeToGetToDeadPos, 0.01f, 0.f, 100.f);

	ImGui::Spacing();
	ImGui::Separator();

	ImGui::Text("Collision variables");
	ImGui::DragFloat("Time to get to real position if collision happened before: ", &timeToGetToCollisionToRealPos, 0.0001f, 0.f, 10.f);
	ImGui::DragFloat("Distance to keep from collision: ", &distanceToKeepFromCollision, 0.001f, 0.f, 100.f);

	if(fov_changed)
		setCameraCascadeFOV();
}

void TCompPlayerCameraController::update(float dt) {
	StateMachineController::update(dt);
}

void TCompPlayerCameraController::ChangeState(const std::string & stateToTransitionTo) {
	calledTransition = true;
	this->stateToTransitionTo = stateToTransitionTo;
}

void TCompPlayerCameraController::checkAndChangeState(const std::string& stateToTransitionTo) {
	if (stateToTransitionTo != state) {
		calledTransition = true;
		this->stateToTransitionTo = stateToTransitionTo;
	}
}

void TCompPlayerCameraController::registerMsgs() {
	DECL_MSG(TCompPlayerCameraController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompPlayerCameraController, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompPlayerCameraController, TMsgInputControllerStatus, OnInputStatus);
}

void TCompPlayerCameraController::onEntityCreated(const TMsgEntityCreated & msg) {
	// Fetch handles if not fetched.
	if (!camaraTransformHandle.isValid())
		camaraTransformHandle = getComponent<TCompTransform>();
	if (!cameraHandle.isValid())
		cameraHandle = getComponent<TCompCamera>();

	// We assume all components should have been found.
	// All of them should be destroyed at the same time so by checking
	// only one of them, we expect to be protected.
	if (!camaraTransformHandle.isValid())
		return;

	setCameraCascadeFOV();
}

void TCompPlayerCameraController::setCameraCascadeFOV() {
	// Get the bigger from from all the fovs the camera has and set it has the fov for the cascades
	// this will prevent changes in the shadow as the fovs change. Be careful not to set a very big FOV
	// or the shadow may degrade.
	float maxFov = -FLT_MAX;
	maxFov = std::max(baseFOV, maxFov);
	maxFov = std::max(aimingFOV, maxFov);
	maxFov = std::max(sprintingFOV, maxFov);
	maxFov = std::max(deadFOV, maxFov);

	TCompCamera * camera = cameraHandle;
	if(camera)
		camera->setCascadeFov(maxFov);
}

void TCompPlayerCameraController::OnInputStatus(const TMsgInputControllerStatus & msg) {
	active = msg.activate;
}

#include "engine.h"
#include "modules/module_camera_mixer.h"

void TCompPlayerCameraController::setMaxUpperPitch(float pitch) {
	minPitch = std::max(-pitchReference, -pitch * pitchReference);
}

void TCompPlayerCameraController::setMinLowerPitch(float pitch) {
	maxPitch = std::min(pitchReference, -pitch * pitchReference);
}

/* On Base */
void TCompPlayerCameraController::onBase(float dt){
	if (!checkHandles()) return;
	TCompTransform * targetTransform = targetTransformHandle;
	TCompTransform * camaraTransform = camaraTransformHandle;
	TCompCamera * camera = cameraHandle;

	VEC3 currentCamPos = camaraTransform->getPosition();
	VEC3 targetPosition = getTargetPosition(targetTransform); // position we will rotate around.
	
	/* Orbit around the player. */
	float currentYaw, currentPitch, nYawDelta = 0.0f, nPitchDelta = 0.0f;
	camaraTransform->getAngles(&currentYaw, &currentPitch);
	
	/* Orbit around the player. */
	
	camaraTransform->setPosition(targetPosition);

	// Calculate rotation
	// Consider input only if the camera is locked.
	if (camera->isLocked()) {
		nYawDelta = handleCameraHorizontalMovement(horizontal_sensitivity_base);
		nPitchDelta = handleCameraVerticalMovement(vertical_sensitivity_base);

	}
	currentYaw += nYawDelta;
	currentPitch += nPitchDelta;
	currentPitch = Maths::clamp(currentPitch, minPitch, maxPitch);
	camaraTransform->setAngles(currentYaw, currentPitch);

	// Now calculate camara position. We start by calculating the expected position.
	VEC3 finalPosition = getCameraPositionOnBase(targetPosition, camaraTransform);

	/* Now that we have the expected position, make all the test to get the actual final pos.
	This is, checking if we are in the middle of a transition or colliding. */

	// If we are in the middle of a transition from a previous position,
	// we lerp the values in this function.
	lerpIfTransitionOrCollision(camaraTransform, camera, finalPosition,
		currentYaw, currentPitch, currentYaw, currentPitch, baseFOV, dt);

	// We check if there is a collision and
	// update  the final position if necessary.
	if (checkForCollision(finalPosition, targetPosition))
		transitionFromCollision = true;
	else if (transitionFromCollision) {
		transitionFromCollision = false;
		setTransitionValues(currentCamPos, camera->getFov(), timeToGetToCollisionToRealPos);
	}
	
	// Finally, we set the new position.
	camaraTransform->setPosition(finalPosition);

	// If the camera got told to change its transition, start transition.
	checkIfTransitionToAnotherState(camaraTransform, camera);
}

Vector3 TCompPlayerCameraController::getCameraPositionOnBase(const Vector3 & targetPosition, TCompTransform * camaraTransform) {
	return targetPosition + camaraTransform->getFront() * offsetFromOrbitCenter.z
		+ camaraTransform->getLeft() * offsetFromOrbitCenter.x + camaraTransform->getUp() * offsetFromOrbitCenter.y;
}

/* On Aim */

void TCompPlayerCameraController::onAim(float dt) {
	if (!checkHandles()) return;
	TCompTransform * targetTransform = targetTransformHandle;
	TCompTransform * camaraTransform = camaraTransformHandle;
	TCompCamera * camera = cameraHandle;

	/* Rotate on its position */
	float currentYaw, currentPitch, nYawDelta = 0.0f, nPitchDelta = 0.0f;
	camaraTransform->getAngles(&currentYaw, &currentPitch);
	
	// Consider input only if the camera is locked.
	if (camera->isLocked()) {
		nYawDelta = handleCameraHorizontalMovement(horizontal_sensitivity_aiming);
		nPitchDelta = handleCameraVerticalMovement(vertical_sensitivity_aiming);
	}
	currentYaw += nYawDelta;
	currentPitch += nPitchDelta;
	currentPitch = Maths::clamp(currentPitch, minPitch, maxPitch);
	camaraTransform->setAngles(currentYaw, currentPitch);

	// Now calculate camara position in players shoulder.
	VEC3 targetPosition = getTargetPosition(targetTransform);
	VEC3 finalPosition = getCameraPositionOnAim(targetPosition, camaraTransform);
	
	/* Now that we have the expected position, make all the test to get the actual final pos.
	This is, checking if we are in the middle of a transition or colliding. */

	// If we are in the middle of a transition from a previous position,
	// we lerp the values in this function.
	lerpIfTransitionOrCollision(camaraTransform, camera, finalPosition,
		currentYaw, currentPitch, currentYaw, currentPitch, aimingFOV, dt);

	// We check if there is a collision and
	// update  the final position if necessary.
	if (checkForCollision(finalPosition, targetPosition))
		transitionFromCollision = true;
	else if (transitionFromCollision) {
		transitionFromCollision = false;
		setTransitionValues(camaraTransform->getPosition(), camera->getFov(), timeToGetToCollisionToRealPos);
	}

	// Finally, we set the new position.
	camaraTransform->setPosition(finalPosition);

	// If the camera got told to change its transition, start transition.
	checkIfTransitionToAnotherState(camaraTransform, camera);
}

Vector3 TCompPlayerCameraController::getCameraPositionOnAim(const Vector3 & targetPosition, TCompTransform * camaraTransform) {
	return targetPosition	+ camaraTransform->getFront() * offsetAiming.z
		+ camaraTransform->getLeft() * offsetAiming.x	+ camaraTransform->getUp() * offsetAiming.y;;
}

/* On Sprint */

void TCompPlayerCameraController::onSprint(float dt) {
	if (!checkHandles()) return;
	TCompTransform * targetTransform = targetTransformHandle;
	TCompTransform * camaraTransform = camaraTransformHandle;
	TCompCamera * camera = cameraHandle;

	VEC3 currentCamPos = camaraTransform->getPosition();

	/* First, get rotation input from player. */
	float nYawDelta = 0.0f, nPitchDelta = 0.0f;
	if (camera->isLocked()) { 	// Consider input only if the camera is locked.
		nYawDelta = handleCameraHorizontalMovement(horizontal_sensitivity_sprint);
		nPitchDelta = handleCameraVerticalMovement(vertical_sensitivity_sprint);
	}

	/* Sprint orbit. */
	// Get yaw from the target. This is the default yaw the camera should be in.
	float objYaw, objPitch;
	vectorToYawPitch(targetTransform->getFront(), objYaw, objPitch);

	// Get actual yaw of camera.
	float currentYaw, currentPitch;
	camaraTransform->getAngles(&currentYaw, &currentPitch);

	// Now, orbit.
	// First, move to target pos.
	VEC3 targetPosition = getTargetPosition(targetTransform);
	camaraTransform->setPosition(targetPosition);

	// Now, rotate in this position.
	float newYaw = currentYaw + nYawDelta;
	currentPitch += nPitchDelta;
	currentPitch = Maths::clamp(currentPitch, -maxPitch, maxPitch);
	camaraTransform->setAngles(newYaw, currentPitch);

	// This is the expected final position in the sprint behaviour.
	VEC3 finalPosition = getCameraPositionOnSprint(targetPosition, camaraTransform);
	
	/* Now that we have the expected position, make all the test to get the actual final pos.
	This is, checking if we are in the middle of a transition or colliding. */

	// If we are in the middle of a transition from a previous position,
	// we lerp the values in this function.
	lerpIfTransitionOrCollision(camaraTransform, camera, finalPosition,
		currentYaw, currentPitch, currentYaw, currentPitch, sprintingFOV, dt);

	// We check if there is a collision and
	// update  the final position if necessary.
	if (checkForCollision(finalPosition, targetPosition))
		transitionFromCollision = true;
	else if (transitionFromCollision) {
		transitionFromCollision = false;
		setTransitionValues(currentCamPos, camera->getFov(), timeToGetToCollisionToRealPos);
	}

	// Finally, we set the new position.
	camaraTransform->setPosition(finalPosition);

	// If the camera got told to change its transition, start transition.
	checkIfTransitionToAnotherState(camaraTransform, camera);
}

Vector3 TCompPlayerCameraController::getCameraPositionOnSprint(const Vector3 & targetPosition, TCompTransform * camaraTransform) {
	return targetPosition + camaraTransform->getFront() * offsetSprinting.z
		+ camaraTransform->getLeft() * offsetSprinting.x + camaraTransform->getUp() * offsetSprinting.y;
}

/* On Dead */

void TCompPlayerCameraController::onDead(float dt) {
	if (!checkHandles()) return;
	TCompTransform * targetTransform = targetTransformHandle;
	TCompTransform * camaraTransform = camaraTransformHandle;
	TCompCamera * camera = cameraHandle;

	VEC3 currentCamPos = camaraTransform->getPosition();
	VEC3 targetPosition = targetTransform->getPosition() + targetTransform->getUp(); // position we will rotate around.

	float yawDiffNewTarget = camaraTransform->getDeltaYawToAimTo(targetPosition);

	/* Orbit around the player. */
	float currentYaw, currentPitch, nYawDelta = 0.0f, nPitchDelta = 0.0f;
	camaraTransform->getAngles(&currentYaw, &currentPitch);

	/* Orbit around the player. */
	camaraTransform->setPosition(targetPosition);
	// Consider input only if the camera is locked.
	if (camera->isLocked()) {
		nYawDelta = handleCameraHorizontalMovement(horizontal_sensitivity_dead);
		nPitchDelta = handleCameraVerticalMovement(vertical_sensitivity_dead);
	}
	currentYaw += nYawDelta;
	float expectedAngle = currentPitch + nPitchDelta;
	if ((expectedAngle >= DEG2RAD(5.0f) && expectedAngle <= maxPitch))
		currentPitch = expectedAngle;
	else if (expectedAngle < DEG2RAD(5.0f))
		currentPitch = DEG2RAD(30.0f);
	camaraTransform->setAngles(currentYaw, currentPitch);

	// This is the expected final position in the dead behaviour.
	VEC3 finalPosition = getCameraPositionOnDead(targetPosition, camaraTransform);

	/* Now that we have the expected position, make all the test to get the actual final pos.
	This is, checking if we are in the middle of a transition or colliding. */
	
	// If we are in the middle of a transition from a previous position,
	// we lerp the values in this function.
	lerpIfTransitionOrCollision(camaraTransform, camera, finalPosition,
		currentYaw, currentPitch, currentYaw + yawDiffNewTarget, currentPitch, deadFOV, dt);

	// We check if there is a collision and
	// update  the final position if necessary.
	if (checkForCollision(finalPosition, targetPosition))
		transitionFromCollision = true;
	else if (transitionFromCollision) {
		transitionFromCollision = false;
		setTransitionValues(currentCamPos, camera->getFov(), timeToGetToCollisionToRealPos);
	}

	// Finally, we set the new position.
	camaraTransform->setPosition(finalPosition);

	// If the camera got told to change its transition, start transition.
	checkIfTransitionToAnotherState(camaraTransform, camera);
}

Vector3 TCompPlayerCameraController::getCameraPositionOnDead(const Vector3 & targetPosition, TCompTransform * camaraTransform) {
	return targetPosition + camaraTransform->getFront() * offsetDead.z + camaraTransform->getLeft() * offsetDead.x;
}

/* General functions. */

// Check all the handles used by the camera.
bool TCompPlayerCameraController::checkHandles() {
	// Fetch player if targetTransform is not valid.
	if (!targetTransformHandle.isValid()) {
		CEntity * ent = getEntityByName(targetName);
		if (!ent) return false;
		targetTransformHandle = ent->getComponent<TCompTransform>();
		// If not found, return false.
		if (!targetTransformHandle.isValid())
			return false;
	}
	
	// Fetch handles if not fetched.
	if(!camaraTransformHandle.isValid())
		camaraTransformHandle = getComponent<TCompTransform>();
	if (!cameraHandle.isValid())
		cameraHandle = getComponent<TCompCamera>();

	// We assume all components should have been found.
	// All of them should be destroyed at the same time so by checking
	// only one of them, we expect to be protected.
	if (!camaraTransformHandle.isValid())
		return false;

	return true;
}

// Sets a new target.
void TCompPlayerCameraController::setTarget(const std::string & nTargetName) {
	targetName = nTargetName;
	CEntity * ent = getEntityByName(targetName);
	if(ent == nullptr)
		targetTransformHandle = ent->getComponent<TCompTransform>();
}


float TCompPlayerCameraController::getAimingDegreesPlayer() {
	TCompTransform * cameraTransform = camaraTransformHandle;
	float yaw, pitch;
	cameraTransform->getAngles(&yaw, &pitch);
	return RAD2DEG(pitch);
}

// Sets the base starting pos of the camera.
void TCompPlayerCameraController::setBaseStartingPos() {
	TCompTransform * targetTransform = targetTransformHandle;
	TCompTransform * camaraTransform = getComponent<TCompTransform>();
	TCompCamera * camera = getComponent<TCompCamera>();
	if (targetTransform == nullptr || camaraTransform == nullptr || camera == nullptr) return;

	// Get position to put camera in.
	VEC3 targetPosition = getTargetPosition(targetTransform);
	VEC3 cameraPos = targetPosition + camaraTransform->getFront() * offsetFromOrbitCenter.z
		+ camaraTransform->getLeft() * offsetFromOrbitCenter.x + camaraTransform->getUp() * offsetFromOrbitCenter.y;
	
	// Get rotation for camera to rotate to center of orbit.
	float yawDiff = camaraTransform->getDeltaYawToAimTo(targetPosition);

	camaraTransform->setPosition(cameraPos);
	camaraTransform->setAngles(yawDiff, 0.0f);
	camera->setFov(baseFOV);
}

// Returns the target position to point to. Applies an offset to the
// transform of the entity which normally is at ground level.
VEC3 TCompPlayerCameraController::getTargetPosition(TCompTransform * targetTransform) {
	return targetTransform->getPosition()
		+ targetTransform->getFront() * targetOffset.z
		+ targetTransform->getLeft() * targetOffset.x
		+ targetTransform->getUp() * targetOffset.y;
}

/* Transition between camera states methods. */

// Lerps the camara to move to the next transition.
void TCompPlayerCameraController::lerpIfTransitionOrCollision(TCompTransform * camaraTransform,
	TCompCamera * camera, Vector3 & finalPosition, float currentYaw, float currentPitch,
	float finalYaw, float finalPitch, float finalFov, float dt) {
	if (transition) {
		currentTimeTransition += dt;
		if (timeForTransition == 0.0f)
			timeForTransition = currentTimeTransition;

		float time = currentTimeTransition / timeForTransition;
		finalPosition = VEC3::Lerp(startingTransitionPos, finalPosition, time);

		float nYaw = Maths::lerpAngle(currentYaw, finalYaw, time);
		float nPitch = Maths::lerpAngle(currentPitch, finalPitch, time);
		camaraTransform->setAngles(nYaw, nPitch);

		camera->setFov(Maths::lerp<float>(startingTransitionFov, finalFov, time));
		if (time >= 1.0f)
			transition = false;
	}
}

// Checks if a change state transition was called, sets the starting values
// of the transition (where the camera is right now).
void TCompPlayerCameraController::checkIfTransitionToAnotherState(TCompTransform * camaraTransform, TCompCamera * camera) {
	if (calledTransition) {
		calledTransition = false;
		setTransitionValues(camaraTransform->getPosition(), camera->getFov(), getTransitionTimeForNextState());
		StateMachineController::ChangeState(stateToTransitionTo);
	}
}

// Returns the transition time for the next state.
float TCompPlayerCameraController::getTransitionTimeForNextState() {
	return transitionTimes[stateToTransitionTo];
}

float TCompPlayerCameraController::handleCameraVerticalMovement(float sensitivity)
{
	VEC2 mouseDelta = EngineInput.mouse().getRawDelta();
	float nPitchDelta = mouseDelta.y * sensitivity;
	if (EngineInput["cam_up"].value > 0) {
		nPitchDelta -= sensitivity * abs(EngineInput["cam_up"].value);
	}
	if (EngineInput["cam_down"].value < 0) {
		nPitchDelta += sensitivity * abs(EngineInput["cam_down"].value);
	}

	return nPitchDelta;
}

float TCompPlayerCameraController::handleCameraHorizontalMovement(float sensitivity)
{
	VEC2 mouseDelta = EngineInput.mouse().getRawDelta();
	float nYawDelta = -mouseDelta.x * sensitivity;
	if (EngineInput["cam_left"].value < 0) {
		nYawDelta += sensitivity * abs(EngineInput["cam_left"].value);
	}
	if (EngineInput["cam_right"].value > 0) {
		nYawDelta -= sensitivity * abs(EngineInput["cam_right"].value);
	}

	return nYawDelta;
}



// Sets the starting transition values.
void TCompPlayerCameraController::setTransitionValues(const Vector3 & startingPosition, float startingFov, float nTimeTransition) {
	transition = true;
	startingTransitionPos = startingPosition;
	startingTransitionFov = startingFov;
	currentTimeTransition = 0.0f;
	timeForTransition = nTimeTransition;
}

/* Checks for collisions. */
bool TCompPlayerCameraController::checkForCollision(Vector3 & finalPosition, const Vector3 & targetPosition) {
	Vector3 playerToCamDir = finalPosition - targetPosition;
	float playerToCamLength = playerToCamDir.Length() + distanceToKeepFromCollision;
	playerToCamDir.Normalize();
	physx::PxRaycastBuffer hit = PhysUtils::shootCameraRaycast(targetPosition, playerToCamDir, playerToCamLength);

	// Collision detected
	if (hit.hasBlock) {
		finalPosition = targetPosition + playerToCamDir * (hit.block.distance - distanceToKeepFromCollision);
		return true;
	}
	return false;
}

void TCompPlayerCameraController::renderDebug() {}