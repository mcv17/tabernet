#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "geometry/curve.h"
#include "../module_scripting.h"

struct TMsgEntityCreated;

class TCompCameraCurveController : public TCompBase
{
	DECL_SIBLING_ACCESS();

	void onEntityCreated(const TMsgEntityCreated& msg);

public:
	~TCompCameraCurveController();

	void load(const json& j, TEntityParseContext& ctx);
	void update(float delta);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();
	static void declareInLua();

	void start();
	void setRatio(float ratio);
	void setTimeForCompleteRatio(float nTime) { _timeForCompleteRatio = nTime; }
	void setLooping(bool looping) { _loop = looping; }

private:
	ComponentEventHandle OnKnotReached;
	ComponentEventHandle OnEndReached;
	CHandle transformH;

	const CCurve * _curve = nullptr;
	int   currentSectionInCurve = -1;
	float _timeForCompleteRatio = 1.f; // How long it takes to get to the end of the curve if the curve has default speed modifiers.
	float _currentSpeedMod = 1.f;
	float _ratio = 0.f;
	bool  _loop = true;
};