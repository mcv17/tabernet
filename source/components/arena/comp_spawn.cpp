#include "mcv_platform.h"
#include "comp_spawn.h"
#include "entity/entity_parser.h"
#include "components/common/comp_transform.h"
#include "engine.h"

DECL_OBJ_MANAGER("spawn", TCompSpawn);

void TCompSpawn::load(const json& j, TEntityParseContext& ctx) {
	if (j.count("enemies_to_spawn") && j["enemies_to_spawn"].is_array()){
		for(auto & enemyType : j["enemies_to_spawn"])
			EnemyTypes.push_back(enemyType);
	}
}

void TCompSpawn::debugInMenu() {}

void TCompSpawn::registerMsgs() {
	DECL_MSG(TCompSpawn, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompSpawn, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompSpawn, TMsgEntityTriggerExit, onTriggerExit);
}

void TCompSpawn::declareInLua() {
	EngineScripting.declareComponent<TCompSpawn>
	(
		"TCompSpawn",
		"SpawnEntity", &TCompSpawn::SpawnEntity,
		"SpawnEnemy", sol::overload(
			sol::resolve<void()>(&TCompSpawn::SpawnEnemy),
			sol::resolve<void(const std::string&, bool)>(&TCompSpawn::SpawnEnemy)
		),
		"SpawnEntityInstant", sol::overload(
			sol::resolve<CHandle(bool)>(&TCompSpawn::SpawnEntityInstant),
			sol::resolve<CHandle(const std::string&, bool)>(&TCompSpawn::SpawnEntityInstant)
		),
		"GetSpawnPosition", &TCompSpawn::GetSpawnPosition,
		"GetSpawnTransform", &TCompSpawn::GetSpawnTransform
	);
}

void TCompSpawn::update(float dt) {
	for (auto & Enemy = EnemiesToSpawn.begin(); Enemy != EnemiesToSpawn.end(); Enemy++) {
		Enemy->TimeToSpawn = Enemy->TimeToSpawn - dt;
		if (Enemy->TimeToSpawn < 0.0f){
			CheckHowToSpawnEntity(Enemy->EntityToSpawn, Enemy->SpawnAlerted);
			EnemiesToSpawn.erase(Enemy);
			break;
		}
	}
}

void TCompSpawn::onEntityCreated(const TMsgEntityCreated & msg) {
	TransformH = getComponent<TCompTransform>();
}

void TCompSpawn::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	if (!isOccupied() && checkOwner()) {
		TMsgSpawnStatus msgToSend;
		msgToSend.occupied = true;
		msgToSend.spawnEntityHandle = CHandle(this).getOwner();
		spawnOwner.sendMsg(msgToSend);
	}
	occupied += 1;
}

void TCompSpawn::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	occupied -= 1;
	if (!isOccupied() && checkOwner()){
		TMsgSpawnStatus msg;
		msg.occupied = false;
		msg.spawnEntityHandle = CHandle(this).getOwner();
		spawnOwner.sendMsg(msg);
	}
}


void TCompSpawn::SpawnEntity(const std::string & EntityToSpawn, float TimeBeforeSpawning, bool SpawnAlerted) {
	if (isOccupied()) return;
	TCompTransform * transform = TransformH;
	if (!transform) return;

	occupied++; // If on deferred, we already add it as occupied even before the entity is spawned.
	SpawnEntityData entityData;
	entityData.EntityToSpawn = EntityToSpawn;
	entityData.TimeToSpawn = TimeBeforeSpawning;
	entityData.SpawnAlerted = SpawnAlerted;
	EnemiesToSpawn.push_back(entityData);
}

CHandle TCompSpawn::SpawnEntityInstant(bool AlertOnSpawn) {
	assert(EnemyTypes.size() == 1 && "I don't know which enemy to spawn");
	return SpawnEntityInstant(EnemyTypes[0], AlertOnSpawn);
}

CHandle TCompSpawn::SpawnEntityInstant(const std::string & EntityToSpawn, bool AlertOnSpawn) {
	if (isOccupied()) return CHandle();
	TCompTransform * transform = TransformH;
	if (!transform) return CHandle();
	
	CHandle SpawnedEntity = CheckHowToSpawnEntity(EntityToSpawn, false);
	return SpawnedEntity;
}

void TCompSpawn::SpawnEnemy() {
	assert(EnemyTypes.size() == 1 && "I don't know which enemy to spawn");
	SpawnEnemy(EnemyTypes[0]);
}

void TCompSpawn::SpawnEnemy(const std::string & EnemyToCreate, bool SpawnAlert) {
	if (isOccupied()) return;

	if(EnemyToCreate == "EnemyMelee")
		EngineScripting.call("spawnMelee", CHandle(this).getOwner(), SpawnAlert);
	else if (EnemyToCreate == "EnemyMeleeMini")
		EngineScripting.call("spawnMeleeMini", CHandle(this).getOwner(), SpawnAlert);
	else if (EnemyToCreate == "EnemyRanged")
		EngineScripting.call("spawnRanged", CHandle(this).getOwner(), SpawnAlert);
	else if (EnemyToCreate == "EnemyRangedBuffed")
		EngineScripting.call("spawnRangedBuffed", CHandle(this).getOwner(), SpawnAlert);
	else if (EnemyToCreate == "EnemyRangedSniperGround")
		EngineScripting.call("spawnRangedSniperGround", CHandle(this).getOwner(), SpawnAlert);
	else if (EnemyToCreate == "EnemyRangedSniperRoof")
		EngineScripting.call("spawnRangedSniperRoof", CHandle(this).getOwner(), SpawnAlert);
	else if (EnemyToCreate == "EnemyImmortal")
		EngineScripting.call("spawnImmortal", CHandle(this).getOwner(), SpawnAlert);
}


CHandle TCompSpawn::CheckHowToSpawnEntity(const std::string & EntityToSpawn, bool SpawnAlerted) {
	TCompTransform * transform = TransformH;
	if (!transform) return CHandle();

	Vector3 anglesToRotate = Vector3::Zero;
	transform->getAngles(&anglesToRotate.x, &anglesToRotate.y);

	CHandle SpawnedEntity;

	if (EntityToSpawn == "EnemyMelee")
		SpawnedEntity = LogicManager::Instance().CreateEnemyMelee(transform->getPosition(), anglesToRotate);
	else if (EntityToSpawn == "EnemyMeleeMini")
		SpawnedEntity = LogicManager::Instance().CreateEnemyMelee(transform->getPosition(), anglesToRotate);
	else if (EntityToSpawn == "EnemyRanged")
		SpawnedEntity = LogicManager::Instance().CreateEnemyRanged(transform->getPosition(), anglesToRotate);
	else if (EntityToSpawn == "EnemyRangedBuffed")
		SpawnedEntity = LogicManager::Instance().CreateEnemyRangedBuffed(transform->getPosition(), anglesToRotate);
	else if (EntityToSpawn == "EnemyRangedSniperGround")
		SpawnedEntity = LogicManager::Instance().CreateEnemyRangedSniperGround(transform->getPosition(), anglesToRotate);
	else if (EntityToSpawn == "EnemyRangedSniperRoof")
		SpawnedEntity = LogicManager::Instance().CreateEnemyRangedSniperRoof(transform->getPosition(), anglesToRotate);
	else if (EntityToSpawn == "EnemyImmortal")
		SpawnedEntity = LogicManager::Instance().CreateEnemyImmortal(transform->getPosition(), anglesToRotate);
	else
		SpawnedEntity = LogicManager::Instance().CreateEntity(EntityToSpawn, transform->getPosition(), anglesToRotate);

	// Decrease the occupied counter as we increased it when we first queried the entity to be spawned.
	// When the entity is spawned, the trigger will cast an event and increase it again.
	occupied--;

	// Send message with the spawned entity.
	SendMessageWithSpawnedEntity(SpawnedEntity);

	// alert message.
	if (SpawnAlerted) {
		TMsgAlertAI msg;
		if (SpawnedEntity.isValid())
			SpawnedEntity.sendMsg(msg);
	}

	return SpawnedEntity;
}

void TCompSpawn::SendMessageWithSpawnedEntity(CHandle SpawnedEntity){
	// Send message with the spawned entity.
	TMsgSpawnEntityCreated msg;
	msg.CreatedEntity = SpawnedEntity;
	if (spawnOwner.isValid())
		spawnOwner.sendMsg(msg);
}

Vector3 TCompSpawn::GetSpawnPosition() {
	TCompTransform * Transform = TransformH;
	if (!Transform) return Vector3();
	return Transform->getPosition();
}

TCompTransform * TCompSpawn::GetSpawnTransform() {
	TCompTransform * Transform = TransformH;
	if (!Transform) return nullptr;
	return Transform;
}

void TCompSpawn::SetSpawnOwner(CHandle nSpawnOwner){
	spawnOwner = nSpawnOwner;
}

bool TCompSpawn::checkOwner() {
	if (spawnOwner.isValid()) return true;
	return false;
}