#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "modules/module_physics.h"

class TCompTransform;

struct TMsgSpawnStatus {
	CHandle spawnEntityHandle;
	bool occupied;
	DECL_MSG_ID();
};

struct TMsgSpawnEntityCreated{
	CHandle CreatedEntity;
	DECL_MSG_ID();
};

class TCompSpawn : public TCompBase {
	DECL_SIBLING_ACCESS();

	struct SpawnEntityData {
		std::string EntityToSpawn;
		float TimeToSpawn;
		bool SpawnAlerted;
	};

	CHandle TransformH;
	
	/* Owner variables. */
	CHandle spawnOwner; // Owner of the spawn, normally an arena.
	std::vector<std::string> EnemyTypes;
	std::vector<SpawnEntityData> EnemiesToSpawn; // Path of enemy to spawn and time before doing it.

	/* Indicates if the spawn is occupied by an entity.
	Used to avoid spawning entities in it. */
	int occupied;

	/* Called when an entity enters or leaves the spawn trigger. */
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onTriggerExit(const TMsgEntityTriggerExit & msg);

	/* Checks if the owner exists, fetch it if not. */
	bool checkOwner();
	CHandle CheckHowToSpawnEntity(const std::string & EntityToSpawn, bool SpawnAlerted);
	void SendMessageWithSpawnedEntity(CHandle SpawnedEntity);

public:
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	void update(float dt);
	static void registerMsgs();
	static void declareInLua();

	/* Spawn functions. */
	void SpawnEntity(const std::string & Enemy, float TimeBeforeSpawning = 0.0f, bool SpawnAlerted = false);
	CHandle SpawnEntityInstant(bool AlertOnSpawn);
	CHandle SpawnEntityInstant(const std::string & EntityToSpawn, bool AlertOnSpawn);

	// Called by C++ code mostly. Used by arenas. Also calls all fxs for the given entity.
	void SpawnEnemy();
	void SpawnEnemy(const std::string & EnemyToCreate, bool SpawnAlert = false);
	
	/* Says if the spawn is occupied by other entities. */
	Vector3 GetSpawnPosition();
	TCompTransform * GetSpawnTransform();
	bool isOccupied() { return occupied > 0; }
	void SetSpawnOwner(CHandle spawnOwner);
	const std::vector<std::string> & GetSpawnEnemyTypes() { return EnemyTypes; }
};