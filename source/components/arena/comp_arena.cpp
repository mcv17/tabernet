#include "mcv_platform.h"
#include "comp_arena.h"
#include "entity/entity_parser.h"
#include "engine.h"

#include "components/common/comp_name.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_health.h"
#include "../common/comp_script.h"
#include "comp_spawn.h"
#include "../scenario/comp_doors.h"

DECL_OBJ_MANAGER("arena", TCompArena);

void TCompArena::EnemiesToSpawnOfGivenType::load(const json & j) {
	TypeOfEnemy = j.value("EnemyType", "");
	NumberOfEnemiesToSpawn = j.value("NumberOfEnemiesToSpawn", 0);
}

void TCompArena::EnemiesToSpawnOfGivenType::debugInMenu() {
	ImGui::Text("Enemy Type: %s", TypeOfEnemy);
	ImGui::Text("Number Of Enemies To Spawn: %d", NumberOfEnemiesToSpawn);
}

void TCompArena::ArenaWave::load(const json & j) {
	MinNumberOfEnemiesAliveDuringThisWave = j.value("MinNumberOfEnemiesAliveDuringThisWave", 0);
	MaxNumberOfEnemiesAliveDuringThisWave = j.value("MaxNumberOfEnemiesAliveDuringThisWave", INT_MAX);
	TimeBetweenSpawns = j.value("TimeBetweenSpawns", 0);
	StartNextWaveAfterSpawningAllEnemies = j.value("StartNextWaveAfterSpawningAllEnemies", false);
	TimeForNextWave = j.value("TimeForNextWave", 0.0f);

	if (j.count("EnemiesToSpawn") && j["EnemiesToSpawn"].is_array()) {
		for (auto & enemy : j["EnemiesToSpawn"]) {
			EnemiesToSpawnOfGivenType EnemyToSpawn;
			EnemyToSpawn.load(enemy);
			EnemiesToSpawn.push_back(EnemyToSpawn);
		}
	}
}

void TCompArena::ArenaWave::debugInMenu() {
	ImGui::Text("Min Number Of Enemies Alive During This Wave: %d", MinNumberOfEnemiesAliveDuringThisWave);
	ImGui::Text("Max Number Of Enemies Alive During This Wave: %d", MaxNumberOfEnemiesAliveDuringThisWave);
	ImGui::Text("Time Between Spawns: %f", TimeBetweenSpawns);
	ImGui::Text("Start Next Wave After Spawning All Enemies: %b", StartNextWaveAfterSpawningAllEnemies);
	ImGui::Text("Time For Next Wave: %f", TimeForNextWave);
}

bool TCompArena::ArenaWave::SpawnEnemy(std::map< EnemyType, std::vector<CHandle>> & freeSpawnsForEnemyType, float & TimeBeforeSpawningNextEnemy) {
	std::vector<EnemiesToSpawnOfGivenType *> EnemiesTriedToBeSpawned;
	for (auto & EnemyType : EnemiesToSpawn)
		EnemiesTriedToBeSpawned.push_back(&EnemyType);

	bool EnemySpawned = false;
	while (!EnemySpawned && EnemiesTriedToBeSpawned.size() > 0) {
		int EnemyToSpawnIndx = rand() % EnemiesTriedToBeSpawned.size();
		EnemiesToSpawnOfGivenType & spawnType = EnemiesToSpawn[EnemyToSpawnIndx];
		EnemySpawned = SpawnEnemyAtRandomSpawn(spawnType.TypeOfEnemy, freeSpawnsForEnemyType);

		if (!EnemySpawned) {
			EnemiesTriedToBeSpawned.erase(EnemiesTriedToBeSpawned.begin() + EnemyToSpawnIndx);
		}
		else {
			spawnType.NumberOfEnemiesToSpawn--;
			if (spawnType.NumberOfEnemiesToSpawn <= 0)
				EnemiesToSpawn.erase(EnemiesToSpawn.begin() + EnemyToSpawnIndx);
		}
	}

	if (EnemySpawned == false) return false;

	// There are no more enemies to spawn and all enemies are dead or next wave starts just after spawning all previous enemies.
	TimeBeforeSpawningNextEnemy = TimeBetweenSpawns;

	return true;
}

bool TCompArena::ArenaWave::SpawnEnemyAtRandomSpawn(EnemyType & enemyType, std::map< EnemyType, std::vector<CHandle>> & freeSpawnsForEnemyType) {
	if(!freeSpawnsForEnemyType.count(enemyType)) return false;
	if (freeSpawnsForEnemyType[enemyType].size() <= 0) return false;

	int EnemyToSpawnIndx = rand() % freeSpawnsForEnemyType[enemyType].size();
	TCompSpawn * spawn = freeSpawnsForEnemyType[enemyType][EnemyToSpawnIndx];
	if (spawn == nullptr) return false;

	spawn->SpawnEnemy(enemyType);
	return true;
}

void TCompArena::load(const json& j, TEntityParseContext& ctx) {
	ActivateFromScript = j.value("ActivateFromScript", ActivateFromScript);
	ScriptToCallOnArenaEnd = j.value("ScriptToCallOnArenaEnd", ScriptToCallOnArenaEnd);

	// Load waves.
	ArenaWaves.clear();
	if (j.count("Waves") && j["Waves"].is_array()) {
		for (auto & waveConfig : j["Waves"]) {
			ArenaWave wave;
			wave.load(waveConfig);
			ArenaWaves.push_back(wave);
		}
	}

	// Load doors.
	doorsToOpenNames.clear();
	if (j.count("DoorsToOpen") && j["DoorsToOpen"].is_array()) {
		for (auto & door : j["DoorsToOpen"])
			doorsToOpenNames.push_back(door.value("DoorName", ""));
	}

	// Load doors.
	doorsToCloseNames.clear();
	if (j.count("DoorsToClose") && j["DoorsToClose"].is_array()) {
		for (auto & door : j["DoorsToClose"])
			doorsToCloseNames.push_back(door.value("DoorName", ""));
	}

	// Load spawns.
	spawnNames.clear();
	if (j.count("Spawns") && j["Spawns"].is_array()) {
		for (auto & spawn : j["Spawns"])
			spawnNames.push_back(spawn.value("SpawnName", ""));
	}

	JArenaConfig = j;
}

void TCompArena::debugInMenu() {
	ImGui::Text("Current Wave: %d", CurrentWave);

	for (auto & arena : ArenaWaves) {
		arena.debugInMenu();
		ImGui::Spacing(0, 5);
		ImGui::Separator();
		ImGui::Spacing(0, 5);
	}

	ImGui::Text("Number of enemies alive: %d", Enemies.size());
}

void TCompArena::registerMsgs() {
	DECL_MSG(TCompArena, TMsgSceneCreated, onSceneCreated);
	DECL_MSG(TCompArena, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompArena, TMsgSpawnStatus, onSpawnStatus);
	DECL_MSG(TCompArena, TMsgSpawnEntityCreated, onSpawnCreatedEntity);
}

void TCompArena::declareInLua(){
	EngineScripting.declareComponent<TCompArena>
	(
		// name in LUA
		"TCompArena",
		"StartArena", &TCompArena::StartArena,
		"EndArena", &TCompArena::EndArena,
		"ResetArena", &TCompArena::ResetArena
	);
}

void TCompArena::onSceneCreated(const TMsgSceneCreated & msg) {
	FetchDoors();
	FetchSpawns();
}

void TCompArena::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	if (!active && !ActivateFromScript) {
		// Activate when the player enters.
		CEntity * entity = msg.h_entity;
		TCompTags * tags = entity->getComponent<TCompTags>();
		TCompHealth* healthComponent = entity->getComponent<TCompHealth>();
		if (tags && tags->hasTag(Utils::getID("player")) && healthComponent && !healthComponent->isDead())
			StartArena();
	}
}

void TCompArena::onSpawnStatus(const TMsgSpawnStatus & msg) {
	CEntity * SpawnEntity = msg.spawnEntityHandle;
	if (!SpawnEntity) return;

	CHandle SpawnCompH = SpawnEntity->getComponent<TCompSpawn>();
	TCompSpawn * SpawnComp = SpawnCompH;
	if (!SpawnComp) return;
	const std::vector<EnemyType> & enemyTypesToSpawn = SpawnComp->GetSpawnEnemyTypes();

	if (msg.occupied) {
		for (auto & enemyType : enemyTypesToSpawn) {
			if (freeSpawnsForEnemyType.count(enemyType)) {
				// Check if present.
				int index = 0;
				for (auto & spawn : freeSpawnsForEnemyType[enemyType]) {
					if (spawn == SpawnCompH){
						freeSpawnsForEnemyType[enemyType].erase(freeSpawnsForEnemyType[enemyType].begin() + index);
						break;
					}
					index++;
				}
			}
		}
	}
	else {
		for (auto & enemyType : enemyTypesToSpawn) {
			if (freeSpawnsForEnemyType.count(enemyType)){
				// Check it's not present.
				bool present = false;
				for (auto & spawn : freeSpawnsForEnemyType[enemyType])
					present = spawn == SpawnCompH;

				// Add if not present.
				if(!present)
					freeSpawnsForEnemyType[enemyType].push_back(SpawnCompH);
			}
		}
	}
}

void TCompArena::onSpawnCreatedEntity(const TMsgSpawnEntityCreated & msg) {
	// Get the entity that got spawned.
	Enemies.push_back(msg.CreatedEntity);

	// Alert entity
	msg.CreatedEntity.sendMsg(TMsgAlertAI());
}

void TCompArena::update(float dt) {
	if (!active) {
		if (Enemies.size() > 0)
			KillAllSpawns();
		return;
	}

	if (TimeBeforeNextWave > 0.0f)
		TimeBeforeNextWave = TimeBeforeNextWave - dt;

	if (timeBeforeNextSpawn > 0.0f)
		timeBeforeNextSpawn = timeBeforeNextSpawn - dt;

	// Check if any enemies died.
	CheckIfAnyEnemiesAreDead();

	// Check if we must spawn enemies in the current wave.
	if(CheckIfWaveIsReady())
		SpawnEnemy();

	// All enemies are spawned and dead.
	if (CheckIfEndArena())
		EndArena();
}

/* Start the arena. */
void TCompArena::StartArena() {
	// Set arena to active.
	active = true;

	// Just in case.
	if (CurrentWave < ArenaWaves.size()) {
		ArenaWave & arenaWave = ArenaWaves[CurrentWave];
		timeBeforeNextSpawn = arenaWave.TimeBetweenSpawns;

		// Send message to close doors.
		for (auto & doorToClose : doorsToClose) {
			CEntity * doorToCloseEntity = doorToClose;
			if (doorToCloseEntity) {
				TCompDoor * Door = doorToCloseEntity->getComponent<TCompDoor>();
				if (Door)
					Door->CloseDoors();
			}
		}
		
		// See if there is anything else that we must do. Like calling a script or something else.
	}
}

void TCompArena::FetchDoors(){
	for (auto & doorName : doorsToOpenNames) {
		CHandle doorEntityHandle = getEntityByName(doorName);
		assert(doorEntityHandle.isValid() && "Attention, door entity handle is invalid.");
		if(doorEntityHandle.isValid())
			doorsToOpen.push_back(doorEntityHandle);
	}

	for (auto & doorName : doorsToCloseNames) {
		CHandle doorEntityHandle = getEntityByName(doorName);
		assert(doorEntityHandle.isValid() && "Attention, door entity handle is invalid.");
		if (doorEntityHandle.isValid())
			doorsToClose.push_back(doorEntityHandle);
	}
}

void TCompArena::FetchSpawns(){
	// Handle of the arena.
	CHandle arenaHandle = CHandle(this).getOwner();

	/* Now fetch all spawns. */
	for (auto & name : spawnNames) {
		CHandle spawnHand = getEntityByName(name);
		if (!spawnHand.isValid())
			Utils::fatal("Spawn with name %s not valid.\n", name.c_str());

		// Add to vector with all spawns.
		spawnHandles.push_back(spawnHand);

		// Get the spawn component.
		CEntity * spawnEntity = spawnHand;
		TCompSpawn * spawnComp = spawnEntity->getComponent<TCompSpawn>();
		if (spawnComp == nullptr)
			Utils::fatal("Spawn with name %s doesn't have spawn component.\n", name.c_str());

		// Set its owner to be the arena.
		spawnComp->SetSpawnOwner(arenaHandle);

		// Add to map. Allow to identify what spawns can identify what types of entities.
		const std::vector<EnemyType> & enemyTypesToSpawn = spawnComp->GetSpawnEnemyTypes();
		for (auto & enemyType : enemyTypesToSpawn) {
			if (!freeSpawnsForEnemyType.count(enemyType))
				freeSpawnsForEnemyType[enemyType] = std::vector<CHandle>();
			freeSpawnsForEnemyType[enemyType].push_back(CHandle(spawnComp));
		}
	}
}

void TCompArena::KillAllSpawns() {
	for (auto & enemy : Enemies) {
		CEntity * enemyEntity = enemy;
		if (enemyEntity && enemyEntity->isActive()) {
			//EngineLogicManager.killEntity(enemy);
			EngineLogicManager.smartRemoveEntity(enemy);
		}
	}
	Enemies.clear();
}

void TCompArena::EndArena() {
	// Send message to open doors.
	for (auto & doorToOpen : doorsToOpen) {
		CEntity * doorToOpenEntity = doorToOpen;
		if (doorToOpenEntity) {
			TCompDoor * Door = doorToOpenEntity->getComponent<TCompDoor>();
			if (Door)
				Door->OpenDoors();
		}
	}

	// Destroy the spawns.
	//for (auto & spawnHandle : spawnHandles) {
	//	if(spawnHandle.isValid())
	//		spawnHandle.destroy();
	//}

	// Function to call on arena end.
	if (ScriptToCallOnArenaEnd != "") {
		TCompScript * Script = getComponent<TCompScript>();
		if (Script)
			Script->executeFunction(ScriptToCallOnArenaEnd);
	}

	active = false;
}

void TCompArena::ResetArena() {
	CurrentWave = 0;
	active = false;
	timeBeforeNextSpawn = 0.0f;
	TimeBeforeNextWave = 0.0f;
	load(JArenaConfig, TEntityParseContext());

	for (auto & doorToOpen : doorsToOpen) {
		CEntity * doorToOpenEntity = doorToOpen;
		if (doorToOpenEntity) {
			TCompDoor * Door = doorToOpenEntity->getComponent<TCompDoor>();
			if (Door)
				Door->ResetDoors();
		}
	}

}


bool TCompArena::CheckIfEndArena() {
	return Enemies.size() == 0 && CurrentWave == ArenaWaves.size();
}

void TCompArena::CheckIfAnyEnemiesAreDead() {
	Enemies.erase(std::remove_if(Enemies.begin(), Enemies.end(), [this](const CHandle & h) {
		return CheckIfEnemyDead(h);
	}), Enemies.end());
}

bool TCompArena::CheckIfEnemyDead(CEntity * e) {
	if (!e) return true;
	TCompHealth * health = e->getComponent<TCompHealth>();
	if (health)
		return health->isDead();
	else
		return true;
}

void TCompArena::SpawnEnemy() {
	
	ArenaWave & arenaWave = ArenaWaves[CurrentWave];

	// Don't spawn if there are more enemies than the maximum number the wave allows.
	int NumberOfEnemiesAlive = Enemies.size();
	if (NumberOfEnemiesAlive >= arenaWave.MaxNumberOfEnemiesAliveDuringThisWave) return;

	// Check for spawning.
	if (arenaWave.CheckIfWaveEnded(NumberOfEnemiesAlive) == false) {
		bool WaveFinished = false;
		while (timeBeforeNextSpawn <= 0.0f || NumberOfEnemiesAlive < arenaWave.MinNumberOfEnemiesAliveDuringThisWave) {
			bool succededSpawning = arenaWave.SpawnEnemy(freeSpawnsForEnemyType, timeBeforeNextSpawn);
			if (!succededSpawning) return;
		}
	}
	else
		StartNextWave(arenaWave.TimeForNextWave);
}


void TCompArena::StartNextWave(float nTimeBeforeNextWave) {
	CurrentWave++;
	TimeBeforeNextWave = nTimeBeforeNextWave;
	if(CurrentWave < ArenaWaves.size())
	timeBeforeNextSpawn = ArenaWaves[CurrentWave].TimeBetweenSpawns;
}

bool TCompArena::CheckIfWaveIsReady() {
	return (CurrentWave < ArenaWaves.size() && TimeBeforeNextWave <= 0.0f); // Wave is on the way.
}