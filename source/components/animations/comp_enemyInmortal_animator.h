#pragma once
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "fsm/context.h"
#include "fsm/fsm.h"
#include "fsm/state.h"


/* Enemy inmortal animator class. */

class TCompEnemyInmortalAnimator : public TCompBase{
	DECL_SIBLING_ACCESS();

	bool _start;

	// FSM 
	std::string _fsm_path;
	const CFSM* _fsm = nullptr;
	CFSMContext _context;

	// Definitions
	std::string _attackingVarName = "attacking";
	std::string _knockbackVarName = "knockback";
	std::string _speedVarName = "speed";

public:
	void load(const json& j, TEntityParseContext& ctx);
	static void registerMsgs();
	void debugInMenu();
	void update(float dt);

	TVariable* getVariable(std::string name);
	void setIsAttacking(bool value);
	void setKnockback(int origin);
	void setSpeed(float speed);

private:
	// Once the entity is created, register animtions if they were not registered before and start the two fsms.
	// Also get the skeleton component.
	void onEntityCreated(const TMsgEntityCreated & msg);
	// Used for disabling the animator if the logic must be stopped.
	void onLogicStatus(const TMsgLogicStatus & msg);
};

