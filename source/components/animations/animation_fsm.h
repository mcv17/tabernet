#pragma once

#include <map>
#include "components/common/comp_base.h"

class IAnimState;

/* A FSM based in the one created by Albert (fsm.h and context.cpp).
We have created a new FSM due to the lack of multiple conditions and functions for transitions.
In the future we may delete this FSM and use the one from Albert again. For now we use this
because it is faster than modifying the parser. */

class AnimationFSM
{
public:

	AnimationFSM();
	virtual ~AnimationFSM();
	
	virtual void init(CHandle nAnimator, const json & json) = 0;
	void start();
	void stop();
	void update(float dt);
	void changeToState(const std::string & stateName);
	void debugInMenu();
	
	void setStartState(const std::string & nState) { _startState = nState; }
	IAnimState * getState(const std::string & nState) { return statemap[nState]; }
	float getTimeInState() { return _timeInState; }

protected:
	void addState(const std::string & name, IAnimState * newState, const json & j);
	virtual void loadStates(const json & j) = 0;

	IAnimState * _currentState = nullptr;
	std::string _startState;
	std::map<std::string, IAnimState *> statemap;
	float _timeInState = 0.0f;
};


class IAnimState {
protected:
	AnimationFSM * fsm; // State machine.
	std::string _stateName; // Name of the state.
public:
	virtual void load(const std::string & nName, AnimationFSM * nFSM, const json & j) = 0;
	virtual void update(AnimationFSM & fsm, float dt) {}
	virtual void onEnter(AnimationFSM & fsm) {}
	virtual void onExit(AnimationFSM & fsm) {}
	virtual void debugInMenu() const;

	const std::string & getStateName() const { return _stateName; }
};