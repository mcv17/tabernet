#include "mcv_platform.h"
#include "animation_blenders.h"

void OneDimensionalBlender::addMotion(AnimationID animation, float threshold, bool isDummy) {
	Motion newMotion = { animation, 0.0f, threshold, -1.0f, -1.0f, isDummy };
	sorted_motion_thresholds.push_back(newMotion);

	// If we have only one animation the blender is pointless and doesn't work by default
	if (sorted_motion_thresholds.size() == 1) return;

	// Sort the motions based on their threshold.
	sortMotions();
	recalculateWeights();
}

void OneDimensionalBlender::addDummyMotion(float threshold) {
	addMotion(-1, threshold, true);
}

void OneDimensionalBlender::removeMotion(int indexInVector) {
	if (sorted_motion_thresholds.size() >= indexInVector) return;
	sorted_motion_thresholds.erase(sorted_motion_thresholds.begin() + indexInVector);
	sortMotions();
	recalculateWeights();
}

void OneDimensionalBlender::updateWeights(TCompSkeleton * animator, float delay) {
	for (int i = 0; i < sorted_motion_thresholds.size(); ++i) {
		Motion & motion = sorted_motion_thresholds[i];
		updateWeight(motion);
		if (!motion.isDummy)
			animator->UpdateCycleAnimationWeights(motion.animation, motion.current_weight, delay, delay);
	}
}

void OneDimensionalBlender::endMotions(TCompSkeleton * animator, float delay) {
	for (int i = 0; i < sorted_motion_thresholds.size(); ++i) {
		Motion & motion = sorted_motion_thresholds[i];
		if (!motion.isDummy)
			animator->RemoveAnimation(motion.animation, delay);
	}
}

void OneDimensionalBlender::updateWeight(Motion & motion) {
	float rightSideMax = motion.rightSideDist == -1.0f ? motion.threshold : motion.threshold + motion.rightSideDist;
	float leftSideMax = motion.leftSideDist == -1.0f ? motion.threshold : motion.threshold - motion.leftSideDist;

	// Is on the left of this motion?
	if (blendParam <= motion.threshold && blendParam >= leftSideMax) {
		motion.current_weight = 1 - (motion.threshold - blendParam) / motion.leftSideDist;
		return;
	}

	// Is on the right of this motion?
	if (motion.threshold <= blendParam && blendParam <= rightSideMax) {
		motion.current_weight = 1 - (blendParam - motion.threshold) / motion.rightSideDist;
		return;
	}

	// else, the current weight is 0.0f.
	motion.current_weight = 0.0f;
}

void OneDimensionalBlender::sortMotions() {
	std::sort(sorted_motion_thresholds.begin(), sorted_motion_thresholds.end(), [](const Motion & m1, const Motion & m2) {
		return m1.threshold < m2.threshold;
	});

	minBlendParamVal = sorted_motion_thresholds.front().threshold;
	maxBlendParamVal = sorted_motion_thresholds.back().threshold;
}

void OneDimensionalBlender::recalculateWeights() {
	// Now calculate the distance between motions.
	for (int i = 0; i < sorted_motion_thresholds.size(); i++) {
		// Sides of the vector.
		if (i == 0) {
			Motion & motion = sorted_motion_thresholds[i];
			Motion & nextMotion = sorted_motion_thresholds[i + 1];
			motion.rightSideDist = nextMotion.threshold - motion.threshold;
		}
		else if (i == sorted_motion_thresholds.size() - 1) {
			Motion & motion = sorted_motion_thresholds[i];
			Motion & prevMotion = sorted_motion_thresholds[i - 1];
			motion.leftSideDist = motion.threshold - prevMotion.threshold;
		}// Normal value.
		else {
			Motion & motion = sorted_motion_thresholds[i];
			Motion & nextMotion = sorted_motion_thresholds[i + 1];
			motion.rightSideDist = nextMotion.threshold - motion.threshold;
			Motion & prevMotion = sorted_motion_thresholds[i - 1];
			motion.leftSideDist = motion.threshold - prevMotion.threshold;
		}
	}
}

void TwoDimensionalBlender::addMotion(AnimationID animation, float vertical_threshold, float horizontal_threshold, bool isDummy) {
	/* Create horizontal vector if necessary. */

	// Check if the vector is already present.
	MotionVector * motionVector = nullptr;
	for (auto & motionV : sorted_motion_thresholds) {
		if (motionV.horizontal_threshold == horizontal_threshold) {
			motionVector = &motionV;
			break;
		}
	}
	
	// Create it if not present.
	if (motionVector == nullptr) {
		MotionVector mVector = { std::vector<Motion>(), 0.0f, horizontal_threshold, -1.0f, -1.0f };
		sorted_motion_thresholds.push_back(mVector);

		// If we have only one animation the blender is pointless and doesn't work by default
		if (sorted_motion_thresholds.size() > 1) {
			sortHorizontalVector();
			recalculateHorizontalWeights();
		}

		for (auto & motionV : sorted_motion_thresholds) {
			if (motionV.horizontal_threshold == horizontal_threshold) {
				motionVector = &motionV;
				break;
			}
		}
	}

	/* Add animation to the horizontal vector if necessary. */
	
	// Add animation to vector.
	Motion newMotion = { animation, 0.0f, vertical_threshold, -1.0f, -1.0f, isDummy };
	motionVector->motion_vector_thresholds.push_back(newMotion);

	if (motionVector->motion_vector_thresholds.size() == 1) return;

	// Sort the motions based on their threshold.
	sortVerticalVector(motionVector->motion_vector_thresholds);
	recalculateVerticalWeights(motionVector->motion_vector_thresholds);
}

void TwoDimensionalBlender::addDummyMotion(float vertical_threshold, float horizontal_threshold) {
	addMotion(-1, vertical_threshold, horizontal_threshold, true);
}

void TwoDimensionalBlender::removeMotion(float horizontal_threshold, int indexInVector) {
	// Check if the vector is already present.
	MotionVector * motionVector = nullptr;
	for (auto & motionV : sorted_motion_thresholds) {
		if (motionV.horizontal_threshold == horizontal_threshold) {
			motionVector = &motionV;
			break;
		}
	}
	if (motionVector == nullptr) return;
	motionVector->motion_vector_thresholds.erase(motionVector->motion_vector_thresholds.begin() + indexInVector);
	sortVerticalVector(motionVector->motion_vector_thresholds);
	recalculateVerticalWeights(motionVector->motion_vector_thresholds);
}

void TwoDimensionalBlender::updateWeights(TCompSkeleton * animator, float delay) {
	for (int i = 0; i < sorted_motion_thresholds.size(); ++i){
		MotionVector & vector = sorted_motion_thresholds[i];
		updateWeightVector(vector);
		
		for (int j = 0; j < vector.motion_vector_thresholds.size(); ++j) {
			Motion & motion = vector.motion_vector_thresholds[j];
			updateWeight(motion, vector.current_weight);
			if (!motion.isDummy)
				animator->UpdateCycleAnimationWeights(motion.animation, motion.current_weight, delay, delay);
		}
	}
}

void TwoDimensionalBlender::updateWeightVector(MotionVector & motionVector) {
	float rightSideMax = motionVector.rightSideDist == -1.0f ? motionVector.horizontal_threshold : motionVector.horizontal_threshold + motionVector.rightSideDist;
	float leftSideMax = motionVector.leftSideDist == -1.0f ? motionVector.horizontal_threshold : motionVector.horizontal_threshold - motionVector.leftSideDist;

	// Is on the left of this motion?
	if (blendParams.x <= motionVector.horizontal_threshold && blendParams.x >= leftSideMax) {
		motionVector.current_weight = 1 - (motionVector.horizontal_threshold - blendParams.x) / motionVector.leftSideDist;
		return;
	}

	// Is on the right of this motion?
	if (motionVector.horizontal_threshold <= blendParams.x && blendParams.x <= rightSideMax) {
		motionVector.current_weight = 1 - (blendParams.x - motionVector.horizontal_threshold) / motionVector.rightSideDist;
		return;
	}

	// else, the current weight is 0.0f.
	motionVector.current_weight = 0.0f;
}

void TwoDimensionalBlender::updateWeight(Motion & motion, float vectorWeight) {
	float rightSideMax = motion.rightSideDist == -1.0f ? motion.threshold : motion.threshold + motion.rightSideDist;
	float leftSideMax = motion.leftSideDist == -1.0f ? motion.threshold : motion.threshold - motion.leftSideDist;

	// Is on the left of this motion?
	if (blendParams.y <= motion.threshold && blendParams.y >= leftSideMax) {
		motion.current_weight = (1 - (motion.threshold - blendParams.y) / motion.leftSideDist) * vectorWeight;
		return;
	}

	// Is on the right of this motion?
	if (motion.threshold <= blendParams.y && blendParams.y <= rightSideMax) {
		motion.current_weight = (1 - (blendParams.y - motion.threshold) / motion.rightSideDist) * vectorWeight;
		return;
	}

	// else, the current weight is 0.0f.
	motion.current_weight = 0.0f;
}

void TwoDimensionalBlender::endMotions(TCompSkeleton * animator, float delay) {
	for (int j = 0; j < sorted_motion_thresholds.size(); ++j) {
		std::vector<Motion> * m = &sorted_motion_thresholds[j].motion_vector_thresholds;
		for (int i = 0; i < m->size(); ++i) {
			Motion & motion = (*m)[i];
			if (!motion.isDummy)
				animator->RemoveAnimation(motion.animation, delay);
		}
	}
}

void TwoDimensionalBlender::sortHorizontalVector() {
	std::sort(sorted_motion_thresholds.begin(), sorted_motion_thresholds.end(), [](const MotionVector & m1, const MotionVector & m2) {
		return m1.horizontal_threshold < m2.horizontal_threshold;
	});

	minBlendParams.x = sorted_motion_thresholds.front().horizontal_threshold;
	maxBlendParams.x = sorted_motion_thresholds.back().horizontal_threshold;
}

void TwoDimensionalBlender::sortVerticalVector(std::vector<Motion> & motions) {
	std::sort(motions.begin(), motions.end(), [](const Motion & m1, const Motion & m2) {
		return m1.threshold < m2.threshold;
	});

	minBlendParams.y = motions.front().threshold;
	maxBlendParams.y = motions.back().threshold;
}

void TwoDimensionalBlender::recalculateHorizontalWeights() {
	// Now calculate the distance between motions.
	for (int i = 0; i < sorted_motion_thresholds.size(); i++) {
		// Sides of the vector.
		if (i == 0) {
			MotionVector & motion = sorted_motion_thresholds[i];
			MotionVector & nextMotion = sorted_motion_thresholds[i + 1];
			motion.rightSideDist = nextMotion.horizontal_threshold - motion.horizontal_threshold;
		}
		else if (i == sorted_motion_thresholds.size() - 1) {
			MotionVector & motion = sorted_motion_thresholds[i];
			MotionVector & prevMotion = sorted_motion_thresholds[i - 1];
			motion.leftSideDist = motion.horizontal_threshold - prevMotion.horizontal_threshold;
		}// Normal value.
		else {
			MotionVector & motion = sorted_motion_thresholds[i];
			MotionVector & nextMotion = sorted_motion_thresholds[i + 1];
			motion.rightSideDist = nextMotion.horizontal_threshold - motion.horizontal_threshold;
			MotionVector & prevMotion = sorted_motion_thresholds[i - 1];
			motion.leftSideDist = motion.horizontal_threshold - prevMotion.horizontal_threshold;
		}
	}
}

void TwoDimensionalBlender::recalculateVerticalWeights(std::vector<Motion> & motions) {
	// Now calculate the distance between motions.
	for (int i = 0; i < motions.size(); i++) {
		// Sides of the vector.
		if (i == 0) {
			Motion & motion = motions[i];
			Motion & nextMotion = motions[i + 1];
			motion.rightSideDist = nextMotion.threshold - motion.threshold;
		}
		else if (i == motions.size() - 1) {
			Motion & motion = motions[i];
			Motion & prevMotion = motions[i - 1];
			motion.leftSideDist = motion.threshold - prevMotion.threshold;
		}// Normal value.
		else {
			Motion & motion = motions[i];
			Motion & nextMotion = motions[i + 1];
			motion.rightSideDist = nextMotion.threshold - motion.threshold;
			Motion & prevMotion = motions[i - 1];
			motion.leftSideDist = motion.threshold - prevMotion.threshold;
		}
	}
}