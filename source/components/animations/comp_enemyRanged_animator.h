#pragma once
#include "entity/entity.h"
#include "fsm/context.h"
#include "fsm/fsm.h"
#include "fsm/state.h"


/* Player animator class. */

struct TMsgEntityCreated;
struct TMsgResetComponent;
struct TMsgEntityDead;
struct TMsgLogicStatus;

class TCompEnemyRangedAnimator : public TCompBase{
	DECL_SIBLING_ACCESS();

	bool _start;

	// FSM 
	std::string _fsm_path;
	const CFSM* _fsm = nullptr;
	CFSMContext _context;

	// Definitions
	const std::string _attackingVarName = "attacking";
	const std::string _speedVarName = "speed";
	const std::string _inAttackPoseVarName = "inAttackPose";
	const std::string _isInjuredVarName = "isInjured";

public:
	void load(const json& j, TEntityParseContext& ctx);
	static void registerMsgs();
	static void declareInLua();
	void debugInMenu();
	void update(float dt);

	const TVariable* getVariable(const std::string& name) const;
	void setIsAttacking(bool value);
	void setSpeed(float speed);
	void setInAttackPose(bool value);
	void setIsInjured(bool value);
	void reset();

private:
	// Once the entity is created, register animtions if they were not registered before and start the two fsms.
	// Also get the skeleton component.
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onEntityDead(const TMsgEntityDead & msg);
	void onResetComponent(const TMsgResetComponent & msg);
	// Used for disabling the animator if the logic must be stopped.
	void onLogicStatus(const TMsgLogicStatus & msg);


};

