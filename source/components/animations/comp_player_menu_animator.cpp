#include "mcv_platform.h"
#include "comp_player_menu_animator.h"
#include "skeleton/comp_skeleton.h"
#include "fsm/module_fsm.h"
#include "engine.h"

DECL_OBJ_MANAGER("player_menu_animator", TCompPlayerMenuAnimator);

void TCompPlayerMenuAnimator::load(const json& j, TEntityParseContext& ctx) {
	_fsm_path = j["src"];
}

void TCompPlayerMenuAnimator::registerMsgs() {
	DECL_MSG(TCompPlayerMenuAnimator, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompPlayerMenuAnimator, TMsgLogicStatus, onLogicStatus);
}

void TCompPlayerMenuAnimator::debugInMenu() {
	TCompBase::debugInMenu();

	CFSM* fsm = const_cast<CFSM*>(_fsm);
	IState* currentState = _context.getCurrentState();
	if (!currentState)
		return;
	ImGui::Text("Current anim: %s", currentState->_name.c_str());
	ImGui::Text("Time in State: %f", _context.getTimeInState());
	ImGui::Separator();
	ImGui::Text("Variables:");
	// Get the variables for every fsm
	for (TVariable var : fsm->getVariables()) {			
		// Check types and make the custom ImGui section 
		if (var.getType() == "boolean") {
			bool value = std::get<bool>(var._value);
			if (ImGui::Checkbox(var._name.c_str(), &value)) {
				_context.setVariable(TVariable(var._name, value));
			}
		}
		else if (var.getType() == "int") {
			int value = std::get<int>(var._value);

			if (ImGui::DragInt(var._name.c_str(), &value, 1, -1, 10)) {
				_context.setVariable(TVariable(var._name, value));
			}
		}
		else if (var.getType() == "float") {
			float value = std::get<float>(var._value);
			
			if (ImGui::DragFloat(var._name.c_str(), &value, 0.1f, -10.0f, 10.0f)) {
				_context.setVariable(TVariable(var._name, value));
			}
		}

		if (currentState->_blenders.size() > 0) {

			ImGui::Separator();
			ImGui::Text("Blendings:");
			int i = 0;
			for (auto blender : currentState->_blenders) {
				ImGui::Text("Blender %d", i++);
				std::vector<OneDimensionalBlender::Motion> motions = blender.sorted_motion_thresholds;
				ImGui::Text("	Motions :   name - threshold");
				for (auto motion : motions) {
					if (motion.animation == -1) continue;
					std::string animName = _context.getSkeletonComp()->getAnimationName(motion.animation);
					std::string text = animName + "  " + std::to_string(motion.threshold);
					ImGui::Text(text.c_str());
				}
			}
		}
	}
}

void TCompPlayerMenuAnimator::onEntityCreated(const TMsgEntityCreated & msg) {
	_fsm = EngineResources.getResource(_fsm_path)->as<CFSM>();

	/* Register animations to Cal3D */
	int animId = 0;
	for (auto state : _fsm->getStates()) {

		TCompSkeleton* skel = getComponent<TCompSkeleton>();
		// Register animations to Cal3D
		for (TAnimation anim : state->_animations) {
			state->animationID = skel->getAnimationID(anim.filename);
		}
	}
	_context.init(const_cast<CFSM*>(_fsm), CHandle(this).getOwner());
}

// Used for disabling the animator if the logic must be stopped.
void TCompPlayerMenuAnimator::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompPlayerMenuAnimator::update(float dt) {
	if (!active) return;
	PROFILE_FUNCTION("EM FSM Context::Update");
	_context.update(dt);
}


TVariable * TCompPlayerMenuAnimator::getVariable(std::string name)
{
	return _context.getVariable(name);
}

void TCompPlayerMenuAnimator::setMenuState(int state) {
	_context.setVariable(TVariable("menu", state));
}
