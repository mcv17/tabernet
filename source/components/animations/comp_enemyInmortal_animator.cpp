#include "mcv_platform.h"
#include "comp_enemyInmortal_animator.h"
#include "skeleton/comp_skeleton.h"
#include "fsm/module_fsm.h"
#include "components/controllers/comp_character_controller.h"
#include "engine.h"

DECL_OBJ_MANAGER("enemyInmortal_animator", TCompEnemyInmortalAnimator);

void TCompEnemyInmortalAnimator::load(const json& j, TEntityParseContext& ctx) {
	_fsm_path = j["src"];
}

void TCompEnemyInmortalAnimator::registerMsgs() {
	DECL_MSG(TCompEnemyInmortalAnimator, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompEnemyInmortalAnimator, TMsgLogicStatus, onLogicStatus);
}

void TCompEnemyInmortalAnimator::debugInMenu() {
	TCompBase::debugInMenu();

	bool attacking = std::get<bool>(getVariable("attacking")->_value);
	int knockback = std::get<int>(getVariable("knockback")->_value);
	float speed = std::get<float>(getVariable("speed")->_value);
	IState* currentState = _context.getCurrentState();
	
	ImGui::Text("Current anim: %s", currentState->_name.c_str());
	ImGui::Text("Time in State: %f", _context.getTimeInState());
	ImGui::Separator();
	ImGui::Text("Variables:");
	if (ImGui::Checkbox("Attacking: ", &attacking))
		_context.setVariable(TVariable("attacking", attacking));
	if (ImGui::DragInt("Knockback: ", &knockback, 1, -1, 1))
		_context.setVariable(TVariable("knockback", knockback));
	if(ImGui::DragFloat("Speed: ", &speed, 0.1f, 0.0f, 30.0f))
		_context.setVariable(TVariable("speed", speed));
	
	if (currentState->_blenders.size() > 0) {

		ImGui::Separator();
		ImGui::Text("Blendings:");
		int i = 0;
		for (auto blender : currentState->_blenders) {
			ImGui::Text("Blender %d", i++);
			std::vector<OneDimensionalBlender::Motion> motions = blender.sorted_motion_thresholds;
			ImGui::Text("	Motions :   name - threshold");
			for (auto motion : motions) {
				std::string text = _context.getSkeletonComp()->getAnimationName(motion.animation) + "  " + std::to_string(motion.threshold);
				ImGui::Text(text.c_str());
			}
		}
	}

}

void TCompEnemyInmortalAnimator::onEntityCreated(const TMsgEntityCreated & msg) {
	_fsm = EngineResources.getResource(_fsm_path)->as<CFSM>();

	/* Register animations to Cal3D */
	int animId = 0;
	for (auto state : _fsm->getStates()) {
		
		TCompSkeleton* skel = getComponent<TCompSkeleton>();
		// Register animations to Cal3D
		for (TAnimation anim : state->_animations) {
			state->animationID = skel->getAnimationID(anim.filename);
		}
	}
	_context.init(const_cast<CFSM*>(_fsm), CHandle(this).getOwner());
}

// Used for disabling the animator if the logic must be stopped.
void TCompEnemyInmortalAnimator::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompEnemyInmortalAnimator::update(float dt) {
	if (!active) return;
	PROFILE_FUNCTION("EI FSM Context::Update");
	// Set speed
	TCompCharacterController* cont = getComponent<TCompCharacterController>();
	if (!cont) return;
	_context.setVariable(TVariable("speed", cont->getCurrentMovementSpeed()));


	_context.update(dt);
}

TVariable * TCompEnemyInmortalAnimator::getVariable(std::string name)
{
	return _context.getVariable(name);
}

void TCompEnemyInmortalAnimator::setIsAttacking(bool value)
{
	TVariable var;
	TVariableValue val = value;

	var._name = _attackingVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyInmortalAnimator::setKnockback(int origin)
{
	TVariable var;
	TVariableValue val = origin;

	var._name = _knockbackVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyInmortalAnimator::setSpeed(float speed)
{
	TVariable var;
	TVariableValue val = speed;

	var._name = _speedVarName;
	var._value = val;

	_context.setVariable(var);
}


