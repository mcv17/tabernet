#include "mcv_platform.h"
#include "comp_enemyMelee_animator.h"
#include "skeleton/comp_skeleton.h"
#include "fsm/module_fsm.h"
#include "components/controllers/comp_character_controller.h"
#include "engine.h"

DECL_OBJ_MANAGER("enemyMelee_animator", TCompEnemyMeleeAnimator);

void TCompEnemyMeleeAnimator::load(const json& j, TEntityParseContext& ctx) {
	_fsm_path = j["src"];
}

void TCompEnemyMeleeAnimator::registerMsgs() {
	DECL_MSG(TCompEnemyMeleeAnimator, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompEnemyMeleeAnimator, TMsgLogicStatus, onLogicStatus);
}

void TCompEnemyMeleeAnimator::debugInMenu() {
	TCompBase::debugInMenu();

	bool attacking = std::get<bool>(getVariable("attacking")->_value);
	int knockback = std::get<int>(getVariable("knockback")->_value);
	bool taunting = std::get<bool>(getVariable("taunting")->_value);
	bool leaping = std::get<bool>(getVariable("leaping")->_value);
	float speed = std::get<float>(getVariable("speed")->_value);
	IState* currentState = _context.getCurrentState();
	
	ImGui::Text("Current anim: %s", currentState->_name.c_str());
	ImGui::Text("Time in State: %f", _context.getTimeInState());
	ImGui::Separator();
	ImGui::Text("Variables:");
	if (ImGui::Checkbox("Attacking: ", &attacking))
		_context.setVariable(TVariable("attacking", attacking));
	if (ImGui::Checkbox("Leaping: ", &leaping))
		_context.setVariable(TVariable("leaping", leaping));
	if (ImGui::Checkbox("Taunting: ", &taunting))
		_context.setVariable(TVariable("taunting", taunting));
	if (ImGui::DragInt("Knockback: ", &knockback, 1.0f, -1, 3))
		_context.setVariable(TVariable("knockback", knockback));
	if(ImGui::DragFloat("Speed: ", &speed, 0.1f, 0.0f, 30.0f))
		_context.setVariable(TVariable("speed", speed));
	
	if (currentState->_blenders.size() > 0) {

		ImGui::Separator();
		ImGui::Text("Blendings:");
		int i = 0;
		for (auto blender : currentState->_blenders) {
			ImGui::Text("Blender %d", i++);
			std::vector<OneDimensionalBlender::Motion> motions = blender.sorted_motion_thresholds;
			ImGui::Text("	Motions :   name - threshold");
			for (auto motion : motions) {
				std::string text = _context.getSkeletonComp()->getAnimationName(motion.animation) + "  " + std::to_string(motion.threshold);
				ImGui::Text(text.c_str());
			}
		}
	}

}

void TCompEnemyMeleeAnimator::onEntityCreated(const TMsgEntityCreated & msg) {
	_fsm = EngineResources.getResource(_fsm_path)->as<CFSM>();

	/* Register animations to Cal3D */
	int animId = 0;
	for (auto state : _fsm->getStates()) {
		
		TCompSkeleton* skel = getComponent<TCompSkeleton>();
		// Register animations to Cal3D
		for (TAnimation anim : state->_animations) {
			state->animationID = skel->getAnimationID(anim.filename);
		}
	}
	_context.init(const_cast<CFSM*>(_fsm), CHandle(this).getOwner());
}

// Used for disabling the animator if the logic must be stopped.
void TCompEnemyMeleeAnimator::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}
	
void TCompEnemyMeleeAnimator::update(float dt) {
	if (!active) return;
	PROFILE_FUNCTION("EM FSM Context::Update");
	// Set speed
	TCompCharacterController* cont = getComponent<TCompCharacterController>();
	if (!cont) return;
	_context.setVariable(TVariable("speed", cont->getCurrentMovementSpeed()));


	_context.update(dt);
}


TVariable * TCompEnemyMeleeAnimator::getVariable(std::string name)
{
	return _context.getVariable(name);
}

void TCompEnemyMeleeAnimator::setIsAttacking(bool value)
{
	TVariable var;
	TVariableValue val = value;

	var._name = _attackingVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyMeleeAnimator::setSpeed(float speed)
{
	TVariable var;
	TVariableValue val = speed;

	var._name = _speedVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyMeleeAnimator::setIsLeaping(bool isLeaping)
{
	TVariable var;
	TVariableValue val = isLeaping;

	var._name = _leapVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyMeleeAnimator::setIsTaunting(bool isTaunting)
{
	TVariable var;
	TVariableValue val = isTaunting;

	var._name = _shoutVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyMeleeAnimator::setKnockback(int knockbackState)
{
	TVariable var;
	TVariableValue val = knockbackState;

	var._name = _knockbackVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyMeleeAnimator::setIsWarIdle(bool isWarIdle)
{
	TVariable var;
	TVariableValue val = isWarIdle;

	var._name = _isWarIdleVarName;
	var._value = val;

	_context.setVariable(var);
}

