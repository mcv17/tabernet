#include "mcv_platform.h"
#include "comp_player_animator.h"
#include "skeleton/comp_skeleton.h"
#include "fsm/module_fsm.h"
#include "engine.h"

DECL_OBJ_MANAGER("player_animator", TCompPlayerAnimator);

void TCompPlayerAnimator::load(const json& j, TEntityParseContext& ctx) {
	_fsm_fullbody_path = j["fullBodyPath"];
	_fsm_lowerbody_path = j["lowerBodyPath"];
	_fsm_upperbody_path = j["upperBodyPath"];
}

void TCompPlayerAnimator::registerMsgs() {
	DECL_MSG(TCompPlayerAnimator, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompPlayerAnimator, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompPlayerAnimator, TMsgHitDamage, onHitDamage);
	DECL_MSG(TCompPlayerAnimator, TMsgFullRestore, onResurrectionMsg);
}

void TCompPlayerAnimator::debugInMenu() {
	TCompBase::debugInMenu();

	for (int i = 0; i < 3; i++) {

		CFSMContext* context = &_context[i];
		CFSM* fsm = const_cast<CFSM*>(_fsm[i]);

		IState* currentState = context->getCurrentState();
		ImGui::Separator();
		ImGui::PushID(i);
		if (ImGui::CollapsingHeader(fsm->getName().c_str())) {
			ImGui::Text("Current anim: %s", currentState->_name.c_str());
			ImGui::Text("Time in State: %f", context->getTimeInState());
			ImGui::Separator();
			ImGui::Text("Variables:");
			// Get the variables for every fsm
			for (TVariable var : fsm->getVariables()) {
				// Get value from blackboard, set it up if it was not initialized
				TVariable v = _blackboard[var._name];
				TVariable* ctxVar = context->getVariable(var._name);
			
				ctxVar->_value = v._value;
				std::string text = v._name + ": ";
			
				// Check types and make the custom ImGui section 
				if (v.getType() == "boolean") {
					bool value = std::get<bool>(v._value);
					if (ImGui::Checkbox(text.c_str(), &value)) {
						v._value = value;
						_blackboard[var._name] = v;
						context->setVariable(v);
					}
				}
				else if (v.getType() == "int") {
					int value = std::get<int>(v._value);

					if (ImGui::DragInt(text.c_str(), &value, 1, -1, 10)) {
						v._value = value;
						_blackboard[var._name] = v;
						context->setVariable(v);
					}
				}
				else if (v.getType() == "float") {
					float start = 0.0;
					float value = std::get<float>(v._value);
					if (v._name == "aimAngle") start = -90.0f;


					if (ImGui::DragFloat(text.c_str(), &value, 0.1f, start, 90.0f)) {
						v._value = value;
						_blackboard[var._name] = v;
						context->setVariable(v);
					}
				}
			}
			
			if (currentState->_blenders.size() > 0) {

				ImGui::Separator();
				ImGui::Text("Blendings:");
				int i = 0;
				for (auto blender : currentState->_blenders) {
					ImGui::Text("Blender %d", i++);
					std::vector<OneDimensionalBlender::Motion> motions = blender.sorted_motion_thresholds;
					ImGui::Text("	Motions :   name - threshold");
					for (auto motion : motions) {
						if (motion.animation == -1) continue;
						std::string animName = context->getSkeletonComp()->getAnimationName(motion.animation);
						std::string text = animName + "  " + std::to_string(motion.threshold);
						//std::string text = context->getSkeletonComp()->getAnimationName(motion.animation) + "  " + std::to_string(motion.threshold);
						ImGui::Text(text.c_str());
					}
				}
			}
		}
		ImGui::PopID();
	}

}

void TCompPlayerAnimator::onEntityCreated(const TMsgEntityCreated & msg) {
	_fsm[PlayerFSM::FULLBODY] = EngineResources.getResource(_fsm_fullbody_path)->as<CFSM>();
	_fsm[PlayerFSM::LOWER_BODY] = EngineResources.getResource(_fsm_lowerbody_path)->as<CFSM>();
	_fsm[PlayerFSM::UPPER_BODDY] = EngineResources.getResource(_fsm_upperbody_path)->as<CFSM>();

	/* Register animations to Cal3D && variables to the blackboard */
	int animId = 0;
	for (int i = 0; i < 3; i++) {
		// Current fsm
		CFSM* fsm = const_cast<CFSM*>(_fsm[i]);

		// Blackboard
		for (TVariable var : fsm->getVariables()) {
			_blackboard[var._name] = TVariable(var._name, var._value);
		}

		// Animations
		for (auto state : _fsm[i]->getStates()) {

			TCompSkeleton* skel = getComponent<TCompSkeleton>();
			// Register animations to Cal3D
			for (TAnimation anim : state->_animations) {
				state->animationID = skel->getAnimationID(anim.filename);
			}
		}
		_context[i].init(const_cast<CFSM*>(_fsm[i]), CHandle(this).getOwner());
	}
}

// Used for disabling the animator if the logic must be stopped.
void TCompPlayerAnimator::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompPlayerAnimator::onHitDamage(const TMsgHitDamage & msg)
{
	//Handle this 'hack' so if player is playing some animations, can't start a knockback one
	int isReloadingNormal = std::get<int>(getVariable("isReloadingNormal", PlayerFSM::UPPER_BODDY)->_value);
	if (isReloadingNormal) return;

	int isReloadingFast = std::get<int>(getVariable("isReloadingFast", PlayerFSM::UPPER_BODDY)->_value);
	if (isReloadingFast) return;

	int isReloadingFail = std::get<int>(getVariable("isReloadingFail", PlayerFSM::UPPER_BODDY)->_value);
	if (isReloadingFail) return;

	int isChangingWeapon = std::get<int>(getVariable("isChangingWeapon", PlayerFSM::UPPER_BODDY)->_value);
	if (isChangingWeapon) return;

	bool scytheAttack = std::get<bool>(getVariable("scytheAttack", PlayerFSM::FULLBODY)->_value);
	if (scytheAttack) return;
	
	bool interacting = std::get<bool>(getVariable("interacting", PlayerFSM::FULLBODY)->_value);
	if (interacting) return;

	bool dead = std::get<bool>(getVariable("dead", PlayerFSM::FULLBODY)->_value);
	if (dead) return;

	int rand = RNG.i(101);
	if (rand <= 20) {
		setVariable("knockback", true);
		setVariable("stop_fullbody", false);
	}
}

void TCompPlayerAnimator::onResurrectionMsg(const TMsgFullRestore & msg) {
	setVariable("dead", false);
}

void TCompPlayerAnimator::update(float dt) {
	if (!active) return;
	PROFILE_FUNCTION("EM FSM Context::Update");
	for (int i = 0; i < 3; i++) {
		// Update variable data
		CFSM* fsm = const_cast<CFSM*>(_fsm[i]);
		for (TVariable var : fsm->getVariables()) {
			TVariable v = _blackboard[var._name];
			_context[i].setVariable(v);
		}

		_context[i].update(dt);
	}
	
	updateWeaponReadyToShoot(dt);
}


TVariable * TCompPlayerAnimator::getVariable(std::string name, PlayerFSM fsmId)
{
	return _context[fsmId].getVariable(name);
}

void TCompPlayerAnimator::setVariable(std::string name, bool value)
{
	_blackboard[name]._value = value;
}

void TCompPlayerAnimator::setVariable(std::string name, float value)
{
	_blackboard[name]._value = value;
}

void TCompPlayerAnimator::setVariable(std::string name, int value)
{
	_blackboard[name]._value = value;
}

void TCompPlayerAnimator::setReload(int weapon)
{
	_blackboard[_reloadNormalVarName]._value = weapon;
	_currentWeaponReloading = weapon;
}

void TCompPlayerAnimator::clearReload()
{
	_currentWeaponReloading = WeaponAnim::WEAPON_NONE;
	_blackboard[_reloadNormalVarName]._value = WeaponAnim::WEAPON_NONE;
	_blackboard[_reloadFastVarName]._value = WeaponAnim::WEAPON_NONE;
	_blackboard[_reloadFailVarName]._value = WeaponAnim::WEAPON_NONE;
}

float TCompPlayerAnimator::getReloadElapsedTime()
{
	TCompSkeleton* playerSkeleton = getComponent<TCompSkeleton>();
	assert(playerSkeleton);
	int animID = playerSkeleton->getAnimationID(_reloadNormalAnimNames[_currentWeaponReloading]);
	return playerSkeleton->getAnimationElapsedTime(animID);
}

void TCompPlayerAnimator::setReloadFast(float time)
{
	TCompSkeleton* playerSkeleton = getComponent<TCompSkeleton>();
	assert(playerSkeleton);
	AnimationID animID = playerSkeleton->getAnimationID(_reloadNormalAnimNames[_currentWeaponReloading]);
	playerSkeleton->RemoveAnimation(animID, 0.3f); //To update the weight of the normal reload to 0 with a delayOut of 0.3 seconds

	_blackboard[_reloadFastVarName]._value = _currentWeaponReloading;
	_blackboard["currentReloadElapsedTime"]._value = time;
}

void TCompPlayerAnimator::setReloadFail(float time)
{
	TCompSkeleton* playerSkeleton = getComponent<TCompSkeleton>();
	assert(playerSkeleton);
	AnimationID animID = playerSkeleton->getAnimationID(_reloadNormalAnimNames[_currentWeaponReloading]);
	playerSkeleton->RemoveAnimation(animID, 0.3f); //To update the weight of the normal reload to 0 with a delayOut of 0.3 seconds

	_blackboard[_reloadFailVarName]._value = _currentWeaponReloading;
	_blackboard["currentReloadElapsedTime"]._value = time;
}

void TCompPlayerAnimator::setChangeWeapon(int weapon)
{
	_blackboard[_changeWeaponVarName]._value = weapon;
}

void TCompPlayerAnimator::setHipShootingWeapon(int weapon)
{
	_blackboard[_hipShootingVarName]._value = weapon;
	_currentWeaponShooting = weapon;
}

void TCompPlayerAnimator::setAimShootingWeapon(int weapon)
{
	_blackboard[_aimShootingVarName]._value = weapon;
	_currentWeaponShooting = weapon;
}

void TCompPlayerAnimator::clearShooting()
{
	_blackboard[_hipShootingVarName]._value = WEAPON_NONE;
	_blackboard[_aimShootingVarName]._value = WEAPON_NONE;
	// We remove the smg animations because they're cycles, just in case something doesn't work quite well
	TCompSkeleton* playerSkeleton = getComponent<TCompSkeleton>();
	assert(playerSkeleton);
	playerSkeleton->RemoveAnimation(_aimShootingAnimNames[_currentWeaponShooting], 0.3f);
}

void TCompPlayerAnimator::updateWeaponReadyToShoot(float dt)
{
	//Get the fsm, and check for the state, if the state is ready to shoot, increment the _timeIndicator var, if not, decrement it
	CFSMContext* context = &_context[PlayerFSM::UPPER_BODDY];
	bool foundStateActive = false;
	for (std::string state : _weaponUpStatesVarNames) {
		if (context->getCurrentState()->_name == state) {
			foundStateActive = true;
			break;
		}
	}
	if (foundStateActive)
		_timeIndicator = std::min((_timeIndicator + dt), 0.3f);
	else
		_timeIndicator = std::max((_timeIndicator - dt), 0.0f);
}

//void TCompPlayerAnimator::setIsAttacking(bool value, PlayerFSM fsmId)
//{
//	TVariable var;
//	TVariableValue val = value;
//
//	var._name = _attackingVarName;
//	var._value = val;
//
//	_context[fsmId].setVariable(var);
//}
//
//void TCompPlayerAnimator::setSpeed(float speed)
//{
//	TVariable var;
//	TVariableValue val = speed;
//
//	var._name = _speedVarName;
//	var._value = val;
//
//	_context.setVariable(var);
//}


