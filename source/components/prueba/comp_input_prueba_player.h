#pragma once

#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components\states\state_machine_controller.h"
#include "components/controllers/comp_player_character_controller.h"

class TCompTransform;

class TCompInputPruebaPlayer : public StateMachineController{

	bool first = false;
	DECL_SIBLING_ACCESS();

	enum class PowerState {NONE, VORTEX, TELEPORT};
	PowerState powerState;
	
	std::string playerCamera;
	CHandle cameraTransformHandle;
	CHandle cameraControllerHandle;
	CHandle cameraHandle; // We use this handle for debug. If the camara is not active, the player won't move.

	TCompPlayerCharacterController* playerController;
	
	// In degrees, threshold for considering when the player is facing the camera.
	// Once achieved, will start firing.
	float facingCameraThreshold = 10.0f;
	
	// This simulates a sprinting animation that must be played. A small delay in input.
	float timeToStartSprintingAgain = 0.25f;
	float currentTimeToStartSprintingAgain = timeToStartSprintingAgain;
	float timeBeforeSprintingEnds = 0.1f;
	float currentTimeBeforeSprintingEnds = timeBeforeSprintingEnds;

	float timeToMoveFreely = 5.0f;
	float currentTimeToMoveFreely = timeToMoveFreely;

	void OnNormalGrounded(float dt);
public:
	void Init() override;

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void update(float dt);	
	static void registerMsgs();

private:
	bool isFacingCamera(TCompTransform * playerTransform, TCompTransform * cameraTransform, float threshold);
};