#include "mcv_platform.h"
#include "comp_input_prueba_player.h"
#include "entity/entity_parser.h"
#include "components/common/comp_transform.h"
#include "components/camera/comp_player_camera_controller.h"
#include "components/camera/comp_camera.h"
#include "components/player/comp_holy_water.h"
#include "components/weapons/comp_weapons_manager.h"
#include "components/powers/comp_vortex_player.h"
#include "components/powers/comp_teleport_player.h"
#include "components/powers/comp_healing.h"

DECL_OBJ_MANAGER("input_prueba_player", TCompInputPruebaPlayer);

void TCompInputPruebaPlayer::load(const json & j, TEntityParseContext& ctx) {
	playerCamera = j.value("cameraFollowing", playerCamera);
	facingCameraThreshold = DEG2RAD(j.value("facingCamerathreshold", facingCameraThreshold));
	Init();
}

void TCompInputPruebaPlayer::debugInMenu() {
	ImGui::Spacing();
	ImGui::Text("Current state: ");
	ImGui::Text(state.c_str());
	ImGui::Spacing();

	float thresholdCamShoot = RAD2DEG(facingCameraThreshold);
	ImGui::DragFloat("Shoot threshold", &thresholdCamShoot, 0.001f, 0.f, 360.f);
	if (thresholdCamShoot != RAD2DEG(facingCameraThreshold))
		facingCameraThreshold = DEG2RAD(thresholdCamShoot);
}

void TCompInputPruebaPlayer::update(float dt)
{
	//playerController = getComponent<TCompPlayerCharacterController>();
	//if (playerController == nullptr)
	//	return;

	// checkPlayerCamera
	if (!cameraTransformHandle.isValid()) {
		CEntity * ent = getEntityByName(playerCamera.c_str());
		cameraTransformHandle = ent->getComponent<TCompTransform>();
		cameraControllerHandle = ent->getComponent<TCompPlayerCameraController>();
		cameraHandle = ent->getComponent<TCompCamera>();
	}

	// TEMP_ADDED - updating the change between states
	assert(!state.empty());
	assert(statemap.find(state) != statemap.end());
	// this is a trusted jump as we've tested for coherence in ChangeState
	(this->*statemap[state])(dt);
}

void TCompInputPruebaPlayer::registerMsgs() {
}

void TCompInputPruebaPlayer::Init() {
	AddState("Aim", (statehandler)&TCompInputPruebaPlayer::OnNormalGrounded);

	ChangeState("Aim");
	powerState = PowerState::NONE;
}

bool TCompInputPruebaPlayer::isFacingCamera(TCompTransform * playerTransform, TCompTransform * cameraTransform, float threshold) {
	float yawObj = playerTransform->getDeltaYawToAimTo(playerTransform->getPosition() + cameraTransform->getFront());
	if (abs(yawObj) <= threshold)
		return true;
	return false;
}

void TCompInputPruebaPlayer::OnNormalGrounded(float dt) {

	if (!first) {
		CEntity * ent = getEntityByName(playerCamera.c_str());
		TCompPlayerCameraController * comp = ent->getComponent<TCompPlayerCameraController>();
		//comp->ChangeState("Aim");
		first = true;
	}

	TCompTransform * playerTransform = getComponent<TCompTransform>();
	TCompPlayerCameraController * playerCamController = cameraControllerHandle; // Get the camera that follows the player.
	TCompTransform * cameraTransform = cameraTransformHandle; // Get the camera's transform.
	TCompCamera * camera = cameraHandle;
	TCompWeaponManager * weaponManager = getComponent<TCompWeaponManager>(); // Get the weapon manager.
	TCompWeaponsController* wp_contr = weaponManager->getCurrentWeapon(); // Also get the current weapon.
	TCompVortexPlayer* vortexPlayer = getComponent<TCompVortexPlayer>();
	TCompTeleportPlayer * teleportPlayer = getComponent<TCompTeleportPlayer>();
	if (!playerTransform || !cameraTransform || !camera->isActive()) return;

	VEC3 playerPosition = playerTransform->getPosition();
	VEC3 playerFront = playerTransform->getFront();

	// Movement
	bool moving = false, wantToSprint = false;
	VEC3 movementDelta;
	if (EngineInput["forward"]) {
		// Only sprint if moving forward.
		if (EngineInput["sprint"])
			wantToSprint = true;

		movementDelta += VEC3::Forward;
		moving = true;
	}
	if (EngineInput["backwards"]) {
		movementDelta -= VEC3::Forward;
		moving = true;
	}
	if (EngineInput["left"]) {
		movementDelta += VEC3::Left;
		moving = true;
	}
	if (EngineInput["right"]) {
		movementDelta -= VEC3::Left;
		moving = true;
	}

	movementDelta.Normalize();

	if (moving)
		playerController->rotateAndMoveFreely(cameraTransform, movementDelta * playerController->getPlayerSpeedNormal(), playerController->getRotationSpeedNormal(), dt);

	bool facingCamera = isFacingCamera(playerTransform, cameraTransform, facingCameraThreshold);

	/* Weapons */
	if (EngineInput["shoot"] && powerState == PowerState::NONE) {
		// If we are not already moving in the direction of the camera. i.e: We are idle.
		if (moving == false) {
			//If not facing the camera, rotate.
				float rotationTime = 0.2f; // Time for making half a turn.
				float rotationSpeed = DEG2RAD(180.0f) / rotationTime;

				TCompTransform* transform = getComponent<TCompTransform>();
				if (!transform) return;
				float yaw, pitch, nYaw, nPitch;
				transform->getAngles(yaw, pitch);

				cameraTransform->getAngles(nYaw, nPitch);
				float yawDelta = nYaw - yaw;

				float rotation = rotationSpeed * dt;
				if (abs(yawDelta) < rotation) {
					rotation = yawDelta;
					Utils::dbg("yawdelta\n");
				}
				else if (yawDelta < 0.0f) {
					rotation = -rotation;
					Utils::dbg("rotation\n");
				}
				else
					Utils::dbg("rotation\n");

				transform->setAngles(rotation + yaw, pitch);
		}
	}
}