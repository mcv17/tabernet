#pragma once

#include <vector>
#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

/* This postfx component applies a basic motion blur post processing effect based on calculating the
difference between the current image and the previous frame viewprojection. The algorithm behind it can be
found at:
// http://john-chapman-graphics.blogspot.com/2013/01/what-is-motion-blur-motion-pictures-are.html
// Also interesting:
// https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch27.html */

class TCompPostFXMotionBlur : public TCompBase {
private:
	// The constant buffer with the variables for the blur effect.
	CCteBuffer<TCteMotionBlur> cte_motion_blur;
	// The render to texture where the image with the applied blur will be rendered.
	CRenderToTexture *	rt = nullptr;
	// The motion blur technique.
	const CTechnique *	tech = nullptr;

public:
	// Constructor and destructor that initiate and destroy the constant buffer.
	TCompPostFXMotionBlur();
	~TCompPostFXMotionBlur();
	
	// Load the parameters for the motion blur from a json file.
	void load(const json& j, TEntityParseContext& ctx);
	// Shows in ImGui the parameters for the effect.
	void debugInMenu();
	// Applies the effect to the render to texture given as parameter.
	CRenderToTexture * apply(CRenderToTexture* textureToApplyPPE);

private:
	// If the width or height is not equal, it will resize the game.
	bool createOrResizeRT(int width, int height);
	// Stores the camera view matrix, is called at the end of the method, therefore
	// storing the last frame view matrix.
	void updateLastFrameViewMatrix();
};