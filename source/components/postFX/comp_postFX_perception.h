#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

class CTecnique;
class CRenderToTexture;
struct TMsgPerceptionStatusChanged;

enum PostFXPerceptionCategory {
	MASK_NONE_CATEGORY,
	MASK_PLAYER_CATEGORY,
	MASK_OBJECTS_CATEGORY,
	MASK_ENEMY_CATEGORY
};

/* This component applies a post processing effect for
simulating a perception effect around the player's screen. */
class TCompPostFXPerception : public TCompBase {
	DECL_SIBLING_ACCESS();

	bool deactivate = false;

	/* Texture to render the effect to. */
	CRenderToTexture * rt;
	CRenderToTexture * rt2;

	const CTexture * lut1 = nullptr; // Pointer to Look Up Table for color grading.
	
	const CTexture * gradient_wave = nullptr; // Used for the corruption wave. Controls color and strength.
	const CTexture * noise = nullptr; // Used for the corruption wave.

	// Perception first and second pass penalty technique.
	const CTechnique * perception_penalty_techn;
	const CTechnique * perception_scenario_outline_techn;
	const CTechnique * perception_category_outline_techn;
	const CTechnique * perception_category_outline_techn_add;
	const CTechnique * perception_second_pass_penalty_techn;

	CHandle playerH;
	CHandle playerTransformH;

	float base_ambient = 0.0f;

	// Perception tint for non perceptive objects.
	Vector4 perception_tint = Vector4(0.12157f, 0.12157f, 0.12157f, 1.0f);

	// Fade variables.
	bool fade_in = true;
	float current_time = 0.0f;
	float fade_in_time = 0.25f;
	float fade_out_time = 0.25f;
	float fadeValue = 0.0f; // 0.0f no fade, 1.0f fade.

	// Outline variables.
	Vector3 outline_scenario_color = Vector3(1.0f, 0.937255f, 0.298f);
	int outline_scenario_width = 1;
	float outline_scenario_edge_threshold = 0.2;
	int outline_scenario_noise_extra_width = 3;
	float outline_scenario_noise_grainy_controller = 10.8;

	// Outline category base values. Fetched from categories below.
	Vector3 color_outline;
	int outline_width;
	int outline_category;
	int outline_noise_extra_width;
	float outline_noise_grainy_controller;

	// Outline perception player.
	Vector3 outline_player_color = Vector3(0.20f, 0.5843f, 0.82f);
	int outline_player_width = 1;
	int outline_player_noise_extra_width = 3;
	float outline_player_noise_grainy_controller = 0.150;

	// Outline perception enemies.
	Vector3 outline_enemies_color = Vector3(0.91f, 0.0f, 0.0f);
	int outline_enemies_width = 1;
	int outline_enemies_noise_extra_width = 3;
	float outline_enemies_noise_grainy_controller = 0.07;

	// Outline perception objects.
	Vector3 outline_objects_color = Vector3(0.0, 0.91f, 0.0f);
	int outline_objects_width = 1;
	int outline_objects_noise_extra_width = 3;
	float outline_objects_noise_grainy_controller = 0.15;

	// Perception sonar.
	float currentSonarRadius = 0.0f; // Current distance of the sonar.
	const float baseSonarSpeedPerSecond = 1.0f; // Base sonar speed.
	float sonarSpeedPerSecond = baseSonarSpeedPerSecond;
	float sonarAceleration = 16.0f; // 1 unit per second added to the velocity.
	float sonarRadiusOffset = 4.f; // Indicates the width of the wave area.
	float sonarWaveNoiseAmplitude = 3.6f; // The bigger the amplitude the more intense the sonar wave peaks and valleys are.
	float nonExploredAreasColorIntensity = 0.015f; // How dark will be displayed all areas where the sonar still has not gone too.


	CCteBuffer<TCtsPerception> cts_perception = CCteBuffer<TCtsPerception>(CTE_BUFFER_SLOT_PERCEPTION);
	CCteBuffer<TCtsPerceptionScenarioOutline> cts_perception_scenario_outline = CCteBuffer<TCtsPerceptionScenarioOutline>(CTE_BUFFER_SLOT_PERCEPTION);

public:
	TCompPostFXPerception();
	~TCompPostFXPerception();

	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();

	CRenderToTexture * applyColoring(CRenderToTexture * textureToApplyPPE);
	CRenderToTexture * applyOutlineScenario(CRenderToTexture * textureToApplyPPE);
	CRenderToTexture * applyOutlineCategory(CRenderToTexture * textureToApplyPPE, CRenderToTexture * rt_acc_depth, eRenderCategory perceptionCategory);
	CRenderToTexture * apply(CRenderToTexture * textureToApplyPPE);

	// Get the outline color, width, etcetera for the given category.
	void fetchOutlineValuesForCategory(eRenderCategory perceptionCategory);
private:
	bool createOrResizeRT(int width, int height);
	void OnPerceptionChanged(const TMsgPerceptionStatusChanged & msg);

	void calcFade();

	bool fetchPlayer();
	bool isFadingIn();
	Vector3 getPlayerPosition();
};