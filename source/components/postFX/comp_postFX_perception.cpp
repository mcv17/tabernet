#include "mcv_platform.h"
#include "comp_postFX_perception.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "render/module_render.h"

#include "components/powers/comp_perception.h"

DECL_OBJ_MANAGER("postfx_perception", TCompPostFXPerception);

TCompPostFXPerception::TCompPostFXPerception(){}

TCompPostFXPerception::~TCompPostFXPerception() {
	if (rt) rt->destroy();
	cts_perception.destroy();
}

void TCompPostFXPerception::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);
	
	// Perception technique.
	
	// Technique for the coloring phase.
	std::string perception_penalty_technname = j.value("perception_penalty_techn", "perception_screen.tech");
	// Technique for the edge detection of the scenario.
	std::string perception_scenario_outline_technname = j.value("perception_scenario_outline", "perception_scenario_outline.tech");
	// Technique for the outlining of a category.
	std::string perception_category_outline_techn_technname = j.value("perception_category_outline_techn", "perception_objects_outline.tech");
	// Technique for the applying the outline generated over a category.
	std::string perception_category_outline_techn_techname_additive = j.value("perception_category_outline_techn_additive", "perception_objects_outline_additive.tech");
	// Technique for the wave.
	std::string perception_second_pass_penalty_technname = j.value("perception_penalty_second_techn", "perception_screen_second.tech");

	perception_tint = loadVector4(j, "perception_tint");
	perception_penalty_techn = EngineResources.getResource(perception_penalty_technname)->as<CTechnique>();
	perception_scenario_outline_techn = EngineResources.getResource(perception_scenario_outline_technname)->as<CTechnique>();
	perception_category_outline_techn = EngineResources.getResource(perception_category_outline_techn_technname)->as<CTechnique>();
	perception_category_outline_techn_add = EngineResources.getResource(perception_category_outline_techn_techname_additive)->as<CTechnique>();
	perception_second_pass_penalty_techn = EngineResources.getResource(perception_second_pass_penalty_technname)->as<CTechnique>();

	// Look up table to colorize all non perceptive objects in the scene.
	std::string lut_technname = j.value("lut_name", "data/textures/lut/lutPerception.dds");
	lut1 = EngineResources.getResource(lut_technname)->as<CTexture>();

	// This gradient texture controls the color of the wave and provides an alpha value used for controlling the fading of the wave.
	std::string corruption_grad_techname = j.value("corruption_gradient_name", "data/textures/corruption_grad/corruption_grad.dds");
	gradient_wave = EngineResources.getResource(corruption_grad_techname)->as<CTexture>();
	
	// A noise texture used for controlling the irregularity of the wave area to give the wave a more irregular shape and avoid it
	// being a perfect circle.
	std::string noise_technname = j.value("noise_name", "data/textures/noise_cloud2.dds");
	noise = EngineResources.getResource(noise_technname)->as<CTexture>();

	// Create the constants uploaded to the shader. (Some of them could probably be hardcoded into the shader but i found ok,
	// to leave them in the constant so they can be modified lively from the ImGui inside if we want to tweak things.).
	if (!cts_perception.create("Perception")) Utils::fatal("Failed while creating the cts_perception constants.");
	if (!cts_perception_scenario_outline.create("Perception Scenario Outline")) Utils::fatal("Failed while creating the cts_perception_scenario_outline constants.");

	// Create the rt to render the perception effect in.
	if (!createOrResizeRT(Render.getWidth(), Render.getHeight())) Utils::dbg("Perception rt couldn't be created\n");
}

void TCompPostFXPerception::registerMsgs() {
	DECL_MSG(TCompPostFXPerception, TMsgPerceptionStatusChanged, OnPerceptionChanged);
}

void TCompPostFXPerception::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	ImGui::Text("Outline Scenario");
	ImGui::Spacing(0, 5);

	ImGui::ColorEdit3("Outline Scenario Perception HDR Tint", &outline_scenario_color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
	ImGui::DragInt("Outline Scenario Perception Edge Width: ", &outline_scenario_width, 1.0f, 0.0f, 100.f);
	ImGui::DragFloat("Outline Scenario Perception Edge Threshold: ", &outline_scenario_edge_threshold, 0.01f, 0.0f, 100.f);
	ImGui::DragInt("Outline Scenario Perception Noise Edge Width: ", &outline_scenario_noise_extra_width, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Outline Scenario Perception Noise Edge Grainyness: ", &outline_scenario_noise_grainy_controller, 0.01f, 0.0f, 100.f);

	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	ImGui::Text("Outline Player");
	ImGui::Spacing(0, 5);

	ImGui::ColorEdit3("Outline Player Perception HDR Tint", &outline_player_color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
	ImGui::DragInt("Outline Player Perception Edge Width: ", &outline_player_width, 1.0f, 0.0f, 100.f);
	ImGui::DragInt("Outline Player Perception Noise Edge Width: ", &outline_player_noise_extra_width, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Outline Player Perception Noise Edge Grainyness: ", &outline_player_noise_grainy_controller, 0.01f, 0.0f, 100.f);

	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	ImGui::Text("Outline Enemies");
	ImGui::Spacing(0, 5);

	ImGui::ColorEdit3("Outline Enemies Perception HDR Tint", &outline_enemies_color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
	ImGui::DragInt("Outline Enemies Perception Edge Width: ", &outline_enemies_width, 1.0f, 0.0f, 100.f);
	ImGui::DragInt("Outline Enemies Perception Noise Edge Width: ", &outline_enemies_noise_extra_width, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Outline Enemies Perception Noise Edge Grainyness: ", &outline_enemies_noise_grainy_controller, 0.01f, 0.0f, 100.f);


	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	ImGui::Text("Outline Objects");
	ImGui::Spacing(0, 5);

	ImGui::ColorEdit3("Outline Objects Perception HDR Tint", &outline_objects_color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
	ImGui::DragInt("Outline Objects Perception Edge Width: ", &outline_objects_width, 1.0f, 0.0f, 100.f);
	ImGui::DragInt("Outline Objects Perception Noise Edge Width: ", &outline_objects_noise_extra_width, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Outline Objects Perception Noise Edge Grainyness: ", &outline_objects_noise_grainy_controller, 0.01f, 0.0f, 100.f);

	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	ImGui::Text("Perception Wave and Tint");
	ImGui::Spacing(0, 5);

	ImGui::ColorEdit3("HDR Tint", &perception_tint.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
	ImGui::DragFloat("Sonar radius speed: ", &sonarSpeedPerSecond, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Sonar radius offset: ", &sonarRadiusOffset, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Sonar wave speed: ", &sonarSpeedPerSecond, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Sonar radius distorsion amplitude: ", &sonarWaveNoiseAmplitude, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Non explored areas color intensity: ", &nonExploredAreasColorIntensity, 0.001f, 0.0f, 5.f);
}

bool TCompPostFXPerception::createOrResizeRT(int width, int height) {
	if (!rt) rt = new CRenderToTexture();
	if (!rt2) rt2 = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {
		char rt_perception[64];
		sprintf(rt_perception, "Perception_%08x", CHandle(this).asUnsigned());
		bool correct = rt->create(rt_perception, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT);
		sprintf(rt_perception, "Perception_%08x", CHandle(this).asUnsigned()+1);
		correct |= rt2->create(rt_perception, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT);
		return correct;
	}
	return true;
}

CRenderToTexture * TCompPostFXPerception::applyColoring(CRenderToTexture * textureToApplyPPE) {
	if (!active || (deactivate && fade_in) || (!deactivate && fade_in)) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	// Do Perception coloring.
	{
		PROFILE_FUNCTION("PostFx - Perception First Pass");
		CGpuScope gpu_scope("Perception First Pass");

		// Set constants.
		{
			PROFILE_FUNCTION("Perception First Pass - Constants");
			cts_perception.PerceptionTint = perception_tint;
			cts_perception.PerceptionFadeColor = perception_tint;
			cts_perception.PerceptionFadeWeight = 0.0f;
			cts_perception.activate();
			cts_perception.updateGPU();
			lut1->activate(TS_LUT_COLOR_GRADING);

			ctes_shared.GlobalAmbientBoost = EngineRender.GetAmbientBoost();
			ctes_shared.GlobalLUTAmount = 1.0f; // Return ambient to default.
			ctes_shared.updateGPU();
		}

		{
			PROFILE_FUNCTION("Perception First Pass - Rendering");
			rt->activateRT();
			drawFullScreenQuad(perception_penalty_techn, textureToApplyPPE);
		}
	}

	return rt;
}

CRenderToTexture * TCompPostFXPerception::applyOutlineScenario(CRenderToTexture * textureToApplyPPE){
	if (!active || (deactivate && fade_in) || (!deactivate && fade_in)) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	// Do Perception Scenario Outlining.
	{
		PROFILE_FUNCTION("PostFx - Perception Outline Scenario");
		CGpuScope gpu_scope("Perception Outline Scenario");

		// Set constants.
		{
			PROFILE_FUNCTION("Perception Outline Scenario - Constants");
			// To check when to start painting the outlines.
			cts_perception_scenario_outline.PerceptionScenarioOutlinePositionPlayer = getPlayerPosition();
			cts_perception_scenario_outline.PerceptionScenarioOutlineWaveRadius = currentSonarRadius;

			// Outline variables.
			cts_perception_scenario_outline.PerceptionScenarioOutlineEdgeColor = outline_scenario_color;
			cts_perception_scenario_outline.PerceptionScenarioOutlineEdgeWidth = outline_scenario_width;
			cts_perception_scenario_outline.PerceptionScenarioOutlineEdgeThreshold = outline_scenario_edge_threshold;

			// Noise edge variables.
			cts_perception_scenario_outline.PerceptionScenarioOutlineNoiseExtraWidth = outline_scenario_noise_extra_width;
			cts_perception_scenario_outline.PerceptionScenarioOutlineNoiseGrainyness = outline_scenario_noise_grainy_controller;

			cts_perception_scenario_outline.activate();
			cts_perception_scenario_outline.updateGPU();
		}

		{
			PROFILE_FUNCTION("Perception Outline Scenario - Rendering");
			rt->activateRT();
			drawFullScreenQuad(perception_scenario_outline_techn, textureToApplyPPE);
		}
	}

	return rt;
}

CRenderToTexture * TCompPostFXPerception::applyOutlineCategory(CRenderToTexture * textureToApplyPPE, CRenderToTexture * rt_acc_depth, eRenderCategory perceptionCategory){
	if (!active || (deactivate && fade_in) || (!deactivate && fade_in)) return nullptr;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	// Get the outline values for the given category.
	fetchOutlineValuesForCategory(perceptionCategory);

	// Do Perception Scenario Outlining.
	{
		PROFILE_FUNCTION("PostFx - Perception Outline Category");
		CGpuScope gpu_scope("Perception Outline Category");

		// Set constants.
		{
			PROFILE_FUNCTION("Perception Outline Category - Constants");
			// To check when to start painting the outlines.
			cts_perception_scenario_outline.PerceptionScenarioOutlinePositionPlayer = getPlayerPosition();
			cts_perception_scenario_outline.PerceptionScenarioOutlineWaveRadius = currentSonarRadius;

			// Outline variables.
			cts_perception_scenario_outline.PerceptionScenarioOutlineEdgeColor = color_outline;
			cts_perception_scenario_outline.PerceptionScenarioOutlineEdgeWidth = outline_width;
			cts_perception_scenario_outline.PerceptionScenarioOutlineCategory = outline_category;

			// Noise edge variables.
			cts_perception_scenario_outline.PerceptionScenarioOutlineNoiseExtraWidth = outline_noise_extra_width;
			cts_perception_scenario_outline.PerceptionScenarioOutlineNoiseGrainyness = outline_noise_grainy_controller;

			cts_perception_scenario_outline.activate();
			cts_perception_scenario_outline.updateGPU();
		}

		// We render the outlines into a texture so we can apply bloom over them without affecting the rest of the image.
		{
			PROFILE_FUNCTION("Perception Outline Category - Rendering Outlines Into Texture");
			// Activate el multi-render-target MRT
			const int nrender_targets = 2;
			ID3D11RenderTargetView * rts[nrender_targets] = {
				rt2->getRenderTargetView(),
				rt_acc_depth->getRenderTargetView()
			};
			Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.getDepthStencilView());
			rt2->clearRenderTargetView();
			drawFullScreenQuad(perception_category_outline_techn, nullptr);
		}

		// We also add the normal outlines into the image already.
		{
			PROFILE_FUNCTION("Perception Outline Category - Add Outlines Into Image");
			textureToApplyPPE->activateRT();
			drawFullScreenQuad(perception_category_outline_techn_add, rt2);
		}
	}

	// Return outlines.
	return rt2;
}

void TCompPostFXPerception::fetchOutlineValuesForCategory(eRenderCategory perceptionCategory) {
	if (perceptionCategory == eRenderCategory::CATEGORY_PERCEPTION_ENEMIES) {
		color_outline = outline_enemies_color;
		outline_width = outline_enemies_width;
		outline_category = PostFXPerceptionCategory::MASK_ENEMY_CATEGORY;
		outline_noise_extra_width = outline_enemies_noise_extra_width;
		outline_noise_grainy_controller = outline_enemies_noise_grainy_controller;
	}
	else if (perceptionCategory == eRenderCategory::CATEGORY_PERCEPTION_OBJECTS) {
		color_outline = outline_objects_color;
		outline_width = outline_objects_width;
		outline_category = PostFXPerceptionCategory::MASK_OBJECTS_CATEGORY;
		outline_noise_extra_width = outline_objects_noise_extra_width;
		outline_noise_grainy_controller = outline_objects_noise_grainy_controller;
	}
	else {
		color_outline = outline_player_color;
		outline_width = outline_player_width;
		outline_category = PostFXPerceptionCategory::MASK_PLAYER_CATEGORY;
		outline_noise_extra_width = outline_player_noise_extra_width;
		outline_noise_grainy_controller = outline_player_noise_grainy_controller;
	}
}

CRenderToTexture * TCompPostFXPerception::apply(CRenderToTexture * textureToApplyPPE) {
	if (!active) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	calcFade();
	if (!active) return textureToApplyPPE;
	
	// Calculate the current radius position.
	// The radius increases with time and follows a kind of acceleration increase.
	sonarSpeedPerSecond += Time.delta * sonarAceleration;
	currentSonarRadius += Time.delta * sonarSpeedPerSecond;

	// Do Perception
	{
		PROFILE_FUNCTION("PostFx - Perception");
		CGpuScope gpu_scope("Perception");

		// Set constants.
		{
			PROFILE_FUNCTION("Perception - Constants");
			gradient_wave->activate(TS_GRADIENT_2);
			noise->activate(TS_NOISE_MAP);

			// Fade variables.
			cts_perception.PerceptionFadeWeight = fadeValue;
			cts_perception.PerceptionFading = isFadingIn();

			// How visible are the areas where the wave still has not passed by.
			cts_perception.PerceptionNonPercievedAreaColorIntensity = nonExploredAreasColorIntensity;
			
			// Wave values.
			cts_perception.PerceptionWaveRadius = currentSonarRadius;
			cts_perception.PerceptionWaveNoiseAmplitude = sonarWaveNoiseAmplitude;
			cts_perception.PerceptionRadiusOffset = sonarRadiusOffset;

			cts_perception.activate();
			cts_perception.updateGPU();
		}

		{
			PROFILE_FUNCTION("Perception - Rendering");
			rt->activateRT();
			drawFullScreenQuad(perception_second_pass_penalty_techn, textureToApplyPPE);
		}
	}

	return rt;
}


void TCompPostFXPerception::calcFade() {
	if (!deactivate) {
		// Check if we are entering perception.
		if (fade_in && current_time == fade_in_time) {
			fade_in = false;
			current_time = current_time + (fade_out_time - fade_in_time);

			CEntity * player = playerH;
			TMsgPerceptionActivateEntities msg = { true };
			player->sendMsg(msg);

			// Position where the wave comes from.
			cts_perception.PerceptionPositionPlayer = getPlayerPosition();
		}

		if (fade_in)
			current_time = Maths::clamp(current_time + Time.delta, 0.0f, fade_in_time);
		else
			current_time = Maths::clamp(current_time - Time.delta, 0.0f, current_time);
	}
	else {
		if (current_time == 0.0f && fade_in) {
			active = false;
			deactivate = false;
		}

		if (!fade_in && current_time == fade_out_time) {
			fade_in = true;
			current_time = current_time + (fade_in_time - fade_out_time);

			CEntity * player = playerH;
			TMsgPerceptionActivateEntities msg = { false };
			if (player)
				player->sendMsg(msg);

			currentSonarRadius = 0.0f;
			sonarSpeedPerSecond = baseSonarSpeedPerSecond;
		}

		// If fading in, we go back.
		if (fade_in)
			current_time = Maths::clamp(current_time - Time.delta, 0.0f, fade_in_time);
		else
			current_time = Maths::clamp(current_time + Time.delta, 0.0f, fade_out_time);
	}

	if (fade_in)
		fadeValue = current_time / fade_in_time;
	else
		fadeValue = current_time / fade_out_time;
}

void TCompPostFXPerception::OnPerceptionChanged(const TMsgPerceptionStatusChanged & msg) {
	if (msg.active) {
		if (fetchPlayer()) {
			active = msg.active;
			deactivate = false;
		}
	}
	else
		deactivate = true;
}

bool TCompPostFXPerception::fetchPlayer() {
	playerH = getEntityByName("Player");
	if (!playerH.isValid()) return false;

	CEntity * entity = playerH;
	playerTransformH = entity->getComponent<TCompTransform>();
	return playerTransformH.isValid();
}

bool TCompPostFXPerception::isFadingIn() {
	return fade_in && active;
}

Vector3 TCompPostFXPerception::getPlayerPosition() {
	playerH = getEntityByName("Player");
	CEntity * entity = playerH;
	if (entity) {
		playerTransformH = entity->getComponent<TCompTransform>();
		TCompTransform* transform = playerTransformH;
		return transform->getPosition();
	}
	return Vector3(0, 0, 0);
}