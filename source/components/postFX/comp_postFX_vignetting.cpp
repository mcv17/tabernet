#include "mcv_platform.h"
#include "comp_postFX_vignetting.h"

#include "engine.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "render/textures/render_to_texture.h"

#include "components/common/comp_transform.h"

DECL_OBJ_MANAGER("postfx_vignetting", TCompPostFXVignetting);

void TCompPostFXVignetting::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);
	
	/* Fog technique. */
	std::string tech_filename = j.value("vignetting_technique", "vignetting.tech");
	vignettingTech = EngineResources.getResource(tech_filename)->as<CTechnique>();

	fall_time = std::max(j.value("fall_time", fall_time), fall_time);

	if(!cts_vignetting.create("Vignetting"))
		Utils::fatal("Failed while creating the cts_vignetting constants.");

	vignettingAmount =  j.value("vignetting_value", 0.0f);
	cts_vignetting.VignettingFallOff = vignettingAmount;

	if(!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Vignetting rt couldn't be created\n");
}

bool TCompPostFXVignetting::createOrResizeRT(int width, int height) {
	if (!rt)
		rt = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {

		char rt_fog[64];
		sprintf(rt_fog, "Fog_%08x", CHandle(this).asUnsigned());
		return rt->create(rt_fog, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	}
	return true;
}

void TCompPostFXVignetting::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Spacing(0, 5);
	ImGui::DragFloat("Vignetting Amount", &vignettingAmount, 0.01f, 0.0f, 10.0f);
	ImGui::DragFloat("Fall Time", &fall_time, 0.001f, 0.00001f, 1.0f);
}

CRenderToTexture * TCompPostFXVignetting::apply(CRenderToTexture * textureToApplyPPE) {
	if (!active) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	if (fadeIn)
		current_fade_time += Time.delta;
	else
		current_fade_time -= Time.delta;

	vignettingAmount = Maths::clamp(current_fade_time / fall_time, 0.0f, 1.0f);

	if (!fadeIn && vignettingAmount == 1.0f)
		active = false;

	// Do vignetting
	{
		PROFILE_FUNCTION("PostFx - Vignetting");
		CGpuScope gpu_scope("Vignetting rendering");
		
		// Set constants.
		{
			PROFILE_FUNCTION("Vignetting - Constants");
			cts_vignetting.VignettingFallOff = vignettingAmount;
			cts_vignetting.activate();
			cts_vignetting.updateGPU();
		}

		{
			PROFILE_FUNCTION("Vignetting - Rendering");
			rt->activateRT();
			drawFullScreenQuad(vignettingTech, textureToApplyPPE);
		}
	}

	return rt;
}