#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"
#include "..\module_scripting.h"

class CTecnique;
class CRenderToTexture;
class TCompCamera;

/* This component creates black (or any other colors) screen bars while rendering an image
for a more cinematic look. This is done by changing the viewport where the final image
should be rendered. While not a real PostFX component, we leave it here as it still works 
very similar to the other components. */
class TCompPostFXBlackBars : public TCompBase {
	DECL_SIBLING_ACCESS();

	bool openingBars;

	// Color for the bars.
	Vector4 screenBarsColor;
	// How big the bars will be based on a percentage of the screen to cover.
	float verticalScreenPercentage = 0.25;
	// How big the bars will be based on pixels.
	int maxVerticalScreenPixelsPerBar = 250;

	// Percentage currently working.
	float currentBarValuePercentage = 0.0f;
	// Time for the bars to appear
	float baseTimeToGetToGoalBar = 2.0f;
	float timeToGetToGoalBar = baseTimeToGetToGoalBar;
	// Delta variance per second.
	float deltaIncreaseForBars = verticalScreenPercentage / timeToGetToGoalBar;


	ComponentEventHandle OnFinishTransitionHandle;
	void OnFinishTransition();

public:
	~TCompPostFXBlackBars();
	
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();

	// This function will set the rendering viewport. The rendered image will be
	// positioned so the screen bars appear.
	void applyScissor(int rt_width, int rt_height);

	// If true, they will open, if false, they will close.
	void setBlackBars(bool OpenBlackBars);
	void setBlackBars(bool OpenBlackBars, float nTimeToGetToGoal);
	void setBlackBarsFull();
	void setBlackBarsHidden();
	
	Vector4 getBarColors() { return screenBarsColor; }
};