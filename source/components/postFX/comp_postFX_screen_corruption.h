#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

class CTecnique;
class CRenderToTexture;
struct TMsgVisionPenaltyStatusChanged;

// NOT USED. DISCARDED. WE HAD TO REMOVE POWERS AND CORRUPTION.

/* This component applies a post processing effect for
simulating a corruption effect around the player's screen. */
class TCompPostFXScreenCorruption : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Texture to render the effect to. */
	CRenderToTexture * rt;

	// Corruption penalty technique.
	const CTechnique * vision_penalty_techn;

	float currentFallOffValue = 0.0f;
	float goalFallOffValue = 0.0f;
	float maxVisionPenaltyShaderEffect = 1.0f;
	float timeForCompleteEffect = 2.0f;
	float deltaEffectPerSecond = maxVisionPenaltyShaderEffect / timeForCompleteEffect;

	CCteBuffer<TCtesCorruption> cts_vision_shader = CCteBuffer<TCtesCorruption>(CTE_BUFFER_SLOT_CORRUPTION);

public:
	~TCompPostFXScreenCorruption();

	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
	// Applies the effect, returns the CRenderToTexture * with the applied effect.
	CRenderToTexture * apply(CRenderToTexture * textureToApplyPPE);

private:
	bool createOrResizeRT(int width, int height);
	void OnVisionPenaltyChanged(const TMsgVisionPenaltyStatusChanged & msg);
};