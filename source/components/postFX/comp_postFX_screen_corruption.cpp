#include "mcv_platform.h"
#include "comp_postFX_screen_corruption.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "render/module_render.h"

DECL_OBJ_MANAGER("postfx_screen_corruption", TCompPostFXScreenCorruption);

TCompPostFXScreenCorruption::~TCompPostFXScreenCorruption() {
	if (rt) rt->destroy();
	cts_vision_shader.destroy();
}

void TCompPostFXScreenCorruption::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);
	
	// Corruption technique.
	std::string tech_filename = j.value("vision_penalty_techn", "vision_penalty.tech");
	vision_penalty_techn = EngineResources.getResource(tech_filename)->as<CTechnique>();

	maxVisionPenaltyShaderEffect = j.value("max_vision_penalty_shader_value", maxVisionPenaltyShaderEffect);
	timeForCompleteEffect = j.value("time_for_complete_fall_off", timeForCompleteEffect);
	deltaEffectPerSecond = maxVisionPenaltyShaderEffect / timeForCompleteEffect;

	if (!cts_vision_shader.create("Corruption")) Utils::fatal("Failed while creating the cts_vision_shader.");
	cts_vision_shader.CorruptionColor = Vector3(0.2f, 0.1f, 0.4f);
	cts_vision_shader.CorruptionFallOff = 0.0;

	if(!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Screen corruption rt couldn't be created\n");
}


void TCompPostFXScreenCorruption::registerMsgs() {
	DECL_MSG(TCompPostFXScreenCorruption, TMsgVisionPenaltyStatusChanged, OnVisionPenaltyChanged);
}

void TCompPostFXScreenCorruption::debugInMenu() {
	TCompBase::debugInMenu();

	bool changed = ImGui::DragFloat("Vision penalty max effect: ", &maxVisionPenaltyShaderEffect, 0.01f, 0.0f, 10.f);
	changed |= ImGui::DragFloat("Corruption time for complete effect: ", &timeForCompleteEffect, 0.01f, 0.0f, 10.f);
	if (changed)
		deltaEffectPerSecond = maxVisionPenaltyShaderEffect / timeForCompleteEffect;

	ImGui::DragFloat3("Corruption color: ", &cts_vision_shader.CorruptionColor.x, 0.01f, 0.0f, 10.f);
}

bool TCompPostFXScreenCorruption::createOrResizeRT(int width, int height) {
	if (!rt)
		rt = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {
		char rt_screen_corruption[64];
		sprintf(rt_screen_corruption, "Screen_corruption_%08x", CHandle(this).asUnsigned());
		return rt->create(rt_screen_corruption, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	}
	return true;
}

CRenderToTexture * TCompPostFXScreenCorruption::apply(CRenderToTexture * textureToApplyPPE) {
	if (!active) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	// Increase as time goes by.
	if (currentFallOffValue < goalFallOffValue)
		currentFallOffValue = Maths::clamp(currentFallOffValue + deltaEffectPerSecond * Time.delta, 0.0f, goalFallOffValue);
	else if(currentFallOffValue > goalFallOffValue)
		currentFallOffValue = Maths::clamp(currentFallOffValue - deltaEffectPerSecond * Time.delta, 0.0f, maxVisionPenaltyShaderEffect);

	// Do Screen Corruption.
	{
		PROFILE_FUNCTION("PostFx - Screen Corruption");
		CGpuScope gpu_scope("Screen Corruption Penalty");

		// Set constants.
		{
			PROFILE_FUNCTION("Screen Corruption - Constants");
			cts_vision_shader.CorruptionFallOff = currentFallOffValue;
			cts_vision_shader.activate();
			cts_vision_shader.updateGPU();
		}

		{
			PROFILE_FUNCTION("Screen Corruption - Rendering");
			rt->activateRT();
			drawFullScreenQuad(vision_penalty_techn, textureToApplyPPE);
		}
	}

	return rt;
}

void TCompPostFXScreenCorruption::OnVisionPenaltyChanged(const TMsgVisionPenaltyStatusChanged & msg) {
	goalFallOffValue = msg.percentage * maxVisionPenaltyShaderEffect;
}