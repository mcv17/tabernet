#ifndef INC_COMPONENT_RENDER_BLUR_H_
#define INC_COMPONENT_RENDER_BLUR_H_

#include "components/common/comp_base.h"
#include <vector>

struct CBlurStep;
class  CTexture;

/* This component applies a blurring post processing effect. */
class TCompPostFXBlur : public TCompBase {
protected:
	std::vector< CBlurStep * > steps;
	Vector4  weights;
	Vector4  distance_factors; // 1 2 3 4
	float global_distance;
	int   nsteps;
	int   nactive_steps;

	int width, height;

public:
	TCompPostFXBlur();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CRenderToTexture * apply(CRenderToTexture * in_texture);

private:
	bool createOrResizeRT(int width, int height);
};

#endif
