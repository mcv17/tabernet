#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

class CTecnique;
class CRenderToTexture;
struct TMsgEntityCreated;

/* This component applies the antialiasing FXAA from NVIDIA as a post processing effect.
FXAA stands for Fast Approximate Anti-Aliasing. Information about this technique
can be found at: https://github.com/NVIDIAGameWorks/D3DSamples */
class TCompPostFXAA : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Texture to render the effect to. */
	CRenderToTexture * rt;

	CCteBuffer<TCtesFXAA> ctes_fxaa;
	const CTechnique * tech; // Antialliasing technique.


public:
	TCompPostFXAA();
	~TCompPostFXAA();

	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	// Applies the effect, returns the CRenderToTexture * with the applied effect.
	CRenderToTexture * apply(CRenderToTexture * textureToApplyPPE);

private:
	bool createOrResizeRT(int width, int height);
};