#include "mcv_platform.h"
#include "comp_postFX_antialiasing.h"

#include "engine.h"
#include "render/textures/render_to_texture.h"
#include "render/nvidia/FXAA3_11.h"

DECL_OBJ_MANAGER("postfx_antialiasing", TCompPostFXAA);

TCompPostFXAA::TCompPostFXAA()
	: ctes_fxaa(CTE_BUFFER_SLOT_FXAA) {
	bool is_ok = ctes_fxaa.create("fxaa");
	assert(is_ok);
}

TCompPostFXAA::~TCompPostFXAA() {
	if (rt) rt->destroy();
	ctes_fxaa.destroy();
}

void TCompPostFXAA::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);

	tech = EngineResources.getResource("fxaa_antialiasing.tech")->as<CTechnique>();
	ctes_fxaa.fxaaQualityEdgeThreshold = j.value("fxaa_QualityEdgeThreshold", ctes_fxaa.fxaaQualityEdgeThreshold);
	ctes_fxaa.fxaaQualityEdgeThresholdMin = j.value("fxaa_QualityEdgeThresholdMin", ctes_fxaa.fxaaQualityEdgeThresholdMin);
	ctes_fxaa.fxaaQualityRcpFrame = loadVector2(j, "fxaa_QualityRcpFrame");
	ctes_fxaa.fxaaQualitySubpix = j.value("fxaa_QualitySubpix", ctes_fxaa.fxaaQualitySubpix);

	if (!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Antialiasing rt couldn't be created\n");
}

void TCompPostFXAA::debugInMenu() {
	TCompBase::debugInMenu();

	// The minimum amount of local contrast required to apply algorithm.
	//   0.333 - too little (faster)
	//   0.250 - low quality
	//   0.166 - default
	//   0.125 - high quality 
	//   0.063 - overkill (slower)
	ImGui::DragFloat("FXAA Edge Threshold", &ctes_fxaa.fxaaQualityEdgeThreshold, 0.001f, 0.0f, 0.4f);

	// It is here now to allow easier tuning.
	// Trims the algorithm from processing darks.
	//   0.0833 - upper limit (default, the start of visible unfiltered edges)
	//   0.0625 - high quality (faster)
	//   0.0312 - visible limit (slower)
	ImGui::DragFloat("FXAA Edge Threshold Min", &ctes_fxaa.fxaaQualityEdgeThresholdMin, 0.0001f, 0.0f, 0.10f);

	// Choose the amount of sub-pixel aliasing removal.
	// This can effect sharpness.
	//   1.00 - upper limit (softer)
	//   0.75 - default amount of filtering
	//   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
	//   0.25 - almost off
	//   0.00 - completely off
	ImGui::DragFloat("FXAA Edge Subpix", &ctes_fxaa.fxaaQualitySubpix, 0.01f, 0.0f, 1.0f);
}

bool TCompPostFXAA::createOrResizeRT(int width, int height) {
	if (!rt)
		rt = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {
		ctes_fxaa.fxaaQualityRcpFrame = Vector2(1.0 / width, 1.0 / height);
		char rt_fxaa[64];
		sprintf(rt_fxaa, "Antialiasing_%08x", CHandle(this).asUnsigned());
		return rt->create(rt_fxaa, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	}
	return true;
}

CRenderToTexture * TCompPostFXAA::apply(CRenderToTexture * textureToApplyPPE) {
	if (!active) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	// Do FXAA
	{
		PROFILE_FUNCTION("PostFx - Antialiasing");
		CGpuScope gpu_scope("Antialiasing rendering");

		{
			PROFILE_FUNCTION("Antialiasing - Rendering");
			// Activate constants.
			ctes_fxaa.updateGPU();
			ctes_fxaa.activate();

			// Apply fxaa on image.
			rt->activateRT();
			drawFullScreenQuad(tech, textureToApplyPPE);
		}
	}

	return rt;
}