#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

class CTecnique;
class CRenderToTexture;
struct TMsgEntityCreated;

class TCompVanish : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Texture to render the effect to. */
	CRenderToTexture * rt;
	
	/* Technique and constants. */
	const CTechnique * vanishTech; 
	CCteBuffer<TCtesVanish> cts_vanish = CCteBuffer<TCtesVanish>(CTE_BUFFER_SLOT_VANISH);

	// Variables 
	float  _totalTime			= 2.5f;
	float _intensityMultiplier = 1.f;
	VEC3 _innerBorderColor		= VEC3(0.2062745098, 0.01568627451, 0.3764705882);
	VEC3 _outterBorderColor		= VEC3(1, 0.03921568627, 0.468627451);
	float  _innerBorderSize		= 0.05f;
	float  _outterBorderSize	= 0.02f;
	bool _update				= false;

public:
	~TCompVanish() {
		if(rt) rt->destroy();
		cts_vanish.destroy();
	}

	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	
	// Applies the effect, returns the CRenderToTexture * with the applied effect.
	void apply();

	void setActive(bool nActive) {
		active = nActive ? true : active;
	}
};