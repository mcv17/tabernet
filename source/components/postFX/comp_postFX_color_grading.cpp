#include "mcv_platform.h"
#include "comp_postFX_color_grading.h"
#include "render/textures/render_to_texture.h"
#include "resources/resource.h"

#include "engine.h"

DECL_OBJ_MANAGER("postfx_color_grading", TCompPostFXColorGrading);

void TCompPostFXColorGrading::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Text("LUT: %s", lut1->getName().c_str());
	ImGui::DragFloat("Current Color Grading", &current_color_grading_amount, 0.001f, 0.0f, 1.0f);
	ImGui::DragFloat("Final Color Grading", &final_color_grading_amount, 0.001f, 0.0f, 1.0f);
	ImGui::DragFloat("Fall Time", &fall_time, 0.001f, 0.00001f, 1.0f);
}

void TCompPostFXColorGrading::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);

	// Lut variables.
	fall_time = std::max(j.value("fall_time", fall_time), fall_time);
	final_color_grading_amount = Maths::clamp(j.value("amount", 0.0f), 0.0f, 1.0f);
	current_color_grading_amount = final_color_grading_amount;
	
	// Lut texture.
	lutPath = j.value("lut", "");
	lut1 = EngineResources.getResource(lutPath)->as<CTexture>();
}

void TCompPostFXColorGrading::activate() {
	if (fall_time == 0.0f)
		current_color_grading_amount = final_color_grading_amount;
	else {
		if (fadeIn)
			current_fade_time = Maths::clamp(current_fade_time + Time.delta, 0.0f, fall_time);
		else
			current_fade_time = Maths::clamp(current_fade_time - Time.delta, 0.0f, fall_time);
		current_color_grading_amount = Maths::clamp(current_fade_time / fall_time, 0.0f, final_color_grading_amount);
	}

	// Do color grading.
	{
		PROFILE_FUNCTION("PostFx - Color Grading");
		CGpuScope gpu_scope("Color Grading set up");
		
		ctes_shared.GlobalLUTAmount = active ? current_color_grading_amount : 0.f;
		ctes_shared.updateGPU();
		lut1->activate(TS_LUT_COLOR_GRADING);
	}
}

void  TCompPostFXColorGrading::setColorGradingTextureToLoad(const std::string & nLutPath){
	lutPath = nLutPath;
	lut1 = EngineResources.getResource(lutPath)->as<CTexture>();
}