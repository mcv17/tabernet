#include "mcv_platform.h"
#include "comp_postFX_focus.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"

#include "engine.h"

DECL_OBJ_MANAGER("postfx_focus", TCompPostFXFocus);

TCompPostFXFocus::TCompPostFXFocus()
: cte_focus(CTE_BUFFER_SLOT_FOCUS)
{
	bool is_ok = cte_focus.create("Focus");
	assert(is_ok);
}

TCompPostFXFocus::~TCompPostFXFocus() {
	cte_focus.destroy();
}

void TCompPostFXFocus::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);
	cte_focus.focus_z_center_in_focus = j.value("z_center_in_focus", 30.f);
	cte_focus.focus_z_margin_in_focus = j.value("z_margin_in_focus", 0.f);
	cte_focus.focus_transition_distance = j.value("transition_distance", 15.f);
	cte_focus.focus_modifier = j.value("focus_modifier", 1.f);

	tech = EngineResources.getResource("focus.tech")->as<CTechnique>();
	mesh = EngineResources.getResource("unit_quad_xy.mesh")->as<CMesh>();

	// with the first use, init with the input resolution
	if (!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Focus rt couldn't be created\n");
}

bool TCompPostFXFocus::createOrResizeRT(int width, int height) {
	if (!rt)
		rt = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {
		char rt_focus[64];
		sprintf(rt_focus, "Focus_%08x", CHandle(this).asUnsigned());
		return rt->create("RT_Focus", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	}
	return true;
}

void TCompPostFXFocus::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::DragFloat("Z Center In Focus", &cte_focus.focus_z_center_in_focus, 0.1f, 0.f, 1000.f );
	ImGui::DragFloat("Margin In Focus", &cte_focus.focus_z_margin_in_focus, 0.1f, 0.f, 300.f);
	ImGui::DragFloat("Transition Distance", &cte_focus.focus_transition_distance, 0.1f, 0.f, 1000.f);
	ImGui::DragFloat("Focus Modifier", &cte_focus.focus_modifier, 0.1f, 0.f, 3.0f);
}

void TCompPostFXFocus::setFocusMultiplier(float multiplier) {
	Maths::clamp(multiplier, 0.0f, 3.0f);
	cte_focus.focus_modifier = multiplier;
}

CRenderToTexture * TCompPostFXFocus::apply(CRenderToTexture * focus_texture, CRenderToTexture * blur_texture) {
	if (!active) return focus_texture;

	if (!createOrResizeRT(focus_texture->getWidth(), focus_texture->getHeight())) return nullptr;
	

	// Do focus
	{
		PROFILE_FUNCTION("PostFx - Focus");
		CGpuScope gpu_scope("Focus rendering");

		assert(rt);

		// Activate constants.
		{
			PROFILE_FUNCTION("Focus - Upload constants");
			cte_focus.updateGPU();
			cte_focus.activate();
		}

		assert(mesh);
		assert(tech);
		assert(focus_texture);
		assert(blur_texture);

		// Render.
		{
			PROFILE_FUNCTION("Focus - Rendering");
			rt->activateRT();
			focus_texture->activate(TS_ALBEDO);
			blur_texture->activate(TS_ALBEDO1);
			tech->activate();
			mesh->activateAndRender();
		}

	}

	return rt;
}