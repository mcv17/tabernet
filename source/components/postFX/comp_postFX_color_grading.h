#ifndef INC_COMPONENT_COLOR_GRADING_H_
#define INC_COMPONENT_COLOR_GRADING_H_

#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

/* This component applies a color grading post processing effect. */
class TCompPostFXColorGrading : public TCompBase {
	std::string lutPath;
	const CTexture*               lut1 = nullptr; // Pointer to Look Up Table for color grading.

	bool fadeIn = true;
	float current_fade_time = 0.0f;
	float fall_time = 0.0001f;
	float current_color_grading_amount = 0.0f;
	float final_color_grading_amount = 1.f;	// Amount of color grading that will be added to the look up table [0 (no effect), 1 (full effect)].
	
public:
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
  
	void activate();

	/* Setters. */
	void setColorGradingCurrentAmount(float nCurrentGradAmount) { current_color_grading_amount = final_color_grading_amount = nCurrentGradAmount; }
	void setColorGradingFallTime(float nFallTime) { fadeIn = nFallTime; }
	void setColorGradingTextureToLoad(const std::string & nLutPath);
	void setActive(bool nActive) { active = fadeIn = nActive; }
};

#endif
