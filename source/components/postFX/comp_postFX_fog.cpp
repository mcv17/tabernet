#include "mcv_platform.h"
#include "comp_postFX_fog.h"

#include "engine.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "render/textures/render_to_texture.h"

#include "components/common/comp_transform.h"

DECL_OBJ_MANAGER("postfx_fog", TCompPostFXFog);

void TCompPostFXFog::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);
	
	/* Fog technique. */
	std::string tech_filename = j.value("fog_technique", "fog.tech");
	fog = EngineResources.getResource(tech_filename)->as<CTechnique>();
	
	/* Gradients can be used to create "stylized fogs". These types of fog change their colors based on a texture and the distance from the player. */
	/* Gradient textures must represent a gradient of colors. Artists should add the colors in the image taking into account the fog factor distribution
	to show colors as they intend to, two debug shaders are prepared in the fog.fx shader to show the fog distribution easily. AplyDebugLinear and ApplyDebugExponential. */
	std::string CurrentFogGradient = j.value("fog_gradient", "data/textures/fog_gradients/fogGradient.dds");
	std::string NextFogGradient = j.value("next_fog_gradient", "data/textures/fog_gradients/fogGradient.dds");

	currentFogGradient = EngineResources.getResource(CurrentFogGradient)->as<CTexture>();
	nextFogGradient = EngineResources.getResource(NextFogGradient)->as<CTexture>();

	/* Fog color for normal, non-stylized fogs.*/
	FogColor = loadVector3(j, "fog_color");

	/* Parameters for linear fog. */
	FogStartDistance = j.value("fog_start_distance", 0.0f);
	FogEndDistance = j.value("fog_end_distance", 1000.0f);

	/* Parameters for exponential fog. */
	FogDensity = j.value("fog_density", 1.0f);

	/* Fog intensity is used by stylized fog to control the intensity of the colors. */
	FogIntensity = j.value("fog_intensity", 1.0f);

	if(!cts_fog_shader.create("Fog"))
		Utils::fatal("Failed while creating the cts_fog constants.");

	if(!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Fog rt couldn't be created\n");
}

bool TCompPostFXFog::createOrResizeRT(int width, int height) {
	if (!rt)
		rt = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {

		char rt_fog[64];
		sprintf(rt_fog, "Fog_%08x", CHandle(this).asUnsigned());
		return rt->create(rt_fog, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	}
	return true;
}

void TCompPostFXFog::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Spacing(0, 5);
	ImGui::ColorEdit3("Fog Color: ", &FogColor.x, ImGuiColorEditFlags_HDR);

	ImGui::DragFloat("Fog Start Distance: ", &FogStartDistance, 0.01f, 0.0f, 10000.0f);
	ImGui::DragFloat("Fog End Distance: ", &FogEndDistance, 0.01f, 0.0f, 10000.0f);

	ImGui::DragFloat("Fog Intensity: ", &FogIntensity, 0.01f, 0.0f, 10.0f);
	ImGui::DragFloat("Fog Density: ", &FogDensity, 0.00001f, 0.0f, 10.0f);

	ImGui::DragFloat("Fog lerp: ", &cts_fog_shader.FogLerp, 0.001f, 0.0f, 1.0f);
}

void TCompPostFXFog::setFogLerp(float nLerp) {
	cts_fog_shader.FogLerp = nLerp;
}

CRenderToTexture * TCompPostFXFog::apply(CRenderToTexture * textureToApplyPPE) {
	if (!active) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	// Do fog
	{
		PROFILE_FUNCTION("PostFx - Fog");
		CGpuScope gpu_scope("Fog rendering");
		
		// Set constants.
		{
			PROFILE_FUNCTION("Fog - Constants");
			cts_fog_shader.FogColor = FogColor;
			cts_fog_shader.FogStart = FogStartDistance;
			cts_fog_shader.FogEnd = FogEndDistance;
			cts_fog_shader.FogIntensity = FogIntensity;
			cts_fog_shader.FogDensity = FogDensity;
			cts_fog_shader.activate();
			cts_fog_shader.updateGPU();
		}

		{
			PROFILE_FUNCTION("Fog - Rendering");
			rt->activateRT();
			currentFogGradient->activate(TS_GRADIENT);
			nextFogGradient->activate(TS_GRADIENT_2);
			drawFullScreenQuad(fog, textureToApplyPPE);
		}
	}

	return rt;
}