#include "mcv_platform.h"
#include "comp_vanish.h"

#include "engine.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "render/textures/render_to_texture.h"

#include "components/common/comp_transform.h"

DECL_OBJ_MANAGER("vanish", TCompVanish);

void TCompVanish::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);
	
	vanishTech = EngineResources.getResource("vanish.tech")->as<CTechnique>();

	if(!cts_vanish.create("Vanish"))
		Utils::fatal("Failed while creating the cts_vanish constants.");

}

void TCompVanish::debugInMenu() {
	TCompBase::debugInMenu();
	if (ImGui::DragFloat("Total time: ", &_totalTime, 0.01f, 0.0f, 10.0f)) _update = true;
	if (ImGui::DragFloat("Intensity multiplier: ", &_intensityMultiplier, 0.01f, 0.0f, 100.0f)) _update = true;
	if (ImGui::DragFloat3("Inner Border Color: ", &_innerBorderColor.x, 1.0f, 0.0f, 255.0f)) _update = true;
	if (ImGui::DragFloat3("Outter Border Color: ", &_outterBorderColor.x, 1.0f, 0.0f, 255.0f)) _update = true;
	if (ImGui::DragFloat("Inner Border size: ", &_innerBorderSize, 0.001f, 0.0f, 10.0f)) _update = true;
	if (ImGui::DragFloat("Outter Border size: ", &_outterBorderSize, 0.001f, 0.0f, 10.0f)) _update = true;

	_update ? apply() : false;
}

void TCompVanish::apply() {


	// Upload constants
	{
		// Set constants.
		{
			PROFILE_FUNCTION("Vanish - Set constants");
			cts_vanish.VanishTotalTime = _totalTime;
			cts_vanish.VanishAddedIntensity = _intensityMultiplier;
			cts_vanish.VanishInnerBorderColor = _innerBorderColor;
			cts_vanish.VanishOutterBorderColor = _outterBorderColor;
			cts_vanish.VanishInnerBorderSize = _innerBorderSize;
			cts_vanish.VanishOutterBorderSize = _outterBorderSize;
		}

		{
			PROFILE_FUNCTION("Vanish - Upload");
			cts_vanish.activate();
			cts_vanish.updateGPU();
		}
	}
}