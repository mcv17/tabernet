#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

class CTecnique;
class CRenderToTexture;
struct TMsgEntityCreated;

/* This component applies a burning post processing effect used for transitions. */
class TCompPostFXBurningFade : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Texture to render the effect to. */
	CRenderToTexture * rt;
	
	/* Technique and constants. */
	const CTechnique * burningFadeTech; // Viggneting technique with the shader to show the effect.
	CCteBuffer<TCtesBurningFade> cts_burning_fade = CCteBuffer<TCtesBurningFade>(CTE_BUFFER_SLOT_BURNING_FADE);

	// Variables.
	Vector3 edgeBurningColor = Vector3(1.5, 0.5, 0.0);
	float burningEdgeSize = 0.15;
	float colorIntensity = 1.6;
	float current_time = 0.0f;
	float time_to_finish_burning_fade = 1.2f;
	float background_treshold = 0.01;

public:
	~TCompPostFXBurningFade() {
		if(rt) rt->destroy();
		cts_burning_fade.destroy();
	}

	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	
	// Applies the effect, returns the CRenderToTexture * with the applied effect.
	CRenderToTexture * apply(CRenderToTexture * textureToApplyPPE);

private:
	bool createOrResizeRT(int width, int height);
};