#ifndef INC_COMPONENT_RENDER_FOCUS_H_
#define INC_COMPONENT_RENDER_FOCUS_H_

#include <vector>
#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

class TCompPostFXFocus : public TCompBase {
private:
  CCteBuffer<TCtesFocus> cte_focus;
  CRenderToTexture *	   rt = nullptr;
  const CTechnique *	   tech = nullptr;
  const CMesh	*   	     mesh = nullptr;

public:
  TCompPostFXFocus();
  ~TCompPostFXFocus();

  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void setFocusMultiplier(float multiplier);
  CRenderToTexture * apply(CRenderToTexture * focus_texture, CRenderToTexture * blur_texture);

private:
	bool createOrResizeRT(int width, int height);
};

#endif
