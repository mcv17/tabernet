#ifndef INC_COMPONENT_RENDER_AO_H_
#define INC_COMPONENT_RENDER_AO_H_

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "render/intel/ASSAO.h"
#include <vector>

class CRenderToTexture;
class CTechnique;
class CMesh;
class CTexture;

/* Ambient occlusion Post Process Effect.
Implements Ambient Occlusion effect. There are two different methods.
A SSAO implementation and Intel's ASSAO implementation. */

class TCompPostFXAO : public TCompBase {
	// Texture where effect is applied to.
	CRenderToTexture*       rt_output = nullptr;
	// White texture returned if SSAO is inactive.
	const CTexture *         white = nullptr;
	// SSAO technique.
	const CTechnique*       tech = nullptr;
	
	// AO variables.
	float                   amount = 1.f;
	float                   radius = 6.f;
	float                   zrange_discard = 0.02f;
	float                   amount_spreading = 0.85f;
	
	// ASSAO settings.
	bool                    use_assao = true;
	ASSAO_Settings          settings;
	ASSAO_InputsDX11        assao_inputs;
	
	DECL_SIBLING_ACCESS();

public:
	~TCompPostFXAO() { rt_output->destroy(); }
	
	void  load(const json& j, TEntityParseContext& ctx);
	void  debugInMenu();
	const CTexture * apply(CTexture * linear_depth_texture, CTexture * normals, ASSAO_Effect * fx);

private:
	bool createOrResizeRT(int width, int height);
	void debugIntelASSAO();
	void debugSSAO();
	void uploadIntelASSAOCts(CTexture * normals, ASSAO_Effect * fx);
	void uploadSSAOCts(CTexture * linear_depth_texture);
};

#endif
