#include "mcv_platform.h"
#include "comp_postFX_black_bars.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "render/module_render.h"

#include "components/camera/comp_camera.h"

DECL_OBJ_MANAGER("postfx_black_bars", TCompPostFXBlackBars);

TCompPostFXBlackBars::~TCompPostFXBlackBars() {
	EngineScripting.UnregisterNewComponentEventForScripts(CHandle(this), "onBlackBarsEnd");
}

void TCompPostFXBlackBars::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);

	// Load bar color, percentage to cover, and number in max pixels.
	screenBarsColor = loadVector4(j, "screen_bars_color");
	verticalScreenPercentage = j.value("screen_bars_vertical_screen_percentage", 0.4);
	maxVerticalScreenPixelsPerBar = j.value("screen_bars_max_vertical_pixels_per_bar", 800);

	// Load the speed for the bars to appear.
	baseTimeToGetToGoalBar = j.value("screen_bars_time_for_bars_to_reach_size", baseTimeToGetToGoalBar);
	timeToGetToGoalBar = baseTimeToGetToGoalBar;
	deltaIncreaseForBars = verticalScreenPercentage / timeToGetToGoalBar;

	// Create scripting events so scripts can bind to this component.
	OnFinishTransitionHandle = EngineScripting.RegisterNewComponentEventForScripts(CHandle(this), "onBlackBarsEnd");
}

void TCompPostFXBlackBars::registerMsgs() {}

void TCompPostFXBlackBars::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::DragFloat4("Screen Bar Colors", &screenBarsColor.x, 0.001f, 0.0f, 1.0f);
	ImGui::DragFloat("Screen Bar Vertical Percentage", &verticalScreenPercentage, 0.001f, 0.0f, 1.0f);
	ImGui::DragInt("Screen Bar Max size in Pixels", &maxVerticalScreenPixelsPerBar, 1, 0, 4000);

	if(ImGui::DragFloat("Screen Bar Time For Bars To Reach Size", &timeToGetToGoalBar, 0.001f, 0.0f, 1.0f))
		deltaIncreaseForBars = verticalScreenPercentage / timeToGetToGoalBar;
}

void TCompPostFXBlackBars::OnFinishTransition() {
	if(!openingBars)
		active = false;
	timeToGetToGoalBar = baseTimeToGetToGoalBar;
	OnFinishTransitionHandle.CallEventForAllScripts(); // Throws event so all scripts binded will receive it.
}

// This function will set the rendering viewport. The rendered image will be
// positioned so the screen bars appear.
void TCompPostFXBlackBars::applyScissor(int rt_width, int rt_height) {
	if (!active) return;

	if (openingBars) {
		if (currentBarValuePercentage < verticalScreenPercentage)
			currentBarValuePercentage = Maths::clamp(currentBarValuePercentage + Time.delta * deltaIncreaseForBars, 0.0f, verticalScreenPercentage);
		else
			OnFinishTransition();
	}
	else {
		if (currentBarValuePercentage > 0.0f)
			currentBarValuePercentage = Maths::clamp(currentBarValuePercentage - Time.delta * deltaIncreaseForBars, 0.0f, verticalScreenPercentage);
		else
			OnFinishTransition();
	}

	int pixelsForBars = currentBarValuePercentage * rt_height;
	int pixelsPerBar = pixelsForBars / 2;
	if (pixelsPerBar > maxVerticalScreenPixelsPerBar)
		pixelsPerBar = maxVerticalScreenPixelsPerBar;

	D3D11_RECT rects[1] = { 0, pixelsPerBar, rt_width, rt_height - pixelsPerBar };
	Render.ctx->RSSetScissorRects(1, rects);
}

void TCompPostFXBlackBars::setBlackBars(bool OpenBlackBars, float nTimeToGetToGoal) {
	openingBars = OpenBlackBars;
	active = true;
	if (nTimeToGetToGoal == 0.0f) nTimeToGetToGoal = 1.0f;
	timeToGetToGoalBar = nTimeToGetToGoal;
	deltaIncreaseForBars = verticalScreenPercentage / timeToGetToGoalBar;
}

void TCompPostFXBlackBars::setBlackBarsFull() {
	active = true;
	openingBars = true;
	currentBarValuePercentage = verticalScreenPercentage;
}

void TCompPostFXBlackBars::setBlackBarsHidden() {
	active = true;
	openingBars = false;
	currentBarValuePercentage = 0.0f;
}

// If true, they will open, if false, they will close.
void TCompPostFXBlackBars::setBlackBars(bool OpenBlackBars) {
	openingBars = OpenBlackBars;
	active = true;
}