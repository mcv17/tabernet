#ifndef INC_COMP_TAGS_H_
#define INC_COMP_TAGS_H_

#include "comp_base.h"

class CTagsManager {
	std::unordered_map< uint32_t, VHandles > entities_by_tag;
	std::unordered_map< uint32_t, std::string > tags_names;

public:
	void registerEntityHasTag(CHandle h, uint32_t tag);
	void unregisterEntityHasTag(CHandle h, uint32_t tag);
	const VHandles & getAllEntitiesByTag(uint32_t tag);
	static CTagsManager & get();
	void renderInMenu();

	void registerTagName(uint32_t tag, const std::string& tag_name);
	const std::string & getTagName(uint32_t tag) const;
};

struct TCompTags : public TCompBase {
	const static uint32_t max_tags = 4;
	uint32_t tags[max_tags];
	
	~TCompTags();
	static void declareInLua();
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	
	bool addTag(uint32_t tag);
	bool addTag(const char * txt);

	bool hasTag(uint32_t tag) const;
	bool hasTag(const char * txt) const;

	void delTag(uint32_t tag);
	void delTag(const char * txt);

	// Returns the tag given an index. Returns a null tag (0) if out of bounds.
	uint32_t getTagFromIndex(int index);

	// Returns true if tags is null or if we access out of bounds.
	bool isTagFromIndexNull(int index);
	
	static uint32_t getTagValue(const char * txt);
};


#endif
