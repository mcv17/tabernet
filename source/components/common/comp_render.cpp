#include "mcv_platform.h"
#include "comp_render.h"

#include "engine.h"
#include "entity/common_msgs.h"

#include "render/render.h"
#include "render/meshes/mesh.h"
#include "render/textures/texture.h"
#include "render/textures/material.h"
#include "render/shaders/technique.h"
#include "render/render_manager.h"

#include "components/common/comp_transform.h"
#include "components/common/comp_render_blending.h"
#include "components/powers/comp_perception.h"
#include "skeleton/comp_skeleton.h"

#include "utils/murmur3/murmur3.h"

DECL_OBJ_MANAGER("render", TCompRender);

using VStrings = std::vector< std::string >;

/* Message functions. */

void TCompRender::registerMsgs() {
	DECL_MSG(TCompRender, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompRender, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompRender, TMsgRenderingStatus, onRenderingStatus);
	DECL_MSG(TCompRender, TMsgResetComponent, onComponentReset);
	DECL_MSG(TCompRender, TMsgDefineLocalAABB, onDefineLocalAABB);
	DECL_MSG(TCompRender, TMsgSetMeshState, onMeshVisibility);
	DECL_MSG(TCompRender, TMsgSetEntityMeshInstantiated, onRenderingFullyInstantiated);
}

void TCompRender::declareInLua(){
	EngineScripting.declareComponent<TCompRender>
	(
		// name in LUA
		"TCompRender",
		"getColor", &TCompRender::getColor,
		"setColor", &TCompRender::setColor
	);
}

void TCompRender::onToggleComponent(const TMsgToogleComponent & msg) {
	if (isInstanced) return;

	active = msg.active;
	is_visible = active ? true : false;
	updateRenderManager();
}


void TCompRender::onEntityCreated(const TMsgEntityCreated & msg) {
	if (is_decal) {
		TCompTransform * transform = getComponent<TCompTransform>();
		if (!transform) return;
		aabb.Extents = transform->getScale() * aabb.Extents;
	}
}

void TCompRender::onRenderingStatus(const TMsgRenderingStatus & msg) {
	if (isInstanced) return;

	active = msg.activate;
	is_visible = active ? true : false;
	updateRenderManager();
}

void TCompRender::onComponentReset(const TMsgResetComponent & msg) 
{
	if (isInstanced) return;

	is_visible = true;

	TCompRenderBlending * blending = getComponent<TCompRenderBlending>();
	if (blending && blending->isActive())
		showMeshesWithState(2);
	else {
		CEntity * player = getEntityByName("Player");
		TCompPerception* perception = player->getComponent<TCompPerception>();
		if (perception && perception->isPerceptionActive())
			showMeshesWithState(1);
		else
			showMeshesWithState(0);
	}
	updateRenderManager();
}

void TCompRender::onDefineLocalAABB(const TMsgDefineLocalAABB & msg) {
	AABB::CreateMerged(*msg.aabb, *msg.aabb, aabb);
}

// Gets called to show meshes with a different state.
// Right now used for perception.
void TCompRender::onMeshVisibility(const TMsgSetMeshState & msg) {
	if (isInstanced) return;
	setCurrentState(msg.state);
}

void TCompRender::onRenderingFullyInstantiated(const TMsgSetEntityMeshInstantiated & msg) {
	if (msg.active) {
		isInstanced = true;
		active = false;
		is_visible = false;
		CRenderManager::get().delKeys(CHandle(this));
	}
	else {
		// Right now we are not creating the aabb again. MAybe we should in the future. This is mostly done for the editor
		// though, as objects should not be removing themselves from instancing on runtime.
		isInstanced = false;
		active = true;
		is_visible = true;
		updateRenderManager();
	}
}

TCompRender::~TCompRender() {
	CRenderManager::get().delKeys(CHandle(this));
	if (isInstanced)
		EngineInstancing.removeFromRender(CHandle(this).getOwner());
}

void TCompRender::load(const json& j, TEntityParseContext& ctx) {
	if (j.count("color"))
		color = loadVector4(j, "color");
	else
		color = VEC4(1, 1, 1, 1);

	can_be_instanced = j.value("can_be_instanced", false);

	if (j.count("meshes")) {
		auto & jmeshes = j["meshes"];
		assert(jmeshes.is_array());
		for (int i = 0; i < jmeshes.size(); ++i)
			readMesh(jmeshes[i]);
	}
	else if (j.count("mesh"))
		readMesh(j);

	updateRenderManager();
}

void TCompRender::readMesh(const json & j) {
	MeshPart mp;

	//if (!j.count("mesh")) {
	//	Utils::fatal("Missing attribute mesh reading mesh in input json %s", j.dump(2, ' ').c_str());
	//}

	std::string mesh_name = j.value("mesh", "data/meshes/Teapot001.mesh");
	mp.mesh = EngineResources.getResource(mesh_name)->as<CMesh>();
	Utils::dbg("For mesh %s\n", mesh_name.c_str());
	int mesh_instances_group = j.value("instances_group", 0);

	if (j.count("materials")) {
		mp.is_visible = j.value("visible", true);
		mp.state = j.value("state", 0);
		// If it has an id, it can be used to map the string to the actual state number.
		if (j.count("state_id")) {
			std::string state_id = j["state_id"];
			assert(name_state_to_id.count(state_id) == 0 && "State ID already exists.");
			if (name_state_to_id.count(state_id) != 0)
				Utils::fatal("State ID already exists for the given tcomp render with mesh: %s", mesh_name);
			name_state_to_id[j["state_id"]] = mp.state;
		}
		
		VStrings names = j["materials"].get< VStrings >();

		int idx = 0;
		for (auto mat_name : names) {
			mp.material = EngineResources.getResource(mat_name)->as<CMaterial>();
			is_decal |= mp.material->category == eRenderCategory::CATEGORY_DECALS;
			mp.mesh_group = idx;
			mp.mesh_instances_group = mesh_instances_group;
			
			Utils::dbg("Slot %d will use mat %s\n", idx, mat_name.c_str());
			++idx;

			parts.push_back(mp);
		}
	}
	else {
		std::string mat_name = j.value("material", "data/materials/plain.material");
		mp.material = EngineResources.getResource(mat_name)->as<CMaterial>();
		is_decal |= mp.material->category == eRenderCategory::CATEGORY_DECALS;
		mp.mesh_group = 0;
		mp.mesh_instances_group = mesh_instances_group;
		mp.is_visible = j.value("visible", true);
		mp.state = j.value("state", 0);

		// If it has an id, it can be used to map the string to the actual state number.
		if (j.count("state_id")) {
			std::string state_id = j["state_id"];
			assert(name_state_to_id.count(state_id) == 0 && "State ID already exists.");
			if (name_state_to_id.count(state_id) != 0)
				Utils::fatal("State ID already exists for the given tcomp render with mesh: %s", mesh_name);
			name_state_to_id[j["state_id"]] = mp.state;
		}

		parts.push_back(mp);
	}

	AABB::CreateMerged(aabb, aabb, mp.mesh->getAABB());
}

void TCompRender::debugInMenu() {
	if (ImGui::Checkbox("Is Active: ", &active)) {
		is_visible = active ? true : false;
		updateRenderManager();
	}

	bool isInstancedMesh = isInstanced;
	if (ImGui::Checkbox("Is Instanced: ", &isInstancedMesh)) {
		if (isInstanced && !isInstancedMesh)
			EngineInstancing.removeFromRender(CHandle(this).getOwner());
		else if(!isInstanced && isInstancedMesh)
			EngineInstancing.addToRender(CHandle(this).getOwner());
		isInstanced = isInstancedMesh;
	}


	ImGui::Checkbox("Is Decal: ", &is_decal);

	ImGui::ColorEdit4("Color", &color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
	bool changed = ImGui::Checkbox("Entity Visible", &is_visible);;
	for (auto& p : parts) {
		ImGui::PushID(&p);
		ImGui::LabelText("Group", "%d", p.mesh_group);
		ImGui::LabelText("Mesh", p.mesh->getName().c_str());
		ImGui::LabelText("State", "%d", p.state);
		changed |= ImGui::Checkbox("Visible", &p.is_visible);
		if (ImGui::TreeNode("Material")) {
			((CMaterial*)p.material)->renderInMenu();
			ImGui::TreePop();
		}
		ImGui::PopID();
	}

	if (ImGui::DragInt("Mesh State", &curr_state, 0.02f, 0, 7))
		showMeshesWithState(curr_state);

	ImGui::DragFloat3("AABB Center", &aabb.Center.x, 0.01f, -5.0f, 5.0f);
	ImGui::DragFloat3("AABB Half", &aabb.Extents.x, 0.01f, -5.0f, 5.0f);

	if (changed)
		updateRenderManager();
}

void TCompRender::renderDebug() {
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;
	MAT44 world = transform->asMatrix();

	TCompSkeleton* c_skel = getComponent<TCompSkeleton>();
	if (c_skel) {
		c_skel->updateCtesBones();
		c_skel->getCBBones().activate();
	}

	Vector4 color = Vector4(1, 1, 1, 1);
	activateObject(world, color);

	for (auto& p : parts) {
		if (!p.is_visible || !is_visible)
			continue;
		activateDebugTech(p.mesh);
		p.mesh->activate();
		p.mesh->renderGroup(p.mesh_group, p.mesh_instances_group);
	}
}

void TCompRender::registerToInstancing(){
	EngineInstancing.addToRender(CHandle(this).getOwner());
}

void TCompRender::unregisterFromInstancing(){
	EngineInstancing.removeFromRender(CHandle(this).getOwner());
}

void TCompRender::showMeshesWithState(int new_state) {
  curr_state = new_state;
  for (auto& p : parts)
    p.is_visible = (p.state == new_state || p.is_secondary);
  updateRenderManager();
}

void TCompRender::updateRenderManager() {
	PROFILE_FUNCTION("UpdateRenderManager");
	CRenderManager & rm = CRenderManager::get();
	CHandle h_me = CHandle(this);

	// First, delete all references from me in the RenderManager
	{
		PROFILE_FUNCTION("DelKeys");
		rm.delKeys(CHandle(this));
	}

	// Then register all draw calls which are active
	{
		PROFILE_FUNCTION("AddKeys");
		for (auto& p : parts) {
			if (!p.is_visible || !is_visible) continue;
			rm.addKey(h_me, p.mesh, p.material, CHandle(), p.mesh_group, p.mesh_instances_group, p.material->category == eRenderCategory::CATEGORY_PARTICLES,
				p.material->category == eRenderCategory::CATEGORY_PARTICLES_PERCEPTION);
		}
	}
}

void TCompRender::addParticlesKeys() {
	PROFILE_FUNCTION("AddParticlesKeys");
	CRenderManager& rm = CRenderManager::get();
	CHandle h_me = CHandle(this);

	// Then register all draw calls which are active
	for (auto& p : parts) {
		if (!p.is_visible || !is_visible) continue;
		if (p.material->category == eRenderCategory::CATEGORY_PARTICLES || p.material->category == eRenderCategory::CATEGORY_PARTICLES_PERCEPTION)
			rm.addKey(h_me, p.mesh, p.material, CHandle(), p.mesh_group, p.mesh_instances_group, p.material->category == eRenderCategory::CATEGORY_PARTICLES,
				p.material->category == eRenderCategory::CATEGORY_PARTICLES_PERCEPTION);
	}
}

bool TCompRender::isStateActive(const std::string & stateName) const {
	if (hasState(stateName)){
		int state = name_state_to_id.at(stateName);
		return curr_state == state;
	}
	return false;
}

int TCompRender::getRenderHashCode() const {
	int hashCode = 0.0;
	for (auto & p : parts) {
		// Hashcode part.
		std::string hash = p.mesh->getName() + " - " + p.material->getName() + " - " + std::to_string(p.mesh_group);
		int32_t val = 0.0;
		MurmurHash3_x86_32((void *)hash.c_str(), hash.size(), 10, &val);
		hashCode += val;
	}
	return hashCode;
}

void TCompRender::setColor(const VEC4 & theColor) { color = theColor; }

void TCompRender::setCurrentState(int state) {
	TCompRenderBlending* blending = getComponent<TCompRenderBlending>();
	if (blending && blending->isActive() && state != name_state_to_id["blended"])
		return; //If the blending is active we don't want to override its state
	
	showMeshesWithState(state);
}

void TCompRender::setCurrentState(const std::string & stateName) {
	assert(hasState(stateName));
	int state = name_state_to_id[stateName];

	TCompRenderBlending* blending = getComponent<TCompRenderBlending>();
	if (blending && blending->isActive() && stateName != "blended")
		return; //If the blending is active we don't want to override its state
	
	showMeshesWithState(state);
}

void TCompRender::setActiveSecondaryState(int state, bool act) {
	assert(hasState(state));

	MeshPart & p = parts[state];
	p.is_visible = act;
	p.is_secondary = act;
}

void TCompRender::setActiveSecondaryState(const std::string & stateName, bool act) {
	assert(hasState(stateName));

	MeshPart & p = parts[name_state_to_id[stateName]];
	p.is_visible = act;
	p.is_secondary = act;
}