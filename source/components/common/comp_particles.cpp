#include "mcv_platform.h"
#include "comp_particles.h"
#include "entity/entity.h"
#include "entity/entity_parser.h"
#include "entity/common_msgs.h"
#include "particles/particle_emitter.h"
#include "particles/module_particles.h"
#include "engine.h"

DECL_OBJ_MANAGER("particles", TCompParticles);

TCompParticles::~TCompParticles()
{
	if (_system)
		Engine.getParticles().disableSystem(_system);
}

void TCompParticles::declareInLua() {
	EngineScripting.declareComponent<TCompParticles>(
		"TCompParticles",
		"setSystemActive", &TCompParticles::setSystemActive,
		"spawnParticles", &TCompParticles::spawnParticles,
		"toggleParticles", &TCompParticles::toggleParticles
		);
}

void TCompParticles::load(const json& j, TEntityParseContext& ctx)
{
	_emitter = EngineResources.getEditableResource(j.value("file", ""))->as<particles::TEmitter>();
	_isComponentActive = j.value("active", _isComponentActive);
	_isSystemActive = j.value("can_emit", _isSystemActive);
	_renderPriority = j.value("priority", _renderPriority);
	_offset = loadVector3(j, "offset");
	if (j.count("bone_to_follow") > 0) {
		_followBone = true;
		_boneToFollow = j["bone_to_follow"];
	}
}

void TCompParticles::registerMsgs()
{
	DECL_MSG(TCompParticles, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompParticles, TMsgToogleComponent, onToggleComponent);
}

void TCompParticles::debugInMenu()
{
	if (ImGui::Checkbox("Active", &active)) {
		toggleParticles();
	}
}

void TCompParticles::update(float dt) {}

void TCompParticles::toggleParticles() {
	if (active) {
		if (_system) 
			Engine.getParticles().disableSystem(_system);
		_system = Engine.getParticles().launchSystem(_emitter, CHandle(this).getOwner(), _offset);
		_system->setPriority(_renderPriority);
		if (_followBone)
			_system->followBone(_boneToFollow);
	}
	else {
		Engine.getParticles().disableSystem(_system);
	}
}

void TCompParticles::onEntityCreated(const TMsgEntityCreated& msg)
{
	_system = Engine.getParticles().launchSystem(_emitter, CHandle(this).getOwner(), _offset);
	_system->setPriority(_renderPriority);
	setActive(_isComponentActive);
	setSystemActive(_isSystemActive);
	if (_followBone)
		_system->followBone(_boneToFollow);
}

void TCompParticles::onToggleComponent(const TMsgToogleComponent& msg)
{
	active = msg.active;
	toggleParticles();
}

void TCompParticles::setSystemActive(bool state) {
	_isSystemActive = state;
	_system->setCanSpawnParticles(state);
}

void TCompParticles::setActive(bool active) {
	TCompBase::active = active;
	_isComponentActive = active;
	toggleParticles();
}

void TCompParticles::spawnParticles(int numParticles) {
	_system->forceEmit(numParticles);
}

