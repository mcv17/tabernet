#pragma once

#include "comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "PxPhysicsAPI.h"


struct TMsgEntityTriggerEnter {
  CHandle h_entity;
  DECL_MSG_ID();
};

struct TMsgEntityTriggerExit {
  CHandle h_entity;
  DECL_MSG_ID();
};

struct TMsgEntityCollisionEnter {
	CHandle h_entity;
	DECL_MSG_ID();
};

struct TMsgEntityCollisionStay {
	CHandle h_entity;
	DECL_MSG_ID();
};

struct TMsgEntityCollisionExit {
	CHandle h_entity;
	DECL_MSG_ID();
};

struct TMsgDeleteAndCreateController {
	DECL_MSG_ID();
};

namespace physx {
  class PxRigidActor;
}

class TCompCollider : public TCompBase {
public:
	DECL_SIBLING_ACCESS();
	
	json jconfig;
	~TCompCollider();
	
	physx::PxRigidActor *        actor = nullptr;
	physx::PxCapsuleController * controller = nullptr;
	CMesh *                      debug_mesh = nullptr;
	
	//Enemy controllers will use this a second time for the railgun and it will be specified true
	void load(const json& j, TEntityParseContext& ctx, bool isRailgun = false); 
	void debugInMenu();
	void renderDebug();
	void onEntityCreated(const TMsgEntityCreated& msg);
	
	typedef void (TCompCollider::*TShapeFn)(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world);
	void renderDebugShape(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world);
	void debugInMenuShape(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world);
	void activateShapes(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world);
	void disableShapes(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world);
	void onEachShape(TShapeFn fn);
	void changeColliderType(bool dynamic, bool kinematic, bool trigger);
	bool isRigidBody();

	static void registerMsgs();

	void destroyCollider();
private:
	void onToggleComponent(const TMsgToogleComponent & msg);
	void onDeleteAndCreateController(const TMsgDeleteAndCreateController & msg);

	VEC3 _prevPosition = VEC3::Zero;
	VEC3 _debugThrowDirOnExplosion = VEC3(0,0,0);
};