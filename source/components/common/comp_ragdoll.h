#ifndef INC_COMP_RAGDOLL_H_
#define INC_COMP_RAGDOLL_H_

#include "components/common/comp_base.h"
#include "PxPhysicsAPI.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "skeleton/skeleton_shape.h"

class CEntity;

struct TCompRagdoll : public TCompBase {
	static void declareInLua();


	std::string jsonStats;
	TSkeletonShapes ragdoll;
	bool _activated = false;
	bool updateRagdollFromSkel = false;
	float _initialForce = 1.0f;
	physx::PxCapsuleController* controller;
	physx::PxArticulation* articulation;

	// Flying ragdoll
	bool _isFlyingRagdool = false;
	Vector3 _flyingDestination = Vector3::Zero;
	float _flyingMagnitude = 0.0f;
	physx::PxArticulationLink* _linkToApplyForce = nullptr;

	// Railgun joint created
	bool _railgunJointJustCreated = false;
	float _railgunJointTime = 0.25f;
	CClock _railgunJointClock = CClock(_railgunJointTime);
	PxFixedJoint* _railgunJoint = nullptr;

	// Remove non polling entites after awhile
	CClock _deleteEntityClock = CClock(2.0f);
	// Dead ragdolls config
	std::string _deadConfigPath = "";
	bool _staticBody = true;

	void updateRagdollFromSkeleton();
	void updateSkeletonFromRagdoll();
	std::pair<std::string, physx::PxArticulationLink*> getBoneFromGlobalPos(VEC3 pos);
	void clearRagdollForces();
	void toggleRagdollGravity(bool active);
	//If wanting random force, pass phyx::PxVec3(0) and true.
	void activateRagdoll(physx::PxVec3 forceVec = physx::PxVec3(0), bool useForce = false);
	void deactivateRagdoll();
	void deleteArticulation();
	// Starts applying every frame a force in order to send the ragdoll flying by the railgun (Can only be used if ragdoll is active)
	void startFlyingRagdoll(Vector3 destination, float magnitude, physx::PxArticulationLink* link);
	// Stops applying the force every frame
	void stopFlyingRagdoll();
	// Updates the force every frame if flying ragdoll
	void updateFlyingRagdoll();
	// Updates every frame clearing the forces after the railgun joint is created
	void updatePostJointRagdoll();
	void updatePhysxBoneShape();
	std::string dumpRagdolls();
	std::string dumpDeadRagdolls();
	bool isActivated() { return _activated; }

	// To load the bones position and rotation and start dead
	void applyDeadConfiguration();

	void onCreated(const TMsgEntityCreated&);
	void onToggleComponent(const TMsgToogleComponent & msg);
	void onComponentReset(const TMsgResetComponent & msg);
	void onEntityDead(const TMsgEntityDead & msg);
	void onResurrectionMsg(const TMsgFullRestore & msg);

public:
	static void registerMsgs();

	~TCompRagdoll();

	void load(const json& j, TEntityParseContext& ctx);

	void update(float elapsed);
	void debugInMenu();
	void renderDebug();

	void setPxFixedJoint(PxFixedJoint* joint) { _railgunJoint = joint; }

	DECL_SIBLING_ACCESS();
};


#endif
