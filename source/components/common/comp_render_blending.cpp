#include "mcv_platform.h"
#include "comp_render_blending.h"
#include "components/powers/comp_perception.h"

#include "engine.h"

#include "components/common/comp_render.h"

DECL_OBJ_MANAGER("render_blending", TCompRenderBlending);

void TCompRenderBlending::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::DragFloat("Time for blending: ", &timeForFullBlending, 0.01f, 0.0f, 10.f);
	ImGui::DragFloat("Max blending: ", &maxBlending, 0.01f, 0.0f, 1.f);
}

void TCompRenderBlending::load(const json& j, TEntityParseContext& ctx) {
	timeForFullBlending = j.value("time_for_blending", timeForFullBlending);
	maxBlending = j.value("max_blending", maxBlending);
}

void TCompRenderBlending::update(float dt) {
	if (!active) return;

	TCompRender * render = renderComp;

	if(currentTime >= timeForFullBlending){
		currentTime = timeForFullBlending;
	}
	else
		currentTime += dt;
	
	Vector4 currentCol = render->color;
	render->color.w = std::max(1.0f - (currentTime/timeForFullBlending), maxBlending);
}

void TCompRenderBlending::registerMsgs() {
	DECL_MSG(TCompRenderBlending, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompRenderBlending, TMsgBlendEntity, onBlending);
	DECL_MSG(TCompRenderBlending, TMsgToogleComponent, onToggleComponent);
}

void TCompRenderBlending::onEntityCreated(const TMsgEntityCreated & msg) {
	renderComp = getComponent<TCompRender>();
	assert(renderComp.isValid());
}

void TCompRenderBlending::onBlending(const TMsgBlendEntity & msg) {
	active = msg.activate;
	if (active) {
		currentTime = 0.0f;
		TMsgSetMeshState msgRender = { 2 }; // 2 is used for transparent.
		CHandle(this).getOwner().sendMsg(msgRender);
	}
	else {
		TCompRender * render = renderComp;
		int state = 0;
		CEntity* player = getEntityByName("Player");
		if (player) {
			TCompPerception * perception = player->getComponent<TCompPerception>();
			if (perception && perception->isPerceptionActive())
				state = 1;
		}
		
		render->color.w = 1.0f;
		TMsgSetMeshState msgRender = { state }; // 0 is used for normal state.
		CHandle(this).getOwner().sendMsg(msgRender);
	}
}