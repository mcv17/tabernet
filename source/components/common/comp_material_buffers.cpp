#include "mcv_platform.h"
#include "comp_material_buffers.h"

#include "render/textures/material_buffers_factory.h"
#include "components/common/comp_render.h"

DECL_OBJ_MANAGER("material_buffers", TCompMaterialBuffers);

void TCompMaterialBuffers::registerMsgs() {
	DECL_MSG(TCompMaterialBuffers, TMsgResetComponent, onComponentReset);
	DECL_MSG(TCompMaterialBuffers, TMsgEntityCreated, onEntityCreated);
}

void TCompMaterialBuffers::onComponentReset(const TMsgResetComponent& msg)
{
	renderH = getComponent<TCompRender>();
	for (auto cte_buffer : constant_buffers_to_activate) 
		cte_buffer.constant_buffer->reset();
}

void TCompMaterialBuffers::onEntityCreated(const TMsgEntityCreated & msg) {
	renderH = getComponent<TCompRender>();

	CHandle entityHandle = CHandle(this);
	// Send the entity handle to constant buffers. Maybe one of them needs the entity for fetching handles or something.
	for (auto & constant : constant_buffers_to_activate)
		constant.constant_buffer->onEntityLoaded(entityHandle);
}

// Get sure to remove all materials.
TCompMaterialBuffers::~TCompMaterialBuffers() {
	for (auto & constant : constant_buffers_to_activate) {
		if (constant.constant_buffer)
			delete constant.constant_buffer;
	}
}

void TCompMaterialBuffers::load(const json& j, TEntityParseContext& ctx) {
	if (j.count("buffers") == 0) return;

	for (auto & mat : j["buffers"]) {
		std::string buffer_name = mat.value("buffer_name", "");
		if (mat.count("buffer_data") == 0) continue;
		
		uint32_t activate_on_mesh_state = mat.value("activate_on_mesh_state", -1);
		MaterialConstantBuffers * p = CMaterialBuffersFactory::Instance().create(buffer_name, mat["buffer_data"]);
		ConstantBufferToActivate buffer = { activate_on_mesh_state , p };
		if (p) {
			uint32_t slotToCheck = p->getCtBuffer()->getSlot();
			// Get sure no two materials share same constants.
			for (auto & cte : constant_buffers_to_activate)
				if (cte.constant_buffer->getCtBuffer()->getSlot() == slotToCheck && (cte.activate_on_mesh_state != -1 && cte.activate_on_mesh_state == slotToCheck))
					Utils::fatal("Attention, two constant buffers are sharing the same slot. This will give conflicts.");

			constant_buffers_to_activate.push_back(buffer);
		}
	}
}

void TCompMaterialBuffers::debugInMenu() {
	for (auto & constant : constant_buffers_to_activate) {
		ImGui::Spacing(0, 5);
		ImGui::Separator();
		ImGui::Spacing(0, 5);
		ImGui::Text("Constant Buffer Mesh State To Activate In: %f", constant.activate_on_mesh_state);
		constant.constant_buffer->debugInMenu();
		ImGui::Spacing(0, 5);
		ImGui::Separator();
		ImGui::Spacing(0, 5);
	}
}

void TCompMaterialBuffers::update(float dt) {
	TCompRender * render = renderH;
	if (!render) return;
	
	// Get the current_active_state.
	current_active_state = render->getCurrentState();

	// Update all constant buffer for the given entity that have no state or are of the current_active_state.
	for (auto & constant : constant_buffers_to_activate) {
		if(constant.activate_on_mesh_state == -1 || constant.activate_on_mesh_state == current_active_state)
			constant.constant_buffer->update(dt);
	}
}

void TCompMaterialBuffers::activate(){
	for (auto & constant : constant_buffers_to_activate) {
		if(constant.activate_on_mesh_state == -1 || constant.activate_on_mesh_state == current_active_state)
			constant.constant_buffer->activate();
	}
}
