#include "mcv_platform.h"
#include "comp_health.h"
#include "comp_transform.h"
#include "comp_name.h"
#include "components/controllers/comp_character_controller.h"
#include "utils/phys_utils.h"
#include "components/powers/comp_perception.h"
#include "comp_aabb.h"

using namespace physx;
using namespace UI;

DECL_OBJ_MANAGER("health_component", TCompHealth);

TCompHealth::TCompHealth() {
	damageOverlayMaxOpacity = 0.7f;
	damageOverlayLifetime = 2.0f;
	damageOverlayLifetimeThreshold = 0.5f;
	damageOverlayRecoverSpeed = 1.0f;
}

void TCompHealth::load(const json & j, TEntityParseContext& ctx) {
	maxHealth = j.value("max_health", maxHealth);
	currentMaxHealth = j.value("current_max_health", maxHealth);
	currentHealth = std::min(j.value("current_health", currentHealth), maxHealth);
	immortal = j.value("immortal", immortal);
	hurtSoundEvent = j.value("hurt_sound_event", hurtSoundEvent);
	hurtSoundChance = j.value("hurt_sound_chance", hurtSoundChance);
	if (j.count("hurt_sound_cooldown") > 0)
		hurtSoundCooldownTimer.setTime(j["hurt_sound_cooldown"]);
	deathSoundEvent = j.value("death_sound_event", deathSoundEvent);
	playerDeathMusic = j.value("player_death_music", playerDeathMusic);
	if (currentMaxHealth < currentHealth)
		currentHealth = currentMaxHealth;
}

void TCompHealth::debugInMenu() {
	TCompBase::debugInMenu();

	bool modified = ImGui::DragFloat("Max Health: ", &maxHealth, 1.0f, 0.0f, 100000.f);
	currentMaxHealth = std::min(currentMaxHealth, maxHealth);

	modified |= ImGui::DragFloat("Current max health: ", &currentMaxHealth, 1.0f, 0.0f, 100000.f);
	maxHealth = std::max(maxHealth, currentMaxHealth);

	currentHealth = std::min(currentHealth, currentMaxHealth);
	modified |= ImGui::DragFloat("Current Health: ", &currentHealth, 1.0f, 0.0f, currentMaxHealth);
	
	ImGui::DragFloat("Current Armour: ", &armour, 1.0f, 0.25f, 2.0f);

	ImGui::Checkbox("Is immortal: ", &immortal);
	ImGui::Checkbox("Is dead: ", &dead);

	ImGui::DragFloat("##addDmgDbg", &giveDebugDamage, 0.01f, 0.0f, 600.0f, "%.2f");
	ImGui::SameLine();
	if (ImGui::Button("Add damage", ImVec2(100, 0))) {
		addDamage(giveDebugDamage);
	}
	ImGui::DragFloat("##addDbgHP", &addDebugHP, 0.01f, 0.0f, 600.0f, "%.2f");
	ImGui::SameLine();
	if (ImGui::Button("Add HP", ImVec2(100, 0))) {
		addHealth(addDebugHP);
	}
	ImGui::DragFloat("##addDbgRatio", &lowerDamageRatioPerSecond, 0.001f, 0.0f, 600.0f, "%.2f");

	if(modified && !immortal)
		checkIfDead();
}

void TCompHealth::registerMsgs() {
	DECL_MSG(TCompHealth, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompHealth, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompHealth, TMsgHitDamage, onHealthLoweredHit);
	DECL_MSG(TCompHealth, TMsgBulletDamage, onHealthLoweredBullet);
	DECL_MSG(TCompHealth, TMsgNailDamage, onHealthLoweredNail);
	DECL_MSG(TCompHealth, TMsgExplosion, onHealthLoweredExplosion);
	DECL_MSG(TCompHealth, TMsgHealing, onHealthHealed);
	DECL_MSG(TCompHealth, TMsgFullRestore, onHealthFullyRestored);
	DECL_MSG(TCompHealth, TMsgResetComponent, onComponentReset);
}

void TCompHealth::declareInLua() {
	EngineScripting.declareComponent<TCompHealth>
		(
			// name in LUA
			"TCompHealth",
			// methods
			"getCurrentHealth", &TCompHealth::getCurrentHealth,
			"setCurrentHealth", &TCompHealth::setCurrentHealth,
			"addHealth", &TCompHealth::addHealth,
			"addDamage", &TCompHealth::addDamage,				
			"setArmour", &TCompHealth::setArmour,				
			"isDead",	&TCompHealth::isDead,
			// attributes
			"immortal",	&TCompHealth::immortal
		);
}


void TCompHealth::onEntityCreated(const TMsgEntityCreated & msg) {
	characterControllerH = getComponent<TCompCharacterController>();
	hitboxesH = getComponent<TCompHitboxes>();
	transformHandle = getComponent<TCompTransform>();

	hurtSoundCooldownTimer.start();

	getHPBarWidget();
	getDamageOverlayWidget();
}

void TCompHealth::getHPBarWidget()
{
	// UI
	if (CEntity* e = getEntityByName("Player")) {
		if (getEntity() == (CHandle)e) {
			isPlayer = true;
			UI::CWidget hpWidget;
			if (EngineUI.findWidgetByName(hpWidget, "hp")) {
				for (auto& child : hpWidget.getChildren()) {
					if (UI::CImage* imageChild = dynamic_cast<UI::CImage*>(child)) {
						if (imageChild->getName() == "bar_hp") {
							hp_bar = imageChild;
						}
						else if (imageChild->getName() == "bar_hp_damage") {
							hp_bar_damage = imageChild;
						}
					}
				}
			}
		}
	}
}

bool TCompHealth::getDamageOverlayWidget() {
	if (!damageOverlay) {
		damageOverlay = EngineUI.getImageWidgetByName("damage_overlay", "damage_overlay_image");
		return (damageOverlay != nullptr);
	}
	else 
		return true;
}

void TCompHealth::resetDamageOverlay() {
	damageOverlayLifetimeClock.setTime(0.0f);
	if (getDamageOverlayWidget()) {
		float& overlayAlpha = damageOverlay->getImageParams()->color.w;
		overlayAlpha = 0.0f;
	}
}

void TCompHealth::onHealthLoweredHit(const TMsgHitDamage & msg){
	addDamage(msg.damage, msg.damageDirection, msg.damageForce);
}

void TCompHealth::onHealthLoweredBullet(const TMsgBulletDamage & msg){
	TCompHitboxes * c_hitboxes = hitboxesH;
	if (LogicManager::Instance().isPerceptionActive() && c_hitboxes && c_hitboxes->isCriticalHitbox(msg.actorHit)) {
		LogicManager::Instance().SpawnCriticalHitboxFXOnHit(msg.hitPos, msg.hitNormal);
		isCriticalHit = true;
		addDamage(msg.damage * 2, msg.damageDirection, msg.damageForce);
	}
	else
		addDamage(msg.damage, msg.damageDirection, msg.damageForce);
}

void TCompHealth::onHealthLoweredNail(const TMsgNailDamage & msg){
	TCompCharacterController * eController = characterControllerH;
	if (eController)
		eController->railgunThrow(msg);
	
	TCompHitboxes* c_hitboxes = hitboxesH;
	if (LogicManager::Instance().isPerceptionActive() && c_hitboxes && c_hitboxes->isCriticalHitbox(msg.actorHit)) {
		LogicManager::Instance().SpawnCriticalHitboxFXOnHit(msg.hitPos, msg.hitNormal);
		addDamage(msg.damage * 2, msg.damageDirection, msg.damageForce);
		isCriticalHit = true;

	}
	else
		addDamage(msg.damage, msg.damageDirection, msg.damageForce);
}

void TCompHealth::onHealthLoweredExplosion(const TMsgExplosion& msg)
{
	addDamage(msg.dmg, msg.throwDir, msg.throwForce);
}

void TCompHealth::onHealthHealed(const TMsgHealing & msg){
	currentHealth += msg.healedPoints;
	if (currentHealth > currentMaxHealth)
		currentHealth = currentMaxHealth;

	targetUIRatio = currentHealth/maxHealth;
	currentUIRatio = currentHealth / maxHealth;
	startingRatio = 0.0f;
}

void TCompHealth::onHealthFullyRestored(const TMsgFullRestore & msg) {
	setCurrentMaxHealth(maxHealth);
	currentHealth = maxHealth;
	targetUIRatio = 1.0f;
	currentUIRatio = 1.0f;
	startingRatio = 0.0f;
	dead = false;
	if (isPlayer)
		resetDamageOverlay();
}

void TCompHealth::onComponentReset(const TMsgResetComponent & msg) {
	setCurrentMaxHealth(maxHealth);
	currentHealth = maxHealth;
	armour = 1.0f;
	dead = false;
	targetUIRatio = 1.0f;
	currentUIRatio = 1.0f;
	startingRatio = 0.0f;
	clearTickDamage();
	if (isPlayer)
		resetDamageOverlay();
}

void TCompHealth::addHealth(float healthToAdd) {
	currentHealth = std::min(currentHealth + healthToAdd, maxHealth);
}

void TCompHealth::addDamage(float damage, VEC3 hitDirection, float hitForce) {
	float deltaDamage = addRawDamage(damage / armour, hitDirection, hitForce);

	// Spawn Hit crosshair on enemies and if they re not dead
	if (!isPlayer && currentHealth > 0) {
		if (isCriticalHit)
			EngineUI.setChildWidgetVisibleByTime("crosshair_hit_critical_w", "hud", true, .2f);
		else
			EngineUI.setChildWidgetVisibleByTime("crosshair_hit_w", "hud", true, .2f);
	}
	if (isPlayer) {
		float deltaDamagePct = deltaDamage / maxHealth;
		if (damageOverlayLifetime > damageOverlayLifetimeClock.initialTime())
			damageOverlayLifetimeClock.setTime(damageOverlayLifetime);
		damageOverlayLifetimeClock.start();

		if (getDamageOverlayWidget()) {
			auto& interpolator = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda("quad_out");
			float& overlayAlpha = damageOverlay->getImageParams()->color.w;
			float ratio = std::min(1.0f, overlayAlpha + deltaDamagePct / damageOverlayLifetimeThreshold);
			ratio = std::max(overlayAlpha * damageOverlayMaxOpacity, ratio);
			overlayAlpha = interpolator(0.0f, damageOverlayMaxOpacity, ratio);
		}
	}

	// Reset critical flag
	isCriticalHit = false;
}

float TCompHealth::addRawDamage(float damage, VEC3 hitDirection, float hitForce) {
	TCompTransform* trans = transformHandle;
	float deltaDamage = currentHealth;

	if (immortal) {
		damage = 0;
		isCriticalHit = false;
	}
	else {
		if (!hurtSoundEvent.empty() && hurtSoundCooldownTimer.isFinished() && RNG.b(hurtSoundChance)) {
			EngineAudio.playEvent(hurtSoundEvent, trans->getPosition());
			hurtSoundCooldownTimer.start();
		}
	}
	if(isPlayer) updateUIWidget(damage);
	currentHealth -= damage;

	// Get position for floating damage, also check if not dead (quick hack)
	if (damage >= 1.0f || immortal && currentHealth - damage > 0) {
		TMsgFloatingDamageText msg;
		msg.damage = damage;
		msg.isCritical = isCriticalHit;
		msg.targetLocation = trans->getPosition();
		if (CEntity * ent = CHandle(this).getOwner())
			ent->sendMsg(msg);
	}
	else {
		damageBuffer += damage;
	}
	
	checkIfDead(hitDirection, hitForce);
	deltaDamage -= currentHealth;
	return deltaDamage;
}

void TCompHealth::checkIfDead(VEC3 hitDirection, float hitForce) {
	if (currentHealth <= 0 && !dead) {
		currentHealth = 0;
		dead = true;
		TMsgEntityDead msg;
		CHandle entHand = CHandle(this).getOwner();
		CEntity * ent = entHand;
		msg.hitDirection = hitDirection;
		msg.hitForce = hitForce;
		ent->sendMsg(msg);
		if (CEntity* entity = getEntity()) {
			if (entity == getEntityByName("Player")) {
				EngineAudio.stopAllSounds();

				EngineAudio.playEvent(playerDeathMusic);
			}
		}
		if (!deathSoundEvent.empty()) {
			TCompTransform * transform = transformHandle;
			EngineAudio.playEvent(deathSoundEvent, transform->getPosition());
		}

	}
}

void TCompHealth::updateUIWidget(float damageApplied, float dt)
{
	if (!hp_bar_damage)	getHPBarWidget();
	if (!hp_bar_damage) return;

	float barXscale = hp_bar_damage->getParams()->scale.x;
	
	if (damageApplied != 0.0f && barXscale == targetUIRatio) {
		waitClockUI.stop();
		waitClockUI.start();
	}

	targetUIRatio = (currentHealth - damageApplied) / currentMaxHealth;
	hp_bar->getParams()->scale.x = targetUIRatio;
	hp_bar->getImageParams()->maxUV.x = hp_bar->getImageParams()->minUV.x + targetUIRatio;

	if (waitClockUI.isFinished()) {
		if (barXscale > targetUIRatio && dt != 0.0f) {
			if (barXscale - (lowerDamageRatioPerSecond * dt) <= targetUIRatio)
				hp_bar_damage->getParams()->scale.x = targetUIRatio;
			else
				hp_bar_damage->getParams()->scale.x -= lowerDamageRatioPerSecond * dt;
		}
	}

	// Update damage blood overlay as well
	if (getDamageOverlayWidget()) {
		float& overlayAlpha = damageOverlay->getImageParams()->color.w;
		if (damageOverlayLifetimeClock.isFinished())
			overlayAlpha = std::max(0.0f, overlayAlpha - dt * damageOverlayRecoverSpeed);
	}
}

void TCompHealth::setArmour(float a) {
	armour = a;
}

void TCompHealth::setTickDamage(float tickDmg, float tickDur, int tickCt) {
	tickDamage = tickDmg;
	tickDuration = tickDur;
	timeNextTick = tickDuration;
	tickCount = tickCt;
}

void TCompHealth::update(float dt) {
	//Check for tick damage
	if (tickCount != 0) {
		timeNextTick -= dt;
		//If next tick ready
		if (timeNextTick <= 0.0f) {
			addDamage(tickDamage);
			tickCount--;
			if (tickCount == 0)
				clearTickDamage();
			timeNextTick = tickDuration;
		}
	}

	// If it isn't the player we don't need to update the widget
	if (isPlayer) {
		updateUIWidget(0.0f, dt); // Progressive UI damage
	}
	else {
		// Show damage in buffer only when it has reached 1.0
		if (damageBuffer >= 1.0f) {
			TCompTransform * transform = transformHandle;
			if (TCompLocalAABB* aabb = getComponent<TCompLocalAABB>()) {
				TMsgFloatingDamageText msg;
				msg.damage = 1.0f;
				msg.isCritical = false;
				VEC3 position = transform->getPosition();
				position.y += aabb->Extents.y;
				msg.targetLocation = position;
				if (CEntity* ent = CHandle(this).getOwner())
					ent->sendMsg(msg);
				damageBuffer = 0.0f;
			}
		}
	}
}