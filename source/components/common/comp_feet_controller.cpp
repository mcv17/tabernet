#include "mcv_platform.h"
#include "comp_feet_controller.h"

#include "comp_script.h"
#include "skeleton/comp_skeleton.h"
#include "cal3d/cal3d.h"
#include "skeleton/cal3d2engine.h"
#include "utils/phys_utils.h"

#include "logic/logic_manager.h"
#include "components/ai/behaviour_tree/comp_bt_enemy_melee.h"

DECL_OBJ_MANAGER("feet_controller", TCompFeetController);

float TCompFeetController::max_squared_distance_to_play_effects = 625; // 625 is 25 meters squared. We store the squared version because it's faster.

// Saves any information needed.
void TCompFeetController::TFootControl::loadBone(const json & boneData) {
	footBoneName = boneData.value("foot_bone_name", "");
	rayDistance = boneData.value("ray_distance", rayDistance);
	downVector = loadVector3(boneData, "downwards_direction");
}

// Called when the entity is fully created. Fetches necessary information.
void TCompFeetController::TFootControl::initBone(CHandle h_skeleton) {
	TCompSkeleton * skeleton = h_skeleton;
	bone_id = skeleton->getGameCoreSkeleton()->getBoneId(footBoneName);
}

void TCompFeetController::TFootControl::callOnTouchingGroundFunc(TCompScript * script, Vector3 & touchingPos, Vector3 & touchingNormal) {
	script->executeFunction("onTouchingGround", touchingPos, touchingNormal);
}

void TCompFeetController::TFootControl::callOnLeavingGroundFunc(TCompScript * script, Vector3 & leavingPos, Vector3 & leavingNormal) {
	script->executeFunction("onLeavingGround", leavingPos, leavingNormal);
}

void TCompFeetController::load(const json& j, TEntityParseContext& ctx) {
	// Load all bones.
	if (j.count("bones") && j["bones"].is_array()) {
		for (auto & bone : j["bones"]) {
			feet.push_back(TFootControl());
			TFootControl & f = feet.back();
			f.loadBone(bone);
		}
	}

	_footstepsSoundEvent = j.value("footsteps_sound_event", _footstepsSoundEvent);
	_quadFootstepsSoundEvent = j.value("quad_footsteps_sound_event", _quadFootstepsSoundEvent);
}

void TCompFeetController::renderDebug() {
	TCompSkeleton * comp_skeleton = h_skeleton;
	TCompScript * comp_script = h_script;
	if (!comp_skeleton || !comp_script) return;

	CalModel * model = comp_skeleton->getModel();
	CalSkeleton * skel = model->getSkeleton();

	// Draw only rays.
	for (auto & foot : feet) {
		CalBone * bone = skel->getBone(foot.bone_id);

		// Get the vector pointing down based on the bone.
		CalQuaternion rot_abs = bone->getRotationAbsolute();
		CalVector rayDir = DX2Cal(foot.downVector);
		rayDir *= rot_abs;

		drawLine(Cal2DX(bone->getTranslationAbsolute()), Cal2DX(bone->getTranslationAbsolute()) + Cal2DX(rayDir) * foot.rayDistance, Vector4(1,1,0,1));
	}
}

void TCompFeetController::update(float dt) {
	TCompSkeleton * comp_skeleton = h_skeleton;
	TCompScript * comp_script = h_script;
	TCompTransform * transform = h_transform;
	if (!comp_skeleton || !comp_script || !transform) return;

	if (!LogicManager::Instance().isPlayerClose(transform->getPosition(), max_squared_distance_to_play_effects)) return;

	CalModel * model = comp_skeleton->getModel();
	CalSkeleton * skel = model->getSkeleton();

	// Check if they have collided or not.
	for (auto & foot : feet) {
		CalBone * bone = skel->getBone(foot.bone_id);

		// Get the vector pointing down based on the bone.
		CalQuaternion rot_abs = bone->getRotationAbsolute();
		CalVector rayDir = DX2Cal(foot.downVector);
		rayDir *= rot_abs;

		// Shoot the ray from the feet downwards.
		physx::PxRaycastBuffer hitData = PhysUtils::shootFootRaycast(Cal2DX(bone->getTranslationAbsolute()), Cal2DX(rayDir), foot.rayDistance);

		// We hit something then
		if (hitData.hasBlock) {
			// If we just touched the ground.
			if (foot.checkJustTouchingTheGround(true)) {
				Vector3 position = PXVEC3_TO_VEC3(hitData.block.position);
				foot.callOnTouchingGroundFunc(h_script, position, PXVEC3_TO_VEC3(hitData.block.normal));
				if (!_footstepsSoundEvent.empty())
					EngineAudio.playEvent(_isQuad ? _quadFootstepsSoundEvent:_footstepsSoundEvent, position);
			}
		}
		else {
			// If we are just stopping touching.
			if (foot.checkJustLeavingTheGround(false))
				foot.callOnLeavingGroundFunc(h_script, PXVEC3_TO_VEC3(hitData.block.position), PXVEC3_TO_VEC3(hitData.block.normal));
		}

		foot.initBone(h_skeleton);
	}

	if (!h_bt.isValid()) {
		h_bt = getComponent<TCompBTEnemyMelee>();
		if (h_bt.isValid()) {
			TCompBTEnemyMelee* bt = h_bt;
			_isQuad = bt->isQuadruped();
		}
	}
}

void TCompFeetController::registerMsgs() {
	DECL_MSG(TCompFeetController, TMsgEntityCreated, onEntityCreated);
}

void TCompFeetController::onEntityCreated(const TMsgEntityCreated & msg) {
	h_script = getComponent<TCompScript>();
	h_skeleton = getComponent<TCompSkeleton>();
	h_transform = getComponent<TCompTransform>();
	h_bt = getComponent<TCompBTEnemyMelee>();

	if (h_bt.isValid()) {
		TCompBTEnemyMelee* bt = h_bt;
		_isQuad = bt->isQuadruped();
	}

	if (!h_skeleton.isValid()) return;
	for (auto & foot : feet)
		foot.initBone(h_skeleton);
}