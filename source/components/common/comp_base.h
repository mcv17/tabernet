#ifndef INC_COMP_BASE_H_
#define INC_COMP_BASE_H_

struct TEntityParseContext;
struct TMsgToogleComponent;

/* A base component from which all components must derive. */
struct TCompBase {
protected:
	bool active = true;

public:
	void debugInMenu() {
		ImGui::Checkbox("Is Active: ", &active);
	}
	
	void renderDebug() {}
	
	void load( const json& j, TEntityParseContext& ctx ) {}
	
	void update(float dt) {}
	
	static void registerMsgs() {}

	static void declareInLua() {}

	void setActive(bool nActive) { active = nActive; }
	bool isActive() const { return active; }

	template< class TComp, class TMsg > void sendMsgToComp(const TMsg & msg);

protected:
	void onToggleComponent(const TMsgToogleComponent & msg);
};

#include "entity/entity.h"
#include "entity/base_callback.h"

// I need to add TComp as parameter as I don't know another way of making it work otherwise.
// A call to this function gets the pointer upcasted to base class.
template< class TComp, class TMsg>
void TCompBase::sendMsgToComp(const TMsg & msg) {
	CHandle comp = getObjectManager<TComp>()->getHandleFromAddr((TComp*)this);

	// Get the component type.
	uint32_t componentType = comp.getType();

	// Now, get all components registered to the msg. Send it to the component
	// that is indicated.
	auto rangeCompsSubsToMsg = all_registered_msgs.equal_range(TMsg::getMsgID());
	while (rangeCompsSubsToMsg.first != rangeCompsSubsToMsg.second) {
		const auto & slot = rangeCompsSubsToMsg.first->second;
		if (slot.comp_type == componentType) {
			slot.callback->sendMsg(comp, &msg);
			return;
		}
		++rangeCompsSubsToMsg.first;
	}
	assert(!"Component is not subscribed to message.");
}

// Add this macro inside each derived class so components can call
// the method for easy access to their handle and their entity handle.
#define DECL_SIBLING_ACCESS()   \
  CHandle getEntity() {                            \
    CEntity * e = CHandle(this).getOwner();   \
    if (!e)                                  \
      return CHandle();                      \
    return CHandle(e);                  \
  } \
	\
  template< typename TComp >    \
  CHandle getComponent() {                            \
    CEntity* e = CHandle(this).getOwner();   \
    if (!e)                                  \
      return CHandle();                      \
    return e->getComponent<TComp>();                  \
  }

/**
	Declares a handle for a component and an getter function that checks 
	if both the entity or the component still exist

		entity_name: name of the entity in const char * form
		component_type: type of the component to retrieve
		component_decl_name: name for the handle and the access function
			if component_decl_name is MyComponent, handle will be declared as
			_h_MyComponent and getter will be declared as getMyComponent

	Execution errors:
		- when entity_name is not an rvalue (i.e. it's a variable or a function return)
		it may cause unintended problems if the value provided at that time is not
		initialized or if the entity doesn't exist
*/
#define DECL_TCOMP_ACCESS(entity_name, component_type, component_decl_name)   \
	CHandle _h_ ## component_decl_name; \
	inline component_type * get ## component_decl_name() { \
		if (!_h_ ## component_decl_name.isValid()) { \
			CEntity* e = getEntityByName(entity_name); \
			if (!e) return nullptr; \
			return e->getComponent<component_type>(); \
		} \
		return _h_ ## component_decl_name; \
	}

#endif
