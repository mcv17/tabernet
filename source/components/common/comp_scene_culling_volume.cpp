#include "mcv_platform.h"
#include "comp_scene_culling_volume.h"

#include "engine.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

#include "comp_tags.h"
#include "components/common/comp_aabb.h"
#include "components/common/comp_group.h"
#include "comp_name.h"

DECL_OBJ_MANAGER("scene_culling_volume", TCompSceneCullingVolume);

void TCompSceneCullingVolume::load(const json & j, TEntityParseContext& ctx){
	// Rendering volumes.
	const json & renderingNeighbors = j["culling_volume_rendering_neighbors"];
	rendering_volumes_names.reserve(renderingNeighbors.size());
	for (auto & volumeName : renderingNeighbors)
		rendering_volumes_names.push_back(volumeName);

	// Logic volumes.
	auto & logicNeighbors = j["culling_volume_logic_neighbors"];
	logic_volumes_names.reserve(logicNeighbors.size());
	for (auto & volumeName : logicNeighbors)
		logic_volumes_names.push_back(volumeName);

	StartingVolume = j.value("starting_volume", false);

	SceneCullingBox.Center = loadVector3("center");
	SceneCullingBox.Extents = loadVector3("half_size");
}

void TCompSceneCullingVolume::debugInMenu(){
	ImGui::TextColored(ImVec4(1, 0, 1, 1), "Rendering volumes");
	for (auto & rendering_name : rendering_volumes_names)
		ImGui::Text(rendering_name.c_str());

	ImGui::TextColored(ImVec4(1, 0, 1, 1), "Logic volumes");
	for (auto & logic_name : logic_volumes_names)
		ImGui::Text(logic_name.c_str());
}

void TCompSceneCullingVolume::registerMsgs() {
	DECL_MSG(TCompSceneCullingVolume, TMsgEntitiesGroupCreated, onSceneCullingCreated);
	DECL_MSG(TCompSceneCullingVolume, TMsgSceneCreated, onSceneLoaded);
	DECL_MSG(TCompSceneCullingVolume, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompSceneCullingVolume, TMsgEntityTriggerExit, onTriggerExit);
}

void TCompSceneCullingVolume::declareInLua() {
	EngineScripting.declareComponent<TCompSceneCullingVolume>
	(
		"TCompSceneCullingVolume",
		// attribute that points to a getter, can also point to a setter (second argument in property)
		"RemoveDynamicEntitiesInVolume", &TCompSceneCullingVolume::RemoveDynamicEntitiesInVolume
	);
}

void TCompSceneCullingVolume::onSceneCullingCreated(const TMsgEntitiesGroupCreated & msg) {
	group_handle = getComponent<TCompGroup>();
}

void TCompSceneCullingVolume::onSceneLoaded(const TMsgSceneCreated & msg) {
	// Fetch rendering volumes.
	rendering_volumes.reserve(rendering_volumes_names.size());
	for (auto & volumeName : rendering_volumes_names) {
		CHandle entity_handle = getEntityByName(volumeName);
		if (!entity_handle.isValid()) continue;
		CEntity * entity = entity_handle;
		CHandle handle = entity->getComponent<TCompSceneCullingVolume>();
		if (!handle.isValid()) continue;
		rendering_volumes.push_back(handle);
	}

	// Fetch logic volumes.
	logic_volumes.reserve(rendering_volumes_names.size());
	for (auto & volumeName : logic_volumes_names) {
		CHandle entity_handle = getEntityByName(volumeName);
		if (!entity_handle.isValid()) continue;
		CEntity * entity = entity_handle;
		CHandle handle = entity->getComponent<TCompSceneCullingVolume>();
		if (!handle.isValid()) continue;
		logic_volumes.push_back(handle);
	}


	/* Start by setting the volume to active. Only the volume where the player
	is will get activated (and all its neighbors). */
	active = StartingVolume ? true : false;
	setRenderingForEntitiesInVolume(active);
	setLogicForEntitiesInVolume(active);
	if (StartingVolume) {
		setRenderingNeighborsVolumesStatus();
		setLogicNeighborsVolumesStatus();
	}
}

void TCompSceneCullingVolume::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	CEntity * entity = msg.h_entity;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags && tags->hasTag(Utils::getID("player"))) {
		if (StartingVolume)
			StartingVolume = false;
		else {
			active = true;
			/* Set neighbors and this volume status */
			setRenderingForEntitiesInVolume(active);
			setLogicForEntitiesInVolume(active);
			setRenderingNeighborsVolumesStatus();
			setLogicNeighborsVolumesStatus();
		}
	}
	else
		addDynamicEntityAABB(entity);
}

void TCompSceneCullingVolume::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	CEntity * entity = msg.h_entity;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags && tags->hasTag(Utils::getID("player"))) {
		active = false;

		/* Set neighbors and this volume status */
		setRenderingForEntitiesInVolume(active);
		setLogicForEntitiesInVolume(active);
		setRenderingNeighborsVolumesStatus();
		setLogicNeighborsVolumesStatus();
	}
	else
		removeDynamicEntityAABB(entity);
}

void TCompSceneCullingVolume::setRenderingNeighborsVolumesStatus() {
	for (auto h : rendering_volumes) {
		TCompSceneCullingVolume * comp = h;
		comp->setRenderingForEntitiesInVolume(active);
	}
}

void TCompSceneCullingVolume::setLogicNeighborsVolumesStatus() {
	for (auto h : logic_volumes) {
		TCompSceneCullingVolume * comp = h;
		comp->setLogicForEntitiesInVolume(active);
	}
}

void TCompSceneCullingVolume::setRenderingForEntitiesInVolume(bool status){
	rendering_active = status ? rendering_active + 1 : rendering_active - 1;
	if (rendering_active > 1) return;

	// Group component contains all entities inside the culling volume on creation.
	// In our engine, we only add static entities by default.
	TCompGroup * groupC = group_handle;
	if (!groupC) return;
	TMsgRenderingStatus msg = { status };
	groupC->sendMsgToEntitiesInGroup<TMsgRenderingStatus>(msg);

	// We don't render dynamics here, we will render them only when logic is also active.
}

void TCompSceneCullingVolume::setLogicForEntitiesInVolume(bool status){
	logic_active = status ? logic_active + 1 : logic_active - 1;
	if (logic_active > 1) return;
	
	TCompGroup * groupC = group_handle;
	if (!groupC) return;
	TMsgLogicStatus msg = { status };
	groupC->sendMsgToEntitiesInGroup<TMsgLogicStatus>(msg);

	for (auto & entity : dynamic_entities_in_volume) {
		TMsgRenderingStatus msg = { status };
		TMsgLogicStatus msg2 = { status };
		CHandle entityH;
		entityH.fromUnsigned(entity);
		entityH.sendMsg<TMsgRenderingStatus>(msg);
		entityH.sendMsg<TMsgLogicStatus>(msg2);
	}
}

void TCompSceneCullingVolume::RemoveDynamicEntitiesInVolume() {
	for (auto & entity : dynamic_entities_in_volume) {
		CHandle entityH;
		entityH.fromUnsigned(entity);
		CEntity * ent = entityH;
		if (!ent) continue;

		// Don't remove if player.
		TCompTags * tags = ent->getComponent<TCompTags>();
		if (!tags) continue;
		if (tags->hasTag("player")) continue;

		// Don't remove if not inside. (This is to prevent removing handles reused by pooling).
		TCompTransform * transform = ent->getComponent<TCompTransform>();
		if (!transform) continue;
		if (!SceneCullingBox.Contains(transform->getPosition())) continue;

		LogicManager::Instance().smartRemoveEntity(entityH);
	}
	dynamic_entities_in_volume.empty();
	dynamic_entities_abbs_in_volume.empty();
}

// If it enters into a volume is inactive, it will deactivate itself.
// If there's a overlap between an active an inactive volume, it deactivates.
// We also did a version that allowed the entity to keep moving by having a static map,
// with the entity and a counter similar to the two variables for logic and rendering
// for each culling volume.
// Each entity had in how many active volumes it was. When 0, it deactivated, when in 1, it activated.
// The final solution was a bit more clunky. This version is more clear and we shouldn't be having so
// tight culling volumes for this extra functionality to matter at all.
// You can find the tightest version in an extra folder, named old_culling_volume.
void TCompSceneCullingVolume::sendMessageOnEntityEnter(CHandle entityH) {
	TMsgRenderingStatus msg = { logic_active };
	TMsgLogicStatus msg2 = { logic_active };
	entityH.sendMsg<TMsgRenderingStatus>(msg);
	entityH.sendMsg<TMsgLogicStatus>(msg2);
}

void TCompSceneCullingVolume::addDynamicEntityAABB(CEntity * dynamic_entity){
	/* Mark the entity as active in this volume if the volume is active. */
	CHandle entityH = dynamic_entity;
	sendMessageOnEntityEnter(entityH);
	
	/* Insert the entity aabb in the culling volume. */
	CHandle aabbs = dynamic_entity->getComponent<TCompAbsAABB>();
	assert(aabbs.isValid());
	if (aabbs.isValid()) {
		dynamic_entities_in_volume.insert(entityH.asUnsigned());
		dynamic_entities_abbs_in_volume.insert(aabbs.asUnsigned());
	}
}

void TCompSceneCullingVolume::removeDynamicEntityAABB(CEntity * dynamic_entity){
	/* Mark the entity as active in this volume. */
	CHandle entityH = dynamic_entity;

	/* Insert the entity aabb in the culling volume. */
	CHandle aabbs = dynamic_entity->getComponent<TCompAbsAABB>();
	assert(aabbs.isValid());
	if (aabbs.isValid()) {
		dynamic_entities_abbs_in_volume.erase(entityH.asUnsigned());
		dynamic_entities_abbs_in_volume.erase(aabbs.asUnsigned());
	}
}

const std::vector<CHandle> & TCompSceneCullingVolume::getStaticEntitiesInsideVolume() const {
	TCompGroup * groupC = group_handle;
	return groupC->getEntitiesInsideGroup();
}