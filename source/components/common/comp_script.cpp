#include "mcv_platform.h"
#include "comp_script.h"
#include "modules/scripting/script_resource.h"
#include "entity/common_msgs.h"
#include "components/common/comp_name.h"

DECL_OBJ_MANAGER("script", TCompScript);

TCompScript::~TCompScript() {
	executeScriptFunct(_onEnd);
}

void TCompScript::load(const json & j, TEntityParseContext & ctx) {
	assert(j.count("path") > 0 && "TCompScript must have a script file registered in JSON");
	
	_interval = j.value("interval", 0.0f);

	// Initiate the component by fetching all script data.
	init(j);
	data = j;

	// Start the script.
	onStart();
}

void TCompScript::registerMsgs() {
	DECL_MSG(TCompScript, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompScript, TMsgSceneCreated, onSceneCreated);
	DECL_MSG(TCompScript, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompScript, TMsgResetComponent, onComponentReset);
	DECL_MSG(TCompScript, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompScript, TMsgEntityTriggerExit, onTriggerExit);
	DECL_MSG(TCompScript, TMsgEntityDead, onEntityDead);
}

void TCompScript::declareInLua() {
	EngineScripting.declareComponent<TCompScript>
	(
		// name in LUA
		"TCompScript",
		"disableNextFrame", &TCompScript::disableNextFrame
	);
}

void TCompScript::update(float dt) {
	if (entityName.empty()) {
		TCompName* c_name = getComponent<TCompName>();
		if (c_name)
			entityName = c_name->getName();
		if (entityName.empty()) return;
	}

	PROFILE_FUNCTION(entityName.c_str());
	elaspedTimeSinceLastExecution += dt;

	if (_disableNextFrame) {
		_disableNextFrame = false;
		getEntity().sendMsg(TMsgToogleComponent{false});
	}

	// Execute whenever the time elapsed is bigger than the interval.
	if (_onUpdate && elaspedTimeSinceLastExecution > _interval) {
		executeScriptFunct(_onUpdate, dt);
		elaspedTimeSinceLastExecution = 0.0f;
	}
}

void TCompScript::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Spacing(0, 5);
	std::string path = data["path"];
	ImGui::Text("Path to script: %s", path.c_str());
	ImGui::DragFloat("Elapsed time since last execution", &elaspedTimeSinceLastExecution);
	ImGui::DragFloat("Interval between executions:", &_interval);

	bool b = _onStart.valid();
	ImGui::Checkbox("Has onStart function", &b);
	b = _onEntityCreated.valid();
	ImGui::Checkbox("Has onEntityCreated function", &b);
	b = _onSceneCreated.valid();
	ImGui::Checkbox("Has onSceneCreated function", &b);
	b = _onUpdate.valid();
	ImGui::Checkbox("Has onUpdate function", &b);
	b = _onTriggerEnter.valid();
	ImGui::Checkbox("Has onTriggerEnter function", &b);
	b = _onTriggerExit.valid();
	ImGui::Checkbox("Has onTriggerExit function", &b);
	b = _onEnable.valid();
	ImGui::Checkbox("Has onEnable function", &b);
	b = _onDisable.valid();
	ImGui::Checkbox("Has onDisable function", &b);
	b = _onEnd.valid();
	ImGui::Checkbox("Has onEnd function", &b);
}

/* Private functions. */

// Once the script gets the information from the json this function is called.
// Loads the script into LUA so it can be used.
void TCompScript::init(const json & j) {
	// Get new fresh environment from main lua state, this way each script is like a sandbox
	// declaring a variable in the script doesn't affect at all the main lua state.
	sol::state & lua = EngineScripting._lua;

	// Create the environment with the same global declarations as main.
	_env = sol::environment(lua, sol::create, lua.globals());

	// Parse the file to load the script.
	CScript * scriptRes = EngineResources.getEditableResource(j["path"])->as<CScript>();
	
	// Set environment.
	scriptRes->setEnvironment(_env);
	EngineScripting.doFile(scriptRes->getPath(), _env);

	// Fetch functions pointers from script if there are any.
	_onStart = _env["onStart"];
	_onEntityCreated = _env["onEntityCreated"];
	_onSceneCreated = _env["onSceneCreated"];
	_onUpdate = _env["onUpdate"];
	_onTriggerEnter = _env["onTriggerEnter"];
	_onTriggerExit = _env["onTriggerExit"];
	_onEnable = _env["onEnable"];
	_onDisable = _env["onDisable"];
	_onEnd = _env["onEnd"];

	sol::table(lua, sol::create);

	// Read variables from json
	if (j.count("globals") > 0) {
		std::string globals = "";
		const json & jGlobals = j["globals"];
		for (auto& j : jGlobals) {
			if (j.is_string())
				globals += std::string(j) + "\n";
		}
		EngineScripting.doString(globals, _env);
	}

	// Get variables.
	if (j.count("variables") > 0) {
		const json & jVariables = j["variables"];
		_env["variables"] = sol::table(lua, sol::create);
		for (auto& var : jVariables) {
			std::string varName = var.value("varName", "");
			if (var["value"].is_string())
				_env["variables"][varName] = std::string(var["value"]);
			else if (var["value"].is_number()) // Convert to float for now.
				_env["variables"][varName] = float(var["value"]);
			else if (var["value"].is_array())
				_env["variables"][varName] = var["value"];
		}
	}

	// Fetch the entity handle.
	_env["entity"] = getEntity();
	_env["component"] = this;
}

// Called when the script gets loaded by first time. Calls the start function.
void TCompScript::onStart() {
	if (!active) return;
	executeScriptFunct(_onStart, _env["variables"]);
}

// Called when the script gets destroyed. Calls the end function.
void TCompScript::onEnd() {
	if (!active) return;
	executeScriptFunct(_onEnd, CHandle(this).getOwner());
}

// On message functions.

void TCompScript::disableNextFrame() {
	_disableNextFrame = true;
}

// Called when the entity is created and all its components has been loaded.
void TCompScript::onEntityCreated(const TMsgEntityCreated & msg){
	if (!active) return;
	executeScriptFunct(_onEntityCreated, CHandle(this).getOwner(), CHandle(this).getOwner().asUnsigned());
	TCompName* c_name = getComponent<TCompName>();
	if (c_name)
		entityName = c_name->getName();

	TCompTransform * t = getComponent<TCompTransform>();
}

// Called when the scene where the component is has been created, use it to fetch
// entities that are part of the same scene.
void TCompScript::onSceneCreated(const TMsgSceneCreated & msg){
	if (!active) return;
	executeScriptFunct(_onSceneCreated);
}

// Called when component gets activated or deactivated.
void TCompScript::onToggleComponent(const TMsgToogleComponent & msg){
	bool prevActive = active;
	active = msg.active;
	if (prevActive != active)
		active ? executeScriptFunct(_onEnable) : executeScriptFunct(_onDisable);
}

// Called if entity has pooling.
void TCompScript::onComponentReset(const TMsgResetComponent & msg){
	// Call the onEndFrom the script.
	onEnd();

	// Redo the component again. We create a new environment from the same file.
	init(data);

	// Call the functions for the newly reseted component.
	onStart();
	TMsgSceneCreated msgScene;
	onSceneCreated(msgScene);
	TMsgEntityCreated msgEntity;
	onEntityCreated(msgEntity);
}

// Called when entity goes inside trigger.
void TCompScript::onTriggerEnter(const TMsgEntityTriggerEnter & msg){
	if (!active) return;
	executeScriptFunct(_onTriggerEnter, msg.h_entity);
}

// Called when entity goes outside trigger.
void TCompScript::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	if (!active) return;
	executeScriptFunct(_onTriggerExit, msg.h_entity);
}

// Used if entity dies but is not removed, as in pooling.
void TCompScript::onEntityDead(const TMsgEntityDead & msg) {
	if (!active) return;
	executeScriptFunct(_onEnd);
}