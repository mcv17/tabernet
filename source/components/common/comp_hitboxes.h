#pragma once

#include "entity/entity.h"
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"
#include "skeleton/skeleton_shape.h"
#include "particles/particle_system.h"

class CEntity;

class TCompHitboxes : public TCompBase {

	std::string jsonStats;

	// Hit point aura system and time before it fades once deleted.
	particles::CSystem * hitPointAura = nullptr;
	CHandle entity_transform;
	static float max_squared_distance_to_play_effects; // How many meters of distance before we stop playing sounds. Add the squared version of the number of meters, if 25 then 625.
	float hitPointFadeTime = 2.0f;

	// Critical hitbox actor reference.
	void * criticalHitbox = nullptr;

	void onCreated(const TMsgEntityCreated&);
	void onToggleComponent(const TMsgToogleComponent & msg);

	void checkParticlesStatusOnPerceptionStatus();
	void updateHitboxesFromSkeleton();
	void generateNewRandomCriticalHitbox(); // returns the position where the critical hitbox is at.
	void createHitBoxParticles(const Vector3 & positionOfCriticalHitBox);
	
	void updatePhysxBoneShape(TSkeletonShapes::TSkeletonBoneShape bone_core);
	std::string dumpHitboxes();

public:
	DECL_SIBLING_ACCESS();

	~TCompHitboxes();

	TSkeletonShapes hitboxes;

	void load(const json& j, TEntityParseContext& ctx);
	void update(float elapsed);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();
	bool isCriticalHitbox(void* actor);

	std::vector<physx::PxRigidActor*> actors;
};

