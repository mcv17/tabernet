#include "mcv_platform.h"
#include "comp_base.h"
#include "entity/common_msgs.h"

void TCompBase::onToggleComponent(const TMsgToogleComponent & msg) {
	active = msg.active;
}