#pragma once

#include "comp_base.h"
#include "entity/entity.h"
 
class CMesh;
class CMaterial;

struct TMsgResetComponent;
struct TMsgEntityCreated;
struct TMsgToogleComponent;
struct TMsgRenderingStatus;
struct TMsgDefineLocalAABB;
struct TMsgSetMeshState;
struct TMsgSetEntityMeshInstantiated;

struct TCompRender : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Render variables. */

	struct MeshPart {
		const CMesh *      mesh = nullptr;
		const CMaterial *  material = nullptr;
		int                mesh_group = 0;
		int                mesh_instances_group = 0;
		bool               is_visible = true;
		int                state = 0;          // user defined...
		bool			   is_secondary = false;
	};

	bool					isInstanced = false; // Whether the given component is instanced or not.
	bool					can_be_instanced = false;  // If false, the tcomprender can't be registered.
	bool					is_decal = false;
	bool                    is_visible = true; // Checks if the mesh is visible, if so it can get rendered.
	CHandle                 h_transform; // Handle of the transform component.
	Vector4					color; // Base color of the obj.
	std::vector< MeshPart > parts; // Mesh parts of the obj.
	int                     curr_state = 0; // Current state of the mesh, this is user defined. Can be used to set a different state (so other elements are shown).
	AABB                    aabb; // AABB
	std::map<std::string, int> name_state_to_id; // Used for finding the state of a mesh based on a name.

	// Loads a mesh.
	void readMesh(const json& j);
	
	// On message functions.
	void onComponentReset(const TMsgResetComponent & msg);
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onToggleComponent(const TMsgToogleComponent & msg);
	void onRenderingStatus(const TMsgRenderingStatus & msg);
	void onDefineLocalAABB(const TMsgDefineLocalAABB& msg);
	void onMeshVisibility(const TMsgSetMeshState & msg);
	void onRenderingFullyInstantiated(const TMsgSetEntityMeshInstantiated & msg);

	friend class TCompLineRender;
	friend class TCompTrailRender;

	// Activates all the meshes with the given state.
	void showMeshesWithState(int new_state);
	// Updates the render manager if changes are done.
	void updateRenderManager();
	// Adds all of the visible particles keys to the render manager
	void addParticlesKeys();
public:

	~TCompRender();
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();
	static void declareInLua();

	// Register for instancing or unregister from instancing.
	void registerToInstancing();
	void unregisterFromInstancing();

	/* Getters. */
	Vector4 getColor() const { return color; }
	int getCurrentState() const { return curr_state; }
	bool isStateActive(const std::string & stateName) const;
	bool hasState(int state) const { return state >= 0 && state < parts.size(); }
	bool hasState(const std::string & stateName) const { return name_state_to_id.count(stateName) > 0; }
	int getRenderHashCode() const; // Returns the hash code of the component. Allows for identification in instancing.
	bool canBeInstanced() const { return can_be_instanced; }

	/* Setters. */
	void setColor(const VEC4 & theColor);
	void setCurrentState(int state);
	void setCurrentState(const std::string & stateName);
	void setActiveSecondaryState(int state, bool act);
	void setActiveSecondaryState(const std::string & stateName, bool act);
};