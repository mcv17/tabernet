#include "mcv_platform.h"
#include "comp_culling.h"
#include "comp_aabb.h"
#include "components/camera/comp_camera.h"
#include "entity/common_msgs.h"

DECL_OBJ_MANAGER("culling", TCompCulling);

#include "components/common/comp_name.h"

void TCompCulling::debugInMenu() {
	TCompBase::debugInMenu();

	auto hm = getObjectManager<TCompAbsAABB>();
	ImGui::Text("%d / %d AABB's visible", bits.count(), hm->size());

	TCompName * name = getComponent<TCompName>();
	ImGui::Text("%s", name->getName());
}

// Si algun plano tiene la caja en la parte negativa
// completamente, el aabb no esta dentro del set de planos
bool TCompCulling::VPlanes::isVisible(const AABB * aabb) const {
	auto it = begin();
	while (it != end()) {
		if (it->isCulled(aabb))
			return false;
		++it;
	}
	return true;
}

void TCompCulling::updateFromMatrix(MAT44 view_proj)
{
	// Construir el set de planos usando la view_proj
	planes.fromViewProjection(view_proj);

	// Start from zero. This is necessary for considering
	// entities that are removed.
	bits.reset();

	// Culls all instanced meshes.
	EngineInstancing.runGPUSceneCulling(cameraH);

	// Traverse all aabb's defined in the game and test them.
	// Instanced AABBs are not created as components so only non instanced entities are found.
	auto hm = getObjectManager<TCompAbsAABB>();
	hm->forEachWithExternalIdx([this]( const TCompAbsAABB* aabb, uint32_t external_idx ){
		if (aabb->isActive() && planes.isVisible(aabb))
			bits.set(external_idx);
	});
}

void TCompCulling::update( float dt ) {
	if (!active) return;

	PROFILE_FUNCTION("Updating culling");
	// We get this TCompCamera and the active one from the manager
	TCompCamera* c_camera = cameraH;
	if (!c_camera) return;

	MAT44 view_proj = c_camera->getViewProjection();
	//e_owner->sendMsg(TMsgGetCullingViewProj{ &view_proj });
	updateFromMatrix(view_proj);
}

void TCompCulling::registerMsgs() {
	DECL_MSG(TCompCulling, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompCulling, TMsgEntityCreated, onEntityCreated);
}

void TCompCulling::onEntityCreated(const TMsgEntityCreated & msg) {
	cameraH = getComponent<TCompCamera>();
}