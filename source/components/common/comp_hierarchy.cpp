#include "mcv_platform.h"
#include "comp_hierarchy.h"
#include "entity/entity.h"
#include "entity/entity_parser.h"
#include "components/common/comp_transform.h"
#include "entity/common_msgs.h"

DECL_OBJ_MANAGER("hierarchy", TCompHierarchy);

void TCompHierarchy::load(const json& j, TEntityParseContext& ctx) {
	assert(j.count("parent") );
	parent_name = j.value("parent", "");
	CTransform::load(j);
}

void TCompHierarchy::registerMsgs() {
	DECL_MSG(TCompHierarchy, TMsgEntitiesGroupCreated, onGroupCreated);
	DECL_MSG(TCompHierarchy, TMsgToogleComponent, onToggleComponent);
}

void TCompHierarchy::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	// I prefer to wait until the group is loaded to resolve my parent
	initParentEntity(msg.ctx.findEntityByName(parent_name));
}

void TCompHierarchy::debugInMenu() {
	TCompBase::debugInMenu();

  ImGui::LabelText("Parent Name", "%s", parent_name.c_str());
  CHandle h_parent_entity = h_parent_transform.getOwner();
  if (h_parent_entity.isValid())
    h_parent_entity.debugInMenu();
  CTransform::renderInMenu();
} 

void TCompHierarchy::initParentEntity(CHandle new_h_parent) {
  CEntity* e_parent = new_h_parent;
  if (e_parent) {
    // Cache the two handles: the comp_transform of the entity I'm tracing, and my comp_transform
    h_parent_transform = e_parent->getComponent<TCompTransform>();
    CEntity* e_my_owner = CHandle(this).getOwner();
    h_my_transform = e_my_owner->getComponent<TCompTransform>();
  }
  else {
    // Invalidate previous contents
    h_parent_transform = CHandle();
    h_my_transform = CHandle();
  }
}

void TCompHierarchy::setParentEntity(CHandle new_h_parent, bool keepOffset) {
	CTransform & t = *this;

	CEntity * e_parent = new_h_parent;
	if (e_parent) {
		// Cache the two handles: the comp_transform of the entity I'm tracing, and my comp_transform
		h_parent_transform = e_parent->getComponent<TCompTransform>();
		CEntity* e_my_owner = CHandle(this).getOwner();
		h_my_transform = e_my_owner->getComponent<TCompTransform>();

		if (!keepOffset) {
			// Set the transform of the hierarchy to be the one of the entity.
			TCompTransform* c_my_transform = h_my_transform;
			assert(c_my_transform);
			CTransform & transform = *this;
			transform = CTransform();
		}
	}
	else
		h_parent_transform = CHandle();
}


void TCompHierarchy::update(float dt) {
  // My parent world transform
  TCompTransform* c_parent_transform = h_parent_transform;
  if (!c_parent_transform) 
    return;

  // My Sibling comp transform
  TCompTransform* c_my_transform = h_my_transform;
  assert(c_my_transform);

  // Combine the current world transform with my 
  CTransform & t = *this;
  c_my_transform->set(c_parent_transform->combineWith(*this));
}
