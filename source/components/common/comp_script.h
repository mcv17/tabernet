#pragma once

#include "comp_base.h"
#include "entity/entity.h"

#include "engine.h"

struct TMsgEntityCreated;
struct TMsgSceneCreated;
struct TMsgToogleComponent;
struct TMsgResetComponent;
struct TMsgEntityTriggerEnter;
struct TMsgEntityTriggerExit;
struct TMsgEntityDead;

class TCompScript : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Private variables. */
	std::string entityName = "UnnamedEntity";

	// Script info from json.
	json data;

	bool _disableNextFrame = false;

	// Time control.
	float elaspedTimeSinceLastExecution = 0.0f; // Holds the time it has passed since last execution of onUpdate.
	float _interval; // How long between calls of the script.

	// LUA stuff.
	sol::environment _env; // Lua environment of this script.

	/* Script function pointers. */

	// Functions that are called by the engine and scripts can access.

	// OnStart function. Called just after the script has been loaded. Components are still not loaded though.
	sol::function _onStart;

	// _onEntityCreated function. Called after an entity has been created. Use it if you need to fetch any components from the given entity.
	sol::function _onEntityCreated;

	// _onSceneCreated function. Called after a scene has been created. This means, the full prefab/scene where the component was has been loaded.
	sol::function _onSceneCreated;

	// Update function that will be called in LUA
	sol::function _onUpdate;

	// OnTriggerEnter function. Called when a trigger enter message is recieved.
	sol::function _onTriggerEnter;

	// OnTriggerExit function. Called when a trigger exit message is recieved.
	sol::function _onTriggerExit;

	// OnEnable function. Called each time an script is activated, (an enable component set to true is recieved).
	sol::function _onEnable;

	// OnDisable function. Called each time an script is deactivated, (an enable component set to false is recieved).
	sol::function _onDisable;

	// OnEnd function. Called before an object is completely destroyed.
	sol::function _onEnd;

	/* Private functions. */

	// Called when the script gets loaded by first time. Calls the start function.
	void onStart();

	// Called when the script gets destroyed. Calls the end function.
	void onEnd();

	// Once the script gets the information from the json this function is called.
	// Loads the script into LUA so it can be used.
	void init(const json & j);

	// Executes a script function.
	template <typename... Args>
	void executeScriptFunct(sol::function & fn, Args && ... args) {
		if (fn == sol::nil) return;

		try {
			fn(std::forward<Args>(args)...);
		}
		catch (const sol::error & e) {
			EngineScripting.dealWithError(e);
		}
	}

	void disableNextFrame();

	// On message functions.
	void onEntityCreated(const TMsgEntityCreated & msg); // Called when the entity is created and all its components has been loaded.
	void onSceneCreated(const TMsgSceneCreated & msg); // Called when the scene where the component is has been created, use it to fetch													   // entities that are part of the same scene.
	void onToggleComponent(const TMsgToogleComponent & msg); // Called when component gets activated or deactivated.
	void onComponentReset(const TMsgResetComponent & msg); // Called if entity has pooling.
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg); // Called when entity goes inside trigger.
	void onTriggerExit(const TMsgEntityTriggerExit & msg); // Called when entity goes outside trigger.
	void onEntityDead(const TMsgEntityDead & msg); // If an entity dies but is not removed due to how pooling module works.

public:
	~TCompScript();

	void load(const json& j, TEntityParseContext& ctx);
	static void registerMsgs();
	static void declareInLua();
	void update(float dt);
	void debugInMenu();

	// Executes a function from the script. Returns true if it was executed, false ow.
	// This can be used to call functions from other components in the code.
	template <typename... Args>
	bool executeFunction(const std::string & scriptFunct, Args && ... args) {
		// What does it matter if it's active? When a component is disabled you can still call one of it's methods
		//if (!active) return false;

		sol::function func = _env[scriptFunct];
		if (func == sol::nil) return false;
		
		try {
			func(std::forward<Args>(args)...);
		}
		catch (const sol::error & e) {
			EngineScripting.dealWithError(e);
			return false;
		}

		return true;
	}
};