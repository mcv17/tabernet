#include "mcv_platform.h"
#include "comp_damage_visual_feedback.h"
#include "components/common/comp_render.h"

DECL_OBJ_MANAGER("visual_damage_feedback", TCompDamageVisualFeedback);

void TCompDamageVisualFeedback::registerMsgs() {
	DECL_MSG(TCompDamageVisualFeedback, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompDamageVisualFeedback, TMsgBulletDamage, onBulletDamage);
	DECL_MSG(TCompDamageVisualFeedback, TMsgNailDamage, onNailDamage);
	DECL_MSG(TCompDamageVisualFeedback, TMsgHitDamage, onHitDamage);
}

void TCompDamageVisualFeedback::update(float dt) {
	if (_isActive) {
		TCompRender * render = getComponent<TCompRender>();
		if (!render) return;

		_feedbackCurTime -= dt;

		// hack to allow change of color while feedback is active
		int currentState = render->getCurrentState();
		if (currentState != _feedbackState && currentState != _storedState) {
			_storedState = currentState;
			render->showMeshesWithState(_feedbackState);
		}
		
		if (_feedbackCurTime <= 0.0f) {
			_isActive = false;
			if (currentState == _feedbackState) //If the totem changed the colour, it already has another one and not the damage
				render->showMeshesWithState(_storedState);
		}
	}
}

void TCompDamageVisualFeedback::load(const json& j, TEntityParseContext& ctx) {
	_feedbackTime = j.value("time", _feedbackTime);
}

void TCompDamageVisualFeedback::debugInMenu() {
	TCompBase::debugInMenu();
}

void TCompDamageVisualFeedback::onBulletDamage(const TMsgBulletDamage & msg) {
	startVisualFeedback();
}

void TCompDamageVisualFeedback::onNailDamage(const TMsgNailDamage & msg) {
	startVisualFeedback();
}

void TCompDamageVisualFeedback::onHitDamage(const TMsgHitDamage & msg) {
	startVisualFeedback();
}

void TCompDamageVisualFeedback::startVisualFeedback() {
	TCompRender * render = getComponent<TCompRender>();
	if (!render) return;

	int renderState = render->getCurrentState();
	if (renderState != _feedbackState)
		_storedState = renderState;
	render->showMeshesWithState(_feedbackState);
	_isActive = true;
	_feedbackCurTime = _feedbackTime;
}