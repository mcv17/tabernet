#pragma once

#include "components/common/comp_base.h"

struct TMsgEntityCreated;
class TCompScript;

// This feet controller component is used for spawning decals and sounds when the feets of a character touch the ground.
// Gives a high precision for spawning particles and sounds and lets the scripting control which sounds and particles
// end up spawning.
class TCompFeetController : public TCompBase {
	DECL_SIBLING_ACCESS();
	
	// Struct holding the data for a single foot. An entity can have as many foot as necessary.
	// Most entities in our game are bipeds so two foot controls will be all that is needed.
	struct TFootControl {
		std::string footBoneName;         // Foot bone name position.
		int         bone_id = -1;         // Id of the bone of the skeleton to track.
		Vector3 downVector;               // Down vector based on the bone orientation. As bones are not rotated accordingly this is the only way of telling them which is supposed to be downwards.
		// Or we could just ignore it and not rotate the vector and simply use Vector3::Down, for correctness I'm just using this instead (allows it to work okay even if the characters flips.).
		float rayDistance = 0.2;          // Ray distance for each ray going outside.
		bool footGrounded = true;         // Whether the foot is touching the ground or not.

		void loadBone(const json & boneData); // Saves any information needed.
		void initBone(CHandle h_skeleton); // Called when the entity is fully created. Fetches necessary information. 
		void callOnTouchingGroundFunc(TCompScript * script, Vector3 & touchingPos, Vector3 & touchingNormal);
		void callOnLeavingGroundFunc(TCompScript * script, Vector3 & leavingPos, Vector3 & leavingNormal);
		bool checkJustTouchingTheGround(bool touchingGround) {
			if (footGrounded == false && touchingGround) {
				footGrounded = true; // Now grounded.
				return true;
			}
			return false;
		}
		bool checkJustLeavingTheGround(bool touchingGround) {
			if (footGrounded && touchingGround == false) {
				footGrounded = false; // Now not grounded.
				return true;
			}
			return false;
		}
	};

	CHandle		h_script;	       // Handle to the comp_script component of the entity.
	CHandle     h_skeleton;        // Handle to comp_skeleton of the entity being tracked
	CHandle     h_transform;       // Handle to the entity position.
	CHandle		h_bt;			   // Handle to bt
	static float max_squared_distance_to_play_effects; // How many meters of distance before we stop playing particles and sounds for feet controllers.
	// Add the squared version of the number of meters, if 25 then 625.

	std::string _footstepsSoundEvent = "";
	std::string _quadFootstepsSoundEvent = "";
	bool _isQuad = false;

	// We support multiple foot, more than two in case we may want to do something with quadruped enemies.
	std::vector<TFootControl> feet;

	void onEntityCreated(const TMsgEntityCreated & msg);
public:
	void load(const json& j, TEntityParseContext& ctx);
	void renderDebug();
	void update(float dt);

	static void registerMsgs();
};