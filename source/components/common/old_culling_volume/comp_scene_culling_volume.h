#pragma once

#include "comp_base.h"
#include <set>

struct TMsgEntitiesGroupCreated;
struct TMsgSceneCreated;
struct TMsgEntityTriggerEnter;
struct TMsgEntityTriggerExit;

/* This is a tighter version of the TCompSceneCullingVolume. It allows entities
present between an active an an inactive culling volume to keep moving. I found
the final code to be less readable and the extra functionality not that important,
we shouldn't have so tight culling volumes that such behaviour should be apreciated.
I decided to leave the code just in case. */

class TCompSceneCullingVolume : public TCompBase {
	DECL_SIBLING_ACCESS();

	// How many times it is active, when 0,
	// it will deactivate itself and send message to neighbors.
	// When 1, it will activate.
	int rendering_active = 1, logic_active = 1;

	/* Holds the group entity that contains all static entities inside the volume. */
	CHandle group_handle;
	/* Holds the rendering and logic neighbors for the culling volume. */
	std::vector<std::string> rendering_volumes_names;
	std::vector<std::string> logic_volumes_names;
	std::vector<CHandle> rendering_volumes;
	std::vector<CHandle> logic_volumes;

	/* First value identifies the entity, second value indicates in how many active
	culling volumes the entity is. This is used to handle AI's moving between volumes.
	When an entity enters an active volume or is present in one that is activated, it adds one.
	Same way, it substracts one when it leaves an active culling box or a culling volume is deactivated.
	If it reaches zero, it deactivates, if it is one, it sends a message to activate it if it was not activated.
	If we don't do this, entities that go outside the active culling volumes, would still work. */
	static std::map<CHandle, int> dynamic_entities;
	
	// Dynamic entities can move between volumes.
	// This set holds the handles of the entities inside the volume
	std::set<CHandle> dynamic_entities_in_volume;
	// This set holds the aabb of dynamic entities inside the culling volume.
	std::set<CHandle> dynamic_entities_abbs_in_volume;

	/* Messages. */
	void onSceneCullingCreated(const TMsgEntitiesGroupCreated & msg);
	void onSceneLoaded(const TMsgSceneCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onTriggerExit(const TMsgEntityTriggerExit & msg);

	// AABB functions.
	void markEntityIfVolumeActive(CHandle entityH);
	void unmarkEntityIfVolumeActive(CHandle entityH);
	void addDynamicEntityAABB(CEntity * dynamic_entity);
	void removeDynamicEntityAABB(CEntity * dynamic_entity);

	// Culling volume neighbors status functions.
	void setRenderingNeighborsVolumesStatus();
	void setLogicNeighborsVolumesStatus();
public:
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
	void setRenderingForEntitiesInVolume(bool status);
	void setLogicForEntitiesInVolume(bool status);

	/* Getters of aabbs inside volume. */
	const std::set<CHandle> & getDynamicEntitiesInsideVolume() const { return dynamic_entities_abbs_in_volume; }
	const std::vector<CHandle> & getStaticEntitiesInsideVolume() const {
		TCompGroup * groupC = group_handle;
		return groupC->getEntitiesInsideGroup();
	}
};