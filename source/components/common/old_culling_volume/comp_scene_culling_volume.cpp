#include "mcv_platform.h"
#include "comp_scene_culling_volume.h"

#include "engine.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

#include "comp_tags.h"
#include "components/common/comp_aabb.h"
#include "components/common/comp_group.h"

DECL_OBJ_MANAGER("scene_culling_volume", TCompSceneCullingVolume);

std::map<CHandle, int> TCompSceneCullingVolume::dynamic_entities;

void TCompSceneCullingVolume::load(const json & j, TEntityParseContext& ctx){
	// Rendering volumes.
	const json & renderingNeighbors = j["culling_volume_rendering_neighbors"];
	rendering_volumes_names.reserve(renderingNeighbors.size());
	for (auto & volumeName : renderingNeighbors)
		rendering_volumes_names.push_back(volumeName);

	// Logic volumes.
	auto & logicNeighbors = j["culling_volume_logic_neighbors"];
	logic_volumes_names.reserve(logicNeighbors.size());
	for (auto & volumeName : logicNeighbors)
		logic_volumes_names.push_back(volumeName);
}

void TCompSceneCullingVolume::debugInMenu(){
	ImGui::TextColored(ImVec4(1, 0, 1, 1), "Rendering volumes");
	for (auto & rendering_name : rendering_volumes_names)
		ImGui::Text(rendering_name.c_str());

	ImGui::TextColored(ImVec4(1, 0, 1, 1), "Logic volumes");
	for (auto & logic_name : logic_volumes_names)
		ImGui::Text(logic_name.c_str());
}

void TCompSceneCullingVolume::registerMsgs() {
	DECL_MSG(TCompSceneCullingVolume, TMsgEntitiesGroupCreated, onSceneCullingCreated);
	DECL_MSG(TCompSceneCullingVolume, TMsgSceneCreated, onSceneLoaded);
	DECL_MSG(TCompSceneCullingVolume, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompSceneCullingVolume, TMsgEntityTriggerExit, onTriggerExit);
}

void TCompSceneCullingVolume::setRenderingNeighborsVolumesStatus() {
	for (auto h : rendering_volumes) {
		TCompSceneCullingVolume * comp = h;
		comp->setRenderingForEntitiesInVolume(active);
	}
}

void TCompSceneCullingVolume::setLogicNeighborsVolumesStatus() {
	for (auto h : logic_volumes) {
		TCompSceneCullingVolume * comp = h;
		comp->setLogicForEntitiesInVolume(active);
	}
}

void TCompSceneCullingVolume::setRenderingForEntitiesInVolume(bool status){
	rendering_active = status ? rendering_active + 1 : rendering_active - 1;
	if (rendering_active > 1) return;

	// Group component contains all entities inside the culling volume on creation.
	// In our engine, we only add static entities by default.
	TCompGroup * groupC = group_handle;
	if (!groupC) return;
	TMsgRenderingStatus msg = { status };
	groupC->sendMsgToEntitiesInGroup<TMsgRenderingStatus>(msg);

	// We don't render dynamics here, we will render them only when logic is also active.
}

void TCompSceneCullingVolume::setLogicForEntitiesInVolume(bool status){
	logic_active = status ? logic_active + 1 : logic_active - 1;
	if (logic_active > 1) return;
	
	TCompGroup * groupC = group_handle;
	if (!groupC) return;
	TMsgLogicStatus msg = { status };
	groupC->sendMsgToEntitiesInGroup<TMsgLogicStatus>(msg);

	for (auto & entity : dynamic_entities_in_volume) {
		// We increase or decrease the value of the dynamic entities inside the box,
		// based on whether we increase or decrease. If the number is bigger than 1,
		// or we have gone from more than 1 to 1, we don't have to send any message.
		auto & entityActiveNum = dynamic_entities[entity];
		entityActiveNum = status ? entityActiveNum + 1 : entityActiveNum - 1;
		if (entityActiveNum > 1 || (entityActiveNum == 1 && !status)) return;

		TMsgRenderingStatus msg = { status };
		TMsgLogicStatus msg2 = { status };
		CHandle entityH = entity; // I have to do this, entity is const and i can't send a message.
		entityH.sendMsg<TMsgRenderingStatus>(msg);
		entityH.sendMsg<TMsgLogicStatus>(msg2);
	}
}

void TCompSceneCullingVolume::markEntityIfVolumeActive(CHandle entityH) {
	// If entity not found, create it.
	if (dynamic_entities.find(entityH) == dynamic_entities.end())
		dynamic_entities[entityH] = 0;

	// If we enter into an active volume, we add one to the dynamic entity counter.
	auto & entNumVolumesPressent = dynamic_entities[entityH];
	entNumVolumesPressent = logic_active ? entNumVolumesPressent + 1 : entNumVolumesPressent;

	// We activate it if it wasn't active before.
	if (entNumVolumesPressent == 1) {
		TMsgRenderingStatus msg = { true };
		TMsgLogicStatus msg2 = { true };
		entityH.sendMsg<TMsgRenderingStatus>(msg);
		entityH.sendMsg<TMsgLogicStatus>(msg2);
	}
}

void TCompSceneCullingVolume::unmarkEntityIfVolumeActive(CHandle entityH) {
	// If entity not found, create it.
	if (dynamic_entities.find(entityH) == dynamic_entities.end())
		dynamic_entities[entityH] = 0;

	// If we go outside an active module, we remove one from the counter.
	auto & entNumVolumesPressent = dynamic_entities[entityH];
	entNumVolumesPressent = logic_active ? entNumVolumesPressent - 1 : entNumVolumesPressent;

	// If the entity is no longer in an active volume, we deactivate it.
	if (entNumVolumesPressent < 1) {
		TMsgRenderingStatus msg = { true };
		TMsgLogicStatus msg2 = { true };
		entityH.sendMsg<TMsgRenderingStatus>(msg);
		entityH.sendMsg<TMsgLogicStatus>(msg2);
	}
}

void TCompSceneCullingVolume::addDynamicEntityAABB(CEntity * dynamic_entity){
	/* Mark the entity as active in this volume if the volume is active. */
	CHandle entityH = dynamic_entity;
	markEntityIfVolumeActive(entityH);
	
	/* Insert the entity aabb in the culling volume. */
	CHandle aabbs = dynamic_entity->getComponent<TCompAbsAABB>();
	assert(aabbs.isValid());
	if (aabbs.isValid()) {
		dynamic_entities_in_volume.insert(entityH);
		dynamic_entities_abbs_in_volume.insert(aabbs);
	}
}

void TCompSceneCullingVolume::removeDynamicEntityAABB(CEntity * dynamic_entity){
	/* Mark the entity as active in this volume. */
	CHandle entityH = dynamic_entity;
	unmarkEntityIfVolumeActive(entityH);

	/* Insert the entity aabb in the culling volume. */
	CHandle aabbs = dynamic_entity->getComponent<TCompAbsAABB>();
	assert(aabbs.isValid());
	if (aabbs.isValid()) {
		dynamic_entities_abbs_in_volume.erase(entityH);
		dynamic_entities_abbs_in_volume.erase(aabbs);
	}
}

void TCompSceneCullingVolume::onSceneCullingCreated(const TMsgEntitiesGroupCreated & msg) {
	group_handle = getComponent<TCompGroup>();
}

void TCompSceneCullingVolume::onSceneLoaded(const TMsgSceneCreated & msg) {
	// Fetch rendering volumes.
	rendering_volumes.reserve(rendering_volumes_names.size());
	for (auto & volumeName : rendering_volumes_names) {
		CHandle entity_handle = getEntityByName(volumeName);
		if (!entity_handle.isValid()) continue;
		CEntity * entity = entity_handle;
		CHandle handle = entity->getComponent<TCompSceneCullingVolume>();
		if (!handle.isValid()) continue;
			rendering_volumes.push_back(handle);
	}

	// Fetch logic volumes.
	logic_volumes.reserve(rendering_volumes_names.size());
	for (auto & volumeName : logic_volumes_names) {
		CHandle entity_handle = getEntityByName(volumeName);
		if (!entity_handle.isValid()) continue;
		CEntity * entity = entity_handle;
		CHandle handle = entity->getComponent<TCompSceneCullingVolume>();
		if (!handle.isValid()) continue;
		logic_volumes.push_back(handle);
	}


	/* Start by setting the volume to active. Only the volume where the player
	is will get activated (and all its neighbors). */
	active = false;
	setRenderingForEntitiesInVolume(active);
	setLogicForEntitiesInVolume(active);
}

void TCompSceneCullingVolume::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	CEntity * entity = msg.h_entity;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags && tags->hasTag(Utils::getID("player"))) {
		active = true;

		/* Set neighbors and this volume status */
		setRenderingForEntitiesInVolume(active);
		setLogicForEntitiesInVolume(active);
		setRenderingNeighborsVolumesStatus();
		setLogicNeighborsVolumesStatus();
	}
	else
		addDynamicEntityAABB(entity);
}

void TCompSceneCullingVolume::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	CEntity * entity = msg.h_entity;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags && tags->hasTag(Utils::getID("player"))) {
		active = false;

		/* Set neighbors and this volume status */
		setRenderingForEntitiesInVolume(active);
		setLogicForEntitiesInVolume(active);
		setRenderingNeighborsVolumesStatus();
		setLogicNeighborsVolumesStatus();
	}
	else
		removeDynamicEntityAABB(entity);
}