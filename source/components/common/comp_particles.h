#pragma once

#include "comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "particles/particle_system.h"

struct TMsgEntityCreated;

namespace particles
{
	struct TEmitter;
}

// Used for emitting particles.
struct TCompParticles : public TCompBase
{
	DECL_SIBLING_ACCESS();

	~TCompParticles();

	static void declareInLua();

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();
	static void registerMsgs();

	void setEmitter(particles::TEmitter* emitter) { _emitter = emitter; }
	bool isSystemActive() { return _isSystemActive; }
	void setSystemActive(bool state);
	void setActive(bool active);

	void spawnParticles(int numParticles);

	void setBaseVelocity(Vector3 velocity) { if (_system) _system->setBaseVelocity(velocity); }
	void clearBaseVelocityNextFrame() { if (_system) _system->clearBaseVelocityNextFrame(); }

private:
	particles::TEmitter * _emitter = nullptr;
	particles::CSystem * _system = nullptr;

	Vector3 _offset;
	bool _isComponentActive = true;
	bool _isSystemActive = true;

	bool _followBone = false;
	std::string _boneToFollow = "";

	int _renderPriority = 0;

	void onEntityCreated(const TMsgEntityCreated&);
	void onToggleComponent(const TMsgToogleComponent & msg);
	void toggleParticles();
};
