#include "mcv_platform.h"
#include "comp_collider.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "modules/module_physics.h"
#include "components/common/comp_transform.h"
#include "components/controllers/comp_character_controller.h"
#include "components/controllers/comp_rigid_body.h"
#include "comp_name.h"

DECL_OBJ_MANAGER("collider", TCompCollider);

using namespace physx;

void TCompCollider::load(const json& j, TEntityParseContext& ctx, bool isKinematic) {
	if (isKinematic) {
		jconfig = j["railgun_collider"];
		EnginePhysics.createActor(*this);
	}
	else {
		jconfig = j;
	}
}

// --------------------------------------------------------------------
TCompCollider::~TCompCollider() {
	/*We first check if it is a controller because physx creates a hidden kinematic actor 
	for every CCT, so we need to remove the controller.
	If the collider is not a controller, then we release the actor directly */
	//https://docs.nvidia.com/gameworks/content/gameworkslibrary/physx/guide/Manual/CharacterControllers.html#hidden-kinematic-actors
	destroyCollider();
}


void TCompCollider::changeColliderType(bool dynamic, bool kinematic, bool trigger) {
	// Destroy previous collider.
	destroyCollider();

	jconfig["dynamic"] = dynamic ? true : false;
	jconfig["static"] = !dynamic ? true : false;
	jconfig["kinematic"] = dynamic && kinematic ? true : false;
	jconfig["trigger"] = trigger ? true : false;

	// Create new collider.
	EnginePhysics.createActor(*this);
}

void TCompCollider::destroyCollider() {
	if (actor && EnginePhysics.isActive()) {
		if (controller) {
			EnginePhysics.getScene()->removeActor(*controller->getActor());
			controller->release();
			controller = nullptr;
			// No need to release the actor.
			actor = nullptr;
		}
		else {
			EnginePhysics.getScene()->removeActor(*actor);
			actor->release(), actor = nullptr;
		}
	}
}

void TCompCollider::activateShapes(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world) {
	shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, true);
	shape->setFlag(PxShapeFlag::eSCENE_QUERY_SHAPE, true);
	actor->setActorFlag(PxActorFlag::eDISABLE_SIMULATION, false);
}

void TCompCollider::disableShapes(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world) {
	shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
	shape->setFlag(PxShapeFlag::eSCENE_QUERY_SHAPE, false);
	actor->setActorFlag(PxActorFlag::eDISABLE_SIMULATION, true);
}

// Iterates over all shapes of the actor, calling the given callback
// resolves the invalid cases, and gives the world matrix including the
// shape offset.
void TCompCollider::onEachShape(TShapeFn fn) {
  TCompTransform* trans = getComponent<TCompTransform>();
  if (!trans) return;
  assert(trans);

  static const PxU32 max_shapes = 8;
  PxShape* shapes[max_shapes];

  if (actor == nullptr) return;

  PxU32 nshapes = actor->getNbShapes();
  assert(nshapes <= max_shapes);

  // Even when the buffer is small, it writes all the shape pointers
  PxU32 shapes_read = actor->getShapes(shapes, sizeof(shapes), 0);

  // An actor can have several shapes
  for (PxU32 i = 0; i < nshapes; ++i) {
    PxShape* shape = shapes[i];
    assert(shape);

    // Combine physics local offset with world transform of the entity
	Matrix world = toTransform(shape->getLocalPose()).asMatrix() * Matrix::CreateFromQuaternion(trans->getRotation()) * Matrix::CreateTranslation(trans->getPosition());
    switch (shape->getGeometryType()) {
    case PxGeometryType::eSPHERE: {
      PxSphereGeometry sphere;
      if (shape->getSphereGeometry(sphere))
        (this->*fn)(i, shape, PxGeometryType::eSPHERE, &sphere, world);
      break;
    }
    case PxGeometryType::eBOX: {
      PxBoxGeometry box;
      if (shape->getBoxGeometry(box))
        (this->*fn)(i, shape, PxGeometryType::eBOX, &box, world);
      break;
    }

    case PxGeometryType::ePLANE: {
      PxPlaneGeometry plane;
      if (shape->getPlaneGeometry(plane))
        (this->*fn)(i, shape, PxGeometryType::ePLANE, &plane, world);
      break;
    }

    case PxGeometryType::eCAPSULE: {
      PxCapsuleGeometry capsule;
      if (shape->getCapsuleGeometry(capsule))
        (this->*fn)(i, shape, PxGeometryType::eCAPSULE, &capsule, world);
      break;
    }

    case PxGeometryType::eTRIANGLEMESH: {
      PxTriangleMeshGeometry trimesh;
      if (shape->getTriangleMeshGeometry(trimesh))
        (this->*fn)(i, shape, PxGeometryType::eTRIANGLEMESH, &trimesh, world);
      break;
    }

		case PxGeometryType::eCONVEXMESH: {
			PxConvexMeshGeometry mesh;
			if (shape->getConvexMeshGeometry(mesh))
				(this->*fn)(i, shape, PxGeometryType::eCONVEXMESH, &mesh, world);
			break;
    }

    default:
      break;
    }
  }
}

// --------------------------------------------------------------------
void TCompCollider::renderDebugShape(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world) {
	if (!shape) return;

	Vector4 color = Vector4(1, 0, 0, 1);
	PxShapeFlags flags = shape->getFlags();
	if (flags & PxShapeFlag::eTRIGGER_SHAPE)
		color = Vector4(1, 1, 0, 1);

  switch (geometry_type) {
    case PxGeometryType::eSPHERE: {
      PxSphereGeometry* sphere = (PxSphereGeometry*)geom;
      drawWiredSphere(world, sphere->radius, color);
      break;
    }
    case PxGeometryType::eBOX: {
      PxBoxGeometry* box = (PxBoxGeometry*)geom;
	  drawWiredAABB(VEC3(0, 0, 0), PXVEC3_TO_VEC3(box->halfExtents), world, color);
      break;
    }
    case PxGeometryType::ePLANE: {
      PxBoxGeometry* box = (PxBoxGeometry*)geom;
      const CMesh* grid = EngineResources.getResource("grid.mesh")->as<CMesh>();
      // To generate a PxPlane from a PxTransform, transform PxPlane(1, 0, 0, 0).
      // Our plane is 0,1,0
      Matrix Z2Y = Matrix::CreateRotationZ((float)-M_PI_2);
      drawMesh(grid, Z2Y * world, Vector4(1, 1, 1, 1));
      break;
    }
    case PxGeometryType::eCAPSULE: {
      PxCapsuleGeometry* capsule = (PxCapsuleGeometry*)geom;
      // world indicates the foot position
      world = world * Matrix::CreateTranslation(0.f, capsule->radius, 0.f);
      drawWiredSphere(world, capsule->radius, color);
      world = world * Matrix::CreateTranslation(0.f, capsule->halfHeight * 2, 0.f);
      drawWiredSphere(world, capsule->radius, color);
      break;
    }
    case PxGeometryType::eTRIANGLEMESH: {
      const CMesh* debug_mesh = (const CMesh* )shape->userData;
      assert(debug_mesh);
      drawMesh(debug_mesh, world, color);
      break;
    }
		case PxGeometryType::eCONVEXMESH: {
			const CMesh* debug_mesh = (const CMesh*)shape->userData;
			assert(debug_mesh);
			drawMesh(debug_mesh, world, color);
			break;
		}
  }
}

void TCompCollider::renderDebug() {
	onEachShape(&TCompCollider::renderDebugShape);
	// commented for now :/
	//TCompTransform* t = getComponent<TCompTransform>();
	//if (t != nullptr)
	// drawLine(t->getPosition(), _debugThrowDirOnExplosion, VEC4(1, 1, 0, 1));
}

// --------------------------------------------------------------------
void TCompCollider::debugInMenuShape(int shapeID, physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, Matrix world) {
	ImGui::PushID(shapeID);

	TCompBase::debugInMenu();

	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);
	ImGui::Text("Current Shape: " + shapeID);
	ImGui::Spacing(0, 5);

	physx::PxTransform localPoseTransform = shape->getLocalPose();
	ImGui::Text("Current offset: %f %f %f", localPoseTransform.p.x, localPoseTransform.p.y, localPoseTransform.p.z);

	PxShapeFlags flags = shape->getFlags();
	if (flags & PxShapeFlag::eTRIGGER_SHAPE) {
		ImGui::Spacing(0, 5);
		ImGui::Text("Is trigger");
	}

	ImGui::Spacing(0, 5);
	ImGui::Text("Geometry specific values: ");
	ImGui::Spacing(0, 5);

	switch (geometry_type) {
	case PxGeometryType::eSPHERE: {
		PxSphereGeometry* sphere = (PxSphereGeometry*)geom;
		ImGui::LabelText("Sphere Radius", "%f", sphere->radius);
		break;
	}
	case PxGeometryType::eBOX: {
		PxBoxGeometry* box = (PxBoxGeometry*)geom;
		ImGui::LabelText("Box", "Half:%f %f %f", box->halfExtents.x, box->halfExtents.y, box->halfExtents.z);
		break;
	}
	case PxGeometryType::ePLANE: {
		PxPlaneGeometry* plane = (PxPlaneGeometry*)geom;
		ImGui::Text("Plane");
		break;
	}
	case PxGeometryType::eCAPSULE: {
		PxCapsuleGeometry* capsule = (PxCapsuleGeometry*)geom;
		ImGui::LabelText("Capsule", "Rad:%f Height:%f", capsule->radius, capsule->halfHeight);
		break;
	}
	case PxGeometryType::eTRIANGLEMESH: {
		PxTriangleMeshGeometry* trimesh = (PxTriangleMeshGeometry*)geom;
		ImGui::LabelText("Triangle mesh", "%d verts", trimesh->triangleMesh->getNbVertices());
		break;
	}
	case PxGeometryType::eCONVEXMESH: {
		PxConvexMeshGeometry* mesh = (PxConvexMeshGeometry*)geom;
		ImGui::LabelText("Convex mesh", "%d verts", mesh->convexMesh->getNbVertices());
		break;
	}
	}

	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	ImGui::PopID();
}

void TCompCollider::debugInMenu() {
	if (!actor) return;
  
	auto actor_type = actor->getType();
	if (actor_type == physx::PxActorType::Enum::eRIGID_DYNAMIC)
		ImGui::Text("Rigid Dynamic");
	else if (actor_type == physx::PxActorType::Enum::eRIGID_STATIC)
		ImGui::Text("Rigid Static");

	if (controller) {
		float rad = controller->getRadius();
		if (ImGui::DragFloat("Controller Radius", &rad, 0.02f, 0.1f, 5.0f))
			controller->setRadius(rad);
		float height = controller->getHeight();
		if (ImGui::DragFloat("Controller Height", &height, 0.02f, 0.1f, 5.0f))
			controller->resize(height);
	}

	static bool active = true;
	ImGui::Checkbox("Disable", &active);
	if (active)
		onEachShape(&TCompCollider::activateShapes);
	else
		onEachShape(&TCompCollider::disableShapes);
	
	onEachShape(&TCompCollider::debugInMenuShape);
}

bool TCompCollider::isRigidBody() {
	return jconfig.count("controller") == 0 && jconfig.value("dynamic", false); // dynamic needs to be true and no controller present.
}

// --------------------------------------------------------------------
void TCompCollider::registerMsgs() {
	DECL_MSG(TCompCollider, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCollider, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompCollider, TMsgDeleteAndCreateController, onDeleteAndCreateController);
}

void TCompCollider::onToggleComponent(const TMsgToogleComponent & msg) {
	active = msg.active;

	if (active) {
		if (controller)
			controller->setFootPosition(VEC3_TO_PXEXTVEC3(_prevPosition));
		else if (actor)
			EnginePhysics.getScene()->addActor(*actor);
	}
	else {
		if (controller) {
			TCompTransform* c_trans = getComponent<TCompTransform>();
			if (c_trans)
				_prevPosition = c_trans->getPosition();
			controller->setFootPosition(PxExtendedVec3(0.0f, -100.0f, 0.0f));
		}
		else if (actor)
			EnginePhysics.getScene()->removeActor(*actor);
	}
}

void TCompCollider::onDeleteAndCreateController(const TMsgDeleteAndCreateController& msg) {
	// We don't need to delete the previous controller because we restarted the physx scene

	// And we create a new one
	TCompName* name = getComponent<TCompName>();
	Utils::dbg("Collider recreated for: %s", name->getName());
	EnginePhysics.createActor(*this);
}

void TCompCollider::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompName * name = getComponent<TCompName>();
	Utils::dbg("Collider created for: %s", name->getName());
	EnginePhysics.createActor(*this);
}