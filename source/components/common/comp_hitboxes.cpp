#include "mcv_platform.h"
#include "comp_hitboxes.h"
#include "engine.h"

#include "skeleton/skeleton_shape.h"
#include "skeleton/skeleton_shape_core.h"
#include "cal3d/cal3d.h"
#include "skeleton/cal3d2engine.h"
#include "skeleton/comp_skeleton.h"
#include "components/common/comp_transform.h"

using namespace physx;

DECL_OBJ_MANAGER("hitboxes", TCompHitboxes);

// Simplify this
#include "resources/resource.h"
#include "render/render.h"

float TCompHitboxes::max_squared_distance_to_play_effects = 1600; // 1600 is 40 meters squared. We store the squared version because it's faster.

TCompHitboxes::~TCompHitboxes() {
	if (!actors.empty() && EnginePhysics.isActive()) {
		for (auto actor : actors) {
			actor->release();
			actor = nullptr;
		}
	}
	LogicManager::Instance().removeSpawnedParticle(hitPointAura, hitPointFadeTime);
}

void TCompHitboxes::registerMsgs() {
	DECL_MSG(TCompHitboxes, TMsgEntityCreated, onCreated);
	DECL_MSG(TCompHitboxes, TMsgToogleComponent, onToggleComponent);
}

void TCompHitboxes::load(const json& j, TEntityParseContext& ctx) {}

void TCompHitboxes::onCreated(const TMsgEntityCreated&) {
	entity_transform = getComponent<TCompTransform>();
	EnginePhysics.createHitboxes(*this);
	generateNewRandomCriticalHitbox();
}

void TCompHitboxes::createHitBoxParticles(const Vector3 & positionOfCriticalHitBox) {
	if(hitPointAura)
		LogicManager::Instance().removeSpawnedParticle(hitPointAura, hitPointFadeTime);
	
	// Create the particle for the hitboxes.
	hitPointAura = LogicManager::Instance().SpawnParticle("data/particles/weak_point_aura.particles", positionOfCriticalHitBox);
}

void TCompHitboxes::onToggleComponent(const TMsgToogleComponent & msg) {
	TCompBase::active = msg.active;

	if (TCompBase::active) {
		//Add the hitboxes to the physx scene
		for (int i = 0; i < hitboxes.num_bones; ++i) {
			PxActor* actor = hitboxes.bones[i].actor;
			EnginePhysics.getScene()->addActor(*actor);
		}
	}
	else {
		//Remove the hitboxes from the physx scene
		for (int i = 0; i < hitboxes.num_bones; ++i) {
			PxActor* actor = hitboxes.bones[i].actor;
			EnginePhysics.getScene()->removeActor(*actor);
		}
	}
	//Toggle the particles in the module_particles
	checkParticlesStatusOnPerceptionStatus();
}

bool TCompHitboxes::isCriticalHitbox(void* actor) {
	if (criticalHitbox == actor)
		return true;
	return false;
}

void TCompHitboxes::update(float dt) {
	// Check if we need to activate or deactivate particles.
	checkParticlesStatusOnPerceptionStatus();
	// Update the hitboxes position based on the skeleton position.
	updateHitboxesFromSkeleton();
}

void TCompHitboxes::checkParticlesStatusOnPerceptionStatus() {
	//If the component is not active, remove the particles if they exist
	if (!active) {
		if (hitPointAura && hitPointAura->isActive())
			hitPointAura->setActive(false);
		return;
	}

	bool perceptionActive = LogicManager::Instance().isPerceptionActive();
	// If perception not active and particles are active, we set them inactive.
	if (!perceptionActive) {
		if (hitPointAura && hitPointAura->isActive())
			hitPointAura->setActive(false);
	}
	else {
		// If perception active and particles are not active, we set them active.
		if (hitPointAura && !hitPointAura->isActive())
			hitPointAura->setActive(true);
	}

	TCompTransform * transform = entity_transform;
	if (perceptionActive && transform) {
		bool isPlayerOnRange = LogicManager::Instance().isPlayerClose(transform->getPosition(), max_squared_distance_to_play_effects);

		// Activate if on range and was deactivated.
		if (isPlayerOnRange && hitPointAura && !hitPointAura->isActive())
			hitPointAura->setActive(true);
		else if (!isPlayerOnRange && hitPointAura && hitPointAura->isActive())
			hitPointAura->setActive(false);
	}
}

void TCompHitboxes::updateHitboxesFromSkeleton() {
	CHandle h_comp_hitboxes(this);
	CEntity* ent = h_comp_hitboxes.getOwner();
	if (!ent)
		return;
	TCompTransform* comp_transform = ent->getComponent<TCompTransform>();
	TCompSkeleton* comp_skel = ent->getComponent<TCompSkeleton>();
	
	if (comp_skel) {
		auto model = comp_skel->getModel();
		if (model) {
			auto core_model = model->getCoreModel();
			if (core_model) {
				auto skel = model->getSkeleton();
				if (skel) {
					auto core_skel = skel->getCoreSkeleton();
					if (core_skel) {

						int root_core_bone_id = core_skel->getVectorRootCoreBoneId()[0];

						for (int i = 0; i < hitboxes.num_bones; ++i) {
							auto& bone_core = hitboxes.bones[i];

							CalBone *cal_bone = skel->getBone(bone_core.idx);
							
							CTransform actor_position;
							VEC3 hitbox_offset = DirectX::XMVector3Rotate(bone_core.core->position_offset, comp_transform->getRotation());

							if (bone_core.has_parent) {
								CalBone *parent_bone = skel->getBone(bone_core.parent_idx);
								Vector3 bone_pos = Cal2DX(cal_bone->getTranslationAbsolute());
								Vector3 parent_pos = Cal2DX(parent_bone->getTranslationAbsolute());
								// Hitbox will be in the middle between the two bones plus an offset
								Vector3 hitbox_pos = (bone_pos - parent_pos) / 2;
								hitbox_pos += parent_pos;
								actor_position.setPosition(hitbox_pos + hitbox_offset);
								// Face the bone (son) with a fixed rotation offset to rotate it according to the two positions
								float yaw, pitch;
								actor_position.getAnglesToFaceTarget(bone_pos, yaw, pitch);
								actor_position.setAngles(yaw, pitch + PI / 2.0f);
							}
							else {
								//Calculate the new position and rotation with offsets for the actor
								// rotate the offset vector with the player orientation
								actor_position.setPosition(Cal2DX(cal_bone->getTranslationAbsolute()) + hitbox_offset);
								QUAT rotation_offset = QUAT::CreateFromYawPitchRoll(bone_core.core->rotation_offset.x, bone_core.core->rotation_offset.y, bone_core.core->rotation_offset.z);
								QUAT bone_rotation = Cal2DX(cal_bone->getRotationAbsolute());
								actor_position.setRotation(rotation_offset * bone_rotation);
							}

							physx::PxTransform px_transform;
							px_transform.p = VEC3_TO_PXVEC3(actor_position.getPosition());
							px_transform.q = QUAT_TO_PXQUAT(actor_position.getRotation());

							bone_core.actor->setGlobalPose(px_transform);
							// Update the position of the particles of the hitpoint
							if (isCriticalHitbox(bone_core.actor))
								hitPointAura->setSystemTransform(CTransform(actor_position.getPosition(), actor_position.getRotation()));
						}
					}
				}
			}
		}
	}
}

void TCompHitboxes::generateNewRandomCriticalHitbox() {
	auto hitbox = hitboxes.bones[RNG.i(hitboxes.num_bones)].actor;
	criticalHitbox = hitbox;
	createHitBoxParticles(PXVEC3_TO_VEC3(hitbox->getGlobalPose().p));
}

void TCompHitboxes::updatePhysxBoneShape(TSkeletonShapes::TSkeletonBoneShape bone_core) {
	//Get Components
	TCompTransform* comp_transform = getComponent<TCompTransform>();
	CTransform* entity_trans = (CTransform*)comp_transform;
	TCompSkeleton* skel = getComponent<TCompSkeleton>();
	auto json_bone_core = bone_core.core;
	auto cal_core_bone = skel->getModel()->getCoreModel()->getCoreSkeleton()->getCoreBone(json_bone_core->bone); 
	
	//Delete the actual shape
	std::vector<PxShape*> shapes;
	shapes.resize(1);
	bone_core.actor->getShapes(&shapes[0], 1);
	bone_core.actor->detachShape(*shapes[0]);
	
	//And delete the actor
	actors.erase(std::remove(actors.begin(), actors.end(), bone_core.actor), actors.end());
	bone_core.actor->release();
	bone_core.actor = nullptr;

	//Calculate the new position and rotation with offsets for the actor
	CTransform actor_position;
	VEC3 hitbox_offset = DirectX::XMVector3Rotate(json_bone_core->position_offset, comp_transform->getRotation());
	actor_position.setPosition(Cal2DX(cal_core_bone->getTranslationAbsolute()) + hitbox_offset);
	QUAT rotation_offset = QUAT::CreateFromYawPitchRoll(json_bone_core->rotation_offset.x, json_bone_core->rotation_offset.y, json_bone_core->rotation_offset.z);
	QUAT bone_rotation = Cal2DX(cal_core_bone->getRotationAbsolute());
	actor_position.setRotation(rotation_offset * bone_rotation);

	//Create a new actor with position and rotation offsets
	PxRigidActor* actor = nullptr;
	PxRigidDynamic* kinematicActor = EnginePhysics.getPhysics()->createRigidDynamic(toPxTransform(actor_position));
	kinematicActor->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, true);
	PxRigidBodyExt::updateMassAndInertia(*kinematicActor, 1.f);
	actor = kinematicActor;
	actors.push_back(actor);
	bone_core.actor = kinematicActor;
	
	//Create a new shape
	PxShape* shape = EnginePhysics.getPhysics()->createShape(PxCapsuleGeometry(json_bone_core->radius, json_bone_core->height / 2.0f), *EnginePhysics.getMaterial());
	shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
	shape->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
	PxTransform relativePose(PxQuat(PxHalfPi, PxVec3(0, 0, 1)));

	PxFilterData filterData;
	filterData.word0 = EnginePhysics.EnemyHitboxes; // word0 = own ID
	shape->setQueryFilterData(filterData);
	shape->setLocalPose(relativePose);
	
	//And attach it
	bone_core.actor->attachShape(*shape);

	//Finally add the actor to the scene
	EnginePhysics.getScene()->addActor(*kinematicActor);
}

std::string TCompHitboxes::dumpHitboxes() {
	std::string returnString = "";
	for (int i = 0; i < hitboxes.num_bones; i++)
	{
		json j;
		auto& bone = hitboxes.bones[i];
		VEC3 pos_offset = bone.core->position_offset;
		VEC3 rot_offset = bone.core->rotation_offset;
		j["bone"] = hitboxes.bones[i].core->bone;
		if (bone.custom_height)
			j["height"] = bone.core->height;
		j["radius"] = bone.core->radius;
		if (pos_offset != VEC3::Zero)
			j["position_offset"] = std::to_string(pos_offset.x) + " " + std::to_string(pos_offset.y) + " " + std::to_string(pos_offset.z);
		if (hitboxes.bones[i].has_parent)
			j["parent_bone"] = bone.core->parent_bone;
		else
			j["rotation_offset"] = std::to_string(rot_offset.x) + " " + std::to_string(rot_offset.y) + " " + std::to_string(rot_offset.z);
		returnString += ",\n" + j.dump(0);
	}
	// Remove first comma
	returnString.erase(returnString.begin());
	return returnString;
}

void TCompHitboxes::debugInMenu() {
	if (ImGui::Button("DEBUG: Activate component (ToggleComponent MSG)")) {
		TMsgToogleComponent msg;
		msg.active = true;
		onToggleComponent(msg);
	}
	if (ImGui::Button("DEBUG: Disable component (ToggleComponent MSG)")) {
		TMsgToogleComponent msg;
		msg.active = false;
		onToggleComponent(msg);
	}
	if (ImGui::Button("Create hitbox test")) {
		//Get Components

		//Calculate the new position and rotation with offsets for the actor
		CTransform actor_position;
		actor_position.setPosition(VEC3(0.0f, 5.0f, 0.0f));

		//Create a new actor with position and rotation offsets
		PxRigidActor* actor = nullptr;
		PxRigidDynamic* kinematicActor = EnginePhysics.getPhysics()->createRigidDynamic(toPxTransform(actor_position));
		kinematicActor->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, true);
		PxRigidBodyExt::updateMassAndInertia(*kinematicActor, 1.f);
		actor = kinematicActor;

		//Create a new shape
		PxShape* shape = EnginePhysics.getPhysics()->createShape(PxCapsuleGeometry(0.05f, 0.1f / 2.0f), *EnginePhysics.getMaterial());
		shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);

		PxFilterData filterData;
		filterData.word0 = EnginePhysics.EnemyHitboxes; // word0 = own ID
		shape->setQueryFilterData(filterData);

		//And attach it
		actor->attachShape(*shape);

		//Finally add the actor to the scene
		EnginePhysics.getScene()->addActor(*kinematicActor);
	}
	
	if (ImGui::Button("Generate new random critical hitbox")) 
		generateNewRandomCriticalHitbox();
	//Json dumper
	if (ImGui::Button("Show dump")) {
		jsonStats = dumpHitboxes();
	}
	ImGui::SameLine();
	if (ImGui::Button("Dump to clipboard")) {
		ImGui::LogToClipboard();
		ImGui::LogText(dumpHitboxes().c_str());
		ImGui::LogFinish();
	}
	ImGui::TextWrapped(jsonStats.c_str());
	// Show Hitboxes
	if (ImGui::TreeNode("All Hitboxes")) {
		if (ImGui::Button("Show all render debug")) {
			for (int i = 0; i < hitboxes.num_bones; ++i) {
				auto hitboxes_bone = hitboxes.bones[i].core;
				hitboxes_bone->render_debug = true;
			}
		}
		if (ImGui::Button("Disable all render debug")) {
			for (int i = 0; i < hitboxes.num_bones; ++i) {
				auto hitboxes_bone = hitboxes.bones[i].core;
				hitboxes_bone->render_debug = false;
			}
		}
		for (int i = 0; i < hitboxes.num_bones; ++i) {
			auto json_bone_core = hitboxes.bones[i].core;
			auto bone_core = hitboxes.bones[i];
			if (ImGui::TreeNode(json_bone_core->bone.c_str())) {
				ImGui::Checkbox("Render debug active: ", &json_bone_core->render_debug);
				//For every hitbox we can modify the height, radius and offsets
				if (ImGui::DragFloat3("Position offset", &json_bone_core->position_offset.x, 0.001f, -3.f, 3.f))
					updatePhysxBoneShape(bone_core);
				if (!bone_core.has_parent) {
					if (ImGui::DragFloat3("Rotation offset", &json_bone_core->rotation_offset.x, 0.001f, -3.f, 3.f))
						updatePhysxBoneShape(bone_core);
				}
				if (ImGui::DragFloat("Radius", &json_bone_core->radius, 0.001f, 0.01f, 10.0f))
					updatePhysxBoneShape(bone_core);
				if (ImGui::DragFloat("Height", &json_bone_core->height, 0.001f, 0.01f, 10.0f)) {
					updatePhysxBoneShape(bone_core);
					bone_core.custom_height = true;
				}
				ImGui::TreePop();
			}
		}
		ImGui::TreePop();
	}
}

void TCompHitboxes::renderDebug()  {
	VEC4 colour = VEC4(0.75, 0.0, 0.75, 1);
	TCompSkeleton* skel = getComponent<TCompSkeleton>();
	for (int i = 0; i < hitboxes.num_bones; ++i) {
		TSkeletonShapes::TSkeletonBoneShape& hitboxes_bone = hitboxes.bones[i];

		if (!hitboxes_bone.core->render_debug)
			continue;

		auto bone_core = hitboxes_bone.core;
		
		physx::PxTransform px_transform = hitboxes_bone.actor->getGlobalPose();
//	physx::PxTransform px_parent_transform = hitboxes.bones[hitboxes_bone.parent_idx].actor->getGlobalPose();
		CTransform transform = toTransform(px_transform);
//		CTransform parent_transform = toTransform(px_parent_transform);
		
		//Line between actual bone and parent bone
		//MAT44 world1 = transform.asMatrix();
		//MAT44 world2 = parent_transform.asMatrix();
		//drawLine(world1.Translation(), world2.Translation(), VEC4(0.0, 1.0, 1.0, 1.0));

		//Spheres simulating the capsule collider
		VEC3 capsule_top_offset = VEC3(0, bone_core->height / 2.0f, 0);
		VEC3 capsule_bot_offset = VEC3(0, -bone_core->height / 2.0f, 0);

		// rotate displacement vector in order to get the right direction to draw two spheres simulating a capsule
		capsule_top_offset = DirectX::XMVector3Rotate(capsule_top_offset, transform.getRotation());
		capsule_bot_offset = DirectX::XMVector3Rotate(capsule_bot_offset, transform.getRotation());

		transform.setPosition(transform.getPosition() + capsule_top_offset);
		MAT44 top_sphere = transform.asMatrix();

		transform.setPosition(transform.getPosition() - capsule_top_offset + capsule_bot_offset);
		MAT44 bot_sphere = transform.asMatrix();

		drawLine(top_sphere.Translation(), bot_sphere.Translation(), colour);
		
		if (hitboxes.bones[i].actor == criticalHitbox) {
			drawWiredSphere(top_sphere, hitboxes.bones[i].core->radius, VEC4(1.0, 1.0, 0.0, 1.0));
			drawWiredSphere(bot_sphere, hitboxes.bones[i].core->radius, VEC4(1.0, 1.0, 0.0, 1.0));
		}
		else {
			drawWiredSphere(top_sphere, hitboxes.bones[i].core->radius, colour);
			drawWiredSphere(bot_sphere, hitboxes.bones[i].core->radius, colour);
		}		
	}
}
