#pragma once
#include "entity\entity.h"
#include "entity\common_msgs.h"
#include "comp_base.h"
#include "engine.h"
#include "ui\widgets\ui_image.h"

class TCompHealth : public TCompBase {
	// Handles.
	CHandle characterControllerH; // Handle to the character controller of the entity with this component.
	CHandle hitboxesH; // Handle to the hitboxes of the entity with this component.
	CHandle transformHandle;


	// Health variables.
	float maxHealth = 100.0f;
	float currentMaxHealth = maxHealth;
	float currentHealth = currentMaxHealth;
	bool immortal = false;
	float armour = 1.0f;
	bool dead = false;
	bool isCriticalHit = false;

	

	// Debug
	float giveDebugDamage = 10.0f;
	float addDebugHP = 10.0f;

	//Tick damage
	float tickDamage = 0.0f;
	float tickDuration = 0.0f;
	float timeNextTick = 0.0f;
	int tickCount = 0; //-1 equals endless

	std::string hurtSoundEvent = "";
	std::string deathSoundEvent = "";
	std::string playerDeathMusic = "";
	float hurtSoundChance = 0.4f;
	CClock hurtSoundCooldownTimer = CClock(1.0f);
	
    // UI
	bool isPlayer = false;
	float targetUIRatio = 1.0f;
	float currentUIRatio = 1.0f;
	float startingRatio = 0.0f;
	float lowerDamageRatioPerSecond = 0.25f;
	float damageBuffer = 0.0f; // used when damage is less than 1 so it doesn't show in the UI
	CClock waitClockUI = CClock(0.3f);
	CClock damageOverlayLifetimeClock;
	float damageOverlayMaxOpacity;
	float damageOverlayLifetime;		// Maximum amount of time the overlay stays on screen
	float damageOverlayLifetimeThreshold;		// Overlay opacity gets at its maximum at this percentage of life
	float damageOverlayRecoverSpeed;		// Speed at which overlay fades out
	UI::CImage* hp_bar;
	UI::CImage* hp_bar_damage;
	UI::CImage* damageOverlay = nullptr;

	void onEntityCreated(const TMsgEntityCreated & msg);

	void getHPBarWidget();
	bool getDamageOverlayWidget();
	void resetDamageOverlay();

	void onHealthLoweredHit(const TMsgHitDamage& msg);
	void onHealthLoweredBullet(const TMsgBulletDamage & msg);
	void onHealthLoweredNail(const TMsgNailDamage & msg);
	void onHealthLoweredExplosion(const TMsgExplosion& msg);
	void onHealthHealed(const TMsgHealing & msg);
	void onHealthFullyRestored(const TMsgFullRestore & msg);
	void onComponentReset(const TMsgResetComponent & msg);

	void checkIfDead(VEC3 hitDirection = VEC3(0, 0, 0), float hitForce = 0.0f);
	void updateUIWidget(float damageApplied = 0.0f, float dt = 0.0f);
public:
	TCompHealth();

	void update(float dt);
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();

	static void registerMsgs();

	static void declareInLua();

	void addHealth(float healthToAdd);
	void addDamage(float damage, VEC3 hitDirection = VEC3(0,0,0), float hitForce = 0.0f);
	float addRawDamage(float damage, VEC3 hitDirection = VEC3(0, 0, 0), float hitForce = 0.0f);

	float getCurrentHealth() { return currentHealth; }
	float getCurrentMaxHealth() { return currentMaxHealth; }
	float getMaxHealth() { return maxHealth; }
	void setCurrentHealth(float nCurHealth) {
		currentHealth = Maths::clamp(nCurHealth, 0.0f, currentMaxHealth);
		if (currentHealth == 0.0f)
			dead = true;
	}
	void setCurrentMaxHealth(float nMaxHealth) { currentMaxHealth = nMaxHealth; }
	void setImmortal(bool nImmortal) { immortal = nImmortal; }
	void setAsDead(bool nDead) { dead = nDead; }
	bool isDead() { return dead; }
	void setArmour(float armour);
	void setTickDamage(float tickDamage, float tickDuration, int tickCount = -1);
	void clearTickDamage() { tickCount = 0; }

	DECL_SIBLING_ACCESS();
};