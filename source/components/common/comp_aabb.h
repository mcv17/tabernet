#ifndef INC_COMPONENT_AABB_H_
#define INC_COMPONENT_AABB_H_

#include "geometry/geometry.h"
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "PxPhysicsAPI.h"

using namespace physx;

struct TMsgEntityCreated;
struct TMsgEntitiesGroupCreated;
struct TMsgLogicStatus;
struct TMsgSetEntityMeshInstantiated;
struct TMsgRagdollActivated;
struct TMsgResetComponent;

// Component base to derive the AbsAABB & LocalAABB
struct TCompAABB : public AABB, public TCompBase {
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void updateFromSiblingsLocalAABBs(CHandle h_entity);
	void updateFromSiblingsAbsAABBs(CHandle h_entity);
};

// This is required to use the culling system
struct TCompAbsAABB : public TCompAABB {
	void onCreate(const TMsgEntityCreated & msg);
	void onGroupCreate(const TMsgEntitiesGroupCreated & msg);
	void onLogicStatus(const TMsgLogicStatus & msg);
	void onRenderingFullyInstantiated(const TMsgSetEntityMeshInstantiated & msg);
	

	DECL_SIBLING_ACCESS();

public:
	static void registerMsgs();
};

// Updates AbsAABB from LocalAABB and CompTransform
// Use this component only if the object is moving in realtime
// during gameplay, otherwise, we are loosing time.
struct TCompLocalAABB : public TCompAABB {
	void update(float dt);
	void renderDebug();
	void debugInMenu();
	void onCreate(const TMsgEntityCreated & msg);
	void onGroupCreate(const TMsgEntitiesGroupCreated & msg);
	void onLogicStatus(const TMsgLogicStatus & msg);
	void onRagdollActivated(const TMsgRagdollActivated& msg);
	void onComponentReset(const TMsgResetComponent& msg);

	bool _attachToRagdoll = false;
	CTransform _ragdollTransform = CTransform();
	PxArticulationLink* _ragdollLink = nullptr;
	CHandle h_ragdoll = CHandle();


	DECL_SIBLING_ACCESS();

public:
	static void registerMsgs();
};

#endif
