#include "mcv_platform.h"
#include "comp_transform.h"
#include "entity/entity_parser.h"
#include "engine.h"

#include "entity/common_msgs.h"

DECL_OBJ_MANAGER("transform", TCompTransform);

void TCompTransform::debugInMenu() {
	TCompBase::debugInMenu();
	CTransform::renderInMenu();
}

void TCompTransform::load(const json& j, TEntityParseContext& ctx) {
	CTransform::load(j);
	set(ctx.root_transform.combineWith(*this));
}

void TCompTransform::set(const CTransform& new_tmx) {
	*(CTransform*)this = new_tmx;
}

void TCompTransform::renderDebug() {
	drawAxis(asMatrix());
}

void TCompTransform::registerMsgs() {
	DECL_MSG(TCompTransform, TMsgToogleComponent, onToggleComponent);
}

void TCompTransform::declareInLua() {
	EngineScripting.declareComponent<TCompTransform>
		(
			"TCompTransform",
			"getPosition", &TCompTransform::getPosition,
			"getRotation", &TCompTransform::getRotation,
			"setPosition", &TCompTransform::setPosition,
			"setRotation", &TCompTransform::setRotation,
			"getScale", &TCompTransform::getScale,
			"setScale", sol::overload
			(
				sol::resolve<void(VEC3)>(&TCompTransform::setScale),
				sol::resolve<void(float)>(&TCompTransform::setScale)
			),
			"getFront", &TCompTransform::getFront,
			"getUp", &TCompTransform::getUp,
			"getLeft", &TCompTransform::getLeft,
			"lookAt", &TCompTransform::lookAt,
			"setAngles", &TCompTransform::setAngles,
			"getYawPitchRoll", &TCompTransform::getYawPitchRoll,
			"getDeltaYawToAimTo", &TCompTransform::getDeltaYawToAimTo
			//sol::base_classes, sol::bases<CTransform>()		// inheritance works, but if we can avoid it it'll be more performant
		);
}
