#pragma once

#include "comp_base.h"
#include <set>

struct TMsgEntitiesGroupCreated;
struct TMsgSceneCreated;
struct TMsgEntityTriggerEnter;
struct TMsgEntityTriggerExit;

/* The Scene Culling Volume component manages the activation and deactivationof entities present in our scene.
Artist's use an exporter created for 3DS Max where culling volumes are defined for all the scene.
These culling volumes contain in their interior the static geometry they must render when activated,
furthermore, they also have references to other neighbor culling volumes that must be active when the volume gets
activated because the player is inside. Apart from managing the logic and rendering of the static entities inside them,
they also control the behaviour of dynamic entities that enter into it, disabling them when they are in a disabled volume. */

class TCompSceneCullingVolume : public TCompBase {
	DECL_SIBLING_ACCESS();

	// Do we start in this volume.
	bool StartingVolume = false;

	// How many times it is active, when 0,
	// it will deactivate itself and send message to neighbors.
	// When 1, it will activate.
	int rendering_active = 1, logic_active = 1;

	/* Holds the group entity that contains all static entities inside the volume. */
	CHandle group_handle;
	/* Holds the rendering and logic neighbors for the culling volume. */
	std::vector<std::string> rendering_volumes_names;
	std::vector<std::string> logic_volumes_names;
	std::vector<CHandle> rendering_volumes;
	std::vector<CHandle> logic_volumes;
	
	// Dynamic entities can move between volumes.
	// This set holds the handles of the entities inside the volume
	std::set<uint32_t> dynamic_entities_in_volume;
	// This set holds the aabb of dynamic entities inside the culling volume.
	std::set<uint32_t> dynamic_entities_abbs_in_volume;

	AABB SceneCullingBox;

	/* Messages. */
	void onSceneCullingCreated(const TMsgEntitiesGroupCreated & msg);
	void onSceneLoaded(const TMsgSceneCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onTriggerExit(const TMsgEntityTriggerExit & msg);

	// AABB functions.
	void sendMessageOnEntityEnter(CHandle entityH);
	void addDynamicEntityAABB(CEntity * dynamic_entity);
	void removeDynamicEntityAABB(CEntity * dynamic_entity);

	// Culling volume neighbors status functions.
	void setRenderingNeighborsVolumesStatus();
	void setLogicNeighborsVolumesStatus();
public:
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
	static void declareInLua();
	void setRenderingForEntitiesInVolume(bool status);
	void setLogicForEntitiesInVolume(bool status);

	void RemoveDynamicEntitiesInVolume();

	/* Getters of aabbs inside volume. */
	const std::set<uint32_t> & getDynamicEntitiesInsideVolume() const { return dynamic_entities_abbs_in_volume; }
	const std::vector<CHandle> & getStaticEntitiesInsideVolume() const;
};