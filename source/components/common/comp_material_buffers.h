#pragma once

#include "comp_base.h" 
#include "entity/entity.h"
#include "entity/common_msgs.h"

class CCteBufferBase;
class MaterialConstantBuffers;

/* These class holds all material buffers that must be activated when rendering an object.
Attention, these materials only get activated if the object is rendered without instancing.
Otherwise, they won't be activated. 

Two materials can't have same mesh_state and constant buffer slot or they will overlap. */
class TCompMaterialBuffers : public TCompBase {
	DECL_SIBLING_ACCESS();

	struct ConstantBufferToActivate {
		uint32_t activate_on_mesh_state = -1; // For now only a mesh state, maybe a vector in the future if necessary. Probably not. If -1, is always activated.
		MaterialConstantBuffers * constant_buffer; // Consant buffer to activate.
	};

	CHandle renderH; // Handle to the render component of the entity.
	uint32_t current_active_state; // State to activate.
	std::vector<ConstantBufferToActivate> constant_buffers_to_activate;

	void onComponentReset(const TMsgResetComponent& msg);
	void onEntityCreated(const TMsgEntityCreated & msg);
public:
	~TCompMaterialBuffers();
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void update(float dt);
	void activate();
	static void registerMsgs();
};
