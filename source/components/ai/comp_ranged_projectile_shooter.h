#pragma once
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"
#include "components/common/comp_transform.h"
#include "components/controllers/comp_player_character_controller.h"
#include "components/game/status_effects/common_status_effects.h"

class TCompRangedProjectileShooter : public TCompBase {

public:

	static void registerMsgs();

	DECL_SIBLING_ACCESS()

	void onEntityCreated(const TMsgEntityCreated & msg);
	void init(CHandle parent, const Vector3& offset);

	//void shootCorruption(bool accurate);
	//void shootFire(bool accurate);
	//void shootIce(bool accurate);
	//void shootSniper(bool accurate);
	CHandle shoot(std::string& prefab, bool accurate);

private:
	std::string _projectileModel = "data/prefabs/rangedEnemyProjectile.json";

	CHandle _transform, _hierarchy, _player;

	DECL_TCOMP_ACCESS("Player", TCompTransform, PlayerTransform);
	DECL_TCOMP_ACCESS("Player", TCompPlayerCharacterController, PlayerCharacterController);


	Vector3 predictPlayerPosition();

};