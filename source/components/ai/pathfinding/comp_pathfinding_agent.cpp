#include "mcv_platform.h"
#include "comp_pathfinding_agent.h";
#include "engine.h"
#include "components/controllers/comp_character_controller.h"
#include "components/common/comp_name.h"

#define DISTANCE_TO_DESTINATION_THRESHOLD				0.25f
#define DISTANCE_TO_WAIT_AGENT									2.0f
#define DISTANCE_TO_FOLLOW_THRESHOLD						0.25f
#define DISTANCE_TO_TP													4.0f

DECL_OBJ_MANAGER("pathfinding_agent", TCompPathfindingAgent)

void TCompPathfindingAgent::debugInMenu() {
	TCompBase::debugInMenu();
	TCompNavmesh* navmesh = getNavmeshComponent();
	ImGui::Text("AGENT DEBUG INFO");
	ImGui::Separator();
	if (isAgentActive()) {
		ImGui::Text("World Pos: %f %f %f", navmesh->getAgentPosition(_id).x, navmesh->getAgentPosition(_id).y, navmesh->getAgentPosition(_id).z);
		ImGui::Text("Target Pos: %f %f %f", _targetPos.x, _targetPos.y, _targetPos.z);
	}
	ImGui::Text("Navmesh name: %s", _navmeshName.c_str());
}

void TCompPathfindingAgent::renderDebug() {}

void TCompPathfindingAgent::load(const json & j, TEntityParseContext & ctx) {
	assert(j.count("navmesh") && "Can't find navmesh for agent");
	_navmeshName = j["navmesh"];
}

void TCompPathfindingAgent::update(float dt) {
	if (isAgentActive()) {
		assert(_id != -1 && "Agent MUST be created!");

		TCompCharacterController * controller = _controller;
		TCompNavmesh * navmesh = getNavmeshComponent();
		const Vector3 targetPos = navmesh->getAgentPosition(_id);
		Vector3 position = controller->getPosition();
		assert(_id != -1 && "Agent MUST be created!");

		// move with dtCrowd agent
		controller->turnToPosition(targetPos);
		float distance = Vector3::Distance(position, targetPos);
		if (distance > DISTANCE_TO_FOLLOW_THRESHOLD) {
			if (distance > DISTANCE_TO_TP)
				_id = navmesh->setAgentPosition(_id, position);
			else if (distance > DISTANCE_TO_WAIT_AGENT) {
				controller->move(targetPos - position);
				_lastPosition = position;
				navmesh->stopAgent(_id);
			}
			else {
				controller->move(targetPos - position);
				_lastPosition = position;
			}
		}

		// disable component when at target
		if (atDestination())
			setActive(false);
	}
	else {
		Utils::dbg("Agent active without ID");
		setActive(false);
	}
}

void TCompPathfindingAgent::registerMsgs() {
	DECL_MSG(TCompPathfindingAgent, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompPathfindingAgent, TMsgResetComponent, onComponentReset);
}

void TCompPathfindingAgent::declareInLua() {
	EngineScripting.declareComponent<TCompPathfindingAgent>
	(
		"TCompPathfindingAgent",
		"create", &TCompPathfindingAgent::create,
		"requestMove", &TCompPathfindingAgent::requestMove,
		"stay", &TCompPathfindingAgent::stay
	);
}

void TCompPathfindingAgent::init() {
	// we assume the agent is at destination when spawned so we disable the component
	setActive(false);

	// attribute initialization
	//CEntity * entityNav = getEntityByName(_navmeshName);
	// assert(entityNav && "Navmesh entity not found");	// navmesh should be loaded before all pathfinding components
	//_navmesh = entityNav->getComponent<TCompNavmesh>();
	_controller = getComponent<TCompCharacterController>();
	_transform = getComponent<TCompTransform>();
	if (_controller.isValid()) {
		TCompCharacterController* controller = _controller;
		_lastPosition = controller->getPosition();
	}

	if (!_controller.isValid()) {
		Utils::dbg("TCompPathfindingAgent : No character controller found");
		return;
	}
	TCompCharacterController * controllerComp = _controller;

	_height = controllerComp->getHeight();
	_radius = controllerComp->getRadius();
	_speed = controllerComp->getMovementSpeed();

}

void TCompPathfindingAgent::requestMove(const Vector3 & pos) {
	TCompNavmesh * navmesh = getNavmeshComponent();
	navmesh->requestMoveTarget(_id, pos);
	_targetPos = pos;
	setActive(true);
}

void TCompPathfindingAgent::stay() {
	TCompNavmesh * navmesh = getNavmeshComponent();
	navmesh->stopAgent(_id);
	setActive(false);
}

void TCompPathfindingAgent::create() {
	TCompNavmesh * navmesh = getNavmeshComponent();
	TCompTransform * transform = _transform;
	assert(transform && navmesh && "Navmesh component not found");
	TCompName * nameComp = getComponent<TCompName>();
	std::string name = (nameComp ? nameComp->getName() : "");

	if (_height > navmesh->getNavMeshData().getAgentHeight())
		Utils::dbg("WARNING: Pathfinding agent %s is higher than the maximum allowed by navmesh", name.c_str());
	if (_radius > navmesh->getNavMeshData().getAgentRadius())
		Utils::dbg("WARNING: Pathfinding agent %s is wider than the intended by the navmesh", name.c_str());

	// register agent in dtCrowd
	_id = navmesh->registerAgent
	(
		transform->getPosition(),
		_height,
		_radius,
		_speed,
		this
	);
	if (_id == -1)
		Utils::dbg("WARNING: Couldn't create pathfinding agent. Maximum amount of agents reached?");

}

void TCompPathfindingAgent::release() {
	TCompNavmesh * navmesh = getNavmeshComponent();
	if (navmesh && navmesh->agentExists(_id))
		navmesh->releaseAgent(_id);
	_id = -1;
	setActive(false);
}

bool TCompPathfindingAgent::atDestination() {
	TCompTransform * transform = _transform;
	if (!transform) return false;

	return (Vector3::Distance(_targetPos, transform->getPosition()) < _radius + DISTANCE_TO_DESTINATION_THRESHOLD);
}

bool TCompPathfindingAgent::isAgentActive()
{
	TCompNavmesh * navmesh = getNavmeshComponent();
	return (navmesh && navmesh->agentExists(_id));
}

TCompNavmesh* TCompPathfindingAgent::getNavmeshComponent() {
	if (_navmesh.isValid())
		return _navmesh;
	else {
		CEntity* entity = getEntityByName(_navmeshName);
		if (!entity) return nullptr;
		_navmesh = entity->getComponent<TCompNavmesh>();
		return _navmesh;
	}
}

void TCompPathfindingAgent::onEntityCreated(const TMsgEntityCreated & msg) {
	init();
}

void TCompPathfindingAgent::onComponentReset(const TMsgResetComponent & msg)
{
	release();
	setActive(false);
}
