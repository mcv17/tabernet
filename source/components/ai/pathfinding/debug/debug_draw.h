#pragma once
#include "../navmesh_data.h"

class NavmeshDebugDraw {

public:
	NavmeshDebugDraw(int cap = 512);
	~NavmeshDebugDraw();

	bool isActive;

	void createFromNavmesh(const dtNavMesh& mesh, const Vector4 & col = Vector4(1, 1, 1, 1));
	void vertex(const float* pos, const Vector4 & color = Vector4(1, 1, 1, 1));
	void vertex(const Vector3 & pos, const Vector4 & color = Vector4(1, 1, 1, 1));
	void clear();
	void end();
	void draw(const Vector3 & pos, const Vector4 & color);

private:
	// Explicitly disabled copy constructor and copy assignment operator.
	NavmeshDebugDraw(const NavmeshDebugDraw&);
	NavmeshDebugDraw& operator=(const NavmeshDebugDraw&);

	struct SimpleVertex {
		VEC3 Pos;
		Vector4 Color;
		SimpleVertex() = default;
		SimpleVertex(VEC3 newPos, Vector4 newColor) : Pos(newPos), Color(newColor) {}
	};

	int m_size;
	int m_cap;
	SimpleVertex * m_verts;
	unsigned int * m_ind;
	CMesh m_mesh;

	void resize(int cap);
	void createNavmeshPoly(const dtNavMesh& mesh, dtPolyRef ref, const Vector4 col);

};