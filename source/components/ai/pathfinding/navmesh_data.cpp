#include "mcv_platform.h"
#include "navmesh_data.h"
#include "Recast.h"
#include "DetourNavMesh.h"
#include "DetourNavMeshBuilder.h"

/* Recast default params */
#define DEFAULT_CELL_SIZE														0.3f
#define DEFAULT_CELL_HEIGHT													0.2f
#define DEFAULT_WALKABLE_SLOPE_ANGLE								45.0f

#define DEFAULT_AGENT_HEIGHT												1.5f
#define DEFAULT_AGENT_RADIUS												0.5f
#define DEFAULT_AGENT_MAX_CLIMB											0.2f

#define DEFAULT_REGION_MIN_SIZE											8
#define DEFAULT_REGION_MERGE_SIZE										20

#define DEFAULT_MAX_EDGE_LENGTH											12.0f
#define DEFAULT_MAX_EDGE_ERROR											1.3f
#define DEFAULT_VERTS_PER_POLY											6

#define DEFAULT_DETAIL_MESH_SAMPLE_DISTANCE					6.0f
#define DEFAULT_DETAIL_MESH_MAX_SAMPLE_ERROR				1.0f

/* DetourCrowd default params */
#define DEFAULT_MAX_AGENTS													25
#define DEFAULT_MAX_AGENT_RADIUS										10.0f

#define DEFAULT_AGENT_MAX_ACCELERATION							2 << 16
#define DEFAULT_AGENT_COLLISION_RANGE_MULTIPLIER		12.0f
#define DEFAULT_AGENT_PATH_OPT_RANGE_MULTIPLIER			30.0f
#define DEFAULT_AGENT_UPDATE_FLAGS									DT_CROWD_ANTICIPATE_TURNS | DT_CROWD_OBSTACLE_AVOIDANCE | DT_CROWD_SEPARATION
#define DEFAULT_AGENT_SEPARATION_WEIGHT							3.0f
#define DEFAULT_AGENT_OBSTACLE_AVOIDANCE_TYPE				3

#define DEFAULT_TILE_SIZE														32

static const int NAVMESHSET_MAGIC = 'M' << 24 | 'S' << 16 | 'E' << 8 | 'T'; //'MSET';
static const int NAVMESHSET_VERSION = 1;

enum PolyAreas {
	POLYAREA_GROUND,
	POLYAREA_WATER,
	POLYAREA_ROAD,
	POLYAREA_DOOR,
	POLYAREA_GRASS,
	POLYAREA_JUMP,
};

enum PolyFlags {
	POLYFLAGS_WALK = 0x01,		// Ability to walk (ground, grass, road)
	POLYFLAGS_SWIM = 0x02,		// Ability to swim (water).
	POLYFLAGS_DOOR = 0x04,		// Ability to move through doors.
	POLYFLAGS_JUMP = 0x08,		// Ability to jump.
	POLYFLAGS_DISABLED = 0x10,		// Disabled polygon
	POLYFLAGS_ALL = 0xffff	// All abilities.
};

struct NavMeshSetHeader {
	int magic;
	int version;
	int numTiles;
	dtNavMeshParams params;
};

struct NavMeshTileHeader {
	dtTileRef tileRef;
	int dataSize;
};

inline unsigned int ilog2(unsigned int v) {
	unsigned int r;
	unsigned int shift;
	r = (v > 0xffff) << 4; v >>= r;
	shift = (v > 0xff) << 3; v >>= shift; r |= shift;
	shift = (v > 0xf) << 2; v >>= shift; r |= shift;
	shift = (v > 0x3) << 1; v >>= shift; r |= shift;
	r |= (v >> 1);
	return r;
}

inline unsigned int nextPow2(unsigned int v) {
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	return v;
}

// -----------------------------------------------

NavmeshData::NavmeshData() :
	m_keepInterResults(false),
	m_totalBuildTimeMs(0),
	m_triareas(0),
	m_solid(0),
	m_chf(0),
	m_cset(0),
	m_pmesh(0),
	m_dmesh(0),
	m_partitionType(PartitionType::WATERSHED)
{
	resetCommonSettings();
	m_navQuery = dtAllocNavMeshQuery();
	m_crowd = dtAllocCrowd();
}
		
NavmeshData::~NavmeshData()
{
	cleanup();

	dtFreeNavMeshQuery(m_navQuery);
	m_navQuery = 0;
	dtFreeCrowd(m_crowd);
	m_crowd = 0;
}

// -----------------------------------------------

int NavmeshData::getMaxAgents() const {
	return DEFAULT_MAX_AGENTS;
}

void NavmeshData::loadInputGeometry(const std::string & filepath) {
	m_geom.load(&m_ctx, filepath);

	char text[64];
	int gw = 0, gh = 0;
	const float* bmin = m_geom.getNavMeshBoundsMin();
	const float* bmax = m_geom.getNavMeshBoundsMax();
	rcCalcGridSize(bmin, bmax, m_cellSize, &gw, &gh);
	const int tw = (gw + m_tileSize - 1) / m_tileSize;
	const int th = (gh + m_tileSize - 1) / m_tileSize;

	// Max tiles and max polys affect how the tile IDs are caculated.
	// There are 22 bits available for identifying a tile and a polygon.
	int tileBits = rcMin((int)ilog2(nextPow2(tw*th)), 14);
	if (tileBits > 14) tileBits = 14;
	int polyBits = 22 - tileBits;
	m_maxTiles = 1 << tileBits;
	m_maxPolysPerTile = 1 << polyBits;
}

void NavmeshData::loadNavmeshFromFile(const char* path) {
	FILE* fp = fopen(path, "rb");
	if (!fp) return;

	// Read header.
	NavMeshSetHeader header;
	size_t readLen = fread(&header, sizeof(NavMeshSetHeader), 1, fp);
	if (readLen != 1) {
		fclose(fp);
		return;
	}
	if (header.magic != NAVMESHSET_MAGIC) {
		fclose(fp);
		return;
	}
	if (header.version != NAVMESHSET_VERSION) {
		fclose(fp);
		return;
	}

	dtNavMesh* mesh = dtAllocNavMesh();
	if (!mesh) {
		fclose(fp);
		return;
	}
	dtStatus status = mesh->init(&header.params);
	if (dtStatusFailed(status)) {
		fclose(fp);
		return;
	}

	// Read tiles.
	for (int i = 0; i < header.numTiles; ++i) {
		NavMeshTileHeader tileHeader;
		readLen = fread(&tileHeader, sizeof(tileHeader), 1, fp);
		if (readLen != 1)
			return;

		if (!tileHeader.tileRef || !tileHeader.dataSize)
			break;

		unsigned char* data = (unsigned char*)dtAlloc(tileHeader.dataSize, DT_ALLOC_PERM);
		if (!data) break;
		memset(data, 0, tileHeader.dataSize);
		readLen = fread(data, tileHeader.dataSize, 1, fp);
		if (readLen != 1)
			return;

		mesh->addTile(data, tileHeader.dataSize, DT_TILE_FREE_DATA, tileHeader.tileRef, 0);
	}

	fclose(fp);

	m_navMesh = mesh;
	m_navQuery->init(m_navMesh, 2048);
}

void NavmeshData::saveNavmeshToFile(const char * path) {
	if (!m_navMesh) return;
	const dtNavMesh* mesh = m_navMesh;

	FILE* fp = fopen(path, "wb");
	if (!fp)
		return;

	// Store header.
	NavMeshSetHeader header;
	header.magic = NAVMESHSET_MAGIC;
	header.version = NAVMESHSET_VERSION;
	header.numTiles = 0;
	for (int i = 0; i < mesh->getMaxTiles(); ++i) {
		const dtMeshTile* tile = mesh->getTile(i);
		if (!tile || !tile->header || !tile->dataSize) continue;
		header.numTiles++;
	}
	memcpy(&header.params, mesh->getParams(), sizeof(dtNavMeshParams));
	fwrite(&header, sizeof(NavMeshSetHeader), 1, fp);

	// Store tiles.
	for (int i = 0; i < mesh->getMaxTiles(); ++i) {
		const dtMeshTile* tile = mesh->getTile(i);
		if (!tile || !tile->header || !tile->dataSize) continue;

		NavMeshTileHeader tileHeader;
		tileHeader.tileRef = mesh->getTileRef(tile);
		tileHeader.dataSize = tile->dataSize;
		fwrite(&tileHeader, sizeof(tileHeader), 1, fp);

		fwrite(tile->data, tile->dataSize, 1, fp);
	}

	fclose(fp);
}

int NavmeshData::addAgent(const float * pos, float height, float radius, float speed) {
	dtCrowdAgentParams params;
	memset(&params, 0, sizeof(params));
	params.height = height;
	params.radius = radius;
	params.maxSpeed = speed;
	params.maxAcceleration = DEFAULT_AGENT_MAX_ACCELERATION;
	params.collisionQueryRange = params.radius * DEFAULT_AGENT_COLLISION_RANGE_MULTIPLIER;
	params.pathOptimizationRange = params.radius * DEFAULT_AGENT_PATH_OPT_RANGE_MULTIPLIER;
	params.separationWeight = DEFAULT_AGENT_SEPARATION_WEIGHT;
	params.updateFlags = DEFAULT_AGENT_UPDATE_FLAGS;
	params.obstacleAvoidanceType = DEFAULT_AGENT_OBSTACLE_AVOIDANCE_TYPE;

	return m_crowd->addAgent(pos, &params);
}

void NavmeshData::debugInMenu()
{
	const float* bmin = m_geom.getNavMeshBoundsMin();
	const float* bmax = m_geom.getNavMeshBoundsMax();
	int gw = 0, gh = 0;
	rcCalcGridSize(bmin, bmax, m_cellSize, &gw, &gh);
	const int tw = (gw + m_tileSize - 1) / m_tileSize;
	const int th = (gh + m_tileSize - 1) / m_tileSize;

	char text[64];
	ImGui::Text("Tiled Navmesh Settings");
	ImGui::DragInt("Tile Size", &m_tileSize, 16, 16, m_maxTiles);
	snprintf(text, 64, "Tiles  %d x %d", tw, th);
	ImGui::Text(text);
	snprintf(text, 64, "Max Tiles  %d", m_maxTiles);
	ImGui::Text(text);
	snprintf(text, 64, "Max Polys per Tile  %d", m_maxPolysPerTile);
	ImGui::Text(text);

	ImGui::Separator();
	ImGui::Text("Rasterization");
	ImGui::DragFloat("Cell Size", &m_cellSize, 0.1f, 1.0f, 0.01f);
	ImGui::DragFloat("Cell Height", &m_cellHeight, 0.1f, 1.0f, 0.01f);

	snprintf(text, 64, "Voxels  %d x %d", gw, gh);
	ImGui::Text(text);

	ImGui::Separator();
	ImGui::Text("Agent");
	ImGui::DragFloat("Height", &m_agentHeight, 0.1f, 5.0f, 0.1f);
	ImGui::DragFloat("Radius", &m_agentRadius, 0.0f, 5.0f, 0.1f);
	ImGui::DragFloat("Max Climb", &m_agentMaxClimb, 0.1f, 5.0f, 0.1f);
	ImGui::DragFloat("Max Slope", &m_agentMaxSlope, 0.0f, 90.0f, 1.0f);

	ImGui::Separator();
	ImGui::Text("Region");
	ImGui::DragFloat("Min Region Size", &m_regionMinSize, 0.0f, 150.0f, 1.0f);
	ImGui::DragFloat("Merged Region Size", &m_regionMergeSize, 0.0f, 150.0f, 1.0f);

	ImGui::Separator();
	ImGui::Text("Partitioning");
	if (ImGui::RadioButton("Watershed", m_partitionType == PartitionType::WATERSHED))
		m_partitionType = PartitionType::WATERSHED;
	if (ImGui::RadioButton("Monotone", m_partitionType == PartitionType::MONOTONE))
		m_partitionType = PartitionType::MONOTONE;
	if (ImGui::RadioButton("Layers", m_partitionType == PartitionType::LAYERS))
		m_partitionType = PartitionType::LAYERS;

	ImGui::Separator();
	ImGui::Text("Polygonization");
	ImGui::DragFloat("Max Edge Length", &m_edgeMaxLen, 0.0f, 50.0f, 1.0f);
	ImGui::DragFloat("Max Edge Error", &m_edgeMaxError, 0.1f, 3.0f, 0.1f);
	ImGui::DragFloat("Verts Per Poly", &m_vertsPerPoly, 3.0f, 12.0f, 1.0f);

	ImGui::Separator();
	ImGui::Text("Detail Mesh");
	ImGui::DragFloat("Sample Distance", &m_detailSampleDist, 0.0, 1.0f);
	ImGui::DragFloat("Max Sample Error", &m_detailSampleMaxError, 0.0f, 16.0f, 1.0f);
}

void NavmeshData::load(const json & j, TEntityParseContext & ctx) {
	m_tileSize = j.value("tile_size", m_tileSize);

	if (j.count("rasterization")) {
		json rasterSettings = j["rasterization"];
		m_cellSize = rasterSettings.value("cell_size", m_cellSize);
		m_cellHeight = rasterSettings.value("cell_height", m_cellHeight);
	}

	if (j.count("agent")) {
		json agentSettings = j["agent"];
		m_agentHeight = agentSettings.value("height", m_agentHeight);
		m_agentRadius = agentSettings.value("radius", m_agentRadius);
		m_agentMaxClimb = agentSettings.value("max_climb", m_agentMaxClimb);
		m_agentMaxSlope = agentSettings.value("max_slope", m_agentMaxSlope);
	}

	if (j.count("region")) {
		json regionSettings = j["region"];
		m_regionMinSize = regionSettings.value("min_size", m_regionMinSize);
		m_regionMergeSize = regionSettings.value("merged_size", m_regionMergeSize);
	}

	std::string type = j.value("partitioning", "watershed");
	if (type == "watershed")
		m_partitionType = PartitionType::WATERSHED;
	else if (type == "monotone")
		m_partitionType = PartitionType::MONOTONE;
	else if (type == "layers")
		m_partitionType = PartitionType::LAYERS;
	else
		Utils::fatal("Error reading partitioning type %s", type.c_str());

	if (j.count("polygonization")) {
		json polygSettings = j["polygonization"];
		m_edgeMaxLen = polygSettings.value("max_edge_length", m_edgeMaxLen);
		m_edgeMaxError = polygSettings.value("max_edge_error", m_edgeMaxError);
		m_vertsPerPoly = polygSettings.value("verts_per_poly", m_vertsPerPoly);
	}

	if (j.count("detail_mesh")) {
		json detailMeshSettings = j["detail_mesh"];
		m_detailSampleDist = detailMeshSettings.value("sample_distance", m_detailSampleDist);
		m_detailSampleMaxError = detailMeshSettings.value("max_sample_error", m_detailSampleMaxError);
	}

}

void NavmeshData::cleanup() {
	delete [] m_triareas;
	m_triareas = 0;
	rcFreeHeightField(m_solid);
	m_solid = 0;
	rcFreeCompactHeightfield(m_chf);
	m_chf = 0;
	rcFreeContourSet(m_cset);
	m_cset = 0;
	rcFreePolyMesh(m_pmesh);
	m_pmesh = 0;
	rcFreePolyMeshDetail(m_dmesh);
	m_dmesh = 0;
	dtFreeNavMesh(m_navMesh);
	m_navMesh = 0;
}

void NavmeshData::resetCommonSettings() {
	m_cellSize = DEFAULT_CELL_SIZE;
	m_cellHeight = DEFAULT_CELL_HEIGHT;
	m_agentMaxSlope = DEFAULT_WALKABLE_SLOPE_ANGLE;
	m_agentHeight = DEFAULT_AGENT_HEIGHT;
	m_agentMaxClimb = DEFAULT_AGENT_MAX_CLIMB;
	m_agentRadius = DEFAULT_AGENT_RADIUS;
	m_edgeMaxLen = DEFAULT_MAX_EDGE_LENGTH;
	m_edgeMaxError = DEFAULT_MAX_EDGE_ERROR;
	m_regionMinSize = DEFAULT_REGION_MIN_SIZE;
	m_regionMergeSize = DEFAULT_REGION_MERGE_SIZE;
	m_vertsPerPoly = DEFAULT_VERTS_PER_POLY;
	m_detailSampleDist = DEFAULT_DETAIL_MESH_SAMPLE_DISTANCE;
	m_detailSampleMaxError = DEFAULT_DETAIL_MESH_MAX_SAMPLE_ERROR;
	m_tileSize = DEFAULT_TILE_SIZE;

}

void NavmeshData::update(float dt) {
	m_crowd->update(dt, nullptr);
}

void NavmeshData::handleRender() {

}

// -----------------------------------------------

void NavmeshData::initializeBuildConfig(const float* bmin, const float* bmax) {
	// Init build configuration
	memset(&m_cfg, 0, sizeof(m_cfg));
	m_cfg.cs = m_cellSize;
	m_cfg.ch = m_cellHeight;
	m_cfg.walkableSlopeAngle = m_agentMaxSlope;
	m_cfg.walkableHeight = (int)ceilf(m_agentHeight / m_cfg.ch);
	m_cfg.walkableClimb = (int)floorf(m_agentMaxClimb / m_cfg.ch);
	m_cfg.walkableRadius = (int)ceilf(m_agentRadius / m_cfg.cs);
	m_cfg.maxEdgeLen = (int)(m_edgeMaxLen / m_cellSize);
	m_cfg.maxSimplificationError = m_edgeMaxError;
	m_cfg.minRegionArea = (int)rcSqr(m_regionMinSize);		// Note: area = size*size
	m_cfg.mergeRegionArea = (int)rcSqr(m_regionMergeSize);	// Note: area = size*size
	m_cfg.maxVertsPerPoly = (int)m_vertsPerPoly;
	m_cfg.tileSize = (int)m_tileSize;
	m_cfg.borderSize = m_cfg.walkableRadius + 3; // Reserve enough padding.
	m_cfg.width = m_cfg.tileSize + m_cfg.borderSize * 2;
	m_cfg.height = m_cfg.tileSize + m_cfg.borderSize * 2;
	m_cfg.detailSampleDist = m_detailSampleDist < 0.9f ? 0 : m_cellSize * m_detailSampleDist;
	m_cfg.detailSampleMaxError = m_cellHeight * m_detailSampleMaxError;

	// Set the area where the navigation will be build.
	// Here the bounds of the input mesh are used, but the
	// area could be specified by an user defined box, etc.
	rcVcopy(m_cfg.bmin, bmin);
	rcVcopy(m_cfg.bmax, bmax);
	m_cfg.bmin[0] -= m_cfg.borderSize*m_cfg.cs;
	m_cfg.bmin[2] -= m_cfg.borderSize*m_cfg.cs;
	m_cfg.bmax[0] += m_cfg.borderSize*m_cfg.cs;
	m_cfg.bmax[2] += m_cfg.borderSize*m_cfg.cs;
}

bool NavmeshData::rasterizeVoxelHeightfield() {
	// Allocate voxel heightfield where we rasterize our input data to.
	m_solid = rcAllocHeightfield();
	if (!m_solid) {
		assert(false && "buildNavigation: Out of memory 'solid'.");
		return false;
	}
	if (!rcCreateHeightfield(&m_ctx, *m_solid, m_cfg.width, m_cfg.height, m_cfg.bmin, m_cfg.bmax, m_cfg.cs, m_cfg.ch)) {
		assert(false && "buildNavigation: Could not create solid heightfield.");
		return false;
	}

	return true;
}

bool NavmeshData::createAreaTypes() {
	const float* verts = m_geom.getMesh()->getVerts();
	const int nverts = m_geom.getMesh()->getVertCount();
	const int* tris = m_geom.getMesh()->getTris();
	const int ntris = m_geom.getMesh()->getTriCount();
	const rcChunkyTriMesh* chunkyMesh = m_geom.getChunkyMesh();

	// Allocate array that can hold triangle flags.
	// If you have multiple meshes you need to process, allocate
	// and array which can hold the max number of triangles you need to process.
	m_triareas = new unsigned char[chunkyMesh->maxTrisPerChunk];
	if (!m_triareas)
	{
		assert(false && "buildNavigation: Out of memory 'm_triareas' (%d).", ntris);
		return false;
	}

	float tbmin[2], tbmax[2];
	tbmin[0] = m_cfg.bmin[0];
	tbmin[1] = m_cfg.bmin[2];
	tbmax[0] = m_cfg.bmax[0];
	tbmax[1] = m_cfg.bmax[2];
	int cid[512];// TODO: Make grow when returning too many items.
	const int ncid = rcGetChunksOverlappingRect(chunkyMesh, tbmin, tbmax, cid, 512);
	if (!ncid)
		return false;

	m_tileTriCount = 0;

	for (int i = 0; i < ncid; ++i)
	{
		const rcChunkyTriMeshNode& node = chunkyMesh->nodes[cid[i]];
		const int* ctris = &chunkyMesh->tris[node.i * 3];
		const int nctris = node.n;

		m_tileTriCount += nctris;

		memset(m_triareas, 0, nctris * sizeof(unsigned char));
		rcMarkWalkableTriangles(&m_ctx, m_cfg.walkableSlopeAngle,
			verts, nverts, ctris, nctris, m_triareas);

		if (!rcRasterizeTriangles(&m_ctx, verts, nverts, ctris, m_triareas, nctris, *m_solid, m_cfg.walkableClimb))
			return false;
	}

	if (!m_keepInterResults)
	{
		delete[] m_triareas;
		m_triareas = 0;
	}

	// Once all geometry is rasterized, we do initial pass of filtering to
	// remove unwanted overhangs caused by the conservative rasterization
	// as well as filter spans where the character cannot possibly stand.
	rcFilterLowHangingWalkableObstacles(&m_ctx, m_cfg.walkableClimb, *m_solid);
	rcFilterLedgeSpans(&m_ctx, m_cfg.walkableHeight, m_cfg.walkableClimb, *m_solid);
	rcFilterWalkableLowHeightSpans(&m_ctx, m_cfg.walkableHeight, *m_solid);

	return true;
}

bool NavmeshData::createRegions() {
	// Compact the heightfield so that it is faster to handle from now on.
	// This will result more cache coherent data as well as the neighbours
	// between walkable cells will be calculated.
	m_chf = rcAllocCompactHeightfield();
	if (!m_chf) {
		assert(false && "buildNavigation: Out of memory 'chf'.");
		return false;
	}
	if (!rcBuildCompactHeightfield(&m_ctx, m_cfg.walkableHeight, m_cfg.walkableClimb, *m_solid, *m_chf)) {
		assert(false && "buildNavigation: Could not build compact data.");
		return false;
	}

	if (!m_keepInterResults) {
		rcFreeHeightField(m_solid);
		m_solid = 0;
	}

	// Erode the walkable area by agent radius.
	if (!rcErodeWalkableArea(&m_ctx, m_cfg.walkableRadius, *m_chf)) {
		assert(false && "buildNavigation: Could not erode.");
		return false;
	}

	// (Optional) Mark areas.
	const ConvexVolume* vols = m_geom.getConvexVolumes();
	for (int i = 0; i < m_geom.getConvexVolumeCount(); ++i)
		rcMarkConvexPolyArea(&m_ctx, vols[i].verts, vols[i].nverts, vols[i].hmin, vols[i].hmax, (unsigned char)vols[i].area, *m_chf);

	// Partition the heightfield so that we can use simple algorithm later to triangulate the walkable areas.
	switch (m_partitionType) {
	case PartitionType::WATERSHED:
		// Prepare for region partitioning, by calculating distance field along the walkable surface.
		if (!rcBuildDistanceField(&m_ctx, *m_chf)) {
			assert(false && "buildNavigation: Could not build distance field.");
			return false;
		}

		// Partition the walkable surface into simple regions without holes.
		if (!rcBuildRegions(&m_ctx, *m_chf, m_cfg.borderSize, m_cfg.minRegionArea, m_cfg.mergeRegionArea)) {
			assert(false && "buildNavigation: Could not build watershed regions.");
			return false;
		}
		break;
	case PartitionType::MONOTONE:
		// Partition the walkable surface into simple regions without holes.
		// Monotone partitioning does not need distancefield.
		if (!rcBuildRegionsMonotone(&m_ctx, *m_chf, 0, m_cfg.minRegionArea, m_cfg.mergeRegionArea)) {
			assert(false && "buildNavigation: Could not build monotone regions.");
			return false;
		}
		break;
	case PartitionType::LAYERS:
		// Partition the walkable surface into simple regions without holes.
		if (!rcBuildLayerRegions(&m_ctx, *m_chf, 0, m_cfg.minRegionArea)) {
			assert(false && "buildNavigation: Could not build layer regions.");
			return false;
		}
		break;
	default:
		return false;
	}

	return true;
}

bool NavmeshData::createRegionContours() {
	// Create contours.
	m_cset = rcAllocContourSet();
	if (!m_cset) {
		assert(false && "buildNavigation: Out of memory 'cset'.");
		return false;
	}
	if (!rcBuildContours(&m_ctx, *m_chf, m_cfg.maxSimplificationError, m_cfg.maxEdgeLen, *m_cset)) {
		assert(false && "buildNavigation: Could not create contours.");
		return false;
	}

	if (m_cset->nconts == 0)
		return false;

	return true;
}

bool NavmeshData::buildPolyMesh() {
	// Build polygon navmesh from the contours.
	m_pmesh = rcAllocPolyMesh();
	if (!m_pmesh) {
		assert(false && "buildNavigation: Out of memory 'pmesh'.");
		return false;
	}
	if (!rcBuildPolyMesh(&m_ctx, *m_cset, m_cfg.maxVertsPerPoly, *m_pmesh)) {
		assert(false && "buildNavigation: Could not triangulate contours.");
		return false;
	}

	return true;
}

bool NavmeshData::createDetailMesh() {
	m_dmesh = rcAllocPolyMeshDetail();
	if (!m_dmesh) {
		assert(false && "buildNavigation: Out of memory 'pmdtl'.");
		return false;
	}

	if (!rcBuildPolyMeshDetail(&m_ctx, *m_pmesh, *m_chf, m_cfg.detailSampleDist, m_cfg.detailSampleMaxError, *m_dmesh)) {
		assert(false && "buildNavigation: Could not build detail mesh.");
		return false;
	}

	if (!m_keepInterResults) {
		rcFreeCompactHeightfield(m_chf);
		m_chf = 0;
		rcFreeContourSet(m_cset);
		m_cset = 0;
	}

	return true;
}

unsigned char * NavmeshData::createNavMesh(const int tx, const int ty, int & navDataSize) {
	// Only build the detour navmesh if we do not exceed the limit.
	unsigned char* navData = 0;
	navDataSize = 0;
	if (m_cfg.maxVertsPerPoly <= DT_VERTS_PER_POLYGON) {
		if (m_pmesh->nverts >= 0xffff) {
			// The vertex indices are ushorts, and cannot point to more than 0xffff vertices.
			assert(false && "Too many vertices per tile");
			return nullptr;
		}

		// Update poly flags from areas to be walkable.
		for (int i = 0; i < m_pmesh->npolys; ++i) {
			if (m_pmesh->areas[i] == RC_WALKABLE_AREA) {
				m_pmesh->areas[i] = POLYAREA_GROUND;
				m_pmesh->flags[i] = POLYFLAGS_WALK;
			}
		}

		dtNavMeshCreateParams params;
		memset(&params, 0, sizeof(params));
		params.verts = m_pmesh->verts;
		params.vertCount = m_pmesh->nverts;
		params.polys = m_pmesh->polys;
		params.polyAreas = m_pmesh->areas;
		params.polyFlags = m_pmesh->flags;
		params.polyCount = m_pmesh->npolys;
		params.nvp = m_pmesh->nvp;
		params.detailMeshes = m_dmesh->meshes;
		params.detailVerts = m_dmesh->verts;
		params.detailVertsCount = m_dmesh->nverts;
		params.detailTris = m_dmesh->tris;
		params.detailTriCount = m_dmesh->ntris;
		params.offMeshConVerts = m_geom.getOffMeshConnectionVerts();
		params.offMeshConRad = m_geom.getOffMeshConnectionRads();
		params.offMeshConDir = m_geom.getOffMeshConnectionDirs();
		params.offMeshConAreas = m_geom.getOffMeshConnectionAreas();
		params.offMeshConFlags = m_geom.getOffMeshConnectionFlags();
		params.offMeshConUserID = m_geom.getOffMeshConnectionId();
		params.offMeshConCount = m_geom.getOffMeshConnectionCount();
		params.walkableHeight = m_agentHeight;
		params.walkableRadius = m_agentRadius;
		params.walkableClimb = m_agentMaxClimb;
		params.tileX = tx;
		params.tileY = ty;
		params.tileLayer = 0;
		rcVcopy(params.bmin, m_pmesh->bmin);
		rcVcopy(params.bmax, m_pmesh->bmax);
		params.cs = m_cfg.cs;
		params.ch = m_cfg.ch;
		params.buildBvTree = true;

		if (!dtCreateNavMeshData(&params, &navData, &navDataSize)) {
			assert(false && "Could not build Detour navmesh.");
			return nullptr;
		}
	}
	else return nullptr;

	return navData;
}

unsigned char * NavmeshData::buildTileMesh(const int tx, const int ty, const float * bmin, const float * bmax, int& dataSize)
{
	if (!m_geom.getMesh())
	{
		assert(false && "buildNavigation: Input mesh is not specified.");
		return false;
	}

	// Step 1. Initialize build config.
	initializeBuildConfig(bmin, bmax);

	// Step 2. Rasterize input polygon soup.
	if (!rasterizeVoxelHeightfield())
		return nullptr;

	// Step 3. Create area types and filter walkable surfaces.
	if (!createAreaTypes())
		return nullptr;

	// Step 4. Partition walkable surface to simple regions.
	if (!createRegions())
		return nullptr;

	// Step 5. Trace and simplify region contours.
	if (!createRegionContours())
		return nullptr;

	// Step 6. Build polygons mesh from contours.
	if (!buildPolyMesh())
		return nullptr;

	// Step 7. Create detail mesh which allows to access approximate height on each polygon.
	if (!createDetailMesh())
		return nullptr;

	// At this point the navigation mesh data is ready, you can access it from m_pmesh.

	// Step 8. Create Detour data from Recast poly mesh.
	return createNavMesh(tx, ty, dataSize);
}

bool NavmeshData::build() {
	// in case a build has already been made
	cleanup();

	m_navMesh = dtAllocNavMesh();
	if (!m_navMesh) {
		assert(false && "Could not create Detour navmesh");
		return false;
	}

	dtNavMeshParams navParams;
	rcVcopy(navParams.orig, m_geom.getNavMeshBoundsMin());
	navParams.tileWidth = m_tileSize * m_cellSize;
	navParams.tileHeight = m_tileSize * m_cellSize;
	navParams.maxTiles = m_maxTiles;
	navParams.maxPolys = m_maxPolysPerTile;

	dtStatus status;

	status = m_navMesh->init(&navParams);
	if (dtStatusFailed(status)) {
		assert(false && "Could not init Detour navmesh");
		return false;
	}

	status = m_navQuery->init(m_navMesh, 2048);
	if (dtStatusFailed(status)) {
		assert(false && "Could not init Detour navmesh query");
		return false;
	}

	const float* bmin = m_geom.getNavMeshBoundsMin();
	const float* bmax = m_geom.getNavMeshBoundsMax();
	int gw = 0, gh = 0;
	rcCalcGridSize(bmin, bmax, m_cellSize, &gw, &gh);
	const int ts = (int)m_tileSize;
	const int tw = (gw + ts - 1) / ts;
	const int th = (gh + ts - 1) / ts;
	const float tcs = m_tileSize * m_cellSize;

	for (int y = 0; y < th; ++y) {
		for (int x = 0; x < tw; ++x) {
			m_lastBuiltTileBmin[0] = bmin[0] + x * tcs;
			m_lastBuiltTileBmin[1] = bmin[1];
			m_lastBuiltTileBmin[2] = bmin[2] + y * tcs;

			m_lastBuiltTileBmax[0] = bmin[0] + (x + 1)*tcs;
			m_lastBuiltTileBmax[1] = bmax[1];
			m_lastBuiltTileBmax[2] = bmin[2] + (y + 1)*tcs;

			int dataSize = 0;
			unsigned char* data = buildTileMesh(x, y, m_lastBuiltTileBmin, m_lastBuiltTileBmax, dataSize);
			if (data)
			{
				// Remove any previous data (navmesh owns and deletes the data).
				m_navMesh->removeTile(m_navMesh->getTileRefAt(x, y, 0), 0, 0);
				// Let the navmesh own the data.
				dtStatus status = m_navMesh->addTile(data, dataSize, DT_TILE_FREE_DATA, 0, 0);
				if (dtStatusFailed(status))
					dtFree(data);
			}
		}
	}

	// ------------

	return true;
}

bool NavmeshData::setupCrowd() {
	if (!m_crowd->init(DEFAULT_MAX_AGENTS, DEFAULT_MAX_AGENT_RADIUS, m_navMesh)) {
		dtFreeCrowd(m_crowd);
		assert(false && "Could not init Detour crowd");
		return false;
	}
	// Make polygons with 'disabled' flag invalid.
	m_crowd->getEditableFilter(0)->setExcludeFlags(POLYFLAGS_DISABLED);

	m_crowd->getEditableFilter(0)->setIncludeFlags(POLYFLAGS_ALL);

	// Setup local avoidance params to different qualities.
	dtObstacleAvoidanceParams avoidParams;
	// Use mostly default settings, copy from dtCrowd.
	memcpy(&avoidParams, m_crowd->getObstacleAvoidanceParams(0), sizeof(dtObstacleAvoidanceParams));

	// Low (11)
	avoidParams.velBias = 0.5f;
	avoidParams.adaptiveDivs = 5;
	avoidParams.adaptiveRings = 2;
	avoidParams.adaptiveDepth = 1;
	m_crowd->setObstacleAvoidanceParams(0, &avoidParams);

	// Medium (22)
	avoidParams.velBias = 0.5f;
	avoidParams.adaptiveDivs = 5;
	avoidParams.adaptiveRings = 2;
	avoidParams.adaptiveDepth = 2;
	m_crowd->setObstacleAvoidanceParams(1, &avoidParams);

	// Good (45)
	avoidParams.velBias = 0.5f;
	avoidParams.adaptiveDivs = 7;
	avoidParams.adaptiveRings = 2;
	avoidParams.adaptiveDepth = 3;
	m_crowd->setObstacleAvoidanceParams(2, &avoidParams);

	// High (66)
	avoidParams.velBias = 0.5f;
	avoidParams.adaptiveDivs = 7;
	avoidParams.adaptiveRings = 3;
	avoidParams.adaptiveDepth = 3;

	m_crowd->setObstacleAvoidanceParams(3, &avoidParams);

	return true;
}
