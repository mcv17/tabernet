#pragma once

#include "navmesh_data.h"
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "debug/debug_draw.h"

typedef unsigned int dtAgentID;

class TCompNavmesh : public TCompBase {

public:

	TCompNavmesh();
	~TCompNavmesh();

	void debugInMenu();
	void renderDebug();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	static void registerMsgs();

	void onEntityCreated(const TMsgEntityCreated & msg);

	void build();

	const Vector3 getAgentPosition(dtAgentID id);
	int setAgentPosition(dtAgentID id, const Vector3& pos);

	int registerAgent(const Vector3 pos, float height, float radius, float speed, CHandle comp);
	void requestMoveTarget(dtAgentID id, const Vector3 & pos);
	void stopAgent(dtAgentID id);
	void releaseAgent(dtAgentID id);
	bool agentExists(dtAgentID id);

	const NavmeshData & getNavMeshData() const { return *_navmeshData; }

	DECL_SIBLING_ACCESS();

private:

	NavmeshData * _navmeshData;
	NavmeshDebugDraw * _debugDraw;
	rcConfig _config;
	Vector3 _position;

	std::vector<CHandle> _agents;

	bool _navmeshLoaded = false;

};