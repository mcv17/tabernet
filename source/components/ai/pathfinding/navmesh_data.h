#pragma once

#include "geometry/InputGeom.h"
#include "DetourNavMesh.h"
#include "DetourCrowd.h"
#include "DetourNavMeshQuery.h"
#include "Recast.h"

class NavmeshData
{
	
public:
	NavmeshData();
	~NavmeshData();

	enum class PartitionType {
		//   - the classic Recast partitioning
		//   - creates the nicest tessellation
		//   - usually slowest
		//   - partitions the heightfield into nice regions without holes or overlaps
		//   - the are some corner cases where this method creates produces holes and overlaps
		//      - holes may appear when a small obstacles is close to large open area (triangulation can handle this)
		//      - overlaps may occur if you have narrow spiral corridors (i.e stairs), this make triangulation to fail
		WATERSHED = 0,
		//   - fastest
		//   - partitions the heightfield into regions without holes and overlaps (guaranteed)
		//   - creates long thin polygons, which sometimes causes paths with detours
		//   * use this if you want fast navmesh generation
		MONOTONE = 1,
		//   - quite fast
		//   - partitions the heighfield into non-overlapping regions
		//   - relies on the triangulation code to cope with holes (thus slower than monotone partitioning)
		//   - produces better triangles than monotone partitioning
		//   - does not have the corner cases of watershed partitioning
		//   - can be slow and create a bit ugly tessellation (still better than monotone)
		//     if you have large open areas with small obstacles (not a problem if you use tiles)
		//   * good choice to use for tiled navmesh with medium and small sized tiles
		LAYERS = 2
	};

	void handleRender();
	bool build();
	bool setupCrowd();

	class InputGeom& getInputGeom() { return m_geom; }
	class dtNavMesh* getNavMesh() { return m_navMesh; }
	class dtNavMeshQuery* getNavMeshQuery() { return m_navQuery; }
	class dtCrowd* getCrowd() { return m_crowd; }

	float getCellSize() const { return m_cellSize; }
	float getCellHeight() const { return m_cellHeight; }
	float getAgentHeight() const { return m_agentHeight; }
	float getAgentRadius() const { return m_agentRadius; }
	float getAgentMaxClimb() const { return m_agentMaxClimb; }
	float getAgentMaxSlope() const { return m_agentMaxSlope; }
	int getMaxAgents() const;

	unsigned char getNavMeshDrawFlags() const { return m_navMeshDrawFlags; }
	void setNavMeshDrawFlags(unsigned char flags) { m_navMeshDrawFlags = flags; }

	void loadInputGeometry(const std::string & filepath);
	void loadNavmeshFromFile(const char * path);
	void saveNavmeshToFile(const char * path);
	int addAgent(const float * pos, float height, float radius, float speed);
	bool isNavmeshInitialized() const { return m_navMesh != nullptr; }

	void update(float dt);
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);

private:
	// Explicitly disabled copy constructor and copy assignment operator.
	NavmeshData(const NavmeshData&);
	NavmeshData& operator=(const NavmeshData&);

	InputGeom m_geom;
	dtNavMesh* m_navMesh;
	dtNavMeshQuery* m_navQuery;
	dtCrowd* m_crowd;

	unsigned char m_navMeshDrawFlags;

	float m_cellSize;
	float m_cellHeight;
	float m_agentHeight;
	float m_agentRadius;
	float m_agentMaxClimb;
	float m_agentMaxSlope;
	float m_regionMinSize;
	float m_regionMergeSize;
	float m_edgeMaxLen;
	float m_edgeMaxError;
	float m_vertsPerPoly;
	float m_detailSampleDist;
	float m_detailSampleMaxError;
	PartitionType m_partitionType;

	int m_maxTiles;
	int m_maxPolysPerTile;
	int m_tileSize;
	float m_lastBuiltTileBmin[3];
	float m_lastBuiltTileBmax[3];
	int m_tileTriCount;

	rcContext m_ctx;

	bool m_keepInterResults;
	float m_totalBuildTimeMs;

	unsigned char* m_triareas;
	rcHeightfield* m_solid;
	rcCompactHeightfield* m_chf;
	rcContourSet* m_cset;
	rcPolyMesh* m_pmesh;
	rcConfig m_cfg;
	rcPolyMeshDetail* m_dmesh;

	void cleanup();
	void resetCommonSettings();

	void initializeBuildConfig(const float* bmin, const float* bmax);
	bool rasterizeVoxelHeightfield();
	bool createAreaTypes();
	bool createRegions();
	bool createRegionContours();
	bool buildPolyMesh();
	bool createDetailMesh();
	unsigned char * createNavMesh(const int tx, const int ty, int & navDataSize);

	unsigned char* buildTileMesh(const int tx, const int ty, const float* bmin, const float* bmax, int& dataSize);
};
