#pragma once

#include "comp_navmesh.h"
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompPathfindingAgent : public TCompBase {

public:

	void debugInMenu();
	void renderDebug();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	static void registerMsgs();
	static void declareInLua();

	void requestMove(const Vector3 & pos);
	void stay();
	void create();
	void release();

	bool atDestination();
	bool isAgentActive();

	DECL_SIBLING_ACCESS()

private:

	dtAgentID _id = -1;
	std::string _navmeshName;
	CHandle _controller, _transform, _navmesh;
	Vector3 _targetPos, _lastPosition = Vector3::Zero;
	float _height, _radius, _speed;

	TCompNavmesh* getNavmeshComponent();

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onComponentReset(const TMsgResetComponent & msg);
	void init();

};