#include "mcv_platform.h"
#include "comp_bt_enemy_ranged.h"
#include "entity/entity_parser.h"
#include "components/controllers/comp_character_controller.h"
#include "components/common/comp_render.h"
#include "utils/time.h"
#include "modules/module_physics.h"
#include "utils/phys_utils.h"
#include "components/common/comp_name.h"
#include "components/animations/comp_enemyRanged_animator.h"
#include "../comp_ranged_projectile_shooter.h"
#include "components/common/comp_hierarchy.h"
#include "components/lighting/comp_light_point.h"
#include "components/common/comp_aabb.h"

DECL_OBJ_MANAGER("ai_bt_enemy_ranged", TCompBTEnemyRanged);

inline bool checkForTimeup(CClock & timer, float timeup) {
	if (timer.isStarted()) {
		if (timer.isPaused())
			timer.resume();
		if (timer.elapsed() > timeup) {
			timer.stop();
			return true;
		}
	}
	else
		timer.start();

	return false;
}

inline bool isAtPosition(const TCompTransform& transform, VEC3 pos, float threshold = 0.2f) {
	return Vector3::Distance(pos - transform.getPosition(), VEC3::Zero) < threshold;
}

void TCompBTEnemyRanged::debugInMenu() {
	TCompBase::debugInMenu();
	// default info for BT
	TCompBehaviourTree::debugInMenu();
}

void TCompBTEnemyRanged::load(const json & j, TEntityParseContext & ctx) {
	// VERY IMPORTANT LINE, WOULDN'T LOAD THE BT OTHERWISE!
	TCompBehaviourTree::load(j, ctx);

	// set C++ variables
	_idleWarTime = j.value("idle_war_time", _idleWarTime);
	_visionCone.angle = j.value("vision_cone_angle", _visionCone.angle);
	_visionCone.depth = j.value("vision_cone_depth", _visionCone.depth);
	_flinchCooldown = j.value("flinch_cooldown", _flinchCooldown);
	_flinchInternalChanceMultiplier = j.value("flinch_internal_chance_multiplier", _flinchInternalChanceMultiplier);
	if (j.count("waypoints")) {
		Vector3 wp;
		std::string buffer;
		const json & jWPs = j["waypoints"];
		for (const json & jWaypoint : jWPs) {
			buffer = jWaypoint.get<std::string>();
			sscanf(buffer.c_str(), "%f %f %f", &wp.x, &wp.y, &wp.z);
			_waypoints.push_back(wp);
		}
	}

	_shoutSurpriseSoundEvent = j.value("shout_surprise_sound_event", _shoutSurpriseSoundEvent);
	_shoutAggresiveSoundEvent = j.value("shout_aggressive_sound_event", _shoutAggresiveSoundEvent);
	_castIceSoundEvent = j.value("cast_ice_sound_event", _castIceSoundEvent);
	_shoutAggresiveIdleWarChance = j.value("shout_aggressive_chance", _shoutAggresiveIdleWarChance);
	if (j.count("shout_aggressive_cooldown") > 0)
		_shoutAggresiveIdleWarCooldown.setTime(j["shout_aggressive_cooldown"]);

	loadDropsData(j);

	// Shooting params
	_shootDistance = j.value("shoot_distance", _shootDistance);
	_minTimeToCastShoot = j.value("min_time_to_cast_shooting", _minTimeToCastShoot);
	_maxTimeToCastShoot = j.value("max_time_to_cast_shooting", _maxTimeToCastShoot);
	_timeBetweenShootings = j.value("time_between_shootings", _timeBetweenShootings);
	if (j.count("projectile_slots") > 0 && j["projectile_slots"].is_array()) {
		for (auto& jElem : j["projectile_slots"]) {
			_projectiles.push_back(jElem);
		}
	}
	if (j.count("shooting_offsets") > 0 && j["shooting_offsets"].is_array()) {
		for (auto& jElem : j["shooting_offsets"]) {
			_shootingOffsets.push_back(loadVector3(jElem));
		}
	}
	// Fire breath params
	if (j.count("fire_breath")) {
		json jFireBreath = j["fire_breath"];
		_fireBreathDamage = jFireBreath.value("damage", _fireBreathDamage);
		_fireBreathDPS = jFireBreath.value("dps", _fireBreathDPS);
		_minTimeToCastCloseRange = jFireBreath.value("min_time_to_cast_close_range", _minTimeToCastCloseRange);
		_maxTimeToCastCloseRange = jFireBreath.value("max_time_to_cast_close_range", _maxTimeToCastCloseRange);
		_timeBetweenCloseRangeAttacks = jFireBreath.value("time_between_attacks", _timeBetweenCloseRangeAttacks);
	}

	// set BT variables
	setVariable("isArena", j.value("is_arena", false));
	setVariable("numWaypoints", _waypoints.size());
	setVariable("forgetDistance", j.value("forget_distance", 20.0f));
	setVariable("chaseAgainDistance",  j.value("chase_again_distance", 8.0f));
}

void TCompBTEnemyRanged::renderDebug() {
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;

	VEC3 origin = transform->getPosition();
	Vector4 color = Vector4(1.0f, 1.0f, 0.0f, 1.0f);
	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);

	drawHorizontalCone(origin, yaw, _visionCone.angle, _visionCone.depth, 8, color);
}

void TCompBTEnemyRanged::init() {
	_timers["idle_war"] = CClock();
	_timers["chase"] = CClock();
	_timers["idle"] = CClock();
	_timers["fall_to_the_ground"] = CClock(_ragdollsTime);
	_timers["dying"] = CClock(_dyingTime);
	_timers["prepare_attack"] = CClock(0.5f);
	_timers["wait"] = CClock();
	_timers["flinch"] = CClock(_flinchCooldown);

	TCompTransform * transform = _transform;
	if (variableExists("shortRangeDistance"))
		_attackCone.depth = getVariable("shortRangeDistance");
}

void TCompBTEnemyRanged::implementFunctions() {
	PROFILE_FUNCTION("TCompBTEnemyRanged:implementFunctions");
	IMPLEMENT_CONDITION("viewPlayer", TCompBTEnemyRanged, viewPlayer);
	IMPLEMENT_CONDITION("canShootPlayer", TCompBTEnemyRanged, canShootPlayer);
	IMPLEMENT_CONDITION("shouldDebuffPlayer", TCompBTEnemyRanged, shouldDebuffPlayer);

	IMPLEMENT_ACTION("Fall to the ground", TCompBTEnemyRanged, fallToTheGround);
	IMPLEMENT_ACTION("Dying", TCompBTEnemyRanged, dying);
	IMPLEMENT_ACTION("Death", TCompBTEnemyRanged, death);
	IMPLEMENT_ACTION("Flinch", TCompBTEnemyRanged, flinch);
	IMPLEMENT_ACTION("Idle war", TCompBTEnemyRanged, idleWar);
	IMPLEMENT_ACTION("Chase", TCompBTEnemyRanged, chase);
	IMPLEMENT_ACTION("Seek waypoint", TCompBTEnemyRanged, seekWpt);
	IMPLEMENT_ACTION("Next waypoint", TCompBTEnemyRanged, nextWpt);
	IMPLEMENT_ACTION("Idle", TCompBTEnemyRanged, idle);
	IMPLEMENT_ACTION("Forget", TCompBTEnemyRanged, forget);
	IMPLEMENT_ACTION("Shoot", TCompBTEnemyRanged, shoot);
	IMPLEMENT_ACTION("Shoot sniper", TCompBTEnemyRanged, shootSniper);
	IMPLEMENT_ACTION("Cast fire attack", TCompBTEnemyRanged, prepareFireAttack);
	IMPLEMENT_ACTION("Cast ice attack", TCompBTEnemyRanged, prepareIceAttack);
	IMPLEMENT_ACTION("Cast sniper attack", TCompBTEnemyRanged, prepareSniperAttack);
	IMPLEMENT_ACTION("Fire breath", TCompBTEnemyRanged, fireBreath);
	IMPLEMENT_ACTION("Wait shooting casting", TCompBTEnemyRanged, waitCastingShootingTime);
	IMPLEMENT_ACTION("Wait close range casting", TCompBTEnemyRanged, waitCastingCloseRangeTime);
	IMPLEMENT_ACTION("Wait between close range", TCompBTEnemyRanged, waitBetweenCloseRange);
	IMPLEMENT_ACTION("Wait between shootings", TCompBTEnemyRanged, waitBetweenShootings);
	IMPLEMENT_ACTION("Stop casting", TCompBTEnemyRanged, removeCastParticleEffects);
	IMPLEMENT_ACTION("Start attack animation", TCompBTEnemyRanged, startAttackAnimation);

	IMPLEMENT_ACTION("prepareForBattle", TCompBTEnemyRanged, prepareForBattle);
}

void TCompBTEnemyRanged::updateVariables()
{
	TCompTransform* transform = getComponent<TCompTransform>();
	TCompTransform* playerTransform = getPlayerTransform();

	if (transform && playerTransform) {
		float distanceToPlayer = VEC3::Distance(transform->getPosition(), playerTransform->getPosition());
		setVariable("distanceToPlayer", distanceToPlayer);
	}
}

bool TCompBTEnemyRanged::raycastToPlayer()
{
	TCompTransform * transform = _transform;
	TCompTransform * playerTransf = getPlayerTransform();
	TCompCharacterController * controller = _controller;
	if (!transform || !playerTransf || !controller || controller->isDead()) return false;

	Vector3 origin = transform->getPosition() + _raycastPoint;
	Vector3 dir = playerTransf->getPosition() - origin + Vector3::Up * _raycastPoint.y;
	float distance = Vector3::Distance(dir, Vector3::Zero);
	dir.Normalize();
	origin += dir * (controller->getRadius() + 0.2f);

	auto filter = CModulePhysics::FilterGroup::Scenario | CModulePhysics::FilterGroup::Player;
	physx::PxSphereGeometry shape =  physx::PxSphereGeometry(0.2f);
	shape.radius = 0.2f;

	auto hitData = PhysUtils::sweep(origin, dir, distance, shape, filter);

	if (hitData.hasBlock) {
		CHandle hitObj;
		hitObj.fromVoidPtr(hitData.block.actor->userData);
		if (hitObj.isValid()) {
			CEntity * entity = hitObj.getOwner();
			if (entity) {
				TCompName * compName = entity->getComponent<TCompName>();
				if (compName && strcmp(compName->getName(), "Player") == 0)
					return true;
			}
		}
	}

	return false;
}

bool TCompBTEnemyRanged::viewPlayer() {
	TCompTransform* transform = _transform;
	TCompTransform* playerTransform = getPlayerTransform();
	TCompHealth* playerHealth = getPlayerHealth();
	if (!playerHealth || !playerTransform)
		return false;
	
	if (playerHealth && playerHealth->isDead()) 
		return false;

	if (isArena() || getVariable("playerDetected") == true) 
		return true;

	return _visionCone.isInRange(*transform, playerTransform->getPosition());
}

bool TCompBTEnemyRanged::canShootPlayer() {
	// for now we leave it like this, later will ceck if something is in the way
	_playerAtSight = _raycastEvent.doMethodFetcher(this, &TCompBTEnemyRanged::raycastToPlayer, _playerAtSight);
	return (_playerAtSight && variableExists("distanceToPlayer") && variableExists("chaseAgainDistance") &&  
					getVariable("distanceToPlayer") < getVariable("chaseAgainDistance"));
}

bool TCompBTEnemyRanged::shouldDebuffPlayer() {
	CHandle statusEffectsHandle = getPlayerStatusEffects();
	if (!statusEffectsHandle.isValid()) return false;
	TCompStatusEffects* statusEff = statusEffectsHandle;
	return !statusEff->hasStatusEffect("movement_speed_ice_projectile");
}

ActionResult TCompBTEnemyRanged::fallToTheGround() {
	CClock& clock = _timers["fall_to_the_ground"];
	if (clock.isStarted()) {
		// wait for ragdolls
		if (clock.isFinished()) {
			clock.stop();
			return ActionResult::LEAVE;
		}
	}
	else {
		clock.start();

		TCompPathfindingAgent* agent = _agent;
		if (agent && agent->isAgentActive()) agent->release();
		removeCastParticleEffects();


		// Destroy collider
		if (_colliderActive) {
			// Attach AABB to ragdoll
			TCompRagdoll* c_ragdoll = getComponent<TCompRagdoll>();;
			if (c_ragdoll) {
				TMsgRagdollActivated msg;
				if (c_ragdoll->ragdoll.num_bones > 0)
					msg.link = c_ragdoll->ragdoll.bones[0].link;
				if (msg.link != nullptr)
					getEntity().sendMsgToComp<TCompLocalAABB>(msg);
			}

			TCompCollider* col = getComponent<TCompCollider>();
			if (col) {
				TMsgToogleComponent msg{ false };
				getEntity().sendMsgToComp<TCompCollider>(msg);
			}

			_colliderActive = false;
		}

	}
	return ActionResult::STAY;
}

ActionResult TCompBTEnemyRanged::dying() {
	// if animation ended
	CClock& clock = _timers["dying"];
	if (clock.isStarted()) {
		if (clock.isFinished()) {
			clock.stop();
			return ActionResult::LEAVE;
		}

		// Update particles pos.
		TCompSkeleton * skeleton = getComponent<TCompSkeleton>();
		if (skeleton && _vanishParticles)
			_vanishParticles->setSystemPosition(skeleton->getBoneAbsPos("Bip001 Spine"));
	}
	else {
		clock.start();

		// Activate the vanish mesh state and the cte data
		TCompRender * render = getComponent<TCompRender>();
		if (!render) return ActionResult::LEAVE;

		// Activate particles on destruction.
		Vector3 posToSpawnParticlesAt;
		TCompTransform * transform = getComponent<TCompTransform>();
		if (transform)
			posToSpawnParticlesAt = transform->getPosition();

		TCompSkeleton * skeleton = getComponent<TCompSkeleton>();
		if (skeleton)
			posToSpawnParticlesAt = skeleton->getBoneAbsPos("Bip001 Spine");

		EngineLogicManager.setStatusVanishEntity(CHandle(this).getOwner(), posToSpawnParticlesAt);

		if (!_vanishParticles)
			_vanishParticles = EngineLogicManager.SpawnParticle("data/particles/vanish_ranged.particles", posToSpawnParticlesAt);
	}

	return ActionResult::STAY;
}

ActionResult TCompBTEnemyRanged::death() {
	TCompCharacterController* con = _controller;
	if (con && !con->isDead()) {
		if (!EnginePool.disableEntity(this->getEntity(), PoolType::Ranged)) //Check if its in a pool and disables it
			getEntity().destroy(); //If not, destroys the entity
		if (!EnginePool.disableEntity(_projectileShooter.getOwner(), PoolType::RangedShooter))
			_projectileShooter.getOwner().destroy();
	}

	_vanishParticles = nullptr;

	setActive(false);

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::flinch() {
	if (_animator.isValid() && _skeleton.isValid()) {
		CClock& cooldown = _timers["flinch"];
		if (!cooldown.isFinished() && cooldown.isStarted())
			return ActionResult::LEAVE;

		TCompSkeleton* skeleton = _skeleton;
		TCompEnemyRangedAnimator* animator = _animator;

		bool isInjured = animator->getVariable("isInjured")->as<bool>();
		if (!isInjured) {
			float speed = animator->getVariable("speed")->as<float>();
			bool isAttacking = animator->getVariable("attacking")->as<bool>();
			if (speed == 0.0f && !isAttacking && RNG.b(_flinchInternalChanceMultiplier * _lastDamageReceived / 100.0f)) {
				// execute injured animation
				animator->setIsInjured(true);
				return ActionResult::STAY;
			}
			else
				return ActionResult::LEAVE;
		}
		else {
			// wait for injured animation to end
			if (skeleton->isAnimationOver("ranged_injured")) {
				animator->setIsInjured(false);
				turnToPlayer();
				cooldown.start();
				return ActionResult::LEAVE;
			}
			return ActionResult::STAY;
		}
	}
	else
		return wait(1.0f);
}

ActionResult TCompBTEnemyRanged::idleWar() {
	// Turn to player
	turnToPlayer();

	// Play aggresive sound sometimes
	if (_shoutAggresiveIdleWarCooldown.isFinished() && RNG.b(_shoutAggresiveIdleWarChance) && !_shoutAggresiveSoundEvent.empty()) {
		TCompTransform * transform = _transform;
		EngineAudio.playEvent(_shoutAggresiveSoundEvent, transform->getPosition());
		_shoutAggresiveIdleWarCooldown.start();
	}

	return wait(_idleWarTime);
}

ActionResult TCompBTEnemyRanged::shootIce() {
	TCompTransform * transform = getComponent<TCompTransform>();
	TCompRangedProjectileShooter* shooter = _projectileShooter;
	if (!transform || !shooter) return ActionResult::LEAVE;

	// Turn to player
	turnToPlayer();

	TCompSkeleton* skeleton = _skeleton;
	if (!skeleton || skeleton->isAnimationOver("ranged_attack")) {
		EngineLogicManager.SpawnParticle(_circleParticleSystemName, transform->getPosition() + Vector3::Up * 0.01f, transform->getFront());
		
		shooter->shoot(_projectiles[0], true);
		if (_animator.isValid()) {
			TCompEnemyRangedAnimator* animator = _animator;
			animator->setIsAttacking(false);
		}

		removeCastParticleEffects();

		return ActionResult::LEAVE;
	}
	else if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	else
		return ActionResult::STAY;
}

ActionResult TCompBTEnemyRanged::chase() {
	TCompTransform * transform = _transform;
	TCompPathfindingAgent * agent = _agent;
	TCompTransform * playerTransf = getPlayerTransform();
	if (!transform || !agent || !playerTransf) return ActionResult::LEAVE;

	if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;

	_playerAtSight = _raycastEvent.doMethodFetcher(this, &TCompBTEnemyRanged::raycastToPlayer, _playerAtSight);
	float distanceToPlayer = getVariable("distanceToPlayer");
	if (distanceToPlayer < getVariable("forgetDistance") && !canShootPlayer()) {
		_pathfindingEvent.doMethodEvent(agent, &TCompPathfindingAgent::requestMove, playerTransf->getPosition());
		return ActionResult::STAY;
	}
	else {
		agent->stay();
		return ActionResult::LEAVE;
	}
}

ActionResult TCompBTEnemyRanged::seekWpt() {
	if (viewPlayer())
		return ActionResult::ABORT;

	TCompCharacterController * controller = getComponent<TCompCharacterController>();
	if (!controller) return ActionResult::LEAVE;
	TCompTransform * transform = getComponent<TCompTransform>();
	if (!transform) return ActionResult::LEAVE;

	TCompTransform* playerTransform = getPlayerTransform();
	if (!playerTransform) return ActionResult::LEAVE;
	VEC3 destination = _waypoints.at(_curWaypoint);

	// Turn to waypoint
	controller->turnToPosition(destination);
	// Move towards waypoint
	controller->move(transform->getFront());

	if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	if (isAtPosition(*transform, destination))
		return ActionResult::LEAVE;
	else
		return ActionResult::STAY;
}

ActionResult TCompBTEnemyRanged::nextWpt() {
	iterateWaypoint();
	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::idle() {
	if (checkForTimeup(_timers["idle"], 0.25f))
		return ActionResult::LEAVE;
	else {
		if (interrupted())
			return ActionResult::INTERRUPT_AND_STAY;
		return ActionResult::STAY;
	}
}

ActionResult TCompBTEnemyRanged::turnToPlayer() {
	TCompCharacterController * controller = getComponent<TCompCharacterController>();
	TCompTransform* playerTransform = getPlayerTransform();
	if (!controller || !playerTransform)
		return ActionResult::LEAVE;

	controller->turnToPosition(playerTransform->getPosition());

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::prepareForBattle() {
	// Create pathfinding agent
	TCompPathfindingAgent * agent = _agent;
	if (agent && !agent->isAgentActive()) {
		agent->create();
		
		if (!_shoutSurpriseSoundEvent.empty()) {
			TCompTransform * transform = _transform;
			EngineAudio.playEvent(_shoutSurpriseSoundEvent, transform->getPosition());
			_shoutAggresiveIdleWarCooldown.start();
		}
	}

	// Play idle war animation
	if (_animator.isValid()) {
		TCompEnemyRangedAnimator* animator = _animator;
		animator->setInAttackPose(true);
	}

	if (!getVariable("playerDetected"))
		setVariable("playerDetected", true);

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::startAttackAnimation() {
	if (_animator.isValid()) {
		TCompEnemyRangedAnimator* animator = _animator;
		animator->setIsAttacking(true);
	}
	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::forget() {
	// Release pathfinding agent
	TCompPathfindingAgent * agent = _agent;
	if (agent) agent->release();

	// Stop idle war animation
	if (_animator.isValid()) {
		TCompEnemyRangedAnimator* animator = _animator;
		animator->setInAttackPose(false);
	}

	removeCastParticleEffects();

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::shoot() {
	switch (_shootType) {
	case EShootType::FIRE:
		return shootFire();
		break;
	case EShootType::ICE:
		return shootIce();
		break;
	}
	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::shootSniper() {
	TCompTransform * transform = _transform;
	TCompRangedProjectileShooter* shooter = _projectileShooter;
	TCompSkeleton* skeleton = _skeleton;
	if (!transform || !shooter || !skeleton) return ActionResult::LEAVE;

	turnToPlayer();

	// If attack anim is finished
	if (skeleton->isAnimationOver("ranged_attack")) {
		// Shoot a numProjectiles projectiles burst with a time space defined by _burstTimer

		//EngineLogicManager.SpawnParticle(_circleParticleSystemName, transform->getPosition() + Vector3::Up * 0.01f, transform->getFront());

		// Exit animation now that it finished
		if (_animator.isValid()) {
			TCompEnemyRangedAnimator* animator = _animator;
			animator->setIsAttacking(false);
		}

		// Shoot projectile
		shooter->shoot(_projectiles[0] ,true);


	}
	else if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	else
		return ActionResult::STAY;
}

ActionResult TCompBTEnemyRanged::shootFire() {
	TCompTransform * transform = _transform;
	TCompRangedProjectileShooter* shooter = _projectileShooter;
	TCompSkeleton* skeleton = _skeleton;
	if (!transform || !shooter || !skeleton) return ActionResult::LEAVE;

	// Turn to player
	turnToPlayer();

	// If attack anim is finished
	if (skeleton->isAnimationOver("ranged_attack")) {
		// Shoot a numProjectiles projectiles burst with a time space defined by _burstTimer

		// Exit animation now that it finished
		if (_animator.isValid()) {
			TCompEnemyRangedAnimator* animator = _animator;
			animator->setIsAttacking(false);
		}

		// If timer is not started means that we can shoot a projectile
		if (!_burstTimer.isStarted()) {
			bool accurate = getVariable("accurateShoot");
			// Shoot projectile
			shooter->shoot(_projectiles[1], accurate);

			int numProjectiles = getVariable("numProjectiles");
			numProjectiles--;
			setVariable("numProjectiles", numProjectiles);

			// Reset timer for the next projectile (if there is)
			if (numProjectiles > 0) {
				_burstTimer.start();

				return ActionResult::STAY;
			}
			else {
				_burstTimer.stop();

				EngineLogicManager.SpawnParticle(_circleParticleSystemName, transform->getPosition() + Vector3::Up * 0.01f, transform->getFront());
				removeCastParticleEffects();

				return ActionResult::LEAVE;
			}
		}
		else {
			// If clock is finished we stop it so it can be started for next projectile
			if (_burstTimer.isFinished())
				_burstTimer.stop();

			return ActionResult::STAY;
		}
	}
	else if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	else
		return ActionResult::STAY;
}

ActionResult TCompBTEnemyRanged::fireBreath() {
	turnToPlayer();

	TCompTransform* transform = _transform;
	TCompTransform* playerTransform = getPlayerTransform();

	// instantiate fire breath (particles & effects)
	TEntityParseContext ctx;
	CTransform tempTransform = *transform;
	tempTransform.setPosition(tempTransform.getPosition() + transform->getFront() * 0.5f + transform->getUp());
	ctx.root_transform = tempTransform;
	parseScene("data/prefabs/enemies/ranged/fireBreath.json", ctx);
	// if player is in range inside the cone inflict the damage
	if (_attackCone.isInRange(*transform, playerTransform->getPosition())) {
		CHandle effHandle = getPlayerStatusEffects();
		TCompStatusEffects* playerStatusEff = effHandle;
		CHandle playerHandle = effHandle.getOwner();
		TMsgHitDamage msg{ this->getEntity(), _fireBreathDamage, Vector3::Zero };
		playerHandle.sendMsg(msg);
		TDamageOverTimeStatusEffect eff;
		eff.damagePerSecond = _fireBreathDPS;
		eff.setTime(5.0f);
		playerStatusEff->addStatusEffect(eff);
	}

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::prepareFireAttack() {
	if (!_particlesCreated) {
		CClock& timer = _timers["prepare_attack"];
		if (!timer.isStarted()) {
			timer.start();
			return ActionResult::STAY;
		}

		if (timer.isFinished()) {
			_shootType = EShootType::FIRE;

			spawnShooter(_shootingOffsets[1]);

			CEntity* shooter = _projectileShooter.getOwner();
			TCompTransform* shooterTransf = shooter->getComponent<TCompTransform>();
			_prep1Particles = EngineLogicManager.SpawnParticle(_fireCastParticleSystemName, getEntity(), "Bip001 L Hand", Vector3(0.08f, 0.045f, 0.0f));
			_prep2Particles = EngineLogicManager.SpawnParticle(_fireCastParticleSystemName, getEntity(), "Bip001 R Hand", Vector3(0.08f, 0.045f, 0.0f));
			_particlesCreated = true;

			timer.stop();
			return ActionResult::LEAVE;
		}

		if (interrupted())
			return ActionResult::INTERRUPT_AND_STAY;

		return ActionResult::STAY;
	}

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::prepareIceAttack() {
	if (!_particlesCreated) {
		CClock& timer = _timers["prepare_attack"];
		if (!timer.isStarted()) {
			timer.start();

			if (!_castIceSoundEvent.empty()) {
				TCompTransform * transform = _transform;
				EngineAudio.playEvent(_castIceSoundEvent, transform->getPosition());
			}

			return ActionResult::STAY;
		}

		if (timer.isFinished()) {
			_shootType = EShootType::ICE;

			spawnShooter(_shootingOffsets[0]);

			CEntity* shooter = _projectileShooter.getOwner();
			TCompTransform* shooterTransf = shooter->getComponent<TCompTransform>();
			_prep1Particles = EngineLogicManager.SpawnParticle(_iceCast1ParticleSystemName, shooter);
			_prep2Particles = EngineLogicManager.SpawnParticle(_iceCast2ParticleSystemName, shooter);
			_prep1Particles->setPriority(_iceCast1ParticleSystemPriority);
			_prep2Particles->setPriority(_iceCast2ParticleSystemPriority);
			_particlesCreated = true;

			timer.stop();
			return ActionResult::LEAVE;
		}

		if (interrupted())
			return ActionResult::INTERRUPT_AND_STAY;

		return ActionResult::STAY;

	}

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::prepareSniperAttack() {
	if (!_particlesCreated) {
		CClock& timer = _timers["prepare_attack"];
		if (!timer.isStarted()) {
			timer.start();

			return ActionResult::STAY;
		}

		if (timer.isFinished()) {

			spawnShooter(_shootingOffsets[0]);

			CEntity* shooter = _projectileShooter.getOwner();
			TCompTransform* shooterTransf = shooter->getComponent<TCompTransform>();

			timer.stop();
			return ActionResult::LEAVE;
		}

		if (interrupted())
			return ActionResult::INTERRUPT_AND_STAY;

		return ActionResult::STAY;

	}

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyRanged::waitCastingShootingTime() {
	turnToPlayer();
	return wait(RNG.f(_minTimeToCastShoot, _maxTimeToCastShoot));
}

ActionResult TCompBTEnemyRanged::waitCastingCloseRangeTime() {
	turnToPlayer();
	return wait(RNG.f(_minTimeToCastCloseRange, _maxTimeToCastCloseRange));
}

ActionResult TCompBTEnemyRanged::waitBetweenCloseRange() {
	turnToPlayer();
	return wait(_timeBetweenCloseRangeAttacks);
}

ActionResult TCompBTEnemyRanged::waitBetweenShootings() {
	turnToPlayer();
	if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	return wait(_timeBetweenShootings);
}

ActionResult TCompBTEnemyRanged::wait(float time) {
	auto& timer = _timers["wait"];
	if (!timer.isStarted()) {
		timer.setTime(time);
		timer.start();
	}
	if (timer.isFinished()) {
		timer.stop();
		return ActionResult::LEAVE;
	}

	if (interrupted()) {
		timer.pause();
		return ActionResult::INTERRUPT_AND_STAY;
	}
	else if (timer.isPaused())
		timer.resume();

	return ActionResult::STAY;
}

ActionResult TCompBTEnemyRanged::wait(CClock & timer) {
	if (!timer.isStarted())
		timer.start();

	if (timer.isFinished()) {
		timer.stop();
		return ActionResult::LEAVE;
	}

	if (interrupted()) {
		timer.pause();
		return ActionResult::INTERRUPT_AND_STAY;
	}
	else if (timer.isPaused())
		timer.resume();

	return ActionResult::STAY;
}

bool TCompBTEnemyRanged::isArena() {
	return getVariable("isArena");
}

void TCompBTEnemyRanged::iterateWaypoint() {
	_curWaypoint = (_curWaypoint + 1) % _waypoints.size();
}

void TCompBTEnemyRanged::spawnShooter(const Vector3& pos) {
	TCompRangedProjectileShooter* shooter = _projectileShooter;
	if (!shooter) {
		CEntity* entity = EnginePool.spawn(PoolType::RangedShooter);
		shooter = entity->getComponent<TCompRangedProjectileShooter>();
		_projectileShooter = shooter;
	}
	shooter->init(getEntity(), pos);
}

ActionResult TCompBTEnemyRanged::removeCastParticleEffects() {
	if (_particlesCreated) {
		_prep1Particles->setCanSpawnParticles(false);
		EngineLogicManager.removeSpawnedParticle(_prep1Particles, 0.5f);
		_prep1Particles = nullptr;
		_prep2Particles->setCanSpawnParticles(false);
		EngineLogicManager.removeSpawnedParticle(_prep2Particles, 0.5f);
		_prep2Particles = nullptr;
		_particlesCreated = false;
	}

	return ActionResult::LEAVE;
}

TCompBTEnemyRanged::~TCompBTEnemyRanged() {
	removeCastParticleEffects();
}

void TCompBTEnemyRanged::registerMsgs() {
	DECL_MSG(TCompBTEnemyRanged, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompBTEnemyRanged, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompBTEnemyRanged, TMsgBulletDamage, onBulletDamage);
	DECL_MSG(TCompBTEnemyRanged, TMsgHitDamage, onOtherDamage);
	DECL_MSG(TCompBTEnemyRanged, TMsgNailDamage, onNailDamage);
	DECL_MSG(TCompBTEnemyRanged, TMsgApplyBuff, onBuffRecieved);
	DECL_MSG(TCompBTEnemyRanged, TMsgRemoveBuff, onBuffRemoved);
	DECL_MSG(TCompBTEnemyRanged, TMsgEntityDead, onEntityDead);
	DECL_MSG(TCompBTEnemyRanged, TMsgDeleteIAController, deleteIAController);
	DECL_MSG(TCompBTEnemyRanged, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompBTEnemyRanged, TMsgResetComponent, onComponentReset);
	DECL_MSG(TCompBTEnemyRanged, TMsgAlertAI, onAlertAI);
}

void TCompBTEnemyRanged::declareInLua() {
	EngineScripting.declareComponent<TCompBTEnemyRanged>
		(
			"TCompBTEnemyRanged",
			"getVariable", &TCompBTEnemyRanged::getVariable,
			"setVariable", &TCompBTEnemyRanged::setVariable,
			"interrupt", &TCompBTEnemyRanged::interrupt,
			"alert", &TCompBTEnemyRanged::alert
			);
}

void TCompBTEnemyRanged::onEntityCreated(const TMsgEntityCreated & msg) {
	_agent = getComponent<TCompPathfindingAgent>();
	_transform = getComponent<TCompTransform>();
	_controller = getComponent<TCompCharacterController>();
	_animator = getComponent<TCompEnemyRangedAnimator>();
	_skeleton = getComponent<TCompSkeleton>();
	assert(_transform.isValid() || _controller.isValid() && "Enemy ranged lacks the required components");
	if (!_agent.isValid()) setActive(false);
	TCompTransform * transform = _transform;

	init();
}

void TCompBTEnemyRanged::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompBTEnemyRanged::onBulletDamage(const TMsgBulletDamage & msg) {
	interrupt("damageReceived");
	_lastDamageReceived = msg.damage;
}

void TCompBTEnemyRanged::onOtherDamage(const TMsgHitDamage & msg) {
	interrupt("damageReceived");
	_lastDamageReceived = msg.damage;
}

void TCompBTEnemyRanged::onNailDamage(const TMsgNailDamage & msg) {
	interrupt("damageReceived");
	_lastDamageReceived = msg.damage;
}

void TCompBTEnemyRanged::onBuffRecieved(const TMsgApplyBuff & msg) {
	// Send msg to render to change materials.
	TMsgSetMeshState msgRender = { 4 }; // 4 is used for active totem buff state.
	this->getEntity().sendMsg(msgRender);

	_damageBuff = msg.damage;

	TCompHealth * cHealth = getComponent<TCompHealth>();
	if (cHealth)
		cHealth->setArmour(msg.armour);
}

void TCompBTEnemyRanged::onBuffRemoved(const TMsgRemoveBuff & msg) {
	// Send msg to render to change materials.
	TMsgSetMeshState msgRender = { 0 }; // 0 is used for normal state.
	this->getEntity().sendMsg(msgRender);

	_damageBuff = 1.0f;

	TCompHealth * cHealth = getComponent<TCompHealth>();
	if (cHealth)
		cHealth->setArmour(1.0f);
}

void TCompBTEnemyRanged::onEntityDead(const TMsgEntityDead & msg) {
	// Drop spawn
	TCompTransform * transform = getComponent<TCompTransform>();
	if (transform)
		spawnDrops(*transform);

	interrupt("isDead");
}

void TCompBTEnemyRanged::deleteIAController(const TMsgDeleteIAController & msg) {
	CHandle iaController = this;
	iaController.destroy();
}

void TCompBTEnemyRanged::onComponentReset(const TMsgResetComponent & msg) {
	reset();

	_colliderActive = true;

	//setvariable("isarena", true);
	setVariable("isDead", false);
	setVariable("playerDetected", false);
	setVariable("isArena", false);
	setVariable("numWaypoints", _waypoints.size());

	_damageBuff = 1.0f;
	_waypoints.clear();
	TCompPathfindingAgent* agent = _agent;
	if (agent && agent->isAgentActive()) agent->release();
	removeCastParticleEffects();
	if (_animator.isValid()) {
		TCompEnemyRangedAnimator* animator = _animator;
		animator->setIsAttacking(false);
	}
	
	// instantiate projectile shooter
	CEntity* entity = EnginePool.spawn(PoolType::RangedShooter);
	TCompRangedProjectileShooter* shooter = entity->getComponent<TCompRangedProjectileShooter>();
	_projectileShooter = shooter;
	shooter->init(getEntity(), Vector3::Up * 2.2f + Vector3::Forward * 0.3f);

	for (auto & timer : _timers) {
		timer.second.stop();
	}
}

void TCompBTEnemyRanged::onAlertAI(const TMsgAlertAI & msg) {
	alert();
}

void TCompBTEnemyRanged::onToggleComponent(const TMsgToogleComponent & msg) {
	TCompBase::onToggleComponent(msg);
	removeCastParticleEffects();
}

void TCompBTEnemyRanged::alert() {
	setVariable("isArena", true);
}
