#include "mcv_platform.h"
#include "comp_bt_enemy_melee.h"
#include "components/controllers/comp_character_controller.h"
#include "components/common/comp_render.h"
#include "components/ai/squads/comp_squad_boss.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_aabb.h"
#include "skeleton/comp_skeleton.h"
#include "components/animations/comp_enemyMelee_animator.h"
#include "components/common/comp_material_buffers.h"
#include "utils/time.h"
#include "utils/phys_utils.h"
#include <string>

DECL_OBJ_MANAGER("ai_bt_enemy_melee", TCompBTEnemyMelee);

void TCompBTEnemyMelee::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Checkbox("msg: ", &_msgrceivd);
	ImGui::Separator();
	ImGui::Text("Current node: ");
	ImGui::SameLine();
	ImGui::TextColored(ImVec4(0.75f, 0.75f, 1.0f, 1.0f), getNodeName().c_str());
	ImGui::Separator();
	ImGui::Text("Player slot asigned: %s", getPlayerSlot().c_str());
	ImGui::Text("Distance parameters: ");
	ImGui::DragFloat("Attack distance", &_attackDistance, 0.1f, 0.0f, 3.0f);
	ImGui::DragFloat("Leap Max distance", &_leapMaxDistance, 0.1f, 0.0f, 15.0f);
	ImGui::DragFloat("Leap Min distance", &_leapMinDistance, 0.1f, 0.0f, 15.0f);
	ImGui::DragFloat("Step distance", &_stepDistance, 0.01f, 0.0f, 5.0f);
	ImGui::Separator();
	ImGui::DragFloat("Damage", &_attackDamage, 0.1f, 0.0f, 50.0f);
	ImGui::DragFloat("Attack probability: ", &_attackProbability, 0.1f, 0.0f, 50.0f);
	ImGui::DragFloat("Attack speed: ", &_attackSpeed, 0.1f, 0.0f, 50.0f);
	ImGui::Text("Buffed parameters: ");
	ImGui::Separator();
	ImGui::DragFloat("Apply buff prob: ", &_buffProbability, 0.1f, 0.0f, 50.0f);
	ImGui::DragFloat("Buff attack multiplier: ", &_buffAttackMultiplier, 0.1f, 0.0f, 50.0f);
	ImGui::Text("Buffed attack: %d", getBuffedAttack());
	ImGui::DragFloat("Buff speed multiplier: ", &_buffAttackSpeedMultiplier, 0.1f, 0.0f, 50.0f);
	ImGui::Text("Buffed speed: %d", getBuffedAttackSpeed());
	ImGui::DragFloat("Buff HP multiplier: ", &_buffHPMultiplier, 0.1f, 0.0f, 50.0f);
	ImGui::Text("Buffed HP: %d", getBuffedHP());
	ImGui::Separator();
	ImGui::Text("Others: ");
	ImGui::DragFloat("Difficulty", &_difficulty, 0.01f, 0.0f, 1.0f);
	ImGui::Text("Is Quadruped? ");
	ImGui::SameLine();
	ImGui::Checkbox("##EMQuadCheckbox", &_isQuadruped);
}

void TCompBTEnemyMelee::init() {
	
}

void TCompBTEnemyMelee::load(const json & j, TEntityParseContext & ctx) {
	//!!!!!!!!!!!!!!!!!!!!!!!!!!
	TCompBehaviourTree::load(j, ctx);

	_attackDistance = j.value("attackDistance", _attackDistance);
	_leapMaxDistance = j.value("leapMaxDistance", _leapMaxDistance);
	_leapMinDistance = j.value("leapMinDistance", _leapMinDistance);
	_attackDamage = j.value("attackDamage", _attackDamage);
	_attackSpeed = j.value("attackSpeed", _attackSpeed);
	_attackProbability = j.value("attackProbability", _attackProbability);

	_buffProbability = j.value("buffProbability", _buffProbability);
	_buffAttackMultiplier = j.value("buffAttackMultiplier", _buffAttackMultiplier);
	_buffAttackSpeedMultiplier = j.value("buffAttackSpeedMultiplierMultiplier", _buffAttackSpeedMultiplier);
	_buffHPMultiplier = j.value("buffHPMultiplier", _buffHPMultiplier);

	_visionCone.angle = j.value("coneVision_angle", _visionCone.angle);
	_visionCone.depth = j.value("coneVision_depth", _visionCone.depth);
	_buffColour = loadVector4(j, "buffColour");
	_difficulty = j.value("difficulty", _difficulty);
	_isQuadruped = j.value("isQuadruped", _isQuadruped);

	_attackEvent = j.value("attack_audio_event", _attackEvent);
	_idleEvent = j.value("idle_audio_event", _idleEvent);
	_idleWarEvent = j.value("idlewar_audio_event", _idleWarEvent);
	_shoutEvent = j.value("shout_audio_event", _shoutEvent);

}

void TCompBTEnemyMelee::renderDebug()
{
	if (TCompTransform* trans = getComponent<TCompTransform>()) {
		float yaw, pitch;
		trans->getAngles(&yaw, &pitch);
		
		drawHorizontalCone(trans->getPosition(), yaw, _visionCone.angle, _visionCone.depth, 16, 
			(_playerSeen || _forceChasePlayer) ? VEC4(1.0f, 0.0f, 0.0f, 1.0f) : VEC4(1.0f, 1.0f, 0.0f, 1.0f));
	}
}

void TCompBTEnemyMelee::implementFunctions()
{
	PROFILE_FUNCTION("TCompBTEnemyMelee:implementFunctions");
	IMPLEMENT_CONDITION("isPlayerAtSight", TCompBTEnemyMelee, isPlayerAtSight);
	IMPLEMENT_CONDITION("isPlayerInLeapRange", TCompBTEnemyMelee, isPlayerInLeapRange);
	IMPLEMENT_CONDITION("isInAttackRange", TCompBTEnemyMelee, isInAttackRange);
	/*
	IMPLEMENT_CONDITION("isBossDead", TCompBTEnemyMelee, isBossDead);
	IMPLEMENT_CONDITION("askForAvailableSlot", TCompBTEnemyMelee, askForAvailableSlot);
	IMPLEMENT_CONDITION("processAttack", TCompBTEnemyMelee, processAttack);
	*/
	IMPLEMENT_ACTION("idleState", TCompBTEnemyMelee, idleState);
	IMPLEMENT_ACTION("prepareForBattle", TCompBTEnemyMelee, prepareForBattle);
	IMPLEMENT_ACTION("applyBuff", TCompBTEnemyMelee, applyBuffState);
	IMPLEMENT_ACTION("chasePlayer", TCompBTEnemyMelee, chasePlayerState);
	IMPLEMENT_ACTION("leap", TCompBTEnemyMelee, leapState);
	IMPLEMENT_ACTION("attack", TCompBTEnemyMelee, attackState);
	IMPLEMENT_ACTION("flinch", TCompBTEnemyMelee, flinchState);
	IMPLEMENT_ACTION("death", TCompBTEnemyMelee, deathState);
	IMPLEMENT_ACTION("dying", TCompBTEnemyMelee, dyingState);
	IMPLEMENT_ACTION("idleWar", TCompBTEnemyMelee, idleWarState);
	/*
	IMPLEMENT_ACTION("seekWpt", TCompBTEnemyMelee, seekWptState);
	IMPLEMENT_ACTION("changeWpt", TCompBTEnemyMelee, changeWptState);
	IMPLEMENT_ACTION("rest", TCompBTEnemyMelee, restState);
	IMPLEMENT_ACTION("goToSlot", TCompBTEnemyMelee, goToSlotState);
	IMPLEMENT_ACTION("runArround", TCompBTEnemyMelee, runArroundState); 
	IMPLEMENT_ACTION("goToCombatZone", TCompBTEnemyMelee, goToCombatZone);
	IMPLEMENT_ACTION("facePlayer", TCompBTEnemyMelee, facePlayer);
	IMPLEMENT_ACTION("attack2", TCompBTEnemyMelee, attack2);
	IMPLEMENT_ACTION("attack3", TCompBTEnemyMelee, attack3);
	IMPLEMENT_ACTION("prepareForBattle", TCompBTEnemyMelee, prepareForBattle);*/
}

ActionResult TCompBTEnemyMelee::idleState()
{
	// Check if any idle anim is running
	if (TCompSkeleton* skel = _skeletonHandle) {
		if(skel->isAnimationExecuting(skel->getAnimationID("melee_idle_stand"))
		|| skel->isAnimationExecuting(skel->getAnimationID("melee_idle_crouch"))) {
			if (skel->getAnimationPercentage("melee_idle_stand") > 0.8f
				|| skel->getAnimationPercentage("melee_idle_crouch") > 0.8f) {
				return ActionResult::LEAVE;
			}else
				return ActionResult::STAY;
		}
	}
	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyMelee::applyBuffState()
{
	if (TCompEnemyMeleeAnimator* animator = _animatorHandle) {
		if (TCompSkeleton* skel = _skeletonHandle) {
			if (skel->isAnimationOver("melee_taunt")) {
				return ActionResult::LEAVE;
			}
			else if (!skel->isAnimationExecuting(skel->getAnimationID("melee_taunt"))) {
				//if (true) { //Debug
				if (RNG.f() <= _buffProbability) {
					_isBuffed = true;
					// Change shader
					if (TCompRender* render = getComponent<TCompRender>()) {
						render->setActiveSecondaryState("blast", true);
						render->setCurrentState("buffed");
						animator->setIsTaunting(true);
					}
				}
			}
		}
	}
	if(TCompTransform* t = getComponent<TCompTransform>())
		EngineAudio.playEvent(_shoutEvent, t->getPosition());
	return ActionResult::STAY;
}

ActionResult TCompBTEnemyMelee::chasePlayerState( )
{
	if (_isKnocking) return ActionResult::LEAVE;

	TCompCharacterController* eController = getComponent<TCompCharacterController>();
	TCompTransform* eTransform = getComponent<TCompTransform>();
	if (!eController || !eTransform) return ActionResult::LEAVE;

	CEntity* player = getEntityByName("Player");
	if (!player) return ActionResult::LEAVE;
	TCompTransform* playerTransform = player->getComponent<TCompTransform>();
	if (!playerTransform) return ActionResult::LEAVE;
	VEC3 destination = playerTransform->getPosition();

	TCompPathfindingAgent* agent = _agentHandle;
	if (!agent) return ActionResult::LEAVE;
	_pathfindingEvent.doMethodEvent(agent, &TCompPathfindingAgent::requestMove, destination);

	if (VEC3::Distance(destination, eTransform->getPosition()) < _attackDistance) {
		agent->stay();
		return ActionResult::LEAVE;
	}

	if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	return ActionResult::STAY;
}

ActionResult TCompBTEnemyMelee::prepareForBattle() {
	// Create pathfinding agent
	TCompPathfindingAgent* agent = _agentHandle;
	if (agent && !agent->isAgentActive())
		agent->create();

	return ActionResult::LEAVE;
}


ActionResult TCompBTEnemyMelee::leapState() {

	if (RNG.f() <= _leapProbability && !_leapDone) {
		if (TCompEnemyMeleeAnimator* animator = _animatorHandle) {
			if (std::get<bool>(animator->getVariable("leaping")->_value)) {
				return ActionResult::STAY;
			}
			else {
				animator->setIsLeaping(true);
				_leapDone = true;
			}
		}
	}

	return ActionResult::LEAVE;

}

ActionResult TCompBTEnemyMelee::goToSlotState()
{

	const VHandles boss_v = CTagsManager::get().getAllEntitiesByTag(Utils::getID("boss"));
	if (boss_v.size() == 0) return ActionResult::LEAVE;
	CEntity* boss = boss_v.at(0);
	if (!boss) return ActionResult::LEAVE;

	TCompSquadBoss* boss_comp = boss->getComponent<TCompSquadBoss>();
	if (!boss_comp) return ActionResult::LEAVE;

	TCompCharacterController* eController = getComponent<TCompCharacterController>();
	TCompTransform* eTransform = getComponent<TCompTransform>();
	if (!eController || !eTransform) return ActionResult::LEAVE;

	EntitySlot es = boss_comp->getSlot(_slotIdx);

	TCompPathfindingAgent* agent = _agentHandle;
	_pathfindingEvent.doMethodEvent(agent, &TCompPathfindingAgent::requestMove, es.position);
	
	if (VEC3::Distance(es.position, eTransform->getPosition()) <  _offset)
		return ActionResult::LEAVE;
	
	return ActionResult::INTERRUPT_AND_STAY;
}

//ActionResult TCompBTEnemyMelee::runArroundState()
//{
//
//	TCompCharacterController* eController = getComponent<TCompCharacterController>();
//	TCompTransform* eTransform = getComponent<TCompTransform>();
//	if (!eController || !eTransform) return ActionResult::LEAVE;
//
//	CEntity* player = getEntityByName("Player");
//	TCompTransform* playerTransform = player->getComponent<TCompTransform>();
//	if (!playerTransform) return ActionResult::LEAVE;
//	VEC3 destination = playerTransform->getPosition();
//
//	eController->turnToPosition((destination - eTransform->getPosition()).Left * 0.01f);
//	eController->move(eTransform->getFront());
//
//	if (askForAvailableSlot()) return ActionResult::LEAVE;
//
//	return ActionResult::INTERRUPT_AND_STAY;
//}


ActionResult TCompBTEnemyMelee::idleWarState( )
{
	facePlayer();
	if (!_isIdleWarEventPlaying) {
		if (RNG.f() <= _idleWarEventProbability) {
			if (TCompTransform* t = getComponent<TCompTransform>())
				EngineAudio.playEvent(_idleWarEvent, t->getPosition());
		}
		_isIdleWarEventPlaying = true;
	}
	ActionResult status = checkTimer(&idleWarTimer);
	if (!isInAttackRange())
		status = ActionResult::LEAVE;

	if (status == ActionResult::LEAVE) {
		TCompEnemyMeleeAnimator* animator = _animatorHandle;
		animator->setIsWarIdle(false);
		_isIdleWarEventPlaying = false;
	}
	return status;
}

ActionResult TCompBTEnemyMelee::attackState( )
{
	TCompSkeleton * skeleton = _skeletonHandle;
	TCompEnemyMeleeAnimator * animator = _animatorHandle;

	if (skeleton->isAnimationOver("melee_attack") && _isAttacking) {
		TCompTransform* eTransform = getComponent<TCompTransform>();
		if (!eTransform) return ActionResult::LEAVE;

		CEntity* player = getEntityByName("Player");
		TCompTransform* playerTransform = player->getComponent<TCompTransform>();
		if (!playerTransform) return ActionResult::LEAVE;

		// TODO: Change this for 2 colliders on enemy hand
		if (VEC3::Distance(playerTransform->getPosition(), eTransform->getPosition()) <= _attackDistance)
			attackPlayer();

		return ActionResult::LEAVE;
	}
	
	if (!_isAttacking) {
		if(TCompTransform* t = getComponent<TCompTransform>())
			EngineAudio.playEvent(_attackEvent, t->getPosition());
		animator->setIsAttacking(true);
		// Also prevent the melee from leaping
		_leapDone = true;
		_isAttacking = true;
	}

	return ActionResult::INTERRUPT_AND_STAY;
}

void TCompBTEnemyMelee::attackPlayer() {
	
	TCompTransform* trans = getComponent<TCompTransform>();
	CEntity* player = getEntityByName("Player");
	if (!player || !trans)
		return;

	TMsgHitDamage dmg;
	dmg.damage = _isBuffed ? getBuffedAttack() : _attackDamage;
	dmg.damageDirection = trans->getFront();
	dmg.h_sender = (CHandle)this;
	player->sendMsg(dmg);

	_isAttacking = false;
}

void TCompBTEnemyMelee::freeSlot()
{

	const VHandles boss_v = CTagsManager::get().getAllEntitiesByTag(Utils::getID("boss"));
	if (boss_v.size() == 0) return;
	CEntity* boss = boss_v.at(0);
	if (!boss) return;

	TCompSquadBoss* boss_comp = boss->getComponent<TCompSquadBoss>();
	if (!boss_comp) return;

	boss_comp->updateSlotInfo(_slotIdx, false);

	_slotIdx = -1;

	
}

std::string TCompBTEnemyMelee::getPlayerSlot()
{
	switch (_slotIdx) {
	case 0:
		return "north";
		break;
	case 1:
		return "east";
		break;
	case 2:
		return "south";
		break;
	case 3:
		return "north";
		break;
	default:
		return "";
		break;
	}
}

float TCompBTEnemyMelee::getBuffedAttack()
{
	return _attackDamage + (_attackDamage * _buffAttackMultiplier);
}

float TCompBTEnemyMelee::getBuffedAttackSpeed()
{
	return _attackSpeed + _attackSpeed * _buffAttackSpeedMultiplier;
}

float TCompBTEnemyMelee::getBuffedHP()
{
	if (TCompHealth* healthComp = getComponent<TCompHealth>()) {
		float health = healthComp->getCurrentMaxHealth();
		return health + health * _buffHPMultiplier;
	}
	else
		return 0.0f;
}

void TCompBTEnemyMelee::setForceChasePlayer(bool state)
{
	_forceChasePlayer = state;
}

void TCompBTEnemyMelee::alert()
{
	setForceChasePlayer(true);
}

ActionResult TCompBTEnemyMelee::flinchState()
{

	TCompEnemyMeleeAnimator* animator = _animatorHandle;
	TCompPathfindingAgent* agent = _agentHandle;

	if (std::get<int>(animator->getVariable("knockback")->_value) != -1) {
		return ActionResult::STAY;
	}
	else if(_isKnocking) {
		_isKnocking = false;
	}
	else {
		int idx = RNG.i(4);
		animator->setKnockback(idx);
		_isKnocking = true;
		agent->stay();
		return ActionResult::STAY;
	}

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyMelee::dyingState()
{
	TCompPathfindingAgent* agent = _agentHandle;
	if (agent && agent->isAgentActive()) agent->release();


	// Phase 1: Wait a bit for starting vanish
	ActionResult vanishStatus = checkTimer(&preVanishTimer);
	if (vanishStatus == ActionResult::INTERRUPT_AND_STAY) return vanishStatus;

	// Phase 2: Remove everything
	ActionResult status = checkTimer(&dyingTimer);
	if (_colliderActive) {

		// Activate the vanish mesh state and the cte data
		TCompRender * render = getComponent<TCompRender>();
		if (!render) return ActionResult::LEAVE;
		EngineLogicManager.setStatusVanishEntity(CHandle(this).getOwner());

		// Attach AABB to ragdoll
		TCompRagdoll* c_ragdoll = getComponent<TCompRagdoll>();;
		if (c_ragdoll) {
			TMsgRagdollActivated msg;
			if (c_ragdoll->ragdoll.num_bones > 0)
				msg.link = c_ragdoll->ragdoll.bones[0].link;
			if (msg.link != nullptr)
				getEntity().sendMsgToComp<TCompLocalAABB>(msg);
		}
	
		TCompCollider* col = getComponent<TCompCollider>();
		if (col) {
			TMsgToogleComponent msg{ false };
			getEntity().sendMsgToComp<TCompCollider>(msg);
		}

		// Activate particles on destruction.
		TCompTransform* trans = getComponent<TCompTransform>();
		Vector3 posToSpawnParticlesAt = trans->getPosition();

		TCompSkeleton * skeleton = getComponent<TCompSkeleton>();
		if (skeleton)
			posToSpawnParticlesAt = skeleton->getBoneAbsPos("Bip001 Spine");

		if(!vanishParticles)
			vanishParticles = LogicManager::Instance().SpawnParticle("data/particles/vanish_melee.particles", posToSpawnParticlesAt);
		
		_colliderActive = false;
	}
	else {
		TCompSkeleton * skeleton = getComponent<TCompSkeleton>();
		if (skeleton && vanishParticles)
			vanishParticles->setSystemPosition(skeleton->getBoneAbsPos("Bip001 Spine"));
	}

	return status == ActionResult::INTERRUPT_AND_STAY ? ActionResult::STAY : ActionResult::LEAVE;
}

ActionResult TCompBTEnemyMelee::deathState()
{
	if (!EnginePool.disableEntity(this->getEntity(), PoolType::Melee)) { //Check if its in a pool and disables it
		getEntity().destroy(); //If not, destroys the entity
			
	}
	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyMelee::facePlayer()
{
	TCompCharacterController* eController = getComponent<TCompCharacterController>();
	if (!eController) return ActionResult::LEAVE;

	CEntity* player = getEntityByName("Player");
	TCompTransform* playerTransform = player->getComponent<TCompTransform>();
	if (!playerTransform) return ActionResult::LEAVE;

	eController->turnToPosition(playerTransform->getPosition());

	return ActionResult::LEAVE;
}

bool TCompBTEnemyMelee::isPlayerAtSight()
{
	if (_forceChasePlayer || _playerSeen) return true;

	TCompCharacterController* eController = getComponent<TCompCharacterController>();
	TCompTransform* eTransform = getComponent<TCompTransform>();
	if (!eController || !eTransform) return false;

	CEntity* player = getEntityByName("Player");
	TCompTransform* playerTransform = player->getComponent<TCompTransform>();
	if (!playerTransform) return false;

	float angleToPlayer = eTransform->getDeltaYawToAimTo(playerTransform->getPosition());
	if (abs(angleToPlayer) <= _visionCone.angle) {
		float distance = VEC3::Distance(eTransform->getPosition(), playerTransform->getPosition());
		if (distance <= _visionCone.depth) {
			_playerSeen = true;
			return true;
		}
	}

	return false;
}

bool TCompBTEnemyMelee::isPlayerInLeapRange() {
	TCompCharacterController* eController = getComponent<TCompCharacterController>();
	TCompTransform* eTransform = getComponent<TCompTransform>();
	if (!eController || !eTransform) return false;

	CEntity* player = getEntityByName("Player");
	TCompTransform* playerTransform = player->getComponent<TCompTransform>();
	if (!playerTransform) return false;

	bool status = false;

	status &= PhysUtils::intersects(eTransform->getPosition(), playerTransform->getPosition(), 0.5f);
	status &= VEC3::Distance(eTransform->getPosition(), playerTransform->getPosition()) <= _leapMaxDistance;
	status &= VEC3::Distance(eTransform->getPosition(), playerTransform->getPosition()) >= _leapMinDistance;

	return status;
}

bool TCompBTEnemyMelee::isInAttackRange()
{
	TCompCharacterController* eController = getComponent<TCompCharacterController>();
	TCompTransform* eTransform = getComponent<TCompTransform>();
	if (!eController || !eTransform) return false;

	CEntity* player = getEntityByName("Player");
	TCompTransform* playerTransform = player->getComponent<TCompTransform>();
	if (!playerTransform) return false;

	return VEC3::Distance(eTransform->getPosition(), playerTransform->getPosition()) <= _attackDistance;
}

//bool TCompBTEnemyMelee::askForAvailableSlot()
//{
//	
//	if (_slotIdx != -1) return true;
//	
//	const VHandles boss_v = CTagsManager::get().getAllEntitiesByTag(Utils::getID("boss"));
//	if (boss_v.size() == 0) return false;
//	CEntity* boss = boss_v.at(0);
//	if (!boss) return false;
//
//	TCompSquadBoss* boss_comp = boss->getComponent<TCompSquadBoss>();
//	if (!boss_comp) return false;
//
//	TCompTransform* eTransform = getComponent<TCompTransform>();
//	if (!eTransform) return false;
//
//	EntitySlot slot = boss_comp->getAvailableSlot(eTransform->getPosition());
//
//	if (slot.idx == -1)
//		return false;
//	else {
//		_slotIdx = slot.idx;
//		boss_comp->updateSlotInfo(slot.idx, true, this->getEntity());
//	}
//
//
//	return true;
//}

bool TCompBTEnemyMelee::isAtPosition(const TCompTransform& transform, VEC3 pos, float threshold) const {
	return VEC3::Distance(pos - transform.getPosition(), VEC3::Zero) < threshold;
}

ActionResult TCompBTEnemyMelee::checkTimer(CClock* timer) {
	if (timer->isStarted() && timer->isFinished()) {
		timer->stop();
		return ActionResult::LEAVE;
	}
	else if (!timer->isStarted()) {
		timer->start();
	}
	return ActionResult::INTERRUPT_AND_STAY;
}

void TCompBTEnemyMelee::onEntityCreated(const TMsgEntityCreated & msg) {
	_animatorHandle = getComponent<TCompEnemyMeleeAnimator>();
	_agentHandle = getComponent<TCompPathfindingAgent>();
	_skeletonHandle = getComponent<TCompSkeleton>();
}

/* Message. */
// Used for disabling the AI if it must be stopped.
void TCompBTEnemyMelee::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompBTEnemyMelee::onBuffRecieved(const TMsgApplyBuff & msg) {
	// Send msg to render to change materials.
	TMsgSetMeshState msgRender = { _externalBuffStateId }; // 4 is used for active totem buff state.
	this->getEntity().sendMsg(msgRender);

	_externalDamageBuff = msg.damage;

	TCompHealth * cHealth = getComponent<TCompHealth>();
	if (cHealth)
		cHealth->setArmour(msg.armour);

}

void TCompBTEnemyMelee::onDebuffRecieved(const TMsgRemoveBuff & msg) {
	// Send msg to render to change materials.
	TMsgSetMeshState msgRender = { 0 }; // 0 is used for normal state.
	this->getEntity().sendMsg(msgRender);

	_externalDamageBuff = 1.0f;

	TCompHealth * cHealth = getComponent<TCompHealth>();
	if (cHealth)
		cHealth->setArmour(1.0f);
}

void TCompBTEnemyMelee::onEntityDead(const TMsgEntityDead& msg) {
	interrupt("isDead");

	// Set to null again. (This is freed by the module particles).
	vanishParticles = nullptr;

	// Drop spawn
	TCompTransform * transform = getComponent<TCompTransform>();
	if (transform)
		spawnDrops(*transform);

	_msgrceivd = true;
}

void TCompBTEnemyMelee::deleteIAController(const TMsgDeleteIAController & msg) {
	CHandle iaController = this;
	iaController.destroy();
}

void TCompBTEnemyMelee::onComponentReset(const TMsgResetComponent & msg) {
	reset();

	_slotIdx = -1;
	_colliderActive = true;
	_msgrceivd = false;
	_leapDone = false;
	_isBuffed = false;
	_isFlinching = false;
	_playerSeen = false;
	_isAttacking = false;
	_isKnocking = false;

	setVariable("patrol", false);
	setVariable("isDead", false);
	setForceChasePlayer(false);

	idleTimer.stop();
	idleWarTimer.stop();
	leapTimer.stop();
	dyingTimer.stop();
}

void TCompBTEnemyMelee::onAlertAI(const TMsgAlertAI& msg)
{
	alert();
}

void TCompBTEnemyMelee::registerMsgs() {
	DECL_MSG(TCompBTEnemyMelee, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompBTEnemyMelee, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompBTEnemyMelee, TMsgBulletDamage, onBulletDamage);
	DECL_MSG(TCompBTEnemyMelee, TMsgHitDamage, onHealthLoweredHit);
	DECL_MSG(TCompBTEnemyMelee, TMsgApplyBuff, onBuffRecieved);
	DECL_MSG(TCompBTEnemyMelee, TMsgRemoveBuff, onDebuffRecieved);
	DECL_MSG(TCompBTEnemyMelee, TMsgEntityDead, onEntityDead);
	DECL_MSG(TCompBTEnemyMelee, TMsgDeleteIAController, deleteIAController);
	DECL_MSG(TCompBTEnemyMelee, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompBTEnemyMelee, TMsgResetComponent, onComponentReset);
	DECL_MSG(TCompBTEnemyMelee, TMsgAlertAI, onAlertAI);
}

void TCompBTEnemyMelee::declareInLua() {
	EngineScripting.declareComponent<TCompBTEnemyMelee>
		(
			"TCompBTEnemyMelee",
			"getVariable", &TCompBTEnemyMelee::getVariable,
			"setVariable", &TCompBTEnemyMelee::setVariable,
			"interrupt", &TCompBTEnemyMelee::interrupt,
			"alert", &TCompBTEnemyMelee::alert
		);
}

void TCompBTEnemyMelee::onHealthLoweredHit(const TMsgHitDamage & msg)
{
	interrupt("damageReceived");
}

void TCompBTEnemyMelee::onBulletDamage(const TMsgBulletDamage & msg)
{
	interrupt("damageReceived");
	_forceChasePlayer = true;
}



