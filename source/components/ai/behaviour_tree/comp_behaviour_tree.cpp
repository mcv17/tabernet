#include "mcv_platform.h"
#include "comp_behaviour_tree.h"
#include "bt_resource.h"
#include "engine.h"
#include "entity/entity_parser.h"

#include "../../../logic/logic_manager.h"
#include "../../../geometry/transform.h"

map<string, BTGlobalData> TCompBehaviourTree::_globalData;

BehaviourTreeNode * TCompBehaviourTree::createNode(string name) {
	assert(findNode(name) == nullptr && "Node already exists");

	BehaviourTreeNode btn(name);
	(*_tree)[name] = btn;
	return &((*_tree)[name]);
}

BehaviourTreeNode * TCompBehaviourTree::createNode(string name, string type, json & params)
{
	assert(findNode(name) == nullptr && "Node already exists");

	BehaviourTreeNode btn(name);
	if (type == "sequence") {
		btn.setType(BehaviourTreeNode::SEQUENCE);
	}
	else if (type == "priority") {
		btn.setType(BehaviourTreeNode::PRIORITY);
	}
	else if (type == "random") {
		btn.setType(BehaviourTreeNode::RANDOM);
	}
	else if (type == "decorator") {
		btn.setType(BehaviourTreeNode::DECORATOR);
		json & jDecorator = params["decorator"];
		std::string decoratorType = jDecorator["type"];
		if (decoratorType == "set") {
			// we create a generic setter action
			auto setterAction = [](std::string name, json & jValue, TCompBehaviourTree* bt) 
			{ 
				bt->setVariable(name, jValue);
				return BehaviourTreeNode::LEAVE; 
			};
			// we bind custom parameters to the recently created action
			// prior to this the function actually had 3 parameters, now has 1
			auto boundAction = std::bind(setterAction, static_cast<std::string>(jDecorator["variable"]), jDecorator["value"], std::placeholders::_1);
			addAction(name, boundAction);
		}
		else if (decoratorType == "execute") {
			addAction(name, jDecorator);
		}
	}
	else if (type == "action") {
		btn.setType(BehaviourTreeNode::ACTION);
		addAction(name, params);
	}
	(*_tree)[name] = btn;
	return &((*_tree)[name]);
}


BehaviourTreeNode * TCompBehaviourTree::findNode(string nodeName)
{
	if (_tree->find(nodeName) == _tree->end()) return nullptr;
	else return &((*_tree)[nodeName]);
}

bool TCompBehaviourTree::interrupted() {
	return (_interruptions.size() > 0);
}

void TCompBehaviourTree::resetInterruptions() {
	if (!_interruptions.empty()) {
		setVariable(_interruptions.front(), false);
		_interruptions.clear();
	}
	while (!_contextStack.empty())
		_contextStack.pop();
	_servingInterruption = false;
}

void TCompBehaviourTree::setConditionImplementation(string funcName, btcondition func) {
	auto & conditions = _globalData[_filename].conditionsToImplement;
	if (conditions.count(funcName) > 0) {
		for (auto& cond : conditions[funcName]) {
			addCondition(cond, func);
		}
		conditions.erase(funcName);
	}
}

void TCompBehaviourTree::setActionImplementation(string funcName, btaction func) {
	auto & actions = _globalData[_filename].actionsToImplement;
	if (actions.count(funcName) > 0 && "Function doesn't need an implementation") {
		for (auto& action : actions[funcName]) {
			addAction(action, func);
		}
		actions.erase(funcName);
	}
}

DynamicVariable TCompBehaviourTree::getVariable(string varName) {
	assert(variableExists(varName));
	return _variables[varName];
}

bool TCompBehaviourTree::variableExists(const string & name) {
	return _variables.count(name) > 0;
}

void TCompBehaviourTree::interrupt(string flag) {
	if (variableExists(flag))
		_interruptions.push_back(flag);
}

void TCompBehaviourTree::setVariable(string varName, const DynamicVariable & value) {
	if (variableExists(varName))
		_variables[varName] = value;
}

void TCompBehaviourTree::recalc() {
	if (_current == nullptr) _root->recalc(this);	// I'm not in a sequence, start from the root
	else _current->recalc(this);				// I'm in a sequence. Continue where I left
}

bool TCompBehaviourTree::handleInterruptions() {
	if (interrupted()) {
		_servingInterruption = true;
		setVariable(_interruptions.front(), true);
		return true;
	}
	else return false;
}

void TCompBehaviourTree::setCurrent(BehaviourTreeNode *nc) {
	if (nc == nullptr) {
		if (_servingInterruption) {
			// restore flag to false
			if (interrupted()) {
				setVariable(_interruptions.front(), false);
				_interruptions.pop_front();
			}
			_servingInterruption = false;
		}
		if (!_contextStack.empty() && !interrupted()) {
			_current = _contextStack.top();
			_contextStack.pop();
			return;
		}
	}

	_current = nc;
}

void TCompBehaviourTree::reset() {
	resetInterruptions();
	setCurrent(nullptr);
}

std::string dropType;
Vector3 dropPositionOffset;
Vector3 dropMinPositionNoise;
Vector3 dropMaxPositionNoise;
Vector3 dropImpulse;
Vector3 dropMinImpulseNoise;
Vector3 dropMaxImpulseNoise;

void TCompBehaviourTree::loadDropsData(const json & j) {
	dropType = j.value("drop_type", "");
	dropProb = j.value("drop_prob", 0.0f);
	dropPositionOffset = loadVector3(j, "drop_position_offset");
	dropMinPositionNoise = loadVector3(j, "drop_min_position_noise");
	dropMaxPositionNoise = loadVector3(j, "drop_max_position_noise");
	dropImpulse = loadVector3(j, "drop_impulse");
	dropMinImpulseNoise = loadVector3(j, "drop_min_impulse_noise");
	dropMaxImpulseNoise = loadVector3(j, "drop_max_impulse_noise");
}

void TCompBehaviourTree::spawnDrops(const CTransform & transform) {
	float randomProb = RNG.f();
	if (dropProb < randomProb) return;

	Vector3 impulseVectorRotated = DirectX::XMVector3Rotate(dropImpulse, transform.getRotation());
	Vector3 dropPositionOffsetRotated = DirectX::XMVector3Rotate(dropPositionOffset, transform.getRotation());
	Vector3 finalDropPosition = transform.getPosition() + dropPositionOffsetRotated;

	LogicManager::Instance().SpawnPicksUpGivenDropType(dropType, finalDropPosition, impulseVectorRotated, dropMinPositionNoise, dropMaxPositionNoise, dropMinImpulseNoise, dropMaxImpulseNoise);
}

bool TCompBehaviourTree::hasAction(string actionName)
{
	return _actions->find(actionName) != _actions->end();
}

void TCompBehaviourTree::addAction(string actionName, btaction action)
{
	assert(_actions->find(actionName) == _actions->end() && "Node action already exists");

	(*_actions)[actionName] = action;
}

void TCompBehaviourTree::addAction(string actionName, json & action) {
	assert(_actions->find(actionName) == _actions->end() && "Node action already exists");

	std::string funcName = action["function"];
	if (funcName == "Nothing") {
		(*_actions)[actionName] = [](TCompBehaviourTree *) { return ActionResult::LEAVE; };
	}
	else if (funcName == "Abort") {
		(*_actions)[actionName] = [](TCompBehaviourTree *) { return ActionResult::ABORT; };
	}
	else {
		auto & actions = _globalData[_filename].actionsToImplement;
		if (actions.count(funcName) == 0) {
			vector<string> actList;
			actions[funcName] = actList;
		}
		actions[funcName].push_back(actionName);
	}
}

// change return to behaviourtreenode::ActionResult
int TCompBehaviourTree::execAction(string actionName)
{
	assert(_actions->find(actionName) != _actions->end() && "Node action does not exist or lacks an implementation");

	return (*_actions)[actionName](this);
}

void TCompBehaviourTree::addCondition(string condName, btcondition cond) {
	auto & conditions = _globalData[_filename].conditions;
	assert(conditions.find(condName) == conditions.end() && "Node condition already exists");

	TBTCondition * tcond = new TBTCondition(cond);

	conditions[condName] = tcond;
}

void TCompBehaviourTree::addCondition(string condName, json & jTransition)
{
	auto & conditions = _globalData[_filename].conditionsToImplement;
	PROFILE_FUNCTION("addCondition");
	assert(conditions.find(condName) == conditions.end() && "Node condition already exists");

	json & jCond = jTransition["params"]["condition"];
	std::string condType = jCond["type"];

	TBTCondition * cond = nullptr;
	if (condType == "function") {
		if (conditions.count(jCond["name"]) == 0)
			conditions[jCond["name"]] = vector<string>();
		conditions[jCond["name"]].push_back(jTransition["target"]);
	}
	else if (condType == "predicate") {
		_globalData[_filename].conditionsToFill[condName] = (&jCond);
	}
}


bool TCompBehaviourTree::testCondition(string condName)
{
	if (_conditions.find(condName) == _conditions.end())
	{
		return true;	// if no condition defined, we assume TRUE
	}
	return _conditions[condName]->test(this);
}

void TCompBehaviourTree::parseJson(const json & j) {
	auto & global = _globalData[_filename];
	{
		PROFILE_FUNCTION("Reference");
		global.jReference = j;
	}

	/* NODES */
	{
		PROFILE_FUNCTION("Nodes");
		// get root node
		json & jRootNode = global.jReference["root_node"];
		BehaviourTreeNode * btn = createNode(jRootNode["name"], jRootNode["type"], jRootNode["params"]);
		global.root = btn;
		_root = btn;
		// get rest of nodes
		json & jNodeArray = global.jReference["nodes"];
		for (auto& jNode : jNodeArray) {
			createNode(jNode["name"], jNode["type"], jNode["params"]);
		}
	}

	/* VARIABLES */
	{
		PROFILE_FUNCTION("Variables");
		// we actually set the variables alongside json reference
		json & jVariables = global.jReference["variables"];
		for (auto& jVar : jVariables) {
			global.variables[jVar["name"]] = jVar["value"];
		}
	}
	_variables = global.variables;

	/* TRANSITIONS */
	{
		PROFILE_FUNCTION("Transitions");
		// map of stored random ratios, it is needed because setRatios method needs as input all the ratios at once
		map<string, vector<float>> ratiosCache;
		json & jTransitions = global.jReference["transitions"];
		for (int i = 0; i < jTransitions.size(); i++) {
			// add child (transition target) to parent (transition source)
			findNode(jTransitions[i]["source"])->addChild(findNode(jTransitions[i]["target"]));
			// give parent pointer to child
			findNode(jTransitions[i]["target"])->setParent(findNode(jTransitions[i]["source"]));
			/* CONDITIONS */
			if (jTransitions[i]["params"].count("condition") > 0) {
				addCondition(jTransitions[i]["target"], jTransitions[i]);
			}
			/* RANDOM */
			else if (jTransitions[i]["params"].count("random") > 0) {
				string nodeName = jTransitions[i]["source"];
				float probability = jTransitions[i]["params"]["random"]["probability"];
				// insert entry as <name of the node, ratio>
				if (ratiosCache.count(nodeName) == 0) {
					vector<float> ratioArray;
					ratiosCache[nodeName] = ratioArray;
				}
				ratiosCache[nodeName].push_back(probability);

			}

		}
		// we actually set the ratios for random nodes
		for (auto& ratioPair : ratiosCache) {
			setNodeRatios(ratioPair.first, ratioPair.second);
		}

		// we add built-in functions
		if (global.conditionsToImplement.count("handleInterruptions") > 0)
			setConditionImplementation("handleInterruptions", [](TCompBehaviourTree * ref) { return ref->handleInterruptions(); });
	}
}

void TCompBehaviourTree::update(float dt)
{
	if (!active) return;

	updateVariables();
	recalc();
}

void TCompBehaviourTree::load(const json & j, TEntityParseContext & ctx) {
	assert(j.count("bt_file") > 0);
	_filename = j["bt_file"];
	if (_globalData.count(_filename) == 0) {
		// load global settings
		_globalData[_filename] = BTGlobalData();
		auto & global = _globalData[_filename];
		_tree = &(global.tree);
		_actions = &(global.actions);

		PROFILE_FUNCTION("TCompBehaviourTree:Load");
		{
			PROFILE_FUNCTION("Parse");
			parseJson(EngineResources.getResource(_filename)->as<CBT>()->getJson());
			_name = j.value("name", "");
		}

		implementFunctions();
		_conditions = global.conditions;
	}
	else {
		// global settings have already been initialized
		auto & global = _globalData[_filename];
		_tree = &(global.tree);
		_actions = &(global.actions);
		_root = _globalData[_filename].root;

		_conditions = global.conditions;
		_variables = global.variables;
	}

	// fill conditions with local variables
	TBTCondition * condPtr;
	json * jLeftPtr;
	json * jRightPtr;
	for (auto & condElem : _globalData[_filename].conditionsToFill) {
		condPtr = nullptr;
		jLeftPtr = nullptr;
		jRightPtr = nullptr;
		std::string name, type;

		// process left operand
		{
			json & tempOperand = (*condElem.second)["left_operand"];
			type = tempOperand["type"];
			if (type == "variable") {
				name = tempOperand["value"];
				jLeftPtr = &_variables[name];
			}
			else if (type == "value") {
				jLeftPtr = &tempOperand["value"]["value"];
			}
		}
		// process right operand
		{
			json & tempOperand = (*condElem.second)["right_operand"];
			type = tempOperand["type"];
			if (type == "variable") {
				name = tempOperand["value"];
				jRightPtr = &_variables[name];
			}
			else if (type == "value") {
				jRightPtr = &tempOperand["value"]["value"];
			}
		}

		condPtr = new TBTSimpleCondition(*jLeftPtr, (*condElem.second)["operator"], *jRightPtr);
		_conditions[condElem.first] = condPtr;
	}

	{
		PROFILE_FUNCTION("rnd");

		// set random values from json
		if (j.count("random_values") > 0 && j["random_values"].is_array()) {
			for (auto& jRand : j["random_values"]) {
				BehaviourTreeNode * node = findNode(jRand["node"]);
				assert(node && "Node from random values list in json file not found");
				vector<float> ratios;
				for (auto& jValue : jRand["values"]) {
					ratios.push_back(jValue);
				}
				node->setRatios(ratios);
			}
		}
	}

	loadDropsData(j);
}

void TCompBehaviourTree::debugInMenu() {
	ImGui::LabelText("Current node", getNodeName().c_str());

	if (ImGui::TreeNode("Variables")) {
		for (auto& var : _variables) {
			const auto varType = var.second.type();
			const char * varName = var.first.c_str();
			switch (varType) {
			case json::value_t::boolean:
				ImGui::Checkbox(varName, var.second.get_ptr<bool *>());
				break;
			case json::value_t::number_integer:
				ImGui::DragScalar(varName, ImGuiDataType_S64, var.second.get_ptr<int64_t *>(), 1.0f, nullptr, nullptr, "%d", 1.0f);
				break;
			case json::value_t::number_float:
				ImGui::DragScalar(varName, ImGuiDataType_Double, var.second.get_ptr<double *>(), 0.1f, nullptr, nullptr, "%.3f", 1.0f);
				break;
			}
		}
		ImGui::TreePop();
	}
}

void TCompBehaviourTree::setNodeRatios(const string & name, vector<float>& ratios) {
	BehaviourTreeNode * node = findNode(name);
	if (!node) return;

	node->setRatios(ratios);
}
