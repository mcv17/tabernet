#include "mcv_platform.h"

#include "utils/time.h"
#include "utils/phys_utils.h"

#include "comp_bt_enemy_immortal.h"
#include "components/controllers/comp_character_controller.h"
#include "components/common/comp_render.h"
#include "components/ai/squads/comp_squad_boss.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_aabb.h"
#include "skeleton/comp_skeleton.h"
#include "components/animations/comp_enemyInmortal_animator.h"

DECL_OBJ_MANAGER("ai_bt_enemy_immortal", TCompBTEnemyImmortal);

//Helpers
//Checks if entity is at position within a threshold
inline bool isAtPosition(const TCompTransform& transform, VEC3 pos, float threshold = 0.2f) {
	return Vector3::Distance(pos - transform.getPosition(), VEC3::Zero) < threshold;
}

//Checks if the timer has started and has finished. If so returns LEAVE, any other case returns INTERRUPT_AND_STAY
ActionResult TCompBTEnemyImmortal::checkTimer(CClock* timer) {
	if (timer->isStarted() && timer->isFinished()) {
		timer->stop();
		return ActionResult::LEAVE;
	}
	else if (!timer->isStarted()) {
		timer->start();
	}
	return ActionResult::INTERRUPT_AND_STAY;
}

void TCompBTEnemyImmortal::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Text("Current node: %s", getNodeName().c_str());
	ImGui::Separator();
	ImGui::Text("Player slot asigned: %s", getPlayerSlot().c_str());
	ImGui::Text("Distance parameters: ");
	ImGui::DragFloat("Forget distance", &_forgetDistance, 0.5f, 0.0f, 50.0f);
	ImGui::DragFloat("Attack distance", &_attackDistance, 0.1f, 0.0f, 3.0f);
	ImGui::Separator();
	ImGui::DragFloat("Damage", &_damage, 0.1f, 0.0f, 50.0f);
	ImGui::DragFloat("DamageBuff", &_damageBuff, 0.1f, 0.0f, 50.0f);
}

void TCompBTEnemyImmortal::init() {
	_idleSoundEventCooldown.start();
}

void TCompBTEnemyImmortal::load(const json& j, TEntityParseContext& ctx) {
	//!!!!!!!!!!!!!!!!!!!!!!!!!!
	TCompBehaviourTree::load(j, ctx);

	_forgetDistance = j.value("forgetDistance", _forgetDistance);
	_attackDistance = j.value("attackDistance", _attackDistance);
	_visionCone.angle = j.value("coneVision_angle", _visionCone.angle);
	_visionCone.depth = j.value("coneVision_depth", _visionCone.depth);
	_buffColour = loadVector4(j, "buffColour");

	_idleSoundEvent = j.value("idle_sound_event", _idleSoundEvent);
	_idleSoundEventChance = j.value("idle_sound_chance", _idleSoundEventChance);
	if (j.count("idle_sound_cooldown"))
		_idleSoundEventCooldown.setTime(j["idle_sound_cooldown"]);

	if (j.count("waypoints") && j["waypoints"].is_array()) {
		for (auto wp : j["waypoints"])
			_waypoints.push_back(loadVector3(wp));
	}
	setVariable("numWaypoints", _waypoints.size());
	setVariable("isArena", j.value("is_arena", false));


}

void TCompBTEnemyImmortal::implementFunctions() {
	PROFILE_FUNCTION("TCompBTEnemyImmortal:implementFunctions");

	//Combat
	IMPLEMENT_CONDITION("isPlayerInSight", TCompBTEnemyImmortal, isPlayerInSight);
	IMPLEMENT_ACTION("prepareForBattle", TCompBTEnemyImmortal, prepareForBattle);
	IMPLEMENT_CONDITION("askForAvailableSlot", TCompBTEnemyImmortal, askForAvailableSlot);
	IMPLEMENT_CONDITION("isBossDead", TCompBTEnemyImmortal, isBossDead);
	IMPLEMENT_ACTION("goToSlot", TCompBTEnemyImmortal, goToSlot);
	IMPLEMENT_ACTION("chasePlayer", TCompBTEnemyImmortal, chasePlayer);
	IMPLEMENT_ACTION("facePlayer", TCompBTEnemyImmortal, facePlayer);
	IMPLEMENT_ACTION("idleWar", TCompBTEnemyImmortal, idleWar);
	IMPLEMENT_ACTION("attack", TCompBTEnemyImmortal, attack);

	//Interruptions damage
	IMPLEMENT_ACTION("flinch", TCompBTEnemyImmortal, flinch);
	IMPLEMENT_ACTION("death", TCompBTEnemyImmortal, death);
	IMPLEMENT_ACTION("dying", TCompBTEnemyImmortal, dying);

	//Patrol
	IMPLEMENT_ACTION("seekWaypoint", TCompBTEnemyImmortal, seekWaypoint);
	IMPLEMENT_ACTION("nextWaypoint", TCompBTEnemyImmortal, nextWaypoint);
	IMPLEMENT_ACTION("rest", TCompBTEnemyImmortal, rest);

	//Idle
	IMPLEMENT_ACTION("idle", TCompBTEnemyImmortal, idle);

}

ActionResult TCompBTEnemyImmortal::idle() {
	// TODO: Run Idle animation

	// Play idle sound event
	if (!_idleSoundEvent.empty() && _idleSoundEventCooldown.isFinished() && RNG.b(_idleSoundEventChance)) {
		TCompTransform* transform = _transHandle;
		EngineAudio.playEvent(_idleSoundEvent, transform->getPosition());
		_idleSoundEventCooldown.start();
	}

	return checkTimer(&_idleTimer);
}

ActionResult TCompBTEnemyImmortal::chasePlayer() {
	// Component checks
	TCompCharacterController* eController = _charControllerHandle;
	TCompTransform* eTransform = _transHandle;
	TCompTransform* playerTransform = getPlayerTransform();
	if (!eController || !eTransform || !playerTransform) return ActionResult::LEAVE;

	// Player position and navigation agent
	Vector3 destination = playerTransform->getPosition();
	TCompPathfindingAgent* agent = _agentHandle;
	_pathfindingEvent.doMethodEvent(agent, &TCompPathfindingAgent::requestMove, destination);

	// If in attack range
	if (VEC3::Distance(destination, eTransform->getPosition()) < _attackDistance)
		return ActionResult::LEAVE;

	// If player has escaped and is not arena abort to the root node
	if (!getVariable("isArena") && VEC3::Distance(destination, eTransform->getPosition()) > _forgetDistance)
		return ActionResult::ABORT;

	// Play idle sound event
	if (!_idleSoundEvent.empty() && _idleSoundEventCooldown.isFinished() && RNG.b(_idleSoundEventChance)) {
		TCompTransform* transform = _transHandle;
		EngineAudio.playEvent(_idleSoundEvent, transform->getPosition());
		_idleSoundEventCooldown.start();
	}

	// If not, keep chasing
	if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	return ActionResult::STAY;
}

ActionResult TCompBTEnemyImmortal::goToSlot() {
	// Get the boss if exists
	const VHandles boss_v = CTagsManager::get().getAllEntitiesByTag(Utils::getID("boss"));
	if (boss_v.size() == 0) return ActionResult::LEAVE;
	CEntity* boss = boss_v.at(0);
	if (!boss) return ActionResult::LEAVE;

	// Components check
	TCompSquadBoss* boss_comp = boss->getComponent<TCompSquadBoss>();
	TCompCharacterController* eController = _charControllerHandle;
	TCompTransform* eTransform = _transHandle;
	if (!boss_comp || !eController || !eTransform) return ActionResult::LEAVE;

	// Get the slot position and move with navigation agent
	EntitySlot es = boss_comp->getSlot(_slotIdx);
	TCompPathfindingAgent* agent = _agentHandle;
	_pathfindingEvent.doMethodEvent(agent, &TCompPathfindingAgent::requestMove, es.position);

	// If in slot
	if (VEC3::Distance(es.position, eTransform->getPosition()) < _offset)
		return ActionResult::LEAVE;

	// If not, keep going to slot
	if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	return ActionResult::STAY;
}

ActionResult TCompBTEnemyImmortal::seekWaypoint() {
	// If player in sight abort to change state
	if (isPlayerInSight())
		return ActionResult::ABORT;

	// Components check
	TCompCharacterController* eController = _charControllerHandle;
	TCompTransform* eTransform = _transHandle;
	TCompTransform* playerTransform = getPlayerTransform();
	if (!eController || !eTransform || !playerTransform) return ActionResult::LEAVE;

	// Get the next waypoint, turn to it and move towards it
	// If it was chasing the player, return to the waypoint with the navigation agent, then free it in NextWaypoint function
	VEC3 destination = _waypoints.at(_curWaypoint);
	if (_wasChasingPlayer) {
		// Move
		TCompPathfindingAgent* agent = _agentHandle;
		_pathfindingEvent.doMethodEvent(agent, &TCompPathfindingAgent::requestMove, destination);
		
		// If already almost in the waypoint, free the navigation agent and move normally
		if (isAtPosition(*eTransform, destination, _returningToWaypointThreshold)) {
			_wasChasingPlayer = false;
			TCompPathfindingAgent* agent = _agentHandle;
			if (agent && agent->isActive())
				agent->release();
		}
	}
	else {
		eController->turnToPosition(destination);
		eController->move(eTransform->getFront());
	}

	// Play idle sound event
	if (!_idleSoundEvent.empty() && _idleSoundEventCooldown.isFinished() && RNG.b(_idleSoundEventChance)) {
		TCompTransform* transform = _transHandle;
		EngineAudio.playEvent(_idleSoundEvent, transform->getPosition());
		_idleSoundEventCooldown.start();
	}

	// If in waypoint leave, if not stay (check for interruptions)
	if (interrupted())
		return ActionResult::INTERRUPT_AND_STAY;
	if (isAtPosition(*eTransform, destination))
		return ActionResult::LEAVE;
	else
		return ActionResult::STAY;
}

ActionResult TCompBTEnemyImmortal::nextWaypoint() {
	_curWaypoint = (_curWaypoint + 1) % (int)_waypoints.size();

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyImmortal::rest() {
	// Starts the timer if not started. If it has finished it returns LEAVE, if not, INTERRUPT_AND_STAY
	ActionResult status = checkTimer(&_restTimer);
	return status;
}

ActionResult TCompBTEnemyImmortal::idleWar() {
	//Check components
	TCompTransform* eTransform = _transHandle;
	TCompTransform* playerTransform = getPlayerTransform();
	if (!eTransform || !playerTransform) return ActionResult::LEAVE;

	// If the player is going away while in idle war
	if (!isAtPosition(*eTransform, playerTransform->getPosition(), _combatChaseAgainThreshold)) {
		_wasAttackingPlayer = true;
		return ActionResult::ABORT;
	}

	// TODO:  Run idleWar anim for x seconds
	// Starts the timer if not started. If it has finished it returns LEAVE, if not, INTERRUPT_AND_STAY
	ActionResult status = checkTimer(&_idleWarTimer);
	return status;
}

ActionResult TCompBTEnemyImmortal::attack() {
	TCompSkeleton* skeleton = _skeletonHandle;
	TCompEnemyInmortalAnimator* animator = _animatorHandle;

	if (_justAttacked && skeleton->isAnimationOver("inmortal_attack")) {
		TCompTransform* eTransform = getComponent<TCompTransform>();
		TCompTransform* playerTransform = getPlayerTransform();
		if (!eTransform || !playerTransform) return ActionResult::LEAVE;

		// TODO: Change this for 2 colliders on enemy hand if we want more realism
		if (VEC3::Distance(playerTransform->getPosition(), eTransform->getPosition()) <= _attackDistance)
			attackPlayer();

		_justAttacked = false;

		return ActionResult::LEAVE;
	}
	else if (!_justAttacked) {
		animator->setIsAttacking(true);
		_justAttacked = true;
	}

	return ActionResult::INTERRUPT_AND_STAY;
}

// Sends the player a damage message
void TCompBTEnemyImmortal::attackPlayer() {
	TCompTransform* trans = _transHandle;
	CEntity* player = getEntityByName("Player");
	if (!player || !trans)
		return;

	TMsgHitDamage dmg;
	dmg.damage = _damage * _damageBuff;
	dmg.damageDirection = trans->getFront();
	dmg.h_sender = (CHandle)this;
	player->sendMsg(dmg);
}

// Frees up the slot if boss still exists
void TCompBTEnemyImmortal::freeSlot() {
	const VHandles boss_v = CTagsManager::get().getAllEntitiesByTag(Utils::getID("boss"));
	if (boss_v.size() == 0) return;
	CEntity* boss = boss_v.at(0);
	if (!boss) return;

	TCompSquadBoss* boss_comp = boss->getComponent<TCompSquadBoss>();
	if (!boss_comp) return;

	boss_comp->updateSlotInfo(_slotIdx, false);

	_slotIdx = -1;
}

// Returns string slot from int index
std::string TCompBTEnemyImmortal::getPlayerSlot() {
	switch (_slotIdx) {
	case 0:
		return "north";
		break;
	case 1:
		return "east";
		break;
	case 2:
		return "south";
		break;
	case 3:
		return "north";
		break;
	default:
		return "";
		break;
	}
}

ActionResult TCompBTEnemyImmortal::flinch() {
	TCompSkeleton* skeleton = _skeletonHandle;
	TCompEnemyInmortalAnimator* animator = _animatorHandle;
	// TODO check for head or torso, atm only torso animation
	if (_isFlinching && skeleton->isAnimationOver("inmortal_knockback")) {
		_isFlinching = false;
		return ActionResult::LEAVE;
	}
	
	if (!_isFlinching) {
		animator->setKnockback(KnockbackAnim::TORSO);
		TCompPathfindingAgent* agent = _agentHandle;
		if (agent && agent->isAgentActive()) agent->stay();
		_isFlinching = true;
	}

	return ActionResult::INTERRUPT_AND_STAY;
}

// In the immortal case the clock will be longer, because the only way to kill it is by the railgun
ActionResult TCompBTEnemyImmortal::dying() {
	//Checks for navigation agent and releases it if exists
	TCompPathfindingAgent* agent = _agentHandle;
	if (agent && agent->isAgentActive()) agent->release();

	ActionResult status = checkTimer(&_dyingTimer);

	if (_colliderActive) {
		// Attach AABB to ragdoll
		TCompRagdoll* c_ragdoll = getComponent<TCompRagdoll>();;
		if (c_ragdoll) {
			TMsgRagdollActivated msg;
			if (c_ragdoll->ragdoll.num_bones > 0)
				msg.link = c_ragdoll->ragdoll.bones[0].link;
			if (msg.link != nullptr)
				getEntity().sendMsgToComp<TCompLocalAABB>(msg);
		}

		TCompCollider* col = getComponent<TCompCollider>();
		if (col) {
			TMsgToogleComponent msg{ false };
			getEntity().sendMsgToComp<TCompCollider>(msg);
		}

		_colliderActive = false;
	}

	// Returns, but no interruptions
	return status == ActionResult::INTERRUPT_AND_STAY ? ActionResult::STAY : ActionResult::LEAVE;
}

ActionResult TCompBTEnemyImmortal::death() {
	//if (!EnginePool.disableEntity(this->getEntity(), PoolType::Immortal)) //Check if its in a pool and disables it
		getEntity().destroy(); //If not, destroys the entity

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyImmortal::facePlayer() {
	// Components check
	TCompCharacterController* eController = _charControllerHandle;
	TCompTransform* playerTransform = getPlayerTransform();
	if (!eController || !playerTransform) return ActionResult::LEAVE;

	// Turn, facing the player
	eController->turnToPosition(playerTransform->getPosition());

	return ActionResult::LEAVE;
}

ActionResult TCompBTEnemyImmortal::prepareForBattle() {
	// Create pathfinding agent
	TCompPathfindingAgent* agent = _agentHandle;
	if (agent && !agent->isAgentActive()) 
		agent->create();

	// To return to the waypoint with the navigation agent if the player escapes and it's a patrol type
	_wasChasingPlayer = true;

	return ActionResult::LEAVE;
}

bool TCompBTEnemyImmortal::isPlayerInSight() {
	// If mode isArena is on, the player will be always detected
	if (getVariable("isArena"))
		return true;

	// If was attacking the player, but the player try to go away while in IdleWar
	if (_wasAttackingPlayer) {
		_wasAttackingPlayer = false;
		return true;
	}

	// Check components
	TCompCharacterController* eController = _charControllerHandle;
	TCompTransform* eTransform = _transHandle;
	TCompTransform* playerTransform = getPlayerTransform();
	if (!eController || !eTransform || !playerTransform) return false;

	// Check if player in vision cone
	float angleToPlayer = eTransform->getDeltaYawToAimTo(playerTransform->getPosition());
	if (abs(angleToPlayer) <= _visionCone.angle) {
		float distance = VEC3::Distance(eTransform->getPosition(), playerTransform->getPosition());
		if (distance <= _visionCone.depth)
			return true;
	}

	return false;
}

bool TCompBTEnemyImmortal::isBossDead() {
	const VHandles boss_v = CTagsManager::get().getAllEntitiesByTag(Utils::getID("boss"));
	if (boss_v.size() == 0) return true;
	CEntity* boss = boss_v.at(0);
	if (!boss) return true;

	TCompSquadBoss* boss_comp = boss->getComponent<TCompSquadBoss>();
	if (!boss_comp) return true;

	return false;
}

bool TCompBTEnemyImmortal::askForAvailableSlot() {
	// If already has a slot assigned we don't need it
	if (_slotIdx != -1) return true;

	// Check if boss exsits
	const VHandles boss_v = CTagsManager::get().getAllEntitiesByTag(Utils::getID("boss"));
	if (boss_v.size() == 0) return false;
	CEntity* boss = boss_v.at(0);
	if (!boss) return false;

	// Check components
	TCompSquadBoss* boss_comp = boss->getComponent<TCompSquadBoss>();
	TCompTransform* eTransform = _transHandle;
	if (!boss_comp || !eTransform) return false;

	EntitySlot slot = boss_comp->getAvailableSlot(eTransform->getPosition());

	// If -1 no slot available, else slot to occuppy
	if (slot.idx == -1)
		return false;
	else {
		_slotIdx = slot.idx;
		boss_comp->updateSlotInfo(slot.idx, true, this->getEntity());
		return true;
	}
}

void TCompBTEnemyImmortal::onEntityCreated(const TMsgEntityCreated& msg) {
	_animatorHandle = getComponent<TCompEnemyInmortalAnimator>();
	_agentHandle = getComponent<TCompPathfindingAgent>();
	_skeletonHandle = getComponent<TCompSkeleton>();
	_transHandle = getComponent<TCompTransform>();
	_charControllerHandle = getComponent<TCompCharacterController>();
}

/* Messages */
// Used for disabling the AI if it must be stopped.
void TCompBTEnemyImmortal::onLogicStatus(const TMsgLogicStatus& msg) {
	active = msg.activate;
}

// Send msg to render to change materials to buff state.
void TCompBTEnemyImmortal::onBuffRecieved(const TMsgApplyBuff& msg) {
	TMsgSetMeshState msgRender = { _buffStateId }; // 4 is used for active totem buff state.
	this->getEntity().sendMsg(msgRender);

	_damageBuff = msg.damage;

	TCompHealth* cHealth = getComponent<TCompHealth>();
	if (cHealth)
		cHealth->setArmour(msg.armour);
}

// Send msg to render to change materials to normal state.
void TCompBTEnemyImmortal::onDebuffRecieved(const TMsgRemoveBuff& msg) {
	TMsgSetMeshState msgRender = { 0 }; // 0 is used for normal state.
	this->getEntity().sendMsg(msgRender);

	_damageBuff = 1.0f;

	TCompHealth* cHealth = getComponent<TCompHealth>();
	if (cHealth)
		cHealth->setArmour(1.0f);
}

// Handles the EntityDead msg and throws an interruption on the bt.
void TCompBTEnemyImmortal::onEntityDead(const TMsgEntityDead& msg) {
	interrupt("isDead");
}

// If somehow we need to delete the IA
void TCompBTEnemyImmortal::deleteIAController(const TMsgDeleteIAController& msg) {
	CHandle iaController = this;
	iaController.destroy();
}

// Resets the values, variables, timers, interruptions, etc.
void TCompBTEnemyImmortal::onComponentReset(const TMsgResetComponent& msg) {
	reset();

	_slotIdx = -1;
	_damageBuff = 1.0f;
	_colliderActive = true;
	_justAttacked = false;

	setVariable("isDead", false);
	setVariable("isArena", false);

	_idleTimer.stop();
	_idleWarTimer.stop();
	_dyingTimer.stop();
}

void TCompBTEnemyImmortal::onAlertAI(const TMsgAlertAI & msg) {
	alert();
}

void TCompBTEnemyImmortal::registerMsgs() {
	DECL_MSG(TCompBTEnemyImmortal, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompBTEnemyImmortal, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompBTEnemyImmortal, TMsgBulletDamage, onBulletDamage);
	DECL_MSG(TCompBTEnemyImmortal, TMsgNailDamage, onNailDamage);
	DECL_MSG(TCompBTEnemyImmortal, TMsgHitDamage, onHitDamage);
	DECL_MSG(TCompBTEnemyImmortal, TMsgApplyBuff, onBuffRecieved);
	DECL_MSG(TCompBTEnemyImmortal, TMsgRemoveBuff, onDebuffRecieved);
	DECL_MSG(TCompBTEnemyImmortal, TMsgEntityDead, onEntityDead);
	DECL_MSG(TCompBTEnemyImmortal, TMsgDeleteIAController, deleteIAController);
	DECL_MSG(TCompBTEnemyImmortal, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompBTEnemyImmortal, TMsgResetComponent, onComponentReset);
	DECL_MSG(TCompBTEnemyImmortal, TMsgAlertAI, onAlertAI);
}

void TCompBTEnemyImmortal::declareInLua() {
	EngineScripting.declareComponent<TCompBTEnemyImmortal>
		(
			"TCompBTEnemyImmortal",
			"getVariable", &TCompBTEnemyImmortal::getVariable,
			"setVariable", &TCompBTEnemyImmortal::setVariable,
			"interrupt", &TCompBTEnemyImmortal::interrupt,
			"alert", &TCompBTEnemyImmortal::alert
			);
}

void TCompBTEnemyImmortal::alert() {
	setVariable("isArena", true);
}

void TCompBTEnemyImmortal::onHitDamage(const TMsgHitDamage& msg) {
	interrupt("damageReceived");
}

void TCompBTEnemyImmortal::onBulletDamage(const TMsgBulletDamage& msg) {
	interrupt("damageReceived");
}

void TCompBTEnemyImmortal::onNailDamage(const TMsgNailDamage& msg) {
	interrupt("damageReceived");
}

