#include "mcv_platform.h"
#include "bt_resource.h"

class CBTResourceType : public CResourceType {
public:
	const char* getExtension(int idx = 0) const override { return "bt"; }

	const char* getName() const override { return "BTs"; }

	IResource * create(const std::string & filename) const override {
		CBT * new_res = new CBT(filename);
		new_res->setNameAndType(filename, this);
		return new_res;
	}
};

CBT::CBT(const std::string & filename) {
	jdata = Utils::loadJson(filename);
}

void CBT::onFileChanged(const std::string& filename) {
	if (filename != getName())
		return;
	jdata = Utils::loadJson(filename);
}

void CBT::renderInMenu() {
	std::string jstr = jdata.dump(2);
	ImGui::Text("%s", jstr.c_str());
}

template<> const CResourceType* getResourceTypeFor<CBT>() {
	static CBTResourceType resource_type;
	return &resource_type;
}



