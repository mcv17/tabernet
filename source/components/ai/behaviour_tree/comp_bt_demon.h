#pragma once

#include "comp_behaviour_tree.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompTransform;
class TCompHealth;
class TCompDemonController;

class TCompBTDemon : public TCompBehaviourTree {
	DECL_SIBLING_ACCESS();

public:
	void init() override;
	void implementFunctions() override;
	void updateVariables() override;

	void load(const json & j, TEntityParseContext & ctx);
	void debugInMenu();
	void renderDebug();

	static void registerMsgs();
	static void declareInLua();

private:
	/* ------------------ Variables ------------------ */

	map<string, CClock> _timers;

	/* Handles of the AI. */
	CHandle demonTransformH;
	CHandle demonControllerH;
	CHandle demonHealthH;
	CHandle playerHandle;
	CHandle playerTransformH;
	CHandle playerHealthH;

	/* General variables. */
	
	// Movement.
	float normalMovementSpeed = 2.f;
	float normalRotationSpeed = 10.f;
	struct TVisionCone {
		float angle = (float)M_PI * 2;
		float depth = 6.0f;
	} visionCone;

	// Entity is dead
	bool dead = false;
	// Percentage to start round 2.
	float lifePercentToChangeRound = 0.65f;
	// Prevents interruptions from happening while doing and action.
	bool ignore_interruptions = false;
	// Wether the player is performing an special attack. Used for counting time between attacks.
	bool on_special_attack = false;

	// Life is in the prefab file in the health component.

	/* Target variables. */
	std::string playerName; // Name of the target. In our game AI only attacks the player.
	float playerOffsetShootAt = 0.f; // Offset from the transform upwards. point where bullets will go to. Normally the center of mass of the character.

	/* Pick random direction general variables. */
	float objectiveYawToWanderTo;

	/* Gatekeeping variables. */
	Vector3 roomCenter;
	float distanceToMoveFromRoomCenter = 20.f; // How far from the center can the demon move.
	float thresholdReturnToRoom = 0.4f; // How close it must be to the room center to return to it when going back.

	/* Idle variables. */
	float timeBeforMakingDecisionOnIdle = 1.0f;

	/* Wander variables. */
	float maxYawChangeOnWander = 180.0f;
	float timeMovingInRandomDirection = 2.0f;

	/* Return to position states. */
	float tpToRoomCenterRotationPerSecond = 60.0f;
	float timeBeforeTP = 3.0f;

	/* Chase variables. */
	float chaseMovementSpeed = 5.f;
	float chaseRotationSpeed = 12.f;
	float addedDistanceToViewToForget = 3.f;

	/* Distance from player variables. */
	float maxAttackDistance = 6.f;
	float securityDistance = 3.f;
	float distanceToGetToTarget = (maxAttackDistance + securityDistance) / 2.0f;

	/* Ide war variables. */
	// Idle war movement.
	float warMovementSpeed = 8.f;
	float warRotationSpeed = 12.f;
	float maxYawChangeOnAttack = 360.0f; // How much yaw change can the player do.
	Vector3 objectiveDirectionToMoveTo;
	float timeMovingInRandomDirectionWhenAttacking = 0.5f;
	float timeBeforeBasicAttacks = 1.0f;
	float timeBeforeSpecialAttack = 5.f;
	float timeBeforeInvokeMinions = 10.0f;
	float facingThreshold = 15.0f;

	/* Escape from player. */
	bool justEscaping = true;
	float distanceExtraSafetySpace = 0.0f;
	float minExtraSafetySpace = 0.5f; // How much min distance it must leave from extra safety space.
	float maxExtraSafetySpace = 3.f; // How much max distance it must leave from extra safety space.

	/* Blast variables. */
	float timeBetweenShootsBlast = 0.1f;
	int currentShotsFired = 0;
	int numberOfShoots = 30;

	/* Orbit variables. */

	float orbitSpeedPerSecond = 20.0f;
	float orbitTime = 2.0f;
	float timeBetweenShotsOrbit = 0.5f;

	/* Sweep attacks. */

	float sweepRotation = 45.0f; // How many degrees in a second.
	float timeBetweenShotsSweep = 0.15f;
	float halfRang = RAD2DEG((float)M_PI_2);
	bool chargeSweepAttack = true;
	float yawDeltaToRotateOnStart = halfRang;
	float yawDeltaToRotateOnEnd = halfRang * 2;

	/* Tp behind player variables. */
	float timeForFastTp = 1.0f;
	float tpBehindPlayerRotationPerSecond = 1000.0f;

	/* These booleans should be passed to the animation fsm
	and pooled from there in each state. */

	/* Basic attack. */
	bool finished_basic_attack = false;
	
	/* Big fireball attack. */
	bool finished_big_fireball_attack = false;

	/* Blast attack. */
	bool finished_blast_animation = false;

	/* Lazer attack. */
	bool finished_cast_lazer = false;
	bool finished_lazer_attack = false;

	/* Fire from ground. */
	bool finished_fire_from_ground = false;
	int currentFireTrapsSpawned = 0;
	int numOfFireTraps = 5;
	float timeBetweenFireTraps = 1.0f;

	/* Cast minions. */
	bool finished_cast_minions = false;
	int currentMinionsSpawned = 0;
	int num_minions_to_cast = 6;
	float timeBetweenMinions = 0.5f;

	/* Boost minions. */
	bool finished_boost_minions = false;

	/* Laugh */
	bool finished_laugh = false;

	/* Happy intro */
	bool finished_happy_intro = false;

	/* Sad intro */
	bool finished_sad_intro = false;

	/* Happy sound. */
	bool finished_happy_sound = false;

	/* Sad sound. */
	bool finished_sad_sound = false;

	/* Low impact. */
	bool finished_low_hit = false;

	/* Critical hit. */
	bool finished_critical_hit = false;

	/* Dying variables. */
	bool finished_dying = false;

	/* Dead variables. */
	bool is_dead = false;
	float timeBeforeDisappearing = 2.0f;


	/* Actions. */

	ActionResult Idle(); // Idle Handle.
	ActionResult Wander(); // Wander handle.
	ActionResult ReturnToDefaultPosTP();
	ActionResult ReturnToDefaultPosWalking();
	ActionResult Chase();
	ActionResult IdleWar();

	ActionResult EscapeFromPlayer();
	ActionResult TPToSafety();

	/* Base attack. */
	ActionResult BasicAttack();

	/* Strong attacks. */
	ActionResult TpBehindPlayer();
	ActionResult BigFireball();
	ActionResult Blast();
	ActionResult SweepAttack();
	ActionResult CastLazer();
	ActionResult ShootLazer();
	ActionResult FireFromGround();
	ActionResult OrbitLeftAroundPlayer();
	ActionResult OrbitRightAroundPlayer();

	/* Invoke minions. */
	ActionResult CastMinionInvocation();
	ActionResult Laugh();
	ActionResult BoostMinions();

	ActionResult DoIntroLaugh();
	ActionResult DoAngryLaugh();

	ActionResult DoHappySound();
	ActionResult DoSadSound();
	ActionResult LowImpact();
	ActionResult CriticalImpact();
	ActionResult Dying();
	ActionResult Dead();

	/* Conditions */
	bool CheckIfOutsideRoom(); // Checks if the bot is far away from the room. Used for gatekeeping.
	bool ViewPlayer(); // Checks if the bot views the player.
	bool CheckIfChaseTarget(); // Checks the distance to the player and chases it if necessary.
	bool CheckIfPlayerTooClose(); // Checks if the player is too close. Escapes if so.
	
	/* Message functions. */
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onLogicStatus(const TMsgLogicStatus & msg);
	void onEntityDead(const TMsgEntityDead & msg);
	void onImpactHit(const TMsgHitDamage & msg);
	void onImpactBullet(const TMsgBulletDamage & msg);
	void onImpactNail(const TMsgNailDamage & msg);

	/* Private functions. */

	/* ---------------- Helper functions ---------------- */

	/* Checks handles to see they are still valid.*/
	bool checkHandles();

	// Checks if somebody is in the room. (A simple sphere test for now).
	bool CheckOnRoom(const Vector3 & currentPosition);

	/* Wander helper functions. */
	
	/* Sets the objetive yaw of the entity to a new one. */
	void SetRandomWanderDirection(float currentYaw, float maxYawChange);
	/* Moves an entity in the direction of the objective yaw. */
	void MoveInRandomDirection(const TCompTransform * transform, TCompDemonController * contr,
		float currentYaw, float currentPitch,
		float movementSpeed, float rotationSpeed, float dt);

	/* Idle - Wander - Chase helper functions */
	// Checks if the bot views the player. A second method similar to the condition but we an added distance.
	bool ViewPlayerDistance(TCompTransform * transform, const Vector3 & targetPos, float nDistance);
	// Distance to a target.
	bool CheckDistanceToTargetSmaller(const Vector3 & currentPosition, const Vector3 & targetPos, float distanceToGetToTarget);

	/* Idle war helper functions. */
	void GetRandomDirectionOnWar(TCompTransform * transform);
	void MoveInRandomDirectionFacingCurrentOrientation(TCompDemonController * contr, float movementSpeed, float dt);

	/* Orbit helper functions. */
	void OrbitMovement(TCompTransform * transform, TCompDemonController * contr,
		const Vector3 & currentPosition, const Vector3 & playerPos, float orbitSpeedPerSecond, float dt);

	/* Extra functions. */
	bool isTargetAlive(); // Checks if the player is alive.
	bool isFacingTarget(const TCompTransform * demonTransform, const TCompTransform * targetTransform, float threshold); 	// Checks if the demon is facing the target.
	void ShotFireBall(const TCompTransform * transform); 	// Shoots a fireball. Target shoot point is the point where the bullets will point to. Normally, the center of the player.
	void ShotAimedFireBall(const TCompTransform * transform, const TCompTransform * playerTransform); 	// Shoots a fireball. Target shoot point is the point where the bullets will point to. Normally, the center of the player.
	void ShotBigFireBall(const TCompTransform * transform, const TCompTransform * playerTransform);
	Vector3 getTargetAimingPos(const TCompTransform * playerTransform); // Returns the aiming pos where bullets should go to. Adds the target offset to the player transform.
	void RotateToTarget(TCompTransform * transform, TCompDemonController * contr, const Vector3 & targetPos, float rotationSpeed, float dt); 	// Rotates the demon to face the target.
};