#pragma once

using namespace std;

class TCompBehaviourTree;

class BehaviourTreeNode
{

public:
	enum Type {
		RANDOM, SEQUENCE, PRIORITY, ACTION, DECORATOR
	};

	enum ActionResult {
		LEAVE, STAY, INTERRUPT_AND_LEAVE, INTERRUPT_AND_STAY, ABORT
	};

	BehaviourTreeNode() {}
	BehaviourTreeNode(string name);

	void create(string name);
	bool isRoot();
	void setParent(BehaviourTreeNode * node);
	void setRight(BehaviourTreeNode * node);
	void addChild(BehaviourTreeNode * node);
	void setType(Type t);
	void recalc(TCompBehaviourTree * tree);
	string getName();
	void setRatios(std::vector<float> & ratios);

private:
	std::string _name;
	Type _type;
	vector<BehaviourTreeNode *> _children;
	BehaviourTreeNode * _parent;
	BehaviourTreeNode * _right;

	std::vector<float> _ratios;
	int _sumOfRatios;
};