#include "mcv_platform.h"
#include "comp_bt_demon.h"

#include "entity/entity_parser.h"

#include "components/common/comp_transform.h"
#include "components/common/comp_health.h"
#include "components/controllers/comp_demon_controller.h"

#include "engine.h"

DECL_OBJ_MANAGER("ai_bt_demon", TCompBTDemon);

void TCompBTDemon::init() {
	setVariable("onStartFirstRound", true);
	setVariable("onStartSecondRound", true);

	setVariable("basicAttackReady", true);
	setVariable("strongAttackReady", false);
	setVariable("invokeMinions", false);
	setVariable("boostMinions", false);

	setVariable("roundOne", true);

	_timers["idle"] = CClock();
	_timers["tp"] = CClock();
	_timers["idle_war"] = CClock();
	_timers["basic_attack"] = CClock();
	_timers["special_attack"] = CClock();
	_timers["blast"] = CClock();
	_timers["sweep"] = CClock();
	_timers["fireFromGround"] = CClock();
	_timers["orbit"] = CClock();
	_timers["orbit_shots"] = CClock();
	_timers["tp"] = CClock();
	_timers["dead"] = CClock();
	_timers["cast_minions"] = CClock();

	_timers["basic_attack"].start();
	_timers["special_attack"].start();
	_timers["invoke_minions"].start();
}


void TCompBTDemon::implementFunctions() {
	/* Conditions. */
	IMPLEMENT_CONDITION("viewPlayer", TCompBTDemon, ViewPlayer);
	IMPLEMENT_CONDITION("checkIfOutsideRoom", TCompBTDemon, CheckIfOutsideRoom);
	IMPLEMENT_CONDITION("checkIfChaseTarget", TCompBTDemon, CheckIfChaseTarget);
	IMPLEMENT_CONDITION("playerTooClose", TCompBTDemon, CheckIfPlayerTooClose);

	/* Actions. */
	IMPLEMENT_ACTION("Dying", TCompBTDemon, Dying);
	IMPLEMENT_ACTION("Dead", TCompBTDemon, Dead);
	IMPLEMENT_ACTION("Big impact", TCompBTDemon, CriticalImpact);
	IMPLEMENT_ACTION("Low impact", TCompBTDemon, LowImpact);

	IMPLEMENT_ACTION("On Hit - Happy sound", TCompBTDemon, DoHappySound);
	IMPLEMENT_ACTION("On Miss - Sad sound", TCompBTDemon, DoSadSound);

	IMPLEMENT_ACTION("Walk to room with TP", TCompBTDemon, ReturnToDefaultPosTP);
	IMPLEMENT_ACTION("Walk to room", TCompBTDemon, ReturnToDefaultPosWalking);

	IMPLEMENT_ACTION("Chase", TCompBTDemon, Chase);

	IMPLEMENT_ACTION("Stay on position", TCompBTDemon, Idle);
	IMPLEMENT_ACTION("Walk around", TCompBTDemon, Wander);

	IMPLEMENT_ACTION("Intro laugh", TCompBTDemon, DoIntroLaugh);
	IMPLEMENT_ACTION("Angry laugh", TCompBTDemon, DoAngryLaugh);

	/* Idle war. */
	IMPLEMENT_ACTION("Idle war", TCompBTDemon, IdleWar);

	/* Base attack. */
	IMPLEMENT_ACTION("Basic ranged attack", TCompBTDemon, BasicAttack);

	/* Escape behaviour */
	IMPLEMENT_ACTION("Escape on foot", TCompBTDemon, EscapeFromPlayer);
	IMPLEMENT_ACTION("TP to safety", TCompBTDemon, TPToSafety);

	/* Strong attacks. */
	IMPLEMENT_ACTION("TP behind", TCompBTDemon, TpBehindPlayer);
	IMPLEMENT_ACTION("Big fireball", TCompBTDemon, BigFireball);
	IMPLEMENT_ACTION("Blast attack", TCompBTDemon, Blast);
	IMPLEMENT_ACTION("Sweep attack", TCompBTDemon, SweepAttack);
	IMPLEMENT_ACTION("Cast lazer", TCompBTDemon, CastLazer);
	IMPLEMENT_ACTION("Shoot lazer", TCompBTDemon, ShootLazer);
	IMPLEMENT_ACTION("Fire from ground", TCompBTDemon, FireFromGround);
	IMPLEMENT_ACTION("Orbit left", TCompBTDemon, OrbitLeftAroundPlayer);
	IMPLEMENT_ACTION("Orbit right", TCompBTDemon, OrbitRightAroundPlayer);

	/* Minion attacks. */
	IMPLEMENT_ACTION("Casting minions", TCompBTDemon, CastMinionInvocation);
	IMPLEMENT_ACTION("Laugh", TCompBTDemon, Laugh);
	IMPLEMENT_ACTION("Boost minions", TCompBTDemon, BoostMinions);
}

void TCompBTDemon::updateVariables() {
	if (!checkHandles()) return;

	CClock & basicAttack = _timers["basic_attack"];
	CClock & specialAttack = _timers["special_attack"];
	CClock & invokeMinions = _timers["invoke_minions"];

	if (timeBeforeBasicAttacks < basicAttack.elapsed()) {
		setVariable("basicAttackReady", true);
		basicAttack.stop();
	}

	if (timeBeforeSpecialAttack < specialAttack.elapsed()) {
		setVariable("strongAttackReady", true);
		specialAttack.stop();
	}

	if (timeBeforeInvokeMinions < invokeMinions.elapsed()) {
		setVariable("invokeMinions", true);
		invokeMinions.stop();
	}

	if (basicAttack.isStopped())
		basicAttack.start();
	if (specialAttack.isStopped() && !on_special_attack)
		specialAttack.start();
	if (invokeMinions.isStopped())
		invokeMinions.start();

	// Check if we must boost minions. This will be done in the future.

	// Life variable.
	TCompHealth * health = demonHealthH;
	float currentHealth = health->getCurrentHealth();
	float maxHealth = health->getMaxHealth();
	setVariable("life", currentHealth);
	setVariable("roundOne", currentHealth >= (lifePercentToChangeRound * maxHealth));
}

void TCompBTDemon::load(const json & j, TEntityParseContext & ctx) {
	TCompBehaviourTree::load(j, ctx);

	// General variables.
	playerName = j.value("player_name", "Player");
	playerOffsetShootAt = j.value("playerOffsetShootAt", playerOffsetShootAt);
	normalMovementSpeed = j.value("normal_movement_speed", normalMovementSpeed);
	normalRotationSpeed = j.value("normal_rotation_speed", normalRotationSpeed);
	visionCone.depth = j.value("vision_depth", visionCone.depth);
	lifePercentToChangeRound = j.value("life_percentage_before_changing_round", lifePercentToChangeRound);

	// Gatekeeping.
	roomCenter = loadVector3(j, "room_center");
	distanceToMoveFromRoomCenter = j.value("distance_to_move_from_room_center", distanceToMoveFromRoomCenter);
	thresholdReturnToRoom = j.value("threshold_return_to_room", thresholdReturnToRoom);

	// Idle variables
	timeBeforMakingDecisionOnIdle = j.value("idle_time_before_decision", timeBeforMakingDecisionOnIdle);

	// Wander variables
	maxYawChangeOnWander = DEG2RAD(j.value("wander_max_yaw_change", maxYawChangeOnWander));
	timeMovingInRandomDirection = j.value("wander_time_in_random_dir", timeMovingInRandomDirection);

	// Return tp.
	tpToRoomCenterRotationPerSecond = DEG2RAD(j.value("tpToRoom_rotation_second", tpToRoomCenterRotationPerSecond));
	timeBeforeTP = j.value("tpToRoom_time_before_tp", timeBeforeTP);

	// Chase variables.
	chaseMovementSpeed = j.value("chase_chase_movement_speed", chaseMovementSpeed);
	chaseRotationSpeed = j.value("chase_chase_rotation_speed", chaseRotationSpeed);
	maxAttackDistance = j.value("chase_max_attack_distance", maxAttackDistance);
	securityDistance = j.value("chase_distance_keep_from_player", securityDistance);
	distanceToGetToTarget = (maxAttackDistance + securityDistance) / 2.0f;
	addedDistanceToViewToForget = j.value("addedDistanceToViewToForget", addedDistanceToViewToForget);

	// Idle war.
	warMovementSpeed = j.value("idleWar_movement_speed", warMovementSpeed);
	warRotationSpeed = j.value("idleWar_rotation_speed", warRotationSpeed);
	maxYawChangeOnAttack = DEG2RAD(j.value("idleWar_max_yaw_change_attack", maxYawChangeOnAttack));
	timeMovingInRandomDirectionWhenAttacking = j.value("idleWar_time_moving_random_dir_attacking", timeMovingInRandomDirectionWhenAttacking);
	timeBeforeBasicAttacks = j.value("idleWar_time_between_shots_normal_attack", timeBeforeBasicAttacks);
	timeBeforeSpecialAttack = j.value("idleWar_time_before_deciding_if_special_attack", timeBeforeSpecialAttack);
	timeBeforeInvokeMinions = j.value("idleWar_time_before_invoke_minions", timeBeforeInvokeMinions);
	facingThreshold = DEG2RAD(j.value("facing_threshold", facingThreshold));

	// Escape.
	minExtraSafetySpace = j.value("min_extra_safety_distance", minExtraSafetySpace);
	maxExtraSafetySpace = j.value("max_extra_safety_distance", maxExtraSafetySpace);
	minExtraSafetySpace = std::min(minExtraSafetySpace, maxExtraSafetySpace);
	maxExtraSafetySpace = std::max(minExtraSafetySpace, maxExtraSafetySpace);

	// Blast attack.
	timeBetweenShootsBlast = j.value("blast_time_between_shoots", timeBetweenShootsBlast);
	numberOfShoots = j.value("blast_number_of_shoots", numberOfShoots);

	// Orbit attack.
	orbitSpeedPerSecond = j.value("orbit_rotation_angle_per_second", orbitSpeedPerSecond);
	orbitTime = j.value("orbit_time", orbitTime);;
	timeBetweenShotsOrbit = j.value("orbit_time_between_shots", timeBetweenShotsOrbit);

	// Sweep attack.
	sweepRotation = DEG2RAD(j.value("sweep_rotation_speed", sweepRotation));
	timeBetweenShotsSweep = j.value("sweep_time_between_shots", timeBetweenShotsSweep);
	halfRang = DEG2RAD(j.value("sweep_half_rang_in_degrees", halfRang));
	yawDeltaToRotateOnStart = halfRang;
	yawDeltaToRotateOnEnd = halfRang * 2;

	// TP Behind player.
	timeForFastTp = j.value("tp_behind_player_time_fast_tp", timeForFastTp);
	tpBehindPlayerRotationPerSecond = DEG2RAD(j.value("tp_behind_player_rotation_speed", tpBehindPlayerRotationPerSecond));

	// Fire traps.
	numOfFireTraps = j.value("fireTraps_num_of_firetraps", numOfFireTraps);
	timeBetweenFireTraps = j.value("fireTraps_time_between_firetraps", timeBetweenFireTraps);

	// Cast minions.
	num_minions_to_cast= j.value("cast_minions_num_minions", num_minions_to_cast);
	timeBetweenMinions = j.value("cast_time_between_minions", timeBetweenMinions);

	// Dead.
	timeBeforeDisappearing = j.value("time_before_disappearing", timeBeforeDisappearing);
}

void TCompBTDemon::debugInMenu() {
	ImGui::LabelText("Current state: ", getNodeName().c_str());

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("General variables");
	ImGui::DragFloat3("Shooting offset target (upwards from transform): ", &playerOffsetShootAt, 0.01f, 0.0f, 1000.f);
	ImGui::DragFloat("Normal Movement Speed: ", &normalMovementSpeed, 0.01f, 0.0f, 1000.f);
	ImGui::DragFloat("Normal Rotation Speed: ", &normalRotationSpeed, 0.01f, 0.0f, 1000.f);
	ImGui::DragFloat("Vision depth: ", &visionCone.depth, 0.01f, 0.0f, 10000.f);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Gatekeeping");
	ImGui::DragFloat3("Room center: ", &roomCenter.x, 0.01f, -100000.f, 100000.f);
	ImGui::DragFloat("Distance to move from room center: ", &distanceToMoveFromRoomCenter, 0.01f, 0.0f, 10000.f);
	ImGui::DragFloat("Room Threshold (How close to get to the center when returnign to it): ", &thresholdReturnToRoom, 0.0001f, 0.0f, 10.f);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Idle - State");
	ImGui::DragFloat("Idle time before decision: ", &timeBeforMakingDecisionOnIdle, 0.01f, 0.0f, 10000.f);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Wander - State");
	ImGui::DragFloat("Wander time moving in random dir: ", &timeMovingInRandomDirection, 0.01f, 0.0f, 10000.f);
	float yawToDeg = RAD2DEG(maxYawChangeOnWander);
	if (ImGui::DragFloat("Wander max variation yaw: ", &yawToDeg, 0.01f, 0.0f, 360.f))
		maxYawChangeOnWander = DEG2RAD(yawToDeg);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Return to room center.");
	ImGui::DragFloat("Casting time tp back to room: ", &timeBeforeTP, 0.01f, 0.0f, 1000.f);
	float yawTPRoomToDeg = RAD2DEG(tpToRoomCenterRotationPerSecond);
	if (ImGui::DragFloat("Tp Speed to Room Center (in Rads): ", &yawTPRoomToDeg, 0.01f, -10000.0f, 10000.0f))
		tpToRoomCenterRotationPerSecond = DEG2RAD(yawTPRoomToDeg);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Chase - State");
	ImGui::DragFloat("Chase movement speed: ", &chaseMovementSpeed, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Chase rotation speed: ", &chaseRotationSpeed, 0.01f, 0.0f, 100.f);
	bool changed;
	changed |= ImGui::DragFloat("Max attack distance: ", &maxAttackDistance, 0.01f, 0.0f, 100.f);
	changed |= ImGui::DragFloat("Distance to keep from player: ", &securityDistance, 0.01f, 0.0f, 100.f);
	if (changed)
		distanceToGetToTarget = (maxAttackDistance + securityDistance) / 2.0f;
	ImGui::DragFloat("Extra forger distance on chase: ", &addedDistanceToViewToForget, 0.01f, 1.f, 100.f);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Idle War - State");

	ImGui::DragFloat("Idle war movement speed: ", &warMovementSpeed, 0.01f, 0.0f, 1000.f);
	ImGui::DragFloat("Idle war rotation speed: ", &warRotationSpeed, 0.01f, 0.0f, 1000.f);
	float yawToDegAttack = RAD2DEG(maxYawChangeOnAttack);
	if (ImGui::DragFloat("Max yaw change movement ", &yawToDegAttack, 0.01f, 0.0f, 360.f))
		maxYawChangeOnAttack = DEG2RAD(yawToDegAttack);
	ImGui::DragFloat("Time moving in random dir when attacking: ", &timeMovingInRandomDirectionWhenAttacking, 0.01f, 0.0f, 1000.f);
	ImGui::DragFloat("Normal ratio of fire, basid attack: ", &timeBeforeBasicAttacks, 0.0001f, 0.0f, 10.f);
	float yawFaceThreshold = RAD2DEG(facingThreshold);
	if (ImGui::DragFloat("Yaw face threshold: ", &yawFaceThreshold, 0.01f, 0.f, 360.0f))
		facingThreshold = DEG2RAD(yawFaceThreshold);
	ImGui::DragFloat("Time before a special attack is considered: ", &timeBeforeSpecialAttack, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Time before invoking minions is considered: ", &timeBeforeInvokeMinions, 0.01f, 0.0f, 100.f);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Escape state");
	ImGui::DragFloat("Min distance to leave from player: ", &minExtraSafetySpace, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Max distance to leave from player: ", &maxExtraSafetySpace, 0.01f, 0.0f, 100.f);
	minExtraSafetySpace = std::min(minExtraSafetySpace, maxExtraSafetySpace);
	maxExtraSafetySpace = std::max(minExtraSafetySpace, maxExtraSafetySpace);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Blast attack");
	ImGui::DragFloat("Time between shoots blast: ", &timeBetweenShootsBlast, 0.0001f, 0.0f, 100.f);
	ImGui::DragInt("Number of shoots of blast: ", &numberOfShoots, 0, 100);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Orbiting states");
	ImGui::DragFloat("Orbit speed per second: ", &orbitSpeedPerSecond, 0.01f, 0.0f, 360.0f);
	ImGui::DragFloat("Orbit time: ", &orbitTime, 0.01f, 0.0f, 100.f);
	ImGui::DragFloat("Time between shoots orbit: ", &timeBetweenShotsOrbit, 0.0001f, 0.0f, 100.f);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Sweep states");
	float yawSweepRotation = RAD2DEG(sweepRotation);
	if (ImGui::DragFloat("Rotation speed in degrees per second in sweep attack: ", &yawSweepRotation, 0.01f, 0.0f, 360.0f))
		sweepRotation = DEG2RAD(yawSweepRotation);
	ImGui::DragFloat("Time between shoots sweep: ", &timeBetweenShotsSweep, 0.0001f, 0.0f, 100.f);
	float yawSweep = RAD2DEG(halfRang);
	if (ImGui::DragFloat("Yaw of the sweep (how wide will the sweep be): ", &yawSweep, 0.01f, 0.0f, 360.0f)) {
		halfRang = DEG2RAD(yawSweep);
		yawDeltaToRotateOnStart = halfRang;
		yawDeltaToRotateOnEnd = halfRang * 2;
	}

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("TP behind player states");
	ImGui::DragFloat("Time for fast tp behind player: ", &timeForFastTp, 0.01f, 0.0f, 10.f);
	float rotationSpeedTPBehindPlayer = RAD2DEG(tpBehindPlayerRotationPerSecond);
	if (ImGui::DragFloat("Tp behind player rotation per second: ", &tpBehindPlayerRotationPerSecond, 0.01f, -360.0f, 360.0f))
		tpBehindPlayerRotationPerSecond = DEG2RAD(rotationSpeedTPBehindPlayer);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Fire traps");
	ImGui::DragInt("Num of fire traps to spawn: ", &numOfFireTraps, 0, 10);
	ImGui::DragFloat("Time between fire traps: ", &timeBetweenFireTraps, 0.01f, 0.0f, 10.f);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Cast minions");
	ImGui::DragInt("Num of minions to cast: ", &num_minions_to_cast, 0, 1000);
	ImGui::DragFloat("Time between minions: ", &timeBetweenMinions, 0.01f, 0.0f, 10.f);

	ImGui::Separator();
	ImGui::Spacing();

	ImGui::Text("Dead state");
	ImGui::DragFloat("Time before disappearing: ", &timeBeforeDisappearing, 0.01f, 0.0f, 10.f);
}

void TCompBTDemon::renderDebug() {
	TCompTransform * transform = demonTransformH;
	if (!transform) return;

	// Vision
	Vector3 currentPosition = transform->getPosition();
	drawCircle(currentPosition, visionCone.depth);

	// Distance from room center
	drawCircle(roomCenter, distanceToMoveFromRoomCenter, Vector4(1, 0, 1, 1));

	// Max attack
	drawCircle(currentPosition, maxAttackDistance, Vector4(1, 1, 0.4f, 1));

	// Distance from room center
	drawCircle(currentPosition, distanceToGetToTarget, Vector4(1, 0, 0, 1));

	// Distance from room center
	drawCircle(currentPosition, securityDistance, Vector4(1, 1, 0, 1));
}

void TCompBTDemon::registerMsgs() {
	DECL_MSG(TCompBTDemon, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompBTDemon, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompBTDemon, TMsgEntityDead, onEntityDead);
	DECL_MSG(TCompBTDemon, TMsgHitDamage, onImpactHit);
	DECL_MSG(TCompBTDemon, TMsgBulletDamage, onImpactBullet);
	DECL_MSG(TCompBTDemon, TMsgNailDamage, onImpactNail);
}

void TCompBTDemon::declareInLua() {
	EngineScripting.declareComponent<TCompBTDemon>
	(
		"TCompBTDemon",
		"getVariable", &TCompBTDemon::getVariable,
		"setVariable", &TCompBTDemon::setVariable,
		"interrupt", &TCompBTDemon::interrupt
	);
}

void TCompBTDemon::onEntityCreated(const TMsgEntityCreated & msg){
	init();
}

void TCompBTDemon::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompBTDemon::onEntityDead(const TMsgEntityDead & msg){
	if (!dead)
		interrupt("damageRecieved");
}

void TCompBTDemon::onImpactHit(const TMsgHitDamage & msg){
	if (!getVariable("damageRecieved") && !ignore_interruptions)
		interrupt("damageRecieved");
}

void TCompBTDemon::onImpactBullet(const TMsgBulletDamage & msg){
	if (!getVariable("damageRecieved") && !ignore_interruptions)
		interrupt("damageRecieved");
}

void TCompBTDemon::onImpactNail(const TMsgNailDamage & msg){
	if(!getVariable("damageRecieved") && !ignore_interruptions)
		interrupt("damageRecieved");
}


/* Actions. */

// Idle Handle.
ActionResult TCompBTDemon::Idle(){
	if (!checkHandles()) return ActionResult::ABORT;

	CClock & clock = _timers["idle"];
	if (!clock.isStarted()) clock.start();

	// Check conditions that could make us stop our action.
	if (CheckIfOutsideRoom() || ViewPlayer() || interrupted()) {
		clock.stop();
		return ActionResult::ABORT;
	}

	if (clock.elapsed() > timeBeforMakingDecisionOnIdle) {
		clock.stop();
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

// Wander handle.
ActionResult TCompBTDemon::Wander() {
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;

	float currentYaw, currentPitch;
	transform->getAngles(&currentYaw, &currentPitch);

	// If just started, we pick a random direction to move to.
	CClock & clock = _timers["idle"];
	if (!clock.isStarted()) {
		SetRandomWanderDirection(currentYaw, maxYawChangeOnWander);
		clock.start();
	}

	// Check conditions that could make us stop our action.
	if (CheckIfOutsideRoom() || ViewPlayer() || interrupted()) {
		clock.stop();
		return ActionResult::ABORT;
	}

	/* Random movement. */
	// Wander in that direction.
	MoveInRandomDirection(transform, contr, currentYaw, currentPitch, normalMovementSpeed, normalRotationSpeed, Time.delta);

	// check if we must stop moving in said direction. If so, stop and check if we must change state.
	if (clock.elapsed() > timeMovingInRandomDirection) {
		clock.stop();
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}


ActionResult TCompBTDemon::ReturnToDefaultPosWalking() {
	if (!checkHandles()) return ActionResult::ABORT;

	// Check conditions that could make us stop our action.
	// If we view player and he is on the room, we attack him again.
	if (ViewPlayer() || interrupted())
		return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;

	// Move to the center of the room.
	RotateToTarget(transform, contr, roomCenter, normalRotationSpeed, Time.delta);
	contr->move(transform->getFront() * normalMovementSpeed * Time.delta);

	// If you got to the position.
	if (CheckDistanceToTargetSmaller(transform->getPosition(), roomCenter, thresholdReturnToRoom))
		return ActionResult::LEAVE;

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::ReturnToDefaultPosTP() {
	if (!checkHandles()) return ActionResult::ABORT;

	CClock & clock = _timers["tp"];
	if (!clock.isStarted()) {
		clock.start();
		ignore_interruptions = true;
	}

	// Check conditions that could make us stop our action.
	// This are, viewing the player inside the room and dying,
	if (ViewPlayer() || interrupted()) {
		ignore_interruptions = false;
		return ActionResult::ABORT;
	}

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;

	// Rotate very fast while casting tp.
	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	contr->setRotation(yaw + Time.delta * tpToRoomCenterRotationPerSecond, pitch);

	// check if we must stop moving in said direction. If so, stop and check if we must change state.
	if (timeBeforeTP < clock.elapsed()) {
		clock.stop();
		contr->setPosition(roomCenter);
		ignore_interruptions = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::Chase() {
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;
	TCompTransform * playerTransform = playerTransformH;

	// Chase player.
	Vector3 currentPosition = transform->getPosition();
	Vector3 playerPos = playerTransform->getPosition();
	RotateToTarget(transform, contr, playerPos, chaseRotationSpeed, Time.delta);
	contr->move(transform->getFront() * chaseMovementSpeed * Time.delta);

	// If we don't view player, get back. Here we add the extra distance to forget.
	if (CheckIfOutsideRoom() || !ViewPlayerDistance(transform, playerTransform->getPosition(), visionCone.depth + addedDistanceToViewToForget) || interrupted())
		return ActionResult::ABORT;

	// Check distance to target.
	if(CheckDistanceToTargetSmaller(currentPosition, playerPos, distanceToGetToTarget))
		return ActionResult::LEAVE;

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::IdleWar() {
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;
	TCompTransform * playerTransform = playerTransformH;

	CClock & idle_clock = _timers["idle_war"];
	if (!idle_clock.isStarted()) {
		idle_clock.start();
		GetRandomDirectionOnWar(transform);
	}

	Vector3 currentPosition = transform->getPosition();
	Vector3 playerPos = playerTransform->getPosition();

	if (CheckIfOutsideRoom() || interrupted()
		|| !CheckDistanceToTargetSmaller(currentPosition, playerPos, maxAttackDistance)
		|| CheckDistanceToTargetSmaller(currentPosition, playerPos, securityDistance)) {
		idle_clock.stop();
		return ActionResult::ABORT;
	}

	// Always move in a direction to make it harder for the player to shoot at.
	MoveInRandomDirectionFacingCurrentOrientation(contr, warMovementSpeed, Time.delta);
	// Try to always face the player.
	RotateToTarget(transform, contr, playerTransform->getPosition(), warRotationSpeed, Time.delta);

	// Check if it is necessary to pick a new direction.
	if (timeMovingInRandomDirectionWhenAttacking < idle_clock.elapsed())
		idle_clock.stop();

	if (isFacingTarget(transform, playerTransform, facingThreshold)) {
		// Shoot to player.
		if (getVariable("basicAttackReady")) {
			idle_clock.stop();
			return ActionResult::LEAVE;
		}
		// Decide if nessary to do random attack.
		if (getVariable("strongAttackReady")){
			idle_clock.stop();
			return ActionResult::LEAVE;
		}
		// Stop if we can invoke minions and we are not on round one anymore.
		if (getVariable("invokeMinions") && !getVariable("roundOne")) {
			idle_clock.stop();
			return ActionResult::LEAVE;
		}
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::EscapeFromPlayer(){
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;
	TCompTransform * playerTransform = playerTransformH;

	if (justEscaping) {
		distanceExtraSafetySpace = RNG.f(maxExtraSafetySpace - minExtraSafetySpace) + minExtraSafetySpace;
		justEscaping = false;
	}

	Vector3 currentPosition = transform->getPosition();
	Vector3 playerPos = playerTransform->getPosition();

	if (CheckIfOutsideRoom() || interrupted() || !CheckDistanceToTargetSmaller(currentPosition, playerPos, securityDistance + distanceExtraSafetySpace)) {
		justEscaping = true;
		return ActionResult::ABORT;
	}

	// Face player but move backwards.
	RotateToTarget(transform, contr, playerPos, chaseRotationSpeed, Time.delta);
	contr->move(-transform->getFront() * chaseMovementSpeed * Time.delta);
	if (isFacingTarget(transform, playerTransform, facingThreshold)) {
		if (getVariable("basicAttackReady")) {
			justEscaping = true;
			return ActionResult::LEAVE;
		}
	}
	return ActionResult::STAY;
}

ActionResult TCompBTDemon::TPToSafety(){
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;
	TCompTransform * playerTransform = playerTransformH;
	
	CClock & clock = _timers["tp"];
	if (!clock.isStarted()) {
		clock.start();
		ignore_interruptions = true;
	}

	if (interrupted()) {
		clock.stop();
		ignore_interruptions = false;
		return ActionResult::ABORT;
	}

	// Rotate very fast while casting tp.
	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	contr->setRotation(yaw + Time.delta * tpBehindPlayerRotationPerSecond, pitch);

	// Now, pick a point and TP to it.
	if (timeForFastTp < clock.elapsed()) {
		clock.stop();
		ignore_interruptions = false;

		// Get behind the player facing it and attack.
		Vector3 playerPos = playerTransform->getPosition();
		Vector3 demonPos = transform->getPosition();
		Vector3 dirFromDemonToPlayer = playerPos - demonPos;
		dirFromDemonToPlayer.Normalize();
		Vector3 posBehindPlayer = playerPos + dirFromDemonToPlayer * distanceToGetToTarget;
		contr->setPosition(posBehindPlayer); // In the collider.

		// we move the transform to calculate the actual rotation we must be at to point to the player.
		transform->setPosition(posBehindPlayer);
		float yaw, pitch;
		transform->getAngles(&yaw, &pitch);
		contr->setRotation(yaw + transform->getDeltaYawToAimTo(playerPos), pitch);
		transform->setPosition(demonPos); // We still have not moved, this will be done by the physics who will update the transform.
		
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::BasicAttack() {
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_basic_attack) {
		ignore_interruptions = true;
		// Play animation of shooting. At the end, shoot.
		finished_basic_attack = true;
	}

	if (interrupted()) {
		finished_basic_attack = false;
		ignore_interruptions = false;
		return ActionResult::ABORT;
	}

	if (finished_basic_attack) {
		TCompTransform * transform = demonTransformH;
		ShotFireBall(transform);
		finished_basic_attack = false;
		ignore_interruptions = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

/* Strong attacks. */
ActionResult TCompBTDemon::TpBehindPlayer() {
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;
	TCompTransform * playerTransform = playerTransformH;

	CClock & clock = _timers["tp"];
	if (!clock.isStarted()) {
		clock.start();
		ignore_interruptions = true;
		on_special_attack = true;
	}

	if (interrupted()) {
		clock.stop();
		ignore_interruptions = false;
		on_special_attack = false;
		return ActionResult::ABORT;
	}

	// Rotate very fast while casting tp.
	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	contr->setRotation(yaw + Time.delta * tpBehindPlayerRotationPerSecond, pitch);

	// Now, pick a point and TP to it.
	if (timeForFastTp < clock.elapsed()) {
		clock.stop();
		ignore_interruptions = false;
		on_special_attack = false;

		// Get behind the player facing it and attack.
		Vector3 playerPos = playerTransform->getPosition();
		Vector3 demonPos = transform->getPosition();
		Vector3 dirFromDemonToPlayer = playerPos - demonPos;
		dirFromDemonToPlayer.Normalize();
		Vector3 posBehindPlayer = playerPos + dirFromDemonToPlayer * distanceToGetToTarget;
		contr->setPosition(posBehindPlayer); // In the collider.

		// we move the transform to calculate the actual rotation we must be at to point to the player.
		transform->setPosition(posBehindPlayer);
		float yaw, pitch;
		transform->getAngles(&yaw, &pitch);
		contr->setRotation(yaw + transform->getDeltaYawToAimTo(playerPos), pitch);
		transform->setPosition(demonPos); // We still have not moved, this will be done by the physics who will update the transform.

		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::BigFireball() {
	if (!checkHandles()) return ActionResult::ABORT;

	// On start.
	if (!finished_big_fireball_attack) {
		ignore_interruptions = true;
		on_special_attack = true;
		// Play animation of shooting. At the end, shoot.
		finished_big_fireball_attack = true;
	}

	// On interrupt
	if (interrupted()) {
		ignore_interruptions = false;
		on_special_attack = false;
		finished_big_fireball_attack = false;
		return ActionResult::ABORT;
	}

	// On end.
	if (finished_big_fireball_attack) {
		TCompTransform * transform = demonTransformH;
		TCompTransform * playerTransform = playerTransformH;
		ShotBigFireBall(transform, playerTransform);
		ignore_interruptions = false;
		on_special_attack = false;
		finished_big_fireball_attack = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::Blast() {
	if (!checkHandles()) return ActionResult::ABORT;

	// On start.
	CClock & clock = _timers["blast"];
	if (!clock.isStarted()) {
		clock.start();
		ignore_interruptions = true;
		on_special_attack = true;
		// Play animation of shooting. At the end, shoot.
		finished_blast_animation = true;
	}

	if (interrupted()) {
		clock.stop();
		ignore_interruptions = false;
		on_special_attack = false;
		finished_blast_animation = false;
		return ActionResult::ABORT;
	}

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;
	TCompTransform * playerTransform = playerTransformH;

	if (finished_blast_animation && (timeBetweenShootsBlast < clock.elapsed())) {
		if (BasicAttack() == ActionResult::LEAVE) {
			currentShotsFired++;
			ignore_interruptions = true;
			clock.stop();
			clock.start();
		}
	}

	if (currentShotsFired >= numberOfShoots) {
		clock.stop();
		ignore_interruptions = false;
		on_special_attack = false;
		finished_blast_animation = false;
		return ActionResult::LEAVE;
	}
	return ActionResult::STAY;
}

ActionResult TCompBTDemon::SweepAttack() {
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;

	// If just started, we start the attack.
	CClock & clock = _timers["sweep"];
	if (!clock.isStarted()) {
		clock.start();
		ignore_interruptions = true;
		on_special_attack = true;
		// Play any animation.
	}

	if (interrupted()) {
		clock.stop();
		ignore_interruptions = false;
		on_special_attack = false;
		finished_blast_animation = false;
		return ActionResult::ABORT;
	}

	// Here we rotate to the beggining angle of our attack.
	if (chargeSweepAttack) {
		float yaw, pitch;
		transform->getAngles(&yaw, &pitch);
		float yawRotated = Time.delta * sweepRotation;
		if (abs(yawDeltaToRotateOnStart) < yawRotated)
			yawRotated = yawDeltaToRotateOnStart;
		yawDeltaToRotateOnStart -= yawRotated;
		contr->setRotation(yaw + yawRotated, pitch);
		// Once we have moved, start attack.
		if (yawDeltaToRotateOnStart <= 0.0f)
			chargeSweepAttack = false;
	}
	else {
		// Shoot fireball.
		if (timeBetweenShotsSweep < clock.elapsed()) {
			if (BasicAttack() == ActionResult::LEAVE) {
				currentShotsFired++;
				ignore_interruptions = true;
				clock.stop();
				clock.start();
			}
		}

		float yaw, pitch;
		transform->getAngles(&yaw, &pitch);
		float yawRotated = -Time.delta * sweepRotation;
		if (abs(yawDeltaToRotateOnEnd) < yawRotated)
			yawRotated = yawDeltaToRotateOnEnd;
		yawDeltaToRotateOnEnd += yawRotated;
		contr->setRotation(yaw + yawRotated, pitch);

		// Once finished, back to normal.
		if (yawDeltaToRotateOnEnd <= 0.0f) {
			clock.stop();
			ignore_interruptions = false;
			on_special_attack = false;
			chargeSweepAttack = true;
			yawDeltaToRotateOnStart = halfRang;
			yawDeltaToRotateOnEnd = halfRang * 2;
			return ActionResult::LEAVE;
		}
	}
	return ActionResult::STAY;
}

ActionResult TCompBTDemon::CastLazer() {
	if (!checkHandles()) return ActionResult::ABORT;

	// On start.
	if (!finished_cast_lazer) {
		// Play animation. Once it ends, it should set this var to true.
		finished_cast_lazer = true;
		ignore_interruptions = true;
		on_special_attack = true;
	}

	// On interrupt.
	if (interrupted()) {
		ignore_interruptions = false;
		finished_cast_lazer = false;
		on_special_attack = false;
		return ActionResult::ABORT;
	}

	// On end.
	if (finished_cast_lazer) {
		ignore_interruptions = false;
		finished_cast_lazer = false;
		on_special_attack = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::ShootLazer() {
	if (!checkHandles()) return ActionResult::ABORT;

	// On start.
	if (!finished_lazer_attack) {
		// Spawn the lazer.
		// Play animation of lazer attack. Once it ends, it should set this var to true.
		finished_lazer_attack = true;
		on_special_attack = true;
		ignore_interruptions = true;
	}

	// On interrupt.
	if (interrupted()) {
		ignore_interruptions = false;
		finished_lazer_attack = false;
		on_special_attack = false;
		return ActionResult::ABORT;
	}

	// On end.
	if (finished_lazer_attack) {
		// Erase the lazer. End the animation.
		ignore_interruptions = false;
		finished_lazer_attack = false;
		on_special_attack = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::FireFromGround() {
	if (!checkHandles()) return ActionResult::ABORT;

	// On start.
	CClock & clock = _timers["fireFromGround"];
	if (!clock.isStarted()) {
		clock.start();
		ignore_interruptions = true;
		on_special_attack = true;
		finished_fire_from_ground = true;
		// Play animation of fire from ground. Once it ends, it should set this var to true.
	}

	// On interrupt.
	if (interrupted()) {
		ignore_interruptions = false;
		finished_fire_from_ground = false;
		on_special_attack = false;
		currentFireTrapsSpawned = 0;
		clock.stop();
		return ActionResult::ABORT;
	}

	if (finished_fire_from_ground) {
		if (timeBetweenFireTraps < clock.elapsed()) {
			// Get player transform. Spawn a fire close to him.
			TCompTransform * transform = playerTransformH;
			// Spawn a fire trap close to him. Call the LM.
			clock.stop();
			clock.start();
			currentFireTrapsSpawned++;
		}
	}

	// On end.
	if (currentFireTrapsSpawned >= numOfFireTraps) {
		// Erase the lazer. End the animation.
		ignore_interruptions = false;
		finished_fire_from_ground = false;
		on_special_attack = false;
		currentFireTrapsSpawned = 0;
		clock.stop();
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::OrbitLeftAroundPlayer(){
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;
	TCompTransform * playerTransform = playerTransformH;
	
	// On start.
	CClock & clock = _timers["orbit"];
	CClock & clock_shots = _timers["orbit_shots"];
	if (!clock.isStarted()) {
		clock.start();
		clock_shots.start();
		ignore_interruptions = true;
		on_special_attack = true;
		// Play any animation.
	}

	// On interrupt.
	if (interrupted()) {
		clock.stop();
		clock_shots.stop();
		ignore_interruptions = false;
		on_special_attack = false;
		return ActionResult::ABORT;
	}

	// Follow player.
	Vector3 currentPosition = transform->getPosition();
	Vector3 playerPos = playerTransform->getPosition();

	// Orbits during the delta time.
	OrbitMovement(transform, contr, currentPosition, playerPos, -orbitSpeedPerSecond, Time.delta);

	if (isFacingTarget(transform, playerTransform, facingThreshold)) {
		if (timeBetweenShotsOrbit < clock_shots.elapsed()) {
			if (BasicAttack() == ActionResult::LEAVE) {
				currentShotsFired++;
				ignore_interruptions = true;
				clock_shots.stop();
				clock_shots.start();
			}
		}
	}

	// On end.
	if (orbitTime < clock.elapsed()) {
		clock.stop();
		clock_shots.stop();
		ignore_interruptions = false;
		on_special_attack = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}
ActionResult TCompBTDemon::OrbitRightAroundPlayer() {
	if (!checkHandles()) return ActionResult::ABORT;

	TCompTransform * transform = demonTransformH;
	TCompDemonController * contr = demonControllerH;
	TCompTransform * playerTransform = playerTransformH;

	// On start.
	CClock & clock = _timers["orbit"];
	CClock & clock_shots = _timers["orbit_shots"];
	if (!clock.isStarted()) {
		clock.start();
		clock_shots.start();
		ignore_interruptions = true;
		on_special_attack = true;
		// Play any animation.
	}

	// On interrupt.
	if (interrupted()) {
		clock.stop();
		clock_shots.stop();
		ignore_interruptions = false;
		on_special_attack = false;
		return ActionResult::ABORT;
	}

	// Follow player.
	Vector3 currentPosition = transform->getPosition();
	Vector3 playerPos = playerTransform->getPosition();

	// Orbits during the delta time.
	OrbitMovement(transform, contr, currentPosition, playerPos, orbitSpeedPerSecond, Time.delta);

	if (isFacingTarget(transform, playerTransform, facingThreshold)) {
		if (timeBetweenShotsOrbit < clock_shots.elapsed()) {
			if (BasicAttack() == ActionResult::LEAVE) {
				currentShotsFired++;
				ignore_interruptions = true;
				clock_shots.stop();
				clock_shots.start();
			}
		}
	}

	// On end.
	if (orbitTime < clock.elapsed()) {
		clock.stop();
		clock_shots.stop();
		ignore_interruptions = false;
		on_special_attack = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::CastMinionInvocation() {
	if (!checkHandles()) return ActionResult::ABORT;

	// On start.
	CClock & clock = _timers["cast_minions"];
	if (!clock.isStarted()) {
		clock.start();
		ignore_interruptions = true;
		// Play animation. Once it ends, it should set this var to true.
		finished_cast_minions = true;
	}

	// On interrupt.
	if (interrupted()) {
		ignore_interruptions = false;
		finished_cast_minions = false;
		currentMinionsSpawned = 0;
		clock.stop();
		return ActionResult::ABORT;
	}

	// Start spawning minions on animation end.
	if (finished_cast_minions) {
		if (timeBetweenMinions < clock.elapsed()) {
			// Cast minions from the arena the demon is at.
			clock.stop();
			clock.start();
			currentMinionsSpawned++;
		}
	}

	// On end.
	if (currentMinionsSpawned >= numOfFireTraps) {
		// Erase the lazer. End the animation.
		ignore_interruptions = false;
		finished_cast_minions = false;
		currentMinionsSpawned = 0;
		clock.stop();
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::BoostMinions() {
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_boost_minions) {
		// Play animation. Once it ends, it should set this var to true.
		finished_boost_minions = true;
		ignore_interruptions = true;
	}

	if (interrupted()) {
		ignore_interruptions = false;
		finished_boost_minions = false;
		return ActionResult::ABORT;
	}

	if (finished_boost_minions) {
		// Cast minions from the arena the demon is at.
		ignore_interruptions = false;
		finished_boost_minions = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::Laugh() {
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_laugh) {
		// Play animation. Once it ends, it should set this var to true.
		finished_laugh = true;
		ignore_interruptions = true;
	}

	if (interrupted()) {
		ignore_interruptions = false;
		finished_laugh = false;
		return ActionResult::ABORT;
	}

	if (finished_laugh) {
		ignore_interruptions = false;
		finished_laugh = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::DoIntroLaugh(){
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_happy_intro) {
		// Play animation. Once it ends, it should set this var to true.
		finished_happy_intro = true;
		ignore_interruptions = true;
	}

	if (interrupted()) {
		ignore_interruptions = false;
		finished_happy_intro = false;
		return ActionResult::ABORT;
	}

	if (finished_happy_intro) {
		ignore_interruptions = false;
		finished_happy_intro = false;
		setVariable("onStartFirstRound", false);
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

#include "components/common/comp_render.h"

ActionResult TCompBTDemon::DoAngryLaugh(){
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_sad_intro) {
		// Play animation. Once it ends, it should set this var to true.
		finished_sad_intro = true;
		ignore_interruptions = true;
	}

	if (interrupted()) {
		ignore_interruptions = false;
		finished_sad_intro = false;
		return ActionResult::ABORT;
	}

	if (finished_sad_intro) {
		ignore_interruptions = false;
		finished_sad_intro = false;
		setVariable("onStartSecondRound", false);
		// for now set the demon red to show it's on the second round.
		TCompRender * rend = getComponent<TCompRender>();
		rend->setColor(Vector4(1, 0, 0, 1));

		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::DoHappySound(){
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_happy_sound) {
		// Play animation. Once it ends, it should set this var to true.
		finished_happy_sound = true;
		ignore_interruptions = true;
	}

	if (interrupted()) {
		ignore_interruptions = false;
		finished_happy_sound = false;
		return ActionResult::ABORT;
	}

	if (finished_happy_sound) {
		ignore_interruptions = false;
		finished_happy_sound = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::DoSadSound(){
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_sad_sound) {
		// Play animation. Once it ends, it should set this var to true.
		finished_sad_sound = true;
		ignore_interruptions = true;
	}

	if (interrupted()) {
		ignore_interruptions = false;
		finished_sad_sound = false;
		return ActionResult::ABORT;
	}

	if (finished_sad_sound) {
		ignore_interruptions = false;
		finished_sad_sound = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::LowImpact(){
	if (!checkHandles())  return ActionResult::ABORT;

	if (!finished_low_hit) {
		// Play animation. Once it ends, it should set this var to true.
		finished_low_hit = true;
		ignore_interruptions = true;
	}

	if (interrupted()) {
		ignore_interruptions = false;
		finished_low_hit = false;
		return ActionResult::ABORT;
	}

	if (finished_low_hit) {
		ignore_interruptions = false;
		finished_low_hit = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::CriticalImpact(){
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_critical_hit) {
		// Play animation. Once it ends, it should set this var to true.
		finished_critical_hit = true;
		ignore_interruptions = true;
	}

	if (interrupted()) {
		ignore_interruptions = false;
		finished_critical_hit = false;
		return ActionResult::ABORT;
	}

	if (finished_critical_hit) {
		ignore_interruptions = false;
		finished_critical_hit = false;
		return ActionResult::LEAVE;
	}

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::Dying(){
	if (!checkHandles()) return ActionResult::ABORT;

	if (!finished_dying) {
		// Play animation. Once it ends, it should set this var to true.
		finished_dying = true;
		dead = true;
	}

	if(finished_dying)
		return ActionResult::LEAVE;

	return ActionResult::STAY;
}

ActionResult TCompBTDemon::Dead(){
	if (!checkHandles()) return ActionResult::ABORT;

	CClock & clock = _timers["dead"];
	if (!clock.isStarted())
		clock.start();

	if (timeBeforeDisappearing < clock.elapsed()) {
		clock.stop();
		CHandle(this).getOwner().destroy();
	}

	return ActionResult::STAY;
}

/* CONDITIONS */

// Checks if the bot is far away from the room. Used for gatekeeping.
bool TCompBTDemon::CheckIfOutsideRoom() {
	TCompTransform * transform = demonTransformH;
	bool farFromRoom = Vector3::Distance(transform->getPosition(), roomCenter) >= distanceToMoveFromRoomCenter;

	if (!farFromRoom)
		return false;
	// If we view the player and he is in the room, we simply go for him instead of returning to the room.
	if (farFromRoom && ViewPlayer())
		return false;
	else if (farFromRoom && !ViewPlayer())
		return true;
}

// Checks if the bot views the player and the player is inside the room.
bool TCompBTDemon::ViewPlayer() {
	// Ignore if the player is dead.
	if (!isTargetAlive()) return false;

	TCompTransform * transform = demonTransformH;
	TCompTransform * targetTransform = playerTransformH;
	Vector3 targetPos = targetTransform->getPosition();

	float angleToPlayer = transform->getDeltaYawToAimTo(targetPos);
	if (abs(angleToPlayer) <= visionCone.angle) {
		float distance = VEC3::Distance(transform->getPosition(), targetPos);
		if (distance <= visionCone.depth && CheckOnRoom(targetPos))
			return true;
	}
	return false;
}

// Checks the distance to the player and chases it if necessary.
bool TCompBTDemon::CheckIfChaseTarget() {
	TCompTransform * transform = demonTransformH;
	TCompTransform * targetTransform = playerTransformH;
	Vector3 currentPosition = transform->getPosition();
	Vector3 targetPos = targetTransform->getPosition();
	float distanceToTarget = Vector3::Distance(currentPosition, targetPos);
	if (distanceToTarget >= maxAttackDistance)
		return true;
	return false;
}

// Checks if the player is too close. Escapes if so.
bool TCompBTDemon::CheckIfPlayerTooClose() {
		TCompTransform * transform = demonTransformH;
		TCompTransform * targetTransform = playerTransformH;
		Vector3 currentPosition = transform->getPosition();
		Vector3 targetPos = targetTransform->getPosition();
		float distanceToTarget = Vector3::Distance(currentPosition, targetPos);
		if (distanceToTarget <= securityDistance)
			return true;
		return false;
}

/* ---------------- Helper functions ---------------- */

/* Checks handles to see they are still valid.*/
bool TCompBTDemon::checkHandles() {
	// Check target is valid.
	if (!playerHandle.isValid())
		playerHandle = getEntityByName(playerName);
	
	if (!playerHandle.isValid()) return false;
	CEntity * playerEnt = playerHandle;
	playerTransformH = playerEnt->getComponent<TCompTransform>();
	playerHealthH = playerEnt->getComponent<TCompHealth>();

	// Check handles demon.
	if (!demonTransformH.isValid()) {
		demonTransformH = getComponent<TCompTransform>();
		demonControllerH = getComponent<TCompDemonController>();
		demonHealthH = getComponent<TCompHealth>();
	}
	if (!demonTransformH.isValid()) return false;

	return true;
}

bool TCompBTDemon::CheckOnRoom(const Vector3 & currentPosition) {
	return Vector3::Distance(currentPosition, roomCenter) <= distanceToMoveFromRoomCenter;
}

/* Wander helper functions. */

void TCompBTDemon::SetRandomWanderDirection(float currentYaw, float maxYawChange) {
	objectiveYawToWanderTo = (RNG.f(maxYawChange) - maxYawChange / 2.0f) + currentYaw;
}

/* Moves in a random direction */
void TCompBTDemon::MoveInRandomDirection(const TCompTransform * transform, TCompDemonController * contr,
	float currentYaw, float currentPitch,	float movementSpeed, float rotationSpeed, float dt) {
	
	float newYaw = Maths::lerpAngle(currentYaw, objectiveYawToWanderTo, rotationSpeed * dt);
	contr->setRotation(newYaw, currentPitch);
	contr->move(transform->getFront() * movementSpeed * dt);
}

/* Idle - Wander - Chase helper functions */

// Checks if the bot views the player. A second method similar to the condition but we an added distance.
bool TCompBTDemon::ViewPlayerDistance(TCompTransform * transform, const Vector3 & targetPos, float nDistance) {
	// Ignore if the player is dead.
	if (!isTargetAlive()) return false;

	float angleToPlayer = transform->getDeltaYawToAimTo(targetPos);
	if (abs(angleToPlayer) <= visionCone.angle) {
		float distance = VEC3::Distance(transform->getPosition(), targetPos);
		if (distance <= nDistance)
			return true;
	}
	return false;
}

// Distance to a target.
bool TCompBTDemon::CheckDistanceToTargetSmaller(const Vector3 & currentPosition, const Vector3 & targetPos, float distanceToGetToTarget) {
	return Vector3::Distance(currentPosition, targetPos) <= distanceToGetToTarget;
}

/* Idle war helper functions. */

void TCompBTDemon::GetRandomDirectionOnWar(TCompTransform * transform) {
	float currentYaw, currentPitch;
	transform->getAngles(&currentYaw, &currentPitch);
	objectiveYawToWanderTo = (RNG.f(maxYawChangeOnAttack) - maxYawChangeOnAttack / 2.0f) + currentYaw;
	transform->setAngles(objectiveYawToWanderTo, currentPitch);
	objectiveDirectionToMoveTo = transform->getFront();
	transform->setAngles(currentYaw, currentPitch);
}

void TCompBTDemon::MoveInRandomDirectionFacingCurrentOrientation(TCompDemonController * contr, float movementSpeed, float dt) {
	contr->move(objectiveDirectionToMoveTo * movementSpeed * dt);
}

/* Orbit helper functions. */
void TCompBTDemon::OrbitMovement(TCompTransform * transform, TCompDemonController * contr,
	const Vector3 & currentPosition, const Vector3 & playerPos, float orbitSpeedSecond, float dt) {
	// Orbit left.
	float distance = Vector3::Distance(currentPosition, playerPos);


	Vector3 toPlayer = playerPos - currentPosition;
	Vector3 up = transform->getUp();
	Vector3 dir = toPlayer.Cross(up);
	dir.Normalize();
	

	Vector3 moveVec = dir * orbitSpeedSecond * dt;
	// Set it on the controller so it affects the collider.
	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	contr->setRotation(yaw + transform->getDeltaYawToAimTo(playerPos), pitch);
	contr->move(moveVec);
}

/* Extra functions. */

// Checks if the player is alive.
bool TCompBTDemon::isTargetAlive() {
	TCompHealth * health = playerHealthH;
	return !health->isDead();
}

bool TCompBTDemon::isFacingTarget(const TCompTransform * demonTransform, const TCompTransform * targetTransform, float threshold) {
	float yawObj = demonTransform->getDeltaYawToAimTo(targetTransform->getPosition());
	if (abs(yawObj) <= threshold)
		return true;
	return false;
}

// Shoots a fireball. Target shoot point is the point where the bullets will point to. Normally, the center of the player.
void TCompBTDemon::ShotFireBall(const TCompTransform * transform) {
	CTransform tempTransform;
	Vector3 shootingPosition = transform->getPosition() + transform->getFront() * 2 + transform->getUp() * 1;
	tempTransform.setPosition(shootingPosition);

	TEntityParseContext ctx;
	ctx.root_transform = tempTransform;
	parseScene("data/prefabs/demon-ranged-orb.json", ctx);
	TMsgAssignBulletOwner msg;
	// Commented due to this msg changed and this code is not used
	//msg.h_owner = CHandle(this).getOwner();
	//msg.source = shootingPosition;
	//msg.destination = transform->getFront();
	//ctx.entities_loaded[0].sendMsg(msg);
}

// Shoots a fireball. Target shoot point is the point where the bullets will point to. Normally, the center of the player.
void TCompBTDemon::ShotBigFireBall(const TCompTransform * transform, const TCompTransform * playerTransform) {
	CTransform tempTransform;
	Vector3 shootingPosition = transform->getPosition() + transform->getFront() * 2 + transform->getUp() * 2;
	tempTransform.setPosition(shootingPosition);

	TEntityParseContext ctx;
	ctx.root_transform = tempTransform;
	parseScene("data/prefabs/demon-ranged-big-orb.json", ctx);
	TMsgAssignBulletOwner msg;
	// Commented due to this msg changed and this code is not used
	//msg.h_owner = CHandle(this).getOwner();
	//msg.source = shootingPosition;
	//msg.destination = transform->getFront();
	//ctx.entities_loaded[0].sendMsg(msg);
}

// Shoots a fireball. Target shoot point is the point where the bullets will point to. Normally, the center of the player.
void TCompBTDemon::ShotAimedFireBall(const TCompTransform * transform, const TCompTransform * playerTransform) {
	CTransform tempTransform;
	Vector3 shootingPosition = transform->getPosition() + transform->getFront() * 2 + transform->getUp() * 1;
	tempTransform.setPosition(shootingPosition);

	TEntityParseContext ctx;
	ctx.root_transform = tempTransform;
	parseScene("data/prefabs/demon-ranged-orb.json", ctx);
	TMsgAssignBulletOwner msg;
	msg.h_owner = CHandle(this).getOwner();
	msg.source = shootingPosition;
	Vector3 direction = getTargetAimingPos(playerTransform) - shootingPosition;
	direction.Normalize();
	// Commented due to this msg changed and this code is not used
	//msg.destination = direction;
	ctx.entities_loaded[0].sendMsg(msg);
}

// Returns the aiming pos where bullets should go to. Adds the target offset to the player transform.
Vector3 TCompBTDemon::getTargetAimingPos(const TCompTransform * playerTransform) {
	return playerTransform->getPosition() + playerTransform->getUp() * playerOffsetShootAt;
}

// Rotates the demon to face the target.
void TCompBTDemon::RotateToTarget(TCompTransform * transform, TCompDemonController * contr, const Vector3 & targetPos, float rotationSpeed, float dt) {
	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	float deltaYaw = transform->getDeltaYawToAimTo(targetPos);
	float finalYaw = Maths::lerp<float>(yaw, yaw + deltaYaw, dt * rotationSpeed);
	contr->setRotation(finalYaw, pitch);
}