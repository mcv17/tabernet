#pragma once
#include "mcv_platform.h"
#include "comp_behaviour_tree.h"
#include "components/common/comp_transform.h"
#include "components\ai\pathfinding\comp_pathfinding_agent.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "utils/time.h"
#include "particles/particle_system.h"

class TCompBTEnemyMelee : public TCompBehaviourTree {
public:
	void debugInMenu();
	void init() override;
	void load(const json & j, TEntityParseContext & ctx);
	void renderDebug();
	static void registerMsgs();
	static void declareInLua();
	void onAlertAI(const TMsgAlertAI& msg);

	void alert();
	DECL_SIBLING_ACCESS();

	bool isQuadruped() { return _isQuadruped; }

private:
	void implementFunctions() override;

	ActionResult idleState();
	ActionResult applyBuffState( );
	ActionResult chasePlayerState( );
	ActionResult leapState();
	ActionResult goToSlotState( );
	//ActionResult runArroundState( );
	// Combat
	ActionResult idleWarState( );
	ActionResult attackState( );
	// Receive damage
	ActionResult flinchState();
	ActionResult dyingState( );
	ActionResult deathState( );
	ActionResult facePlayer( );
	ActionResult prepareForBattle();
	// Decorators
	//ActionResult prepareForBattle();

	// Utils
	bool isPlayerAtSight();
	bool isPlayerInLeapRange(); //check distance and if anything intersects
	bool isInAttackRange();
	/*
	bool askForAvailableSlot();
	*/
	//bool isPlayerAtAttackRange();
	bool isAtPosition(const TCompTransform& transform, VEC3 pos, float threshold) const;
	ActionResult checkTimer(CClock* timer);
	void attackPlayer();
	void freeSlot();
	std::string getPlayerSlot();
	float getBuffedAttack();
	float getBuffedAttackSpeed();
	float getBuffedHP();
	void  setForceChasePlayer(bool state);

	/* VARS */
	CHandle _skeletonHandle;
	CHandle _animatorHandle;
	CHandle _controllerHandle;
	CHandle _agentHandle;
	bool _isOnTrack = true;
	CTimedEvent _pathfindingEvent = CTimedEvent(0.2f);
	bool _colliderActive = true;

	particles::CSystem * vanishParticles = nullptr;

	float _attackDistance = 1.5f;
	float _attackProbability = 0.8f;
	float _idleWarEventProbability = 0.3f;
	float _attackSpeed = 0.5f;
	float _chaseAgainDistance = 2.0f;
	float _stepDistance = .5f;
	float _leapProbability = 0.2f;
	float _leapMaxDistance = 6.0f;
	float _leapMinDistance = 3.0f;
	bool  _leapDone = false;
	bool _isQuadruped = false;
	float _attackDamage = 10.0f;
	bool _isBuffed = false;
	bool _isFlinching = false;
	bool _forceChasePlayer = false;
	bool _playerSeen = false;
	bool _isAttacking = false;
	bool _isKnocking = false;
	bool _isIdleWarEventPlaying = false;
	//temp
	bool _msgrceivd = false;

	// Player slot if available. -1 means that has no assigned slot
	int _slotIdx = -1;

	// % factors
	float _difficulty = .25f;	// % for a combo attack
	float _tauntFactor = 0.3f;  // % to throw a taunt

	std::string _attackEvent;
	std::string _idleEvent;
	std::string _idleWarEvent;
	std::string _shoutEvent;

	struct TVisionCone {
		float angle = (float)M_PI_4;
		float depth = 20.0f;
	} _visionCone;

	// offset for checking distances
	float _offset = 0.1f;

	// Buffs variables
	VEC4 _buffColour{ 0.8f, 0.8f, 0.0f, 1.0f };
	VEC4 _baseColour;
	float _buffProbability = 0.2f;
	float _buffAttackMultiplier = 0.2f;
	float _buffAttackSpeedMultiplier = 0.2f;
	float _buffHPMultiplier = 0.2f;
	float _externalDamageBuff = 1.0f;
	int	  _externalBuffStateId = 4;


	float _idleTime = 2.0f;
	float _idleWarTime = 1.0;
	// Temp
	float _betweenAttacksTime = 2.0f;
	float _dyingTime = 2.5f;
	float _leapTime = 2.0f;
	float _preVanishTime = 1.0f;

	// Timers
	CClock idleTimer = CClock(_idleTime);
	CClock idleWarTimer = CClock(_idleWarTime);
	CClock leapTimer = CClock(_leapTime);
	// Temp
	CClock _betweenAttacksTimer = CClock(_betweenAttacksTime);
	CClock preVanishTimer = CClock(_preVanishTime);
	CClock dyingTimer = CClock(_dyingTime);

	// Animation var names
	std::string _attackingVarName = "attacking";

	/* MSGS */
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onLogicStatus(const TMsgLogicStatus & msg);
	void onBuffRecieved(const TMsgApplyBuff & msg);
	void onDebuffRecieved(const TMsgRemoveBuff & msg);
	void onEntityDead(const TMsgEntityDead& msg);
	void deleteIAController(const TMsgDeleteIAController & msg);
	void onHealthLoweredHit(const TMsgHitDamage & msg);
	void onBulletDamage(const TMsgBulletDamage & msg);
	void onComponentReset(const TMsgResetComponent & msg);
	//void onAlertAI(const TMsgAlertAI& msg);
};