#pragma once
#include "comp_behaviour_tree.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_transform.h"
#include "utils/time.h"
#include "components/common/comp_health.h"
#include "components\ai\pathfinding\comp_pathfinding_agent.h"
#include "components/game/status_effects/comp_status_effects.h"
#include "particles/particle_system.h"

class TCompBTEnemyRanged : public TCompBehaviourTree {

public:
	~TCompBTEnemyRanged();

	static void registerMsgs();
	static void declareInLua();

	DECL_SIBLING_ACCESS();

	void onEntityCreated(const TMsgEntityCreated& msg);
	void onLogicStatus(const TMsgLogicStatus & msg);
	void onBulletDamage(const TMsgBulletDamage& msg);
	void onOtherDamage(const TMsgHitDamage & msg);
	void onNailDamage(const TMsgNailDamage& msg);
	void onBuffRecieved(const TMsgApplyBuff & msg);
	void onBuffRemoved(const TMsgRemoveBuff & msg);
	void onEntityDead(const TMsgEntityDead& msg);
	void deleteIAController(const TMsgDeleteIAController & msg);
	void onComponentReset(const TMsgResetComponent & msg);
	void onAlertAI(const TMsgAlertAI& msg);
	void onToggleComponent(const TMsgToogleComponent& msg);

	void alert();

	void debugInMenu();
	void load(const json & j, TEntityParseContext & ctx);
	void renderDebug();

private:
	enum class EShootType {
		FIRE, ICE
	};

	struct TCone {
		float angle;
		float depth;

		bool isInRange(const CTransform& coneTransf, const Vector3& targetPos) const {
			float angleToPlayer = coneTransf.getDeltaYawToAimTo(targetPos);
			if (abs(angleToPlayer) <= angle) {
				float distance = VEC3::Distance(coneTransf.getPosition(), targetPos);
				if (distance <= depth)
					return true;
			}

			return false;
		}
	};

	TCone _visionCone{ (float)M_PI_4, 10.0f };
	TCone _attackCone{ (float)M_PI_4, 3.0f };

	bool _colliderActive = true;

	// Buffs variables
	float _damageBuff = 1.0f;

	float _lastDamageReceived = 0.0f;

	float _shootDistance = 5.0f;
	bool _playerAtSight = false;
	EShootType _shootType = EShootType::FIRE;
	Vector3 _raycastPoint = Vector3::Forward * 1.5f + Vector3::Up * 1.7f;

	std::vector<VEC3> _waypoints;
	int _curWaypoint;

	unordered_map<string, CClock> _timers;
	CTimedEvent _pathfindingEvent = CTimedEvent(0.2f);
	CTimedEvent _raycastEvent = CTimedEvent(0.5f);
	CClock _burstTimer = CClock(0.4f);
	float _idleWarTime = 0.5f;
	float _flinchCooldown = 5.0f;
	float _flinchInternalChanceMultiplier = 1.0f;
	float _ragdollsTime = 4.0f, _dyingTime = 2.0f;
	float _minTimeToCastShoot = 0.5f, _maxTimeToCastShoot = 2.0f;
	float _minTimeToCastCloseRange = 0.25f, _maxTimeToCastCloseRange = 1.0f;
	float _timeBetweenShootings = 1.0f, _timeBetweenCloseRangeAttacks = 1.0f;

	CHandle _agent, _transform, _controller, _animator, _skeleton;
	CHandle _projectileShooter;
	std::vector<std::string> _projectiles;

	float _fireBreathDPS = 2.0f;
	float _fireBreathDamage = 2.0f;

	bool _particlesCreated = false;
	particles::CSystem* _vanishParticles = nullptr;
	particles::CSystem* _prep1Particles = nullptr;
	particles::CSystem* _prep2Particles = nullptr;
	std::string _fireCastParticleSystemName = "data/particles/fire_shooting_prepare1.particles";
	std::string _iceCast1ParticleSystemName = "data/particles/ice_shooting_prepare.particles";
	std::string _iceCast2ParticleSystemName = "data/particles/snow_flakes.particles";
	std::string _circleParticleSystemName = "data/particles/black_magic_circle.particles";
	int _iceCast1ParticleSystemPriority = 1;
	int _iceCast2ParticleSystemPriority = 0;
	// Vector3::Up * 1.5f + Vector3::Backward; // To shoot from the front
	std::vector<Vector3> _shootingOffsets;

	std::string _shoutSurpriseSoundEvent = "";
	std::string _shoutAggresiveSoundEvent = "";
	std::string _castIceSoundEvent = "";
	float _shoutAggresiveIdleWarChance = 0.1f;
	CClock _shoutAggresiveIdleWarCooldown = CClock(4.0f, true);

	DECL_TCOMP_ACCESS("Player", TCompTransform, PlayerTransform);
	DECL_TCOMP_ACCESS("Player", TCompStatusEffects, PlayerStatusEffects);
	DECL_TCOMP_ACCESS("Player", TCompHealth, PlayerHealth);

	void init() override;
	void implementFunctions() override;
	void updateVariables() override;

	bool raycastToPlayer();

	/* CONDITIONS */
	bool viewPlayer();
	bool canShootPlayer();
	bool shouldDebuffPlayer();

	/* ACTIONS */
	ActionResult fallToTheGround();
	ActionResult dying();
	ActionResult death();
	ActionResult flinch();
	ActionResult idleWar();
	ActionResult chase();
	ActionResult seekWpt();
	ActionResult nextWpt();
	ActionResult idle();
	ActionResult turnToPlayer();
	ActionResult forget();
	ActionResult shoot();
	ActionResult shootSniper();
	ActionResult shootIce();
	ActionResult shootFire();
	ActionResult fireBreath();
	ActionResult prepareFireAttack();
	ActionResult prepareIceAttack();
	ActionResult prepareSniperAttack();
	ActionResult waitCastingShootingTime();
	ActionResult waitCastingCloseRangeTime();
	ActionResult waitBetweenCloseRange();
	ActionResult waitBetweenShootings();
	ActionResult removeCastParticleEffects();

	ActionResult prepareForBattle();
	ActionResult startAttackAnimation();

	ActionResult wait(float time);
	ActionResult wait(CClock& timer);
	bool isArena();
	void iterateWaypoint();
	void spawnShooter(const Vector3& pos);
};