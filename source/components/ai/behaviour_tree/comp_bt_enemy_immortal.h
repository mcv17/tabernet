#pragma once
#include "mcv_platform.h"
#include "comp_behaviour_tree.h"

#include "components/common/comp_transform.h"
#include "components/ai/pathfinding/comp_pathfinding_agent.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_health.h"

#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "utils/time.h"

class TCompBTEnemyImmortal : public TCompBehaviourTree {
public:
	void debugInMenu();
	void init() override;
	void load(const json & j, TEntityParseContext & ctx);
	static void registerMsgs();
	static void declareInLua();
	DECL_SIBLING_ACCESS();

	void alert();

private:
	void implementFunctions() override;

	/* BT */
	// Movement
	ActionResult chasePlayer();
	ActionResult goToSlot();
	ActionResult facePlayer();
	ActionResult seekWaypoint();
	ActionResult nextWaypoint();
	ActionResult rest();
	ActionResult idle();

	// Combat
	ActionResult idleWar();
	ActionResult attack();

	// Receive damage
	ActionResult flinch();
	ActionResult dying();
	ActionResult death();
	
	// Decorators
	ActionResult prepareForBattle();

	// Utils
	bool isPlayerInSight();
	bool isBossDead();
	bool askForAvailableSlot();

	ActionResult checkTimer(CClock* timer);
	void attackPlayer();
	void freeSlot();
	std::string getPlayerSlot();

	/* VARS */
	// Handles
	CHandle _skeletonHandle;
	CHandle _animatorHandle;
	CHandle _agentHandle;
	CHandle _transHandle;
	CHandle _charControllerHandle;
	int _buffStateId = 4;
	CTimedEvent _pathfindingEvent = CTimedEvent(0.2f);
	bool _colliderActive = true;

	float _forgetDistance = 20.0f;
	bool _wasChasingPlayer = false;
	float _attackDistance = 1.5f;
	bool _wasAttackingPlayer = false;
	bool _isFlinching = false;
	bool _justAttacked = false;

	float _damage = 10.0f;

	// Player slot if available. -1 means that has no assigned slot
	int _slotIdx = -1;

	struct TVisionCone {
		float angle = (float)M_PI_2;
		float depth = 20.0f;
	} _visionCone;

	// offset for checking distances
	float _offset = 0.1f;
	// Threshold for when was chasing the player and returns to a waypoint
	float _returningToWaypointThreshold = 1.0f;
	// Threshold when in IdleWar and player moves to chase again
	float _combatChaseAgainThreshold = 3.5f;

	// Buffs variables
	VEC4 _buffColour{ 0.8f, 0.8f, 0.0f, 1.0f };
	VEC4 _baseColour;

	float _damageBuff = 1.0f;

	float _idleTime = 2.0f;
	float _minIdleWarTime = 1.5f;
	float _maxIdleWarTime = 3.0f;
	float _minRestTime = 0.5f;
	float _maxRestTime = 3.0f;
	// Temp
	float _dyingTime = 2.5f;

	// Timers
	CClock _idleTimer = CClock(_idleTime);
	CClock _idleWarTimer = CClock(_minIdleWarTime, _maxIdleWarTime);
	CClock _restTimer = CClock(_minRestTime, _maxRestTime);
	// Temp
	CClock _dyingTimer = CClock(_dyingTime);

	std::vector<VEC3> _waypoints;
	int _curWaypoint;

	// Animation var names
	std::string _attackingVarName = "attacking";
	std::string _knockbackVarName = "knockback";

	// Sounds
	std::string _idleSoundEvent = "";
	float _idleSoundEventChance = 0.025f;
	CClock _idleSoundEventCooldown = CClock(4.0f, true);

	/* MSGS */
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onLogicStatus(const TMsgLogicStatus & msg);
	void onBuffRecieved(const TMsgApplyBuff & msg);
	void onDebuffRecieved(const TMsgRemoveBuff & msg);
	void onEntityDead(const TMsgEntityDead& msg);
	void deleteIAController(const TMsgDeleteIAController & msg);
	void onHitDamage(const TMsgHitDamage & msg);
	void onBulletDamage(const TMsgBulletDamage & msg);
	void onNailDamage(const TMsgNailDamage& msg);
	void onComponentReset(const TMsgResetComponent & msg);
	void onAlertAI(const TMsgAlertAI& msg);

	DECL_TCOMP_ACCESS("Player", TCompTransform, PlayerTransform);
	DECL_TCOMP_ACCESS("Player", TCompHealth, PlayerHealth);
};