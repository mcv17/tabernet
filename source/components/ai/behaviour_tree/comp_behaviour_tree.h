#pragma once

#include "components\common\comp_base.h"
#include "behaviour_tree_node.h"
#include <stack>

/*	Macros to provide implementation to conditions/actions: it needs the name of the function in yEd, 
*		the class of the method that is going to implement it, and the method
*		We use a lambda expression because it needs a reference to the this pointer and we pass it as a 
*		parameter.
*/
#define IMPLEMENT_CONDITION(name, cclass, cmethod) setConditionImplementation(name, [](TCompBehaviourTree * ref) { return static_cast<cclass *>(ref)->cmethod(); })
#define IMPLEMENT_ACTION(name, cclass, cmethod) setActionImplementation(name, [](TCompBehaviourTree * ref) { return static_cast<cclass *>(ref)->cmethod(); })

using namespace std;

typedef nlohmann::json DynamicVariable;
typedef BehaviourTreeNode::ActionResult ActionResult;

typedef std::function<ActionResult(TCompBehaviourTree *)> btaction;
typedef std::function<bool (TCompBehaviourTree *)> btcondition;

// simple struct that only has a function stored as a variable
struct TBTCondition {

	TBTCondition() {}
	TBTCondition(btcondition f) : test(f) {}

	btcondition test;

};

class Transform;

// struct for simple conditions (i.e. a > b, life <= 20, isAlive)
// it uses json as operands because you can use any c++ condition operator to compare their values
struct TBTSimpleCondition : public TBTCondition {

	TBTSimpleCondition(DynamicVariable & l, std::string op, DynamicVariable & r) : leftOperand(l), rightOperand(r) {
		if (op == "==")
			test = [this](TCompBehaviourTree *) { return leftOperand == rightOperand; };
		else if (op == "!=")
			test = [this](TCompBehaviourTree *) { return leftOperand != rightOperand; };
		else if (op == "<=")
			test = [this](TCompBehaviourTree *) { return leftOperand <= rightOperand; };
		else if (op == ">=")
			test = [this](TCompBehaviourTree *) { return leftOperand >= rightOperand; };
		else if (op == "<")
			test = [this](TCompBehaviourTree *) { return leftOperand < rightOperand; };
		else if (op == ">")
			test = [this](TCompBehaviourTree *) { return leftOperand > rightOperand; };
	}

private:
	// operands as json references of the _jReference attribute in BT
	const DynamicVariable & leftOperand;
	const DynamicVariable & rightOperand;

};

struct BTGlobalData {

	~BTGlobalData() {
		for (auto& cond : conditions) {
			delete cond.second;
		}
	}

	// the nodes (to search a node by name from any other node)
	unordered_map<string, BehaviourTreeNode> tree;
	// the C++ functions that implement node actions, hence, the behaviours
	unordered_map<string, btaction> actions;
	// the C++ functions that implement conditions
	unordered_map<string, TBTCondition *> conditions;

	map<string, DynamicVariable> variables;

	// we keep json reference as it will serve as a holder for values in comparisons
	json jReference;

	// conditions that lack a C++ implementation
	// map key is the name of the function, value is correspondence in _conditions map
	map<string, vector<string>> conditionsToImplement;
	// actions that lack a C++ implementation 
	// map key is the name of the function, values are correspondences in _actions map
	map<string, vector<string>> actionsToImplement;
	// some conditions need variables (which are different for every BT), so we use this to provide
	// the variable pointer in a local space
	map<string, DynamicVariable *> conditionsToFill;

	BehaviourTreeNode * root;
};

// Implementation of the behavior tree
// uses the BehaviourTreeNode so both work as a system
// tree implemented as a map of btnodes for easy traversal
// behaviours are a map to member function pointers, to 
// be defined on a derived class. 
// BT is thus designed as a pure abstract class, so no 
// instances or modifications to bt / btnode are needed...
class TCompBehaviourTree : public TCompBase
{
public:

	virtual void init() { }

	virtual void load(const json& j, TEntityParseContext& ctx);

	virtual void debugInMenu();
	void update(float dt);

	virtual void alert() {}

	void setVariable(string varName, const DynamicVariable & value);
	DynamicVariable getVariable(string varName);
	bool variableExists(const string& name);

	std::string getNodeName() const { return _current == nullptr ? "root" : _current->getName(); }

	void interrupt(string flag);

	friend class BehaviourTreeNode;

protected:
	// functions to be executed by son classes to provide implementation for both conditions and actions
	void setConditionImplementation(string, btcondition);
	void setActionImplementation(string, btaction);

	// get a node by name
	BehaviourTreeNode * findNode(string nodeName);

	// this function will provide implementation for all actions and conditions
	virtual void implementFunctions() = 0;

	bool interrupted();
	bool testCondition(string condName);

	void setCurrent(BehaviourTreeNode * current);

	void reset();

	void loadDropsData(const json & j);
	void spawnDrops(const CTransform & transform);

private:
	static map<string, BTGlobalData> _globalData;

	string _name;
	string _filename;

	unordered_map<string, BehaviourTreeNode> * _tree;
	unordered_map<string, btaction> * _actions;
	unordered_map<string, TBTCondition *> _conditions;

	map<string, DynamicVariable> _variables;

	BehaviourTreeNode * _root;
	BehaviourTreeNode * _current = nullptr;

	// interruptions queue to be processed sequentially
	list<string> _interruptions;
	// context for the execution before the interruption (the node that was going to be executed)
	stack<BehaviourTreeNode*> _contextStack;
	// is currently executing an interruption?
	bool _servingInterruption = false;

	// Drop variables.
	std::string dropType;
	float dropProb; // From 0.0f to 1.0f, how probable a drop will fall.
	Vector3 dropPositionOffset;
	Vector3 dropMinPositionNoise;
	Vector3 dropMaxPositionNoise;
	Vector3 dropImpulse;
	Vector3 dropMinImpulseNoise;
	Vector3 dropMaxImpulseNoise;

	// moved to private as really the derived classes do not need to see this
	BehaviourTreeNode * createNode(string name);
	BehaviourTreeNode * createNode(string name, string type, json & params);

	// internals used by btnode and other bt calls
	void addAction(string actionName, btaction action);
	void addAction(string actionName, json & action);
	bool hasAction(string actionName);
	int execAction(string actionName);
	void addCondition(string condName, btcondition cond);
	void addCondition(string condName, json & cond);

	void setNodeRatios(const string & name, vector<float> & ratios);

	// read the structure of BT from a provided json
	void parseJson(const json& j);

	// call this once per frame to compute the AI. No need to derive this one, 
	// as the behaviours are derived via btactions and the tree is declared on create
	void recalc();

	void resetInterruptions();
	virtual void updateVariables() { }

	bool handleInterruptions();

};