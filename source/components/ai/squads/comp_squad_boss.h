#pragma once
#include "entity/entity.h"
#include "entity/common_msgs.h"

struct EntitySlot {
	EntitySlot() {
		position = VEC3(0, -100, 0);
		filled = false;
		idx = -1;
		minion = CHandle();
	}
	VEC3 position;
	bool filled;
	int idx;
	CHandle minion;
};

struct Team {
	std::vector<CHandle> members;
	std::string mission;
};

class TCompSquadBoss : public TCompBase {
public:


	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();
	void renderDebug();

	static void registerMsgs();

	EntitySlot getAvailableSlot(VEC3 pos);
	void updateStatusInfo(std::string key, bool value);
	void updateSlotInfo(int slot, bool filled, CHandle minion = CHandle());
	EntitySlot getSlot(int idx);

protected:
	// Vars
	std::vector<Team> _teams;
	int _numSlots = 4;
	int _teamSize = 3;
	float _slotDistance = 2.0f;
	int _currentMinions = 0;
	std::vector<EntitySlot> _playerSlots;
	std::vector<EntitySlot> _ownSlots;
	std::map<std::string, bool> _statusInfo;
	
	// Functions
	void updatePlayerSlots();
	void updateTeams();
	std::string getMinionName(CHandle h);

	DECL_SIBLING_ACCESS();
};