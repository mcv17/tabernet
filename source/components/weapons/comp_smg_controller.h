#pragma once
#include "components/weapons/comp_weapons_controller.h"
#include "entity/entity.h"
#include "components/game/status_effects/common_status_effects.h"
#include "components/common/comp_particles.h"
#include "components/render/comp_line_render.h"
#include "comp_weapons_manager.h"

struct TMsgEntityCreated;
struct TMsgEntityTriggerEnter;
struct TMsgEntityTriggerExit;

class TCompSmgController : public TCompWeaponsController {
	DECL_SIBLING_ACCESS();

	enum ESmgModes {
		ELEMENTAL_FIRE, ELEMENTAL_ICE
		, NUM_MODES
	};

	struct TFireBulletStatusEffect : public TDamageOverTimeStatusEffect {
		TFireBulletStatusEffect() : TDamageOverTimeStatusEffect("fire_bullet") {}
	} _fireStatusEffect;

	struct TIceBulletStatusEffect : public TMovementSpeedStatusEffect {
		TIceBulletStatusEffect() : TMovementSpeedStatusEffect("ice_bullet") {}

		float overrideQty = 0.02f;
		float overrideMinimum = 0.2f;

		void overrideEffect(const CStatusEffect& eff) {
			// continue overriding time
			CStatusEffect::overrideEffect(eff);

			// subtract speed mult up to a minimum
			float prevSpeedMult = speedMult;
			speedMult -= dynamic_cast<const TIceBulletStatusEffect*>(&eff)->overrideQty;
			speedMult = std::max(overrideMinimum, speedMult);

			if (speedMult < prevSpeedMult) {
				TMsgMovementSpeedMultiplier msg;
				// calculate multiplier to stack to the last one
				msg.mult = speedMult / prevSpeedMult;
				_entity.sendMsg(msg);
			}
		}
	} _iceStatusEffect;

	WeaponStats _fireModeStats, _iceModeStats;

	// Debug ray.
	bool _shootRaycast = false;
	VEC3 _rayOrig, _rayDest;

	// Handle of the player.
	CHandle playerH;
	CHandle playerTransformH;

	// Transform of the weapon.
	CHandle gunTransform;

	std::string _shootSoundEvent = "";
	std::string _reloadSoundEvent = "";
	std::string _specialSoundEvent = "";

	std::string _projectileParticlesEntityName = "";
	std::string _bulletShellsParticlesEntityName = "";
	std::string _smokeParticlesEntityName = "";
	std::string _muzzleFlashParticlesEntityName = "";

	float SmgBulletForce = 10.0f;

	ESmgModes _mode;
	CClock _specialModeTimer = CClock(4.0f);

	// Particles.
	CClock _smokeIfOverusedTimer = CClock(2.0f); 
	CClock _smokeIfOverusedIntervalTimer = CClock(0.3f);
	CClock _smokePSystemStopEmitTimer = CClock(4.0f);
	CClock _smokePSystemDisableTimer = CClock(1.0f);

	DECL_TCOMP_ACCESS(_projectileParticlesEntityName, TCompParticles, ProjectilePSystem);
	DECL_TCOMP_ACCESS(_projectileParticlesEntityName, TCompTransform, ProjectilePSystemTransform);
	DECL_TCOMP_ACCESS(_bulletShellsParticlesEntityName, TCompParticles, BulletShellsPSystem);
	DECL_TCOMP_ACCESS(_smokeParticlesEntityName, TCompParticles, SmokePSystem);
	DECL_TCOMP_ACCESS(_muzzleFlashParticlesEntityName, TCompParticles, MuzzleFlashPSystem);
	DECL_TCOMP_ACCESS("Player", TCompWeaponManager, WeaponManager);

	// On message functions.
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onTriggerExit(const TMsgEntityTriggerExit & msg);

	// Private functions.
	bool checkPlayer();
	CEntity* shootWithStat(WeaponStats& stats);
	bool fireShoot();
	bool iceShoot();
	void handleSpecialModeChange();
	void updateSpecialIcon() override;
	void updateSpecialIconActivated(bool isSpecialModeActivated) override;

public:
	TCompSmgController();

	static void declareInLua();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);
	void drawCrosshair();
	void handleChangeToSelf();
	static void registerMsgs();

	const char* getSelectedMode() const;

	bool shootNormal();
	bool shootSpecial();
	bool reload();
};