#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"

#include "components/game/status_effects/status_effect.h"

class TCompShotgunGrenade : public TCompBase {
	DECL_SIBLING_ACCESS();

	json health_eff;
	json damage_eff;

	float _timeToLive = 0.0f;
	bool _activatedVanish = false;
	float _remainingTimeToActivateVanish = 2.0f;
	std::vector<CHandle> _buffedEntities;

	std::string _smokeSoundEvent = "";
	
	void onTriggerEnter(const TMsgEntityTriggerEnter& msg);
	void onTriggerExit(const TMsgEntityTriggerExit& msg);
	void onEntityCreated(const TMsgEntityCreated& msg);

	void spawnDecal();
	void loadStatusEffects(const json& j);
	
public:
	~TCompShotgunGrenade();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);

	void activateVanish();

	static void registerMsgs();
};