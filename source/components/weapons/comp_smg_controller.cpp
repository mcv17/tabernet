#include "mcv_platform.h"
#include "comp_weapons_controller.h"
#include "comp_smg_controller.h"
#include "engine.h"
#include "entity/msgs.h"
#include "utils/utils.h"

#include "modules/module_physics.h"
#include "utils/phys_utils.h"

#include "components/common/comp_name.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_health.h"
#include "skeleton/comp_bone_tracker.h"
#include "skeleton/comp_skeleton.h"
#include "components/ui/bar/comp_reload_ui_controller.h"
#include "components/ui/comp_crosshair_ui_controller.h"
#include "components/camera/comp_camera_manager.h"
#include "components/camera/comp_camera.h"
#include "components/game/status_effects/comp_status_effects.h"

#include "entity/entity_parser.h"
#include "ui/widgets/ui_image.h"

#define DISPERSION_SCREEN_FACTOR 0.02f

using namespace physx;


DECL_OBJ_MANAGER("smg", TCompSmgController);

TCompSmgController::TCompSmgController()
{
	_weaponName = "Smg";
	_stats.damage = 10.0f;
	_stats.rof = .1f;
	_stats.dispersion = 1.0f;
	_stats.dispersionRecover = 1.0f;
	_stats.rateOfDispersion = 0.18f;
	_stats.effectiveDistance = 10.0f;
	_stats.reloadTime = 1.0f;
	_timeToNextShot = 0;
	_capacity = 40;
	_activeReloadTimer = CClock(_activeReloadOffset);

	_fireModeStats = _stats;
	_iceModeStats = _stats;
	_iceModeStats.damage = 0.0f;

	_mode = ELEMENTAL_FIRE;
}

void TCompSmgController::declareInLua() {
	EngineScripting.declareComponent<TCompSmgController>(
		"TCompSmgController",
		"getSelectedMode", &TCompSmgController::getSelectedMode
	);
}

void TCompSmgController::load(const json& j, TEntityParseContext& ctx) {
	TCompWeaponsController::load(j, ctx);

	_smokeIfOverusedTimer.setTime(j.value("time_for_smoke", _smokeIfOverusedTimer.initialTime()));
	_smokeIfOverusedIntervalTimer.setTime(j.value("smoke_time_interval_check", _smokeIfOverusedIntervalTimer.initialTime()));
	_smokePSystemStopEmitTimer.setTime(j.value("smoke_stop_emit_time", _smokeIfOverusedTimer.initialTime()));
	_specialModeTimer.setTime(j.value("special_mode_time", 4.0f));

	_shootSoundEvent = j.value("shoot_sound_event", _shootSoundEvent);
	_reloadSoundEvent = j.value("reload_sound_event", _reloadSoundEvent);
	_specialSoundEvent = j.value("special_sound_event", _specialSoundEvent);

	SmgBulletForce = j.value("SmgBulletForce", SmgBulletForce);

	if (j.count("fire_mode_stats")) {
		auto& jFireMode = j["fire_mode_stats"];
		loadWeapon(jFireMode, _fireModeStats, _stats);

		if (jFireMode.count("fire_bullet_effect")) {
			auto& jEffect = jFireMode["fire_bullet_effect"];
			_fireStatusEffect.setTime(jEffect.value("time", 4.0f));
			_fireStatusEffect.damagePerSecond = jEffect.value("dps", _fireStatusEffect.damagePerSecond);
		}
	}
	else
		_fireModeStats = _stats;

	if (j.count("ice_mode_stats")) {
		auto& jIceMode = j["ice_mode_stats"];
		loadWeapon(jIceMode, _iceModeStats, _stats);

		if (jIceMode.count("fire_bullet_effect")) {
			auto& jEffect = jIceMode["fire_bullet_effect"];
			_iceStatusEffect.setTime(jEffect.value("time", 4.0f));
			_iceStatusEffect.speedMult = jEffect.value("speed_multiplier", _iceStatusEffect.speedMult);
			_iceStatusEffect.overrideQty = jEffect.value("override_decrement", _iceStatusEffect.overrideQty);
			_iceStatusEffect.overrideMinimum = jEffect.value("minimum_speed", _iceStatusEffect.overrideMinimum);
		}
	}
	else
		_iceModeStats = _stats;

	_projectileParticlesEntityName = j.value("projectile_entity", _projectileParticlesEntityName);
	_bulletShellsParticlesEntityName = j.value("bullet_shells_entity", _bulletShellsParticlesEntityName);
	_smokeParticlesEntityName = j.value("smoke_entity", _smokeParticlesEntityName);
	_muzzleFlashParticlesEntityName = j.value("muzzle_flash_entity", _muzzleFlashParticlesEntityName);
}

void TCompSmgController::renderDebug()
{
	TCompTransform * transform = gunTransform;
	if (!transform) return;
	drawWiredSphere(getCannonPosition(transform), 0.10f);

	// Draw the raycast
	if (_shootRaycast)
		drawLine(_rayOrig, _rayDest, Vector4(1, 0, 0, 1));
}

void TCompSmgController::debugInMenu(){
	TCompWeaponsController::debugInMenu();
}

void TCompSmgController::update(float dt) {
	_curDispersion = std::max(0.0f, _curDispersion - _stats.dispersionRecover * dt);

	if (isSpecialModeActivated() && _specialModeTimer.isFinished()) {
		_specialModeTimer.stop();
		switchSpecialMode();
	}

	if (_smokeIfOverusedIntervalTimer.isFinished())
		_smokeIfOverusedTimer.stop();
	TCompParticles* smoke = getSmokePSystem();
	if (smoke && smoke->isActive()) {
		if (_smokePSystemStopEmitTimer.isFinished()) {
			_smokePSystemDisableTimer.start();
			smoke->setSystemActive(false);
		}
		else if (_smokePSystemDisableTimer.isFinished()) {
			_smokePSystemDisableTimer.stop();
			smoke->setActive(false);
		}
	}
}

void TCompSmgController::drawCrosshair() {
	if (!checkHUD()) return;

	if(CEntity * hud = hudHandle){
		TCompCrosshairUIController * crosshair = hud->getComponent<TCompCrosshairUIController>();
		if (crosshair) {
			float innerRadius = std::min(_stats.dispersion, _curDispersion) * WORLD_TO_SCREEN_FACTOR;
			float outerRadius = _stats.dispersion * WORLD_TO_SCREEN_FACTOR;
			crosshair->draw(outerRadius, innerRadius);
		}
	}
}

void TCompSmgController::handleChangeToSelf() {
	_mode = static_cast<ESmgModes>((_mode + 1) % NUM_MODES);
	updateSpecialIcon();
}

void TCompSmgController::registerMsgs() {
	DECL_MSG(TCompSmgController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompSmgController, TMsgEntitiesGroupCreated, onGroupCreated);
	DECL_MSG(TCompSmgController, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompSmgController, TMsgEntityTriggerExit, onTriggerExit);
}

const char * TCompSmgController::getSelectedMode() const {
	switch (_mode) {
	case ESmgModes::ELEMENTAL_FIRE:
		return "Elemental fire";
		break;
	case ESmgModes::ELEMENTAL_ICE:
		return "Elemental ice";
		break;
	default:
		return "";
		break;
	}
}

void TCompSmgController::onEntityCreated(const TMsgEntityCreated & msg) {
	gunTransform = getComponent<TCompTransform>();
	cameraPlayerOutputH = getEntityByName(CameraPlayerName);
}

void TCompSmgController::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	TCompWeaponsController::onGroupCreated(msg);

	bone_tracker_comp = getComponent<TCompBoneTracker>();
	if (starting_weapon)
		attachWeaponToHand();
	else
		attachWeaponToHolder();
}

void TCompSmgController::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	// Raise weapon animation. Change to block state so we can't shoot.
	// We can also check the type of entity if necessary.
}

void TCompSmgController::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	// Low weapon animation. Change to normal state so we can shoot again.
	// We can also check the type of entity if necessary.
}

bool TCompSmgController::checkPlayer() {
	if (playerH.isValid()) return true;

	playerH = getEntityByName("Player");
	if (!playerH.isValid())
		return false;

	CEntity * player = playerH;
	playerTransformH = player->getComponent<TCompTransform>();

	return true;
}

CEntity* TCompSmgController::shootWithStat(WeaponStats & stats) {
	if (!gunTransform.isValid())
		return nullptr;
	TCompTransform* w_trans = gunTransform;

	CEntity* pEntityHit = nullptr;

	// Get camera transform, position and direction.
	// Also get player handle and transform.
	// If not found or invalid, cancel.
	if (!checkCamera() || !checkPlayer()) return nullptr;

	CEntity * camera = cameraPlayerH;
	TCompTransform * c_trans = cameraTransformH;
	TCompCamera * cameraComponent = cameraCameraH;

	VEC3 cameraPos = c_trans->getPosition();
	VEC3 cameraDirection = c_trans->getFront();
	float infiniteDistance = cameraComponent->getFar() + 1.0f; // Get as far as the camera zfar.

	// Shoot the ray from the camera front. If we hit something, we will shoot to that point, otherwise, we point to a place
	// far away in the "infinite".
	PxRaycastBuffer pointToShotAtHitData = PhysUtils::shootPlayerRaycast(c_trans->getPosition(), cameraDirection, infiniteDistance);
	VEC3 pointToShotAt = cameraPos + cameraDirection * infiniteDistance; // We point to the infinite by default.
	VEC3 pointToInfinite = pointToShotAt;
	if (pointToShotAtHitData.hasBlock)
		pointToShotAt = cameraPos + (cameraDirection * pointToShotAtHitData.block.distance); // Get the hitPoint on World Position

	// Get smg cannon position and shoot from there again.
	CTransform r_trans;
	r_trans.setPosition(getCannonPosition(w_trans));
	r_trans.setRotation(w_trans->getRotation());
	Vector3 r_position = r_trans.getPosition();
	VEC3 dirToEnemy = pointToShotAt - r_position;
			
	// If camera is pointing behind the player shoot to the "infinite".
	// Prevents bullets going bacwards.
	if (cameraDirection.Dot(dirToEnemy) < 1.0f)
		dirToEnemy = pointToInfinite;
	dirToEnemy.Normalize();

	/* Apply dispersion */

	// Calculate scale factor so dispersion will be according to how close the camera is to the player
	CEntity * player = playerH;
	TCompTransform * p_trans = playerTransformH;
	float scale_factor = cameraDirection.Dot(p_trans->getPosition() - c_trans->getPosition());
			
	// Get random polar coordinates for deviation
	float r = RNG.f(_curDispersion) * scale_factor * DISPERSION_SCREEN_FACTOR;
	float theta = (float)RNG.d(2.0 * M_PI);
	// Obtain deviation vector with x y coordinates
	VEC3 deviation;
	deviation.x = r * cosf(theta);
	deviation.y = r * sinf(theta);
			
	// Rotate the deviation vector to match the direction of the enemy
	deviation = DirectX::XMVector3Rotate(deviation, c_trans->getRotation());
			
	// Add bullet deviation
	dirToEnemy = dirToEnemy + deviation;
	dirToEnemy.Normalize();
			
	// Dispersion applies always AFTER a shot was made
	// Formula to apply dispersion over time using rate of fire (upper value > _dispersion so it won't flicker when it gets to max dispersion)
	_curDispersion = Maths::clamp(_curDispersion + stats.rateOfDispersion * stats.dispersion, 0.0f, stats.dispersion + stats.rateOfDispersion * stats.dispersion);

	// Shot from the rifle to the point we hit in the camera.
	PxRaycastBuffer bulletHitData = PhysUtils::shootPlayerRaycast(r_position, dirToEnemy, stats.maxDistance);

	// DEBUG RAY
	_shootRaycast = true;
	_rayOrig = r_trans.getPosition();
	_rayDest = r_trans.getPosition() + (dirToEnemy * stats.maxDistance);

	if (bulletHitData.hasBlock) {
		float distance = bulletHitData.block.distance;
		_rayDest = r_trans.getPosition() + (dirToEnemy * distance);
		// Get hit entity and send the msg.
		CHandle entityHit;
		CHandle h_comp;
		h_comp.fromVoidPtr(bulletHitData.block.actor->userData);

		if (h_comp.isValid())
			entityHit = h_comp.getOwner();

		if (!entityHit.isValid())
			return nullptr;

		pEntityHit = entityHit;

		float damage = 0.0f;
		if (distance > _stats.effectiveDistance) {
			float falloffRatio = (distance - _stats.effectiveDistance) / (_stats.maxDistance - _stats.effectiveDistance);
			damage = floor(_stats.falloff(_stats.damage, 0.0f, falloffRatio));
		}
		else
			damage = floor(_stats.damage);

		if (damage > 0.0f) {
			TMsgBulletDamage msg;
			msg.h_sender = CHandle(this).getOwner();
			msg.damage = damage;
			msg.damageDirection = dirToEnemy;
			msg.damageForce = SmgBulletForce;
			msg.actorHit = bulletHitData.block.actor;
			entityHit.sendMsg(msg);

			// Spawn a decal on the entity.
			spawnParticlesAndDecalOnHit(entityHit, bulletHitData, dirToEnemy);
		}
	}
	
	// Bullet tracer
	TCompWeaponManager* weaponManager = getWeaponManager();
	weaponManager->addTracer(r_position, _rayDest, 
													 stats.tracerThickness, stats.tracerColor, 
													 stats.tracerDuration, stats.tracerDelay, stats.tracerStartFadeFactor
	);

	_timeToNextShot = Time.accumulated_delta + stats.rof;

	// Projectile particles
	TCompTransform* projectilesTransform = getProjectilePSystemTransform();
	if (projectilesTransform) {
		projectilesTransform->setPosition(r_position);
		projectilesTransform->lookAt(r_position, _rayDest);
	}

	TCompParticles* projectiles = getProjectilePSystem();
	if(projectiles && RNG.b(0.5f))
		projectiles->spawnParticles(1);
	
	TCompParticles* bulletShells = getBulletShellsPSystem();
	if(bulletShells)
		bulletShells->spawnParticles(1);

	// Muzzle flash particles
	TCompParticles * muzzleFlash = getMuzzleFlashPSystem();
	if (muzzleFlash)
		muzzleFlash->spawnParticles(1);


	return pEntityHit;
}

bool TCompSmgController::fireShoot() {
	if (!_isReloading && _timeToNextShot < Time.accumulated_delta) {
		CEntity* entity = shootWithStat(_fireModeStats);
		if (entity) {
			// apply fire effect
			TCompStatusEffects* effects = entity->getComponent<TCompStatusEffects>();
			if (effects)
				effects->addStatusEffect(_fireStatusEffect);
		}

		// Send camera shake.
		sendCameraShakeOnShot();

		/* Play audio event*/
		if (!_shootSoundEvent.empty())
			EngineAudio.playEvent(_shootSoundEvent);

		return true;
	}
	return false;
}

bool TCompSmgController::iceShoot() {
	if (!_isReloading && _timeToNextShot < Time.accumulated_delta) {
		CEntity* entity = shootWithStat(_iceModeStats);
		if (entity) {
			// apply ice effect
			TCompStatusEffects* effects = entity->getComponent<TCompStatusEffects>();
			if (effects)
				effects->addStatusEffect(_iceStatusEffect);
		}

		// Send camera shake.
		sendCameraShakeOnShot();

		/* Play audio event*/
		if (!_shootSoundEvent.empty())
			EngineAudio.playEvent(_shootSoundEvent);

		return true;
	}
	return false;
}

void TCompSmgController::handleSpecialModeChange() {
	if (isSpecialModeActivated()) {
		_specialModeTimer.stop();
		updateSpecialIcon();
	}
}

void TCompSmgController::updateSpecialIcon()
{
	UI::CImage specialIcon;
	std::string widgetName;

	widgetName = _mode == ESmgModes::ELEMENTAL_FIRE ? "special_smg_fire" : "special_smg_ice";

	if (EngineUI.findWidgetByName(specialIcon, "special_icons")) {
		for (auto& child : specialIcon.getChildren()) {
			if (UI::CImage * imageChild = dynamic_cast<UI::CImage*>(child)) {
				if (imageChild->getName() == widgetName)
					imageChild->getParams()->visible = true;
				else
					imageChild->getParams()->visible = false;
			}
		}
	}
	updateSpecialIconActivated(_specialModeActivated);
}

void TCompSmgController::updateSpecialIconActivated(bool isSpecialModeActivated)
{
	UI::CImage* img = nullptr;
	UI::CImage* swapIcon = nullptr;
	UI::CImage* activateIcon = nullptr;
	if (_mode == ESmgModes::ELEMENTAL_FIRE)
		img = EngineUI.getImageWidgetByName("special_icons", "special_smg_fire");
	else if (_mode == ESmgModes::ELEMENTAL_ICE)
		img = EngineUI.getImageWidgetByName("special_icons", "special_smg_ice");

	if (img) {
		img->getImageParams()->color = isSpecialModeActivated ? _specialActivatedColor : VEC4(1.0f, 1.0f, 1.0f, 1.0f);
		if (swapIcon = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button_smg"))
			swapIcon->getImageParams()->color = isSpecialModeActivated ? _specialActivatedColor : VEC4(1.0f, 1.0f, 1.0f, 1.0f);
		if (activateIcon = EngineUI.getImageWidgetByName("special_icons_buttons", "special_button"))
			activateIcon->getImageParams()->color = isSpecialModeActivated ? _specialActivatedColor : VEC4(1.0f, 1.0f, 1.0f, 1.0f);
	}
}

bool TCompSmgController::shootNormal() {
	if (!_isReloading && _currentAmmo > 0 && _timeToNextShot < Time.accumulated_delta) {
		shootWithStat(_stats);
		_currentAmmo--;

		// Send camera shake.
		sendCameraShakeOnShot();

		// Spawn particles.
		TCompParticles* smoke = getSmokePSystem();
		if (smoke && !smoke->isActive()) {
			if (_smokeIfOverusedTimer.isStopped())
				_smokeIfOverusedTimer.start();
			else if (_smokeIfOverusedTimer.isFinished()) {
				smoke->setActive(true);
				smoke->setSystemActive(true);
				_smokeIfOverusedTimer.stop();
			}
		}
		_smokeIfOverusedIntervalTimer.start();
		_smokePSystemStopEmitTimer.start();

		/* Play audio event*/
		// TODO_SOUND: ammo qty parameter
		if (!_shootSoundEvent.empty())
			EngineAudio.playEvent(_shootSoundEvent);

		return true;
	}
	else
		return false;
}

bool TCompSmgController::shootSpecial() {
	if (!_specialModeTimer.isStarted()) {
		_specialModeTimer.start();
		useSpecialAmmo();
		EngineAudio.playEvent(_specialSoundEvent);
	}

	switch (_mode) {
	case ELEMENTAL_FIRE:
		return fireShoot();
		break;
	case ELEMENTAL_ICE:
		return iceShoot();
		break;
	}
}

bool TCompSmgController::reload()
{
	if (_storedAmmo > 0 && _currentAmmo < _capacity && !_activeReloadTried) {
		if (_isReloading ) {
			_activeReloadTried = true;
			if (TCompReloadUIController* reloadBarComp = getComponent<TCompReloadUIController>()) 
				return reloadBarComp->checkIfSuccess();
		}
		else {
			_isReloading = true;

			if (TCompReloadUIController* reloadBarComp = getComponent<TCompReloadUIController>())
				reloadBarComp->startReload(_stats.reloadTime, _activeReloadOffset, _activeReloadPenaltyTime);

			if (!_reloadSoundEvent.empty())
				EngineAudio.playEvent(_reloadSoundEvent);

			return true;
		}

		return false;
	}

	return false;
}