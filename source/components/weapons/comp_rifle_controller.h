#pragma once
#include "components/weapons/comp_weapons_controller.h"
#include "entity/entity.h"
#include <geometry\curve.h>
#include "components/render/comp_line_render.h"
#include "components/common/comp_particles.h"
#include "comp_weapons_manager.h"

struct TMsgEntityCreated;
struct TMsgEntityTriggerEnter;
struct TMsgEntityTriggerExit;

class TCompRifleController : public TCompWeaponsController {
	DECL_SIBLING_ACCESS();

	/** Variables **/
	// Const
	const std::string _projecticlePath = "data/prefabs/weapons/rifleGrenade.json";
	// Helpers
	bool _shootRaycast = false;
	VEC3 _rayOrig, _rayDest;
	CHandle gunTransform;
	CHandle gunCollider;
	bool _knotChanged;
	
	// Data
	CCurve _shootCurve;
	float _hitRange = 10.0f;
	std::vector<VEC3> _knotOffsets; //Offsets from the tip of the rifle
	float _specialShootCD = 2.0f;
	CClock _specialCDClock;

	std::string _shootSoundEvent = "";
	std::string _reloadSoundEvent = "";
	std::string _grenadeSoundEvent = "";

	std::string _lineRenderEntityName = "";
	std::string _smokeParticlesEntityName = "";
	std::string _muzzleFlashEntityName = "";

	float RifleBulletForce;

	int _smokeNumParticles = 32;

	DECL_TCOMP_ACCESS(_smokeParticlesEntityName, TCompParticles, SmokePSystem);
	DECL_TCOMP_ACCESS(_muzzleFlashEntityName, TCompParticles, MuzzleFlashPSystem);
	DECL_TCOMP_ACCESS("Player", TCompWeaponManager, WeaponManager);

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onTriggerExit(const TMsgEntityTriggerExit & msg);

	// UI
	void updateSpecialIconActivated(bool isSpecialModeActivated) override;

public:
	TCompRifleController();
	
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);
	void drawCrosshair();
	static void registerMsgs();

	std::vector<VEC3> getCurveKnots() { return _shootCurve.getKnots(); }
	bool shootNormal();
	bool shootSpecial();
	void updateAimCurve();
	bool reload();
};