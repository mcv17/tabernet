#pragma once
#include "components/weapons/comp_weapons_controller.h"
#include "entity/entity.h"

struct TMsgEntityCreated;

class TCompScytheController : public TCompBase {
	DECL_SIBLING_ACCESS();

	CHandle player_weapon_manager;
	std::string HolderEntity;
	bool SpawnedDrops = false;

	// Bone tracker component of the weapon.
	CHandle bone_tracker_comp;

	// In which bone it starts in.
	bool start_attached = false;

	// Bone to attach weapon's grip to.
	std::string handBoneName;

	// Holder bone to attach the weapon when not carrying it.
	std::string holderBoneName;

	// Offset from the bone when the grip is being grabbed.
	Vector3 gripOffset;
	Quaternion gripRotationOffset;

	// Offset from the bone when the scythe is being hold in the back or somewhere else. 
	Vector3 holderOffset;
	Quaternion holderRotationOffset;

	float _damage = 0.0f;
	std::vector<CHandle> _enemies;

	CHandle _collider;
	CHandle _transform;

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & trigger_enter);

public:

	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);

	static void registerMsgs();

	void attachWeaponToHand();
	void attachWeaponToHolder();
	void enableDamage(float dmg);
	void disableDamage();

	void CheckIfWeMustSpawnAmmo(const CTransform & transform);
	void spawnScytheDrops(const std::string & dropType, const CTransform & transform);
};