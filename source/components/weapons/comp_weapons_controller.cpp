#include "mcv_platform.h"
#include "comp_weapons_controller.h"

#include "components/common/comp_transform.h"
#include "components/common/comp_tags.h"
#include "components/camera/comp_camera.h"
#include "skeleton/comp_bone_tracker.h"

#include "entity/entity.h"
#include "utils/utils.h"
#include "geometry/geometry.h"
#include "ui/widgets/ui_text_font.h"

#include "../../utils/surface_querier.h"
#include "ui/widgets/ui_image.h"
#include "comp_weapons_manager.h"

const float TCompWeaponsController::WORLD_TO_SCREEN_FACTOR = 0.025f;
int TCompWeaponsController::_maxAltCapacity;
int TCompWeaponsController::_currentAltAmmo;

void TCompWeaponsController::load(const json & j, TEntityParseContext & ctx) {
	// Name of the weapon.
	_weaponName = j.value("weapon_name", _weaponName);

	starting_weapon = j.value("start_with_it_equipped", false);

	// Name of the entity carrying the weapon.
	name_entity_carrying_weapon = j.value("entity_carrying_weapon", "");

	/* Holder variables. */
	holderBoneName = j.value("holder_bone", ""); // Holder bone to attach weapon to when carrying it.
	holderOffset = loadVector3(j, "holder_offset"); // Offset where the weapon must be, relative to the holder bone (avoid having more than one bone for each weapon if they are all carried in the back).
	Vector3 angles = loadVector3(j, "holder_rotation_offset");
	float yaw = DEG2RAD(angles.x);
	float pitch = DEG2RAD(angles.y);
	float roll = DEG2RAD(angles.z);
	holderRotationOffset = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);

	/* Grip variables. */
	handBoneName = j.value("hand_bone", ""); // Hand to attach weapon's grip to.
	gripOffset = loadVector3(j, "grip_offset");	// Offset where the grip is, remove when we have an actual bone for the weapon.
	angles = loadVector3(j, "grip_rotation_offset");
	yaw = DEG2RAD(angles.x);
	pitch = DEG2RAD(angles.y);
	roll = DEG2RAD(angles.z);
	gripRotationOffset = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);

	// Where bullets come from.
	if (j.count("shooting_offset"))
		offsetGunCannon = loadVector3(j, "shooting_offset");

	// Weapon stats variables.
	loadWeapon(j, _stats, _stats);
	_baseDispersion = _stats.dispersion;
	_capacity = j.value("capacity", _capacity);

	// Active reload variables.
	_activeReloadOffset = j.value("active_reload_offset", _activeReloadOffset);
	_activeReloadPenaltyTime = j.value("active_reload_penalty", _activeReloadPenaltyTime);

	// Ammo variables
	_storedAmmo = j.value("starting_ammo", 240);
	_currentAmmo = std::min(_storedAmmo, _capacity);
	_storedAmmo -= _currentAmmo;

	//Special ammo (shared in all weapons, so put the same number)
	_maxAltCapacity = j.value("maxAltCapacity", 5);
	_currentAltAmmo = j.value("currentAltCapacity", 2);

	// Load lua functios for the weapon.
	OnBulletHitFXFunction = j.value("OnHitFXFunction", "spawnOnHitFXDefaultWeapon");
	OnBloodSplatterFxFunction = j.value("OnBloodSplatterFxFunction", "spawnBloodSplatterFXDefaultWeapon");

	CameraPlayerName = j.value("camera_player_name", "Camera-Player-Output");
	shakePerBullet = j.value("shake_per_bullet", 0.0f);
	maxShake = j.value("max_shake", 0.0f);
}

void TCompWeaponsController::debugInMenu() {
	ImGui::LabelText("Name of the weapon: ", _weaponName.c_str());
	
	TCompBoneTracker * bone_tracker = bone_tracker_comp;

	ImGui::DragFloat3("Grip Offset: ", &gripOffset.x, 0.0001f, -10.0f, 10.0f);

	static Vector3 gripAngles;
	if (ImGui::DragFloat3("Grip rotation offset: ", &gripAngles.x, 0.1f, 1000.0f, 1000.f))
		gripRotationOffset = gripRotationOffset.CreateFromYawPitchRoll(DEG2RAD(gripAngles.x), DEG2RAD(gripAngles.y), DEG2RAD(gripAngles.z));

	if (bone_tracker->getBoneNameTracked() == handBoneName) {
		bone_tracker->setBoneOffset(gripOffset);
		bone_tracker->setBoneRotationOffset(gripRotationOffset);
	}

	ImGui::DragFloat3("Holder Offset: ", &holderOffset.x, 0.01f, -10.0f, 10.f);
	
	static Vector3 holderAngles;
	if (ImGui::DragFloat3("Holder rotation offset: ", &holderAngles.x, 0.1f, 1000.0f, 1000.f))
		holderRotationOffset = holderRotationOffset.CreateFromYawPitchRoll(DEG2RAD(holderAngles.x), DEG2RAD(holderAngles.y), DEG2RAD(holderAngles.z));

	if (bone_tracker->getBoneNameTracked() == holderBoneName) {
		bone_tracker->setBoneOffset(holderOffset);
		bone_tracker->setBoneRotationOffset(holderRotationOffset);
	}

	ImGui::DragFloat3("Offset cannon: ", &offsetGunCannon.x, 0.01f, -10.0f, 10.f);
	
	ImGui::DragFloat("Weapon damage: ", &_stats.damage, 0.01f, 0.0f, 100000.f);
	ImGui::DragFloat("Rate of Fire: ", &_stats.rof, 0.01f, 0.0f, 100000.f);
	ImGui::DragFloat("Dispersion: ", &_stats.dispersion, 0.01f, 0.0f, 100000.f);
	ImGui::DragFloat("Effective distance: ", &_stats.effectiveDistance, 0.01f, 0.0f, 100000.f);
	ImGui::DragInt("Capacity: ", &_capacity, 1, 0, 100000);
	
	ImGui::DragFloat("Active Reload Offset: ", &_activeReloadOffset, 0.01f, 0.0f, _stats.reloadTime);
	ImGui::DragFloat("Active Reload Penalty Time: ", &_activeReloadPenaltyTime, 0.01f, 0.0f, 5.0f);
	
	std::string ammoLabel = "Current ammo: " + std::to_string(_currentAmmo) + "/" + std::to_string(_capacity);
	ImGui::ProgressBar((float)_currentAmmo / _capacity, ImVec2(-1, 0), ammoLabel.c_str());
	
	ImGui::Text("Stats dumper");
	if (ImGui::Button("Show dump")) {
		jsonStats = dumpWeaponStats();
	}
	ImGui::SameLine();
	if (ImGui::Button("Dump to clipboard")) {
		ImGui::LogToClipboard();
		ImGui::LogText(dumpWeaponStats().c_str());
		ImGui::LogFinish();
	}
	ImGui::TextWrapped(jsonStats.c_str()); 

	ImGui::DragFloat("Shake per bullet: ", &shakePerBullet, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("Max shake for weapon: ", &maxShake, 0.01f, 0.0f, 1.0f);
}

bool TCompWeaponsController::shoot() {
	if (_specialModeActivated){
		if(shootSpecial()){
			updateAmmoUI();
			return true;
		}
	}
	else{
		if(shootNormal()){
			updateAmmoUI();
			return true;
		}
	}
}

void TCompWeaponsController::setSpecialMode(bool state) {
	if (_specialModeActivated != state) {
		if (state == true && _currentAltAmmo <= 0) // look that still has special ammo available
			return;
		handleSpecialModeChange();
		_specialModeActivated = state;
		updateSpecialIconActivated(state);
	}
}

void TCompWeaponsController::switchSpecialMode() {
	setSpecialMode(!_specialModeActivated);
	updateSpecialIcon();
}

std::string TCompWeaponsController::dumpWeaponStats() {
	std::string returnString;
	json j;
	j["damage"] = _stats.damage;
	j["rof"] = _stats.rof;
	j["dispersion"] = _stats.dispersion;
	j["effectiveDistance"] = _stats.effectiveDistance;
	j["capacity"] = _capacity;
	j["reloadTime"] = _stats.reloadTime;
	j["rateOfDispersion"] = _stats.rateOfDispersion;
	j["activeReloadOffset"] = _activeReloadOffset;
	j["activeReloadPenalty"] = _activeReloadPenaltyTime;
	returnString = j.dump(0);
	// Remove brackets
	returnString.pop_back();
	returnString.erase(returnString.begin());
	return returnString;
}

void TCompWeaponsController::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {}

void TCompWeaponsController::updateAmmoUI()
{
	UI::CWidget ammoWidget;
	std::string ammostr;
	int ammoQt = -1;

	if (EngineUI.findWidgetByName(ammoWidget, "ammo_text")) {
		for (auto& child : ammoWidget.getChildren()) {
			if (UI::CTextFont * textChild = dynamic_cast<UI::CTextFont*>(child)) {
				if (textChild->getName() == "current_ammo") {
					ammoQt = getCurrentAmmo();
				}
				else if (textChild->getName() == "total_ammo"){
					ammoQt = getTotalAmmo() - getCurrentAmmo();
				}
				else if (textChild->getName() == "special_current_ammo") {
					ammoQt = getCurrentAltAmmo();
				}

				if (ammoQt != -1) {
					if(ammoQt < 10)
						ammostr = " " + std::to_string(ammoQt);
					else
						ammostr = std::to_string(ammoQt);
					textChild->getTextParams()->text = std::wstring(ammostr.begin(), ammostr.end());
					if (ammoQt <= 0) {
						textChild->getTextParams()->textColor.f[0] = 0.7f;
						textChild->getTextParams()->textColor.f[1] = 0.0f;
						textChild->getTextParams()->textColor.f[2] = 0.0f;
						textChild->getTextParams()->textColor.f[3] = 1.0f;
					}
					else if (textChild->getTextParams()->textColor.f[0] != 0.9f) {
						textChild->getTextParams()->textColor.f[0] = 0.9f;
						textChild->getTextParams()->textColor.f[1] = 0.9f;
						textChild->getTextParams()->textColor.f[2] = 0.9f;
						textChild->getTextParams()->textColor.f[3] = 1.0f;
					}
					ammoQt = -1;
				}

			}
		}
	}
}

void TCompWeaponsController::updateWeaponIcon(int weaponSlot)
{
	UI::CWidget weaponIcon;
	std::string widgetName;

	if (weaponSlot == (int)Weapon::SMG_WEAPON)
		widgetName = "smg_icon";
	else if (weaponSlot == (int)Weapon::SHOTGUN_WEAPON)
		widgetName = "shotgun_icon";
	else if (weaponSlot == (int)Weapon::RIFLE_WEAPON)
		widgetName = "rifle_icon";

	if (EngineUI.findWidgetByName(weaponIcon, "weapon_icons")) {
		for (auto& child : weaponIcon.getChildren()) {
			if (UI::CImage * imageChild = dynamic_cast<UI::CImage*>(child)) {
				if(imageChild->getName() == widgetName)
					imageChild->getParams()->visible = true;
				else
					imageChild->getParams()->visible = false;
			}
		}
	}
}

void TCompWeaponsController::updateWeaponCrosshair(int weaponSlot)
{
	UI::CWidget crosshairWidget;
	std::string widgetName;
	std::string names[3] = { "crosshair_smg" , "crosshair_shotgun" , "crosshair_rifle" };

	if (weaponSlot == (int)Weapon::SMG_WEAPON)
		widgetName = "crosshair_smg";
	else if (weaponSlot == (int)Weapon::SHOTGUN_WEAPON)
		widgetName = "crosshair_shotgun";
	else if (weaponSlot == (int)Weapon::RIFLE_WEAPON)
		widgetName = "crosshair_rifle";

	for (auto name : names) { 
		if (EngineUI.findWidgetByName(crosshairWidget, name)){
			if (name == widgetName) {
				for (auto& child : crosshairWidget.getChildren())
					child->getParams()->visible = true;
			}
			else
				for (auto& child : crosshairWidget.getChildren())
					child->getParams()->visible = false;
		}
	}
}

void TCompWeaponsController::updateSpecialIcon()
{
	UI::CImage specialIcon;
	std::string widgetName;

	if (_weaponName == "Smg")
		widgetName = "special_smg_fire";
	else if (_weaponName == "Shotgun")
		widgetName = "special_shotgun";
	else if (_weaponName == "Rifle")
		widgetName = "special_rifle";

	if (EngineUI.findWidgetByName(specialIcon, "special_icons")) {
		for (auto& child : specialIcon.getChildren()) {
			if (UI::CImage * imageChild = dynamic_cast<UI::CImage*>(child)) {
				if (imageChild->getName() == widgetName)
					imageChild->getParams()->visible = true;
				else
					imageChild->getParams()->visible = false;
			}
		}
	}
}

// Fetch camera handles. Returns true if everything valid, false ow.
bool TCompWeaponsController::checkCamera() {
	if (cameraPlayerH.isValid())
		return true;

	cameraPlayerH = getEntityByName("Camera-Player");
	if (!cameraPlayerH.isValid())
		return false;

	CEntity* camera = cameraPlayerH;
	cameraCameraH = camera->getComponent<TCompCamera>();
	cameraTransformH = camera->getComponent<TCompTransform>();
	return true;
}

// Fetch hud handles. Returns true if everything valid, false ow.
bool TCompWeaponsController::checkHUD() {
	if (hudHandle.isValid())
		return true;

	hudHandle = getEntityByName("HUD");
	if (!hudHandle.isValid())
		return false;

	return true;
}

void TCompWeaponsController::resetReload() { 
	_isReloading = false; 
	_activeReloadTimer.stop();
	_activeReloadTried = false; 
	updateAmmoUI();
}

void TCompWeaponsController::resetAmmo() { 
	int prevAmmo = _currentAmmo;
	int currentAvailableAmmo = std::min(_storedAmmo, _capacity);
	int ammoUsedInReload = std::min(_capacity - prevAmmo, currentAvailableAmmo);
	_currentAmmo = prevAmmo + ammoUsedInReload;
	_storedAmmo = _storedAmmo - ammoUsedInReload;
}

void TCompWeaponsController::resetDispersion() {
	_curDispersion = 0.0f;
}

void TCompWeaponsController::resetMaxDispersion() {
	_stats.dispersion = _baseDispersion;
}

Vector3 TCompWeaponsController::getCannonPosition(const TCompTransform * weaponTransform) {
	return Vector3(weaponTransform->getPosition()
		+ weaponTransform->getFront() * offsetGunCannon.z
		+ weaponTransform->getLeft() * offsetGunCannon.x
		+ weaponTransform->getUp() * offsetGunCannon.y);
}

void TCompWeaponsController::attachWeaponToHand() {
	TCompBoneTracker * bone_tracker = bone_tracker_comp;
	if (!bone_tracker) return;

	// Set weapon to now follow the holder bone.
	// Add the necessary offsets so the gun actually appears where it should.
	bone_tracker->setNewBoneToTrack(handBoneName);
	bone_tracker->setBoneOffset(gripOffset);
	bone_tracker->setBoneRotationOffset(gripRotationOffset);
}

void TCompWeaponsController::attachWeaponToHolder() {
	TCompBoneTracker * bone_tracker = bone_tracker_comp;
	if (!bone_tracker) return;

	// Set weapon to now follow the holder bone.
	// Add the necessary offsets so the gun actually appears where it should.
	bone_tracker->setNewBoneToTrack(holderBoneName);
	bone_tracker->setBoneOffset(holderOffset);
	bone_tracker->setBoneRotationOffset(holderRotationOffset);
}

void TCompWeaponsController::loadWeapon(const json& j, WeaponStats& stats, WeaponStats& defaultStats) {
	stats.damage = j.value("damage", defaultStats.damage);
	stats.rof = j.value("rof", defaultStats.rof);
	stats.dispersion = j.value("dispersion", defaultStats.dispersion);
	stats.effectiveDistance = j.value("effective_distance", defaultStats.effectiveDistance);
	stats.maxDistance = j.value("max_distance", defaultStats.maxDistance);
	if (j.count("falloff") > 0)
		stats.falloff = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(j["falloff"]);
	else
		stats.falloff = [](float a, float b, float c) { return 0.0f; };
	stats.reloadTime = j.value("reload_time", defaultStats.reloadTime);
	stats.rateOfDispersion = j.value("rate_of_dispersion", defaultStats.rateOfDispersion);
	stats.dispersionRecover = j.value("dispersion_recover", defaultStats.dispersionRecover);

	stats.tracerDelay = j.value("tracer_delay", defaultStats.tracerDelay);
	stats.tracerDuration = j.value("tracer_duration", defaultStats.tracerDuration);
	stats.tracerThickness = j.value("tracer_thickness", defaultStats.tracerThickness);
	stats.tracerColor = loadVector4(j, "tracer_color");
	stats.tracerStartFadeFactor = j.value("tracer_start_fade_factor", defaultStats.tracerStartFadeFactor);
}

void TCompWeaponsController::spawnParticlesAndDecalOnHit(CHandle entityHit, const physx::PxRaycastBuffer & bulletHitData, const Vector3 & rayDir) {
	// If we hit an enemy, we will spawn blood particles in the hit point and throw a ray from the hitpoint onwards to spawn a blood decal on the hit.
	if (hitEnemy(bulletHitData)) {
		// Get the entity hit.
		std::string surfaceHit = getSurfaceHit(entityHit);

		// Spawn blood on enemy hit.
		LogicManager::Instance().spawnFXOnHitPos(PXVEC3_TO_VEC3(bulletHitData.block.position), PXVEC3_TO_VEC3(bulletHitData.block.normal), OnBulletHitFXFunction, surfaceHit);

		// Throw ray to see where to spawn blood.
		throwBloodSplatterRay(bulletHitData, rayDir);
	}
	else{
		// Get the entity hit.
		std::string surfaceHit = getSurfaceHit(entityHit);

		// Spawn bullet hit on surface.
		LogicManager::Instance().spawnFXOnHitPos(PXVEC3_TO_VEC3(bulletHitData.block.position), PXVEC3_TO_VEC3(bulletHitData.block.normal), OnBulletHitFXFunction, surfaceHit);
	}
}

void TCompWeaponsController::sendCameraShakeOnShot() {
	LogicManager::Instance().SendDeltaTrauma(cameraPlayerOutputH, shakePerBullet, maxShake);
}

std::string TCompWeaponsController::getSurfaceHit(CHandle entityHit) {
	SurfaceQuerier::SurfaceType surfaceType = "";
	CEntity * entity = entityHit;
	if (entity) {
		TCompTags * tags = entity->getComponent<TCompTags>();
		if (tags)
			surfaceType = SurfaceQuerier::SurfaceQuerierManager::GetSurfaceType(tags);
	}
	return surfaceType;
}

bool TCompWeaponsController::hitEnemy(const physx::PxRaycastBuffer & bulletHitData) {
	physx::PxFilterData data = bulletHitData.block.shape->getQueryFilterData();
	return data.word0 == CModulePhysics::FilterGroup::Enemy || data.word0 == CModulePhysics::FilterGroup::Ragdoll || data.word0 == CModulePhysics::FilterGroup::EnemyHitboxes;
}

void TCompWeaponsController::throwBloodSplatterRay(const physx::PxRaycastBuffer & bulletHitData, const Vector3 & rayDir) {
	// Throw ray to see where to spawn blood. Blood splatter rays have a default max distance of 10.0 units behind.
	physx::PxRaycastBuffer bloodHitData = PhysUtils::shootBloodSplatterRaycast(PXVEC3_TO_VEC3(bulletHitData.block.position), rayDir, 3.0f);

	// Spawn blood in the given position if we hit somewhere.
	if (bloodHitData.hasBlock)
		LogicManager::Instance().spawnFXOnHitPos(PXVEC3_TO_VEC3(bloodHitData.block.position), PXVEC3_TO_VEC3(bloodHitData.block.normal), OnBloodSplatterFxFunction);
}