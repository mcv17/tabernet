#include "mcv_platform.h"
#include "comp_scythe_controller.h"
#include "modules/module_physics.h"
#include "engine.h"
#include "entity/msgs.h"
#include "utils/phys_utils.h"
#include "utils/utils.h"

#include "skeleton/comp_bone_tracker.h"

#include "entity/entity_parser.h"
#include "comp_weapons_manager.h"

#define DISPERSION_SCREEN_FACTOR 0.008f

using namespace physx;

DECL_OBJ_MANAGER("scythe_controller", TCompScytheController);

void TCompScytheController::load(const json& j, TEntityParseContext& ctx) {
	HolderEntity = j.value("holder_entity", "");

	// Hand to attach weapon's grip to.
	handBoneName = j.value("hand_bone", "");

	// Holder bone to attach weapon to when carrying it.
	holderBoneName = j.value("holder_bone", "");

	// Whether we start with the scythe attached or not.
	start_attached = j.value("start_attached", false);

	// Offset where the grip is, remove when we have an actual bone for the weapon.
	gripOffset = loadVector3(j, "grip_offset");
	Vector3 angles = loadVector3(j, "grip_rotation_offset");
	float yaw = DEG2RAD(angles.x);
	float pitch = DEG2RAD(angles.y);
	float roll = DEG2RAD(angles.z);
	gripRotationOffset = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);

	// Offset where the weapon must be, relative to the holder bone (avoid having more than one bone for each weapon if they are all carried in the back).
	holderOffset = loadVector3(j, "holder_offset");
	angles = loadVector3(j, "holder_rotation_offset");
	yaw = DEG2RAD(angles.x);
	pitch = DEG2RAD(angles.y);
	roll = DEG2RAD(angles.z);
	holderRotationOffset = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);

	_damage = j.value("damage", _damage);
}

void TCompScytheController::debugInMenu(){
	ImGui::DragFloat3("Grip Offset: ", &gripOffset.x, 0.0001f, -10.0f, 10.0f);

	static Vector3 gripAngles;
	if (ImGui::DragFloat3("Grip rotation offset: ", &gripAngles.x, 0.1f, 1000.0f, 1000.f))
		gripRotationOffset = gripRotationOffset.CreateFromYawPitchRoll(DEG2RAD(gripAngles.x), DEG2RAD(gripAngles.y), DEG2RAD(gripAngles.z));

	TCompBoneTracker * bone_tracker = bone_tracker_comp;

	if (bone_tracker->getBoneNameTracked() == handBoneName) {
		bone_tracker->setBoneOffset(gripOffset);
		bone_tracker->setBoneRotationOffset(gripRotationOffset);
	}

	ImGui::DragFloat3("Holder Offset: ", &holderOffset.x, 0.01f, -10.0f, 10.f);

	static Vector3 holderAngles;
	if (ImGui::DragFloat3("Holder rotation offset: ", &holderAngles.x, 0.1f, 1000.0f, 1000.f))
		holderRotationOffset = holderRotationOffset.CreateFromYawPitchRoll(DEG2RAD(holderAngles.x), DEG2RAD(holderAngles.y), DEG2RAD(holderAngles.z));

	if (bone_tracker->getBoneNameTracked() == holderBoneName) {
		bone_tracker->setBoneOffset(holderOffset);
		bone_tracker->setBoneRotationOffset(holderRotationOffset);
	}
}

void TCompScytheController::renderDebug() {}

void TCompScytheController::update(float dt) {
	if (_damage > 0.0f && _collider.isValid() && _transform.isValid()) {
		TCompTransform* transform = _transform;
		TCompCollider* collider = _collider;
		Vector3 pos = transform->getPosition();
		Quaternion rot = transform->getRotation();

		PxRigidDynamic * actor = static_cast<PxRigidDynamic *>(collider->actor);
		PxTransform pTransf = actor->getGlobalPose();
		pTransf.p = VEC3_TO_PXVEC3(pos);
		pTransf.q = QUAT_TO_PXQUAT(rot);
		actor->setKinematicTarget(pTransf);

	}
}

void TCompScytheController::registerMsgs() {
	DECL_MSG(TCompScytheController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompScytheController, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompScytheController::onEntityCreated(const TMsgEntityCreated & msg) {
	_collider = getComponent<TCompCollider>();
	_transform = getComponent<TCompTransform>();
	bone_tracker_comp = getComponent<TCompBoneTracker>();

	CEntity * holder = getEntityByName(HolderEntity);
	if (holder)
		player_weapon_manager = holder->getComponent<TCompWeaponManager>();

	// By default we attach it to the back at the beginning.
	start_attached ? attachWeaponToHand() : attachWeaponToHolder();
}

void TCompScytheController::attachWeaponToHand(){
	TCompBoneTracker * bone_tracker = bone_tracker_comp;
	if (!bone_tracker) return;

	// Set weapon to now follow the holder bone. Add the necessary offset.
	bone_tracker->setNewBoneToTrack(handBoneName);
	bone_tracker->setBoneOffset(gripOffset);
	bone_tracker->setBoneRotationOffset(gripRotationOffset);
}

void TCompScytheController::attachWeaponToHolder(){
	TCompBoneTracker * bone_tracker = bone_tracker_comp;
	if (!bone_tracker) return;

	// Set weapon to now follow the holder bone. Add the necessary offset.
	bone_tracker->setNewBoneToTrack(holderBoneName);
	bone_tracker->setBoneOffset(holderOffset);
	bone_tracker->setBoneRotationOffset(holderRotationOffset);
}

void TCompScytheController::enableDamage(float dmg) {
	_damage = dmg;;
}

void TCompScytheController::disableDamage() {
	_damage = 0.0f;
	_enemies.clear();
	SpawnedDrops = false;
}

void TCompScytheController::onTriggerEnter(const TMsgEntityTriggerEnter & trigger_enter) {
	if (_damage > 0.0f) {
		//Check if enemy
		CEntity* e = trigger_enter.h_entity;
		CHandle h = e;
		if (!h.isValid())
			return;

		//else check if new to the vector and needs to be added
		if (std::find(_enemies.begin(), _enemies.end(), trigger_enter.h_entity) == _enemies.end()) {
			TMsgHitDamage msg;
			msg.damage = _damage;
			e->sendMsg(msg);
			_enemies.push_back(trigger_enter.h_entity);
		}

		if (!SpawnedDrops) {
			TCompTransform * transform = e->getComponent<TCompTransform>();
			if(transform)
				CheckIfWeMustSpawnAmmo(*transform);
		}
	}
}

void TCompScytheController::CheckIfWeMustSpawnAmmo(const CTransform & transform) {
	TCompWeaponManager * WeaponManager = player_weapon_manager;
	if (!WeaponManager) return;

	for (int i = 0; i < Weapon::TOTAL_WEAPONS; ++i) {
		TCompWeaponsController * WeaponController = WeaponManager->getWeapon((Weapon)i);
		if (!WeaponController) continue;
		if (WeaponController->getTotalAmmo() == 0) {
			spawnScytheDrops(WeaponController->getName(), transform);
			SpawnedDrops = true;
		}
	}
}


void TCompScytheController::spawnScytheDrops(const std::string & dropType, const CTransform & transform) {
	Vector3 impulseVectorRotated = DirectX::XMVector3Rotate(Vector3(0, 3, 0), transform.getRotation());
	Vector3 dropPositionOffsetRotated = DirectX::XMVector3Rotate(Vector3(0, 2.0, 0.0), transform.getRotation());
	Vector3 finalDropPosition = transform.getPosition() + dropPositionOffsetRotated;
	LogicManager::Instance().SpawnPicksUpGivenDropType(dropType, finalDropPosition, impulseVectorRotated);
}