#include "mcv_platform.h"
#include "comp_weapons_manager.h"
#include "comp_smg_controller.h"
#include "comp_shotgun_controller.h"
#include "comp_rifle_controller.h"
#include "components/ui/bar/comp_reload_ui_controller.h"

#include "entity/common_msgs.h"

DECL_OBJ_MANAGER("weapon_manager", TCompWeaponManager);

void TCompWeaponManager::load(const json& j, TEntityParseContext& ctx) {
	_currentWeapon = j.value("starting_current_weapon", _currentWeapon);
	const json & weaponArray = j["weapon_entities"];

	assert(weaponArray.is_array());
	for (auto & weapon : weaponArray)
		nameWeaponEntities.push_back(weapon.get<std::string>());

	_lineRenderEntityName = j.value("line_render", _lineRenderEntityName);
}

void TCompWeaponManager::debugInMenu()
{
	ImGui::LabelText("Current weapon: ", std::to_string(_currentWeapon).c_str());
	int currWeaponOld = _currentWeapon;
	ImGui::DragInt("Set current weapon: ", &_currentWeapon, 1.0, 0, _maxWeaponsIndx);
	if (currWeaponOld != _currentWeapon)
		setActiveWeaponSlot(currWeaponOld);
}

void TCompWeaponManager::registerMsgs(){
	DECL_MSG(TCompWeaponManager, TMsgEntitiesGroupCreated, onGroupCreated);
}

void TCompWeaponManager::onGroupCreated(const TMsgEntitiesGroupCreated & msg){
	for (auto & entityName : nameWeaponEntities)
		weaponEntitiesH.push_back(getEntityByName(entityName));
	nameWeaponEntities.clear();

	CEntity * ent = weaponEntitiesH[0];
	weaponEntitiesH[0] = ent->getComponent<TCompSmgController>();
	ent = weaponEntitiesH[1];
	weaponEntitiesH[1] = ent->getComponent<TCompShotgunController>();
	ent = weaponEntitiesH[2];
	weaponEntitiesH[2] = ent->getComponent<TCompRifleController>();

	fetchWeapons();
	setActiveWeaponSlotInternal(_currentWeapon);
}

void TCompWeaponManager::update(float dt) {
	fetchWeapons();
	TCompWeaponsController* weapon = getCurrentWeapon();
	weapon->update(dt);
	if (_showCrosshair)
		weapon->drawCrosshair();

	if (!_uiInitialized) {
		weapon->updateAmmoUI();
		weapon->updateWeaponCrosshair(_currentWeapon);
		_uiInitialized = true;
	}

	TCompLineRender* lineRender = getLineRender();
	if (lineRender) {
		// Make tracers gradually disappear
		if (!_tracerDurationTimers.empty()) {
			auto& tracer = _tracerDurationTimers.front();
			float elapsed = tracer.clock.elapsed();
			float goal = tracer.clock.initialTime();
			if (elapsed > goal) {
				lineRender->removeLine(tracer.idx);
				_tracerDurationTimers.pop_front();
			}

			for (auto& tracer : _tracerDurationTimers) {
				auto& vertex = lineRender->getLineVertex(tracer.idx);
				elapsed = tracer.clock.elapsed();
				goal = tracer.clock.initialTime();
				Vector4 color = vertex.color;
				Vector4 colorStart = color;
				color.w = Interpolator::quintOut(tracer.alpha, 0.0f, elapsed / goal);
				// Make the tracer more transparent at the start
				colorStart.w = color.w * tracer.startFadeFactor;
				lineRender->setLineColor(tracer.idx, colorStart, color);
			}
		}

		// Make tracers appear
		if (!_tracerDelayTimers.empty()) {
			auto& tracer = _tracerDelayTimers.front();
			if (tracer.clock.isFinished()) {
				tracer.clock.setTime(tracer.duration);
				tracer.clock.start();
				_tracerDurationTimers.push_back(tracer);
				_tracerDelayTimers.pop_front();
			}
		}
	}
}

void TCompWeaponManager::addTracer(const Vector3 & start, const Vector3 & end, float thickness, const Vector4 & color, float tracerDuration, float tracerDelay, float tracerStartFadeFactor) {
	TCompLineRender* lineRender = getLineRender();
	if (lineRender) {
		Vector4 tempColor = color;
		tempColor.w = 0.0f;
		TTracerTimer tracer;
		tracer.idx = lineRender->drawLine(start, end, thickness, tempColor);
		tracer.duration = tracerDuration;
		tracer.alpha = color.w;
		tracer.clock.setTime(tracerDelay);
		tracer.clock.start();
		tracer.startFadeFactor = tracerStartFadeFactor;
		_tracerDelayTimers.push_back(tracer);
	}
}

void TCompWeaponManager::fetchWeapons(){
	TCompSmgController * smg = weaponEntitiesH[0];
	_weapons[(size_t) Weapon::SMG_WEAPON] = smg;
	TCompShotgunController * shotgun = weaponEntitiesH[1];
	_weapons[(size_t) Weapon::SHOTGUN_WEAPON] = shotgun;
	TCompRifleController * rifle = weaponEntitiesH[2];
	_weapons[(size_t) Weapon::RIFLE_WEAPON] = rifle;
}

void TCompWeaponManager::setActiveWeaponSlotInternal(int slot) {
	TCompWeaponsController * weapon = _weapons[_previousWeapon];
	weapon->attachWeaponToHolder();
	weapon->resetDispersion();
	weapon->setSpecialMode(false);

	TCompWeaponsController * newWeapon = _weapons[_currentWeapon];
	newWeapon->attachWeaponToHand();

	newWeapon->updateAmmoUI();
	newWeapon->updateWeaponIcon(slot);
	newWeapon->updateSpecialIcon();
}

void TCompWeaponManager::changeNextWeapon()
{
	_previousWeapon = _currentWeapon;

	int slot = _currentWeapon + 1;
	slot %= 3;
	setActiveWeaponSlot(slot);
}

void TCompWeaponManager::changePrevWeapon()
{
	_previousWeapon = _currentWeapon;

	int slot = _currentWeapon - 1;
	if (slot == -1)
		slot = _maxWeaponsIndx;
	setActiveWeaponSlot(slot);
}

bool TCompWeaponManager::setActiveWeaponSlot(int slot)
{
	if (_weapons[_currentWeapon] && _weapons[_currentWeapon]->isReloading())
		cancelCurrentReload();

	if (slot < 0 || slot > _maxWeaponsIndx)
		Maths::clamp(slot, 0, _maxWeaponsIndx);

	if (_currentWeapon == slot) {
		TCompWeaponsController * weapon = _weapons[slot];
		weapon->handleChangeToSelf();
		return false;
	}
	else {
		_previousWeapon = _currentWeapon;
		_currentWeapon = slot;
		getCurrentWeapon()->updateWeaponCrosshair(slot);
		return true;
	}
}

void TCompWeaponManager::cancelCurrentReload() 
{
	CEntity * uiReloadBar = getEntityByName("HUD_reload_bar");
	if (uiReloadBar) {
		TCompReloadUIController * uiReloadBarComp = uiReloadBar->getComponent<TCompReloadUIController>();
		if (uiReloadBarComp)
			uiReloadBarComp->abortReload();
	}
	getCurrentWeapon()->resetReload();
}

void TCompWeaponManager::showCroshair(bool isShown) {
	_showCrosshair = isShown;
}

TCompWeaponsController * TCompWeaponManager::getCurrentWeapon()
{
	return _weapons[_currentWeapon];
}

TCompWeaponsController * TCompWeaponManager::getWeapon(Weapon w) {
	return _weapons[(size_t) w];
}
