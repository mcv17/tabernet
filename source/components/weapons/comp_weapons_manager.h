#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "comp_weapons_controller.h"
#include "components/render/comp_line_render.h"

struct TMsgEntitiesGroupCreated;

enum Weapon {
	SMG_WEAPON = 0, SHOTGUN_WEAPON = 1, RIFLE_WEAPON = 2, TOTAL_WEAPONS = 3
};

class TCompWeaponManager : public TCompBase {
	std::vector<std::string> nameWeaponEntities;
	std::vector<CHandle> weaponEntitiesH;
	TCompWeaponsController * _weapons[3];

	CHandle grip; // Entity representing the grip of the weapon (where the hand must be attached to).
	CHandle handEntity; // The hand where the grip must be attached when active.
	CHandle holderEntity; // The place where the weapon must be attached when inactive.(The grip will be in this exact position).

	int _currentWeapon = 0;
	// Used when we change a weapon, so the animation is of the weapon changed
	int _previousWeapon = 0;
	int _maxWeaponsIndx = 2;

	bool _showCrosshair;
	bool _uiInitialized = false;
	// Tracers
	struct TTracerTimer {
		size_t idx;
		float duration;
		float alpha;
		CClock clock;
		float startFadeFactor;
	};
	std::list<TTracerTimer> _tracerDelayTimers;
	std::list<TTracerTimer> _tracerDurationTimers;
	std::string _lineRenderEntityName = "";

	DECL_TCOMP_ACCESS(_lineRenderEntityName, TCompLineRender, LineRender);


	void fetchWeapons();

public:
	void load(const json& j, TEntityParseContext & ctx);
	void debugInMenu();
	void update(float dt);

	void changeNextWeapon();
	void changePrevWeapon();
	//Returns true if we are changing to another weapon
	bool setActiveWeaponSlot(int slot);
	void setActiveWeaponSlotInternal(int slot);

	void cancelCurrentReload();

	void showCroshair(bool isShown = true);

	void addTracer(const Vector3& start, const Vector3& end, float thickness, const Vector4& color, float tracerDuration, float tracerDelay, float tracerStartFadeFactor);

	TCompWeaponsController* getCurrentWeapon();
	int getCurrentWeaponID() { return _currentWeapon; }
	int getPreviousWeaponID() { return _previousWeapon; }
	TCompWeaponsController * getWeapon(Weapon w);

	static void registerMsgs();
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);

	DECL_SIBLING_ACCESS();
};