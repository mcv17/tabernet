#pragma once

#include "components/common/comp_base.h"

#include "modules/module_physics.h"
#include "utils/phys_utils.h"

class TCompTransform;
struct TMsgEntitiesGroupCreated;

class TCompWeaponsController : public TCompBase {

protected:
	static const float WORLD_TO_SCREEN_FACTOR;
	
	/* General. */
	std::string _weaponName; // For debugging purposes only.
	std::string jsonStats; // Stats of the weapon.
	bool starting_weapon;

	/* Player camera handles. Used for calculating shooting position. */

	// Handles of the main player camera.
	CHandle cameraPlayerH;
	CHandle cameraTransformH;
	CHandle cameraCameraH;

	// Handle for the hud to show information.
	CHandle hudHandle;

	/* Weapon bones. */

	// Name of the entity carrying the weapon. Used for fetching the skeleton to where weapons should be at.
	std::string name_entity_carrying_weapon;

	// Bone tracker component of the weapon. Used for setting the entity
	// to track the bone of the player hand or one of the bones from the back.
	CHandle bone_tracker_comp;

	/* Grip variables. Used when weapon is attached to the hand of the player. */
	std::string handBoneName; // Bone to attach weapon to. We will attach the grip of the weapon to it. That's why we need the two offsets below.
	Vector3 gripOffset; // Offset where the weapon will be positioned based on the hand bone. We don't have bones
	// for the weapons so this is the best method we can use to add the gun where it needs to be.
	Quaternion gripRotationOffset; // Rotation offset applied to the weapon when attached to the hand.
	// Used to correct bones rotation not being correct.

	/* Holder variables. Used when weapon is attached to the back or somewhere else. */
	std::string holderBoneName; // Bone to attach weapon to when being carried in the back or somewhere else.
	Vector3 holderOffset; // Offset where the weapon will be based on the holder bone.
	Quaternion holderRotationOffset; // Rotation offset for the holder.

	/* Cannon offset */

	// Where the bullet comes from based on the transform of the entity.
	Vector3 offsetGunCannon = Vector3(0.0f, 0.0f, 1.65f);

	// Weapon stats.
	struct WeaponStats {
		float damage;
		float rof; // rate of fire
		float dispersion; // how accurate is the weapon
		float effectiveDistance; // how far can the bullets do 100% damage
		float maxDistance;  // how far can the bullets go
		std::function<float(float, float, float)> falloff; // interpolator applied for loss of damage over distance
		float reloadTime; // time it takes to reload this weapon
		float rateOfDispersion; // dispersion per shooting
		float dispersionRecover; // dispersion recover while not shooting
		float tracerDuration = 0.2f, tracerDelay = 0.05f, tracerThickness = 0.05f;
		float tracerStartFadeFactor = 0.2f; // factor that controls how transparent is the start of the tracer
		Vector4 tracerColor;
	} _stats;

	float _activeReloadOffset; // from 0..1 point in time to hit 
	float _activeReloadPenaltyTime; // Seconds to extend reload for failing the active reload
	CClock _activeReloadTimer; // To know if player hits the point
	bool _activeReloadTried; // If player already tried this
	float _timeToNextShot; // float future time when we can shoot the next bullet
	int _capacity; // Mag capacity
	int _currentAmmo; // Current mag ammo
	int _storedAmmo; // Ammo not in mag
	static int _maxAltCapacity; // Maximum alternative fire capacity in all weapons (shared)
	static int _currentAltAmmo; // Current alternative fire ammo in all weapons (shared)
	bool _isReloading = false;
	float _curDispersion = 0.0f;
	float _baseDispersion;
	bool _specialModeActivated = false;

	// Camera shake.
	CHandle cameraPlayerOutputH;
	std::string CameraPlayerName;
	float shakePerBullet;
	float maxShake;

	// UI
	VEC4 _specialActivatedColor = VEC4(1.0f, 0.847f, 0.0f, 1.0f);

	/* Weapon LUA Functions */
	std::string OnBulletHitFXFunction; // When a bullet from a weapon hits a surface. Controls spawning of fx. Receives position of impact, normal and surface hit.
	std::string OnBloodSplatterFxFunction; // When a ray representing a splatter of blood hits the ground. Receives position of impact, normal and surface hit.

	// Protected functions.
	bool checkCamera(); // Fetch camera handles. Returns true if everything valid, false ow.
	bool checkHUD(); // Fetch hud handles. Returns true if everything valid, false ow.
	virtual void handleSpecialModeChange() {}

	// Spawn particle functions.
	bool hitEnemy(const physx::PxRaycastBuffer & bulletHitData);
	void throwBloodSplatterRay(const physx::PxRaycastBuffer & bulletHitData, const Vector3 & rayDir);
	std::string getSurfaceHit(CHandle entityHit);

	void loadWeapon(const json& j, WeaponStats& stats, WeaponStats& defaultStats);

	// On message functions.
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);

	

public:
	void load(const json & j, TEntityParseContext & ctx);
	void debugInMenu();
	virtual void update(float dt) = 0;

	// Weapon functions.
	// Returns true if can and has shooted this frame, else false
	bool shoot();
	virtual bool shootNormal() = 0;
	virtual bool shootSpecial() = 0;
	virtual bool reload() = 0;
	void resetReload();
	void resetAmmo();		// Takes ammo from stored ammo and puts it in mag
	void resetDispersion();
	void resetMaxDispersion();
	Vector3 getCannonPosition(const TCompTransform * weaponTransform);
	void attachWeaponToHand();
	void attachWeaponToHolder();
	virtual void drawCrosshair() {}
	virtual void handleChangeToSelf() {}
	void spawnParticlesAndDecalOnHit(CHandle entityHit, const physx::PxRaycastBuffer & bulletHitData, const Vector3 & rayDir);
	void sendCameraShakeOnShot();

	/* Getters and setters. */
	std::string getName() { return _weaponName; }
	int getCurrentAmmo() { return _currentAmmo; }
	int getStoredAmmo() { return _storedAmmo; }
	int getTotalAmmo() { return _currentAmmo + _storedAmmo; }
	int getCapacity() { return _capacity; }
	int getMaxAltCapacity() { return _maxAltCapacity; }
	float getReloadTime() { return _stats.reloadTime; }
	bool isReloading() { return _isReloading; }
	float getMaxDispersion() { return _stats.dispersion; }
	float getBaseMaxDispersion() { return _baseDispersion; }
	bool isSpecialModeActivated() { return _specialModeActivated; }
	void increaseStoredAmmo(int qty) { _storedAmmo += qty; }
	void setStoredAmmo(int qty) { _storedAmmo = qty; }
	void setCurrentAmmoToMagCapacity() { _currentAmmo = _capacity; }
	void setMaxDispersion(float dispersion) { _stats.dispersion = dispersion; }
	void setSpecialMode(bool state);
	void switchSpecialMode();
	//UI
	void updateAmmoUI();
	void updateWeaponIcon(int weaponSlot);
	void updateWeaponCrosshair(int weaponSlot);
	virtual void updateSpecialIcon();
	virtual void updateSpecialIconActivated(bool isSpecialModeActivated) = 0;

	static int getCurrentAltAmmo() { return _currentAltAmmo; }
	static void useSpecialAmmo() { _currentAltAmmo = std::max(0, _currentAltAmmo - 1); }
	static void increaseSpecialAmmo(int qty) { _currentAltAmmo = std::min(_maxAltCapacity, _currentAltAmmo + qty); }

	// Dump weapon stats.
	std::string dumpWeaponStats();
};