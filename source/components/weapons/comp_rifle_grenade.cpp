#include "mcv_platform.h"
#include "comp_rifle_grenade.h"
#include "logic/logic_manager.h"
#include "comp_rifle_controller.h"
#include "components/scenario/comp_explodable.h"
#include "components/render/comp_trail_render.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "../common/comp_tags.h"

DECL_OBJ_MANAGER("rifle_grenade", TCompRifleGrenade);

void TCompRifleGrenade::load(const json& j, TEntityParseContext& ctx) {
	
	_hitDamage = j.value("power", _hitDamage);
	_hitInnerRadius = j.value("inner_radius", _hitInnerRadius);
	_hitOuterRadius = j.value("outer_radius", _hitOuterRadius);
	_throwForce = j.value("throw_force", _throwForce);
}

void TCompRifleGrenade::debugInMenu()
{
	TCompBase::debugInMenu();
	ImGui::Separator();
	ImGui::Text("Grenade force: ");
	ImGui::DragFloat("", &_throwForce, 10.0f, 1.0f, 10000.0f);
}

void TCompRifleGrenade::renderDebug()
{

}

void TCompRifleGrenade::update(float dt) {
	TCompTransform* grenadeTrans = _transformHandle;

	Vector3 pos = grenadeTrans->getPosition();
	if (_isFirstUpdate) {
		_isFirstUpdate = false;
		TCompTrailRender* trail = _trailHandle;
		trail->unlockPosistion();
	}
	else
		grenadeTrans->lookAt(pos, pos + (pos - _lastPosition));

	_lastPosition = pos;
}


void TCompRifleGrenade::registerMsgs() {
	DECL_MSG(TCompRifleGrenade, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompRifleGrenade, TMsgEntitiesGroupCreated, onGroupCreated);
	DECL_MSG(TCompRifleGrenade, TMsgEntityCollisionEnter, onCollisionEnter);
	DECL_MSG(TCompRifleGrenade, TMsgEntityTriggerEnter , onTriggerEnter);
}

void TCompRifleGrenade::onEntityCreated(const TMsgEntityCreated & msg) {

	if (CEntity * rifleEntity = getEntityByName("Rifle-Weapon")) {
		_rifleHandle = CHandle(rifleEntity);
		// Rifle components
		TCompTransform* rifleTrans = rifleEntity->getComponent<TCompTransform>();
		TCompRifleController* rifleController = rifleEntity->getComponent<TCompRifleController>();
		// Grenade components
		TCompTransform* grenadeTrans = getComponent<TCompTransform>();
		TCompCollider* grenadeCol = getComponent<TCompCollider>();
		
		if (rifleTrans && rifleController && grenadeTrans && grenadeCol) {
			// Set grenade transform to rifle cannon 
			if (CEntity* camera = getEntityByName("Camera-Player")) {
				if (TCompTransform * cameraTrans = camera->getComponent<TCompTransform>()) {

					CTransform t;
					VEC3 cannonPos = rifleController->getCannonPosition(rifleTrans);

					t.setPosition(cannonPos);
					t.setRotation(rifleTrans->getRotation());

					grenadeCol->actor->setGlobalPose(toPxTransform(t));
					// Throw grenade
					if (physx::PxRigidDynamic * dyna = grenadeCol->actor->is<physx::PxRigidDynamic>()) {
						physx::PxVec3 forceVec = VEC3_TO_PXVEC3(VEC3(cameraTrans->getFront() * _throwForce));
						dyna->addForce(forceVec, PxForceMode::eIMPULSE); //TODO: follow the curve
					}

					grenadeTrans->setPosition(cannonPos);
					grenadeTrans->lookAt(cannonPos, cannonPos + cameraTrans->getFront());
				}
			}

			_transformHandle = grenadeTrans;
		}
	}

}

void TCompRifleGrenade::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	CHandle hTrail = msg.ctx.findEntityByName("rifle_grenade_trail");
	if (hTrail.isValid()) {
		CEntity* trailEntity = hTrail;
		_trailHandle = trailEntity->getComponent<TCompTrailRender>();
	}
}

void TCompRifleGrenade::onCollisionEnter(const TMsgEntityCollisionEnter& msg){
	// Avoid colliding with the rifle
	if (msg.h_entity == _rifleHandle) return;
	
	TCompTransform* t = _transformHandle;
	CTransform pos;
	pos.setPosition(t->getPosition());

	CEntity* e = EnginePool.spawn(PoolType::Explosion, pos.getPosition(), 1.0f);
		
	TCompExplodable* exp = e->getComponent<TCompExplodable>();
	exp->setRadius(_hitInnerRadius, _hitOuterRadius);
	exp->setPower(_hitDamage);
	exp->setPosition(t->getPosition());
	exp->explode();

	getEntity().destroy();
}

void TCompRifleGrenade::onTriggerEnter(const TMsgEntityTriggerEnter& msg) {
	// Avoid colliding with the rifle
	if (msg.h_entity == _rifleHandle) return;
	
	CEntity* e = msg.h_entity;
	
	if (!e) return;
	
	if (TCompTags* c_tags = e->getComponent<TCompTags>()) {
		if (!c_tags->hasTag(c_tags->getTagValue("enemy")))
			return;
	}

	TCompTransform* t = _transformHandle;
	CTransform pos;
	pos.setPosition(t->getPosition());

	e = EnginePool.spawn(PoolType::Explosion, pos.getPosition(), 1.0f);

	TCompExplodable* exp = e->getComponent<TCompExplodable>();
	exp->setRadius(_hitInnerRadius, _hitOuterRadius);
	exp->setPower(_hitDamage);
	exp->setPosition(t->getPosition());
	exp->explode();

	getEntity().destroy();
}
