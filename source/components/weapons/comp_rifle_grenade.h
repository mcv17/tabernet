#pragma once
#include "entity/entity.h"
#include <geometry\curve.h>
#include "components\common\comp_collider.h"
#define AUDIO_RIFLE_SHOOT "event:/rifle_grenade"

class TCompRifleGrenade : public TCompBase {
	DECL_SIBLING_ACCESS();

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);

	/** Variables **/
	// Data
	float _hitDamage = 200.0f;
	float _hitInnerRadius = 2.0f;
	float _hitOuterRadius = 5.0f;
	float _throwForce = 20.0f;
	std::vector<VEC3> _travelKnots;
	// Helpers
	CHandle _colliderHandle;
	CHandle _transformHandle;
	CHandle _rifleHandle;
	CHandle _trailHandle;
	bool _isDead = false;
	bool _isFirstUpdate = true;
	Vector3 _lastPosition;

public:
	TCompRifleGrenade() {}
	
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);
	static void registerMsgs();

	void onCollisionEnter(const TMsgEntityCollisionEnter& msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter& msg);
};