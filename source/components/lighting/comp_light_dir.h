#pragma once

#include "comp_base_light.h"

class CCamera;
class CTexture;
class CRenderToTextureArray;
struct TMsgEntityCreated;

class TCompCamera;
struct TCompCulling;

/* This class represents an Directional Light that comes from the sun. Therefore,
only the direction of the light is actually used. The position is irrelevant. 
This type of class implements CSM to support shadows for high view distances. */

class TCompLightDir : public TCompBaseLight{
	DECL_SIBLING_ACCESS();

	// Handles for the transform, camera and culling component of the light.
	CHandle cameraCompH;
	CHandle cullingCompH;

	// Stores the values of the light parameters that are related
	// to the camera. Will be loaded on entity create.
	json lightProjectionParameters;

	// Shadows params
	bool              shadows_enabled = false;    // Dynamic
	bool              casts_shadows = false;      // Static
	int               shadows_resolution = 256;
	float             shadows_step = 1.65f;
	CRenderToTextureArray * shadows_rt = nullptr;

	/* Cascade variables. */
	Matrix cascade_view_projections[NUM_CASCADES_SHADOW_MAP]; // Stores the view matrix and projection of each cascade for uploading to the shader. Used for sampling shadows.
	
	// For the CSM shader.
	Matrix globalShadows;
	Vector4 cascadeOffsets[NUM_CASCADES_SHADOW_MAP]; // Array of offset of each cascade.
	Vector4 cascadeScales[NUM_CASCADES_SHADOW_MAP]; // Array of scales of each cascade.

	// Used for better optimizing spheres positioning. Fast fix. For first cascade we move x units in front.
	// Next cascades are offsets based on their radius.
	float centerOffsets[NUM_CASCADES_SHADOW_MAP] = { 4.0, 0.65, 0.60 };

	float clipSpaceCascadeEnds[NUM_CASCADES_SHADOW_MAP]; // Array of distances between cascades of the camera.

	// Debug variables.
	float radiusDebug[NUM_CASCADES_SHADOW_MAP];
	Matrix shadowCenter[NUM_CASCADES_SHADOW_MAP];
	Matrix view_pos[NUM_CASCADES_SHADOW_MAP];
	Matrix cascade_volume[NUM_CASCADES_SHADOW_MAP];
	Matrix culling_volume[NUM_CASCADES_SHADOW_MAP];

public:
	~TCompLightDir();

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();
	static void declareInLua();
	
	void activate();
	void generateShadowMap(CCamera & renderCamera);
	void doCSM(CCamera & renderCamera);

private:

	/* Creates a global shadow matrix. */
	Matrix MakeGlobalShadowMatrix(CCamera & renderCamera, const Vector3 & lightDir);

	/* Returns the cascade offset and scale. Used for correct picking of cascades in the shader. */
	void GetCascadeOffsetAndScale(int cascadeIdx, const Matrix & globalShadowMatrix, CCamera & shadowCamera);

	/* Move by texels. */
	void createShadowRoundedMatrix(CCamera & shadowCamera);

	/* Culling methods. */
	void updateCulling(CCamera & lightCameraWithShadowValues, TCompCulling * cullingComp,
		int cascadeIdx, const Vector3 & maxExtents, const Vector3 & minExtents, const Vector3 & cascadeExtents);

	void onEntityCreated(const TMsgEntityCreated & msg);
};
