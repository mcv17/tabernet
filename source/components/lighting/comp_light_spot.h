#pragma once

#include "comp_base_light.h"

class CTexture;
class CRenderToTexture;
struct TMsgEntityCreated;

class TCompLightSpot : public TCompBaseLight {
	DECL_SIBLING_ACCESS();

	// Camera and culling component.
	CHandle cameraCompH;
	CHandle cullingCompH;

	// Stores the values of the light parameters that are related
	// to the camera. Will be loaded on entity create.
	json lightParameters;

	// Projected texture.
	const CTexture* projector = nullptr;

	// Shadows params
	bool              shadows_enabled = false;    // Dynamic
	bool              casts_shadows = false;      // Static
	int               shadows_resolution = 256;
	float             shadows_step = 1.65f;
	CRenderToTexture* shadows_rt = nullptr;

public:
	void debugInMenu();
	void renderDebug();
	void load(const json & j, TEntityParseContext& ctx);
	void update(float dt);
	static void registerMsgs();

	void activate();
	void generateShadowMap();
	Matrix getViewProjection();

private:
	void onEntityCreated(const TMsgEntityCreated & msg);
};