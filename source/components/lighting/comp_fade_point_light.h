#pragma once
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"

class TCompFadePointLight : public TCompBase {

public:

	static void registerMsgs();

	DECL_SIBLING_ACCESS();

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	void setTimeToFade(float time) { _timeToFade = time; }
	void setDestroyEntityWhenFinished(float destroy) { _destroyEntityWhenFinished = destroy; }
	void setInterpolator(const std::string& interpolator);

	void reset();

private:

	CHandle _hCompLight;
	CClock _fadeTimer;
	std::string _interpolatorName = "linear";
	std::function<float(float, float, float)> _interpolator;
	float _timeToFade = 1.0f;
	float _baseIntensity;
	bool _destroyEntityWhenFinished = false;

	void onEntityCreated(const TMsgEntityCreated& msg);

};