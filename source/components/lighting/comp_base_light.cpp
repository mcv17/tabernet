#include "mcv_platform.h"
#include "comp_base_light.h"
#include "components/common/comp_transform.h"
#include "engine.h"

DECL_OBJ_MANAGER("base_light", TCompBaseLight);

void TCompBaseLight::debugInMenu() {
	ImGui::Spacing(0, 5);

	ImGui::DragFloat("Intensity", &intensity, 0.01f, 0.f, 100000.f);
	ImGui::ColorEdit3("Color", &color.x);

	ImGui::Spacing(0, 5);
	ImGui::Separator();
}

void TCompBaseLight::load(const json& j, TEntityParseContext& ctx) {
	if (j.count("color"))
		color = loadVector4(j, "color");
	intensity = j.value("intensity", intensity);
}

void TCompBaseLight::renderDebug() {}

void TCompBaseLight::update(float delta) {}

void TCompBaseLight::setPosition(const Vector3 & nPosition) {
	TCompTransform * transform = transform_h;
	if (!transform) return;
	transform->setPosition(nPosition);
}