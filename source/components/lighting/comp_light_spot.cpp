#include "mcv_platform.h"
#include "comp_light_spot.h"

#include "../common/comp_transform.h"
#include "../camera/comp_camera.h"
#include "../common/comp_culling.h"

#include "render/textures/texture.h" 
#include "render/textures/render_to_texture.h" 
#include "render/render_manager.h" 

#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("light_spot", TCompLightSpot);

DXGI_FORMAT readFormat(const json& j, const std::string& label);

void TCompLightSpot::registerMsgs() {
	DECL_MSG(TCompLightSpot, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompLightSpot, TMsgToogleComponent, onToggleComponent);
}

void TCompLightSpot::onEntityCreated(const TMsgEntityCreated & msg) {
	transform_h = getComponent<TCompTransform>();
	cameraCompH = getComponent<TCompCamera>();
	cullingCompH = getComponent<TCompCulling>();

	assert(cameraCompH.isValid() && "Attention light directions require a camera component!");
	TCompCamera * camera = cameraCompH;
	TCompCulling * cullingComp = cullingCompH;
	TEntityParseContext ctx;

	// Load parameters for camera.
	if(camera)
		camera->load(lightParameters, ctx);
	if (cullingComp)
		cullingComp->setActive(false);
}

void TCompLightSpot::load(const json& j, TEntityParseContext& ctx) {
	// Store information for the camera component.
	lightParameters = j;
	lightParameters["visible_in_debug"] = false; // we don't want to use them in the camera manager.
	lightParameters["ortho_centered"] = true; // If the spot was orthographic, they must be centered.

	// Information about the light properties. I.e, color, intensity...
	TCompBaseLight::load(j, ctx);

	// Load a projector texture if we have one.
	std::string projector_name = j.value("projector", "data/textures/lights/projectors/null_projection.dds");
	projector = EngineResources.getResource(projector_name)->as<CTexture>();


	// Check if we need to allocate a shadow map.
	casts_shadows = j.value("casts_shadows", false);
	if (casts_shadows) {
		shadows_step = j.value("shadows_step", shadows_step);
		shadows_resolution = j.value("shadows_resolution", shadows_resolution);
		auto shadowmap_fmt = readFormat(j, "shadows_fmt");
		assert(shadows_resolution > 0);
		shadows_rt = new CRenderToTexture;
		// Make a unique name to have the Resource Manager happy with the unique names for each resource
		char my_name[64];
		sprintf(my_name, "shadow_map_%08x", CHandle(this).asUnsigned());

		// Added a placeholder Color Render Target to be able to do a alpha test when rendering
		// the grass.
		bool is_ok = shadows_rt->create(my_name, shadows_resolution, shadows_resolution, DXGI_FORMAT_R8G8B8A8_UNORM, shadowmap_fmt);
		assert(is_ok);
	}
	shadows_enabled = casts_shadows;
}

void TCompLightSpot::debugInMenu() {
	TCompBaseLight::debugInMenu();
	ImGui::Spacing(0, 5);

	ImGui::DragFloat("Shadow step Size", &shadows_step, 0.01f, 0.f, 5.f);

	ImGui::Spacing(0, 5);

	TCompCamera * camera = cameraCompH;
	if (!camera)return;
	camera->debugInMenu();

	ImGui::Spacing(0, 5);
	ImGui::Text("Shadow result");

	if(shadows_rt)
		shadows_rt->renderInMenu();
}

void TCompLightSpot::renderDebug() {
	TCompCamera * camera = cameraCompH;
	if (!camera) return;
	camera->debugInMenu();
}

void TCompLightSpot::update(float dt) {}

// Updates the Shader Cte Light with MY information
void TCompLightSpot::activate() {
	TCompCamera * camera = cameraCompH;
	if (!camera) return;

	projector->activate(TS_PROJECTOR);

	// To avoid converting the range -1..1 to 0..1 in the shader
	// we concatenate the view_proj with a matrix to apply this offset
	MAT44 mtx_offset = MAT44::CreateScale(VEC3(0.5f, -0.5f, 1.0f))
		* MAT44::CreateTranslation(VEC3(0.5f, 0.5f, 0.0f));

	ctes_light.LightColor = color;
	ctes_light.LightIntensity = intensity;
	ctes_light.LightPosition = camera->getEye();
	ctes_light.LightRadius = camera->getFar();
	ctes_light.LightViewProjOffset = camera->getViewProjection() * mtx_offset;
	ctes_light.LightFront = camera->getFront();

	// If we have a ZTexture, it's the time to activate it
	if (shadows_rt) {
		ctes_light.LightShadowInverseResolution = 1.0f / (float)shadows_rt->getWidth();
		ctes_light.LightShadowStep = shadows_step;
		ctes_light.LightShadowStepDivResolution = shadows_step / (float)shadows_rt->getWidth();
		ctes_light.LightHasShadows = true;
		assert(shadows_rt->getZTexture());
		shadows_rt->getZTexture()->activate(TS_LIGHT_SHADOW_MAP);
	}
	else
		ctes_light.LightHasShadows = false;

	ctes_light.updateGPU();
}

void TCompLightSpot::generateShadowMap() {
	if (!shadows_rt || !shadows_enabled)
		return;

	TCompCamera * camera = cameraCompH;
	TCompCulling * cullingComp = cullingCompH;
	if (!cullingComp) return;

	// In this slot is where we activate the render targets that we are going
	// to update now. You can't be active as texture and render target at the
	// same time
	CTexture::setNullTexture(TS_LIGHT_SHADOW_MAP);

	// Set the camera of the light as the rendering camera. As we will render from it.
	CRenderManager::get().setEntityCamera(cameraCompH.getOwner());

	// Before starting we get sure to activate the culling component if the light has one.
	// We also calculate the culling view matrix we will use during the culling process.
	// This view will be in the position of the light and extend from it until the zfar of the projection.
	if (cullingComp) {
		cullingComp->setActive(true);
		cullingComp->update(0.0f); // Also update the culling so we cull from the new perspective.
	}

	// Activate the render to texture and draw.
	CGpuScope gpu_scope(shadows_rt->getName().c_str());
	shadows_rt->activateRT();
	{
		{
			PROFILE_FUNCTION("Clear&SetCommonCtes");
			shadows_rt->clearDepthBuffer();
			// We are going to render the scene from the light position & orientation
			activateCamera(*camera, shadows_rt->getWidth(), shadows_rt->getHeight());
		}
		{
			PROFILE_FUNCTION("Render_Shadow_Maps");
			CRenderManager::get().render(CATEGORY_SHADOWS);
		}
	}

	if (cullingComp)
		cullingComp->setActive(false);
}


Matrix TCompLightSpot::getViewProjection() {
	TCompCamera * camera = cameraCompH;
	if (!camera) Matrix::Identity;
	return camera->getViewProjection();
}