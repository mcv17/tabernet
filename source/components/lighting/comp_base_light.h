#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "geometry/camera.h"

class TCompBaseLight : public TCompBase {
protected:
	CHandle			transform_h;
	VEC4            color = VEC4(1, 1, 1, 1);
	float           intensity = 1.0f;

public:
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float delta);

	float getIntensity() { return intensity; }
	VEC4 getColor() { return color; }

	void setPosition(const Vector3 & nPosition);
	void setIntensity(float new_intensity) { intensity = new_intensity; }
	void setColor(const VEC4 & new_color) { color = new_color; }
	virtual void setRange(float nRange) {}
};