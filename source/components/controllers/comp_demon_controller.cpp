#include "mcv_platform.h"
#include "comp_demon_controller.h"
#include "engine.h"
#include "modules/module_physics.h"
#include "components/common/comp_name.h"

DECL_OBJ_MANAGER("movement_demon_controller", TCompDemonController);

const float TCompDemonController::DEFAULT_HEIGHT = 1.0f;
const float TCompDemonController::DEFAULT_RADIUS = 1.0f;

void TCompDemonController::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Checkbox("Gravity applied:", &_gravity);
}

void TCompDemonController::renderDebug() {}

void TCompDemonController::load(const json& j, TEntityParseContext& ctx) {
	_gravity = j.value("gravity", _gravity);
}

void TCompDemonController::update(float dt) {
	simulate(dt);
}

void TCompDemonController::registerMsgs() {
	DECL_MSG(TCompDemonController, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompDemonController, TMsgLogicStatus, onLogicStatus);
}

void TCompDemonController::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompDemonController::resize(float height) {
	physx::PxCapsuleController * controller = getController();
	if(controller)
		controller->resize(height);
}

void TCompDemonController::setRadius(float radius) {
	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->setRadius(radius);
}

void TCompDemonController::setSlopeLimit(float slopeLimit) {
	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->setSlopeLimit(slopeLimit);
}

void TCompDemonController::setStepOffset(float stepOffset) {
	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->setStepOffset(stepOffset);
}

void TCompDemonController::move(const VEC3 & movement) {
	_displacementDelta = VEC3_TO_PXVEC3(movement);
}

void TCompDemonController::setRotation(const VEC3 & rotation){
	TCompTransform* transform = getComponent<TCompTransform>();;
	if (!transform) return;
	transform->setAngles(rotation.x, rotation.y);
}

void TCompDemonController::setRotation(float yaw, float pitch, float roll){
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;
	transform->setAngles(yaw, pitch, roll);
}

void TCompDemonController::setPosition(const VEC3 & position) {
	if (!active) return;

	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->setPosition(VEC3_TO_PXEXTVEC3(position));
}

void TCompDemonController::simulate(float dt) {
	if (!active) return;

	if(_gravity) applyGravity();
	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->move(_displacementDelta, 0.001f, dt, physx::PxControllerFilters());
	_displacementDelta = physx::PxVec3(physx::PxZero);
}

void TCompDemonController::applyGravity() {
	_displacementDelta += EnginePhysics.getScene()->getGravity();
}

physx::PxCapsuleController * TCompDemonController::getController() {
	TCompCollider * col = getComponent<TCompCollider>();
	if (!col)
		return nullptr;
	return col->controller;
}