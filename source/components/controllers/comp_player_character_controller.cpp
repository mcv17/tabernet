#include "mcv_platform.h"
#include "comp_player_character_controller.h"
#include "engine.h"
#include "modules/module_physics.h"
#include "components/common/comp_name.h"

DECL_OBJ_MANAGER("player_character_controller", TCompPlayerCharacterController);

void TCompPlayerCharacterController::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::LabelText("Current movement speed", "%f", _curMovmentSpeed);
	ImGui::LabelText("Current rotation speed", "%f", _curRotationSpeed);
	ImGui::LabelText("Current velocity", "%f %f %f", _curVelocity.x, _curVelocity.y, _curVelocity.z);

	ImGui::Separator();

	ImGui::DragFloat("Normal speed", &_playerSpeedNormal, 0.001f, 0.f, 100000.f);
	ImGui::DragFloat("Normal rotation Speed", &_rotationSpeedNormal, 0.001f, 0.f, 100000.f);
	ImGui::DragFloat("Speed multiplier backwards", &_playerSpeedMultiplierBackwards, 0.001f, 0.f, 100000.f);
	ImGui::DragFloat("Sprinting speed", &_rotationSpeedSprinting, 0.001f, 0.f, 100000.f);
	ImGui::DragFloat("Sprinting rotation Speed", &_rotationSpeedSprinting, 0.001f, 0.f, 100000.f);
	ImGui::DragFloat("Aiming speed", &_playerSpeedAiming, 0.001f, 0.f, 1000.f);
	ImGui::DragFloat("Aiming rotation Speed", &_rotationSpeedAiming, 0.001f, 0.f, 100000.f);
	ImGui::DragFloat("Hip rotation Speed", &_rotationSpeedHipShooting, 0.001f, 0.f, 100000.f);
	if (ImGui::DragFloat("StepOffset", &_stepOffset, 0.01f, 0.0f, 1.0f))
		setStepOffset(_stepOffset);
}

void TCompPlayerCharacterController::renderDebug() {}

void TCompPlayerCharacterController::load(const json& j, TEntityParseContext& ctx) {
	_playerSpeedNormal = j.value("playerSpeedNormal", _playerSpeedNormal);
	_rotationSpeedNormal = j.value("rotationSpeedNormal", _rotationSpeedNormal);
	_playerSpeedMultiplierBackwards = j.value("playerSpeedMultiplierBackwards", _playerSpeedMultiplierBackwards);
	_rotationSpeedSprinting = j.value("rotationSpeedSprinting", _rotationSpeedSprinting);
	_playerSpeedSprinting = j.value("playerSprintingSpeed", _playerSpeedSprinting);
	_playerSpeedAiming = j.value("playerAimingSpeed", _playerSpeedAiming);
	_rotationSpeedAiming = j.value("rotationSpeedAiming", _rotationSpeedAiming);
	_rotationSpeedHipShooting = j.value("rotationSpeedHipShooting", _rotationSpeedHipShooting);
	_stepOffset = j.value("step_offset", _stepOffset);

	_filterData.word0 = EnginePhysics.loadFilter(j, "mask", EnginePhysics.ScenarioLow | EnginePhysics.Trigger | EnginePhysics.DynamicScenario);
}

void TCompPlayerCharacterController::update(float dt) {
	physx::PxVec3 movement = _displacement * dt;
	updateInteractor(movement);
	applyGravity(movement, dt);
	simulate(movement, dt);
	updateRotation();
}

void TCompPlayerCharacterController::registerMsgs() {
	DECL_MSG(TCompPlayerCharacterController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompPlayerCharacterController, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompPlayerCharacterController, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompPlayerCharacterController, TMsgMovementSpeedMultiplier, onMovementSpeedMult);
}

void TCompPlayerCharacterController::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompPlayerCharacterController::declareInLua() {
	EngineScripting.declareComponent<TCompPlayerCharacterController>
		(
			"TCompPlayerCharacterController",
			"move", &TCompPlayerCharacterController::move,
			"teleport", &TCompPlayerCharacterController::teleport,
			"isGrounded", &TCompPlayerCharacterController::isGrounded
			);
}

void TCompPlayerCharacterController::resize(float height) {
	getController()->resize(height);
}

void TCompPlayerCharacterController::setRadius(float radius) {
	getController()->setRadius(radius);
}

void TCompPlayerCharacterController::setSlopeLimit(float slopeLimit) {
	getController()->setSlopeLimit(slopeLimit);
}

void TCompPlayerCharacterController::setStepOffset(float stepOffset) {
	getController()->setStepOffset(stepOffset);
}

void TCompPlayerCharacterController::move(VEC3 disp) {
	_displacement += VEC3_TO_PXVEC3(disp);
}

void TCompPlayerCharacterController::teleport(VEC3 pos) {
	if (!active) return;

	getController()->setFootPosition(VEC3_TO_PXEXTVEC3(pos));
	_isGrounded = false;
}

void TCompPlayerCharacterController::simulate(const physx::PxVec3 & movement, float dt) {
	if (!active) return;

	if (movement != physx::PxZero) {
		auto flags = getController()->move(movement, 0.001f, dt, _filters);
		_isGrounded = flags.isSet(physx::PxControllerCollisionFlag::eCOLLISION_DOWN);
	}

	// Update _lastPosition so we can calculate the movement speed and velocity of the controller
	TCompTransform* transform = _transform;
	Vector3& newPos = transform->getPosition();
	if (_lastPosition == newPos) {
		_curMovmentSpeed = 0.0f;
		_curVelocity = Vector3::Zero;
	}
	else {
		_curVelocity = (newPos - _lastPosition) / _lastDeltaTime;
		_curMovmentSpeed = Vector3::Distance(Vector3::Zero, _curVelocity);
		_lastPosition = newPos;
	}
	_lastDeltaTime = dt;
	_displacement = physx::PxVec3(physx::PxZero);
}

void TCompPlayerCharacterController::updateRotation() {
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;

	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	transform->setAngles(_currentYaw, pitch);
	_curRotationSpeed = abs(_currentYaw - yaw);
}

void TCompPlayerCharacterController::applyGravity(physx::PxVec3 & movement, float dt) {
	if (_isGrounded)
		_currentGravity = 0.0f;
	else
		_currentGravity = std::min(_currentGravity + dt * 0.05f, 1.0f);
	movement += EnginePhysics.getScene()->getGravity() * _currentGravity;
}

void TCompPlayerCharacterController::interact(float dt) {
	TCompInteractor * interactor = getInteractor();
	if (!interactor) return;
	interactor->interact(dt);
}

float TCompPlayerCharacterController::getCurrentMovementSpeed() {
	return _curMovmentSpeed;
}

Vector3 TCompPlayerCharacterController::getCurrentVelocity() {
	return _curVelocity;
}

float TCompPlayerCharacterController::getCurrentRotationSpeed() {
	return _curRotationSpeed;
}

// Function rotates to face cameraTransform front according to rotation speed,
// also sets position to the designated newPos.
void TCompPlayerCharacterController::rotateAndMoveInDir(TCompTransform * cameraTransform, const VEC3 & dir, float rotationSpeed, float dt) {	
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;
	VEC3 relativeDisp;		// the displacement relative to the camera position
	float yaw, pitch;

	transform->getAngles(&yaw, &pitch);
	// get quaternion from only yaw component of the player reversed
	QUAT quat = QUAT::CreateFromYawPitchRoll(yaw + (float)M_PI, 0.0f, 0.0f);
	// rotate displacement vector in order to get the right direction relative to the camera
	relativeDisp = DirectX::XMVector3Rotate(dir, quat);

	transform->getAngles(&yaw, &pitch);
	float yawObj = transform->getDeltaYawToAimTo(transform->getPosition() + cameraTransform->getFront());
	float finalYaw = Maths::lerpAngle(yaw, yaw + yawObj, dt * rotationSpeed);
	move(relativeDisp);
	_currentYaw = finalYaw;
}

// Function rotates to direction relative to cameraTransform according to rotation speed,
// also sets position to the designated newPos.
void TCompPlayerCharacterController::rotateAndMoveFreely(TCompTransform * cameraTransform, const VEC3 & dir, float rotationSpeed, float dt) {
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;
	VEC3 playerPos = transform->getPosition();
	VEC3 relativeDisp;		// the displacement relative to the camera position
	float yaw, pitch, finalYaw;

	cameraTransform->getAngles(&yaw, &pitch);
	// get quaternion from only yaw component of the camera
	QUAT quat = QUAT::CreateFromYawPitchRoll(yaw + (float)M_PI, 0.0f, 0.0f);
	// rotate displacement vector in order to get the right direction relative to the camera
	relativeDisp = DirectX::XMVector3Rotate(dir, quat);

	transform->getAngles(&yaw, &pitch);
	finalYaw = transform->getDeltaYawToAimTo(playerPos + relativeDisp);
	finalYaw = Maths::lerpAngle(yaw, yaw + finalYaw, dt * rotationSpeed);
	move(relativeDisp);
	_currentYaw = finalYaw;
}

void TCompPlayerCharacterController::rotateInDir(TCompTransform * cameraTransform, float rotationSpeed, float dt) {
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;
	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	float yawObj = transform->getDeltaYawToAimTo(transform->getPosition() + cameraTransform->getFront());
	float finalYaw = Maths::lerpAngle(yaw, yaw + yawObj, dt * rotationSpeed);
	_currentYaw = finalYaw;
}

void TCompPlayerCharacterController::updateInteractor(const physx::PxVec3 & movement) {
	auto controller = getController();
	if (!controller) return;
	TCompTransform * playerTransf = getComponent <TCompTransform>();
	if (!playerTransf) return;
	auto interactor = getInteractor();
	if (!interactor) return;
	// We prepare a transform in order to send position and rotation to physx kinematic body
	CTransform interactorTransf;
	// Set the position to the front of the player and move it up so it doesn't sink into the ground
	interactorTransf.setPosition(playerTransf->getPosition() + PXVEC3_TO_VEC3(movement)
		+ playerTransf->getFront() + playerTransf->getUp() * interactor->getHalfSize().y);
	interactorTransf.setRotation(playerTransf->getRotation());
	// Move kinematic body
	interactor->moveToTarget(interactorTransf);
}

physx::PxCapsuleController * TCompPlayerCharacterController::getController() {
	if (!_controller) {
		TCompCollider * col = getComponent<TCompCollider>();
		if (!col)
			return nullptr;
		_controller = col->controller;
	}

	return _controller;
}

void TCompPlayerCharacterController::onEntityCreated(const TMsgEntityCreated & msg) {
	_transform = getComponent<TCompTransform>();
	if (!_transform.isValid()) setActive(false);
	setStepOffset(_stepOffset);

	_filters = physx::PxControllerFilters(&_filterData);
	auto* controller = getController();
	controller->invalidateCache();
	controller->move(physx::PxZero, 0.001f, 0.016f, _filters);

	// Initialize last position to be equal for the first frame as the actual one
	TCompTransform* transform = _transform; 
	_lastPosition = transform->getPosition();

	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	_currentYaw = yaw;
}

void TCompPlayerCharacterController::onMovementSpeedMult(const TMsgMovementSpeedMultiplier & msg) {
	_playerSpeedAiming *= msg.mult;
	_playerSpeedNormal *= msg.mult;
	_playerSpeedSprinting *= msg.mult;
}
