#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "PxPhysicsAPI.h"
#include "components/common/comp_transform.h"
#include "components/player/comp_interactor.h"

class TCompPlayerCharacterController : public TCompBase {

public:
	DECL_SIBLING_ACCESS();

	void debugInMenu();
	void renderDebug();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	static void registerMsgs();
	static void declareInLua();

	void resize(float height);	// use always this method to modify height of controller
	void setRadius(float radius);
	void setSlopeLimit(float slopeLimit);
	void setStepOffset(float stepOffset);

	void move(VEC3 displacement);
	void teleport(VEC3 position);

	// Actions
	void rotateAndMoveInDir(TCompTransform * cameraTransform, const VEC3 & dir, float rotationSpeed, float dt);
	void rotateAndMoveFreely(TCompTransform * cameraTransform, const VEC3 & dir, float rotationSpeed, float dt);
	void rotateInDir(TCompTransform * cameraTransform, float rotationSpeed, float dt);
	void interact(float dt);

	// Getters
	float getCurrentMovementSpeed();
	Vector3 getCurrentVelocity();
	float getCurrentRotationSpeed();
	float const getPlayerSpeedNormal() const { return _playerSpeedNormal; }
	float const getPlayerSpeedSprinting() const { return _playerSpeedSprinting; }
	float const getPlayerSpeedAiming() const { return _playerSpeedAiming; }
	float const getPlayerSpeedMultiplierBackwards() const { return _playerSpeedMultiplierBackwards; }
	float const getRotationSpeedNormal() const { return _rotationSpeedNormal; }
	float const getRotationSpeedSprinting() const { return _rotationSpeedSprinting; }
	float const getRotationSpeedAiming() const { return _rotationSpeedAiming; }
	float const getRotationSpeedHipShooting() const { return _rotationSpeedHipShooting; }
	bool isGrounded() const { return _isGrounded; }

	// Messages.
	void onLogicStatus(const TMsgLogicStatus & msg);

private:
	DECL_TCOMP_ACCESS("Player_interactor", TCompInteractor, Interactor);

	physx::PxCapsuleController * _controller = nullptr;
	physx::PxFilterData _filterData;
	physx::PxControllerFilters _filters;
	CHandle _transform = CHandle();

	physx::PxVec3 _displacement = physx::PxVec3(0);
	float _currentYaw = 0.0f;
	Vector3 _lastPosition; //Initialized in onEntityCreated
	float _curMovmentSpeed = 0.0f, _curRotationSpeed = 0.0f;
	float _lastDeltaTime = 0.0f;
	Vector3 _curVelocity = Vector3::Zero;

	// Json parameters
	float _playerSpeedNormal = 5.0f, _rotationSpeedNormal = 8.0f;
	float _playerSpeedSprinting = 9.0f, _rotationSpeedSprinting = 3.5f;
	float _playerSpeedAiming = 2.0f, _rotationSpeedAiming = 10.0f;
	float _playerSpeedMultiplierBackwards = 0.75f;
	float _rotationSpeedHipShooting = 20.0f;
	float _stepOffset = 0.25f;

	bool _isGrounded = false;
	float _currentGravity = 0.0f;

	void applyGravity(physx::PxVec3 & movementOut, float dt);
	void simulate(const physx::PxVec3 & movement, float dt);
	void updateRotation();
	void updateInteractor(const physx::PxVec3 & movement);
	physx::PxCapsuleController * getController();

	void onEntityCreated(const TMsgEntityCreated& msg);
	void onMovementSpeedMult(const TMsgMovementSpeedMultiplier& msg);

};

