#include "mcv_platform.h"
#include "comp_character_controller.h"
#include "engine.h"
#include "modules/module_physics.h"
#include "utils/phys_utils.h"
#include "components/common/comp_name.h"
#include "components/animations/comp_enemyMelee_animator.h"
#include "components/ai/behaviour_tree/comp_bt_enemy_melee.h"
#include "components/ai/behaviour_tree/comp_bt_enemy_ranged.h"
#include "components/ai/behaviour_tree/comp_bt_enemy_immortal.h"
#include "components/common/comp_aabb.h"

using namespace physx;

DECL_OBJ_MANAGER("character_controller", TCompCharacterController);

const float TCompCharacterController::DEFAULT_HEIGHT = 1.0f;
const float TCompCharacterController::DEFAULT_RADIUS = 1.0f;

void TCompCharacterController::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::DragFloat("RailgunThrowMagnitude", &_railgunThrowMagnitude, 1.0f, 0.0f, 5000.0f);

	ImGui::Checkbox("Pinned To wall?", &_bodyFlyData.jointCreated);
	if (_bodyFlyData.jointCreated) {
		ImGui::LabelText("Bone name", "%s", _bodyFlyData.boneName.c_str());
		ImGui::Checkbox("broken joint?", &debugJoint.broken);

		ImGui::Separator();
		ImGui::Text("Joint info:");
		ImGui::Text("World point: %f %f %f", _bodyFlyData.hitCoord.x, _bodyFlyData.hitCoord.y, _bodyFlyData.hitCoord.z);
	}

	ImGui::Separator();
	
	ImGui::LabelText("Current movement speed", "%f", _curMovmentSpeed);
	ImGui::LabelText("Current rotation speed", "%f", _curRotationSpeed);
	ImGui::LabelText("Current velocity", "%f %f %f", _curVelocity.x, _curVelocity.y, _curVelocity.z);

	ImGui::Separator();

	ImGui::DragFloat("Movement speed", &_movementSpeed, 0.1f, 0.f, 1000.f);
	ImGui::DragFloat("Rotation speed", &_rotationSpeed, 0.1f, 0.f, 1000.f);

	ImGui::Separator();

	if (ImGui::DragFloat("StepOffset", &_stepOffset, 0.01f, 0.0f, 1.0f)) 
		setStepOffset(_stepOffset);
}

void TCompCharacterController::renderDebug() {

	// Render joint
	if (_isDead && _bodyFlyData.jointCreated) {
		drawWiredSphere(debugJoint.hitCoord.getPosition(), 0.1f, VEC4(0.75, 0.0, 0.75, 1));
		drawLine(debugJoint.hitCoord.getPosition(), debugJoint.hitCoord.getFront() * 1.f, VEC4(0.0, 1.0, 1.0, 1.0));
	}
}

void TCompCharacterController::load(const json& j, TEntityParseContext& ctx) {
	// Will be used to load/unload components
	_json = j;
	_ctx = &ctx;

	_movementSpeed = j.value("movement_speed", _movementSpeed);
	_rotationSpeed = j.value("rotation_speed", _rotationSpeed);
	_stepOffset = j.value("step_offset", _stepOffset);
	_railgunThrowMagnitude = j.value("railgun_throw_magnitude", _railgunThrowMagnitude);

	_filterData.word0 = EnginePhysics.loadFilter(j, "mask", EnginePhysics.ScenarioLow | EnginePhysics.Trigger | EnginePhysics.DynamicScenario);
}

void TCompCharacterController::update(float dt) {
	if (!_isDead) {
		if (_enabledLastFrame) {
			ignoreFrameAfterEnabledCollider(dt);
			return;
		}
		physx::PxVec3 movement = _displacement * dt;
		{
			PROFILE_FUNCTION("CC - Normal");
			{
				{
					PROFILE_FUNCTION("CC - Simulation Normal - Gravity");
					applyGravity(movement, dt);
				}
				PROFILE_FUNCTION("CC - Simulation");
				simulate(movement, dt);
			}
			{
				PROFILE_FUNCTION("CC - Rotation");
				updateRotation(dt);
			}
		}
	}
	else {
		{
			PROFILE_FUNCTION("CC - Flying");
			if (checkFlying())
				flyToWall();
			else if (!_bodyFlyData.jointCreated) {
				createJointToWall();
			}
		}
	}

}

void TCompCharacterController::registerMsgs() {
	DECL_MSG(TCompCharacterController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCharacterController, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompCharacterController, TMsgResetComponent, onResetComponent);
	DECL_MSG(TCompCharacterController, TMsgLogicStatus, onLogicStatus);
	DECL_MSG(TCompCharacterController, TMsgMovementSpeedMultiplier, onMovementSpeedMult);
}

void TCompCharacterController::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}

void TCompCharacterController::onResetComponent(const TMsgResetComponent& msg) {
	_isDead = false;
	_bodyFlyData = BodyFlyData{};
	debugJoint = DebugJoint{};
}

void TCompCharacterController::onMovementSpeedMult(const TMsgMovementSpeedMultiplier & msg) {
	_movementSpeed *= msg.mult;
}

void TCompCharacterController::declareInLua() {
	EngineScripting.declareComponent<TCompCharacterController>
		(
			"TCompBTDemon",
			"move", &TCompCharacterController::move,
			"turnToYaw", &TCompCharacterController::turnToYaw,
			"turnToPosition", &TCompCharacterController::turnToPosition,
			"teleport", &TCompCharacterController::teleport,
			"movementSpeed", sol::property(&TCompCharacterController::getMovementSpeed, &TCompCharacterController::setMovementSpeed),
			"rotationSpeed", sol::property(&TCompCharacterController::getRotationSpeed, &TCompCharacterController::setRotationSpeed),
			"isDead", &TCompCharacterController::isDead,
			"isGrounded", &TCompCharacterController::isGrounded
			);
}

void TCompCharacterController::resize(float height) {
	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->resize(height);
}

void TCompCharacterController::railgunThrow(const TMsgNailDamage & msg)
{
	PROFILE_FUNCTION("RailgunThrow");
	TCompCollider* col = getComponent<TCompCollider>();
	TCompTransform* trans = getComponent<TCompTransform>();
	if (!col || !trans) return;

	// Check wether the pin is possible
	auto hitData = PhysUtils::railgunRaycast(msg.hitCoord, msg.damageDirection);
	
	if (hitData.hasBlock) {
		if (!_isDead) {
			// Demon dead
			TCompHealth* health = getComponent<TCompHealth>();
			if (health)
				health->addDamage(1000);

			_isDead = true;
		}

		// Save the shape and the actor
		_bodyFlyData.shapeHit = hitData.block.shape;
		_bodyFlyData.actorHit = hitData.block.actor;

		// get ragdolls and throw them away
		TCompRagdoll* ragdoll = getComponent<TCompRagdoll>();
		ragdoll->activateRagdoll();
		// Get the bone hit
		std::pair<std::string, PxArticulationLink*> hitBone = ragdoll->getBoneFromGlobalPos(PXVEC3_TO_VEC3(msg.hitCoord));
		_bodyFlyData.boneName = hitBone.first;
		_bodyFlyData.bone = hitBone.second;

		// Make enemy body fly until it reaches the wall
		_bodyFlyData.throwDirection = msg.damageDirection;
		_bodyFlyData.hitCoord = PXVEC3_TO_VEC3(hitData.block.position);
		_bodyFlyData.distanceToWall = hitData.block.distance;
		
		CHandle h_wallCollider;
		h_wallCollider.fromVoidPtr(hitData.block.actor->userData);
		_bodyFlyData.wallCollider = h_wallCollider;
		_bodyFlyData.jointCreated = false;

		ragdoll->startFlyingRagdoll(_bodyFlyData.hitCoord, _railgunThrowMagnitude, _bodyFlyData.bone);

		// Deactivate bt
		TCompBTEnemyMelee* melee = getComponent<TCompBTEnemyMelee>();
		TCompBTEnemyRanged* ranged = getComponent<TCompBTEnemyRanged>();
		TCompBTEnemyImmortal* immortal = getComponent<TCompBTEnemyImmortal>();
		
		if (melee)
			melee->setActive(false);
		else if (ranged)
			ranged->setActive(false);
		else if (immortal)
			immortal->setActive(false);

		// Attach AABB to ragdoll
		if (ragdoll) {
			TMsgRagdollActivated msg;
			if (ragdoll->ragdoll.num_bones > 0) {
				msg.link = ragdoll->ragdoll.bones[0].link;
				if (msg.link != nullptr)
					getEntity().sendMsgToComp<TCompLocalAABB>(msg);
			}
		}
		
		// Deactivate collider
		TCompCollider* col = getComponent<TCompCollider>();
		if (col) {
			TMsgToogleComponent msg{ false };
			getEntity().sendMsgToComp<TCompCollider>(msg);
		}

		// Deactivate pathfinding agent
		TCompPathfindingAgent* agent = getComponent<TCompPathfindingAgent>();
		if (agent && agent->isAgentActive()) agent->release();

		// Set entity as dead.
		TCompHealth * health = getComponent<TCompHealth>();
		if (health) health->setCurrentHealth(0.0f);
	}
}

bool TCompCharacterController::checkFlying() {

	// General case: If the two distances are lower than the threshold it means that the enemy is able to create the joint
	if (_bodyFlyData.distanceToWall < _bodyFlyData.thresholdToCreateJoint)
		return false;

	// Bouncing case: If the distance is bigger than the closer to wall, and the closest distance is lower than the threshold, it means it has bounced
	if (_bodyFlyData.distanceToWall > _bodyFlyData.distanceCloserToWall && _bodyFlyData.distanceCloserToWall < _bodyFlyData.distanceIgnoreThreshold)
		return false;

	return true;
}

/* [out] Distance between the body and the wall */
void TCompCharacterController::flyToWall()
{
	// Update the distance to the wall
	_bodyFlyData.distanceToWall = (PXVEC3_TO_VEC3(_bodyFlyData.bone->getGlobalPose().p) - _bodyFlyData.hitCoord).Length();

	// Check if its nearer to the wall, and if it is update the distanceCloserToWall
	if (_bodyFlyData.distanceCloserToWall > _bodyFlyData.distanceToWall)
		_bodyFlyData.distanceCloserToWall = _bodyFlyData.distanceToWall;
}

void TCompCharacterController::createJointToWall() {
	PxPhysics* physics = EnginePhysics.getPhysics();

	// Joint transform
	CTransform jointTransform;
	
	// Set the direction and the position of the joint
	VEC3 jointDir = (_bodyFlyData.throwDirection * -1);
	jointDir.Normalize();
	jointTransform.setRotation(QUAT::CreateFromYawPitchRoll(jointDir.x, jointDir.y, jointDir.z));
	// We add an offset so the joint isn't pixel perfect on the wall, and we leave a gap
	jointTransform.setPosition(_bodyFlyData.hitCoord + jointDir * 0.3f); 

	// And the debug joint
	debugJoint.hitCoord.setPosition(_bodyFlyData.hitCoord + jointDir * 0.3f);
	debugJoint.hitCoord.setRotation(QUAT::CreateFromYawPitchRoll(jointDir.x, jointDir.y, jointDir.z));

	// Calculate localframe 0: Scenario
	CEntity* wallE = _bodyFlyData.wallCollider->getEntity();

	// Get the wall world position from the shape and actor
	CTransform wallWorld;
	wallWorld = toTransform(_bodyFlyData.shapeHit->getLocalPose() * _bodyFlyData.actorHit->getGlobalPose());
	// Calculate the localFrame with the joint transform and the wallWorld.Invert 
	CTransform wallLocal;
	wallLocal.fromMatrix(jointTransform.asMatrix() * wallWorld.asMatrix().Invert());
	
	// Calculate localframe 1: enemy bone
	PxTransform boneTransform = _bodyFlyData.bone->getGlobalPose();

	CTransform enemyLocal;
	CTransform boneModified;
	boneModified.setPosition(jointTransform.getPosition());
	boneModified.setRotation(toTransform(boneTransform).getRotation());
	//enemyLocal.fromMatrix(jointTransform.asMatrix() * toTransform(boneTransform).asMatrix().Invert());
	enemyLocal.fromMatrix(jointTransform.asMatrix() * boneModified.asMatrix().Invert());

	// Make joint
	
	PxFixedJoint* joint = PxFixedJointCreate(*physics,
		_bodyFlyData.wallCollider->actor, toPxTransform(wallLocal),
		_bodyFlyData.bone, toPxTransform(enemyLocal));

	debugJoint.broken = (joint->getConstraintFlags() && PxConstraintFlag::eBROKEN) != 0;

	TCompRagdoll* ragdoll = getComponent<TCompRagdoll>();
	if (ragdoll) {
		//ragdoll->toggleRagdollGravity(true);
		ragdoll->stopFlyingRagdoll();
		ragdoll->clearRagdollForces();
		ragdoll->setPxFixedJoint(joint);
	}

	_bodyFlyData.jointCreated = true;
}


void TCompCharacterController::ignoreFrameAfterEnabledCollider(float dt) {
	_enabledLastFrame = false;

	// Simulate a move so physx doesn't go crazy next frame
	physx::PxCapsuleController* controller = getController();
	if (controller) {
		auto flags = controller->move(PxVec3(0.0f, 0.0f, 0.0f), 0.001f, dt, _filters);
		_isGrounded = flags.isSet(physx::PxControllerCollisionFlag::eCOLLISION_DOWN);
	}
	// Override the position after the move simulation
	teleport(_lastPosition);

	// Lastly set some variables for the next frame calculation
	_curMovmentSpeed = 0.0f;
	_curVelocity = Vector3::Zero;
	_lastDeltaTime = dt;
	_displacement = physx::PxVec3(physx::PxZero);
}

Vector3 TCompCharacterController::getPosition() {
	physx::PxCapsuleController* controller = getController();
	auto& pos = controller->getPosition();
	return PXVEC3_TO_VEC3(pos);
}

float TCompCharacterController::getMovementSpeed() {
	return _movementSpeed;
}

float TCompCharacterController::getCurrentMovementSpeed() {
	return _curMovmentSpeed;
}

Vector3 TCompCharacterController::getCurrentVelocity() {
	return _curVelocity;
}

float TCompCharacterController::getRotationSpeed() {
	return _rotationSpeed;
}

float TCompCharacterController::getCurrentRotationSpeed() {
	return _curRotationSpeed;
}

float TCompCharacterController::getHeight() {
	return getController()->getHeight();
}

float TCompCharacterController::getRadius() {
	return getController()->getRadius();
}

bool TCompCharacterController::isDead()
{
	return _isDead;
}

bool TCompCharacterController::isGrounded() {
	return _isGrounded;
}

void TCompCharacterController::setMovementSpeed(float speed) {
	_movementSpeed = speed;
}

void TCompCharacterController::setRotationSpeed(float speed) {
	_rotationSpeed = speed;
}

void TCompCharacterController::setRadius(float radius) {
	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->setRadius(radius);
}

void TCompCharacterController::setSlopeLimit(float slopeLimit) {
	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->setSlopeLimit(slopeLimit);
}

void TCompCharacterController::setStepOffset(float stepOffset) {
	physx::PxCapsuleController * controller = getController();
	if (controller)
		controller->setStepOffset(stepOffset);
}

void TCompCharacterController::move(const VEC3 &  disp) {
	VEC3 d = disp * _movementSpeed;
	_displacement += VEC3_TO_PXVEC3(d);
}

void TCompCharacterController::leap(const VEC3 &  disp) {
	VEC3 d = disp * 100.0f;
	_displacement += VEC3_TO_PXVEC3(d);
}

void TCompCharacterController::turnToYaw(float yaw) {
	_targetYaw = yaw;
}

void TCompCharacterController::turnToPosition(const VEC3 & pos) {
	TCompTransform * transform = getComponent<TCompTransform>();
	if (!transform) return;

	float yaw, pitch;
	transform->getAngles(&yaw, &pitch);
	turnToYaw(yaw + transform->getDeltaYawToAimTo(pos));
}

void TCompCharacterController::turnToTransform(const CTransform & lookAtTransf) {
	turnToPosition(lookAtTransf.getPosition());
}

void TCompCharacterController::setYaw(float yaw) {
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;

	float currentYaw, currentPitch;
	transform->getAngles(&currentYaw, &currentPitch);
	transform->setAngles(yaw, currentPitch);

	_targetYaw = yaw;
}

void TCompCharacterController::setPitch(float pitch) {
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;

	float currentYaw, currentPitch;
	transform->getAngles(&currentYaw, &currentPitch);
	transform->setAngles(currentYaw, pitch);
}

void TCompCharacterController::teleport(const VEC3 &  pos) {
	if (!active) return;

	getController()->setFootPosition(VEC3_TO_PXEXTVEC3(pos));
	_isGrounded = false;
}

void TCompCharacterController::simulate(const physx::PxVec3 & movement, float dt) {
	if (!active) return;
	{
		PROFILE_FUNCTION("CC - Simulation Normal - Movement");
		physx::PxCapsuleController * controller;
		{
			PROFILE_FUNCTION("CC - Simulation Normal - GetController");
			controller = getController();
		}
		{
			PROFILE_FUNCTION("CC - Simulation Normal - Move PHYSX");
			if (controller) {
				if (movement != physx::PxZero) {
					auto flags = controller->move(movement, 0.001f, dt, _filters);
					_isGrounded = flags.isSet(physx::PxControllerCollisionFlag::eCOLLISION_DOWN);
				}
			}
		}

		// Update _lastPosition so we can calculate the movement speed and velocity of the controller
		TCompTransform* transform = _transform;
		Vector3& newPos = transform->getPosition();
		if (_lastPosition == newPos) {
			_curMovmentSpeed = 0.0f;
			_curVelocity = Vector3::Zero;
		}
		else {
			_curVelocity = (newPos - _lastPosition) / _lastDeltaTime;
			_curMovmentSpeed = Vector3::Distance(Vector3::Zero, _curVelocity);
			_lastPosition = newPos;
		}
		_lastDeltaTime = dt;
		_displacement = physx::PxVec3(physx::PxZero);
	}
}

void TCompCharacterController::updateRotation(float dt) {
	TCompTransform* transform = getComponent<TCompTransform>();
	if (!transform) return;

	float currentYaw, pitch;
	transform->getAngles(&currentYaw, &pitch);
	float newYaw = Maths::lerpAngle(currentYaw, _targetYaw, _rotationSpeed * dt);
	transform->setAngles(newYaw, pitch);
	_curRotationSpeed = abs(newYaw - currentYaw);
}

void TCompCharacterController::applyGravity(physx::PxVec3 & movement, float dt) {
	if (_isGrounded)
		_currentGravity = 0.0f;
	else
		_currentGravity = std::min(_currentGravity + dt * 0.05f, 1.0f);
	movement += EnginePhysics.getScene()->getGravity() * _currentGravity;
}

physx::PxCapsuleController * TCompCharacterController::getController() {
	TCompCollider * col = getComponent<TCompCollider>();
	if (!col)
		return nullptr;
	return col->controller;
}

void TCompCharacterController::onEntityCreated(const TMsgEntityCreated & msg) {
	_transform = getComponent<TCompTransform>();
	if (!_transform.isValid()) setActive(false);
	setStepOffset(_stepOffset);

	_filters = physx::PxControllerFilters(&_filterData);
	auto* controller = getController();
	controller->invalidateCache();
	controller->move(physx::PxZero, 0.001f, 0.016f, _filters);
}
