#pragma once

#include "../common/comp_base.h"
#include "../../entity/common_msgs.h"
#include "PxPhysicsAPI.h"

struct TMsgRigidBodyCreated {
	DECL_MSG_ID();
};


/* A Rigid Body class implemented to allow the instantiation of a rigid body component
for controlling colliders from code directly without having to delve into physX actors directly.
It's necessary to provide a TCompCollider from which this component will fetch the rigid body.
If no rigidbody is found, it will prompt an error an deactivate. */

// https://docs.nvidia.com/gameworks/content/gameworkslibrary/physx/guide/Manual/RigidBodyDynamics.html
// https://docs.unity3d.com/ScriptReference/Rigidbody.html

struct TMsgEntityCreated;
struct TMsgLogicStatus;

class TCompRigidBody : public TCompBase {
	DECL_SIBLING_ACCESS();

	json dataToLoad;
	physx::PxRigidBody * rigidBody = nullptr;

	/* On message received functions. */
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onRigidBodyCreatedFromCollider(const TMsgRigidBodyCreated & msg);

	void configureRigidBody();
public:
	
	void debugInMenu();
	void renderDebug();
	void load(const json & j, TEntityParseContext& ctx);
	void update(float dt);
	static void registerMsgs();
	static void declareInLua();

	// Adds a force to the rigidbody.
	void addForce(const Vector3 & force, bool autowake = true);
	// Adds an impulse to the rigidbody. Use it instead of force if you want
	// instantaneous changes.
	void addImpulse(const Vector3 & impulse, bool autowake = true);
	void addAcceleration(const Vector3 & acceleration, bool autowake = true);
	void addVelocityChange(const Vector3 & velocityChange, bool autowake = true);
	void addTorqueForce(const Vector3 & force, bool autowake = true);
	void addTorqueImpulse(const Vector3 & impulse, bool autowake = true);
	void addTorqueAcceleration(const Vector3 & acceleration, bool autowake = true);
	void addTorqueVelocityChange(const Vector3 & velocityChange, bool autowake = true);
	void setPosition(const Vector3 & position, bool autowake = true);
	void setRotation(const Quaternion & newRotation);
	void setLinearVelocity(const Vector3 & linVel, bool autowake = true);
	void setAngularVelocity(const Vector3 & angVel, bool autowake = true);
	void setMass(float mass);
	void setInertia(const Vector3 & inertia);
	void setKinematic(bool isKinematic);
	void setGravityActive(bool gravityActive);

	/* Getters. */
	Vector3 getLinearVelocity() const;
	Vector3 getAngularVelocity() const;
	float getMass() const;
	Vector3 getInertia() const;
	bool isKinematic() const;
	bool isGravityActive() const;
};