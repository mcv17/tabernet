#include "mcv_platform.h"
#include "comp_rigid_body.h"

#include "engine.h"
#include "components/common/comp_collider.h"

DECL_OBJ_MANAGER("rigid_body", TCompRigidBody);

void TCompRigidBody::load(const json & j, TEntityParseContext& ctx) { 
	dataToLoad = j; 
}

void TCompRigidBody::registerMsgs() {
	DECL_MSG(TCompRigidBody, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompRigidBody, TMsgRigidBodyCreated, onRigidBodyCreatedFromCollider);
	DECL_MSG(TCompRigidBody, TMsgToogleComponent, onToggleComponent);
}

void TCompRigidBody::declareInLua() {
	EngineScripting.declareComponent<TCompRigidBody>(
		// name in LUA
		"TCompRigidBody",
		// methods
		"addForce", &TCompRigidBody::addForce,
		"addImpulse", &TCompRigidBody::addImpulse,
		"addTorqueForce", &TCompRigidBody::addTorqueForce,
		"addTorqueImpulse", &TCompRigidBody::addTorqueImpulse,
		"setPosition", &TCompRigidBody::setPosition,
		"setLinearVelocity", &TCompRigidBody::setLinearVelocity,
		"setAngularVelocity", &TCompRigidBody::setAngularVelocity,
		"setMass", &TCompRigidBody::setMass,
		"setInertia", &TCompRigidBody::setInertia,
		"setKinematic", &TCompRigidBody::setKinematic,
		"setGravityActive", &TCompRigidBody::setGravityActive
	);
}

void TCompRigidBody::onEntityCreated(const TMsgEntityCreated & msg){
	configureRigidBody();
}

void TCompRigidBody::onRigidBodyCreatedFromCollider(const TMsgRigidBodyCreated & msg) {
	configureRigidBody();
}

void TCompRigidBody::configureRigidBody() {
	CHandle entityH = CHandle(this).getOwner();
	if (!entityH.isValid()) return;
	CEntity * entity = entityH;
	if (!entity) return;

	TCompCollider * collider = entity->getComponent<TCompCollider>();
	if (!collider || !collider->isRigidBody()) {
		assert("Controller needs to be dynamic for rigidbodies to work!" && false);
		return;
	}

	rigidBody = (physx::PxRigidBody*)collider->actor;
	if (rigidBody == nullptr) return;

	if (dataToLoad.empty()) return;

	if (dataToLoad.count("mass") > 0)
		setMass(dataToLoad["mass"]);
	if (dataToLoad.count("inertia") > 0)
		setInertia(loadVector3(dataToLoad["inertia"]));
	if (dataToLoad.count("kinematic") > 0)
		setKinematic(dataToLoad["kinematic"]);
	if (dataToLoad.count("gravity") > 0)
		setGravityActive(dataToLoad["gravity"]);
	dataToLoad.clear();
}

void TCompRigidBody::debugInMenu(){
	ImGui::Text("Current mass: %f", getMass());
	Vector3 inertia = getInertia();
	ImGui::Text("Current inertia: %f %f %f", inertia.x, inertia.y, inertia.z);
	ImGui::Text("Is Kinematic: %s", isKinematic() ? "true" : "false");
	ImGui::Text("Is Gravity Active: %s", isGravityActive() ? "true" : "false");

	ImGui::Spacing(0, 5);

	static Vector3 newPosition;
	ImGui::DragFloat3("Set new position: ", &newPosition.x, 0.01f, -100000.0f, 100000.0f);
	if (ImGui::Button("Set new position:"))
		setPosition(newPosition);

	ImGui::Spacing(0, 5);

	static Vector3 forceToApply;
	ImGui::DragFloat3("Force vector: ", &forceToApply.x, 0.01f, -1000.0f, 1000.0f);
	if (ImGui::Button("Apply force:"))
		addForce(forceToApply);

	ImGui::Spacing(0, 5);

	static Vector3 impulseToApply;
	ImGui::DragFloat3("Impulse vector: ", &impulseToApply.x, 0.01f, -1000.0f, 1000.0f);
	if (ImGui::Button("Apply impulse:"))
		addImpulse(impulseToApply);

	ImGui::Spacing(0, 5);

	static Vector3 torqueForceToApply;
	ImGui::DragFloat3("Torque force vector: ", &torqueForceToApply.x, 0.01f, -1000.0f, 1000.0f);
	if (ImGui::Button("Apply torque force:"))
		addForce(torqueForceToApply);

	ImGui::Spacing(0, 5);

	static Vector3 torqueImpulseToApply;
	ImGui::DragFloat3("Torque impulse vector: ", &torqueImpulseToApply.x, 0.01f, -1000.0f, 1000.0f);
	if (ImGui::Button("Apply torque impulse:"))
		addImpulse(torqueImpulseToApply);

	ImGui::Spacing(0, 5);

	static Vector3 linearVelocity;
	ImGui::DragFloat3("Set linear velocity vector: ", &linearVelocity.x, 0.01f, -1000.0f, 1000.0f);
	if (ImGui::Button("Set linear velocity:"))
		setLinearVelocity(linearVelocity);

	ImGui::Spacing(0, 5);

	static Vector3 angularVelocity;
	ImGui::DragFloat3("Set angular velocity vector: ", &angularVelocity.x, 0.01f, -1000.0f, 1000.0f);
	if (ImGui::Button("Set angular velocity:"))
		setAngularVelocity(angularVelocity);

	ImGui::Spacing(0, 5);

	static float newMass;
	ImGui::DragFloat("Set new mass: ", &newMass, 0.01f, 0.0f, 1000.0f);
	if (ImGui::Button("Set Mass:"))
		setMass(newMass);

	ImGui::Spacing(0, 5);

	static Vector3 newInertia;
	ImGui::DragFloat3("Set new inertia: ", &newInertia.x, 0.01f, 0.0f, 1000.0f);
	if (ImGui::Button("Set Inertia:"))
		setInertia(newInertia);

	ImGui::Spacing(0, 5);
}

void TCompRigidBody::renderDebug(){}

void TCompRigidBody::update(float dt) {}

void TCompRigidBody::addForce(const Vector3 & force, bool autowake){
	if (rigidBody)
		rigidBody->addForce(VEC3_TO_PXVEC3(force), physx::PxForceMode::eFORCE, autowake);
}

void TCompRigidBody::addImpulse(const Vector3 & impulse, bool autowake) {
	if (rigidBody)
		rigidBody->addForce(VEC3_TO_PXVEC3(impulse), physx::PxForceMode::eIMPULSE, autowake);
}

void TCompRigidBody::addAcceleration(const Vector3 & acceleration, bool autowake) {
	if (rigidBody)
		rigidBody->addForce(VEC3_TO_PXVEC3(acceleration), physx::PxForceMode::eACCELERATION, autowake);
}

void TCompRigidBody::addVelocityChange(const Vector3 & velocityChange, bool autowake) {
	if (rigidBody)
		rigidBody->addForce(VEC3_TO_PXVEC3(velocityChange), physx::PxForceMode::eVELOCITY_CHANGE, autowake);
}

void TCompRigidBody::addTorqueForce(const Vector3 & force, bool autowake) {
	if (rigidBody)
		rigidBody->addTorque(VEC3_TO_PXVEC3(force), physx::PxForceMode::eFORCE, autowake);
}

void TCompRigidBody::addTorqueImpulse(const Vector3 & impulse, bool autowake) {
	if (rigidBody)
		rigidBody->addTorque(VEC3_TO_PXVEC3(impulse), physx::PxForceMode::eIMPULSE, autowake);
}

void TCompRigidBody::addTorqueAcceleration(const Vector3 & acceleration, bool autowake) {
	if (rigidBody)
		rigidBody->addTorque(VEC3_TO_PXVEC3(acceleration), physx::PxForceMode::eACCELERATION, autowake);
}

void TCompRigidBody::addTorqueVelocityChange(const Vector3 & velocityChange, bool autowake) {
	if (rigidBody)
		rigidBody->addTorque(VEC3_TO_PXVEC3(velocityChange), physx::PxForceMode::eVELOCITY_CHANGE, autowake);
}

void TCompRigidBody::setPosition(const Vector3 & position, bool autowake){
	if (rigidBody) {
		physx::PxTransform transform = rigidBody->getGlobalPose();
		transform.p = VEC3_TO_PXVEC3(position);
		rigidBody->setGlobalPose(transform, autowake);
	}
}

void TCompRigidBody::setRotation(const Quaternion & newRotation) {
	if (rigidBody) {
		physx::PxTransform & rbTransform = rigidBody->getGlobalPose();
		rbTransform.q = QUAT_TO_PXQUAT(newRotation);
		rigidBody->setGlobalPose(rbTransform);
	}
}

void TCompRigidBody::setMass(float mass){
	if (rigidBody)
		rigidBody->setMass(mass);
}

void TCompRigidBody::setInertia(const Vector3 & inertia) {
	if (rigidBody)
		rigidBody->setMassSpaceInertiaTensor(VEC3_TO_PXVEC3(inertia));
}

void TCompRigidBody::setLinearVelocity(const Vector3 & linVel, bool autowake){
	if (rigidBody)
		rigidBody->setLinearVelocity(VEC3_TO_PXVEC3(linVel), autowake);
}

void TCompRigidBody::setAngularVelocity(const Vector3 & angVel, bool autowake){
	CHandle entityH = CHandle(this).getOwner();
	if (!entityH.isValid()) return;
	CEntity * entity = entityH;
	TCompCollider * collider = entity->getComponent<TCompCollider>();
	if (rigidBody)
		rigidBody->setAngularVelocity(VEC3_TO_PXVEC3(angVel), autowake);
}

void TCompRigidBody::setKinematic(bool isKinematic) {
	if (isKinematic && rigidBody) {
		setMass(0.0f);
		setInertia(Vector3::Zero);
	}
}

void TCompRigidBody::setGravityActive(bool gravityActive) {
	if (rigidBody)
		rigidBody->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, !gravityActive);
}

Vector3 TCompRigidBody::getLinearVelocity() const {
	return rigidBody ? PXVEC3_TO_VEC3(rigidBody->getLinearVelocity()) : Vector3::Zero;
}

Vector3 TCompRigidBody::getAngularVelocity() const {
	return rigidBody ? PXVEC3_TO_VEC3(rigidBody->getAngularVelocity()) : Vector3::Zero;
}

float TCompRigidBody::getMass() const {
	return rigidBody ? rigidBody->getMass() : 0.0f;
}

Vector3 TCompRigidBody::getInertia() const {
	return rigidBody ? PXVEC3_TO_VEC3(rigidBody->getMassSpaceInertiaTensor()) : Vector3::Zero;
}

bool TCompRigidBody::isKinematic() const {
	return rigidBody ? rigidBody->getRigidBodyFlags() & physx::PxRigidBodyFlag::eKINEMATIC : false;
}

bool TCompRigidBody::isGravityActive() const {
	return rigidBody ? !(rigidBody->getActorFlags() & physx::PxActorFlag::eDISABLE_GRAVITY) : false;
}