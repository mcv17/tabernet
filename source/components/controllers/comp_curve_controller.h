#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "geometry/curve.h"

class TCompCurveController : public TCompBase
{
  DECL_SIBLING_ACCESS();

public:
  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);
  void renderDebug();
	static void registerMsgs();

  void setRatio(float ratio);
	
private:
  void applyRatio();

  const CCurve* _curve = nullptr;
  float _speed = 10.f;
  float _ratio = 0.f;
  bool  _enabled = true;
  CHandle _target;
  std::string _targetName;
};

