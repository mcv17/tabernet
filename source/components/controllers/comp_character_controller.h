#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "PxPhysicsAPI.h"
#include "components/common/comp_transform.h"
#include "components/player/comp_interactor.h"

class TCompCharacterController : public TCompBase {

public:
	static const float DEFAULT_HEIGHT;
	static const float DEFAULT_RADIUS;

	DECL_SIBLING_ACCESS();

	void debugInMenu();
	void renderDebug();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	static void registerMsgs();
	static void declareInLua();

	// Actions
	void move(const VEC3 &  displacement);
	void leap(const VEC3 &  target);
	void turnToYaw(float yaw);
	void turnToPosition(const VEC3 & pos);
	void turnToTransform(const CTransform & transf);
	void setYaw(float yaw);
	void setPitch(float pitch);
	void teleport(const VEC3 & position);
	void resize(float height);	// use always this method to modify height of controller
	void railgunThrow(const TMsgNailDamage & msg);
	bool checkFlying();
	void flyToWall();
	void createJointToWall();
	void ignoreFrameAfterEnabledCollider(float dt);

	// Getters
	Vector3 getPosition();
	float getMovementSpeed();
	float getCurrentMovementSpeed();
	Vector3 getCurrentVelocity();
	float getRotationSpeed();
	float getCurrentRotationSpeed();
	float getHeight();
	float getRadius();
	bool isDead();
	bool isGrounded();
	
	// Setters
	void setMovementSpeed(float speed);
	void setRotationSpeed(float speed);
	void setRadius(float radius);
	void setSlopeLimit(float slopeLimit);
	void setStepOffset(float stepOffset);
	void setColliderEnabledLastFrame(Vector3 position) { _enabledLastFrame = true; _lastPosition = position; }

	// Messages
	void onLogicStatus(const TMsgLogicStatus& msg);
	void onResetComponent(const TMsgResetComponent& msg);
	void onMovementSpeedMult(const TMsgMovementSpeedMultiplier& msg);

private:
	physx::PxVec3 _displacement;
	float _targetYaw;
	float _movementSpeed = 4.0f;
	float _rotationSpeed = 20.0f;
	float _curMovmentSpeed, _curRotationSpeed;
	Vector3 _curVelocity, _lastPosition;
	float _lastDeltaTime;
	bool _isDead = false;
	bool _isGrounded = false;
	float _currentGravity;
	float maxDistanceRange = 15.0f;
	float _stepOffset = 0.25f;
	float _railgunThrowMagnitude = 2000.0f;
	bool _enabledLastFrame = false; // Used when we enable the collider, and it comes from under the floor (number of frames to ignore)

	physx::PxFilterData _filterData;
	physx::PxControllerFilters _filters;

	CHandle _transform;

	// Railgun only
	struct BodyFlyData {
		VEC3 throwDirection;						// Direction from the shot to the wall point
		VEC3 hitCoord;								// Point on target where the enemy will be pinned
		float distanceToWall = 100000.0F;			// Current distance between the enemy bone and the hitCoord
		float distanceIgnoreThreshold = 2.2f;		// Distance to ignore when checking if it has bounced
		float thresholdToCreateJoint = 0.3f;		// Threshold needed in order to create the joint and stop flying
		float distanceCloserToWall = 100000.0F;		// Every update this will be modified if it flies closer to the wall
		bool jointCreated = false;					// Check if the joint's already created

		TCompCollider* wallCollider = nullptr;  
		physx::PxShape* shapeHit = nullptr;			// Shape of the hitCoord
		physx::PxRigidActor* actorHit = nullptr;	// Actor of the hitCoord
		physx::PxArticulationLink* bone = nullptr;	// Bone hit
		std::string boneName;						// Name of the bone
	} _bodyFlyData;

	std::vector<VEC3> _debugPoints;

	void applyGravity(physx::PxVec3 & movement, float dt);
	void simulate(const physx::PxVec3 & movement, float dt);
	void updateRotation(float dt);

	json _json;
	TEntityParseContext* _ctx;

	struct DebugJoint {
		CTransform hitCoord;
		bool broken = false;
	} debugJoint;



	physx::PxCapsuleController * getController();

	void onEntityCreated(const TMsgEntityCreated& msg);

};

