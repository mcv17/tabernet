#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "PxPhysicsAPI.h"
#include "components/common/comp_transform.h"
#include "components/player/comp_interactor.h"

// DISCARDED. BOSS WON'T ENTER.

class TCompDemonController : public TCompBase {

public:
	static const float DEFAULT_HEIGHT;
	static const float DEFAULT_RADIUS;

	DECL_SIBLING_ACCESS();

	void debugInMenu();
	void renderDebug();
	void load(const json & j, TEntityParseContext& ctx);
	void update(float dt);
	static void registerMsgs();

	// Actions
	void move(const VEC3 & movement);
	void setRotation(const VEC3 & rotation);
	void setRotation(float yaw, float pitch, float roll = 0.0f);
	void setPosition(const VEC3 & position);
	void resize(float height);	// use always this method to modify height of controller
	
	// Setters
	void setRadius(float radius);
	void setSlopeLimit(float slopeLimit);
	void setStepOffset(float stepOffset);

	// Messages.
	void onLogicStatus(const TMsgLogicStatus & msg);
private:
	physx::PxVec3 _displacementDelta; // How much it will move this update.
	bool _gravity = true;

	void simulate(float dt);
	void applyGravity();

	physx::PxCapsuleController * getController();

};

