#pragma once

/* These classes allow for the control of a weight (a float value) based on different logics. 
They are mostly used for IK control of the alpha variable that controls the effect overlay of
the animation by the IK. */

namespace WeightController {

	class IWeightController {
	protected:
		float amount = 0.0f;

	public:
		virtual void update(const float dt) = 0;
		const float& getAmount() { return amount; }
	};


	class TLinearFadeInFadeOutWeightController : public IWeightController {
	public:
		void update(const float dt) override {
			return linear(start, end, ratio);
		}
	};

	/* Controls the */
	class TFootIKWeightController : public IWeightController {
	public:
		void update(const float dt) override {
			return linear(start, end, ratio);
		}
	};


}