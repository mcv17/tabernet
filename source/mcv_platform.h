#pragma once

// Global settings
#define _CRT_SECURE_NO_WARNINGS     // Don't warn about using fopen..
#define WIN32_LEAN_AND_MEAN         // Reduce the size of things included in windows.h
#define _USE_MATH_DEFINES           // M_PI M_PI_2

// Windows/OS Platform
#define NOMINMAX                    // To be able to use std::min without windows problems
#include <windows.h>

// DirectX
#include <d3d11.h>
#pragma comment (lib, "d3d11.lib") 

// C/C++
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <cstdarg>
#include <cstdint>        // uint32_t
#include <algorithm>
#include <vector> // Better if we don't add STL containers here (exception being this one), keeps code cleaner and more organized.
#include <string>

/* Engine headers common in many parts of the engine. */
// Only add general functionality headers that are common in all the engine.
#include "profiling/Remotery.h"
// Utility
#include "utils/utils.h"
#include "windows/windows_utils.h"
#include "utils/rng.h"
#define RNG RandomNumberGenerator::getGlobalInstance()
#define LOGGER(str) Utils::dbg(str);

// Resources.
#include "resources/resource.h"

// Imgui
#include "imgui/imgui_source/imgui.h"

// Geometry.
#include "geometry/geometry.h"

// Render.
#include "render/render.h"

// Handles.
#include "handle/handle.h"

// Profiling.
#include "profiling/profiling.h"

