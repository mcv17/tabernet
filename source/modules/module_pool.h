#pragma once
#include "modules/module.h"
#include "entity/entity.h"

//We use this enum to create all the pools that we will need
enum PoolType { 
	Melee, 
	Ranged, 
	Immortal, 
	RangedShooter, 
	Explosion, 
	RangedShooterBuffed,
	RangedShooterSniperGround, 
	RangedShooterSniperRoof, 
	Size }; 
//Size always the last elment, and it is just for the for loop

class CModulePool : public IModule
{
	struct TPoolEntity {
		bool active = false;
		CHandle handle;
	};

	//All the pool information
	struct TPool {
		PoolType pt;
		std::vector<TPoolEntity> pool;
		int defaultCapacity;
		int currentCapacity = 0;
		std::string prefabPath;
		CHandle rootEntity;
	};

	struct TEntityToBeRemoved {
		CHandle handle;
		PoolType poolType;
		float time;
	};

	CHandle poolsParent;
	std::vector<TEntityToBeRemoved> _entitiesToBeRemoved;

	//Pools
	std::map<std::string, TPool> pools;

	// Check if module already started
	bool _started = false;

	//Creates all the entities needed to contain the pools
	void createEntityContainers();

	//Creates all the pools from the enum PoolType and reads the config from the json modules/pool.json
	void createPools();

	//Spawns a new entity, disables it, and add it to the back of the pool
	CHandle spawnInPool(PoolType pt, bool active = false);

	//Fills the pool with new entities, if full ignores the call
	void startPool(PoolType pt);

	//Fills all the available pools with new entities
	void startAllPools();

	// Removes entities if necessary
	void checkEntitiesTimeToLive(float dt);

public:
	const char *PoolTypeStr[8] = { 
		"Melee", 
		"Ranged", 
		"Immortal", 
		"RangedShooter", 
		"Explosion", 
		"RangedShooterBuffed",
		"RangedShooterSniperGround",
		"RangedShooterSniperRoof"
	};

	CModulePool(const std::string& name);
	bool start() override;
	void update(float dt) override;
	void renderDebug() override{}
	void renderInMenu();

	CHandle spawn(PoolType pt);
	CHandle spawn(PoolType pt, VEC3 pos);
	CHandle spawn(PoolType pt, VEC3 pos, QUAT rot);	
	CHandle spawn(PoolType pt, float timeToLive);
	CHandle spawn(PoolType pt, VEC3 pos, float timeToLive);
	CHandle spawn(PoolType pt, VEC3 pos, QUAT rot, float timeToLive);
	bool disableEntity(CHandle e);
	bool disableEntity(CHandle e, PoolType pt);
	void disableAllPools();
	void killAllActiveEntities();

	void restartControllers();
};