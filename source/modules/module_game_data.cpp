#include "mcv_platform.h"
#include "module_game_data.h"
#include "../components/pickups/PickUpUtils.h"
#include "../utils/surface_querier.h"

CModuleGameData::CModuleGameData(const std::string & name) : IModule(name) {}

bool CModuleGameData::start() {
	// Load all your modules here.

	// Load pickup functions static class. Fetches information about pickups drop rates
	// for different types of enemies. Manages the spawning logic for deciding what pickup
	// to get.
	PickUpFunctions::SpawnDropsManager::Init("data/modules/game_data/spawnPickups.json");

	// Load surface querier data into the static class.
	SurfaceQuerier::SurfaceQuerierManager::Init("data/modules/game_data/surfaces_game.json");

	return true;
}

void CModuleGameData::renderInMenu(){
	PickUpFunctions::SpawnDropsManager::renderInMenu();
}