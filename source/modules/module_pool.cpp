#include "mcv_platform.h"
#include "module_pool.h"
#include "engine.h"
#include "utils/json_resource.h"
#include "entity/entity_parser.h"
#include "components/common/comp_group.h"
#include "components/common/comp_name.h"
#include "components/common/comp_transform.h"
#include "components/controllers/comp_character_controller.h"
#include "handle/handle.h"
#include "entity/entity.h"

CModulePool::CModulePool(const std::string& name)
  : IModule(name)
{}

bool CModulePool::start()
{
	if (_started)
		disableAllPools();
	else {
		createEntityContainers();
		createPools();
		startAllPools();
		_started = true;
	}
	
	return true;
}

void CModulePool::update(float dt) {
	checkEntitiesTimeToLive(dt);
}

void CModulePool::createEntityContainers() {
	// Create a new fresh entity to store the pools
	poolsParent.create<CEntity>();

	// Cast to entity object
	CEntity* e = poolsParent;

	// Create a new instance of the TCompGroup, TCompName & TCompTransform
	CHandle h_group = getObjectManager<TCompGroup>()->createHandle();
	CHandle h_name = getObjectManager<TCompName>()->createHandle();
	CHandle h_trans = getObjectManager<TCompTransform>()->createHandle();
	TCompName* name = h_name;
	name->setName("Module_Pooling");
	TCompTransform* trans = h_trans;
	trans->setPosition(VEC3(0.f, 0.f, 0.f));

	// Add it to the entity
	e->setComponent(h_group.getType(), h_group);
	e->setComponent(h_name.getType(), h_name);
	e->setComponent(h_trans.getType(), h_trans);
}

void CModulePool::createPools() {
	json cfg = Utils::loadJson("data/modules/pool.json");

	CEntity* root = poolsParent;
	CHandle h_group = root->getComponent<TCompGroup>();

	for (int i = 0; i < PoolType::Size; i++)
	{
		if (cfg.count(PoolTypeStr[i]) && cfg[PoolTypeStr[i]].is_object()) { //If found in the json
			//Get the current json object
			auto& cfgPool = cfg[PoolTypeStr[i]]; 

			std::vector<TPoolEntity> entityPool;
			TPool tpool;

			tpool.pt = static_cast<PoolType>(i);
			tpool.defaultCapacity = cfgPool.value("capacity", 10);
			tpool.prefabPath = cfgPool["prefabPath"];
			tpool.pool = entityPool;

			assert(!(tpool.prefabPath.empty()) && "[ModuleBoot::createPools] The prefab path for %s has not been found in the json. ", PoolTypeStr[i]);

			//Create the entity that will hold the pool entities
			tpool.rootEntity.create<CEntity>();

			// Cast to entity object
			CEntity* e = tpool.rootEntity;

			// Create a new instance of the TCompGroup, TCompName & TCompTransform
			CHandle h_group_child = getObjectManager<TCompGroup>()->createHandle();
			CHandle h_name = getObjectManager<TCompName>()->createHandle();
			CHandle h_trans = getObjectManager<TCompTransform>()->createHandle();
			TCompName* name = h_name;
			name->setName(tpool.prefabPath.c_str());
			TCompTransform* trans = h_trans;
			trans->setPosition(VEC3(0.f, 0.f, 0.f));

			// Add it to the entity
			e->setComponent(h_group_child.getType(), h_group_child);
			e->setComponent(h_name.getType(), h_name);
			e->setComponent(h_trans.getType(), h_trans);

			//Finally add this entity to the root entity of the pooling module
			TCompGroup* c_group = h_group;
			c_group->add(tpool.rootEntity);

			pools[PoolTypeStr[i]] = tpool;
		}		
	}
}

void CModulePool::startAllPools() {
	for (int i = 0; i < PoolType::Size; i++)
		startPool(static_cast<PoolType>(i));
}

void CModulePool::checkEntitiesTimeToLive(float dt) {
	std::vector<TEntityToBeRemoved>::iterator it;
	for (it = _entitiesToBeRemoved.begin(); it != _entitiesToBeRemoved.end(); it++) {
		TEntityToBeRemoved& entityToBeRemoved = *it;
		entityToBeRemoved.time -= dt;
		if (entityToBeRemoved.time <= 0.0f) {
			disableEntity(entityToBeRemoved.handle, entityToBeRemoved.poolType);
			it = _entitiesToBeRemoved.erase(it);
			if (it == _entitiesToBeRemoved.end()) break;
		}
	}
}

void CModulePool::startPool(PoolType pt) {
	for (int i = 0; i < pools[PoolTypeStr[pt]].defaultCapacity; i++)
		spawnInPool(pt);
}

CHandle CModulePool::spawnInPool(PoolType pt, bool active) {
	TMsgToogleEntity msgToggle = { false };
	TMsgLogicStatus msgLogic = { false };
	TEntityParseContext ctx;

	//Get the pool information
	TPool* tpool = &pools[PoolTypeStr[pt]];

	parseScene(tpool->prefabPath, ctx);
	Utils::dbg("[ModulePool:spawn] Parsing new enemy " + std::string(PoolTypeStr[pt]));

	//Add it to its group
	CEntity* e = tpool->rootEntity;
	TCompGroup* c_group = e->getComponent<TCompGroup>();
	c_group->add(ctx.entities_loaded[0]);

	//We send the msg to disable its components
	ctx.entities_loaded[0].sendMsg(msgToggle);
	ctx.entities_loaded[0].sendMsg(msgLogic);

	TPoolEntity tpentity;
	tpentity.active = active;
	tpentity.handle = ctx.entities_loaded[0];

	tpool->pool.push_back(tpentity);
	tpool->currentCapacity++;

	return tpentity.handle;
}

CHandle CModulePool::spawn(PoolType pt) {
	TPool* tpool = &pools[PoolTypeStr[pt]];
	std::vector<TPoolEntity>* pool = &tpool->pool;

	TMsgToogleEntity msgToggle = { true };
	TMsgLogicStatus msgLogic = { true };
	TMsgResetComponent msgReset;

	bool spawned = false;
	CHandle h = CHandle();
	//Search for a disabled entity to reuse
	for (int i = 0; i < pool->size(); i++)
	{
		if (pool->at(i).active)
			continue;
		
		spawned = true;
		h = pool->at(i).handle;
		pool->at(i).active = true;
		break;
	}

	//The pool is full and we need to parse a new entity and add it (true because this will be active)
	if (!spawned)
		h = spawnInPool(pt, true);

	//Activate the entity and reset components
	CEntity* e = h;
	e->sendMsg(msgReset);
	e->sendMsg(msgToggle);
	e->sendMsg(msgLogic);

	return h;
}

CHandle CModulePool::spawn(PoolType pt, VEC3 pos) {
	return spawn(pt, pos, QUAT::Identity);
}

CHandle CModulePool::spawn(PoolType pt, VEC3 pos, QUAT rot) {
	CHandle h = spawn(pt);
CEntity* e = h;

TCompCollider* col = e->getComponent<TCompCollider>();

if (col && col->controller) {
	col->controller->setFootPosition(VEC3_TO_PXEXTVEC3(pos));
	TCompCharacterController* c_con = e->getComponent<TCompCharacterController>();
	if (c_con)
		c_con->setColliderEnabledLastFrame(pos);
}
else if (col && col->actor) {
	CTransform trans;
	trans.setPosition(pos);
	trans.setRotation(rot);
	col->actor->setGlobalPose(toPxTransform(trans));
}

TCompTransform* c_trans = e->getComponent<TCompTransform>();
if (c_trans) {
	c_trans->setPosition(pos);
	c_trans->setRotation(rot);
}

TCompCharacterController* c_charCon = e->getComponent<TCompCharacterController>();
if (c_charCon && c_trans) {
	float yaw, pitch;
	c_trans->getAngles(&yaw, &pitch);
	c_charCon->teleport(pos);
	c_charCon->setYaw(yaw);
	c_charCon->setPitch(pitch);
}

return h;
}

CHandle CModulePool::spawn(PoolType pt, float timeToLive) {
	TEntityToBeRemoved entityToBeRemoved;
	entityToBeRemoved.handle = spawn(pt);
	entityToBeRemoved.poolType = pt;
	entityToBeRemoved.time = timeToLive;
	_entitiesToBeRemoved.push_back(entityToBeRemoved);
	return entityToBeRemoved.handle;
}

CHandle CModulePool::spawn(PoolType pt, VEC3 pos, float timeToLive) {
	TEntityToBeRemoved entityToBeRemoved;
	entityToBeRemoved.handle = spawn(pt, pos);
	entityToBeRemoved.poolType = pt;
	entityToBeRemoved.time = timeToLive;
	_entitiesToBeRemoved.push_back(entityToBeRemoved);
	return entityToBeRemoved.handle;
}

CHandle CModulePool::spawn(PoolType pt, VEC3 pos, QUAT rot, float timeToLive) {
	TEntityToBeRemoved entityToBeRemoved;
	entityToBeRemoved.handle = spawn(pt, pos, rot);
	entityToBeRemoved.poolType = pt;
	entityToBeRemoved.time = timeToLive;
	_entitiesToBeRemoved.push_back(entityToBeRemoved);
	return entityToBeRemoved.handle;
}

bool CModulePool::disableEntity(CHandle h) {
	//TODO Improve: Check if has tag or some other way of finding its type first

	//If not, search all the pools
	for (int i = 0; i < PoolType::Size; i++)
	{
		if (disableEntity(h, static_cast<PoolType>(i)))
			return true;
	}

	return false;
}

bool CModulePool::disableEntity(CHandle h, PoolType pt) {
	for (int i = 0; i < pools[PoolTypeStr[pt]].pool.size(); i++)
	{
		if (h == pools[PoolTypeStr[pt]].pool.at(i).handle) {
			if (pools[PoolTypeStr[pt]].pool.at(i).active) {
				TMsgToogleEntity msgToggle = { false };
				TMsgLogicStatus msgLogic = { false };

				CEntity* e = h;
				e->sendMsg(msgToggle);
				e->sendMsg(msgLogic);

				pools[PoolTypeStr[pt]].pool.at(i).active = false;
			}
			return true;
		}
	}

	return false;
}

void CModulePool::restartControllers() {
	TMsgDeleteAndCreateController msg;

	for (auto& pool : pools) {
		for (auto& entity : pool.second.pool) {
			if (entity.handle.isValid()) {
				CEntity* e = entity.handle;
				e->sendMsg(msg);
			}
		}
	}
}

void CModulePool::disableAllPools() {
	TMsgToogleEntity msgToggle = { false };
	TMsgLogicStatus msgLogic = { false };

	for (auto& pool : pools) {
		for (auto& entity : pool.second.pool){
			if (entity.active && entity.handle.isValid()) {
				CEntity* e = entity.handle;
				e->sendMsg(msgToggle);
				e->sendMsg(msgLogic);
				entity.active = false;
			}
		}
	}
}

void CModulePool::killAllActiveEntities() {
	TMsgToogleEntity msgToggle = { false };
	TMsgLogicStatus msgLogic = { false };
	TMsgResetComponent msgReset;

	for (auto& pool : pools) {
		for (auto& entity : pool.second.pool) {
			if (entity.active && entity.handle.isValid()) {
				EngineLogicManager.killEntity(entity.handle);
			}
		}
	}
}

void CModulePool::renderInMenu() {
	for (int i = 0; i < PoolType::Size; i++)
	{
		//Each pool
		if (ImGui::TreeNode(PoolTypeStr[i])) {
			//Each entity in the pool and its status

			TPool* tpool = &pools[PoolTypeStr[i]];
			for (int j = 0; j < tpool->pool.size(); j++)
			{
				CEntity* e = tpool->pool.at(j).handle;
				if (ImGui::Button("Reset")) {
					TMsgResetComponent msg;
					e->sendMsg(msg);
				}
				ImGui::SameLine();
				e->debugInMenu();
			}
			ImGui::TreePop();
		}
		
	}
}