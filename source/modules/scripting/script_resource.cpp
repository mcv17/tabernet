#include "mcv_platform.h"
#include "script_resource.h"
#include "engine.h"

class CScriptResourceType : public CResourceType {
public:
	const char* getExtension(int idx) const override { return "lua"; }

	const char* getName() const override { return "Scripts"; }

	IResource * create(const std::string & filename) const override {
		CScript * new_res = new CScript(filename);
		new_res->setNameAndType(filename, this);
		return new_res;
	}
};

CScript::CScript(const std::string & filename) : _path(filename) {}

void CScript::onFileChanged(const std::string& filename) {
	// get file as text
	if (filename != getName())
		return;

	// parse script
	if (_env)
		EngineScripting.doFile(_path, *_env);
	else
		EngineScripting.doFile(_path);
}

const std::string CScript::getLuaCode() const {
	return Utils::loadTextFile(_path);
}

void CScript::setEnvironment(sol::environment & env) {
	_env = &env;
}

void CScript::renderInMenu() {
	ImGui::Text("%s", getLuaCode().c_str());
}

template<> const CResourceType* getResourceTypeFor<CScript>() {
	static CScriptResourceType resource_type;
	return &resource_type;
}