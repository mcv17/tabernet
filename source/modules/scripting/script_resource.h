#pragma once

#include "resources/resource.h"

class CScript : public IResource {

public:
	CScript(const std::string & filename);

	void renderInMenu() override;

	void onFileChanged(const std::string & filename) override;

	const std::string getLuaCode() const;
	const std::string & getPath() const { return _path; }

	void setEnvironment(sol::environment & env);

private:
	const std::string _path;
	sol::environment * _env = nullptr;

};
