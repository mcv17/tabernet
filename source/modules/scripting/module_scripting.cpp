#include "mcv_platform.h"
#include "entity/entity.h"
#include "module_scripting.h"
#include "components/common/comp_health.h"
#include "components/common/comp_script.h"
#include "script_resource.h"
#include "test_scripting.h"
#include <filesystem>

ComponentEventHandle::ComponentEventHandle(CHandle & nCompHandle, const EventID & nEventIDToCall) : ComponentHandle(nCompHandle), EventIDToCall(nEventIDToCall) {}

bool isEntityActive(CHandle& handle) {
	CEntity* entity = handle;
	return entity->isActive();
}

void ComponentEventHandle::CallEventForScript(CHandle CompHandleOfScriptToBindToEvent) {
	EngineScripting.CallEvent(ComponentHandle, EventIDToCall, CompHandleOfScriptToBindToEvent);
}

void ComponentEventHandle::CallEventForAllScripts() {
	EngineScripting.CallEventAllScript(ComponentHandle, EventIDToCall);
}

bool CModuleScripting::start() {
	// Load some libraries for LUA.
	// https://simion.com/info/lua_base.html
	_lua.open_libraries(sol::lib::base);
	// http://lua-users.org/wiki/StringLibraryTutorial
	_lua.open_libraries(sol::lib::string);
	// http://lua-users.org/wiki/CoroutinesTutorial
	_lua.open_libraries(sol::lib::coroutine);
	// http://lua-users.org/wiki/MathLibraryTutorial
	_lua.open_libraries(sol::lib::math);
	// http://lua-users.org/wiki/OsLibraryTutorial
	_lua.open_libraries(sol::lib::os);
	_lua.open_libraries(sol::lib::table);

	TimedEvents = std::vector<TimedScriptEvent>();

	// Bind different basic structures from our engine so they can be used by LUA.
	declareBasics();
	
	// Interpret scripts for events, like triggers for example.
	declareGeneralFunctions();

	// Start script for initializations and the such.
	const CScript * initScript = EngineResources.getResource("data/scripts/init.lua")->as<CScript>();
	doFile(initScript->getPath());

	return true;
}

void CModuleScripting::declareBasics() {
	// Here we should declare basic functions like dbg or basic classes like vector, quaternion, etc...

	// Define CHandle to be accessible by LUA.
	_lua.new_usertype<CHandle>("CHandle",
		"new", sol::constructors<CHandle(), CHandle(uint32_t, uint32_t, uint32_t)>(),
		"isValid", &CHandle::isValid,
		"asUnsigned", &CHandle::asUnsigned,
		"asVoidPtr", &CHandle::asVoidPtr,
		"destroy", &CHandle::destroy,
		"isEntityActive", &isEntityActive
		);
	
	// Vector3
	_lua.new_usertype<Vector3>(
		// name in LUA
		"Vector3",
		// constructor
		"new", sol::constructors<Vector3(), Vector3(float), Vector3(float, float, float)>(),
		// static
		"up", sol::var(Vector3::Up),
		"down", sol::var(Vector3::Down),
		"left", sol::var(Vector3::Left),
		"right", sol::var(Vector3::Right),
		"forward", sol::var(Vector3::Forward),
		"backward", sol::var(Vector3::Backward),
		// "sum", [](const Vector3 & a, const Vector3 & b) { return a + b; }, 
		// operators
		sol::meta_function::addition, [](const Vector3 & a, const Vector3 & b) { return a + b; },
		sol::meta_function::subtraction, [](const Vector3 & a, const Vector3 & b) { return a - b; },
		sol::meta_function::multiplication, sol::overload
		(
			[](const Vector3 & a, float b) { return a * b; },
			[](float b, const Vector3 & a) { return a * b; },
			[](const Vector3 & a, const Vector3 & b) { return a * b; }
		),
		sol::meta_function::division, [](const Vector3 & a, float b) { return a / b; },
		sol::meta_function::equal_to, [](const Vector3 & a, const Vector3 & b) { return a == b; },
		sol::meta_function::unary_minus, [](const Vector3 & a) { return -a; },
		sol::meta_function::to_string, [](const Vector3 & a)
		{
			std::stringstream sstream;
			sstream << "(" << a.x << ", " << a.y << ", " << a.z << ")";
			return sstream.str();
		},
		// attributes
		"x", &Vector3::x,
		"y", &Vector3::y,
		"z", &Vector3::z,
		// methods
		"add", &Vector3::operator+=,
		"diff", &Vector3::operator-=,
		"scale", sol::resolve<Vector3 & (float)>(&Vector3::operator*=),
		"mult", sol::resolve<Vector3 & (const Vector3 &)>(&Vector3::operator*=),
		"dot", &Vector3::Dot,
		"cross", sol::resolve<Vector3(const Vector3 &) const>(&Vector3::Cross),
		"length", &Vector3::Length,
		"transform", sol::resolve<Vector3 (const Vector3 &, const Quaternion&)>(&Vector3::Transform),
		"normalize", sol::resolve<void()>(&Vector3::Normalize));

	// Vector4
	_lua.new_usertype<Vector4>(
		// name in LUA
		"Vector4",
		// constructor
		"new", sol::constructors<Vector4(), Vector4(float), Vector4(float, float, float, float)>(),
		// operators
		sol::meta_function::addition, [](const Vector4 & a, const Vector4 & b) { return a + b; },
		sol::meta_function::subtraction, [](const Vector4 & a, const Vector4 & b) { return a - b; },
		sol::meta_function::multiplication, sol::overload
		(
			[](const Vector4 & a, float b) { return a * b; },
			[](float b, const Vector4 & a) { return a * b; },
			[](const Vector4 & a, const Vector4 & b) { return a * b; }
		),
		sol::meta_function::division, [](const Vector4 & a, float b) { return a / b; },
		sol::meta_function::equal_to, [](const Vector4 & a, const Vector4 & b) { return a == b; },
		sol::meta_function::unary_minus, [](const Vector4 & a) { return -a; },
		sol::meta_function::to_string, [](const Vector4 & a)
		{
			std::stringstream sstream;
			sstream << "(" << a.x << ", " << a.y << ", " << a.z << ")";
			return sstream.str();
		},
		// attributes
		"x", &Vector4::x,
		"y", &Vector4::y,
		"z", &Vector4::z,
		"w", &Vector4::w,
		// methods
		"add", &Vector4::operator+=,
		"diff", &Vector4::operator-=,
		"scale", sol::resolve<Vector4 & (float)>(&Vector4::operator*=),
		"mult", sol::resolve<Vector4 & (const Vector4 &)>(&Vector4::operator*=),
		"dot", &Vector4::Dot,
		"cross", sol::resolve<Vector4(const Vector4 &, const Vector4 &) const>(&Vector4::Cross),
		"length", &Vector4::Length,
		"transform", sol::resolve<Vector4(const Vector4 &, const Quaternion&)>(&Vector4::Transform),
		"normalize", sol::resolve<void()>(&Vector4::Normalize)
	);

	// Quaternion
	_lua.new_usertype<Quaternion>(
		"Quaternion",
		// constructor
		"new", sol::constructors <Quaternion(), Quaternion(float, float, float, float), Quaternion(const Quaternion &)>(),
		// attributes
		"x", &Quaternion::x,
		"y", &Quaternion::y,
		"z", &Quaternion::z,
		"w", &Quaternion::w,
		// methods
		"add", &Quaternion::operator+=,
		"dif", &Quaternion::operator-=,
		"scale", sol::resolve<Quaternion & (float)>(&Quaternion::operator*=),
		"mult", sol::resolve<Quaternion & (const Quaternion &)>(&Quaternion::operator*=),
		"dot", &Quaternion::Dot,
		"length", &Quaternion::Length,
		"CreateFromYawPitchRoll", &Quaternion::CreateFromYawPitchRoll
		);
	
	// static (this needs to be defined outside)
	_lua["Quaternion"]["identity"] = Quaternion::Identity;

	// Transform
	_lua.new_usertype<CTransform>(
		"Transform",
		sol::constructors<CTransform(), CTransform(Vector3, Quaternion, float)>(),
		"getPosition", &CTransform::getPosition,
		"getRotation", &CTransform::getRotation,
		"setPosition", &CTransform::setPosition,
		"setRotation", &CTransform::setRotation,
		"getScale", &CTransform::getScale,
		"setScale", sol::overload
		(
			sol::resolve<void (VEC3)>(&CTransform::setScale),
			sol::resolve<void(float)>(&CTransform::setScale)
		),
		"getFront", &CTransform::getFront,
		"getUp", &CTransform::getUp,
		"getLeft", &CTransform::getLeft,
		"lookAt", &CTransform::lookAt,
		"getYawPitchRoll", &CTransform::getYawPitchRoll,
		"getDeltaYawToAimTo", &CTransform::getDeltaYawToAimTo
		);

	LogicManager::declareInLua();

	// For global variable Time
	_lua.new_usertype<TElapsedTime>(
		"TElapsedTime",
		"new", sol::no_constructor,
		"delta", sol::readonly(&TElapsedTime::delta)
		);

	_lua.new_usertype<RandomNumberGenerator>(
		"RNGClass",
		"new", sol::no_constructor,
		"i", sol::overload( 
			sol::resolve<int(int)>(&RandomNumberGenerator::i),
			sol::resolve<int(int, int)>(&RandomNumberGenerator::i)
		),
		"f", sol::overload(
			sol::resolve<float(float)>(&RandomNumberGenerator::f),
			sol::resolve<float(float, float)>(&RandomNumberGenerator::f)
		),
		"b", &RandomNumberGenerator::b
		);
	// Time
	_lua.new_usertype<CClock>
		(
			"CClock",
			sol::constructors<CClock(float)>(),
			"start", &CClock::start,
			"stop", &CClock::stop,
			"pause", &CClock::pause,
			"resume", &CClock::resume,
			"setTime", &CClock::setTime,
			"setRandomTime", &CClock::setRandomTime,
			"isStarted", &CClock::isStarted,
			"isPaused", &CClock::isPaused,
			"isFinished", &CClock::isFinished,
			"isStopped", &CClock::isStopped,
			"initialTime", &CClock::initialTime,
			"elapsed", &CClock::elapsed,
			"remaining", &CClock::remaining
			);

	// Modules
	_lua.new_usertype<Input::TButton>
		(
			"TButton",
			"new", sol::no_constructor,
			"isPressed", &Input::TButton::isPressed
		);
	_lua.new_usertype<Input::CModuleInput>
		(
			"ModuleInputClass",
			"new", sol::no_constructor,
			sol::meta_function::index, sol::resolve<const Input::TButton& (const std::string&) const>(&Input::CModuleInput::operator[])
		);
	
	// Global functions
	_lua["dbg"] = sol::resolve<void(const sol::variadic_args &)>(&Utils::dbg);
	_lua["getEntity"] = &getEntityByName;

	// Global variables
	_lua["LogicManager"] = sol::readonly_property(LogicManager::Instance());
	_lua["Time"] = &Time;
	_lua["RNG"] = &RNG;
	_lua["Input"] = &EngineInput;
}

void CModuleScripting::declareGeneralFunctions() {
	// Load global functions.
	const CScript * initScript = EngineResources.getResource("data/scripts/global.lua")->as<CScript>();
	doFile(initScript->getPath());

	// Now, load all events scripts on load so they can be called from any other script.
	for (auto& file : std::filesystem::recursive_directory_iterator("data/scripts/events/")) {
		std::string ext = file.path().extension().generic_string();
		std::transform(ext.begin(), ext.end(), ext.begin(), ::toupper);
		if (ext == ".LUA") {
			const CScript * script = EngineResources.getResource(file.path().string())->as<CScript>();
			doFile(script->getPath());
		}
	}
}

void CModuleScripting::update(float delta) {
	if (EngineInput[VK_F11].justPressed())
		envCall("test", _testEnv);

	// Hold size before calling functions as they may add new timed events.
	int TimedEventsSizeBeforeExecutingFunctions = TimedEvents.size();
	for (int i = 0; i < TimedEventsSizeBeforeExecutingFunctions; ++i) {
		TimedScriptEvent & Event = TimedEvents[i];
		Event.TimeToCallEvent -= delta;
		if (Event.TimeToCallEvent <= 0.0f) {
			TCompScript * ScriptHandle = Event.scriptEvent.ScriptComponentHandle;
			if (!ScriptHandle) continue;
			ScriptHandle->executeFunction(Event.scriptEvent.FunctionToCall);
		}
	}

	TimedEvents.erase(std::remove_if(TimedEvents.begin(), TimedEvents.end(), [](const TimedScriptEvent & obj) {
		return obj.TimeToCallEvent <= 0.0f;
		}), TimedEvents.end());
}

void CModuleScripting::renderInMenu() {
	if (ImGui::SmallButton("Execute test()"))
		envCall("test", _testEnv);
}

void CModuleScripting::onEntitiesLoaded() {
	// Load the test environment and the test functions. These functions are used for debugging mostly.
	_testEnv = sol::environment(_lua, sol::create, _lua.globals());
	CScript * testScript = EngineResources.getEditableResource("data/scripts/test.lua")->as<CScript>();
	testScript->setEnvironment(_testEnv);
	doFile(testScript->getPath(), _testEnv);
	_testEnv["cppTest"] = &cppTest;

	// Load UI environment and scripts
	_uiEnv = sol::environment(_lua, sol::create, _lua.globals());
	for (auto& file : std::filesystem::recursive_directory_iterator("data/scripts/ui/")) {
		std::string ext = file.path().extension().generic_string();
		std::transform(ext.begin(), ext.end(), ext.begin(), ::toupper);
		if (ext == ".LUA") {
			const CScript * script = EngineResources.getResource(file.path().string())->as<CScript>();
			doFile(script->getPath(), _uiEnv);
		}
	}

	call("onEntitiesLoaded");
}

void CModuleScripting::bindEntityToLua(const std::string & name, CHandle entity) {
	// bind reference to LUA
	_lua[name] = entity;
}

bool CModuleScripting::existsInLua(const char * objName) {
	return (_lua[objName] != sol::lua_nil);
}

bool CModuleScripting::existsInLua(const std::string & objName) {
	return (_lua[objName] != sol::lua_nil);
}

bool CModuleScripting::existsInLuaEnv(const std::string & objName, sol::environment & env) {
	return (env[objName] != sol::lua_nil);
}

bool CModuleScripting::doFile(const std::string & path) {
	try {
		// execute the script
		_lua.script_file(path);
	}
	catch (sol::error & e) {
		dealWithError(e);
		return false;
	}

	return true;
}

bool CModuleScripting::doFile(const std::string & path, sol::environment & env) {
	try {
		// execute the script
		_lua.script_file(path, env);
	}
	catch (sol::error & e) {
		dealWithError(e);
		return false;
	}

	return true;
}

bool CModuleScripting::doString(const std::string & code) {
	try {
		// execute the script
		_lua.script(code);
	}
	catch (sol::error & e) {
		dealWithError(e);
		return false;
	}

	return true;
}

bool CModuleScripting::doString(const std::string & code, sol::environment & env) {
	try {
		// execute the script
		_lua.script(code, env);
	}
	catch (sol::error & e) {
		dealWithError(e);
		return false;
	}

	return true;
}


void CModuleScripting::dealWithError(const sol::error & e) {
	Utils::error("ERROR PARSING LUA\n%s", e.what());
}

ComponentEventHandle CModuleScripting::RegisterNewComponentEventForScripts(CHandle CompHandleFiringEvent, const std::string & EventID){
	if (!ComponentEvents.count(CompHandleFiringEvent))
		ComponentEvents[CompHandleFiringEvent] = ComponentEventData();

	auto & Handles = ComponentEvents[CompHandleFiringEvent].EventsForComponents;
	if (Handles.count(EventID)) return ComponentEventHandle(CompHandleFiringEvent, EventID);

	Handles[EventID] = EventData();
	return ComponentEventHandle(CompHandleFiringEvent, EventID);
}

bool CModuleScripting::UnregisterNewComponentEventForScripts(CHandle CompHandleFiringEvent, const std::string & EventID){
	if (!ComponentEvents.count(CompHandleFiringEvent)) return false;

	ComponentEvents[CompHandleFiringEvent].EventsForComponents.erase(EventID);
	if (ComponentEvents[CompHandleFiringEvent].EventsForComponents.size() == 0)
		ComponentEvents.erase(CompHandleFiringEvent);
	return true;
}

bool CModuleScripting::BindScriptToComponentEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent, const std::string & FunctionToFireOnEventCall){
	// Register new event.
	RegisterNewComponentEventForScripts(CompHandleFiringEvent, EventID);

	EventData & Eventdata = ComponentEvents[CompHandleFiringEvent].EventsForComponents[EventID];
	if (Eventdata.ScriptsBindedToEvent.count(CompHandleOfScriptToBindToEvent)) return false;

	Eventdata.ScriptsBindedToEvent[CompHandleOfScriptToBindToEvent] = FunctionToFireOnEventCall;
	return true;
}

bool CModuleScripting::UnbindScriptToComponentEvent(CHandle CompHandleWithEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent){
	// Component firing event was not registered.
	if (!ComponentEvents.count(CompHandleWithEvent)) return false;

	// Is event registered.
	auto & EventsForComponent = ComponentEvents[CompHandleWithEvent].EventsForComponents;
	if (!EventsForComponent.count(EventID)) return false;

	if (!EventsForComponent[EventID].ScriptsBindedToEvent.count(CompHandleWithEvent)) return false;
	EventsForComponent[EventID].ScriptsBindedToEvent.erase(CompHandleWithEvent);

	// Remove if no events are registered.
	if (EventsForComponent[EventID].ScriptsBindedToEvent.size() == 0)
		UnregisterNewComponentEventForScripts(CompHandleWithEvent, EventID);

	return true;
}

void CModuleScripting::BindTimedScriptEvent(CHandle CompHandleOfScriptToBindToEvent, const std::string & FunctionToFireOnEventCall, float TimeBeforeCalling) {
	TimedEvents.push_back(TimedScriptEvent(ScriptEvent(CompHandleOfScriptToBindToEvent, FunctionToFireOnEventCall), TimeBeforeCalling));
}

void CModuleScripting::ClearTimedScriptEvents() {
	TimedEvents.clear();
}

void CModuleScripting::CallEventAllScript(CHandle CompHandleFiringEvent, const std::string & EventID){
	if (!ComponentEvents.count(CompHandleFiringEvent)) return;

	auto & EventsForComponents = ComponentEvents[CompHandleFiringEvent].EventsForComponents;
	if (!EventsForComponents.count(EventID)) return;

	for (auto & Script : EventsForComponents[EventID].ScriptsBindedToEvent) {
		TCompScript * ScriptHandle = Script.first;
		if (!ScriptHandle) continue;
		ScriptHandle->executeFunction(Script.second);
	}

	return;
}

void CModuleScripting::CallEventAllScript(const ComponentEventHandle & Handle) {
	CallEventAllScript(Handle.ComponentHandle, Handle.EventIDToCall);
}

void CModuleScripting::CallEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent) {
	if (!ComponentEvents.count(CompHandleFiringEvent)) return;

	auto & EventsForComponents = ComponentEvents[CompHandleFiringEvent].EventsForComponents;
	if (!EventsForComponents.count(EventID)) return;

	if (!EventsForComponents[EventID].ScriptsBindedToEvent.count(CompHandleOfScriptToBindToEvent)) return;

	TCompScript * ScriptHandle = CompHandleOfScriptToBindToEvent;
	if (!ScriptHandle) return;
	ScriptHandle->executeFunction(EventsForComponents[EventID].ScriptsBindedToEvent[CompHandleOfScriptToBindToEvent]);
	return;
}

void CModuleScripting::CallEvent(const ComponentEventHandle & Handle, CHandle CompHandleOfScriptToBindToEvent) {
	CallEvent(Handle.ComponentHandle, Handle.EventIDToCall, CompHandleOfScriptToBindToEvent);
}