#include "mcv_platform.h"
#include "engine.h"
#include "module_audio.h"
#include <components\controllers\comp_character_controller.h>

#pragma comment(lib, "fmod_vc.lib" )
#pragma comment(lib, "fmodstudio_vc.lib")

bool CModuleAudio::start()
{
	FMOD_RESULT r;

	/* Init FMOD */
	r = Studio::System::create(&studio);
	r = studio->initialize(1024, FMOD_STUDIO_INIT_NORMAL || FMOD_STUDIO_INIT_LIVEUPDATE || FMOD_STUDIO_INIT_DEFERRED_CALLBACKS, FMOD_INIT_NORMAL, 0);
	assert(r == FMOD_OK && "[ModuleAudio::start] FMOD Studio could not initialize correctly");

	/* Load mandatory banks */
	r = studio->loadBankFile("data/audio/Master.bank", FMOD_STUDIO_LOAD_BANK_NORMAL, &_masterBank);
	r = studio->loadBankFile("data/audio/Master.strings.bank", FMOD_STUDIO_LOAD_BANK_NORMAL, &_stringsBank);
	assert(r == FMOD_OK && "[ModuleAudio::start] Could not load master banks");

	/* Load gamestate audio */
	/* TODO: Change to main menu */
	loadGSAudio("gs_gametesting");

	return r == FMOD_OK;
}

void CModuleAudio::stop()
{
	// Unload all banks
	for (auto b : _banks) {
		b.second.bank->unloadSampleData();
		b.second.bank->unload();
	}

	for (auto e : _events) {
		e.second->release();
	}

	_masterBank->unload();
	_stringsBank->unload();
	_banks.clear();
	_events.clear();
}

void CModuleAudio::update(float dt)
{
	CEntity* cameraEntity = EngineLogicManager.getOutputCamera();
	CEntity* player = getEntityByName("Player");
	if (player != nullptr && cameraEntity != nullptr) {
		if (TCompTransform * cameraTransform = cameraEntity->getComponent<TCompTransform>()) {

			FMOD_3D_ATTRIBUTES atts;
			atts.forward = getFMODVector3(-cameraTransform->getFront()); // Don't know why but it has to be negative
			atts.position = getFMODVector3(cameraTransform->getPosition());
			atts.up = getFMODVector3(cameraTransform->getUp());

			// TODO Keep in mind
			// We use the player velocity instead of the camera because it's easily accesible from the controller
			// It shouldn't matter much but it would be more correct with the output camera velocity
			if (TCompCharacterController* controller = player->getComponent< TCompCharacterController>()) {
				atts.velocity = getFMODVector3(controller->getCurrentVelocity());
			}

			studio->setListenerAttributes(0, &atts);
		}
	}

	studio->update();
}

void CModuleAudio::renderInMenu()
{
	ImGui::Text("Banks");
	for (auto b : _banks) {
		if (ImGui::TreeNode(b.first.c_str())) {
			for (auto e : b.second.events) {
				ImGui::PushID(e.c_str());
				ImGui::Text(e.c_str());
				ImGui::SameLine();
				if (ImGui::Button("Play")) {
					playEvent(e);
				}
					
				ImGui::SameLine();

				if (ImGui::Button("Stop")) {
					stopEvent(e);
				}
				ImGui::PopID();
			}
			ImGui::TreePop();
		}
	}
}

void CModuleAudio::playEvent(std::string path, VEC3 location)
{
	FMOD_RESULT r;
	if (_events.find(path) != _events.end()) {
		Studio::EventInstance* i = _events.at(path);
		
		i->setVolume(1.0f);
		i->setPaused(false);
		if (location != VEC3(0, 0, 0)) {
			FMOD_3D_ATTRIBUTES attributes;
			FMOD_VECTOR fmodPosition;
			fmodPosition.x = location.x;
			fmodPosition.y = location.y;
			fmodPosition.z = location.z;

			attributes.position = fmodPosition;


			i->set3DAttributes(&attributes);
		}
		r = i->start();

		if(r != FMOD_OK)
			Utils::dbg("Error playing: %s", path.c_str());
	}
	else
		Utils::dbg("%s not found", path.c_str());
}

void CModuleAudio::setEventParameter(std::string path, std::string paramName, float paramValue) {
	
	FMOD_RESULT r;
	if (_events.find(path) != _events.end()) {
		Studio::EventInstance* i = _events.at(path);

		i->setParameterByName(paramName.c_str(), paramValue);
	}
	else
		Utils::dbg("%s not found", path.c_str());
}

FMOD_VECTOR CModuleAudio::getFMODVector3(VEC3 v)
{
	FMOD_VECTOR fv;
	fv.x = v.x;
	fv.y = v.y;
	fv.z = v.z;

	return fv;
}

void CModuleAudio::stopEvent(std::string path)
{
	FMOD_RESULT r;
	if (_events.find(path) != _events.end()) {
		Studio::EventInstance* i = _events.at(path);
		r = i->stop(FMOD_STUDIO_STOP_IMMEDIATE);

		if (r != FMOD_OK)
			Utils::dbg("Error stoping: %s", path.c_str());
	}
	else
		Utils::dbg("%s not found", path.c_str());
}


void CModuleAudio::stopAllSounds()
{
	for (const auto& event : _events) {
		event.second->stop(FMOD_STUDIO_STOP_IMMEDIATE);
	}
}

void CModuleAudio::loadGSAudio(std::string gs)
{
	PROFILE_FUNCTION("ModuleAudio::loadGSAudio");


	json jsonFile = Utils::loadJson("data/audio/audio.json");
	json j = jsonFile[gs];
	json params = jsonFile["params"];

	for (std::string bankPath : j) {
		
		/* Get the bank's name from the path */
		std::string jsonName = bankPath.substr(bankPath.find_last_of('/') + 1, bankPath.size());
		std::string bankName = jsonName.substr(0, jsonName.size() - 5);
		//

		// Register bank with a blocking mode
		FMOD_RESULT r;
		Studio::Bank* b = NULL;
		r = studio->loadBankFile(bankPath.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &b);
		assert(r == FMOD_OK && b != nullptr && "[ModuleAudio::loadGSAudio] Could not load bank: %s", bankName);

		Bank bank;
		bank.name = bankName;
		bank.bank = b;

		/* Get Events from bank */
		Studio::EventDescription** events = nullptr;
		int numEvents = 0;
		int count = 0;
		b->getEventCount(&numEvents);

		events = new Studio::EventDescription*[numEvents];
		b->getEventList(events, numEvents, &count);

		for (int i = 0; i < count; i++) {
			FMOD_RESULT er;
			/* Load sample data */
			assert(events[i]->isValid() && "[ModuleAudio::loadGSAudio] Event is not valid in bank: %s", bankName);
			er = events[i]->loadSampleData();

			/* Create event instance */
			Studio::EventInstance* ei = NULL;
			er = events[i]->createInstance(&ei);
			assert(er == FMOD_OK && ei != nullptr && "[ModuleAudio::loadGSAudio] Could not load event from bank: %s", bankName);

			/* Get event name */
			char evtPath[512];
			er = events[i]->getPath(evtPath, 512, nullptr);
			std::string str = evtPath;
			assert(er == FMOD_OK && !str.empty() && "[ModuleAudio::loadGSAudio] Could not find path");

			_events.insert(std::pair<std::string, Studio::EventInstance*>(str, ei));

			// Register event to bank
			bank.events.push_back(str);
		}

		/* Register bank */
		_banks.insert(std::pair<std::string, Bank>(bankName, bank));
		
	}
}
