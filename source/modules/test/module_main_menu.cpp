#include "mcv_platform.h"
#include "module_main_menu.h"
#include "engine.h"
#include "input/input.h"
#include "ui/module_ui.h"
#include "entity/entity_parser.h"
#include "components/camera/comp_camera.h"
#include "ui/widgets/ui_progress.h"
#include "ui/widgets/ui_button.h"

CModuleMainMenu::CModuleMainMenu(const std::string& name)
  : IModule(name)
{}

bool CModuleMainMenu::start()
{
	EngineUI.activateWidget("black_screen");
	startedFadeIn = false;
	return true;
}

void CModuleMainMenu::stop()
{
	EngineUI.deactivateWidget("main_menu");
	firstFrame = true;
}

void CModuleMainMenu::declareInLua() {
	sol::environment& uiEnv = EngineScripting.getUIEnvironment();
	uiEnv.new_usertype< CModuleMainMenu>("CModuleMainMenu", "new", sol::no_constructor, "startGame", &CModuleMainMenu::onOptionStart);
	uiEnv["ModuleMainMenu"] = this;
}

void CModuleMainMenu::handlePlayer() {
	TCompSkeleton* c_skel = getPlayerMenuSkeleton();
	TCompParticles* c_particles = getPlayerMenuParticles();
	
	if (!c_skel || !c_particles)
		return;
	float animPercentage = c_skel->getAnimationPercentage("fb_menu_idle");

	if (animPercentage >= 0.6f && !spawnedSmokeInLoop) {
		c_particles->spawnParticles(300);
		spawnedSmokeInLoop = true;
	}

	if (animPercentage < 0.6f && spawnedSmokeInLoop)
		spawnedSmokeInLoop = false;
}

void CModuleMainMenu::onOptionStart()
{
	Utils::dbg("[ModuleMainMenu]: START");

	EngineAudio.playEvent("event:/UI/GameStart");
	EngineAudio.setEventParameter("event:/UI/GameStart", "LoadingScreenActivated", 1.0f);
	EngineAudio.setEventParameter("event:/Ambience/MainMenu", "MenuAmbienceOn", 0.0f);

	//Set progress bar to 0
	UI::CProgress* progressBar = dynamic_cast<UI::CProgress*>(Engine.getUI().getWidgetByAlias("loading_progress_bar"));
	if (progressBar)
		progressBar->setRatio(0.0f);	

	// Play start animation
	getPlayerMenuAnimator()->setMenuState(1);

	isStarting = true;
}

void CModuleMainMenu::starting() {

	// Get animation state and percentage
	int animationState = std::get<int>(getPlayerMenuAnimator()->getVariable("menu")->_value); // 0 idle // 1 starting // -1 none
	float animPercentage = getPlayerMenuSkeleton()->getAnimationPercentage("fb_menu_start");

	if (!startedFadeIn && animPercentage >= 0.5f) {
		EngineLogicManager.fadeInBlackScreen();
		startedFadeIn = true;
	}

	if (animationState == 2) {
		// If animation finished
		//Activate the loading screen widget
		Engine.getUI().activateWidget("loading_screen");
		//Disable the main menu widget
		Engine.getUI().deactivateWidget("main_menu");
		//Load the next gamestate entities
		EngineModules.changeToGamestateNonBlocking("gs_gametesting");

		EngineLogicManager.fadeOutBlackScreen("BlackScreenFadeOutInstant");

		//EngineUI.unregisterController(menu);
		isStarting = false;
	}
}

void CModuleMainMenu::onOptionContinue()
{
	//Engine.getModules().changeToGamestate("gameplay");
	Utils::dbg("[ModuleMainMenu]: CONTINUE");
}

void CModuleMainMenu::onOptionExit()
{
	Utils::dbg("[ModuleMainMenu]: EXIT");

	CApplication::instance().sendQuitMsg(); //Hack to close the app
}

void CModuleMainMenu::update(float dt)
{
	// Check for UI has initialized in order to run the fades and show logo
	if (EngineUI.getImageWidgetByName("black_screen", "black_screen_image") != nullptr && !_logoStarted) {
		_logoScreenClock.start();
		EngineUI.activateWidget("logo");
		EngineLogicManager.fadeInBlackScreen("FinalBlackScreenFadeOut");
		_logoStarted = true;
		return;
	}

	// Play logo screen
	if (_logoScreenClock.isStarted() && !_logoScreenClock.isFinished()) {
		// Check if splash time is finished and then transition to main menu with a fade
		if (_logoScreenClock.elapsed() > _logoScreenClock.initialTime() - 1.5f && !_fadeOutNotStarted) {
			EngineLogicManager.fadeInBlackScreen("BlackScreenFadeTransition");
			_fadeOutNotStarted = true;
		}
		return;
	}

	// Play the UPF logo
	if (EngineUI.getImageWidgetByName("black_screen", "black_screen_image") != nullptr && !_upfLogoStarted) {
		_upfLogoClock.start();
		EngineUI.deactivateWidget("logo");
		EngineUI.activateWidget("upfLogo");
		//EngineLogicManager.fadeInBlackScreen("FinalBlackScreenFadeOut");
		_upfLogoStarted = true;
		_fadeOutNotStarted = false;
		return;
	}

	// Check UPF logo
	if (_upfLogoClock.isStarted() && !_upfLogoClock.isFinished()) {
		// Check if splash time is finished and then transition to main menu with a fade
		if (_upfLogoClock.elapsed() > _upfLogoClock.initialTime() - 1.5f && !_fadeOutNotStarted) {
			EngineLogicManager.fadeInBlackScreen("BlackScreenFadeTransition");
			_fadeOutNotStarted = true;
		}
		return;
	}

	// Check for UI has initialized in order to run the fades
	if (EngineUI.getImageWidgetByName("black_screen", "black_screen_image") != nullptr && !_splashStarted) {
		_splashScreenClock.start();
		EngineUI.deactivateWidget("upfLogo");
		EngineUI.activateWidget("splash");
		//EngineLogicManager.fadeInBlackScreen("FinalBlackScreenFadeOut");
		_splashStarted = true;
		_fadeOutNotStarted = false;
	}
	// Play splash screen
	if (_splashScreenClock.isStarted() && !_splashScreenClock.isFinished()) {
		// Check if splash time is finished and then transition to main menu with a fade
		if (_splashScreenClock.elapsed() > _splashScreenClock.initialTime() - 1.5f && !_fadeOutNotStarted) {
			EngineLogicManager.fadeInBlackScreen("BlackScreenFadeTransition");
			_fadeOutNotStarted = true;
		}
	}
	else if (_splashScreenClock.isFinished() && !_mainMenuRunning) {
		_mainMenuRunning = true;
		EngineUI.deactivateWidget("splash");
		getPlayerMenuAnimator()->setMenuState(0);
	}

	if (_mainMenuRunning) {
		if (firstFrame && _splashStarted) {
			getPlayerMenuAnimator()->setMenuState(0);
		}

		if (firstFrame) {
			//Get the ui and activate the main menu widget
			EngineUI.activateWidget("main_menu");
			EngineAudio.playEvent("event:/Ambience/MainMenu");
			EngineAudio.setEventParameter("event:/Ambience/MainMenu", "MenuAmbienceOn", 1.0f);
			declareInLua();
			firstFrame = false;
		}

		if (isStarting) {
			starting();
		}

		handlePlayer();

		// Close the app in the main menu
		if (EngineInput["pause"].justPressed() && !isStarting) {
			CApplication::instance().sendQuitMsg();
		}
	}
}

void CModuleMainMenu::runSplashScreen()
{
	
}
