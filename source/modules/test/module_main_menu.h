#pragma once

#include "modules/module.h"
#include "ui/controllers/ui_menu_controller.h"

#include "components/animations/comp_player_menu_animator.h"
#include "skeleton/comp_skeleton.h"
#include "components/common/comp_particles.h"

class CModuleMainMenu : public IModule
{
	float desiredMS = 16.6f;
	float currentMS = 0.0f;
	bool isStarting = false;
	bool firstFrame = true;

	// Used to know if needs to spawn smoke particles in the animation loop
	bool spawnedSmokeInLoop = false;

	// Used when performing a blackScreenFadeIn
	bool startedFadeIn = false;
	bool _logoStarted = false;
	bool _upfLogoStarted = false;
	bool _splashStarted = false;
	// Time�3s from fade + how much you want the splash to be on screen
	CClock _logoScreenClock = CClock(7.5f);
	CClock _upfLogoClock = CClock(6.0f);
	CClock _splashScreenClock = CClock(6.0f);
	UI::CMenuController* menu;

	void onOptionStart();
	void onOptionContinue();
	void onOptionExit();
	void starting();

	void declareInLua();

	void handlePlayer();

	DECL_TCOMP_ACCESS("PlayerMenu", TCompPlayerMenuAnimator, PlayerMenuAnimator);
	DECL_TCOMP_ACCESS("PlayerMenu", TCompSkeleton, PlayerMenuSkeleton);
	DECL_TCOMP_ACCESS("Mouth_Player", TCompParticles, PlayerMenuParticles);

public:
	CModuleMainMenu(const std::string& name);

	bool start() override;
	void stop() override;
	void update(float dt) override;
	void renderDebug() override {}
private:
	void runSplashScreen();
	bool _mainMenuRunning = false;
	bool _fadeOutNotStarted = false;
};