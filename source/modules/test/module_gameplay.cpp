#include "mcv_platform.h"
#include "module_gameplay.h"
#include "engine.h"
#include "utils/utils.h"
#include "ui/widgets/ui_progress.h"
#include "modules/module_camera_mixer.h"
#include "components/postFX/comp_postFX_blur.h"
#include "components/postFX/comp_postFX_focus.h"
#include "components/common/comp_tags.h"
#include "ui/widgets/ui_image.h"
#include "ui/ui_widget.h"

CModuleGameplay::CModuleGameplay(const std::string& name) : IModule(name) {}

bool CModuleGameplay::start() {
	return true;
}

void CModuleGameplay::stop() {
	EnginePool.disableAllPools();
	EnginePhysics.restartScene();
	EnginePool.restartControllers();

	_pauseUIActive = false;
	_firstFrame = true;
	togglePauseUI(false);
	EngineUI.deactivateWidget("hud");
	EngineAudio.stopAllSounds();
	EngineLogicManager.removeEntitiesByTag("pickup");
	cleanStatusEffects();
}

void CModuleGameplay::update(float dt) {
	if (_firstFrame) {
		declareInLua();
		_firstFrame = false;
		_startDelayCinematic.start();
	}

	if (_startDelayCinematic.isFinished() && _startDelayCinematic.isStarted()) {
		_startDelayCinematic.stop();
		EngineLogicManager.CallGlobalScriptFunction("StartIntroCinematic");
	}

	if (EngineInput["shift"].isPressed() && EngineInput["back"].justPressed()) {
		//Set progress bar to 0
		UI::CProgress* progressBar = dynamic_cast<UI::CProgress*>(Engine.getUI().getWidgetByAlias("loading_progress_bar"));
		if (progressBar)
			progressBar->setRatio(0.0f);

		//Activate the loading screen widget
		Engine.getUI().activateWidget("loading_screen");
		//Disable the main menu widget
		Engine.getUI().deactivateWidget("hud");
		//Load the next gamestate entities
		EngineModules.changeToGamestate("gs_main_menu");
	}

	if (EngineInput["pause"].justPressed()) {
		_pauseUIActive = !_pauseUIActive;
		togglePauseUI(_pauseUIActive);
	}

	if (_pauseUIActive)
		checkInputModeChanged();

}

void CModuleGameplay::togglePauseUI(bool active) {
	// Disable hud, toggle blur and enable pause widget
	if (active) {
		// Toggle widgets and images
		EngineUI.deactivateWidget("hud");
		EngineUI.activateWidget("pause_menu");
		_pauseUIKeyboard = !EngineInput.isControllerActive();
		togglePauseUIControlsImage(_pauseUIKeyboard);

		// Blur & Focus
		CEntity* e = EngineCameraMixer.getMixerOuputCamera();
		TCompPostFXBlur* c_blur = e->getComponent<TCompPostFXBlur>();
		TCompPostFXFocus* c_focus = e->getComponent<TCompPostFXFocus>();
		c_focus->setFocusMultiplier(0.0f);

		// TimeRatio to 0
		EngineLogicManager.setTimeScale(0.0f);

		// Enable mouse for the UI
		EngineLogicManager.setActiveMouse(true);
		EngineLogicManager.setCenterMouseActive(false);

		// Disable player input
		EngineLogicManager.setActivePlayerInput(false);
	}
	// Enable hud, toggle blur, and disable pause widget
	else {
		// Toggle widgets
		EngineUI.activateWidget("hud");
		EngineUI.deactivateWidget("pause_menu");

		// Blur & Focus
		CEntity* e = EngineCameraMixer.getMixerOuputCamera();
		TCompPostFXBlur* c_blur = e->getComponent<TCompPostFXBlur>();
		TCompPostFXFocus* c_focus = e->getComponent<TCompPostFXFocus>();
		c_focus->setFocusMultiplier(1.0f);

		// TimeRatio to 1
		EngineLogicManager.setTimeScale(1.0f);

		// Disable the mouse to control the player again
		EngineLogicManager.setActiveMouse(true);
		EngineLogicManager.setCenterMouseActive(true);

		// Enable player input
		EngineLogicManager.setActivePlayerInput(true);
	}
}

void CModuleGameplay::togglePauseUIControlsImage(bool keyboard) {
	auto keyImage = EngineUI.getImageWidgetByName("controls", "keyboard");
	if (keyImage) keyImage->getParams()->visible = keyboard;
	auto conImage = EngineUI.getImageWidgetByName("controls", "gamepad");
	if (conImage) conImage->getParams()->visible = !keyboard;
}

void CModuleGameplay::checkInputModeChanged() {
	// We check if the controller state is the same as the keyboard stored. If they are the same we need to update the image
	if (EngineInput.isControllerActive() == _pauseUIKeyboard) {
		_pauseUIKeyboard = !_pauseUIKeyboard;
		togglePauseUIControlsImage(_pauseUIKeyboard);
	}
}

void CModuleGameplay::closePauseUI() {
	togglePauseUI(false);
	_pauseUIActive = false;
}

void CModuleGameplay::openPauseUI() {
	togglePauseUI(true);
	_pauseUIActive = true;
}

void CModuleGameplay::onMainMenu() {
	// Disable keybindings images
	auto keyImage = EngineUI.getImageWidgetByName("controls", "keyboard");
	if (keyImage) keyImage->getParams()->visible = false;
	auto conImage = EngineUI.getImageWidgetByName("controls", "gamepad");
	if (conImage) conImage->getParams()->visible = false;

	// Disable pause widget
	EngineUI.deactivateWidget("pause_menu");
	//Activate the loading screen widget
	Engine.getUI().activateWidget("loading_screen");
	//Load the next gamestate entities
	EngineModules.changeToGamestate("gs_main_menu");
}

void CModuleGameplay::declareInLua() {
	sol::environment& uiEnv = EngineScripting.getUIEnvironment();
	uiEnv.new_usertype<CModuleGameplay>
		(
			"CModuleGameplay", 
			"new", 
			sol::no_constructor, 
			"continue", &CModuleGameplay::closePauseUI,
			"main_menu", &CModuleGameplay::onMainMenu
		);
	uiEnv["ModuleGameplay"] = this;
}

void CModuleGameplay::cleanStatusEffects()
{
	TMsgClearStatusEffects msg;
	if (CEntity* player = getEntityByName("Player")) {
		player->sendMsg(msg);
	}
}
