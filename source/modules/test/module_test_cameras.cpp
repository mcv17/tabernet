#include "mcv_platform.h"
#include "module_test_cameras.h"
#include "engine.h"
#include "input/input.h"
#include "modules/module_camera_mixer.h"
#include "geometry/curve.h"
#include "components/common/comp_transform.h"
#include "components/camera/comp_curve_controller.h"

CModuleTestCameras::CModuleTestCameras(const std::string& name)
  : IModule(name)
{}

bool CModuleTestCameras::start()
{
  Engine.getCameraMixer().setDefaultCamera(getEntityByName("Camera_Default"));
  Engine.getCameraMixer().setOutputCamera(getEntityByName("Camera_Output"));

  return true;
}

void CModuleTestCameras::stop()
{
}

void CModuleTestCameras::update(float dt)
{
  const float kBlendTime = 1.f;
  static Interpolator::TQuadInOutInterpolator quadInt;
  static Interpolator::TBackOutInterpolator backInt;
  static Interpolator::TBounceOutInterpolator bounceInt;

  if (EngineInput[VK_F1].justPressed())
  {
    CHandle hCamera = getEntityByName("Camera_A");
    Engine.getCameraMixer().blendCamera(hCamera, kBlendTime, &quadInt);
  }
  else if (EngineInput[VK_F2].justPressed())
  {
    CHandle hCamera = getEntityByName("Camera_B");
    Engine.getCameraMixer().blendCamera(hCamera, kBlendTime/*, &backInt*/);
  }
  else if(EngineInput[VK_F3].justPressed())
  {
    CHandle hCamera = getEntityByName("Camera_C");
    Engine.getCameraMixer().blendCamera(hCamera, kBlendTime/*, &bounceInt*/);
  }
  else if (EngineInput[VK_F4].justPressed())
  {
    CHandle hCamera = getEntityByName("Camera_C");
    Engine.getCameraMixer().blendCamera(hCamera, 0.f);
  }

  CHandle hCameraTarget = getEntityByName("moving_camera_target");
  if (hCameraTarget.isValid())
  {
    static float time = 0.f;
    time += dt;
    const float ratio = sin(time);
    const VEC3 pos = VEC3::Lerp(VEC3(-10, 0, 0), VEC3(10, 0, 0), ratio);

    CEntity* eCameraTarget = hCameraTarget;
    TCompTransform* cTransform = eCameraTarget->getComponent<TCompTransform>();
    cTransform->setPosition(pos);

    CHandle hCurveController = getEntityByName("curve_test");
    CEntity* eCurve = hCurveController;
    TCompCurveController* cCurvecontroller = eCurve->getComponent<TCompCurveController>();
    const float curveRatio = (ratio + 1.f) * 0.5f;
    cCurvecontroller->setRatio(curveRatio);
  }
}

void CModuleTestCameras::renderDebug()
{
}
