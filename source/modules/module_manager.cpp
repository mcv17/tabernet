#include "mcv_platform.h"
#include "module_manager.h"
#include "module_boot.h"
#include "utils/json.hpp"
#include <fstream>
#include "engine.h"

void CModuleManager::init()
{
	loadModulesFromConfig("data/modules.json");
	loadGamestatesFromConfig("data/gamestates.json");
	
	startModules( _system_modules );

	if(!_startGamestate.empty())
		changeToGamestate(_startGamestate);
}

void CModuleManager::stop()
{
	if (_currentGamestate)
		stopModules(*_currentGamestate);
	stopModules(_system_modules); // better in reverse order
}

void CModuleManager::clear() {
	stop();
	_all_modules.clear(); // Here we free all memory allocated for each module and remove the pointers.
	_system_modules.clear();
	_render_debug_modules.clear();
	_update_modules.clear();
	_loading_modules.clear();
	_gamestates.clear();
	_currentGamestate = nullptr;
	_requestedGamestate = nullptr;
	_startGamestate.clear();
}

void CModuleManager::registerGameModule(IModule * module)
{
	_all_modules.addModule(module);
}

void CModuleManager::registerSystemModule(IModule * module)
{
	_all_modules.addModule(module);
	_system_modules.push_back(module);
}

void CModuleManager::changeToGamestate(const std::string & name) {
	// Check if valid
	CGamestate* gs = getGamestate(name);
	if (!gs)
		return;

	// Check for situations that shouldn't happen.
	assert(_modulesLoadingFlags == 0, "We shouldn't call a gamestate change if there are modules still loading");
	
	// If we aren't going from the gs_main_menu to the gs_gametesting, we shouldn't have modules still starting
	if (_currentGamestate && !(_currentGamestate->_name == "gs_main_menu" && name == "gs_gametesting") && _modulesStartingFlags > 0) {
		std::string fatalError = "We shouldn't go from " + _currentGamestate->_name + " to " + name + " while there are still modules starting";
		Utils::fatal(fatalError.c_str());
	}

	assert(_currentGamestate != gs, "The same gamestate is already running");
	_requestedGamestate = gs;
}

void CModuleManager::changeToGamestateNonBlocking(const std::string& name) {
	// Perform all the checks to change the game state
	changeToGamestate(name);

	// And turn on the flags of the modules with a non-blocking update
	setIsLoadingModule(ModulesLoading::Boot, true);
	// If the instancing module is still starting, change it to loading without blocking
	if (getIsStartingModule(ModulesLoading::Instancing)) {
		setIsLoadingModule(ModulesLoading::Instancing, true);
		// And clear it from starting modules
		setIsStartingModule(ModulesLoading::Instancing, false);
	}
}

void CModuleManager::update(float dt)
{
	PROFILE_FUNCTION("CModuleManager::update");
	updateGamestate(dt); // We change of gamestate if a new one was requested.

	if (_modulesLoadingFlags) { // Any flag from any module on
		_loadingTimeRemaining = _frameDuration;

		// NonBlocking load time variables
		std::chrono::time_point<std::chrono::steady_clock> start;
		std::chrono::time_point<std::chrono::steady_clock> end;
		start = std::chrono::steady_clock::now();

		for (auto module : _loading_modules) {
			std::string profileString = module->getName() + "::update";
			if (module->isActive()) {
				PROFILE_FUNCTION(profileString.c_str());
				module->update(dt);

				// Update the remaining time after the module updated
				end = std::chrono::steady_clock::now();
				std::chrono::duration<double> diff = end - start;
				_loadingTimeRemaining -= diff.count();
			}
		}
		if (_modulesLoadingFlags == 0) //All flags off when finished non-blocking load
			finishedUpdatingGamestate(dt);
	}
	else {
		for (auto module : _update_modules) {
			std::string profileString = module->getName() + "::update";
			if (module->isActive()) {
				PROFILE_FUNCTION(profileString.c_str());
				module->update(dt);
			}
		}
	}
}

void CModuleManager::renderDebug()
{
	CGpuScope gpu_trace("Modules::renderDebug");
	for (auto module : _render_debug_modules) {
		if (module->isActive()){
			CGpuScope gpu_trace(module->getName().c_str());
			module->renderDebug();
		}
	}
}

void CModuleManager::startModules( VModules & modules )
{
  PROFILE_FUNCTION("CModuleManager::startModules");
  for (auto module : modules) {
    if (module->isActive()) continue;
    TFileContext fc(module->getName());
    PROFILE_FUNCTION(module->getName().c_str());
    module->setActive(module->start());
  }
}

void CModuleManager::stopModules( VModules & modules )
{
	for (auto module : modules) {
		if (module->isActive()) {
			module->stop();
			module->setActive(false);
		}
	}
}

void CModuleManager::setIsLoadingModule(ModulesLoading module, bool isLoading) {
	if (isLoading)
		_modulesLoadingFlags |= module;
	else
		_modulesLoadingFlags &= ~module;
}

bool CModuleManager::getIsLoadingModule(ModulesLoading module) {
	if ((_modulesLoadingFlags & module) > 0)
		return true;
	return false;
}

void CModuleManager::setIsStartingModule(ModulesLoading module, bool isStarting) {
	if (isStarting)
		_modulesStartingFlags |= module;
	else
		_modulesStartingFlags &= ~module;
}

bool CModuleManager::getIsStartingModule(ModulesLoading module) {
	if ((_modulesStartingFlags & module) > 0)
		return true;
	return false;
}

void CModuleManager::updateGamestate(float & dt)
{
	if (!_requestedGamestate)
		return;

	// Stop game modules
	if (_currentGamestate)
		stopModules(*_currentGamestate);
	
	// Start game modules
	startModules(*_requestedGamestate);
	_currentGamestate = _requestedGamestate;
	_requestedGamestate = nullptr;

	// We start a nonBlocking load, if not, we load all the entities for this gamestate in one batch
	if (_modulesLoadingFlags)
		EngineBoot.startLoadGamestateNonBlocking(_currentGamestate->_name);
	else
		EngineBoot.loadGamestateBlocking(_currentGamestate->_name);
	
	// If we are loading all in one batch we end here, and do the scripting calls
	if (!_modulesLoadingFlags) 
		finishedUpdatingGamestate(dt);
}

void CModuleManager::finishedUpdatingGamestate(float & dt) {
	EngineScripting.call("onGamestateChange");
	EngineScripting.call(_currentGamestate->_name);
	// We clear the entities after loading a new gamestate
	CHandleManager::destroyAllPendingObjects();

	// Reset delta time to prevent a frame with a delta time the size of the time loading.
	Application.resetTimeSinceLastRender();
	dt = 0.0f;
}

void CModuleManager::loadModulesFromConfig( const std::string & pathToConfigFile )
{
	json jsonData = Utils::loadJson(pathToConfigFile);

	_update_modules.clear();
	_render_debug_modules.clear();
	
	for (auto & moduleName : jsonData["update"])
	{
		IModule* module = getModule(moduleName);
		if(module)
			_update_modules.push_back(module);
	}

	for (auto& moduleName : jsonData["update_while_loading"])
	{
		IModule* module = getModule(moduleName);
		if (module)
			_loading_modules.push_back(module);
	}
	
	for (auto& moduleName : jsonData["render_debug"])
		activateRenderDebug(moduleName);

	// Frame limit when non-blocking load
	_frameDuration = 1.0f / jsonData.value<float>("frames_while_loading", 30.0f);
}

void CModuleManager::activateRenderDebug(const std::string & moduleNameToAdd) {
	IModule* module = getModule(moduleNameToAdd);
	if (module)
		_render_debug_modules.push_back(module);
}

void CModuleManager::removeRenderDebug(const std::string & moduleNameToRemove) {
	for (auto & module = _render_debug_modules.begin(); module != _render_debug_modules.end(); module++)
	{
		if ((*module)->getName() == moduleNameToRemove) {
			_render_debug_modules.erase(module);
			break;
		}
	}
}

bool CModuleManager::isModuleInRenderDebug(const std::string & moduleNameToRemove) {
	for (auto & module = _render_debug_modules.begin(); module != _render_debug_modules.end(); module++)
		if ((*module)->getName() == moduleNameToRemove)
			return true;
	return false;
}

// Loads modules from config file.
void CModuleManager::loadGamestatesFromConfig(const std::string & pathToGamestatesConfigFile) {
	json jsonData = Utils::loadJson(pathToGamestatesConfigFile);
	json& jsonGamestates = jsonData["gamestates"];

	for (auto& jsonGs : jsonGamestates.items())
	{
		CGamestate gs;

		gs._name = jsonGs.key();
		for (auto& jsonModule : jsonGs.value())
		{
			IModule* module = getModule(jsonModule);
			gs.push_back(module);
		}
		_gamestates.addGamestate(gs);
	}

	_startGamestate = jsonData["start"].get<std::string>();
}

IModule * CModuleManager::getModule(const std::string & moduleName)
{
	return _all_modules.findModule(moduleName);
}

CGamestate * CModuleManager::getGamestate(const std::string & gameStateName)
{
	return _gamestates.findGameState(gameStateName);
}

const std::string CModuleManager::getCurrentGS()
{
	return _currentGamestate->_name;
}

const std::string CModuleManager::getRequestedGS() {
	if (_requestedGamestate)
		return _requestedGamestate->_name;
	return "";
}

const std::string CModuleManager::getStartGS() {
	return _startGamestate;
}

void CModuleManager::renderInMenu() {
	renderModulesInMenu("All...", _all_modules._modules);
	renderModulesInMenu("System modules", _system_modules);
	renderModulesInMenu("Render Debug modules", _render_debug_modules);
	renderModulesInMenu("Update modules", _update_modules);
	renderModulesInMenu("Loading modules", _loading_modules);
}

void CModuleManager::renderActiveModules() {
	renderActiveModulesInMenu("Render Debug modules", _render_debug_modules);
	renderActiveModulesInMenu("Update modules", _update_modules);
	renderActiveModulesInMenu("System modules", _system_modules);
}

void CModuleManager::renderFollowedModules(const std::unordered_set <std::string> & pinnedModules) {
	ImGui::PushID("Pinned-Modules"); // Ids are used to differentiate elements, allow differentiation between nodes with same label.
	
	int modulesWritten = 0;
	for (auto & m : _all_modules._modules) {
		if (pinnedModules.find(m->getName()) == pinnedModules.end()) continue;
		
		modulesWritten++;
		
		// Maybe a little bit redundant to add this but i like it.
		std::string isActive = m->isActive() ? " - Activated" : " - Deactivated";
		if (ImGui::TreeNode((m->getName() + isActive).c_str())) {
			m->renderInMenu();
			ImGui::TreePop();
		}
	}
	
	if (modulesWritten == 0)
		ImGui::Text("No pinned modules were found.");
	
	ImGui::PopID();
}

void CModuleManager::renderGamestate() {
	std::string gamestateText = "Current GameState is: " + _currentGamestate->_name;
	ImGui::Text(gamestateText.c_str());
}

void CModuleManager::renderModulesInMenu(const char* title, VModules& some_modules) {
	if (!ImGui::CollapsingHeader(title))
		return;

	ImGui::PushID(title); // Ids are used to differentiate elements, allow differentiation between nodes with same label.
	if (ImGui::TreeNode("Active modules")) {
		int modulesWritten = 0;
		for (auto& m : some_modules) {
			bool moduleActive = m->isActive();
			if (moduleActive == false) continue;
			modulesWritten++;

			// Maybe a little bit redundant to add this but i like it.
			std::string isActive = " - Activated";
			const std::string & moduleName = m->getName();
			if (ImGui::TreeNode((moduleName + isActive).c_str())) {
				m->renderInMenu();

				bool activated = isModuleInRenderDebug(moduleName);

				ImGui::Spacing(0, 5);
				if (ImGui::Checkbox("Activate module renderDebug", &activated))
					activated ? activateRenderDebug(moduleName) : removeRenderDebug(moduleName);
				ImGui::Spacing(0, 5);

				ImGui::TreePop();
			}
		}

		if (modulesWritten == 0)
			ImGui::Text("No active modules were found.");
		ImGui::TreePop();
	}

	if (ImGui::TreeNode("Deactivated modules")) {
		int modulesWritten = 0;
		for (auto & m : some_modules) {
			bool moduleActive = m->isActive();
			if (moduleActive) continue;
			modulesWritten++;

			// Maybe a little bit redundant to add this but i like it.
			std::string isActive = " - Deactivated";
			if (ImGui::TreeNode((m->getName() + isActive).c_str())) {
				m->renderInMenu();
				ImGui::TreePop();
			}
		}

		if (modulesWritten == 0)
			ImGui::Text("No deactivated modules were found.");
		ImGui::TreePop();
	}
	ImGui::PopID();
}

void CModuleManager::renderActiveModulesInMenu(const char* title, VModules& some_modules) {
	if (!ImGui::CollapsingHeader(title))
		return;

	ImGui::PushID(title); // Ids are used to differentiate elements, allow differentiation between nodes with same label.

	int modulesWritten = 0;
	for (auto& m : some_modules) {
		if (m->isActive() == false) continue;
		modulesWritten++;
		if (ImGui::TreeNode(m->getName().c_str())) {
			m->renderInMenu();
			ImGui::TreePop();
		}
	}
	if (modulesWritten == 0)
		ImGui::Text("No active modules were found.");
	
	ImGui::PopID();
}
