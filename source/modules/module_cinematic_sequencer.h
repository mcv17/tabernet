#pragma once

#include "modules/module.h"

class CCinematicSequencer : public IModule
{
	CHandle CurrentCinematicPlaying;
	CHandle CameraUsedBeforeCinematic;
	bool OnCinematicEndReturnToPreviousCamera = true;

	std::map<std::string, CHandle> LevelSequencesPlaying;

public:
	CCinematicSequencer(const std::string & name);

	bool start() override;
	void update(float dt) override;
	void renderDebug() override;

	// Cinematics are scripts that affect the ingame camera. When a cinematic is started,
	// the previous camera is stored. Once the cinematic ends, if EndCinematic is called,
	// the previous camera will be returned.
	void StartCinematic(const std::string & CinematicPath);

	void updateUI();

	void EndCinematic();

	// Level sequences.
	void PlayLevelSequence(const std::string & LevelSequencePath);
	void EndLevelSequence(const std::string & LevelSequencePath);
};