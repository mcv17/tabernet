#include "mcv_platform.h"
#include "module_container.h"
#include "module.h"


void CModuleContainer::addModule(IModule * module) {
#ifdef DEBUG
	const std::string & moduleName = module->getName();
	// Gets sure the module is not already added.
	// We only check in debug, we could also check on release
	// due to this operation not being too expensive but we won't.
	// This should not happen.
	auto it = _searchModule.find(moduleName);
	if (it != _searchModule.end())
		assert("Module: " + moduleName.c_str() + " has been added already");
#endif
	
	_modules.push_back(module);
	_searchModule[module->getName()] = (int)_modules.size() - 1;
}

IModule * CModuleContainer::findModule(const std::string & moduleName) {
	auto it = _searchModule.find(moduleName);
	return it != _searchModule.end() ? _modules[it->second] : NULL;
}

void CModuleContainer::clear() {
	for (auto module : _modules)
		delete module;
	_modules.clear();
	_searchModule.clear();
}