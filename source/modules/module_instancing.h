#pragma once

#include "modules/module.h"
#include "components/common/comp_compute.h"
#include "components/common/comp_buffers.h"

#include <filesystem>

class CMeshInstanced;
class CGPUBuffer;
class CMaterial;
class CComputeShader;

class CModuleInstancing : public IModule {
	typedef uint32_t CHandleAsInt;
	typedef std::string InstancedID;
	typedef int InstancedVectorIndex;
	typedef int PrefabVectorIndex;
	typedef int PrefabHash;

	bool loadedData = false; // First load of the module, load all the instancing decals information.

	/* Meshes instancing with GPU Culling. */

	// This struct is used by all meshes instanced in the scene.
	// Size MUST match the size of the gpu_instances
	// This structure is used in the GPU.
	struct TInstance {
		VEC3      aabb_center;    // AABB
		uint32_t  prefab_idx;     // In the array of prefabs
		VEC3      aabb_half;
		uint32_t  dummy2;
		MAT44     world;          // Matrix
		VEC4      objColor;       // Mostly used only by decals. Maybe we could have created a second GPU culling for it but for now I'm keeping it simple and not creating an specific new one.
	};

	// This struct is used by all culled instances, contain the instanced information.
	struct TCulledInstance {
		MAT44     world;          // Matrix
		VEC4      objColor;       // Mostly used only by decals. Maybe we could have created a second GPU culling for it but for now I'm keeping it simple and not creating an specific new one.
	};

	// Given a prefab, holds the information necessary for its culling and instancing.
	// This struct is used by the GPU, we use to to know which render types must be rendered.
	// Render types contain the mesh/material combination to render.
	struct TPrefab {
		static const uint32_t max_render_types_per_prefab = 6; // 4 for now seems more than enough. These means that at least, we will store 4 positons for each posible material in the culling instances.
		uint32_t  id;						// Index for the prefab, used to access the actual prefab position.
		uint32_t  lod_prefab = -1;			// Pointer to the lod. For now, only a lod level is supported.
		float     lod_threshold = 1e5;		// Used for measuring when to apply the lod.
		uint32_t  num_objs = 0;             // Number of instances of this type
		uint32_t  num_render_type_ids = 0;  // How many parts we must add
		uint32_t  total_num_objs = 0;       // Takes into account if another prefab is using me to render a LOD
		uint32_t  render_type_ids[max_render_types_per_prefab] = { 0,0,0,0,0,0 }; // How many combinations of mesh, material and group are supported.
	};

	// Each draw call requires this 5 uint's as arguments.
	// We can command the GPU to execute a DrawIndexedInstance
	// using the args store in some GPU buffer.
	struct DrawIndexedInstancedArgs {
		uint32_t indexCount;
		uint32_t instanceCount;     // at offset 4
		uint32_t firstIndex;
		uint32_t firstVertex;
		uint32_t firstInstance;
	};

public:
	// These Render Types are the one that get rendered. They are created for each combination of mesh,
	// render group and material.
	// This structure is NOT used in the GPU
	struct TRenderType {
		int renderTypeID = -1; // Used to bind with the draw data. We need it because we reorder the render types.
		const CMesh *     mesh = nullptr;
		uint32_t          group = 0;
		const CMaterial * material = nullptr;
		char              title[64];
		bool operator==(const TRenderType & other) const {
			return mesh == other.mesh && group == other.group && material == other.material;
		}
	};

private:
	// This data is uploaded to the GPU so the args are filled. Then, when calling DrawIndexed,
	// the correct meshes will be drawn. The CPU will fill the base variable with the starting pos
	// where meshes will start.
	struct TDrawData {
		DrawIndexedInstancedArgs args;      // 5 ints
		uint32_t                 base = 0;  // Where in the array of culled_instances we can start adding objs.This is calculated on the CPU on the preparePrefab function.
		uint32_t                 max_instances = 0;  // How many instances of this render type we will ever use
		uint32_t                 dummy = 0;

	};

	// Culling planes and position of the camera. Used for applying both culling and LOD to prefabs.
	// Uploaded to the GPU.
	struct TCullingPlanes {
		VEC4  planes[6];
		VEC3  CullingCameraPos;
		float dummy;
	};

	// Instancing constants, used for rendering instancing.
	// Uploaded to the GPU.
	struct TCtesInstancing {
		uint32_t total_num_objs = 0; // Used to know how many objs in the objs vector are actually instanced and how many are empty spaces.
		uint32_t instance_base = 0; // Updated on each render of a renderType, allows the instancing shader to know where to start rendering the next data.
		uint32_t instancing_padding[2];
	};

	// Entity instances information.
	int currentInstancedObjId = 0;
	std::vector< TInstance >                instanced_objects; // Stores all the elements that are rendered and culled in the scene.
	std::map<CHandleAsInt, InstancedVectorIndex> entity_to_instanceInformation; // For easy removal of entities.
	std::map<InstancedVectorIndex, CHandleAsInt> instanceInformation_to_entity; // For easy removal of entities.

	// Stores the prefab information that is going to be uploaded to the GPU.
	// This information is used by the CS to perform both culling and LOD.
	std::vector< TPrefab >        prefabs;
	std::map<PrefabHash, PrefabVectorIndex>	  prefabHash_to_id; // To find a prefab fast and identify whether it is registered or not.

	// Draw data is used by the CPU and GPU, indexed by rendertype, stores where the data starts in the culling volumes buffer and how many should be rendered.
	std::vector< TDrawData >      draw_datas;

	// Stores all the render types that exist (i.e combinations of mesh, material and group). Renders them on a renderCategory call.
	bool sortRenderTypes = false; // Set to true each time a new render type is added.
	std::vector< TRenderType >  render_types;

	bool                          is_dirty = false; // When an entity is added, it's necessary to reupload the data again.
	// In the future it would be nice to have two instancing objects one for static and one for dynamic.
	// This way, less data would need to be parse. As we don't support dynamic instancing right now, we leave it as it is.

	bool                          show_debug = true; // Render debug or not.

	CCamera                       culling_camera;	// Camera that is used for culling.
	TCullingPlanes                culling_planes;	// Culling planes computed from the camera and uploaded to the GPU.
	TCtesInstancing               ctes_instancing;  // Instancing constants used during the CS and renderCategory passes.

	// CS variables used in the Culling and LOD.
	TCompCompute                  comp_compute;		// Compute shader that executes the culling.
	TCompBuffers                  comp_buffers;		// Holds the buffers used by the CS.

	CGPUBuffer*                   gpu_objs = nullptr;   // Pointer to the buffer of instancing values.
	CGPUBuffer*                   gpu_draw_datas = nullptr; // Pointer to the buffer of draw data values (i.e which render types must be drawn).
	CGPUBuffer*                   gpu_prefabs = nullptr; // Pointer to the buffer of prefabs.
	CCteBufferBase*               gpu_ctes_instancing = nullptr; // constants used in both the Culling phase and the rendering phase.

	uint32_t                      max_objs = 0; // How many instancing objects can be allocated in the GPU.
	uint32_t                      max_render_types = 0; // How many render types can be allocated in the GPU.
	uint32_t                      max_prefabs = 0;	// How much space is allocated for prefab data in the GPU.

	// True when loading without blocking the game. 
	// (Will only be true if startState is main menu and it is enabled in the gpu_culling.json)
	bool is_loading = false; 
	// To be able to load without blocking the game or not. Read from json - gpu_culling.json
	bool able_to_non_blocking_load = false;
	// Vector that holds all the different instanced files that we need to load
	std::vector<std::filesystem::directory_entry> instanced_prefabs_files;
	// Iterator of the vector of all the different instanced files
	std::vector<std::filesystem::directory_entry>::iterator instanced_prefabs_files_it;
	// Json iteration idx between frames
	int json_iterator_current_idx = 0;
	// Exists instancing data to be loaded?
	bool existsInstancingData = false;

	/* GPU instancing functions. */
	void startLoadInstancingData();
	void loadInstancingData(); // Load instancing data for the module. This means memory to allocate for gpu buffers and, as we only have an scene for now,

	// also prefabs that are present in the scene and should be registered. If we would have a bigger game, we should register and unregister these prefabs when loading diferent leves.
	// As we don't, we keep it simple.
	uint32_t registerPrefab(CHandle prefabHand); // Register a prefab.
	uint32_t addRenderType(TRenderType & new_render_type); // Register a render type. A render type is a combination of a mesh, material and group (triangles to render of mesh).
	void setPrefabLod(uint32_t high_prefab_idx, uint32_t low_prefab_idx, float threshold); // Set Prefab Level of detail.
	uint32_t increasePrefabNumInstance(PrefabHash prefabHash); // Adds a new instance to the prefab.
	static bool sortRenderTypesKeys(const TRenderType & rt1, const TRenderType & rt2); // Sorts render types by mesh and material.

	void preparePrefabs(); // Goes through all prefabs, sets the draw_datas values for each render type to know where they start.
	void clearRenderDataInGPU(); // Sets all draw_datas number of prefabs to 0 so noe gets rendered. Called on each run pass to reset values.
	void updateCullingPlanes(const CCamera & camera); // Updates the culling planes given the camera passed as parameter.

	// ---------------------------------------------------------------------------------------------------------------------------------------
	
	/* Decal variables.*/

	/* Instanced decals. */

	// Instanced decals, includes color.
	struct TInstancedDynamicDecals {
		Matrix world;
		VEC4  color;
		float timeToLive;
		float fadeOutTime;
	};

	// Holds the pointer and the data used by instancing.
	struct InstancedDynamicDecals {
		InstancedID decal_id = "";

		CMeshInstanced * instances_mesh = nullptr; // Pointer to the instanced mesh that holds the instanced data.
		const CMaterial * decal_mat = nullptr; // Pointer to the material for the decal.
		Vector4 default_color = Vector4::One; // White by default.
		Vector3 default_volume_scale = Vector3::One; // default scale of the projection volume in the x (horizontal), y(vertical) and z(depth).
		bool fade_with_time = false; // If true, time_to_live and fade_out will be applied.
		float default_time_to_live = -1.0f; // How long does the particle live.
		float default_fade_out = -1.0f;  // How long it takes for the particle to go from 1 opacity to 0.
		// Fade out can be equal or smaller than time to live. If equal, the moment the particle it spawned, it will already start to fade until it dies.

		std::vector<TInstancedDynamicDecals> instancing_data; // Instanced data buffer to pass to the mesh.
		int next_decal_idx = 0; // Where the next decal to be placed will appear.
		int numberOfDecals = 0; // How many decals are in the vector.
	};

	// Map to find an instanced mesh index and vector of instanced meshes.
	std::map<InstancedID, InstancedVectorIndex> decals_instanced_name_to_internal_id; // Maps name of the decal to it's internal ID.
	std::vector<InstancedDynamicDecals> dynamic_decals_instanced_meshes; // Dynamic decals indexed by internal ID.
	bool dynamic_decals_debug = false;

	/* Decal private functions. */

	// Load instancing decals.
	void loadInstancingDecals();

	// Used for removing decals when they finish.
	void removeDynamicDecal(InstancedDynamicDecals & instancing_dynamic_decals, int decalToRemoveIndx);

public:
	CModuleInstancing(const std::string & name);

	// Module basic functions.
	bool start() override;
	void stop() override;
	void update(float dt) override;
	void renderDebug() override;
	void renderInMenu() override;
	
	/* Meshes public functions. */

	void addToRender(CHandle prefabHand); // Add an entity to the render.
	void removeFromRender(CHandle prefabHand); // Removes an entity from the render.
	void runGPUSceneCulling(CHandle h_camera); // Runs the GPU Culling of the given data.
	void renderCategory(eRenderCategory category); // Renders a category type.
	void renderDynamicDecals();

	/* Decal public functions. */

	// Decals functions for adding decals.
	void addDynamicDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const float roll = 0.0f);
	void addDynamicDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const float roll = 0.0f);
	void addDynamicDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const Vector3 & scaleProj, const float roll = 0.0f);
	void addDynamicDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const float roll = 0.0f);
	void addDynamicDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const float roll = 0.0f);
	void addDynamicDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const Vector3 & scaleProj, const float roll = 0.0f);

	void updateDynamicDecals(float dt);

	// Decal functions for removing a decal or a type of decal.
	void removeAllDynamicDecals();
	
	int getDynamicDecalID(const std::string & decal_name);

};