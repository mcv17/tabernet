#include "mcv_platform.h"
#include "module_instancing.h"

#include "render/compute/gpu_buffer.h"
#include "render/meshes/mesh_instanced.h"

#include "components/camera/comp_camera.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_render.h"
#include "components/common/comp_lod.h"
#include "components/common/comp_aabb.h"
#include "render/textures/material.h"
#include "render/compute/compute_shader.h"

#include "utils/utils.h"
#include "entity/entity.h"
#include "entity/entity_parser.h"
#include "utils/json_resource.h"

#include <set>

CModuleInstancing::CModuleInstancing(const std::string & name) : IModule(name){}

bool CModuleInstancing::start() {
	// Load instancing meshes and decals.
	if (!loadedData) {
		startLoadInstancingData();
		// We check for the startGS name, if it's the menu we will load without blocking
		if (able_to_non_blocking_load && EngineModules.getStartGS() == "gs_main_menu") {
			// Do non-blocking load
			// Set the flag in the modules manager
			EngineModules.setIsStartingModule(ModulesLoading::Instancing, true);
			loadInstancingDecals();
			is_loading = true;
		}
		else {
			loadInstancingData();
			loadInstancingDecals();
			loadedData = true;
		}
	}
	
	return true;
}

void CModuleInstancing::stop() {
	instanced_objects.clear();
	entity_to_instanceInformation.clear();
	instanceInformation_to_entity.clear();
}

void CModuleInstancing::update(float dt){
	// If in the future we would add support for dynamic instancing of meshes, we would add it here.
	
	// Update dynamic decals.
	updateDynamicDecals(dt);

	if (is_loading) {
		loadInstancingData();
	}
}

void CModuleInstancing::renderInMenu() {
	ImGui::PushID("Instancing-Tab-Sections");
	if (ImGui::BeginTabBar("Instancing Sections", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Static mesh and declas instancing")) {
			ImGui::Text("Number of instantiated objects: %ld", (uint32_t)currentInstancedObjId);
			ImGui::Checkbox("Show Debug", &show_debug);

			ImGui::Text("Current number of render types: %d / %d", render_types.size(), max_render_types);

			if (ImGui::TreeNode("Objs")) {
				for (auto& obj : instanced_objects)
					ImGui::Text("Prefab:%d at %f %f %f", obj.prefab_idx, obj.aabb_center.x, obj.aabb_center.y, obj.aabb_center.z);
				ImGui::TreePop();
			}

			if (ImGui::TreeNode("Prefabs")) {
				int idx = 0;
				for (auto& p : prefabs) {
					ImGui::Text("[%d] %3d num_objs with %d render types Lod:%d at %f Total:%d", idx, p.num_objs, p.num_render_type_ids, p.lod_prefab, p.lod_threshold, p.total_num_objs);
					if (ImGui::TreeNode("Render Type IDs")) {
						for (uint32_t i = 0; i < p.num_render_type_ids; ++i)
							ImGui::Text("[%d] %d", i, p.render_type_ids[i]);
						ImGui::TreePop();
					}
					++idx;
				}
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("Render Types")) {
				int idx = 0;
				for (auto& rt : render_types) {
					ImGui::Text("[%d] %d M:%s Mat:%s", idx, rt.group, rt.mesh->getName().c_str(), rt.material->getName().c_str());
					++idx;
				}
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("Draw Datas")) {
				for (auto& dd : draw_datas)
					ImGui::Text("Base:%3d indices:%4d from:%d", dd.base, dd.args.indexCount, dd.args.firstIndex);
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("GPU Draw Datas")) {
				gpu_draw_datas->copyGPUtoCPU();
				TDrawData* dd = (TDrawData*)gpu_draw_datas->cpu_data.data();
				for (uint32_t i = 0; i < draw_datas.size(); ++i, ++dd) {
					ImGui::Text("Base:%3d Draw Instances:%3d #Idxs:%4d From:%d max_instances:%d dummy:%d", dd->base, dd->args.instanceCount, dd->args.indexCount, dd->args.firstIndex, dd->max_instances, dd->dummy);
				}
				ImGui::TreePop();
			}
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Dynamic decals")) {
			ImGui::Text("Number of decals registered: %ld", (uint32_t)dynamic_decals_instanced_meshes.size());
			ImGui::Checkbox("Show dynamic decals debug", &dynamic_decals_debug);

			ImGui::Spacing(0, 5);

			for (auto & decalInfo : dynamic_decals_instanced_meshes) {
				if (ImGui::TreeNode(decalInfo.decal_id.c_str())) {
					ImGui::PushID(decalInfo.decal_id.c_str());
					ImGui::Text("Number of instantiated decals: %f", decalInfo.numberOfDecals);
					std::string material = "Decal material: " + decalInfo.decal_mat->getName();
					ImGui::Text(material.c_str());
					bool fade = decalInfo.fade_with_time;
					ImGui::Checkbox("Fade with time", &fade);
					if (decalInfo.fade_with_time) {
						ImGui::Text("Default fade out: %f", decalInfo.default_fade_out);
						ImGui::Text("Default time to live: %f", decalInfo.default_time_to_live);
					}
					ImGui::ColorEdit4("Color", &decalInfo.default_color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
					ImGui::DragFloat3("Decal volume scale (width, height, depth)", &decalInfo.default_volume_scale.x, 0.01f, 0.0f, 10000.0f);

					ImGui::Spacing(0, 10);

					if (ImGui::TreeNode("Show decals instantiated")) {
						if (decalInfo.numberOfDecals == 0)
							ImGui::Text("There are no decals instantiated.");
						for (int idx = 0; idx < decalInfo.numberOfDecals; ++idx) {
							auto & decal = decalInfo.instancing_data[idx];
							ImGui::PushID(&decal);
							VEC3 scale, trans;
							QUAT rot;
							decal.world.Decompose(scale, rot, trans);
							CTransform tmx;
							tmx.setRotation(rot);
							tmx.setPosition(trans);
							tmx.setScale(scale);
							bool changed = false;
							if (tmx.renderInMenu()) {
								changed = true;
								decal.world = tmx.asMatrix();
							}
							if (ImGui::ColorEdit4("Color", &decal.color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR))
								changed = true;

							if (ImGui::Button("Remove Decal"))
								removeDynamicDecal(decalInfo, idx);

							if (changed) {
								// Update the data of decals meshes if they have changed
								decalInfo.instances_mesh->setInstancesData(decalInfo.instancing_data.data(), decalInfo.numberOfDecals, sizeof(TInstancedDynamicDecals));
							}

							ImGui::Spacing(0, 5);

							ImGui::PopID();
						}
						ImGui::TreePop();
					}

					ImGui::Spacing(0, 10);
					if (ImGui::TreeNode("Add decal")) {
						static Vector3 position;
						static Vector3 lookAtPosition;
						static Vector4 color;
						static Vector3 scale;

						ImGui::DragFloat3("Position", &position.x, 0.01f, -100000.0f, 100000.0f);
						ImGui::DragFloat3("Look at position", &lookAtPosition.x, 0.01f, -100000.0f, 100000.0f);
						ImGui::ColorEdit4("Color", &color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
						ImGui::DragFloat3("Scale", &scale.x, 0.01, 0.0f, 1000.0f);
						if (ImGui::Button("Add instance of prefab type")) {
							addDynamicDecal(decalInfo.decal_id, position, lookAtPosition, color, scale);
						}
						ImGui::TreePop();
					}
					ImGui::PopID();
					ImGui::TreePop();
				}
			}

			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}

	ImGui::Spacing(0, 15);
	EngineImGui.showModuleInWindowButton(this);
	ImGui::Spacing(0, 15);

	ImGui::PopID();
}

void CModuleInstancing::renderDebug() {
	{
		PROFILE_FUNCTION("GPUCulling");
		if (show_debug) {
			for (int i = 0; i < currentInstancedObjId; ++i) {
				auto & obj = instanced_objects[i];
				drawWiredAABB(obj.aabb_center, obj.aabb_half, MAT44::Identity, VEC4(1, 0, 0, 1));
			}
		}
	}
	{
		PROFILE_FUNCTION("DynamicDecals");
		if (dynamic_decals_debug) {
			for (auto & decalInfo : dynamic_decals_instanced_meshes) {
				for (int idx = 0; idx < decalInfo.numberOfDecals; idx++) {
					auto & decal = decalInfo.instancing_data[idx];
					drawAxis(decal.world);
				}
			}
		}
	}
}

/* GPU instancing functions. */

void CModuleInstancing::startLoadInstancingData()
{
	PROFILE_FUNCTION("startLoadInstancingData");
	// Start by loading the gpu_culling.json.
	if (!Utils::fileExists("data/modules/gpu_culling.json")) return;
	json j = Utils::loadJson("data/modules/gpu_culling.json");

	// Check if it can load without blocking
	able_to_non_blocking_load = j.value<bool>("able_to_non_blocking_load", able_to_non_blocking_load); // False by default

	// Activate GPU instancing debugging.
	show_debug = j.value("show_debug", show_debug);

	// Create a compute component and a buffer component.
	TEntityParseContext ctx;
	comp_compute.load(j["compute"], ctx);
	comp_buffers.load(j["buffers"], ctx);

	// Hold each obj instance candidate to be culled and rendered.
	gpu_objs = comp_buffers.getBufferByName("instances");
	assert(gpu_objs || Utils::fatal("Missing required buffer to hold the instances to be culled\n"));
	max_objs = gpu_objs->num_elems;
	assert(gpu_objs->bytes_per_elem == sizeof(TInstance) || Utils::fatal("GPU/CPU struct size don't match for instances %d vs %d\n", gpu_objs->bytes_per_elem, (uint32_t)sizeof(TInstance)));

	// Holds all the culled instances that have passed the culling test.
	auto gpu_culled_instances = comp_buffers.getBufferByName("culled_instances");
	assert(gpu_culled_instances);
	assert(gpu_culled_instances->bytes_per_elem == sizeof(TCulledInstance) || Utils::fatal("GPU/CPU struct size don't match for culled_instances %d vs %d\n", gpu_culled_instances->bytes_per_elem, (uint32_t)sizeof(TCulledInstance)));

	// Stores the cameras culling planes.
	assert(comp_buffers.getCteByName("TCullingPlanes")->size() == sizeof(TCullingPlanes));

	// Used for telling the Compute Shader the number of instances to be handled.
	gpu_ctes_instancing = comp_buffers.getCteByName("TCtesInstancing");
	assert(gpu_ctes_instancing);
	assert(gpu_ctes_instancing->size() == sizeof(TCtesInstancing));

	// Prefabs that need to be rendered.
	gpu_prefabs = comp_buffers.getBufferByName("prefabs");
	max_prefabs = gpu_prefabs->num_elems;
	assert(gpu_prefabs->bytes_per_elem == sizeof(TPrefab) || Utils::fatal("GPU/CPU struct size don't match for prefabs %d vs %d\n", gpu_prefabs->bytes_per_elem, (uint32_t)sizeof(TPrefab)));

	// How many instances of each type have passed the culling test.
	gpu_draw_datas = comp_buffers.getBufferByName("draw_datas");
	max_render_types = gpu_draw_datas->num_elems;
	assert(gpu_draw_datas->bytes_per_elem == sizeof(TDrawData) || Utils::fatal("GPU/CPU struct size don't match for draw_datas %d vs %d\n", gpu_draw_datas->bytes_per_elem, (uint32_t)sizeof(TDrawData)));

	// Reserve in CPU all the memory that we might use, when we upload CPU data, we read from valid memory,
	// as we upload/read the full buffer size.
	instanced_objects.reserve(max_objs);
	prefabs.reserve(max_prefabs);
	render_types.reserve(max_render_types);
	draw_datas.reserve(max_render_types);

	// Populate the vector with the contents
	if (std::filesystem::exists("data/modules/gpu_culling/instanced_prefabs/")) {
		// Read all files in the directory of instanced prefabs. These files contain the path to all the prefabs that will be checked
		// for being instanced. These files are created by the exporter. In the future I might add subfolders to only load things
		// for the required scene. For now we register them all at the beginning and that's all.
		for (const auto& entry : std::filesystem::recursive_directory_iterator("data/modules/gpu_culling/instanced_prefabs/")) {
			instanced_prefabs_files.push_back(entry);
		}
		if (instanced_prefabs_files.size() > 0) {
			instanced_prefabs_files_it = instanced_prefabs_files.begin();
			existsInstancingData = true;
		}
	}
}

void CModuleInstancing::loadInstancingData() {
	// Check frame time remaining (only used if non-blocking load)
	float loadingTimeRemaining = EngineModules.getLoadingTimeRemaining();
	if (loadingTimeRemaining < 0.0f)
		return;

	// NonBlocking load time variables
	std::chrono::time_point<std::chrono::steady_clock> start;
	std::chrono::time_point<std::chrono::steady_clock> end;

	// Loop through all the instanced_prefabs files
	while (existsInstancingData && instanced_prefabs_files_it != instanced_prefabs_files.end()) {
		if ((*instanced_prefabs_files_it).status().type() != std::filesystem::file_type::directory) {
			std::string ext = (*instanced_prefabs_files_it).path().extension().generic_string();
			std::string filePath = (*instanced_prefabs_files_it).path().string();
			PROFILE_FUNCTION(filePath.c_str());

			// ignore if not a .instanced_scene_data file.
			if (_stricmp(ext.c_str(), ".instanced_scene_data") != 0) {
				std::advance(instanced_prefabs_files_it, 1);
				continue;
			}

			// Open the file, instanced_scene_data are still jsons.
			const json prefabs_to_parse = Utils::loadJson(filePath);

			assert(prefabs_to_parse.count("prefabs_to_register_for_instancing") && "Field was not found. Is the file correct?");

			auto & prefabsToRegister = prefabs_to_parse["prefabs_to_register_for_instancing"];
			while (json_iterator_current_idx < prefabsToRegister.size()){
				auto prefabPath = prefabsToRegister.at(json_iterator_current_idx);
				std::string path = prefabPath;
				PROFILE_FUNCTION(path.c_str());
				if (Utils::fileExists(path.c_str())) {
					// Check the start time
					start = std::chrono::steady_clock::now();

					TEntityParseContext ctx;
					ctx.is_registering_for_instancing = true;
					parseScene(path, ctx);

					// Register the entity to fetch all the necessary data from it.
					// Store them so we can delete them once all are registered. We don't want these entities, only the data.
					for (auto & p : ctx.entities_loaded)
						registerPrefab(p);

					// Remove them.
					for (auto & p : ctx.entities_loaded)
						p.destroy(); // Mark it to be removed.

					// we need to call this funcion so they get removed right now.
					CHandleManager::destroyAllPendingObjects();
				}

				// Increment the iterator
				json_iterator_current_idx++;


				if (is_loading) {
					// We get the end timestamp and compute the diff from the start
					end = std::chrono::steady_clock::now();
					std::chrono::duration<double> diff = end - start;
					// If we have used all of the frame time allowed we return false
					loadingTimeRemaining -= diff.count();
					if (loadingTimeRemaining < 0.0f)
						return;
				}
			}

			// When finished with the file, increment the file iterator
			std::advance(instanced_prefabs_files_it, 1);
			// And reset the json array idx
			json_iterator_current_idx = 0;
		}
	}
	loadedData = true;
	is_loading = false; 

	// Disable the flag in the modules manager, can be starting or loading, if we have changed the gamestate while instancing was still processing the prefabs
	if (EngineModules.getIsStartingModule(ModulesLoading::Instancing))
		EngineModules.setIsStartingModule(ModulesLoading::Instancing, false);
	else
		EngineModules.setIsLoadingModule(ModulesLoading::Instancing, false);

	return;
}

uint32_t CModuleInstancing::registerPrefab(CHandle prefabHand) {
	// Parse the entity, read its TCompRender and its LOD component and get the necessary information.
	CEntity * e = prefabHand;
	assert(e);

	// Get sure it has at least a render component, otherwise ignore.
	TCompRender * cr = e->getComponent<TCompRender>();
	if (!cr) return false;

	// Get sure it can be registered. If not, get out.
	if (!cr->canBeInstanced()) return -1;

	// Used to identity the prefab. Multiple 'prefabs' from our engine may bind to the same hash code
	// if they all share identical TCompRender combinations of mesh material and group.
	int prefabHash = cr->getRenderHashCode();

	auto & prefabID = prefabHash_to_id.find(prefabHash);
	// If found, don't register it again.
	if (prefabID != prefabHash_to_id.end()) return prefabID->second;

	// Store the prefab path (it's id from outside), with its id in the prefabs data uploaded to the GPU.
	int idx_prefab = prefabHash_to_id.size();
	prefabHash_to_id[prefabHash] = idx_prefab;

	// Register.
	TPrefab prefab;
	prefab.id = idx_prefab; // ID of the prefab.
	prefab.num_objs = 0;
	prefab.num_render_type_ids = 0;

	// Get all the parts of the render, and for each one, store the mesh, material and the group
	// it's using (Which part of the mesh is rendering).
	for (auto& p : cr->parts) {
		TRenderType tid;
		tid.mesh = p.mesh;
		tid.group = p.mesh_group;

		// Get instanced version of the material. We simply append the _instanced_gpu
		// to the end of the mat, this is how our exporter creates the instanced version.
		std::string material_instanced_name = p.material->getName();
		auto off = material_instanced_name.find(".material");
		material_instanced_name = material_instanced_name.substr(0, off) + "_instanced_gpu.material";

		tid.material = EngineResources.getResource(material_instanced_name)->as<CMaterial>();

		// Find which type of instance type is this draw call. If it already exist, we don't have to create
		// a new one.
		uint32_t render_type_id = addRenderType(tid);

		// Save it.
		assert(prefab.num_render_type_ids < TPrefab::max_render_types_per_prefab);
		prefab.render_type_ids[prefab.num_render_type_ids] = render_type_id;
		++prefab.num_render_type_ids;

		// Add shadow material if it has one.

		// Because we already have the instanced version of the material,
		// if the base material has a shadow mat, the instanced should have
		// the shadow version, at least if exported with our tools.
		tid.material = tid.material->getShadowsMaterial();
		if (!tid.material) continue;

		// Find which type of instance type is this draw call. If it already exist, we don't have to create
		// a new one.
		render_type_id = addRenderType(tid);

		// Save it.
		assert(prefab.num_render_type_ids < TPrefab::max_render_types_per_prefab);
		prefab.render_type_ids[prefab.num_render_type_ids] = render_type_id;
		++prefab.num_render_type_ids;

	}

	assert(prefabs.size() + 1 < max_prefabs || Utils::fatal("We need more space in the gpu buffer 'prefabs'. Current size is %d\n", max_prefabs));

	// Add the prefab.
	prefabs.push_back(prefab);
	uint32_t idx = (uint32_t)prefabs.size() - 1;

	// Register a lod if exists a component in the hi-quality prefab.
	TCompLod * c_lod = e->getComponent<TCompLod>();
	if (c_lod) {
		// Load the low-quality prefab.
		TEntityParseContext ctx;
		bool is_ok = parseScene(c_lod->replacement_prefab, ctx);
		CHandle h_lod = ctx.entities_loaded[0];
		uint32_t lod_idx = registerPrefab(h_lod);
		setPrefabLod(idx, lod_idx, c_lod->threshold);

		for(auto & p : ctx.entities_loaded)
			p.destroy();
	}

	return idx;
}

uint32_t CModuleInstancing::addRenderType(TRenderType & new_render_type) {
	for (auto& render_type : render_types) {
		if (render_type == new_render_type)
			return render_type.renderTypeID;
	}

	assert((render_types.size() + 1 < max_render_types) || Utils::fatal("Too many (%d) render_types registered. The GPU Buffer 'draw_datas' need to be larger.\n", max_render_types));

	// Add the ID of this render type.
	new_render_type.renderTypeID = render_types.size();

	// Register. Copy the key
	render_types.push_back(new_render_type);

	// Get name of the mesh.
	std::string mesh_name = new_render_type.mesh->getName();
	auto off = mesh_name.find_last_of("/");
	mesh_name = mesh_name.substr(off);

	// Get name of the material.
	std::string mat_name = new_render_type.material->getName();
	off = mat_name.find_last_of("/");
	mat_name = mat_name.substr(off);

	// Create the name for the prefab.
	snprintf(render_types.back().title, sizeof(TRenderType::title), "%s:%d %s", mesh_name.c_str(), new_render_type.group, mat_name.c_str());

	// Collect the range of triangles we need to render.
	TDrawData draw_data = {};
	const TMeshGroup& g = new_render_type.mesh->getGroups()[new_render_type.group];
	draw_data.args.indexCount = g.num_indices;
	draw_data.args.firstIndex = g.first_idx;
	draw_datas.push_back(draw_data);

	sortRenderTypes = true;

	return new_render_type.renderTypeID;
}

void CModuleInstancing::setPrefabLod(uint32_t high_prefab_idx, uint32_t low_prefab_idx, float threshold) {
	assert(high_prefab_idx < prefabs.size());
	TPrefab & hq = prefabs[high_prefab_idx];
	hq.lod_prefab = low_prefab_idx;
	hq.lod_threshold = threshold;
}

uint32_t CModuleInstancing::increasePrefabNumInstance(PrefabHash prefabHash) {
	PrefabVectorIndex prefab_index = prefabHash_to_id[prefabHash];
	// Add the prefab.
	prefabs[prefab_index].num_objs++;
	return prefab_index;
}

bool CModuleInstancing::sortRenderTypesKeys(const TRenderType & rt1, const TRenderType & rt2) {
	if (rt1.material != rt2.material) {
		if (rt1.material->category != rt2.material->category)
			return rt1.material->category < rt2.material->category;
		if (rt1.material->priority != rt2.material->priority)
			return rt1.material->priority < rt2.material->priority;
		// Compare using pointers... 
		return rt1.material < rt2.material;
	}
	if (rt1.mesh != rt2.mesh)
		return rt1.mesh < rt2.mesh;
	return rt1.group < rt2.group;
}

void CModuleInstancing::preparePrefabs() {
	// Update the prefabs data.

	// Count my official number of objects
	for (auto& p : prefabs)
		p.total_num_objs = p.num_objs;

	// Then, if a LOD is possible, it can also have as many total_num_objs as the base prefab.
	for (auto& p : prefabs) {
		if (p.lod_prefab != -1) {
			auto & p_low = prefabs[p.lod_prefab];
			assert((p_low.lod_prefab == -1) || Utils::fatal("We don't support yet, lod of lod"));
			p_low.total_num_objs += p.total_num_objs;
		}
	}

	// Clear counts
	for (auto& dd : draw_datas)
		dd.max_instances = 0;

	for (auto& p : prefabs) {
		// Each prefab will render potencially in several render types
		for (uint32_t idx = 0; idx < p.num_render_type_ids; ++idx) {
			uint32_t render_type_id = p.render_type_ids[idx];
			draw_datas[render_type_id].max_instances += p.total_num_objs;
		}
	}

	// Set the base now that we now how many instances of each render type we have.
	uint32_t base = 0;
	for (auto & dd : draw_datas) {
		dd.base = base;
		base += dd.max_instances;
	}

	uint32_t max_culled_instances = comp_buffers.getBufferByName("culled_instances")->num_elems;
	assert((base <= max_culled_instances) || Utils::fatal("We require more space in the buffer %d. Current %d", base, max_culled_instances));

	assert(gpu_prefabs);
	gpu_prefabs->copyCPUtoGPUFrom(prefabs.data());
}

// Every frame, we need to clear the #instances in the gpu. We could do that in a cs also.
void CModuleInstancing::clearRenderDataInGPU() {
	if (draw_datas.size() > 0) {
		for (auto& dd : draw_datas)
			dd.args.instanceCount = 0;

		// This line has to be done at least once
		gpu_draw_datas->copyCPUtoGPUFrom(draw_datas.data());
	}
}

void CModuleInstancing::updateCullingPlanes(const CCamera& camera) {
	MAT44 m = camera.getViewProjection().Transpose();
	VEC4 mx(m._11, m._12, m._13, m._14);
	VEC4 my(m._21, m._22, m._23, m._24);
	VEC4 mz(m._31, m._32, m._33, m._34);
	VEC4 mw(m._41, m._42, m._43, m._44);
	culling_planes.planes[0] = (mw + mx);
	culling_planes.planes[1] = (mw - mx);
	culling_planes.planes[2] = (mw + my);
	culling_planes.planes[3] = (mw - my);
	culling_planes.planes[4] = (mw + mz);      // + mz if frustum is 0..1
	culling_planes.planes[5] = (mw - mz);
	culling_planes.CullingCameraPos = camera.getPosition();
}


/* Meshes public functions. */

AABB getRotatedBy(AABB src, const MAT44 &model);

void CModuleInstancing::addToRender(CHandle prefabHand) {
	assert((instanced_objects.size() + 1 < max_objs) || Utils::fatal("Too many (%d) instances registered. The GPU Buffers 'gpu_instances' need to be larger.\n", max_objs));

	// Right now, we still don't support dynamic object, it shouldn't be much difficult to add them.
	// Maybe, an extra culling shader should be used for fast culling. Or directly cull with CPU. 

	// Start by checking the prefab is not already registered.
	if (entity_to_instanceInformation.count(prefabHand.asUnsigned())) return;
	
	// Get the entity render comp.
	CEntity * e = prefabHand;
	assert(e);

	// Get sure it has at least a render component, otherwise ignore.
	TCompRender * cr = e->getComponent<TCompRender>();
	// Get the transform.
	TCompTransform * c_transform = e->getComponent< TCompTransform >();

	if (!cr || !c_transform) return;

	if (!cr->canBeInstanced()) return;

	// Check the prefab exists.
	int prefabHash = cr->getRenderHashCode();
	if (!(prefabHash_to_id.count(prefabHash) > 0)) return; 	// If not found, leave.

	// This should'nt happen.
	assert(c_transform && cr && "The prefab was not found.");
	if (!cr || !c_transform) return;

	// Here we place our prefab into the objects.
	TInstance obj;
	obj.world = c_transform->asMatrix();
	AABB aabb_abs = getRotatedBy(cr->aabb, obj.world);
	obj.aabb_center = aabb_abs.Center;
	obj.prefab_idx = increasePrefabNumInstance(prefabHash);
	obj.aabb_half = aabb_abs.Extents;
	obj.objColor = cr->color;

	if (instanced_objects.size() <= currentInstancedObjId)
		instanced_objects.push_back(obj);
	else
		instanced_objects[currentInstancedObjId] = obj;

	currentInstancedObjId++;

	// Mark it dirty so data is reuploaded.
	is_dirty = true;

	// Add it to these maps so it can be removed easily.
	int idx_instanced_objects = currentInstancedObjId - 1;
	entity_to_instanceInformation[prefabHand.asUnsigned()] = idx_instanced_objects; // For easy removal of entities.
	instanceInformation_to_entity[idx_instanced_objects] = prefabHand.asUnsigned(); // For easy removal of entities.

	// Finally, send a message to the entity that it has been instantiated. It will remove its TCompRender and its AABB component.
	// As both the rendering and the aabb culling are managed by this module. We don't expect it to stop instantiating itself during it's life.
	// Otherwise, you could avoid removing both and try to keep them deactivated.
	if (e != nullptr) {
		TMsgSetEntityMeshInstantiated msg;
		e->sendMsg<TMsgSetEntityMeshInstantiated>(msg);
	}
}

void CModuleInstancing::removeFromRender(CHandle prefabHand) {
	uint32_t handAsInt = prefabHand.asUnsigned();
	if (entity_to_instanceInformation.count(handAsInt) < 1) return; // not Found.

	int idToRemove = entity_to_instanceInformation[handAsInt];
	int lastID = currentInstancedObjId - 1;

	// Decrease the number of the given type of prefab. Now, it won't render anymore.
	prefabs[instanced_objects[idToRemove].prefab_idx].num_objs--;

	// If only one element or last element, nothing special to do.
	if (lastID == -1 || lastID == (currentInstancedObjId-1)) {
		entity_to_instanceInformation.erase(handAsInt);
		instanceInformation_to_entity.erase(idToRemove);
	}
	else {
		// Copy the value from the last element to the id to remove.
		instanceInformation_to_entity[idToRemove] = instanceInformation_to_entity[lastID];

		// Update the position.
		CHandleAsInt lastPosHand = instanceInformation_to_entity[lastID];
		entity_to_instanceInformation[lastPosHand] = idToRemove;

		// Remove the other position.
		entity_to_instanceInformation.erase(handAsInt);
		instanceInformation_to_entity.erase(lastID);
	}

	// Reduce the current id to put an object in, it's the last element always.
	currentInstancedObjId--;

	// Mark it dirty so data is reuploaded.
	is_dirty = true;

	
	CEntity * e = prefabHand;
	if (e != nullptr) {
		TMsgSetEntityMeshInstantiated msg = { false };
		e->sendMsg<TMsgSetEntityMeshInstantiated>(msg);
	}
}

// This is called when we run the Compute Shaders. It performs GPU Culling on the uploaded data.
void CModuleInstancing::runGPUSceneCulling(CHandle h_camera) {
	CGpuScope gpu_scope("GPU Culling");

	// Can't update a buffer if it's still bound as vb. So unbound it.
	CVertexShader::deactivateResources();

	// Get the camera that is going to be used for culling.
	TCompCamera* c_camera = h_camera;
	culling_camera = *(CCamera*)c_camera;

	// Update the culling planes with our given camera.
	updateCullingPlanes(culling_camera);

	// Upload culling planes to GPU.
	comp_buffers.getCteByName("TCullingPlanes")->updateGPU(&culling_planes);

	// If data was modified, we have to reupload it.
	if (is_dirty) {
		gpu_objs->copyCPUtoGPUFrom(instanced_objects.data());

		// Move through all prefabs and update data.
		preparePrefabs();

		// Notify total number of objects we must try to cull
		ctes_instancing.total_num_objs = (uint32_t)currentInstancedObjId;
		gpu_ctes_instancing->updateGPU(&ctes_instancing);

		is_dirty = false;
	}

	// Before applying culling to see what must be rendered. Clean previous render data.
	clearRenderDataInGPU();

	// Run the culling in the GPU. Now, we have our culled instances in the GPU. And we know
	// How many instances are visible.
	comp_compute.sizes[0] = (uint32_t)currentInstancedObjId;
	comp_compute.run(&comp_buffers);

	CComputeShader::deactivate();
}


bool operator<(const CModuleInstancing::TRenderType & t1, const eRenderCategory & category) {
	return t1.material->category < category;
}
bool operator<(const eRenderCategory &category, const CModuleInstancing::TRenderType & t1) {
	return category < t1.material->category;
}

// This is called when rendering from the RenderManager
void CModuleInstancing::renderCategory(eRenderCategory category) {
	CGpuScope gpu_scope("GPU Culling");

	// Sorting if any render types have been added.
	if (sortRenderTypes) {
		std::sort(render_types.begin(), render_types.end(), &sortRenderTypesKeys);
		sortRenderTypes = false;
	}

	// Find the limits of the category
	auto range = std::equal_range(render_types.begin(), render_types.end(), category);
	if (range.first == range.second) return; // No materials of the given category.

	// Activate in the vs
	gpu_ctes_instancing->activate();

	// Now, get materials.

	// I want to use pointers (to avoid debug layout of VC on iterators)
	// but I can't dereference the end(), so use distance to get pointers
	auto offset_to_first = std::distance(render_types.begin(), range.first);
	auto offset_to_last = std::distance(render_types.begin(), range.second);

	const TRenderType * it = render_types.data() + offset_to_first;
	const TRenderType * last = render_types.data() + offset_to_last;

	//// Now that we have the range of objects we must render, render them:
	static TRenderType null_render_type;
	const TRenderType * prev_it = &null_render_type;

	//// Draw instacing.
	while (it != last) {
		auto & draw_data = draw_datas[it->renderTypeID];

		// Don't render something that doesn't has entities.
		if (draw_data.max_instances != 0) {
			CGpuScope gpu_render_type(it->title);

			// Because SV_InstanceID always start at zero, but the matrices
			// of each group have different starting offset. This allow use
			// where in the culling instances vector is the information for this
			// render type.
			ctes_instancing.instance_base = draw_data.base;
			gpu_ctes_instancing->updateGPU(&ctes_instancing);


			if (it->material != prev_it->material) {
				it->material->activate();
				it->material->activateCompBuffers(&comp_buffers);
			}
			if (it->mesh != prev_it->mesh)
				it->mesh->activate();

			uint32_t offset = it->renderTypeID * sizeof(TDrawData);
			Render.ctx->DrawIndexedInstancedIndirect(gpu_draw_datas->buffer, offset);

			// Prev_it holds the last activated and rendered object.
			prev_it = it;
		}
		++it;
	}

	// We paint all dynamic decals without culling they are way too simple
	// to bother with it. Maybe we could test in the future if adding it
	// to the normal GPU culling or an specific culling to avoid reuploading all static and dynamic
	// data each frame might be worth it.
	if (category == eRenderCategory::CATEGORY_DECALS)
		renderDynamicDecals();
}

void CModuleInstancing::renderDynamicDecals() {
	// Draw dynamic decals.
	for (auto & decal : dynamic_decals_instanced_meshes) {
		if (decal.numberOfDecals == 0) continue; // Don't paint if empty.
		CGpuScope gpu_render_type(decal.decal_id.c_str());
		decal.decal_mat->activate();
		decal.instances_mesh->activate();
		decal.instances_mesh->renderGroup(0, 0);
	}
}

/* Decal private functions. */

void CModuleInstancing::loadInstancingDecals() {
	PROFILE_FUNCTION("loadInstancingDecals");
	// Load static decals.
	if (std::filesystem::exists("data/prefabs/decals/static/")) {
		// Read all static decals. They are defined as normal entities. Once registered, the entities will be 
		for (const auto & entry : std::filesystem::recursive_directory_iterator("data/prefabs/decals/static/")) {
			if (entry.status().type() != std::filesystem::file_type::directory) {
				std::string filePath = entry.path().string();
				PROFILE_FUNCTION(filePath.c_str());

				// ignore if not a .json file.
				if (filePath.find_last_of(".json") == std::string::npos) continue;

				if (Utils::fileExists(filePath.c_str())) {
					TEntityParseContext ctx;
					ctx.is_registering_for_instancing = true;
					bool is_ok = parseScene(filePath, ctx);
					assert(is_ok);

					// Register the entity to fetch all the necessary data from it.
					// Store them so we can delete them once all are registered. We don't want these entities, only the data.
					for (auto & p : ctx.entities_loaded)
						registerPrefab(p);

					// Remove them.
					for (auto & p : ctx.entities_loaded)
						p.destroy(); // Mark it to be removed.

					// we need to call this funcion so they get removed right now.
					CHandleManager::destroyAllPendingObjects();
				}
			}
		}
	}

	// Load dynamic decals.
	if (std::filesystem::exists("data/prefabs/decals/dynamic/")) {
		for (auto & prefabPath : std::filesystem::directory_iterator("data/prefabs/decals/dynamic/")) {
			if (prefabPath.status().type() != std::filesystem::file_type::directory) {
				std::string filePath = prefabPath.path().string();

				// Load Json.
				json instance = Utils::loadJson(filePath);

				// Fetch variables for instancing, get the ID to identify the decal.
				std::string instanced_mesh_path = filePath;
				std::string decal_id = instance.value("decal_id", "");
				int numInstances = instance.value("num_instances_reserved", 0);
				std::string decal_path = instance.value("decal_material", "");
				Vector4 default_color = loadVector4(instance, "decal_color");
				float volume_x_scale = instance.value("volume_x_scale", 1.0); // X scale of the projection volume.
				float volume_y_scale = instance.value("volume_y_scale", 1.0); // Y scale of the projection volume.
				float volume_z_scale = instance.value("volume_z_scale", 1.0); // Z scale of the projection volume.
				bool update_with_time = instance.count("time_to_live") || instance.count("default_fade_out"); // If any of these is present, we update with time.
				float time_to_live = instance.value("time_to_live", 1.0);
				float default_fade_out = instance.value("default_fade_out", 1.0);

				// Get sure time_to_live and default fade out are not negative.
				if (update_with_time) {
					if (default_fade_out < 0.0f)
						default_fade_out = 0.0f; 
					if (time_to_live <= 0.0f)
						time_to_live = 0.001f; // Not exactly 0.0f so our shader work correctly if it would be set to 0.0f.
				}

				assert(time_to_live >= default_fade_out && "Get sure the particle time to life is equal or bigger than the time it will take for it to fade completely.");
				if (default_fade_out > time_to_live)
					time_to_live = default_fade_out;

				// Get sure the id is not repeated.
				assert(decals_instanced_name_to_internal_id.count(decal_id) < 1 && "Decal ID is already registered, use another ID for the decal so it can be found easily with code.");

				// Fill the data for instancing.
				InstancedDynamicDecals instancingData;
				instancingData.decal_id = decal_id;
				instancingData.instances_mesh = (CMeshInstanced *)EngineResources.getResource(filePath)->as<CMesh>();
				instancingData.decal_mat = EngineResources.getResource(decal_path)->as<CMaterial>();
				instancingData.instancing_data.resize(instancingData.instances_mesh->getNumInstancesAllocated());
				instancingData.default_color = default_color;
				instancingData.default_volume_scale.x = volume_x_scale;
				instancingData.default_volume_scale.y = volume_y_scale;
				instancingData.default_volume_scale.z = volume_z_scale;
				instancingData.default_time_to_live = time_to_live;
				instancingData.default_fade_out = default_fade_out;
				instancingData.fade_with_time = update_with_time;

				// Add the mesh.
				dynamic_decals_instanced_meshes.push_back(instancingData);
				decals_instanced_name_to_internal_id[decal_id] = dynamic_decals_instanced_meshes.size() - 1;
			}
		}
	}
}

/* Decal public functions. */

void CModuleInstancing::addDynamicDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const float roll) {
	// Get decal.
	auto & decal = decals_instanced_name_to_internal_id.find(decal_name);
	if (decal == decals_instanced_name_to_internal_id.end()) return; // Return if not found.

	// Add the decal.
	addDynamicDecal((*decal).second, position, lookAtPos, dynamic_decals_instanced_meshes[(*decal).second].default_color, roll);
}

void CModuleInstancing::addDynamicDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const float roll){
	// Get decal.
	auto & decal = decals_instanced_name_to_internal_id.find(decal_name);
	if (decal == decals_instanced_name_to_internal_id.end()) return; // Return if not found.

	// Add the decal.
	addDynamicDecal((*decal).second, position, lookAtPos, color, roll);
}

void CModuleInstancing::addDynamicDecal(const std::string & decal_name, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const Vector3 & scaleProj, const float roll) {
	// Get decal.
	auto & decal = decals_instanced_name_to_internal_id.find(decal_name);
	if (decal == decals_instanced_name_to_internal_id.end()) return; // Return if not found.

	// Add the decal.
	addDynamicDecal((*decal).second, position, lookAtPos, color, scaleProj, roll);
}

void CModuleInstancing::addDynamicDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const float roll) {
	assert(decal_ID >= 0 && decal_ID < dynamic_decals_instanced_meshes.size() && "Decal ID was not correct.");
	addDynamicDecal(decal_ID, position, lookAtPos, dynamic_decals_instanced_meshes[decal_ID].default_color, dynamic_decals_instanced_meshes[decal_ID].default_volume_scale, roll);
}

void CModuleInstancing::addDynamicDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const float roll) {
	assert(decal_ID >= 0 && decal_ID < dynamic_decals_instanced_meshes.size() && "Decal ID was not correct.");
	addDynamicDecal(decal_ID, position, lookAtPos, color, dynamic_decals_instanced_meshes[decal_ID].default_volume_scale, roll);
}

void CModuleInstancing::addDynamicDecal(int decal_ID, const Vector3 & position, const Vector3 & lookAtPos, const Vector4 & color, const Vector3 & scaleProj, const float roll) {
	assert(decal_ID >= 0 && decal_ID < dynamic_decals_instanced_meshes.size() && "Decal ID was not correct.");

	auto & decalInfo = dynamic_decals_instanced_meshes[decal_ID];
	int max_decals = decalInfo.instancing_data.size(); // How many decals we can support.

	VEC3 front = lookAtPos - position;
	float yaw, pitch;
	vectorToYawPitch(front, yaw, pitch);

	// Add the data.
	Matrix worldMatrix = Matrix::CreateScale(scaleProj) * Matrix::CreateFromYawPitchRoll(yaw, pitch, roll) * Matrix::CreateTranslation(position);
	TInstancedDynamicDecals data = { worldMatrix, color, decalInfo.default_time_to_live, decalInfo.default_fade_out };
	
	decalInfo.instancing_data[decalInfo.next_decal_idx] = data;
	decalInfo.next_decal_idx = (decalInfo.next_decal_idx + 1) % max_decals;

	// Only send as many decals as the vector allows.
	if (decalInfo.numberOfDecals < max_decals)
		decalInfo.numberOfDecals++;

	// Update the data of decals meshes here if we are not updating it each frame.
	if(decalInfo.fade_with_time == false)
		decalInfo.instances_mesh->setInstancesData(decalInfo.instancing_data.data(), decalInfo.numberOfDecals, sizeof(TInstancedDynamicDecals));
}

void CModuleInstancing::removeDynamicDecal(InstancedDynamicDecals & instancing_dynamic_decals, int decalToRemoveIndx) {
	auto & instancing_data = instancing_dynamic_decals.instancing_data;
	assert(decalToRemoveIndx < instancing_data.size() && "Decal to remove has a bigger indx than the instancing vector it is at.");
	if (instancing_data.size() == 0) return; // Protection just in case.

	int lastDecalInTheArrayIndx = instancing_dynamic_decals.numberOfDecals - 1;

	// If more than one element. Add the last one in the position to remove.
	if (lastDecalInTheArrayIndx > 0)
		instancing_data[decalToRemoveIndx] = instancing_data[lastDecalInTheArrayIndx];

	instancing_dynamic_decals.next_decal_idx = lastDecalInTheArrayIndx;
	instancing_dynamic_decals.numberOfDecals--;

	// Update the data of decals meshes here if we are not updating it each frame.
	if (instancing_dynamic_decals.fade_with_time == false)
		instancing_dynamic_decals.instances_mesh->setInstancesData(instancing_dynamic_decals.instancing_data.data(), instancing_dynamic_decals.numberOfDecals, sizeof(TInstancedDynamicDecals));
}

void CModuleInstancing::updateDynamicDecals(float dt) {
	for (auto & decalInfo : dynamic_decals_instanced_meshes) {
		if (decalInfo.fade_with_time == false) continue;
		for (int idx = 0; idx < decalInfo.numberOfDecals; idx++) {
			auto & decal = decalInfo.instancing_data[idx];
			decal.timeToLive -= dt;
			if (decal.timeToLive < 0.0f){
				removeDynamicDecal(decalInfo, idx);
				idx--; // We want to remain at the same indx as now, there is a different decal in the same pos after removing the previous one.
				continue;
			}
		}

		// Update the data of decals meshes if they have changed
		decalInfo.instances_mesh->setInstancesData(decalInfo.instancing_data.data(), decalInfo.numberOfDecals, sizeof(TInstancedDynamicDecals));
	}
}

void CModuleInstancing::removeAllDynamicDecals(){
	for (auto & decalInfo : dynamic_decals_instanced_meshes) {
		decalInfo.numberOfDecals = 0;
		memset(decalInfo.instancing_data.data(), 0, decalInfo.instancing_data.size() * sizeof(TInstancedDynamicDecals));
	}
}

int CModuleInstancing::getDynamicDecalID(const std::string & decal_name) {
	// Get decal.
	auto & decal = decals_instanced_name_to_internal_id.find(decal_name);
	if (decal == decals_instanced_name_to_internal_id.end()) return -1; // Return if not found.

	return (*decal).second;
}