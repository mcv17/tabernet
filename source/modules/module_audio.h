#pragma once

#include "modules/module.h"
#include "FMOD/studio/fmod_studio.hpp"
#include "FMOD/core/fmod.hpp"

using namespace FMOD;

class CModuleAudio : public IModule
{

	struct Bank {
		std::string name;
		Studio::Bank* bank;
		std::vector<std::string> events;
	};

	/* FMOD software layer */
	//System* sys;
	Studio::System* studio;
	std::map<std::string, Bank> _banks;
	std::map<std::string, Studio::EventInstance*> _events;

	// Banks
	/*** These 2 are mandatory **********/
	Studio::Bank* _masterBank = NULL;
	Studio::Bank* _stringsBank = NULL;
	/***********************************/

	


public:
	
	CModuleAudio(const std::string& name) : IModule(name) {}
	/* Here we will initialize FMOD */
	bool start() override;
	void stop() override;
	void update(float dt) override;
	void renderDebug() override {}
	void renderInMenu() override;

	/* Load audio from gamestate */
	void loadGSAudio(std::string gs);

	// Functions
	void playEvent(std::string path, VEC3 position = VEC3(0,0,0));
	void stopEvent(std::string path);
	void stopAllSounds();
	void setEventParameter(std::string path, std::string paramName, float paramValue);
	FMOD_VECTOR getFMODVector3(VEC3 v);
};

