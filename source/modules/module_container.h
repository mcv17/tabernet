#pragma once

#include <vector>
#include <map>
#include <string>

class IModule;

/* CModuleContainer class works as a container holding pointers to modules and allowing for fast addition and search. 
Sadly due to the abstraction of modules we can't use contiguous memory for their storage :C. */
class CModuleContainer {
private:
	std::map<std::string, int> _searchModule; // Allows for fast search of modules, store its name as a key and its vector position as value.

public:
	std::vector<IModule *> _modules; // Holds pointer to modules. This class is responsible for the freeing of their memory.

	void addModule(IModule * module); // Adds module to the container.
	IModule * findModule(const std::string & moduleName); // Returns module pointer if present.
	void clear(); // Frees all modules from memory and removes their pointers from the container.
};