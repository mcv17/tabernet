#pragma once
#include "module.h"
#include "module_container.h"
#include "gamestate_container.h"

#include <unordered_set>

class CModuleManager
{
	CModuleContainer _all_modules; // Holds all modules' memory. Responsible of their deallocation.

	VModules _system_modules; // Modules used by the system.
	VModules _update_modules; // Modules that should be run during the update fase.
	VModules _loading_modules; // Modules that should be run while loading.
	VModules _render_debug_modules; // Modules that should be run during the rendering fase.
	
	CGamestateContainer _gamestates; // All posible gamestates. A gamestate is simply a vector with pointers to modules to activate.
	CGamestate * _currentGamestate = nullptr;
	CGamestate * _requestedGamestate = nullptr;
	std::string _startGamestate;

	//bool isLoading = false; // Used when we want to load scenes without blocking the update loop.
	int _modulesLoadingFlags = 0; // Used when we want to load scenes without blocking the update loop.
	int _modulesStartingFlags = 0; // Used for the modules that can be loaded on the menu without blocking the game

	// When we want to load without blocking
	float _frameDuration = 0.016f; // Default 60 frames
	float _loadingTimeRemaining = 0.0f; // Used so the modules know how many ms they have this frame to load

	void startModules(VModules & modules);
	void stopModules(VModules & modules);
	
	// After calling ChangeToGameState, UpdateGamestate is called in update. It changes our gamestate to the asked one.
	// Prevents changing gamestate in the middle of an update frame.
	void updateGamestate(float & dt);

	// Used for rendering modules in menu.
	void renderModulesInMenu(const char* title, VModules& some_modules);
	void renderActiveModulesInMenu(const char* title, VModules& some_modules);

public:

	// We start the module manager by loading modules from our config file and starting them.
	void init();
	// Stops all modules that are being executed and any present system modules.
	void stop();
	// Removes all modules from memory.
	void clear();
	
	/* Register a module or unregister it removing it from memory and execution. */
	void registerGameModule(IModule * module);
	void registerSystemModule(IModule * module);

	/* Load config. */
	// Loads modules from config file.
	void loadModulesFromConfig(const std::string & pathToModulesConfigFile);
	// Loads modules from config file.
	void loadGamestatesFromConfig(const std::string & pathToGamestatesConfigFile);

	/* Change to new game states. */
	// We change to a new game state. Game states can add or remove new nodes.
	void changeToGamestate(const std::string& name);

	// We change to a new game state but we want some kind of animation while it loads on the background.
	// The following modules will load without blocking:
	//   Boot, Instancing
	void changeToGamestateNonBlocking(const std::string& name);

	// Used to set the state of a module, so we know if it is loading without blocking or not. 
	void setIsLoadingModule(ModulesLoading module, bool isLoading);
	// Used to get the state of a module, so we know if it is loading without blocking or not.
	bool getIsLoadingModule(ModulesLoading module);

	// Used to set the state of a module, so we know if it is starting in the background of the menu or not. 
	void setIsStartingModule(ModulesLoading module, bool isStarting);
	// Used to get the state of a module, so we know if it is starting in the background of the menu or not. 
	bool getIsStartingModule(ModulesLoading module);

	// Called when it finishes changing the gamestate, or from boot module if finishes a nonBlocking load.
	void finishedUpdatingGamestate(float & dt);
	
	// We update all modules in the _update_modules vector.
	void update(float dt);
	// We render all modules in render debug.
	void renderDebug();

	// Renders in a menu debug info. Used by ImGUI.
	void renderInMenu();
	void renderActiveModules();
	void renderFollowedModules(const std::unordered_set <std::string> & pinnedModules);
	void renderGamestate();

	/* Activate or deactivate render debug modules. */
	void activateRenderDebug(const std::string & moduleName);
	void removeRenderDebug(const std::string & moduleName);
	bool isModuleInRenderDebug(const std::string & moduleNameToRemove);


	/* Getters. */

	/* Returns us a module from its name. */
	IModule * getModule(const std::string & moduleName);
	/* Returns us a gamestate from its name. */
	CGamestate * getGamestate(const std::string & gameStateName);
	/* Returns the string of the current gamestate. */
	const std::string getCurrentGS();
	/* Returns the string of the requested gamestate. */
	const std::string getRequestedGS();
	/* Returns the string of the start gamestate. */
	const std::string getStartGS();

	float getLoadingTimeRemaining() const { return _loadingTimeRemaining; }
};
