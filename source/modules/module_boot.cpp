#include "mcv_platform.h"
#include "module_boot.h"
#include "engine.h"
#include "modules/module_camera_mixer.h"
#include "entity/entity_parser.h"

#include "utils/json_resource.h"
#include "resources/prefab_resource.h"
#include "components/ai/behaviour_tree/bt_resource.h"
#include "render/textures/material.h"

#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"

CModuleBoot::CModuleBoot(const std::string& name)
	: IModule(name)
{}

bool CModuleBoot::start()
{
	// Cache the json file as a resource on memory
	_jsonFile = EngineResources.getResource("data/boot.json")->as<CJson>()->getJson();

	// And parse the cameras main file. These will be alive during all the runtime
	TEntityParseContext ctx;
	parseScene("data/prefabs/cameras.json", ctx);

	json& preloaded_src = _jsonFile["preload_resources"];
	for (auto& category : preloaded_src) {
		std::string categoryType = category["type"];
		for (auto& src : category["resources"]) {
			if(categoryType.compare("CJson") == 0)
				EngineResources.getResource(src)->as<CJson>();
			if (categoryType.compare("CPrefab") == 0)
				EngineResources.getResource(src)->as<CPrefab>();
			else if (categoryType.compare("CBT") == 0)
				EngineResources.getResource(src)->as<CBT>();
			else if (categoryType.compare("CMesh") == 0)
				EngineResources.getResource(src)->as<CMesh>();
			else if (categoryType.compare("CMaterial") == 0)
				EngineResources.getResource(src)->as<CMaterial>();
		}
	}

	return true;
}

void CModuleBoot::update(float dt) {
	if (_isLoading)
		loadGamestateNonBlocking();
}

void CModuleBoot::renderInMenu() {
	// Pair: Gamestate string -- VHandles
	for (auto& pair : _entitiesPerGamestate) {
		if (ImGui::TreeNode(pair.first.c_str())) {
			ImGui::PushID(pair.first.c_str());
			ImGui::Text("Total entities in this gamestate: %i", pair.second.size());
			for (CHandle& entityH : pair.second) {
				CEntity* entity = entityH;
				entity->debugInMenu();
			}
			ImGui::PopID();
			ImGui::TreePop();
		}
	}
}

void CModuleBoot::loadGamestateBlocking(std::string gamestate) {
	_lastGamestate = _currentGamestate;
	_currentGamestate = gamestate;

	// Get the resource again for hotreload
	_jsonFile = EngineResources.getResource("data/boot.json")->as<CJson>()->getJson();

	if (_jsonFile[gamestate].empty())
		return;

	auto prefabs = _jsonFile[gamestate].get< std::vector< std::string > >();
	for (auto& p : prefabs) {
		//TFileContext fc(p); TODO Check if useless
		TEntityParseContext ctx;
		PROFILE_FUNCTION_COPY_TEXT(p.c_str());
		Utils::dbg("Parsing boot prefab %s\n", p.c_str());
		parseScene(p, ctx);
		// Save the handles of the entities
		_entitiesPerGamestate[gamestate].insert(std::end(_entitiesPerGamestate[gamestate]), std::begin(ctx.entities_loaded), std::end(ctx.entities_loaded));
	}

	EngineScripting.onEntitiesLoaded();
	sendMessagesToEntitiesLoaded(gamestate);
	clearLastGamestateEntities(_lastGamestate);
}

void CModuleBoot::startLoadGamestateNonBlocking(std::string gamestate) {
	assert(!_isLoading, "Cannot start loading a new gamestate if another is being loaded");

	_lastGamestate = _currentGamestate;
	_currentGamestate = gamestate;

	// Get the prefabs list
	_prefabs = _jsonFile[gamestate].get< std::vector< std::string > >();
	if (_prefabs.size() > 0) {

		// Get the total depth0 entities count
		for (auto prefab : _prefabs) {
			const json& j_scene = EngineResources.getResource(prefab)->as<CJson>()->getJson();
			if (j_scene.is_array()) {
				_depth0EntityCount += j_scene.size();
			}
		}
		_prefabIt = _prefabs.begin();
		_isLoading = true;
	}

	// Start a new Entity parse context for the first prefab
	_ctx = new TEntityParseContext;
}

void CModuleBoot::loadGamestateNonBlocking() {
	// Update the progress bar from the loading UI widget
	UI::CProgress* progressBar = dynamic_cast<UI::CProgress*>(Engine.getUI().getWidgetByAlias("loading_progress_bar"));
	if (progressBar)
		progressBar->setRatio(_entitiesLoadedCount / _depth0EntityCount);

	float loadingTimeRemaining = EngineModules.getLoadingTimeRemaining();
	if (loadingTimeRemaining < 0.0f)
		return;

	// NonBlocking load time variables
	std::chrono::time_point<std::chrono::steady_clock> start;
	std::chrono::time_point<std::chrono::steady_clock> end;

	// Load until we run out of time for this frame, or if we have time, finish parsing the prefabs.
	while (_prefabIt != _prefabs.end()){
		PROFILE_FUNCTION_COPY_TEXT((*_prefabIt).c_str());
		Utils::dbg("Parsing boot prefab %s\n", (*_prefabIt).c_str());

		start = std::chrono::steady_clock::now();
		bool finishedPrefab = parseScene(*_prefabIt, *_ctx, 0, loadingTimeRemaining);
		_entitiesLoadedCount += (*_ctx).entities_loaded.size();
		
		if (finishedPrefab)	{ // We finished parsing the current prefab
			// Save the handles of the entities
			_entitiesPerGamestate[_currentGamestate].insert(std::end(_entitiesPerGamestate[_currentGamestate]), std::begin(_ctx->entities_loaded), std::end(_ctx->entities_loaded));

			// Increment the iterator
			std::advance(_prefabIt, 1);

			// Start a new Entity parse context for each prefab
			_ctx = new TEntityParseContext;

			// We get the end timestamp and compute the diff from the start
			end = std::chrono::steady_clock::now();
			std::chrono::duration<double> diff = end - start;
			// If we have used all of the frame time allowed we return false
			loadingTimeRemaining -= diff.count();
			if (loadingTimeRemaining < 0.0f)
				return;
		}
		// If false, it means he have used all the frame duration parsing stuff so we leave for now
		else
			return;
	}
	
	// When we finished loading all the prefabs
	_isLoading = false;
	EngineScripting.onEntitiesLoaded();
	sendMessagesToEntitiesLoaded(_currentGamestate);
	EngineModules.setIsLoadingModule(ModulesLoading::Boot, false);
	clearLastGamestateEntities(_lastGamestate);
}

void CModuleBoot::sendMessagesToEntitiesLoaded(std::string gamestate) {
	for (CHandle& entityH : _entitiesPerGamestate[gamestate]) {
		CEntity* e = entityH;
		if (e) {
			//TODO send message
		}
	}
}

void CModuleBoot::clearLastGamestateEntities(std::string gamestate) {
	// Last gamestate was nothing because it's the boot of the game
	if (gamestate.empty())
		return;

	// Check if exists in the vector
	if (_entitiesPerGamestate.find(gamestate) == _entitiesPerGamestate.end())
		return;

	// If exists, mark all the entities from the previous gamestate to be deleted
	auto it = _entitiesPerGamestate[gamestate].begin();
	while (it != _entitiesPerGamestate[gamestate].end()) {
		(*it).destroy();
		it = _entitiesPerGamestate[gamestate].erase(it);
	}
}

