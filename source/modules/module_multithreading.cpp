#include "mcv_platform.h"
#include "module_multithreading.h"
#include "tbb/tbb.h"

bool is_multithreaded_enabled = true;

bool MultithreadingModule::start() {
  int nDefThreads = tbb::task_scheduler_init::default_num_threads();
  tbb::task_scheduler_init init(nDefThreads);
  return true;
}

void MultithreadingModule::stop() {
  //todo: stop the scheduler
}

void MultithreadingModule::update(float dt) {
}

void MultithreadingModule::renderDebug()
{
}

void MultithreadingModule::renderInMenu()
{
	ImGui::Checkbox("Is multithreading active: ", &is_multithreaded_enabled);

}