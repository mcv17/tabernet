#pragma once
#include "mcv_platform.h"
#include <utility>

// Used when asyncLoading so it doesn't block the system
enum ModulesLoading {
	Instancing = 1 << 0,
	Boot = 1 << 1
};

/* Abstract class representing an interface all modules should
implement for their correct implementation in the engine. */

class IModule
{
protected:
	std::string _name; // Name of the module, works as an identifier. We could think of adding an universal identifier instead.
	
	bool _active = false; // Says whether the node is active at the moment.

  IModule(const IModule& other) = delete;

public:
	// Used by ImGui to see if the module must be shown as a window.
	// This could be put in a ImGUI class specific to each module that uses ImGUI
	// to abstract from the rest. I will leave it here for now.
	bool showAsWindow = false;

	IModule( const std::string & name )
	{
		_name = name;
	}
	
	virtual bool start() { return true; }
	
	virtual void stop() {};
	
	virtual void update(float dt) = 0;
	
	virtual void renderDebug() = 0;
	
	virtual void renderInMenu() {}
	
	/* Getters and setters. */

	const std::string & getName() const { return _name; }

	bool isActive() const{ return _active; }
	
	void setActive( bool active) { _active = active; }
};

using VModules = std::vector<IModule*>;
