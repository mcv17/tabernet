#include "mcv_platform.h"
#include "module_editor.h"
#include "entity/entity.h"
#include "entity/entity_parser.h"
#include "particles/module_particles.h"
#include "ui/module_ui.h"

#include "components/common/comp_name.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_render.h"

#include "engine.h"

#include <filesystem>
#include <fstream>
#include "utils/phys_utils.h"
#include "components/camera/comp_camera.h"
#include "components/camera/comp_camera_manager.h"

CModuleEditor::CModuleEditor(const std::string & name) : IModule(name) {}

bool CModuleEditor::start() {
	// Start by fetching all folders inside the prefabs folder. Returns a vector with all the directories
	// inside data/prefabs. Also returns data/prefabs as a directory (the base one).
	std::vector<std::string> directories;
	fetchAllPrefabDirectories(directories);

	// Parse all the categories from the directories inside the data/prefabs.
	fetchAllPrefabCategories(directories);

	// Fetch all prefabs paths inside a category.
	fetchAllPrefabsInsideCategories();

	// Copy all categories into a vector for use by ImGui.
	categories_list_vector.reserve(prefabs_by_category.size());
	for (auto const & category : prefabs_by_category)
		categories_list_vector.push_back(category.first.c_str());

	// Parse all the particles to edit
	EngineParticles.reloadParticlesJson();

	// Debug UI
	EngineUI.activateWidget("debug");

	// Prepare curve editor variables.
	editorCurve.setNameAndType("EditorCurve", getResourceTypeFor<CTransformCurve>());

	return true;
}

/* Taken from: https://eliasdaler.github.io/using-imgui-with-sfml-pt2/#combobox-listbox */
static auto vector_getter = [](void * vec, int idx, const char ** out_text)
{
	auto & vector = *static_cast<std::vector<std::string>*>(vec);
	if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
	*out_text = vector.at(idx).c_str();
	return true;
};

/* Adapted */
static auto prefab_by_categories_getter = [](void * vec, int idx, const char ** out_text)
{
	auto & vector = *static_cast<std::vector<std::pair<CModuleEditor::PrefabName, CModuleEditor::PrefabPath>>*>(vec);
	if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
	*out_text = vector.at(idx).first.c_str();
	return true;
};

void CModuleEditor::renderInMenu() {
	ImGui::PushID("Editor-Tab-Sections");
	if (ImGui::BeginTabBar("Editor Sections", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Scene Creation")) {
			if (ImGui::BeginTabBar("Editor-SceneCreation-Sections", ImGuiTabBarFlags_None))
			{
				if (ImGui::BeginTabItem("Entity Placement"))
				{
					renderSceneEditor();
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("Export Entities")) {
					renderAssetsToExportWindow();
					ImGui::EndTabItem();
				}
				ImGui::EndTabBar();
			}
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Particles")) {
			EngineParticles.renderInMenu();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Curves")) {
			renderCurveEditor();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("UI")) {
			EngineUI.renderInMenu();
			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}
	ImGui::PopID();

	EngineImGui.showModuleInWindowButton(this);
}


void CModuleEditor::renderSceneEditor() {
	// Show categories list box.
	static int current_category = 0;
	static int previouslySelectedCategory = -1;
	ImGui::Spacing(0, 5);
	ImGui::Text("Prefab categories");
	ImGui::ListBox("", &current_category, vector_getter, static_cast<void*>(&categories_list_vector), categories_list_vector.size());
	ImGui::Spacing(0, 15);

	// Show prefabs in selected category.
	std::string & selectedCategoryName = categories_list_vector[current_category];
	PrefabVector & vectorPrefab = prefabs_by_category[selectedCategoryName];
	static int current_prefab_selected = 0;

	// Variables used for spawning an object.
	static CTransform spawnTransform = CTransform();

	// If we change category, we mark as selected by default the first prefab from the newly selected category.
	if (current_category != previouslySelectedCategory) {
		previouslySelectedCategory = current_category;
		current_prefab_selected = 0;
	}

	ImGui::Text("Category Prefabs");
	ImGui::ListBox(" ", &current_prefab_selected, prefab_by_categories_getter, static_cast<void*>(&vectorPrefab), vectorPrefab.size());
	ImGui::Spacing(0, 15);

	ImGui::Text("Prefab spawning variables");
	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	static char entityNameBuffer[128] = "\0";
	ImGui::InputText("Custom new entity name", entityNameBuffer, 128);

	ImGui::Text("Spawning position");
	spawnTransform.renderInMenu();
	ImGui::Spacing(0, 15);

	if(ImGui::Button("Set Spawn To Camera Position")){
		TCompCamera * cam = TCompCameraManager::getCurrentCamera();
		if(cam)
			spawnTransform.setPosition(cam->getPosition());
	}
	ImGui::Spacing(0, 15);

	if(ImGui::Button("Set Spawn To Camera Direction")){
		TCompCamera * cam = TCompCameraManager::getCurrentCamera();
		if (cam) {
			physx::PxRaycastBuffer hitData = PhysUtils::shootCameraRaycast(cam->getPosition(), cam->getFront(), 1000000000.0f);
			if (hitData.hasBlock)
				spawnTransform.setPosition(cam->getPosition() + cam->getFront() * hitData.block.distance);
		}
	}
	ImGui::Spacing(0, 15);

	if (ImGui::Button("Set Spawn LookAt To Camera Position")) {
		TCompCamera * cam = TCompCameraManager::getCurrentCamera();
		if (cam)
			spawnTransform.lookAt(spawnTransform.getPosition(), cam->getPosition());
	}
	ImGui::Spacing(0, 15);

	if (ImGui::Button("Set Spawn Decal To Camera Direction")) {
		TCompCamera * cam = TCompCameraManager::getCurrentCamera();
		if (cam) {
			physx::PxRaycastBuffer hitData = PhysUtils::shootCameraRaycast(cam->getPosition(), cam->getFront(), cam->getFar() + 1.f);
			if (hitData.hasBlock) {
				Vector3 pos = PXVEC3_TO_VEC3(hitData.block.position);
				Vector3 normal = PXVEC3_TO_VEC3(hitData.block.normal);
				Vector3 lok = pos + normal;
				spawnTransform.lookAt(pos, pos + normal);
			}
		}
	}
	ImGui::Spacing(0, 15);

	ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.6f, 0.6f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.7f, 0.7f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.8f, 0.8f));
	if (ImGui::Button("Spawn", ImVec2(100, 45)))
		spawnSelectedAsset(vectorPrefab[current_prefab_selected].second, spawnTransform, entityNameBuffer);


	ImGui::PopStyleColor(3);

	ImGui::Spacing(0, 15);

}

void CModuleEditor::fetchAllPrefabDirectories(std::vector<std::string> & save_directories) {
	save_directories.push_back("data/prefabs");
	for (auto & directory : std::filesystem::recursive_directory_iterator("data/prefabs")) {
		if (directory.status().type() == std::filesystem::file_type::directory)
			save_directories.push_back(directory.path().string());
	}
}

void CModuleEditor::fetchAllPrefabCategories(std::vector<std::string> & directories) {
	// Get the prefab categories parsed correctly to show to users.
	std::string prefixToTake = "data/prefabs\\";
	int prefixSize = prefixToTake.size();
	std::string barsToSubstitute = "\\";

	std::vector<std::string> categories;

	// Start by adding the root category for all prefabs on the base directory.
	addPrefabCategory(std::string("root"));

	// Now fetch all the other categories from the other prefab directories.
	for (int i = 1; i < directories.size(); ++i) {
		std::string directory = directories[i];

		// Start by removing the data/prefabs\\ from the category.
		directory = directory.substr(prefixSize, directory.size());

		size_t pos = 0;
		std::string categoryName = "";

		// Parse the category for the prefab.
		while ((pos = directory.find(barsToSubstitute)) != std::string::npos) {
			categoryName += directory.substr(0, pos) + "/";
			directory.erase(0, pos + barsToSubstitute.length());
		}
		if (directory.size() > 0)
			categoryName += directory;

		// Add the category.
		addPrefabCategory(categoryName);
	}
}

bool CModuleEditor::addPrefabCategory(CategoryName & category) {
	if (prefabs_by_category.find(category) != prefabs_by_category.end()) return false;
	prefabs_by_category[category] = PrefabVector();
}

void CModuleEditor::fetchAllPrefabsInsideCategories() {
	for (auto & category : prefabs_by_category) {
		std::string categoryName = category.first;
		if (categoryName == "root")
			categoryName = "/";
		else
			categoryName = "/" + categoryName;

		for (auto & prefabPath : std::filesystem::directory_iterator("data/prefabs" + categoryName)) {

			if (prefabPath.status().type() != std::filesystem::file_type::directory) {
				std::string pathString = prefabPath.path().string();
				std::string pathPrefab = "";

				// Parse prefab path. Convert all \\ to /.
				size_t pos = 0;
				std::string barsToSubstitute = "\\";
				while ((pos = pathString.find(barsToSubstitute)) != std::string::npos) {
					pathPrefab += pathString.substr(0, pos) + "/";
					pathString.erase(0, pos + barsToSubstitute.length());
				}
				if (pathString.size() > 0)
					pathPrefab += pathString;

				// Parse prefab name.
				// Check that the file is a .json.
				std::string jsonToSubstitute = ".json";
				if ((pos = pathString.find(jsonToSubstitute)) != std::string::npos) {
					size_t prefabStart = 0;
					while ((pos = pathString.find("/")) != std::string::npos) {
						pathString.erase(0, pos + 1);
						prefabStart = pos;
					}

					category.second.push_back(std::pair<PrefabName, PrefabPath>(pathString.erase(pathString.find(jsonToSubstitute), jsonToSubstitute.length()), pathPrefab));
				}
			}
		}
	}
}

void CModuleEditor::spawnSelectedAsset(const std::string & assetPath, const CTransform & transform, const char * entityNameBuffer) {
	TEntityParseContext ctx;
	ctx.root_transform = transform;
	parseScene(assetPath, ctx);

	if (ctx.entities_loaded.size() == 0) return;

	// Don't use instancing for prefabs created with the editor by default. This is to allow prefabs to be moved
	// and not appear disabled on creation.
	for (auto & ent : ctx.entities_loaded) {
		CEntity * entity = ent;
		TCompRender * render = entity->getComponent<TCompRender>();
		if(render)
			render->unregisterFromInstancing();
	}

	// Add asset.
	SpawnedAsset asset = { ctx.entities_loaded[0], assetPath };
	spawnedAssets.push_back(asset);

	// Rename first object from the prefab with the new name.
	if (ctx.entities_loaded.size() > 0) {
		CEntity * entity = ctx.entities_loaded[0];
		TCompName * name = entity->getComponent<TCompName>();
		if(name && std::strcmp(entityNameBuffer, "") != 0)
			name->setName(entityNameBuffer);
	}
}


// Render assets to export windows.
void CModuleEditor::renderAssetsToExportWindow() {
	// Remove asset if group entity not found.
	for (auto & it = spawnedAssets.begin(); it != spawnedAssets.end(); ++it) {
		auto & asset = *it;
		if (!asset.handleGroupEntity.isValid()) {
			spawnedAssets.erase(it);
			break;
		}
	}

	ImGui::Text("Entities to export");
	ImGui::Spacing(0, 10);
	ImGui::Separator();
	ImGui::Spacing(0, 10);

	// Show all assets that will be exported.
	int indx = 0;
	for (auto & asset : spawnedAssets) {
		CEntity * ent = asset.handleGroupEntity;
		if (!ent) continue;
		TCompName * name = ent->getComponent<TCompName>();

		ImGui::PushID("Entity-To-Export-" + indx);

		// Name of the asset.
		if (name)
			ImGui::Text("Asset Name: %s", name->getName());
		
		// Prefab type.
		ImGui::Text("Prefab: %s", asset.prefabPath.c_str());

		// Press to remove asset.
		if (ImGui::Button("Delete prefab"))
			asset.handleGroupEntity.destroy();

		// Add separator if not last one.
		if (indx != (spawnedAssets.size() - 1)) {
			ImGui::Spacing(0, 5);
			ImGui::Separator();
			ImGui::Spacing(0, 5);
		}

		indx++;
		ImGui::PopID();
	}


	ImGui::Spacing(0, 10);
	ImGui::Separator();
	ImGui::Spacing(0, 10);

	ImGui::Text("Export entities to data/editor/exported/ with the name choosen below.");

	static char exportedFileName[128] = "";
	ImGui::InputText("Export entities to file with name: ", exportedFileName, 128);
	ImGui::Spacing(0, 5);

	if (ImGui::Button("Export prefabs to file"))
		exportEntitiesToFile(exportedFileName);

	ImGui::Spacing(0, 10);
}

void CModuleEditor::exportEntitiesToFile(const std::string & exportedFileName) {
	// Right now, our exporter only support exporting prefabs as they are created, if a prefab has more than one entity
	// but some of them are deleted or modified, the exporter will still export the normal unmodified version. This is due to the fact
	// that we have not implemented a method to save the settings of each component and therefore, we can't save each entity individual changes
	// without implementing it. As we don't have time to start implementing it, we are simply keeping with saving the prefab path and
	// the name for the first entity and the transform for it. Nevertheless, we probably won't have group prefabs.

	json exportedEntities;
	for (auto & asset : spawnedAssets) {
		json entity;
		if (!asset.handleGroupEntity.isValid()) continue;
		CEntity * ent = asset.handleGroupEntity;
		if (!ent) continue;
		TCompName * name = ent->getComponent<TCompName>();

		// Name.
		if(name)
			entity["entity"]["name"] = name->getName();

		TCompTransform * transform = ent->getComponent<TCompTransform>();
		if (transform) {
			// Position.
			Vector3 & pos = transform->getPosition();
			std::string positionAsString = std::to_string(pos.x) + " " + std::to_string(pos.y) + " " + std::to_string(pos.z);
			entity["entity"]["transform"]["pos"] = positionAsString;

			// Get yaw, pitch, roll as degrees to export.
			float yawAsDeg, pitchAsDeg, rollAsDeg;
			transform->getAngles(&yawAsDeg, &pitchAsDeg, &rollAsDeg);
			yawAsDeg = RAD2DEG(yawAsDeg);
			pitchAsDeg = RAD2DEG(pitchAsDeg);
			rollAsDeg = RAD2DEG(rollAsDeg);
			std::string angles = std::to_string(pitchAsDeg) + " " + std::to_string(yawAsDeg) + " " + std::to_string(rollAsDeg);
			entity["entity"]["transform"]["angles"] = angles;

			// Scale.
			Vector3 & scale = transform->getScale();
			std::string scaleAsString = std::to_string(scale.x) + " " + std::to_string(scale.y) + " " + std::to_string(scale.z);
			entity["entity"]["transform"]["scale"] = scaleAsString;
		}

		entity["entity"]["prefab"] = asset.prefabPath;
		exportedEntities.push_back(entity);
	}

	std::ofstream o("data/editor/exported/" + exportedFileName + ".json");
	o << std::setw(4) << exportedEntities << std::endl;

}

void CModuleEditor::renderCurveEditor() {
	ImGui::PushID("Curve-Editor-Sections");
	if (ImGui::BeginTabBar("Curve Sections", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Create Curve")) {
			renderCreateCurve();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("View Curves")) {
			EngineResources.renderResourceTypeInMenu<CTransformCurve>();
			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}
	ImGui::PopID();
}

void CModuleEditor::renderCreateCurve() {
	ImGui::Text("Create curves by using this ingame editor. It will allow you to see the final shape of the curve as it's created.");

	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	// Variables used for spawning an object.
	ImGui::Text("Next knot parameters");
	ImGui::Spacing(0, 5);
	knotToPlace.renderInMenuForCameraCurves();
	int currentKnotEditingAsShownToPlayer = currentKnotEditing+1;
	if (ImGui::DragInt("Current knot editing", &currentKnotEditingAsShownToPlayer, 1.0, 0, editorCurve.getKnots().size()))
		currentKnotEditing = currentKnotEditingAsShownToPlayer - 1;
	ImGui::Spacing(0, 5);

	// Set the camera pos as the knot position.
	ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.6f, 0.6f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.7f, 0.7f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.8f, 0.8f));
	if (ImGui::Button("Set transform to current Pos", ImVec2(180, 20))) {
		TCompCamera * cam = TCompCameraManager::getCurrentCamera();
		knotToPlace.setPosition(cam->getPosition());
		
		float yaw, pitch, roll = 0.0f;
		vectorToYawPitch(cam->getFront(), yaw, pitch);
		knotToPlace.setAngles(yaw, pitch, roll);
	}
	ImGui::PopStyleColor(3);

	ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.6f, 0.6f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.7f, 0.7f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.8f, 0.8f));
	if (ImGui::Button("Place knot", ImVec2(80, 25))) {
		editorCurve.addKnot(currentKnotEditing, knotToPlace);
		currentKnotEditing = editorCurve.getKnots().size();
	}
	ImGui::PopStyleColor(3);
	
	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	// Render the knots in the curve.
	editorCurve.renderInMenuEditor(knotToPlace, currentKnotEditing);

	// We update the knot value each frame so it shows the effects in real time in the render debug.
	editorCurve.addKnot(currentKnotEditing, knotToPlace);

	ImGui::Spacing(0, 5);
	ImGui::Separator();
	ImGui::Spacing(0, 5);

	// Place the curve.
	static char scriptBuffer[200] = "";
	ImGui::InputText("Filename for the curve", scriptBuffer, 200);
	ImGui::Spacing(0, 5);

	ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.6f, 0.6f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.7f, 0.7f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(6.0f / 7.0f, 0.8f, 0.8f));
	if (ImGui::Button("Create curve", ImVec2(100, 25))) {
		// If we are at the end of the tail, this is the always updating knot, not placed.
		if (currentKnotEditing == editorCurve.getKnots().size())
			editorCurve.removeKnot(currentKnotEditing);

		saveCurveToJson(scriptBuffer);
	}
	ImGui::PopStyleColor(3);

	ImGui::Spacing(0, 10);
}

void CModuleEditor::saveCurveToJson(const std::string & curveName){
	//Load the configuration of the module
	std::string savePath = "data/curves/";
	
	json curve;
	for (auto & knot : editorCurve.getKnots()) {
		
		float yaw, pitch, roll;
		knot.getAngles(&yaw, &pitch, &roll);
		
		Vector3 pos = knot.getPosition();
		std::string position = std::to_string(pos.x) + " " + std::to_string(pos.y) + " " + std::to_string(pos.z);
		std::string angles = std::to_string(RAD2DEG(pitch)) + " " + std::to_string(RAD2DEG(yaw)) + " " + std::to_string(RAD2DEG(roll));

		json transformInfo;
		transformInfo["pos"] = position;
		transformInfo["angles"] = angles;
		curve.push_back(transformInfo);
	}
	
	std::ofstream file(savePath + curveName + ".trans_curve");
	file << std::setw(4) << curve << std::endl;
}

void CModuleEditor::renderDebug(){
	drawAxis(knotToPlace.asMatrix());
	
	// If we are at the end of the tail, this is the always updating knot, not placed.
	if (currentKnotEditing == editorCurve.getKnots().size())
		editorCurve.removeKnot(currentKnotEditing);
	
	editorCurve.renderDebug();
	EngineResources.renderResourceTypeInRenderDebug<CTransformCurve>();
}