#include "mcv_platform.h"
#include "gamestate_container.h"

void CGamestateContainer::addGamestate(CGamestate & gamestate) {
#ifdef DEBUG
	const std::string & gameStateName = gamestate->_name;
	// Gets sure the gamestate is not already added.
	// We only check in debug, we could also check on release
	// due to this operation not being too expensive but we won't.
	// This should not happen.
	auto it = _searchGamestate.find(gamestate);
	if (it != _searchGamestate.end())
		assert("Module: " + gameStateName.c_str() + " has been added already");
#endif

	_gamestates.push_back(gamestate);
	_searchGamestate[gamestate._name] = (int)_gamestates.size() - 1;
}

CGamestate * CGamestateContainer::findGameState(const std::string & gamestateName) {
	auto it = _searchGamestate.find(gamestateName);
	return it != _searchGamestate.end() ? &(_gamestates[it->second]) : NULL;
}

void CGamestateContainer::clear() {
	_gamestates.clear();
	_searchGamestate.clear();
}