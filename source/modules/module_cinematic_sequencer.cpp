#include "mcv_platform.h"
#include "module_cinematic_sequencer.h"
#include "components/camera/comp_camera_manager.h"
#include "logic/logic_manager.h"
#include "components/common/comp_name.h"
#include "components/weapons/comp_weapons_manager.h"

CCinematicSequencer::CCinematicSequencer(const std::string & name) : IModule(name) {}

bool CCinematicSequencer::start() { return true; }

void CCinematicSequencer::update(float dt) {}

void CCinematicSequencer::renderDebug(){}

void CCinematicSequencer::StartCinematic(const std::string & CinematicPath){
	CameraUsedBeforeCinematic = LogicManager::Instance().getDefaultCamera();
	CurrentCinematicPlaying = LogicManager::Instance().CreateEntity(CinematicPath, Vector3::Zero);
	LogicManager::Instance().deactivateWidget("hud");
}

void CCinematicSequencer::updateUI()
{
	if (CEntity* player = getEntityByName("Player")) {
		if (TCompWeaponManager* weaponManager = player->getComponent<TCompWeaponManager>()) {
			weaponManager->getCurrentWeapon()->updateAmmoUI();
		}
	}
}

void CCinematicSequencer::EndCinematic(){
	if (!CurrentCinematicPlaying.isValid()) return;
	CurrentCinematicPlaying.destroy();
	
	if (!CameraUsedBeforeCinematic.isValid()) return;
	LogicManager::Instance().setDefaultCamera(CameraUsedBeforeCinematic);
	LogicManager::Instance().activateWidget("hud");
	updateUI();
}

void CCinematicSequencer::PlayLevelSequence(const std::string & LevelSequencePath) {
	if(LevelSequencesPlaying.count(LevelSequencePath)) return;

	CHandle SequenceHandle = LogicManager::Instance().CreateEntity(LevelSequencePath, Vector3::Zero);
	LevelSequencesPlaying[LevelSequencePath] = SequenceHandle;
	LogicManager::Instance().deactivateWidget("hud");
}

void CCinematicSequencer::EndLevelSequence(const std::string & LevelSequencePath) {
	if (!LevelSequencesPlaying.count(LevelSequencePath)) return;
	LevelSequencesPlaying.erase(LevelSequencePath);
	LogicManager::Instance().activateWidget("hud");
	updateUI();
}