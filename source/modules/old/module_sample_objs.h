#pragma once

#include "modules/module.h"
#include "render/shaders/technique.h"

#include "geometry/transform.h"
#include "utils/json_resource.h"


class CMesh;
class CTexture;

class AIRotator;


class CModuleSampleObjs : public IModule
{
public:
	struct TObj {
		CTransform  transform;
		std::string name;
		int         id = 0;
		Vector4        color = Vector4(0, 1, 0, 1);
		float       speed = 1.0f;
		float       rotation_speed = 1.0f;
		static int player_id;

		AIRotator * aictrl;

		const CMesh *    mesh = nullptr;
		const CTexture * texture = nullptr;

		Matrix getWorld() const;
		TObj();
		void update(float elapsed);
		void renderInMenu();

		static int counter;
	};

	static TObj * player;

private:
	std::vector< TObj > objs;
	const CTechnique * tech_objs;
	const CJson * json_res = nullptr;

	void renderObjs();
	void updateObjs(float dt);
	void renderDebugObjs();

public:

	CModuleSampleObjs(const std::string & name);

	bool start() override;
	void stop() override;
	void update(float dt);
	void render();
	void renderInMenu() override;
};