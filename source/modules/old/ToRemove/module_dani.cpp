#include "mcv_platform.h"
#include "module_dani.h"
#include "jsonFunctions.h"

#include "engine.h"
#include "render/primitives.h"
#include "render/meshes/mesh.h"
#include "render/textures\texture.h"

#include "geometry/angular.h"

#include "utils/json.hpp"

#include "render/module_render.h"

int CModuleDani::TEntity::player_id = 0;
int CModuleDani::TEntity::counter = 0;
float CModuleDani::entityDefaultScale = 1;
Vector3 CModuleDani::ref_point;
Vector4 CModuleDani::ref_color = Vector4(0, 1, 0, 1);

void to_json(json & j, const CModuleDani::TEntity & obj) {
	j["transform"] = obj.transform;
	j["name"] = obj.name;
	j["speed"] = obj.speed;
}

void from_json(const json& j, CModuleDani::TEntity& obj) {
	obj.name = j.value("name", obj.name);
	obj.speed = j.value("speed", obj.speed);


	/* I leave this commented in case anyone wants to use this for this module */
	/*
	CResourcesManager & resources = CEngine::instance().getResourceManager();
	std::string mesh_name = j.value("mesh", "data/meshes/Teapot001.mesh");
	obj.mesh = resources.getResource(mesh_name)->as<CMesh>();

	std::string texture_name = j.value("mesh", "data/textures/piedras_c.dds");
	obj.texture = resources.getResource(texture_name)->as<CTexture>();
	*/

	obj.mesh = &cube; // Pointer to the cube primitive.
}

CModuleDani::CModuleDani(const std::string & name) : IModule(name){}

bool CModuleDani::start()
{

	entitiesTechnique = CEngine::instance().getResourceManager().getResource("data/techniques/debug.tech")->as<CTechnique>();

	// Read entities from json
	json_res = CEngine::instance().getResourceManager().getResource("data/entities.json")->as<CJson>();
	entities = json_res->getJson().get< std::vector<TEntity> >();

	cameraEye = camera.getEye();
	cameraTarget = camera.getTarget();
	cameraUp = camera.getUp();

	return true;
}

void CModuleDani::stop()
{
	CModuleDani::TEntity::counter = 0;
}

void CModuleDani::update(float dt)
{
	updateObjs(dt);
}

void CModuleDani::renderInMenu() {
	renderDebugObjs();
}

void CModuleDani::render()
{
	// Render grid and axis of the world.
	tech_debug->activate();
	grid.activateAndRender();
	axis.activateAndRender();

	// Render an axis that works as a reference point.
	ctes_object.World = Matrix::CreateTranslation(ref_point);
	ctes_object.ObjColor = ref_color;
	ctes_object.updateGPU();
	entitiesTechnique->activate();
	cube.activateAndRender();

	renderObjs();
}

void CModuleDani::renderObjs() {
	for (auto & ent : entities) {
		ent.transform.setScale(entityDefaultScale); // Cube scale.
		ctes_object.World = ent.getWorld();
		ctes_object.ObjColor = ent.color;
		ctes_object.updateGPU();

		entitiesTechnique->activate();

		if (ent.mesh)
			ent.mesh->activateAndRender();

		ent.transform.setScale(2); // axis scale.
		ctes_object.World = ent.getWorld();
		ctes_object.updateGPU();
		tech_debug->activate();
		axis.activateAndRender();
	}
}

void CModuleDani::renderDebugObjs() {
	if (ImGui::TreeNode("Create new entity.")) {
		std::string textEntityName = "Entity name will be: Entity - " + std::to_string(entities.size() + 1);
		ImGui::Text(textEntityName.c_str());
		ImGui::DragFloat3("Position", &nEntityPos.x, 0.1f, -10.f, 10.f);
		ImGui::DragFloat3("Rotation", &nEntityRotation.x, 0.1f, -10.f, 10.f);
		ImGui::DragFloat("Scale", &nEntityScale, 0.1f, 0.2f, 10.f);
		ImGui::DragFloat4("Color", &nEntityColor.x, 0.05f, -1.f, 1.f);
		if (ImGui::SmallButton("Create.")) {
			TEntity nEntity;
			nEntity.name = "Entity - " + std::to_string(entities.size() + 1);
			nEntity.mesh = &cube;
			nEntity.transform.setPosition(nEntityPos);
			nEntity.transform.setRotation(Quaternion::CreateFromYawPitchRoll(nEntityRotation.x, nEntityRotation.y, nEntityRotation.z));
			nEntity.transform.setScale(nEntityScale);
			nEntity.color = nEntityColor;
			entities.push_back(nEntity);
		}
		ImGui::TreePop();
	}
	
	if (ImGui::TreeNode("Modify reference point")) {
		ImGui::DragFloat3("Ref Point", &ref_point.x, 0.1f, -10.f, 10.f);
		ImGui::TreePop();
	}

	if (ImGui::TreeNode("Move camera")) {
		ImGui::DragFloat3("Eye", &cameraEye.x, 0.1f, -10.f, 10.f);
		ImGui::DragFloat3("Target", &cameraTarget.x, 0.1f, -10.f, 10.f);
		ImGui::DragFloat3("Up", &cameraUp.x, 0.1f, -10.f, 10.f);
		if (ImGui::SmallButton("Set new values for camera."))
			camera.lookAt(cameraEye, cameraTarget, cameraUp);
		ImGui::TreePop();
	}

	if (ImGui::TreeNode("Show all entities.")) {
		for (auto& obj : entities)
			obj.renderInMenu();
		ImGui::TreePop();
	}
}

void CModuleDani::updateObjs(float delta) {
	for (auto& obj : entities)
		obj.update(delta);
}

Matrix CModuleDani::TEntity::getWorld() const {
	return transform.asMatrix();
}

CModuleDani::TEntity::TEntity() {
	char buf[64];
	sprintf(buf, "Obj %d", counter);
	name = buf;
	id = counter;
	counter++;
}

void CModuleDani::TEntity::update(float elapsed) {
	if (Utils::isPressed('P') && Utils::isPressed('2'))
		CEngine::instance().getModuleManager().changeToGamestate("gs_gametesting");

	if (player_id == id) {
		Vector3 front = transform.getFront();
		Vector3 pos = transform.getPosition();

		if (Utils::isPressed('W')) {
			pos = pos + front * speed * elapsed;
		}
		if (Utils::isPressed('S')) {
			pos = pos - front * speed * elapsed;
		}
		transform.setPosition(pos);
		if (Utils::isPressed('A')) {
			Quaternion qYaw = Quaternion::CreateFromAxisAngle(Vector3(0, 1, 0), elapsed * rotation_speed);
			transform.setRotation(transform.getRotation() * qYaw);
		}
		if (Utils::isPressed('D')) {
			Quaternion qYaw = Quaternion::CreateFromAxisAngle(Vector3(0, 1, 0), -elapsed * rotation_speed);
			transform.setRotation(transform.getRotation() * qYaw);
		}
		if (Utils::isPressed('Z')) {
			Quaternion qPitch = Quaternion::CreateFromAxisAngle(transform.getLeft(), -elapsed * rotation_speed);
			transform.setRotation(transform.getRotation() * qPitch);
		}
		if (Utils::isPressed('X')) {
			Quaternion qPitch = Quaternion::CreateFromAxisAngle(transform.getLeft(), elapsed * rotation_speed);
			transform.setRotation(transform.getRotation() * qPitch);
		}
	}
	else {
		// soy un bot...
	}
}

void CModuleDani::TEntity::renderInMenu() {
	if (ImGui::TreeNode(name.c_str())) {

		ImGui::Text("Player transform");
		transform.renderInMenu();
		ImGui::Separator();

		float yaw_to_ref_point = transform.getDeltaYawToAimTo(ref_point);
		float deg_yaw_to_ref_point = RAD2DEG(yaw_to_ref_point);
		ImGui::LabelText("Delta Yaw to Ref", "%f", deg_yaw_to_ref_point);

		if (ImGui::SmallButton("Look to ref Point")) {
			float yaw, pitch;
			transform.getAngles(yaw, pitch);
			yaw += yaw_to_ref_point * 0.1f;
			transform.setAngles(yaw, pitch);
		}

		if (ImGui::SmallButton("Make it the player"))
			player_id = id;

		json j = *this;
		std::string jstr = j.dump(2);
		ImGui::Text("%s", jstr.c_str());

		ImGui::TreePop();
	}
}
