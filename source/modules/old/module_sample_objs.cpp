#include "mcv_platform.h"
#include "module_sample_objs.h"
#include "jsonFunctions.h"

#include "engine.h"
#include "render/primitives.h"
#include "render/meshes/mesh.h"
#include "render/textures\texture.h"

#include "geometry/angular.h"

#include "utils/json.hpp"

#include "components\aicontroller\ai_controller.h"
#include "components\aicontroller\ai_rotator.h"

int CModuleSampleObjs::TObj::player_id = 0;
CModuleSampleObjs::TObj * CModuleSampleObjs::player = nullptr;
int CModuleSampleObjs::TObj::counter = 0;

Vector3                ref_point;

void to_json(json & j, const CModuleSampleObjs::TObj & obj) {
	j["transform"] = obj.transform;
	j["name"] = obj.name;
	j["speed"] = obj.speed;
}

void from_json(const json& j, CModuleSampleObjs::TObj& obj) {
	//j["pos"] = obj.transform.getPosition();
	obj.name = j.value("name", obj.name);
	obj.speed = j.value("speed", obj.speed);

	CResourcesManager & resources = CEngine::instance().getResourceManager();
	std::string mesh_name = j.value("mesh", "data/meshes/Teapot001.mesh");
	obj.mesh = resources.getResource(mesh_name)->as<CMesh>();

	std::string texture_name = j.value("mesh", "data/textures/piedras_c.dds");
	obj.texture = resources.getResource(texture_name)->as<CTexture>();
}

CModuleSampleObjs::CModuleSampleObjs(const std::string & name) : IModule(name){}

bool CModuleSampleObjs::start()
{
	tech_objs = CEngine::instance().getResourceManager().getResource("data/techniques/objs.tech")->as<CTechnique>();

	// Read entities from json
	json_res = CEngine::instance().getResourceManager().getResource("data/entities.json")->as<CJson>();
	objs = json_res->getJson().get< std::vector<TObj> >();

	return true;
}

void CModuleSampleObjs::stop()
{
	CModuleSampleObjs::TObj::counter = 0;
}

void CModuleSampleObjs::update(float dt)
{
	player = &objs[0];
	updateObjs(dt);
}

void CModuleSampleObjs::renderInMenu() {
	renderDebugObjs();
}

void CModuleSampleObjs::render()
{
	activateObject(Matrix::Identity);
	tech_debug->activate();

	grid.activateAndRender();
	axis.activateAndRender();

	activateObject(Matrix::CreateTranslation(ref_point));
	axis.activateAndRender();

	drawCircle(Vector3(1, 0, 0), 5.0f, Vector4(1, 1, 0, 1));

	renderObjs();
}

Matrix CModuleSampleObjs::TObj::getWorld() const {
	return transform.asMatrix();
}

CModuleSampleObjs::TObj::TObj() {
	char buf[64];
	sprintf(buf, "Obj %d", counter);
	name = buf;
	id = counter;
	counter++;
	if (id != player_id) {
		AIRotator * ai = new AIRotator();
		aictrl = ai;
		aictrl->Init();
	}
}

void CModuleSampleObjs::TObj::update(float elapsed) {
	if (Utils::isPressed('P') && Utils::isPressed('3'))
		CEngine::instance().getModuleManager().changeToGamestate("gs_logictesting");

	if (player_id == id) {
		Vector3 front = transform.getFront();
		Vector3 pos = transform.getPosition();

		if (Utils::isPressed('W')) {
			pos = pos + front * speed * elapsed;
		}
		if (Utils::isPressed('S')) {
			pos = pos - front * speed * elapsed;
		}
		transform.setPosition(pos);
		if (Utils::isPressed('A')) {
			Quaternion qYaw = Quaternion::CreateFromAxisAngle(Vector3(0, 1, 0), elapsed * rotation_speed);
			transform.setRotation(transform.getRotation() * qYaw);
		}
		if (Utils::isPressed('D')) {
			Quaternion qYaw = Quaternion::CreateFromAxisAngle(Vector3(0, 1, 0), -elapsed * rotation_speed);
			transform.setRotation(transform.getRotation() * qYaw);
		}
		if (Utils::isPressed('Z')) {
			Quaternion qPitch = Quaternion::CreateFromAxisAngle(transform.getLeft(), -elapsed * rotation_speed);
			transform.setRotation(transform.getRotation() * qPitch);
		}
		if (Utils::isPressed('X')) {
			Quaternion qPitch = Quaternion::CreateFromAxisAngle(transform.getLeft(), elapsed * rotation_speed);
			transform.setRotation(transform.getRotation() * qPitch);
		}
	}
	else {
		// soy un bot...
		aictrl->setPlayerPos(player);
		aictrl->obj = this;
		aictrl->update(elapsed);
	}
}

void CModuleSampleObjs::TObj::renderInMenu() {
	if (ImGui::TreeNode(name.c_str())) {
		transform.renderInMenu();

		float yaw_to_ref_point = transform.getDeltaYawToAimTo(ref_point);
		float deg_yaw_to_ref_point = RAD2DEG(yaw_to_ref_point);
		ImGui::LabelText("Delta Yaw to Ref", "%f", deg_yaw_to_ref_point);

		if (ImGui::SmallButton("Look to ref Point")) {
			float yaw, pitch;
			transform.getAngles(yaw, pitch);
			yaw += yaw_to_ref_point * 0.1f;
			transform.setAngles(yaw, pitch);
		}

		if (ImGui::SmallButton("Make it the player"))
			player_id = id;

		json j = *this;
		std::string jstr = j.dump(2);
		ImGui::Text("%s", jstr.c_str());

		ImGui::TreePop();
	}
}

void CModuleSampleObjs::renderObjs() {
	axis.activate();
	for (auto& obj : objs) {
		ctes_object.World = obj.getWorld();
		ctes_object.updateGPU();

		tech_objs->activate();
		if (obj.texture)
			obj.texture->activate(0);

		//if (obj.mesh)
		//	obj.mesh->activateAndRender();

		Vector3 pos = obj.transform.getPosition();
		//drawLine(pos, pos + obj.transform.getFront() * 2.0f, VEC4(1, 1, 0, 1));
		drawLine(Vector3(10, 0, 10), Vector3(-10, 0, -10), Vector4(1, 0, 1, 1));

		tech_debug->activate();
		axis.render();
	}
}

void CModuleSampleObjs::renderDebugObjs() {
	if (ImGui::SmallButton("New..."))
		objs.push_back(TObj());

	ImGui::DragFloat3("Ref Point", &ref_point.x, 0.1f, -10.f, 10.f);

	for (auto& obj : objs)
		obj.renderInMenu();
}

void CModuleSampleObjs::updateObjs(float delta) {
	for (auto& obj : objs)
		obj.update(delta);
}
