#pragma once
#include "modules/module.h"

class CModuleBoot : public IModule
{
public:
	CModuleBoot(const std::string& name);
	bool start() override;
	void stop() override {};
	void update(float dt) override;
	void renderDebug() override {};
	void renderInMenu() override;

	void loadGamestateBlocking(std::string gamestate);
	void startLoadGamestateNonBlocking(std::string gamestate);
	void loadGamestateNonBlocking();

	bool isPerformingNonBlockingLoading() { return _isLoading; }

private:
	json _jsonFile;

	// When we want to load without blocking
	float _frameDuration = 0.033f; // Default 30 frames
	bool _isLoading = false; 
	std::vector<std::string> _prefabs; 
	std::vector<std::string>::iterator _prefabIt;
	TEntityParseContext* _ctx;
	std::string _currentGamestate = "";
	// We will use this to clear the entities from the previous gamestate
	std::string _lastGamestate = "";

	std::map<std::string, VHandles> _entitiesPerGamestate;

	// Progress bar loading UI variables
	float _depth0EntityCount = 0.0f;
	float _entitiesLoadedCount = 0.0f;

	void sendMessagesToEntitiesLoaded(std::string gamestate);
	void clearLastGamestateEntities(std::string gamestate);

};