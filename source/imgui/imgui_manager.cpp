#include "mcv_platform.h"
#include "imgui/imgui_manager.h"

#include "render/render.h"
#include "windows/app.h"

#include "imgui/imgui_source/imgui_impl_win32.h"
#include "imgui/imgui_source/imgui_impl_dx11.h"

#include "engine.h"
#include "modules/module_manager.h"
#include "modules/module_entities.h"
#include "render/module_render.h"
#include "modules/module_editor.h"
#include "resources/resource_manager.h"
#include "utils/json_resource.h"
#include "components/camera/comp_camera_manager.h"
#include "components/camera/comp_camera.h"

CEngineImGUIManager::CEngineImGUIManager(){}

CEngineImGUIManager::~CEngineImGUIManager() {}

CEngineImGUIManager & CEngineImGUIManager::instance() {
	static CEngineImGUIManager instance;
	return instance;
}

bool CEngineImGUIManager::init(CModuleManager * nModuleManager, CResourcesManager * nResourcesManager) {
	PROFILE_FUNCTION("CEngineImGUIManager::init");

	json cfg = Utils::loadJson("data/config.json");
	loadEditor = cfg.value("load_editor", loadEditor);
	showWindow = cfg.value("show_imgui", showWindow);
	
	// Module manager and resources manager.
	moduleManager = nModuleManager;
	resourcesManager = nResourcesManager;

	// Load modules that are used for fast access.
	moduleEntities = (CModuleEntities *)nModuleManager->getModule("entities");
	moduleEditor = (CModuleEditor *)nModuleManager->getModule("editor");

	// Load pinned modules that can be accessed fast from the ImGui manager.
	json jsonData = Utils::loadJson("data/imgui.json");
	for (auto moduleName : jsonData["imgui-marked-modules"])
		pinnedModules.insert(moduleName.get<std::string>());
	
	// Load all modules that can be shown as windows.
	for (auto moduleName : jsonData["imgui-show-modules-as-windows"]) {
		std::string name = moduleName.get<std::string>();
		IModule * module = moduleManager->getModule(name);
		if (module)
			ShowModuleInWindow(module);
		else {
			std::string textMessage = "Module with name: " + name + " was not found in data/imgui.json it won't be shown as a window.";
			Utils::dbg(textMessage.c_str());
		}
	}

	// Setup ImGui context

	// Get pointer for the app used to bind ImGui.
	auto & app = CApplication::instance();

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	// Define custom color theme
	std::string theme = jsonData.value("theme", "");
	if (theme == "Dark")
		ImGui::StyleColorsDark();
	else if (theme == "Light")
		ImGui::StyleColorsLight();
	else if (theme == "Cherry")
		ImGui::CherryTheme();
	else if (theme == "RayTeak")
		ImGui::StyleColorsRayTeak();
	else
		ImGui::StyleColorsClassic();

	ImGui_ImplWin32_Init(app.getWindowHandler());
	ImGui_ImplDX11_Init(Render.device, Render.ctx);

	// Load extra fonts
	ImGuiIO& io = ImGui::GetIO();
	_loadedFonts["default"] = io.Fonts->AddFontDefault();
	_loadedFonts["ruda_regular_16"] = io.Fonts->AddFontFromFileTTF("data/fonts/imgui/Ruda-Regular.ttf", 16.0f);

	return true;
}

void CEngineImGUIManager::stop() {
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

// Start the Dear ImGui frame
void CEngineImGUIManager::beginImguiFrame() {
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
}

// Once all rendering is done, both ImGUI and render modules, call this function for
// showing the imgui content.
void CEngineImGUIManager::endImguiFrame() {
	PROFILE_FUNCTION("endImgui");
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

void CEngineImGUIManager::update(float dt) {
	// Cheats.
	if (EngineInput[VK_F9].justPressed()) {
		TCompCamera * Camera = TCompCameraManager::getCurrentCamera();
		if (Camera)
			EngineScripting.call("TeleportPlayer", Camera->getPosition());
	}

	if (EngineInput[VK_F5].justPressed())
		showImGuiDemo = !showImGuiDemo;
	if (EngineInput[VK_F6].justPressed())
		showOverlay = !showOverlay;
	if (EngineInput[VK_CONTROL].isPressed() && EngineInput[VK_F1].justPressed())
		showWindow = !showWindow;
}

void CEngineImGUIManager::renderImguiEngineWindow() {
	PROFILE_FUNCTION("ImGui - RenderInMenu");
	if (showImGuiDemo)
		ImGui::ShowDemoWindow();
	if (showOverlay)
		renderOverlay();
	if (!showWindow)
		return;

	// Create a window called "Engine - ImGUI", with a menu bar.
	if (!ImGui::Begin("Engine - ImGUI", &showWindow, ImGuiWindowFlags_MenuBar
		| ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_AlwaysHorizontalScrollbar)) {
		return; // Early out if the window is collapsed, as an optimization.
	}

	// We render the menu bar showing the different sections of our debugging tool
	renderMenuBar();
	// After the menu bar, we render the content of the currently selected view.
	renderCurrentContent();
	// Render any opened windows.
	renderWindows();

	ImGui::End(); // This line closes the window opened in the method. It must be closed each frame.
}

void CEngineImGUIManager::renderMenuBar() {
	// Menu
	if (ImGui::BeginMenuBar())
	{
		if (ImGui::Button("Engine"))
		{
			currentSection = SEngineImGUISections::ENGINE_SECTION;
		}
		if (ImGui::Button("Scene"))
		{
			currentSection = SEngineImGUISections::SCENE_SECTION;
		}
		if (loadEditor) {
			if (ImGui::Button("Editor"))
			{
				currentSection = SEngineImGUISections::EDITOR_SECTION;
			}
		}
		if (ImGui::BeginMenu("Window"))
		{
			ImGui::MenuItem("Style Editor", NULL, &show_style_editor);
			if (ImGui::MenuItem("Quit", "CTRL+F1")) { showWindow = false; }
			ImGui::EndMenu();
		}
		ImGui::EndMenuBar();
	}
}

void CEngineImGUIManager::renderCurrentContent(){
	switch (currentSection) {
	case SEngineImGUISections::ENGINE_SECTION:
		renderEngineSection();
		break;
	case SEngineImGUISections::SCENE_SECTION:
		renderSceneSection();
		break;
	case SEngineImGUISections::EDITOR_SECTION:
		renderEditorSection();
		break;
	default:
		break;
	}

	if (show_style_editor) { ImGui::Begin("Style Editor", &show_style_editor); ImGui::ShowStyleEditor(); ImGui::End(); }
}

void CEngineImGUIManager::renderEngineSection(){
	ImGui::PushID("Engine-Section");

	if (ImGui::BeginTabBar("Engine - Sections", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Entities"))
		{
			moduleEntities->renderEntitiesMenu();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Components"))
		{
			moduleEntities->renderComponentsMenu();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Modules"))
		{
			if (ImGui::BeginTabBar("Modules - Sections", ImGuiTabBarFlags_None))
			{
				if (ImGui::BeginTabItem("Show all Modules")) {
					PROFILE_FUNCTION("RenderInMenu - Modules");
					moduleManager->renderInMenu();
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("Modules followed")) {
					moduleManager->renderFollowedModules(pinnedModules);
					ImGui::EndTabItem();
				}
				ImGui::EndTabBar();
			}
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Resources"))
		{
			PROFILE_FUNCTION("RenderInMenu - Resources");
			resourcesManager->renderInMenu();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Debug"))
		{
			if (ImGui::BeginTabBar("Debug - Sections", ImGuiTabBarFlags_None)) {
				if (ImGui::BeginTabItem("Render Debug variables.")) {

					ImGui::Spacing(0, 5);
					static bool isActiveRenderDebug = EngineRender.getRenderDebugActive();
					if (ImGui::Checkbox("Activate renderDebug", &isActiveRenderDebug))
						EngineRender.toggleRenderDebug();
					ImGui::Spacing(0, 5);

					static bool wireframeMode = EngineModules.isModuleInRenderDebug("render");
					if (ImGui::Checkbox("Activate wireframe", &wireframeMode)) {
						if (wireframeMode) {
							EngineModules.activateRenderDebug("render");
							EngineRender.setRenderDebug(true);
						}
						else
							EngineModules.removeRenderDebug("render");
					}
					ImGui::Spacing(0, 5);


					static bool activatePlane = false;
					CEntity * entity = getEntityByName("Plane o como se llame");
					ImGui::Checkbox("Activate plane", &activatePlane);
					if(entity && activatePlane)
						entity->renderDebug();

					ImGui::EndTabItem();
				}
				ImGui::EndTabBar();
			}
			ImGui::EndTabItem();
		}
		
		ImGui::EndTabBar();
	}

	ImGui::PopID();
}

void CEngineImGUIManager::renderSceneSection() {
	ImGui::PushID("Scene-Section");
	
	if (ImGui::BeginTabBar("Engine - Sections", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("GameState and Modules")) {
			moduleManager->renderGamestate();
			moduleManager->renderActiveModules();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Entities")) {
			moduleEntities->renderEntitiesMenu();
			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}

	ImGui::PopID();
}

void CEngineImGUIManager::renderEditorSection(){
	//This if should never occur, but it's fail-safe
	if (!loadEditor)
		return;

	ImGui::PushID("Editor-Section");
	if (ImGui::BeginTabBar("Editor - Sections", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Editor")) {
			moduleEditor->renderInMenu();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Entities")) {
			moduleEntities->renderEntitiesMenu();
			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}
	ImGui::PopID();
}

// Checks if module is being shown as window.
bool CEngineImGUIManager::isBeingShownAsWindow(const std::string & nameModule) {
	auto it = showModuleInWindow.find(nameModule);
	if (it != showModuleInWindow.end())
		return true;
	return false;
}

void CEngineImGUIManager::showModuleInWindowButton(IModule * module) {
	ImGui::Spacing();
	ImGui::Separator();
	ImGui::Spacing();

	bool oldShowAsWindow = module->showAsWindow;
	std::string checkBoxText = "Show " + module->getName() + " as window.";

	ImGui::Checkbox(checkBoxText.c_str(), &module->showAsWindow);
	if (oldShowAsWindow != module->showAsWindow) {
		if (module->showAsWindow)
			ShowModuleInWindow(module);
		else
			RemoveModuleFromShowingInWindow(module);
	}
}

void CEngineImGUIManager::ShowModuleInWindow(IModule * module){
	module->showAsWindow = true;
	showModuleInWindow[module->getName()] = module;
}

void CEngineImGUIManager::RemoveModuleFromShowingInWindow(IModule * module){
	module->showAsWindow = false;
}

void CEngineImGUIManager::renderWindows(){
	// Render windows.
	for (auto module : showModuleInWindow) {
		if (ImGui::Begin(module.first.c_str(), &module.second->showAsWindow))
		{
			module.second->renderInMenu();
			ImGui::End();
		}
	}

	// Check if any one of them is closed and remove them. We will remove one
	// per frame as to avoid storing them in a list and because its nearly
	// impossible to remove most than one per frame.
	for (auto module : showModuleInWindow) {
		if (module.second->showAsWindow == false) {
			showModuleInWindow.erase(module.first);
			break;
		}
	}
}

void CEngineImGUIManager::renderOverlay() {
	const float DISTANCE = 10.0f;
	static int corner = 0;
	ImVec2 window_pos = ImVec2((corner & 1) ? ImGui::GetIO().DisplaySize.x - DISTANCE : DISTANCE, (corner & 2) ? ImGui::GetIO().DisplaySize.y - DISTANCE : DISTANCE);
	ImVec2 window_pos_pivot = ImVec2((corner & 1) ? 1.0f : 0.0f, (corner & 2) ? 1.0f : 0.0f);
	if (corner != -1)
		ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::SetNextWindowBgAlpha(0.3f); // Transparent background
	if (ImGui::Begin("Debugger Overlay", &showOverlay, (corner != -1 ? ImGuiWindowFlags_NoMove : 0) | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav))
	{
		ImGui::Text("(right-click to change position)");
		ImGui::Separator();
		ImGui::Text("%.3f ms/frame (%.1f FPS) %.2f", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate, Time.current);

		// Mouse info
		Vector2 mousePos = EngineInput.mouse().getPosition();
		ImGui::Text("Mouse Screen Cords: %.2f %.2f", mousePos.x, mousePos.y);

		// Player position and rotation
		CEntity* player = getEntityByName("Player");
		TCompTransform* c_trans;
		if (player) {
			c_trans = player->getComponent<TCompTransform>();
			if (c_trans) {
				Vector3 pos = c_trans->getPosition();
				Quaternion rot = c_trans->getRotation();
				ImGui::Text("Player Position: %.2f %.2f %.2f", pos.x, pos.y, pos.z);
				ImGui::Text("Player Rotation: %.2f %.2f %.2f %.2f", rot.x, rot.y, rot.z, rot.w);
			}
		}
		if (ImGui::BeginPopupContextWindow())
		{
			if (ImGui::MenuItem("Custom", NULL, corner == -1)) corner = -1;
			if (ImGui::MenuItem("Top-left", NULL, corner == 0)) corner = 0;
			if (ImGui::MenuItem("Top-right", NULL, corner == 1)) corner = 1;
			if (ImGui::MenuItem("Bottom-left", NULL, corner == 2)) corner = 2;
			if (ImGui::MenuItem("Bottom-right", NULL, corner == 3)) corner = 3;
			if (showOverlay && ImGui::MenuItem("Close")) showOverlay = false;
			ImGui::EndPopup();
		}
	}
	ImGui::End();
}