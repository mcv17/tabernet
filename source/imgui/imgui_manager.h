#pragma once

#include "modules/module.h"
#include <unordered_set>
#include <unordered_map>

class IModule;
class CModuleManager;
class CResourcesManager;
class CModuleEntities;
class CModuleEditor;

/*
This module manages the ImGui context used in our engine for accessing information about entities, resources, modules,
while ingame in an easy way. It provides a window that acts as main entry point displaying all the information that we found
relevant to access while working in the game. It does so by providing a more ordered and structured layout for displaying
the information.

By default it uses the following classes for providing functionality:

* ModuleManager for displaying a list with all the modules that exist in our game and being able to access the renderInMenu methods
that display the information of the module. This also includes the two modules below ModuleEntities and ModuleEditor.

* ResourcesManager for displaying all resouces loaded into the engine (textures, meshes, jsons, rendered textures, ...).

* ModuleEntities for displaying all entities and components that are currently allocated in the engine.
It's pointer allows us to call it's rendering function from within this manager and show the information in a more comfortable way.

* ModuleEditor which provides an editor also implemented with ImGUI and is responsible of loading all the necessary
information for it's current working. 

Further pointers to other modules may be added in the future if we want to add them to this main window and directly
call their ImGui functions from within this code instead of having to acess them through the modules tab.
*/

class CEngineImGUIManager {
	// Sections in our ImGUI context. Used for deciding which section must be rendered now.
	enum class SEngineImGUISections {
		ENGINE_SECTION = 0,
		EDITOR_SECTION,
		SCENE_SECTION,
		IMGUI_WINDOW_SECTION
	};

	std::map<std::string, ImFont*> _loadedFonts;

	/* IMGUI control variables. */
	bool showWindow = false;
	bool showImGuiDemo = false;
	bool showOverlay = false;
	SEngineImGUISections currentSection = SEngineImGUISections::ENGINE_SECTION;
	bool show_style_editor = false;
	bool loadEditor = false;

	/* References to rendered modules in Imgui. */
	
	CModuleManager * moduleManager;	// Provides access to all modules registered in the engine.
	std::unordered_set <std::string> pinnedModules; // Modules that are marked as important, shown in a special section for easy access.
	std::unordered_map <std::string, IModule *> showModuleInWindow; // Modules that are being shown as separate windows, for having multiple windows at the same time.
	
	CResourcesManager * resourcesManager; // Used for showing rendered resources.

	/* Pointer to modules whose render in menu is called from within the code.
	Allows for fast access of their method and for displaying their information
	inside the layout of our ImGui main window. */
	CModuleEntities * moduleEntities;
	CModuleEditor * moduleEditor;

	// Rendering functions for the window..
	void renderMenuBar(); // Renders the menu bar that displays the sections of the manager. These are Engine, Scene, Editor and Window.
	void renderCurrentContent(); // Renders the content of the currently selected sections.
	void renderEngineSection(); // Renders the content of the engine section. Entities, components, modules, resources, ...
	void renderSceneSection(); // Renders the content for the scene section. Current gamestate, modules active by type and entities.
	void renderEditorSection(); // Rendes the in game editor for placing assets and editing particles in real time.
	void renderWindows(); // Renders all the opened windows.
	void renderOverlay();

	// Display modules as windows.
	void ShowModuleInWindow(IModule * module); // Makes a module display as a window.
	void RemoveModuleFromShowingInWindow(IModule * module); // Removes a module from displaying as a window.

	// Constructor and destructor for the ImGui manager.
	CEngineImGUIManager();
	~CEngineImGUIManager();
public:
	// Singleton instance.
	static CEngineImGUIManager & instance();
	CEngineImGUIManager(CEngineImGUIManager const&) = delete;
	void operator=(CEngineImGUIManager const&) = delete;

	// Initiates the ImGui window by loading all config data for the window from jsons and setting up the Imgui context.
	bool init(CModuleManager * nModuleManager, CResourcesManager * nResourcesManager);

	// Stops the module closing down the ImGui context opened.
	void stop();

	// Reads input from keyboard. Allows for opening or closing the window through keyboard.
	void update(float dt);

	// Starts the ImGui frame, called at the beggining of the rendering phase.
	void beginImguiFrame();

	// Main entry point, call it to start rendering the window and all the data.
	void renderImguiEngineWindow();

	// Closes the ImGui frame, call it at the end of the rendering. Before the swap chain is called.
	// Copies the rendered ImGui content to the back buffer.
	void endImguiFrame();

	// This method can be called by modules that want to be windowable. Will print a functional button that can be used for showing the module as a window.
	void showModuleInWindowButton(IModule * module);

	// Checks if module is being shown as window.
	bool isBeingShownAsWindow(const std::string & nameModule);
};
