//****************************************************************************//
// animation_action.h                                                         //
// Copyright (C) 2001, 2002 Bruno 'Beosil' Heidelberger                       //
//****************************************************************************//
// This library is free software; you can redistribute it and/or modify it    //
// under the terms of the GNU Lesser General Public License as published by   //
// the Free Software Foundation; either version 2.1 of the License, or (at    //
// your option) any later version.                                            //
//****************************************************************************//

#ifndef CAL_ANIMATION_ACTION_H
#define CAL_ANIMATION_ACTION_H


#include "cal3d/global.h"
#include "cal3d/animation.h"


class CalCoreAnimation;


class CAL3D_API CalAnimationAction : public CalAnimation
{
public:
  CalAnimationAction(int nAnimationID, CalCoreAnimation* pCoreAnimation);
  virtual ~CalAnimationAction() { }

  bool execute(float delayIn, float delayOut, float weightTarget = 1.0f, bool autoLock=false, float startTime = 0.0f);
  bool update(float deltaTime);

  // Added by MCV
  bool remove(float timeout);

	// For fast retrieval to compute the animation duration. Added by ourselves for our engine adaptation.
	int getAnimationID() { return animation_id; }

private:
	int animation_id;
  float m_delayIn;
  float m_delayOut;
  float m_delayTarget;
  float m_weightTarget;
  bool  m_autoLock;

  // Added by MCV
  float m_total_duration;
  float m_startTime;
};

#endif
