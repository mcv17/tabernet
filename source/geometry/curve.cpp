#include "mcv_platform.h"
#include "curve.h"

class CCurveResourceType : public CResourceType {
public:
	const char* getExtension( int idx ) const override { return "curve"; }
	
	const char* getName() const override
	{
		return "Curves";
	}
	
	IResource * create(const std::string& name) const override
	{
		CCurve * new_curve = new CCurve();
		
		auto jData = Utils::loadJson(name);
		for (auto & jKnot : jData)
		{
			VEC3 pos = loadVector3(jKnot);
			new_curve->addKnot(pos);
		}
		
		new_curve->setNameAndType(name, this);
		return new_curve;
	}
};

template<>
const CResourceType * getResourceTypeFor<CCurve>() {
	static CCurveResourceType resource_type;
	return &resource_type;
}

// Adds a new know to the given curve.
void CCurve::addKnot(const VEC3 & pos)
{
	_knots.push_back(pos);
}

VEC3 CCurve::evaluate(float ratio) const{
	int CurrentSection;
	return evaluate(ratio, CurrentSection);
}

// Returns the position in the curve given a ratio that goes from 0 to 1.
VEC3 CCurve::evaluate(float ratio, int & CurrentSection) const
{
	ratio = Maths::clamp(ratio, 0.f, 1.f);
	int numSections = (int)_knots.size() - 3; // -start point, -end point
	int section = ratio >= 1.f ? numSections - 1 : static_cast<int>(ratio * numSections);
	CurrentSection = section;
	float ratioPerSection = 1.f / numSections;
	float sectionRatio = fmodf(ratio, ratioPerSection) / ratioPerSection;

	const int idx = section + 1;

	const VEC3 p1 = _knots[idx - 1];
	const VEC3 p2 = _knots[idx];
	const VEC3 p3 = _knots[idx + 1];
	const VEC3 p4 = _knots[idx + 2];

	return VEC3::CatmullRom(p1, p2, p3, p4, sectionRatio);
}

// Render debug the curve.
void CCurve::renderDebug(const CTransform & world) const
{
	const int nSamples = 100;
	const VEC4 color(1.f, 1.f, 0.f, 1.f);
	const MAT44 mWorld = world.asMatrix();
	VEC3 prevPos = VEC3::Transform(evaluate(0.f), mWorld);

	for (int i = 1; i < nSamples; ++i)
	{
		const float ratio = static_cast<float>(i) / static_cast<float>(nSamples);
		const VEC3 pos = VEC3::Transform(evaluate(ratio), mWorld);
		drawLine(prevPos, pos, color);
		prevPos = pos;
	}
}

void CCurve::drawCurve(VEC4 color)
{
	if(_knots.size() > 1)
		for (int i = 1; i < _knots.size(); i++)
			drawLine(_knots[i - 1], _knots[i], color);
	
}

void CCurve::editKnot(VEC3 value, int pos)
{
	if (pos < _knots.size())
		_knots[pos] = value;
}

void CCurve::setAllKnots(std::vector<VEC3> knots)
{
	_knots.clear();
	for (auto& knot : knots) _knots.push_back(knot);
}

template<>
CTransform interpolateCurveKnot<CTransform>(const CTransform * p, float ratio) {
 
	VEC3 newPos = VEC3::CatmullRom(
		p[0].getPosition(),
		p[1].getPosition(),
		p[2].getPosition(),
		p[3].getPosition(),
		ratio);

	QUAT newQuat = QUAT::Slerp(p[1].getRotation(), p[2].getRotation(), ratio);

	return CTransform(newPos, newQuat, 1.0f);
}

template<>
void drawSegmentBetweenCurveKnots< CTransform >(const CTransform & n1, const CTransform & n2, const MAT44 world, VEC4 color) {
	VEC3 p1 = VEC3::Transform(n1.getPosition(), world);
	VEC3 p2 = VEC3::Transform(n2.getPosition(), world);
	drawLine(p1, p2, color);
}

template<>
void drawCurveKnot< CTransform >(const CTransform& n, const MAT44 world, VEC4 color) {
	MAT44 final_world = n.asMatrix() * world;
	drawAxis(final_world);
}

class CTransCurveResourceType : public CResourceType {
public:
	
	const char * getExtension(int idx) const override { return "trans_curve"; }
	
	const char* getName() const override
	{
		return "TransCurve";
	}
	
	IResource * create(const std::string & name) const override
	{
		CTransformCurve * new_curve = new CTransformCurve();
		new_curve->setNameAndType(name, this);
		new_curve->onFileChanged(name);
		return new_curve;
	}
};

template<>
const CResourceType* getResourceTypeFor<CTransformCurve>() {
	static CTransCurveResourceType resource_type;
	return &resource_type;
}


void CTransformCurve::load(const json & jdata) {
	knots = jdata.get< std::vector< CTransform > >();
}

void CTransformCurve::onFileChanged(const std::string & filename) {
	if (filename == getName()) {
		json j = Utils::loadJson(filename);
		load(j);
	}
}

void CTransformCurve::renderInMenu() {
	ImGui::Checkbox("Show Knots In Render Debug", &show_knots);
	ImGui::DragInt("# Samples", &debug_samples, 0.2f, 2, 512);
	if (ImGui::TreeNode("Knots")) {
		int idx = 0;
		for (auto & n : knots) {
			char title[64];
			snprintf(title, 64, "Knot %d/%d", idx+1, (int)knots.size());
			if (ImGui::TreeNode(title)) {
				std::string text = "Show " + std::string(title);
				if (ImGui::TreeNode(text.c_str())) {
					n.renderInMenu();
					ImGui::TreePop();
				}

				if (ImGui::Button("Remove knot"))
					removeKnot(idx);

				ImGui::TreePop();
			}
			++idx;
		}
		ImGui::TreePop();
	}
}


void CTransformCurve::renderInMenuEditor(CTransform & knotToPlace, int & currentIDEditing) {
	ImGui::Checkbox("Show Knots In Render Debug", &show_knots);
	ImGui::DragInt("# Samples", &debug_samples, 0.2f, 2, 512);
	if (ImGui::TreeNode("Knots")) {
		int idx = 0;
		for (auto & n : knots) {
			char title[64];
			snprintf(title, 64, "Knot %d/%d", idx + 1, (int)knots.size());
			if (ImGui::TreeNode(title)) {
				bool editing = idx == currentIDEditing;
				ImGui::Checkbox("Is knot being edited", &editing);
				
				ImGui::Spacing(0, 5);
				if (n.renderInMenu()) {
					currentIDEditing = idx;
					knotToPlace = n;
				}

				ImGui::Spacing(0, 5);
				if (ImGui::Button("Remove knot"))
					removeKnot(idx);

				ImGui::TreePop();
			}
			++idx;
		}
		ImGui::TreePop();
	}
}

void CTransformCurve::renderDebug(const MAT44 & world, const VEC4 & color) const {
	if (show_knots) {
		CGpuScope gpu_knots(getName().c_str());
		for (auto& n : knots)
			drawCurveKnot(n, world, color);
	}
	
	if (knots.size() < 4) return;

	CGpuScope gpu_scope(getName().c_str());

	CTransform prevNode = evaluate(0.f);
	for (int i = 1; i < debug_samples; ++i) {
		const float ratio = static_cast<float>(i) / static_cast<float>(debug_samples - 1.f);
		CTransform newNode = evaluate(ratio);
		drawSegmentBetweenCurveKnots(prevNode, newNode, world, color);
		prevNode = newNode;
	}
}

CTransform CTransformCurve::evaluate(float ratio) const {
	int CurrentSection;
	return evaluate(ratio, CurrentSection);
}

CTransform CTransformCurve::evaluate(float ratio, int & CurrentSection) const {
	if (knots.size() < 4) return CTransform();

	ratio = Maths::clamp(ratio, 0.f, 1.f);
	if (ratio >= 1.0f)
		return interpolateCurveKnot(knots.data() + knots.size() - 4, 1.0f);

	int numSections = (int)knots.size() - 3; // -start point, -end point
	int section = static_cast<int>(ratio * numSections);
	CurrentSection = section;
	float ratioPerSection = 1.f / numSections;
	float sectionRatio = fmodf(ratio, ratioPerSection) / ratioPerSection;
	const int idx = section + 1;
	assert(idx > 0);
	assert(idx + 2 < knots.size());
	return interpolateCurveKnot(knots.data() + idx - 1, sectionRatio);
}

void CTransformCurve::addKnot(const CTransform & transformToAdd) {
	knots.push_back(transformToAdd);
}

void CTransformCurve::addKnot(int id, const CTransform & transformToAdd){
	if (id >= knots.size())
		knots.resize(id+1);

	knots[id] = transformToAdd;
}

void CTransformCurve::removeKnot(int id) {
	if (id >= knots.size()) return;

	knots.erase(knots.begin() + id);
}