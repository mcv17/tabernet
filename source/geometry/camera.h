#pragma once

#include "geometry.h"
#include "angular.h"

#define NUM_CASCADES_SHADOW_MAP 3
#define NUM_FRUSTUM_CORNERS NUM_CASCADES_SHADOW_MAP * 8

class CCamera {
protected:
	MAT44 view;
	MAT44 projection;
	MAT44 view_projection;

	VEC3  eye;
	VEC3  target;
	VEC3  up_aux;

	VEC3  front;
	VEC3  left;
	VEC3  up;

  float z_near = 0.1f;
  float z_far = 1000.0f;
  float fov_radians = DEG2RAD( 60.0f );
  float aspect_ratio = 1.0f;
  bool  is_ortho = false;

  // Ortho params
  float ortho_left = 0.f;
  float ortho_top = 0.f;
  float ortho_width = 1.0f;
  float ortho_height = 1.0f;
  bool  ortho_centered = false;

  void updateViewProjection();
  void updateProjection();

  // viewport
  struct TViewport {
    int x0 = 0;
    int y0 = 0;
    int width = 640;
    int height = 480;
  };
  TViewport viewport;

	/* Cascade Static variables. */
	static float cascade_start_percentage; // Where the cascades start based on the camera frustum.
	static float cascade_max_percentage; // Where the cascades stop based on the camera frustum.
	static float perc_c1, perc_c2, perc_c3; // Percentage where each cascade starts and ends.
	
	// Stores the end of each frusta in percentage.
	static float cascade_ends[NUM_CASCADES_SHADOW_MAP];
	
	/* Camera dependant variables.*/

	// Fov used by the cascade algorithm to calculate the bounding spheres of the frusta.
	// We use this values instead of the normal fov to allow changes in the fov of the camera
	// without the shadow shimmering. This variable should be set to the bigger possible fov
	// by the controller of the camera so shadows don't shimmer or show artifacts.
	float cascade_fov;
	
	// Stores the end of each frust in z coords based on camera space.
	float cascade_ends_clip_space[NUM_CASCADES_SHADOW_MAP];
	
	// The 8 corners of the csm complete frustum.
	Vector3 cascade_complete_frustum_corners[8];
	
	// Frustas corners. Used for CSM. All cameras will have as many frusta
	// as levels of CSM are defined to allow for correct CSM rendering.
	Vector3 cascade_frustas_corners[NUM_FRUSTUM_CORNERS];
	
	// The radius of the bounding sphere of each frusta.
	float sphere_radius_frustas[NUM_CASCADES_SHADOW_MAP];

	// Control variables for knowing when to calculate the data.
	bool updated_complete_frustum_corners = false;
	bool updated_frustas_corners = false;
	bool updated_sphere_radius = false;


public:
	CCamera();
	CCamera(const VEC3 & new_eye, const VEC3 & new_target,
	const VEC3 & new_up_aux, float nFov_radians, float nAspect_ratio,
	float nZ_near, float nZ_far);
	
	void lookAt(VEC3 new_eye, VEC3 new_target, VEC3 new_up_aux = VEC3(0, 1, 0));
	void setProjectionParams(float new_fov_radians, float new_z_near, float new_z_far);
	void setOrthoParams(bool centered, float left, float width, float top, float height, float new_z_near, float new_z_far);
	void setUIProjection(bool is_centered, float new_left, float new_width, float new_top, float new_height, float new_z_near, float new_z_far);
	void setAspectRatio(float new_aspect_ratio);
	void setViewport(int x0, int y0, int width, int height);
	void activateViewport();
	bool getScreenCoordsOfWorldCoord(VEC3 world_pos, VEC3 *screen_coords) const;
	

	/* CSM */
	// Returns the 8 corners of the complete frustum used in CSM.
	const Vector3 * calculateCSMFrustumCorners();
	// Returns the corners (8 * NumOfCascades) for each of the frustas of the CSM. Used for debugging.
	const Vector3 * calculateCSMFrustasCorners();

	const float * calculateCSMFrustaBSRadius();

	// Returns the radius and center of each frusta bounding sphere.
	const float * calculateCSMBSRadiusAndCenter(Vector3 * center);

	// Returns all the corners of the camera cascade frustum in world space.
	const Vector3 * getWSCascadeFrustasCorners() const { return cascade_frustas_corners; }
	// Returns the clip space distance for each cascade. Can be used for picking cascade or fading the last one.
	const float * getClipSpaceCascadeEnds() const { return cascade_ends_clip_space; }

	/* Getters and setters. */
	
	Matrix getView() const { return view; }
	Matrix getProjection() const { return projection; }
	Matrix getViewProjection() const { return view_projection; }
	
	VEC3 getPosition() const { return eye; }
	VEC3 getFront() const { return front; }
	VEC3 getLeft() const { return left; }
	VEC3 getUp() const { return up_aux; }
	VEC3 getEye() const { return eye; }
	VEC3 getTarget() const { return target; }
  
	float getNear() const { return z_near; }
	float getFar() const { return z_far; }
	float getFov() const { return fov_radians; }
	float getAspectRatio() const { return aspect_ratio; }
	float getCascadeFov() const { return cascade_fov; }

	/* Setters for matrices. */

	// Sets a view matrix. This view matrix doesn't update the rest of variables.
	// Use lookat if u want to set the camera view matrix. This is used only
	// for shadow generation where most extra parameters are not necessary.
	void setViewMatrix(const Matrix & nView) { view = nView; }

	// Sets a projection matrix. This projection matrix doesn't update the rest of variables.
	// This is used only for shadow generation where most extra parameters are not necessary.
	void setProjectionMatrix(const Matrix & nProj) { projection = nProj; }

	// Sets a view projection matrix. This view projection matrix doesn't update the rest of variables.
	// This is used only for shadow generation where most extra parameters are not necessary.
	void setViewProjectionMatrix(const Matrix & nViewProj) { view_projection = nViewProj; }

	/* CSM methods. */
	
	// Sets the fov that will be used to calculate the frustum for the CSM.
	void setCascadeFov(float nCascadeFov) { cascade_fov = nCascadeFov; }

protected:
	void setCascadeFrustaZ();
	// Calculates the corners for a given frustum.
	void calculateFrustumCorners(Vector3 * results, float z_near, float z_far);
	// Sets the CSM variables dirty, must be recalculated next frame.
	void setCSMVariablesDirty();

	void calculateBSRadius();
	void calculateBSCenter(Vector3 * bsCenters);
};
