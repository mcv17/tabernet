#pragma once

class CTransform {
	VEC3  position = VEC3::Zero;
	QUAT rotation = QUAT::Identity;
	VEC3 scale = VEC3::One;

public:

	CTransform() = default;
	CTransform(VEC3 new_pos, QUAT new_rot, float new_scale = 1.0f) : rotation(new_rot), position(new_pos), scale(new_scale){}

	VEC3 getPosition() const { return position; }
	QUAT getRotation() const { return rotation; }
	void setPosition(const VEC3 & new_position) { position = new_position; }
	void setRotation(const QUAT & new_rotation) { rotation = new_rotation; }
	
	Matrix asMatrix() const;
	void fromMatrix(Matrix mtx);

	VEC3 getFront() const;
	VEC3 getUp() const;
	VEC3 getLeft() const;
	
	void getAngles(float* yaw, float* pitch , float* roll = nullptr) const;
	Vector3 getYawPitchRoll();
	void setAngles(float yaw, float pitch, float roll = 0.0f);
	void setScale(float nScale);
	void setScale(VEC3 nScale);
	VEC3 getScale();

	bool isInFront(const VEC3 & p) const;
	bool isInLeft(const VEC3 & p) const;
	bool isTop(const VEC3 & p) const;

	void lookAt(const VEC3 & eye, const VEC3 & target, const VEC3 & up_aux = VEC3(0, 1, 0));
	void getAnglesToFaceTarget(const VEC3 & target, float & yaw, float & pitch); 	// Return the angles to face a target. No delta but absolute values.
	void rotateAroundVector(const VEC3& norm, float angle);

	CTransform combineWith(const CTransform & delta_transform) const;
	CTransform lerp(const CTransform & a, const CTransform & b, float t) const;
	
	float getDeltaYawToAimTo(const VEC3 & target) const;
	float getDeltaPitchToAimTo(const VEC3 & target) const;
	float getDeltaYawToAimToFromDir(const VEC3 & dir) const;
	float getDeltaPitchToAimToFromDir(const VEC3 & dir) const;

	bool load(const json & j);
	
	bool renderInMenu();
	bool renderInMenuForCameraCurves();
};
