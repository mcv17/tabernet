#pragma once

#include "resources/resource.h"
#include "render/render.h"

// A basic curve composed of normal positions.
class CCurve : public IResource
{
public:
	void addKnot(const VEC3 & pos);	// Adds a new know to the given curve.
	VEC3 evaluate(float ratio) const;
	VEC3 evaluate(float ratio, int & CurrentSection) const; // Returns the position in the curve given a ratio that goes from 0 to 1.
	void renderDebug(const CTransform & world = CTransform()) const; // Render debug the curve.
	void drawCurve(VEC4 color = VEC4(0,1,0,1));
	void editKnot(VEC3 value, int pos);
	void setAllKnots(std::vector<VEC3> knots);
	std::vector<VEC3> getKnots() { return _knots; }

private:
	std::vector<VEC3> _knots; // Knots of the curve.
};

// These are the two functions we need to implement to use a new type of curve
template< typename TKnot >
TKnot interpolateCurveKnot(const TKnot * p1, float ratio);


template< typename TKnot >
void drawSegmentBetweenCurveKnots(const TKnot& n1, const TKnot& n2, const MAT44 world, VEC4 color);


template< typename TKnot >
void drawCurveKnot(const TKnot& n1, const MAT44 world, VEC4 color);

// Forward declaration that we can read a transform from json, defined elsewhere
void from_json(const json & j, CTransform & t);


// A curve composed of transforms. Use it if you want to store both transform and rotation.
class CTransformCurve : public IResource
{
	std::vector<CTransform> knots;
	bool               show_knots = false;
	int                debug_samples = 100;

public:

	void load(const json & jdata);
	void renderInMenu() override;
	void renderInMenuEditor(CTransform & knotToPlace, int & currentIDEditing);
	void renderDebug(const MAT44 & world = MAT44::Identity, const VEC4 & color = VEC4(1, 1, 1, 1)) const;

	CTransform evaluate(float ratio) const;
	CTransform evaluate(float ratio, int & CurrentSection) const;

	void addKnot(const CTransform & transformToAdd);
	void addKnot(int id, const CTransform & transformToAdd);
	void removeKnot(int id);
	
	void onFileChanged(const std::string & filename) override;

	const std::vector<CTransform> & getKnots() const { return knots; }
};