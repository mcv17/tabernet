#pragma once

/* Header for math libraries and extra math functions and constants. */

#include "SimpleMath.h"
using namespace DirectX::SimpleMath;
typedef Vector2 VEC2;
typedef Vector3 VEC3;
typedef Vector4 VEC4;
typedef Matrix MAT44;
typedef Quaternion QUAT;
typedef DirectX::BoundingBox AABB;

#include <algorithm>
#define DEG2RAD(degrees) (degrees) * (float)M_PI/180.0f
#define RAD2DEG(radians) (radians) * 180.0f/(float)M_PI

#include "camera.h"
#include "angular.h"
#include "transform.h"
#include "interpolators.h"

extern Vector2 loadVector2(const std::string& str);
extern Vector2 loadVector2(const json & j, const char* attr);
extern Vector2 loadVector2(const json & j, const char* attr, const Vector2& defaultValue);
extern Vector3 loadVector3(const std::string& str);
extern Vector3 loadVector3(const json & j, const char* attr);
extern Vector3 loadVector3(const json& j, const char* attr, const Vector3& defaultValue);
extern Quaternion loadQuaternion(const json & j, const char* attr);
extern Vector4 loadVector4(const json& j);
extern Vector4 loadVector4(const json & j, const char* attr);
extern Vector4 loadVector4(const json& j, const char* attr, const VEC4& defaultValue);
extern std::string saveVector2(const Vector2 vec);
extern std::string saveVector3(const Vector3 vec);
extern std::string saveVector4(const Vector4 vec);

namespace Maths {
	template < typename T > T clamp(T value, T lower, T upper);
	template < typename T > T lerp(T a, T b, float f);
	float lerpAngle(float a, float b, float t);
	template < typename T > T clampMagnitudeVector(const T & vector, float maxLength);
	
	float SmoothDamp(float current, float target, float & currentVelocity,
		float smoothTime, float deltaTime, float maxSpeed = FLT_MAX);
	template < typename T > T SmoothDamp(T current, T target, T & currentVelocity,
		float smoothTime, float deltaTime, float maxSpeed = FLT_MAX);
	float SmoothDampAngle(float current, float target, float & currentVelocity,
		float smoothTime, float deltaTime, float maxSpeed = FLT_MAX);
}
