#include "mcv_platform.h"
#include "transform.h"
#include "angular.h"

Matrix CTransform::asMatrix() const {
  return 
	  Matrix::CreateScale(scale)
    * Matrix::CreateFromQuaternion(rotation)
    * Matrix::CreateTranslation(position);
}

VEC3 CTransform::getFront() const {
	VEC3 front = -Matrix::CreateFromQuaternion(rotation).Forward();
	front.Normalize();
	return front;
}

VEC3 CTransform::getUp() const {
	VEC3 up = Matrix::CreateFromQuaternion(rotation).Up();
	up.Normalize();
	return up;
}

VEC3 CTransform::getLeft() const {
	VEC3 left = -Matrix::CreateFromQuaternion(rotation).Left();
	left.Normalize();
	return left;
}

void CTransform::fromMatrix(Matrix mtx) {
	VEC3 scale3;
	bool is_valid = mtx.Decompose(scale3, rotation, position);
	scale = scale3;
}

void CTransform::lookAt(const VEC3 & eye, const VEC3 & target, const VEC3 & up_aux) {
	position = eye;
	VEC3 front = target - eye;
	float yaw, pitch;
	vectorToYawPitch(front, yaw, pitch);
	setAngles(yaw, pitch, 0.f);
}

void CTransform::getAnglesToFaceTarget(const VEC3 & target, float & yaw, float & pitch) {
	VEC3 front = target - position;
	vectorToYawPitch(front, yaw, pitch);
}

void CTransform::rotateAroundVector(const VEC3 & norm, float angle) {
	rotation *= DirectX::XMQuaternionRotationNormal(norm, angle);
}

CTransform CTransform::combineWith(const CTransform & delta_transform) const {
	CTransform new_t;
	new_t.rotation = delta_transform.rotation * rotation;

	VEC3 delta_pos_rotated = VEC3::Transform(delta_transform.position, rotation);
	new_t.position = position + (delta_pos_rotated * scale);

	new_t.scale = scale * delta_transform.scale;
	return new_t;
}

CTransform CTransform::lerp(const CTransform & a, const CTransform & b, float t) const {
	CTransform transform;
	transform.position = VEC3::Lerp(a.position, b.position, t);
	transform.rotation = QUAT::Lerp(a.rotation, b.rotation, t);
	transform.scale = Maths::lerp<VEC3>(a.scale, b.scale, t);
	return transform;
}

void CTransform::getAngles(float* yaw, float* pitch , float* roll ) const{
	VEC3 front = getFront();
	vectorToYawPitch(front, *yaw, *pitch);

	// If requested...
	if (roll) {
		VEC3 roll_zero_left = VEC3(0, 1, 0).Cross(getFront());
		VEC3 roll_zero_up = VEC3(getFront()).Cross(roll_zero_left);
		VEC3 my_real_left = getLeft();
		float rolled_left_on_up = my_real_left.Dot(roll_zero_up);
		float rolled_left_on_left = my_real_left.Dot(roll_zero_left);
		*roll = atan2f(rolled_left_on_up, rolled_left_on_left);
	}
}

Vector3 CTransform::getYawPitchRoll() {
	float yaw, pitch, roll;
	getAngles(&yaw, &pitch, &roll);
	return Vector3(yaw, pitch, roll);
}

void CTransform::setAngles(float yaw, float pitch, float roll) {
	rotation = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);
}

void CTransform::setScale(float nScale) {
	scale = VEC3(nScale, nScale, nScale);
}

void CTransform::setScale(VEC3 nScale) {
	scale = nScale;
}

VEC3 CTransform::getScale() {
	return scale;
}

bool CTransform::isInFront(const VEC3 & p) const {
	VEC3 delta = p - position;
	return delta.Dot(getFront()) > 0.f;
}

bool CTransform::isInLeft(const VEC3 & p) const {
	VEC3 delta = p - position;
	return delta.Dot(getLeft()) > 0.f;
}

bool CTransform::isTop(const VEC3 & p) const {
	VEC3 delta = p - position;
	return delta.Dot(getUp()) > 0.f;
}

bool CTransform::renderInMenu() {
	VEC3 front = getFront();
	VEC3 up = getUp();
	VEC3 left = getLeft();
	
	bool changed_pos = ImGui::DragFloat3("Pos", &position.x, 0.01f, -10000.0f, 10000.0f);
	bool changed_rot = ImGui::DragFloat4("Quat", &rotation.x, 0.01f, -3.14f, 3.14f);
	
	bool changed = false;
	float yaw, pitch, roll = 0.f;
	getAngles(&yaw, &pitch, &roll);
	float yaw_deg = RAD2DEG(yaw);
	if (ImGui::DragFloat("Yaw", &yaw_deg, 0.1f, -180.0f, 180.0f)) {
		changed = true;
		yaw = DEG2RAD(yaw_deg);
	}
	
	float pitch_deg = RAD2DEG(pitch);
	float max_pitch = 90.0f - 1e-3f;
	if (ImGui::DragFloat("Pitch", &pitch_deg, 0.1f, -max_pitch, max_pitch)) {
		changed = true;
		pitch = DEG2RAD(pitch_deg);
	}

	float roll_deg = RAD2DEG(roll);
	if (ImGui::DragFloat("Roll", &roll_deg, 0.1f, -180.0f, 180.0f)) {
		changed = true;
		roll = DEG2RAD(roll_deg);
	}

	if (changed) 
		setAngles(yaw, pitch, roll );
	
	bool changed_scaled = ImGui::DragFloat3("Scale", &scale.x, 0.01f, 0.0f, 10.f);
	ImGui::LabelText("Front", "%f %f %f", front.x, front.y, front.z);
	ImGui::LabelText("Up", "%f %f %f", up.x, up.y, up.z);
	ImGui::LabelText("Left", "%f %f %f", left.x, left.y, left.z);
	return changed | changed_pos | changed_scaled | changed_rot;
}

bool CTransform::renderInMenuForCameraCurves() {

	bool changed_pos = ImGui::DragFloat3("Pos", &position.x, 0.01f, -10000.0f, 10000.0f);

	bool changed = false;
	float yaw, pitch, roll = 0.f;
	getAngles(&yaw, &pitch, &roll);
	float yaw_deg = RAD2DEG(yaw);
	if (ImGui::DragFloat("Yaw", &yaw_deg, 0.1f, -180.0f, 180.0f)) {
		changed = true;
		yaw = DEG2RAD(yaw_deg);
	}

	float pitch_deg = RAD2DEG(pitch);
	float max_pitch = 90.0f - 1e-3f;
	if (ImGui::DragFloat("Pitch", &pitch_deg, 0.1f, -max_pitch, max_pitch)) {
		changed = true;
		pitch = DEG2RAD(pitch_deg);
	}

	float roll_deg = RAD2DEG(roll);
	if (ImGui::DragFloat("Roll", &roll_deg, 0.1f, -180.0f, 180.0f)) {
		changed = true;
		roll = DEG2RAD(roll_deg);
	}

	if (changed)
		setAngles(yaw, pitch, roll);

	bool changed_scaled = ImGui::DragFloat("Speed factor", &scale.x, 0.01f, 0.0f, 10.f);
	return changed | changed_pos | changed_scaled;
}

float CTransform::getDeltaYawToAimTo(const VEC3 & target) const {
	VEC3 dir_to_target = target - position;
	float dot_left = getLeft().Dot(dir_to_target);
	float dot_front = getFront().Dot( dir_to_target );
	return atan2f(dot_left, dot_front);
}

float CTransform::getDeltaPitchToAimTo(const VEC3 & target) const {
	VEC3 dir_to_target = target - position;
	float dot_left = getLeft().Dot(dir_to_target);
	float dot_up = getUp().Dot(dir_to_target);
	return -atan2f(dot_up, dot_left);
}

float CTransform::getDeltaYawToAimToFromDir(const VEC3 & dir) const{
	float dot_left = getLeft().Dot(dir);
	float dot_front = getFront().Dot(dir);
	return atan2f(dot_left, dot_front);
}

float CTransform::getDeltaPitchToAimToFromDir(const VEC3 & dir) const{
	float dot_left = getLeft().Dot(dir);
	float dot_up = getUp().Dot(dir);
	return -atan2f(dot_up, dot_left);
}

bool CTransform::load(const json& j) {

	if (j.count("pos"))
		position = loadVector3(j, "pos");

	if (j.count("rotation"))
		setRotation(loadQuaternion(j, "rotation"));

	if (j.count("lookat"))
		lookAt(getPosition(), loadVector3(j, "lookat"));

	if (j.count("axis")) {
		VEC3 axis = loadVector3(j, "axis");
		float angle_deg = j.value("angle", 0.f);
		float angle_rad = DEG2RAD(angle_deg);
		setRotation(QUAT::CreateFromAxisAngle(axis, angle_rad));
	}

	if (j.count("angles")) {
		Vector3 angles = loadVector3(j, "angles");
		float yaw = DEG2RAD(angles.y);
		float pitch = DEG2RAD(angles.x);
		float roll = DEG2RAD(angles.z);
		setAngles(yaw, pitch, roll);
	}

	if (j.count("scale")) {
		auto & scaleVal = j["scale"];
		if (scaleVal.is_number())
			scale = Vector3(scaleVal, scaleVal, scaleVal);
		else
			scale = loadVector3(j, "scale", Vector3::One);
	}
	return true;
}
