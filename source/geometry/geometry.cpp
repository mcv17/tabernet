#include "mcv_platform.h"
#include "geometry.h"

VEC2 loadVector2(const std::string & str) {
	VEC2 v;
	sscanf(str.c_str(), "%f %f", &v.x, &v.y);
	return v;
}

VEC2 loadVector2(const json& j, const char* attr) {
	auto k = j.value(attr, "0 0");
	VEC2 v;
	sscanf(k.c_str(), "%f %f", &v.x, &v.y);
	return v;
}

VEC2 loadVector2(const json& j, const char* attr, const Vector2& defaultValue) {
	VEC2 v = defaultValue;
	if (j.count(attr) == 0)
	{
		return v;
	}
	auto k = j.value(attr, "0 0");
	sscanf(k.c_str(), "%f %f", &v.x, &v.y);
	return v;
}

VEC3 loadVector3(const std::string & str) {
  VEC3 v;
  sscanf(str.c_str(), "%f %f %f", &v.x, &v.y, &v.z);
  return v;
}

VEC3 loadVector3(const json& j, const char* attr) {
  auto k = j.value(attr, "0 0 0");
  VEC3 v;
  sscanf(k.c_str(), "%f %f %f", &v.x, &v.y, &v.z);
  return v;
}

VEC3 loadVector3(const json& j, const char* attr, const VEC3& defaultValue) {
  if (j.count(attr) <= 0)
  {
    return defaultValue;
  }
  return loadVector3(j, attr);
}

QUAT loadQuaternion(const json& j, const char* attr) {
	auto k = j.value(attr, "0 0 0 1");
	QUAT v;
	sscanf(k.c_str(), "%f %f %f %f", &v.x, &v.y, &v.z, &v.w);
	return v;
}

Vector4 loadVector4(const json& j) {
  auto k = j.get<std::string>();
  VEC4 v;
  sscanf(k.c_str(), "%f %f %f %f", &v.x, &v.y, &v.z, &v.w);
  return v;
}

Vector4 loadVector4(const json& j, const char* attr) {
	auto k = j.value(attr, "1 1 1 1");
	Vector4 v;
	sscanf(k.c_str(), "%f %f %f %f", &v.x, &v.y, &v.z, &v.w);
	return v;
}

Vector4 loadVector4(const json& j, const char* attr, const Vector4& defaultValue) {
  if (j.count(attr) <= 0)
  {
    return defaultValue;
  }
  return loadVector4(j, attr);
}

std::string saveVector2(const Vector2 vec) {
	return std::to_string(vec.x) + " " + std::to_string(vec.y);
}

std::string saveVector3(const Vector3 vec) {
	return std::to_string(vec.x) + " " + std::to_string(vec.y) + " " + std::to_string(vec.z);
}

std::string saveVector4(const Vector4 vec) {
	return std::to_string(vec.x) + " " + std::to_string(vec.y) + " " + std::to_string(vec.z) + " " + std::to_string(vec.w);
}

template < typename T > T Maths::clamp(T value, T lower, T upper) {
	return std::max(lower, std::min(value, upper));
}

template < typename T > T Maths::lerp(T a, T b, float f) {
	return a + f * (b - a);
}

float Maths::lerpAngle(float a, float b, float t){
	t = Maths::clamp(t, 0.0f, 1.0f);
	float delta = b - a;
	if(delta > M_PI)
		a += 2.0f * (float)M_PI;
	else if (delta < -M_PI)
		a -= 2.0f * (float)M_PI;

	return a + t * (b - a);
}

template < typename T > T Maths::clampMagnitudeVector(const T & vector, float maxLength)
{
	if (vector.LengthSquared() > maxLength * maxLength) {
		T normVec;
		vector.Normalize(normVec);
		return normVec * maxLength;
	}
	return vector;
}

// Unity's Smooth Damp function, great for spring-like amortiguation.
float Maths::SmoothDamp(float current, float target, float & currentVelocity,
	float smoothTime, float deltaTime, float maxSpeed)
{
	// Based on Game Programming Gems 4 Chapter 1.10
	smoothTime = std::max(0.0001f, smoothTime);
	float omega = 2.0f/smoothTime;

	float x = omega * deltaTime;
	float exp = 1.0f/(1.0f + x + 0.48f * x * x + 0.235f * x * x * x);
	float change = current - target;
	float originalTo = target;

	// Clamp maximum speed
	float maxChange = maxSpeed * smoothTime;
	change = Maths::clamp(change, -maxChange, maxChange);
	target = current - change;

	float temp = (currentVelocity + omega * change) * deltaTime;
	currentVelocity = (currentVelocity - omega * temp) * exp;
	float output = target + (change + temp) * exp;

	// Prevent overshooting
	if (originalTo - current > 0.0f == output > originalTo)
	{
		output = originalTo;
		currentVelocity = (output - originalTo) / deltaTime;
	}
	return output;
}

// Gradually changes a vector towards a desired goal over time.
template < typename T > T Maths::SmoothDamp(T current, T target, T & currentVelocity, float smoothTime,
	float deltaTime, float maxSpeed)
{
	smoothTime = std::max(0.0001f, smoothTime);
	float omega = 2.f / smoothTime;

	float x = omega * deltaTime;
	float exp = 1.f / (1.f + x + 0.48f * x * x + 0.235f * x * x * x);
	T change = current - target;
	T originalTo = target;

	float maxChange = maxSpeed * smoothTime;
	change = clampMagnitudeVector<T>(change, maxChange);
	target = current - change;

	T temp = (currentVelocity + omega * change) * deltaTime;
	currentVelocity = (currentVelocity - omega * temp) * exp;
	T output = target + (change + temp) * exp;

	T originalMinusCurrent = originalTo - current;
	if (originalMinusCurrent.Dot(output - originalTo) > 0)
	{
		output = originalTo;
		currentVelocity = (output - originalTo) / deltaTime;
	}

	return output;
}

float Maths::SmoothDampAngle(float current, float target, float & currentVelocity,
	float smoothTime, float deltaTime, float maxSpeed) {
	float delta = target - current;
	if (delta > M_PI)
		target = current + 2.0f * (float)M_PI;
	else if (delta < -M_PI)
		target = current - 2.0f * (float)M_PI;
	return SmoothDamp(current, target, currentVelocity,
		smoothTime, deltaTime, maxSpeed = FLT_MAX);
}


template int Maths::clamp(int value, int lower, int upper);
template float Maths::clamp(float value, float lower, float upper);
template double Maths::clamp(double value, double lower, double upper);

template float Maths::lerp<float>(float, float, float);
template double Maths::lerp<double>(double, double, float);
template VEC3 Maths::lerp<VEC3>(VEC3, VEC3, float);

template VEC2 Maths::SmoothDamp(VEC2 current, VEC2 target, VEC2 & currentVelocity,
	float smoothTime, float maxSpeed, float deltaTime);
template VEC3 Maths::SmoothDamp(VEC3 current, VEC3 target, VEC3 & currentVelocity,
	float smoothTime, float maxSpeed, float deltaTime);
template VEC4 Maths::SmoothDamp(VEC4 current, VEC4 target, VEC4 & currentVelocity,
	float smoothTime, float maxSpeed, float deltaTime);

template VEC2 Maths::clampMagnitudeVector(const VEC2 & vector, float maxLength);
template VEC3 Maths::clampMagnitudeVector(const VEC3 & vector, float maxLength);
template VEC4 Maths::clampMagnitudeVector(const VEC4 & vector, float maxLength);
