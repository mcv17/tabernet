#pragma once

#include "mcv_platform.h"
#include "ui_text_animation.h"
#include "geometry/geometry.h"

namespace UI
{
	void CTextAnimation::addSpecialNode(NodeSpecial new_special) {
		specialNodes.push_back(new_special);
	}

	void CTextAnimation::displaySpecialPopup(NodeSpecial* node, int id){
		ImGui::Text("Text");
		ImGui::Text("");
		ImGui::Text("Node: %d", id);
		if (!node->special.empty()) {
			char temp[150]; 
			strcpy(temp, node->special.c_str());

			ImGui::Text(node->special.c_str());
			ImGui::InputText("##Text", temp, 150);
			node->special = temp;
		}
		ImGui::Text("Time: %.2f", node->time);
	}

	void CTextAnimation::displayMediaControls() {
		ImGui::PushID(this);
		ImGui::BeginGroup();
		if (ImGui::Button("Play")) {
			startAnimation(looping);
		}
		ImGui::SameLine();
		if (ImGui::Button("Pause")) {
			pauseAnimation();
		}
		ImGui::SameLine();
		if (ImGui::Button("Stop")) {
			stopAnimation();
		}
		ImGui::SameLine();
		ImGui::Text("Loop: ");
		ImGui::SameLine();
		ImGui::Checkbox("##RepLopp", &looping);
		ImGui::Text("Time: %f", current_time);
		ImGui::Text("MaxTime: ");
		ImGui::SameLine();
		ImGui::DragFloat("##maxfloat", &max_time, 0.01f, current_time, 20.f, "%.2f");
		ImGui::EndGroup();
		ImGui::PopID();
	}

	void CTextAnimation::displaySpecialNodeContextMenu(std::string node_content){}

	void CTextAnimation::update(float dt)
	{
		if (running) {
			current_time += dt;
			if (current_time >= max_time)
				if (looping)
					current_time = 0.0f;
				else 
					stopAnimation();
		}
	}

	void CTextAnimation::setAnimationTime(float time)
	{
		if (time <= max_time)
			current_time = time;
		else
			Utils::error("WARNING - [CImageAnimation::setAnimationTime] New time is greater than max time\n");
	}

}
