#pragma once

#include "mcv_platform.h"
#include "imgui/imgui_source/imgui_internal.h"
#include "imgui/imgui_source/imgui.h"
#include <fstream>
#include <iomanip>

namespace UI
{

	struct NodeVEC2 {
		float time = 0.f;
		VEC2 vec2 = VEC2();
	};

	struct NodeVEC4 {
		float time = 0.f;
		VEC4 vec4 = VEC4(1.0, 1.0f, 1.0f, 1.0f);
	};

	struct NodeSpecial {
		float time = 0.f;
		std::string special;
	};

	class CBaseWidgetAnimation {

	protected:

		std::string _name;
		std::vector<NodeVEC2> transformNodes;
		std::vector<NodeVEC2> scaleNodes;
		std::vector<NodeVEC4> colorNodes;
		std::vector<NodeSpecial> specialNodes;
		std::string transformInterpolator{ "linear" };
		std::string scaleInterpolator{ "linear" };
		std::string colorInterpolator{ "linear" };
		std::string specialInterpolator{ "linear" };
		// Animation duration
		float max_time = 0.0f;
		float current_time = 0.0f;
		bool running = false;
		bool looping = false;
		std::string animationFilePath = "data/ui/animations/";

	public:

		void configure(std::string name, float new_max_time, bool isLooping) {
			_name = name;
			max_time = new_max_time;
			looping = isLooping;
		}

		std::string vec2ToString(VEC2 v) {
			return std::to_string(v.x) + " " + std::to_string(v.y);
		}

		std::string vec3ToString(VEC3 v) {
			return std::to_string(v.x) + " " + std::to_string(v.y) + " " + std::to_string(v.z);
		}

		std::string vec4ToString(VEC4 v) {
			return std::to_string(v.x) + " " + std::to_string(v.y) + " " + std::to_string(v.z) + " " + std::to_string(v.w);
		}

		void addTransformNode(VEC2 new_transform, float time) {
			NodeVEC2 n;
			n.time = time;
			n.vec2 = new_transform;
			transformNodes.push_back(n);
		}

		void addScaleNode(VEC2 new_scale, float time) {
			NodeVEC2 n;
			n.time = time;
			n.vec2 = new_scale;
			scaleNodes.push_back(n);
		}

		void addColorNode(VEC4 new_color, float time) {
			NodeVEC4 n;
			n.time = time;
			n.vec4 = new_color;
			colorNodes.push_back(n);
		}

		void setTransformInterpolator(std::string interpolator) { transformInterpolator = interpolator; }
		void setScaleInterpolator(std::string interpolator) { scaleInterpolator = interpolator; }
		void setColorInterpolator(std::string interpolator) { colorInterpolator = interpolator; }
		void setSpecialInterpolator(std::string interpolator) { specialInterpolator = interpolator; }

		void displayTransformPopup(NodeVEC2 node, int id) {
			ImGui::Text("Transform");
			ImGui::Text("");
			ImGui::Text("Node: %d", id);
			ImGui::Text("X: %.2f  Y: %.2f", node.vec2.x, node.vec2.y);
			ImGui::Text("Time: %.2f", node.time);
		}

		void displayScalePopup(NodeVEC2 node, int id) {
			ImGui::Text("Scale");
			ImGui::Text("");
			ImGui::Text("Node: %d", id);
			ImGui::Text("X: %.2f  Y: %.2f", node.vec2.x, node.vec2.y);
			ImGui::Text("Time: %.2f", node.time);
		}
		void displayColorPopup(NodeVEC4 node, int id) {
			ImGui::Text("Color");
			ImGui::Text("");
			ImGui::Text("Node: %d", id);
			ImGui::Text("Color: RGBA: %.2f %.2f %.2f %.2f", node.vec4.x, node.vec4.y, node.vec4.z, node.vec4.w);
			ImGui::SameLine();
			ImGui::ColorEdit4("##2f", (float*)& node.vec4, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel);
			ImGui::Text("Time: %.2f", node.time);
		}

		void displayTransformContextMenu(VEC2* transform, int idx) {
			ImGui::Text("Transform");
			ImGui::DragFloat2("", &transform->x, 0.01f, -100.0f, 100.0f, "%.2f");
			ImGui::Text("");
			if (ImGui::Button("Delete")) {
				if ((transformNodes.begin() + idx) != transformNodes.end() - 1) {
					transformNodes.pop_back();
				}
				else
					transformNodes.erase(transformNodes.begin() + idx);
			}
		}

		void displayScaleContextMenu(VEC2* scale, int idx) {
			ImGui::Text("Scale");
			ImGui::DragFloat2("", &scale->x, 0.01f, -100.0f, 100.0f, "%.2f");
			ImGui::Text("");
			if (ImGui::Button("Delete")) {
				if ((scaleNodes.begin() + idx) != scaleNodes.end() - 1) {
					scaleNodes.pop_back();
				}
				else
					scaleNodes.erase(scaleNodes.begin() + idx);
			}
		}

		void displayColorContextMenu(VEC4* color, int idx) {
			ImGui::Text("Color");
			ImGui::Text("RGBA: %.2f %.2f %.2f %.2f", color->x, color->y, color->z, color->w);
			ImGui::SameLine();
			ImGui::ColorEdit4("##2f", (float*)color, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel | ImGuiColorEditFlags_AlphaBar | ImGuiColorEditFlags_RGB);
			ImGui::Text("");
			if (ImGui::Button("Delete")) {
				if ((colorNodes.begin() + idx) != colorNodes.end() - 1) {
					colorNodes.pop_back();
				}
				else
					colorNodes.erase(colorNodes.begin() + idx);
			}
		}


		virtual void addSpecialNode(NodeSpecial new_special) = 0;
		virtual void displaySpecialPopup(NodeSpecial* new_special, int id) = 0;
		virtual void displaySpecialNodeContextMenu(std::string node_content) = 0;
		virtual void displayMediaControls() = 0;

		void renderTimeline() {

			if (ImGui::Button("Add Keyframe"))
				ImGui::OpenPopup("##Effect");

			ImGui::SameLine();

			if (ImGui::Button("Save to file"))
				saveAnimations(_name);

			ImGui::SameLine();

			static char str[150];
			if (ImGui::InputText("##Name", str, 150))
				_name = str;

			if (ImGui::BeginPopup("##Effect")) {
				ImGui::Text("Select animation type:");
				if (ImGui::Selectable("Transform"))
					addTransformNode(VEC2(0.0f, 0.0f), 0.0f);
				if (ImGui::Selectable("Scale"))
					addScaleNode(VEC2(0.0f, 0.0f), 0.0f);
				if (ImGui::Selectable("Color"))
					addColorNode(VEC4(0.0f, 0.0f, 0.0f, 1.0f), 0.0f);
				if (ImGui::Selectable("Special")) {
					NodeSpecial node;
					node.time = 0.0f;
					addSpecialNode(node);
				}
				ImGui::EndPopup();
			}

			ImGui::Columns(2, "tree", false);

			/*if (bool open1 = ImGui::TreeNode((void*)_name.c_str(), _name.c_str()))
			{*/
			ImGui::Text("");

			ImGui::SetColumnWidth(0, ImGui::GetFontSize() * 7);
			ImGui::BeginGroup();
			ImGui::Text("");
			ImGui::Text("Position");
			ImGui::Text("");
			ImGui::Text("Scale");
			ImGui::Text("");
			ImGui::Text("Color");
			ImGui::Text("");
			ImGui::Text("Special");
			ImGui::EndGroup();

			ImGui::NextColumn();

			ImGui::Text("");
			ImGui::Text("");
			ImGui::BeginGroup();

			/* Node positioning info */
			ImDrawList*		draw_list = ImGui::GetWindowDrawList();
			const ImVec2*	p = &ImGui::GetCursorScreenPos();
			const float		nodeSize = 5.0f;
			ImVec2			cursorPos = *p;
			ImGuiWindow*	win = ImGui::GetCurrentWindow();
			const float		timeline_radius = 12.0f;// max_time / 0.5f;
			const ImU32		inactive_color = ImGui::ColorConvertFloat4ToU32(GImGui->Style.Colors[ImGuiCol_Button]);
			const ImU32		active_color = ImGui::ColorConvertFloat4ToU32(GImGui->Style.Colors[ImGuiCol_ButtonHovered]);
			cursorPos.y += nodeSize;

			// Draw 
			ImVec2 timeCursor; 
			timeCursor.x = p->x + win->Size.x * current_time / (max_time * 2);
			timeCursor.y = p->y;
			draw_list->AddTriangleFilled(ImVec2(timeCursor.x - 0.25f + current_time, timeCursor.y), ImVec2(timeCursor.x + 0.25f, timeCursor.y), ImVec2(timeCursor.x, timeCursor.y - 0.5f), IM_COL32(50, 50, 50, 255));

			/* Begin timeline rendering */
			for (auto lineHorizontalPos = 0.0f; lineHorizontalPos < max_time + 1.0f; lineHorizontalPos += 0.5f) {
				cursorPos.x = p->x + win->Size.x * lineHorizontalPos / (max_time * 2 );
				draw_list->AddLine(ImVec2(cursorPos.x, cursorPos.y - nodeSize), ImVec2(cursorPos.x, cursorPos.y + ImGui::GetFontSize() * 10 - nodeSize), inactive_color);
			}
			// Draw transform nodes
			for (auto a = 0; a < transformNodes.size(); a++) {
				ImGui::PushID(a);
				cursorPos.x = p->x + win->Size.x * transformNodes[a].time / (max_time*2);
				ImGui::SetCursorScreenPos(cursorPos - ImVec2(timeline_radius, timeline_radius));

				ImGui::InvisibleButton(std::to_string(a).c_str(), ImVec2(2 * timeline_radius, 2 * timeline_radius));
				if (ImGui::IsItemActive() && ImGui::IsMouseDragging(0, 0.0f)) {
					transformNodes[a].time += ImGui::GetIO().MouseDelta.x / win->Size.x * (max_time*2);
					if (transformNodes[a].time < 0.0f)
						transformNodes[a].time = 0.0f;
					if (transformNodes[a].time > max_time)
						transformNodes[a].time = max_time;
				}
				if (ImGui::IsItemHovered()) {
					ImGui::BeginTooltip();
					displayTransformPopup(transformNodes[a], a);
					ImGui::EndTooltip();
				}
				if (ImGui::BeginPopupContextItem("transformPopup")) {
					displayTransformContextMenu(&transformNodes[a].vec2, a);
					ImGui::EndPopup();
				}
				draw_list->AddCircleFilled(cursorPos, nodeSize, ImGui::IsItemActive() || ImGui::IsItemHovered() ? active_color : inactive_color);
				if (a != transformNodes.size())
					ImGui::SameLine();

				ImGui::PopID();
			}

			// Draw scale nodes
			cursorPos.y += 35.0f;
			for (auto a = 0; a < scaleNodes.size(); a++) {
				ImGui::PushID(a + 10);
				cursorPos.x = p->x + win->Size.x * scaleNodes[a].time / (max_time * 2);
				ImGui::SetCursorScreenPos(cursorPos - ImVec2(timeline_radius, timeline_radius));

				ImGui::InvisibleButton(std::to_string(a).c_str(), ImVec2(2 * timeline_radius, 2 * timeline_radius));
				if (ImGui::IsItemActive() && ImGui::IsMouseDragging(0, 0.0f)) {
					scaleNodes[a].time += ImGui::GetIO().MouseDelta.x / win->Size.x * (max_time * 2);
					if (scaleNodes[a].time < 0.0f)
						scaleNodes[a].time = 0.0f;
					if (scaleNodes[a].time > max_time)
						scaleNodes[a].time = max_time;
				}
				if (ImGui::IsItemHovered()) {
					ImGui::BeginTooltip();
					displayScalePopup(scaleNodes[a], a);
					ImGui::EndTooltip();
				}
				if (ImGui::BeginPopupContextItem("scalePopup")) {
					displayScaleContextMenu(&scaleNodes[a].vec2, a);
					ImGui::EndPopup();
				}
				draw_list->AddCircleFilled(cursorPos, nodeSize, ImGui::IsItemActive() || ImGui::IsItemHovered() ? active_color : inactive_color);
				if (a != scaleNodes.size())
					ImGui::SameLine();

				ImGui::PopID();
			}

			// Draw color nodes 
			cursorPos.y += 30.0f;
			for (auto a = 0; a < colorNodes.size(); a++) {
				ImGui::PushID(a + 100);
				cursorPos.x = p->x + win->Size.x * colorNodes[a].time / (max_time * 2);
				ImGui::SetCursorScreenPos(cursorPos - ImVec2(timeline_radius, timeline_radius));

				ImGui::InvisibleButton(std::to_string(a).c_str(), ImVec2(2 * timeline_radius, 2 * timeline_radius));
				if (ImGui::IsItemActive() && ImGui::IsMouseDragging(0, 0.0f)) {
					colorNodes[a].time += ImGui::GetIO().MouseDelta.x / win->Size.x * (max_time * 2);
					if (colorNodes[a].time < 0.0f)
						colorNodes[a].time = 0.0f;
					if (colorNodes[a].time > max_time)
						colorNodes[a].time = max_time;
				}
				if (ImGui::IsItemHovered()) {
					ImGui::BeginTooltip();
					displayColorPopup(colorNodes[a], a);
					ImGui::EndTooltip();
				}
				if (ImGui::BeginPopupContextItem("colorPopup")) {
					displayColorContextMenu(&colorNodes[a].vec4, a);
					ImGui::EndPopup();
				}
				draw_list->AddCircleFilled(cursorPos, nodeSize, ImGui::IsItemActive() || ImGui::IsItemHovered() ? active_color : inactive_color);
				if (a != colorNodes.size())
					ImGui::SameLine();

				ImGui::PopID();
			}

			cursorPos.y += 30.0f;
			for (auto a = 0; a < specialNodes.size(); a++) {
				ImGui::PushID(a + 1000);
				cursorPos.x = p->x + win->Size.x * specialNodes[a].time / max_time;
				ImGui::SetCursorScreenPos(cursorPos - ImVec2(timeline_radius, timeline_radius));

				ImGui::InvisibleButton(std::to_string(a).c_str(), ImVec2(2 * timeline_radius, 2 * timeline_radius));
				if (ImGui::IsItemActive() && ImGui::IsMouseDragging(0, 0.0f)) {
					specialNodes[a].time += ImGui::GetIO().MouseDelta.x / win->Size.x * max_time;
					if (specialNodes[a].time < 0.0f)
						specialNodes[a].time = 0.0f;
					if (specialNodes[a].time > max_time)
						specialNodes[a].time = max_time;
				}
				if (ImGui::IsItemHovered()) {
					ImGui::BeginTooltip();
					displaySpecialPopup(&specialNodes[a], a);
					ImGui::EndTooltip();
				}
				if (ImGui::BeginPopupContextItem()) {
					displaySpecialNodeContextMenu(specialNodes[a].special);
					ImGui::EndPopup();
				}
				draw_list->AddCircleFilled(cursorPos, nodeSize, ImGui::IsItemActive() || ImGui::IsItemHovered() ? active_color : inactive_color);
				if (a != specialNodes.size())
					ImGui::SameLine();

				ImGui::PopID();
			}

			ImGui::EndGroup();
				
			ImGui::NextColumn();
			ImGui::Columns(1);
			
		}

		NodeVEC2 getTransformKeyframe(VEC2 origTransform) {
			NodeVEC2 returnNode, next;
			// Add offset in case the last node's time it's the same than max_time
			next.time = max_time + 1.0f;

			NodeVEC2 previous;
			previous.time = -1.0f;

			if (colorNodes.size() == 1) {
				previous.time = 0.0f;
				previous.vec2 = origTransform;
				next = transformNodes[0];
			}
			else {
				for (auto& node : transformNodes) {
					if (node.time <= current_time && current_time - node.time < current_time - previous.time) {
						previous = node;
					}
					else if (node.time >= current_time && node.time - current_time < next.time - current_time && node.time != previous.time) {
						next = node;
					}
				}
			}

			float ratio = (current_time - previous.time) / (next.time - previous.time);
			returnNode.vec2.x = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(transformInterpolator)(previous.vec2.x, next.vec2.x, ratio);
			returnNode.vec2.y = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(transformInterpolator)(previous.vec2.y, next.vec2.y, ratio);
			returnNode.time = current_time;

			return returnNode;
		}

		NodeVEC2 getScaleKeyframe(VEC2 origScale) {
			NodeVEC2 returnNode, next;
			// Add offset in case the last node's time it's the same than max_time
			next.time = max_time + 1.0f;

			NodeVEC2 previous;
			previous.time = -1.0f;

			if (colorNodes.size() == 1) {
				previous.time = 0.0f;
				previous.vec2 = origScale;
				next = scaleNodes[0];
			}
			else {
				for (auto& node : scaleNodes) {
					if (node.time <= current_time && current_time - node.time < current_time - previous.time) {
						previous = node;
					}
					else if (node.time >= current_time && node.time - current_time < next.time - current_time && node.time != previous.time) {
						next = node;
					}
				}
			}

			float ratio = (current_time - previous.time) / (next.time - previous.time);
			returnNode.vec2.x = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(scaleInterpolator)(previous.vec2.x, next.vec2.x, ratio);
			returnNode.vec2.y = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(scaleInterpolator)(previous.vec2.y, next.vec2.y, ratio);
			returnNode.time = current_time;

			return returnNode;
		}

		NodeVEC4 getColorKeyframe(VEC4 origColor) {
			NodeVEC4 returnNode, next;
			// Add offset in case the last node's time it's the same than max_time
			next.time = max_time + 1.0f;  

			NodeVEC4 previous;
			previous.time = -1.0f;

			if (colorNodes.size() == 1) {
				previous.time = -1.0f;
				previous.vec4 = origColor;
				next = colorNodes[0];
			}
			else {
				for (auto& node : colorNodes) {
					if (node.time <= current_time && current_time - node.time < current_time - previous.time) {
						previous = node;
					}
					else if (node.time >= current_time && node.time - current_time < next.time - current_time && node.time != previous.time) {
						next = node;
					}
				}
			}

			float ratio = (current_time - previous.time) / (next.time - previous.time);
			returnNode.vec4.x = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(colorInterpolator)(previous.vec4.x, next.vec4.x, ratio);
			returnNode.vec4.y = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(colorInterpolator)(previous.vec4.y, next.vec4.y, ratio);
			returnNode.vec4.z = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(colorInterpolator)(previous.vec4.z, next.vec4.z, ratio);
			returnNode.vec4.w = Interpolator::InterpolatorManager::Instance().getInterpolatorAsLambda(colorInterpolator)(previous.vec4.w, next.vec4.w, ratio);
			returnNode.time = current_time;

			return returnNode;
		}

		virtual void startAnimation(){}
		virtual	void stopAnimation(){}
		virtual void pauseAnimation(){}
		virtual void update(float dt){}
		bool isRunning() { return running; }
		bool isLoop() { return looping; }

		void saveAnimations(std::string widgetName) {
			json j;
			std::vector<json> jTransformNodes;
			std::vector<json> jScaleNodes;
			std::vector<json> jColorNodes;
			std::vector<json> jSpecialNodes;

			for (auto& tnode : transformNodes) {
				json node;
				node["time"] = tnode.time;
				node["value"] = vec2ToString(tnode.vec2);
				jTransformNodes.push_back(node);
			}

			for (auto& tnode : scaleNodes) {
				json node;
				node["time"] = tnode.time;
				node["value"] = vec2ToString(tnode.vec2);
				jScaleNodes.push_back(node);
			}

			for (auto& tnode : colorNodes) {
				json node;
				node["time"] = tnode.time;
				node["value"] = vec4ToString(tnode.vec4);
				jColorNodes.push_back(node);
			}

			for (auto& tnode : specialNodes) {
				json node;
				node["time"] = tnode.time;
				node["value"] = tnode.special;
				jSpecialNodes.push_back(node);
			}

			j["name"] = widgetName;
			j["transform_nodes"] = jTransformNodes;
			j["scale_nodes"] = jScaleNodes;
			j["color_nodes"] = jColorNodes;
			j["special_nodes"] = jSpecialNodes;
			j["animation_time"] = max_time;
			j["IsLooping"] = looping;

			std::string testpath = animationFilePath + widgetName + ".json";
			std::ofstream file(testpath.c_str());
			file << std::setw(4) << j << std::endl;
		}

		int getNumTransformNodes() { return static_cast<int>(transformNodes.size()); }
		int getNumScaleNodes() { return static_cast<int>(scaleNodes.size()); }
		int getNumColorNodes() { return static_cast<int>(colorNodes.size()); }
	
	};
}
