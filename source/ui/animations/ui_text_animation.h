#pragma once

#include "mcv_platform.h"
#include "ui_base_animation.h"

namespace UI
{

	class CTextAnimation : public CBaseWidgetAnimation {

	public:

		CTextAnimation() {}
		CTextAnimation(std::string name, float anim_time){
			_name = name;
			max_time = anim_time;
		}

		void addSpecialNode(NodeSpecial new_special) override;
		void displaySpecialPopup(NodeSpecial* node, int id);
		void displayMediaControls();
		void displaySpecialNodeContextMenu(std::string node_content) override;
		
		void startAnimation(bool loop = false) {
			running = true;
			looping = loop;
		}
		void stopAnimation() {
			running = false;
			current_time = 0.0f;
		}
		void pauseAnimation() {
			running = false;
		}
		void update(float dt);

		std::string getName() { return _name; }
		float		getAnimationTime() { return max_time; }
		void setAnimationTime(float time);

	private:

	};
}