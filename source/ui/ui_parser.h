#pragma once
#include "widgets/ui_image.h"
#include "widgets/ui_progress.h"
#include "widgets/ui_text_font.h"
#include "widgets/ui_button.h"

namespace UI
{
  class CWidget;
  class CEffect;
  struct TParams;
  struct TImageParams;
  struct TTextParams;
  struct TTextFontParams;
  struct TProgressParams;

  class CParser
  {
  public:

	  static CParser& getInstance() {
		  static CParser instance;
		  return instance;
	  }

    void loadFile(const json& jsonWidgetsListFile);
	static void saveFile(CWidget* widget);
    void loadWidget(const std::string& widgetFile);

    CWidget* parseWidget(const json& jData, CWidget* parent);

    CWidget* parseWidget(const json& jData)	;
    CWidget* parseImage(const json& jData);
	CWidget* parseText(const json& jData);
	CWidget* parseTextFont(const json& jData);
    CWidget* parseButton(const json& jData);
    CWidget* parseButtonContainer(const json& jData);
    CWidget* parseProgress(const json& jData);

    void parseParams(TParams& params, const json& jData);
    void parseParams(TImageParams& params, const json& jData);
	void parseParams(TTextParams& params, const json& jData);
	void parseParams(TTextFontParams& params, const json& jData);
    void parseParams(TProgressParams& params, const json& jData);

    CEffect* parseEffect(const json& jData);
    CEffect* parseFXAnimateUV(const json& jData);
    CEffect* parseFXScale(const json& jData);
    CEffect* parseFXMovement(const json& jData);

    Interpolator::IInterpolator* parseInterpolator(const std::string& type);

	json parseWidgetChild(CImage& child); 
	json parseWidgetChild(CProgress& child);
	json parseWidgetChild(CTextFont& child);
	json parseWidgetChild(CButton& child);
	json parseWidgetChild(CWidget& child);


	std::string vec2ToString(VEC2 v) {
		return std::to_string(v.x) + " " + std::to_string(v.y);
	}

	std::string vec3ToString(VEC3 v) {
		return std::to_string(v.x) + " " + std::to_string(v.y) + " " + std::to_string(v.z);
	}

	std::string vec4ToString(VEC4 v) {
		return std::to_string(v.x) + " " + std::to_string(v.y) + " " + std::to_string(v.z) + " " + std::to_string(v.w);
	}

	
  };
}
#define UIParser CParser::getInstance()