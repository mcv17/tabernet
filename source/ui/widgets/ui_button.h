#pragma once

#include "ui/ui_widget.h"
#include "ui/ui_utils.h"

namespace UI
{
  class CButton : public CWidget
  {
  public:
	  CButton();
	  void render() override;
		void update(float dt) override;
		void init() override;

	void renderInMenu() override;
    void setCurrentState(const std::string& stateName);
		bool hasFocus() { return _hasFocus; }
		void setFocus(bool focus);

  private:
		const char* NORMAL_STATE = "default";
		const char* HOVER_STATE = "hover";
		const char* PRESSED_STATE = "pressed";
		bool hasInit = false;

    struct TState
    {
      TImageParams bgParams, fgParams;
      TTextFontParams textParams;
    };
    std::unordered_map<std::string, TState> _states;
		std::string _pressedSoundEvent;
    std::string _currentState = NORMAL_STATE;
		bool _isButtonPressed = false;

		TBoundingBox _boundingBox;
		bool _hasFocus = false, _hasHover = false;

		void renderState(const std::string& state);

    friend class CParser;
  };
}
