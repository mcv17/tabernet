#pragma once

#include "mcv_platform.h"
#include "ui/widgets/ui_image.h"
#include "ui/ui_utils.h"
#include "utils/json.hpp"
#include "ui/ui_effect.h"
#include "engine.h"
#include "render/module_render.h"


void UI::CImage::loadAnimations(json animData)
{
	const std::string	name;
	bool				isLooping;
	float				animationTime;
	CImageAnimation* new_anim = new CImageAnimation();

	const std::string jName = animData.value("name", name);
	const bool jIsLooping = animData.value("IsLooping", isLooping);
	const float jAnimTime = animData.value("animation_time", animationTime);

	new_anim->configure(jName, jAnimTime, jIsLooping);

	if (animData.count("transform_nodes") > 0) {
		for (const auto& node : animData["transform_nodes"]) {
			new_anim->addTransformNode(loadVector2(node, "value"), node["time"]);
		}
	}

	if (animData.count("scale_nodes") > 0) {
		for (const auto& node : animData["scale_nodes"]) {
			new_anim->addScaleNode(loadVector2(node, "value"), node["time"]);
		}
	}

	if (animData.count("color_nodes") > 0) {
		for (const auto& node : animData["color_nodes"]) {
			new_anim->addColorNode(loadVector4(node, "value"), node["time"]);
		}
	}

	//Interpolators
	new_anim->setTransformInterpolator(animData.value("transform_interpolator", "linear"));
	new_anim->setScaleInterpolator(animData.value("scale_interpolator", "linear"));
	new_anim->setColorInterpolator(animData.value("color_interpolator", "linear"));
	new_anim->setSpecialInterpolator(animData.value("special_interpolator", "linear"));

	animations.push_back(new_anim);

}

void UI::CImage::init() {
	if (_imageParams.mask) {
		_maskedImage.create((_name + "masked_texture").c_str(), _imageParams.size.x, _imageParams.size.y, DXGI_FORMAT_R8G8B8A8_UNORM);
	}
}

void UI::CImage::render()
{
	// render screen quad with texture
	MAT44 world = _pivot * MAT44::CreateScale(_imageParams.size.x, _imageParams.size.y, 1.f) * _absolute;
	if (_imageParams.mask) {
		auto* rt = _maskedImage.activateRTAndClear();
		renderMasked(_imageParams.texture, _imageParams.mask, _imageParams.minUV, _imageParams.maxUV, Vector2::Zero, Vector2::One, _imageParams.color, _imageParams.additive, _imageParams.frameSize, _imageParams.nFrames, _imageParams.timeRatio);
		
		Render.startRenderingBackBuffer();
		renderBitmap(world, &_maskedImage);
	}
	else
		renderBitmap(world, _imageParams.texture, _imageParams.minUV, _imageParams.maxUV, _imageParams.color, _imageParams.additive, _imageParams.frameSize, _imageParams.nFrames, _imageParams.timeRatio);
}

void UI::CImage::update(float dt)
{
	for (auto& anim : animations) {
		anim->update(dt);
		if (anim->isRunning()) {
			if (anim->getNumTransformNodes() > 0) {
				lastTransform = anim->getTransformKeyframe(origTransform).vec2;
				_params.position = lastTransform;
			}
			if (anim->getNumScaleNodes() > 0) {
				lastScale = anim->getScaleKeyframe(origScale).vec2;
				_params.scale = lastScale;
			}
			if (anim->getNumColorNodes() > 0) {
				lastColor = anim->getColorKeyframe(origColor).vec4;
				_imageParams.color = lastColor;
			}
		}
	}

	_imageParams.currentTime += dt;

	//If it is animated
	if (_imageParams.nFrames != Vector2::Zero) {
		//Compute the current time ratio
		_imageParams.timeRatio = _imageParams.currentTime / (_imageParams.nFrames.x * _imageParams.nFrames.y * _imageParams.frameDuration);
		//Check if looped, or when it reaches 1.0 it stays at the end of the animation
		_imageParams.timeRatio = _imageParams.isAnimationLooped ? _imageParams.timeRatio : std::min(_imageParams.timeRatio, 1.0f);
	}

	for (auto fx : _effects)
	{
		fx->update(dt);
	}
}

void UI::CImage::renderInMenu() {

	if (ImGui::TreeNode(getName().c_str())) {

		ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
		ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.13f, 0.14f, 0.168f, 1.0f));
		ImGui::BeginChild("Image Widget", ImVec2(0, 0), true, 0);

		ImGui::Text(std::string(_name + "/" + _alias).c_str());
		ImGui::Separator();
		ImGui::Text("Params:");
		ImGui::Text("");
		ImGui::Text("");
		ImGui::Text("Pivot: ");
		ImGui::SameLine();
		ImGui::PushItemWidth(100);
		ImGui::DragFloat2("##Pivot", &_params.pivot.x, 0.2f, 0.0f, 2000.0f);
		ImGui::PopItemWidth();
		ImGui::SameLine();
		ImGui::Text("Position: ");
		ImGui::SameLine();
		ImGui::PushItemWidth(100);
		ImGui::DragFloat2("##Position", &_params.position.x, 1.0f, -100.0f, 2000.0f);
		ImGui::PopItemWidth();
		ImGui::SameLine();
		ImGui::Text("Scale: ");
		ImGui::SameLine();
		ImGui::PushItemWidth(100);
		ImGui::DragFloat2("##Scale", &_params.scale.x, 0.01f, -100.0f, 100.0f);
		ImGui::PopItemWidth();
		ImGui::Text("Rotation: ");
		ImGui::SameLine();
		ImGui::PushItemWidth(200);
		ImGui::DragFloat("##Rotation", &_params.rotation, 0.01f, -1, 1.0f);
		ImGui::PopItemWidth();
		ImGui::SameLine();
		ImGui::Text("Is Visible? ");
		ImGui::SameLine();
		ImGui::Checkbox("##Visible", &_params.visible);

		ImGui::Separator();
		ImGui::Text("Effects");
		for (auto effect : _effects) {
			effect->renderInMenu();
		}

		MENUTEXT("");

		ImGui::Separator();
		ImGui::Text("Image Widget:");
		ImGui::Columns(2, "mycolumns");
		ImGui::BeginGroup();
		MENUTEXT("");
		MENUTEXT("");
		MENUTEXT("");
		MENUTEXT("");

		if (ImGui::ImageButton(_imageParams.texture->getShaderResourceView(), ImVec2(300, 100)))
			ImGui::OpenPopup("ImagePopup");

		if (ImGui::BeginPopup("ImagePopup")) {
			ImGui::Image((void*)_imageParams.texture->getShaderResourceView(),
				ImVec2(Maths::clamp(_imageParams.size.x, 0.0f, 960.0f), Maths::clamp(_imageParams.size.y, 0.0f, 512.0f)));
			ImGui::EndPopup();
		}

		ImGui::NextColumn();

		ImGui::Text("Size: %.2f x %.2f", _imageParams.size.x, _imageParams.size.y);
		MENUTEXT("");

		MENUTEXT("Additive ?");
		ImGui::SameLine();
		ImGui::Checkbox("##Additive", &_imageParams.additive);
		MENUTEXT("");

		ImGui::Columns(2, "color_column");

		MENUTEXT("Color:");
		if (ImGui::ColorButton("ColorButton", *(ImVec4*)& _imageParams.color, 0, ImVec2(50, 25))) {
			ImGui::OpenPopup("ColorPicker");
		}

		if (ImGui::BeginPopup("ColorPicker")) {
			ImGui::ColorPicker4("##ColorPicker", &_imageParams.color.x, misc_flags);
			ImGui::EndPopup();
		}

		ImGui::SameLine();

		/*COLOR INFO*/
		ImGui::BeginGroup();
		MENUTEXT("R: ");
		ImGui::SameLine();
		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(0.8f, 0.0f, 0.0f, 0.2f));
		char rBuff[5];
		sprintf(rBuff, "%.2f", _imageParams.color.x);
		ImGui::ProgressBar(_imageParams.color.x / 1.0f, ImVec2(125.0f, 0.0f), rBuff);
		ImGui::PopStyleColor();

		MENUTEXT("G: ");
		ImGui::SameLine();
		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(0.0f, 0.8f, 0.0f, 0.2f));
		char gBuff[5];
		sprintf(gBuff, "%.2f", _imageParams.color.y);
		ImGui::ProgressBar(_imageParams.color.y / 1.0f, ImVec2(125.0f, 0.0f), gBuff);
		ImGui::PopStyleColor();

		MENUTEXT("B: ");
		ImGui::SameLine();
		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(0.0f, 0.0f, 0.8f, 0.2f));
		char bBuff[5];
		sprintf(bBuff, "%.2f", _imageParams.color.z);
		ImGui::ProgressBar(_imageParams.color.z / 1.0f, ImVec2(125.0f, 0.0f), bBuff);
		ImGui::PopStyleColor();

		MENUTEXT("A: ");
		ImGui::SameLine();
		ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(0.3f, 0.3f, 0.3f, 0.2f));
		char aBuff[5];
		sprintf(aBuff, "%.2f", _imageParams.color.w);
		ImGui::ProgressBar(_imageParams.color.w / 1.0f, ImVec2(125.0f, 0.0f), aBuff);
		ImGui::PopStyleColor();
		ImGui::EndGroup();
		/**/

		ImGui::EndGroup();

		ImGui::NextColumn();

		ImGui::BeginGroup();
		MENUTEXT("");
		MENUTEXT("Min UV:");
		ImGui::SameLine();
		ImGui::PushItemWidth(200);
		ImGui::DragFloat2("##minUV", &_imageParams.minUV.x, 0.01f, 0.0f, 1.0f);
		ImGui::PopItemWidth();
		ImGui::SameLine();
		ImGui::Spacing(5.0f);
		MENUTEXT("Max UV:");
		ImGui::SameLine();
		ImGui::PushItemWidth(200);
		ImGui::DragFloat2("##maxUV", &_imageParams.maxUV.x, 0.01f, 0.0f, 1.0f);
		ImGui::PopItemWidth();
		MENUTEXT("");
		ImGui::EndGroup();

		ImGui::NextColumn();

		ImGui::BeginGroup();
		MENUTEXT("Frame Size:");
		ImGui::SameLine();
		ImGui::PushItemWidth(100);
		ImGui::DragFloat2("##frameSize", &_imageParams.frameSize.x, 0.01f, 0.0f, 1.0f);
		ImGui::PopItemWidth();
		ImGui::SameLine();
		ImGui::Spacing(5.0f);

		MENUTEXT("Num Frames:");
		ImGui::SameLine();
		ImGui::PushItemWidth(100);
		ImGui::DragFloat2("##nFrames", &_imageParams.nFrames.x, 0.01f, 0.0f, 1.0f);
		ImGui::PopItemWidth();
		ImGui::SameLine();
		ImGui::Spacing(5.0f);

		MENUTEXT("Frame Duration: ");
		ImGui::SameLine();
		ImGui::PushItemWidth(100);
		ImGui::DragFloat("##FrameDuration", &_imageParams.frameDuration, 0.01f, 0.0f, 1.0f);
		ImGui::PopItemWidth();

		ImGui::Spacing();
		ImGui::EndGroup();

		ImGui::Columns(1);

		/* Animation Editor */
		if (animations.size() > 0) {
			static bool p_open;
			static int selected = 0;
			ImGui::SetNextWindowSize(ImVec2(700, 300), ImGuiCond_FirstUseEver);
			if (ImGui::Begin("Animation Editor", &p_open))
			{
				// left
				std::string leftId = "left pane" + _name;
				ImGui::BeginChild(leftId.c_str(), ImVec2(150, 0), true);
				for (int i = 0; i < animations.size(); i++)
				{
					char label[128];
					sprintf(label, animations[i]->getName().c_str(), i);
					if (ImGui::Selectable(label, selected == i))
						selected = i;
				}
				ImGui::EndChild();
				ImGui::SameLine();
				// right
				ImGui::BeginGroup();


				std::string leftViewId = "item view" + _name;
				ImGui::BeginChild(leftViewId.c_str(), ImVec2(0, -ImGui::GetFrameHeightWithSpacing())); // Leave room for 1 line below us
				ImGui::Text(animations[selected]->getName().c_str(), selected);
				ImGui::Separator();
				std::string childId = "Child1" + _name;
				ImGui::BeginChild(childId.c_str(), ImVec2(ImGui::GetWindowContentRegionWidth(), 0), true);
				animations[selected]->renderTimeline();
				ImGui::EndChild();
				ImGui::EndChild();
				ImGui::EndGroup();


				ImGui::End();
			}

			// Media controls
			std::string mediaId = "left media control" + _name;
			bool mediaWndOpen = true;
			if (ImGui::Begin(mediaId.c_str(), &mediaWndOpen, ImVec2(300, 150))) {
				animations[selected]->displayMediaControls();
				ImGui::End();
			}
		}

		ImGui::EndChild();
		ImGui::PopStyleColor();
		ImGui::PopStyleVar();
		ImGui::TreePop();
	}
}

void UI::CImage::playAnimation(std::string name, bool loop, float startTime)
{
	for (auto& anim : animations) {
		if (anim->getName() == name) {
			origTransform = _params.position;
			origScale = _params.scale;
			origColor = _imageParams.color;

			currentAnimation = anim;
			currentAnimation->startAnimation(loop);
			currentAnimation->setAnimationTime(startTime);
		}
	}
}

void UI::CImage::stopAnimation(std::string name)
{
	currentAnimation->stopAnimation();

	_params.position = origTransform;
	_params.scale = origScale;
	_imageParams.color = origColor;
}

float UI::CImage::getCurrentAnimationPercentage() {
	if (currentAnimation)
		return currentAnimation->getAnimationPercentage();
	return 0.0f;
}
