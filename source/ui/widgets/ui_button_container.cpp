#include "mcv_platform.h"
#include "ui_button_container.h"
#include "logic/logic_manager.h"
#include "engine.h"
#include "ui_button.h"

void UI::CButtonContainer::update(float dt) {
	if (_children.size() < 2)
		return;

	auto logicManager = LogicManager::Instance();
	int numButtons = _children.size();

	CButton* child = getChildBtn(_focusChild);
	if (!child->hasFocus())
		regainFocus();

	if (EngineInput[_prevButtonMapping].justPressed() && (_focusChild > 0 || _loops)) {
		// Previous key pressed
		child->setFocus(false);
		_focusChild = (numButtons + (_focusChild - 1)) % numButtons;
		getChildBtn(_focusChild)->setFocus(true);

		playSwitchSoundEvent();
	}
	else if (EngineInput[_nextButtonMapping].justPressed() && (_focusChild < numButtons - 1 || _loops)) {
		// Next key pressed
		child->setFocus(false);
		_focusChild = (_focusChild + 1) % numButtons;
		getChildBtn(_focusChild)->setFocus(true);
		
		playSwitchSoundEvent();
	}

	CWidget::update(dt);
}

/* Regain focus if for some reason (e.g. the mouse has changed the focus) the one that
*	 was supposed to be focused is not.
*/
void UI::CButtonContainer::regainFocus() {
	bool focusedBtnFound = false;
	for (int i = 0; i < _children.size(); i++) {
		if (getChildBtn(i)->hasFocus()) {
			_focusChild = i;
			focusedBtnFound = true;
		}
	}
	if (!focusedBtnFound) {
		getChildBtn(_focusChild)->setFocus(true);
	}
}

void UI::CButtonContainer::resetFocus() {
	for (int i = 0; i < _children.size(); i++) {
		getChildBtn(i)->setFocus(false);
	}
	_focusChild = 0;
}

void UI::CButtonContainer::playSwitchSoundEvent() {
	if (!_switchSoundEvent.empty())
		EngineAudio.playEvent(_switchSoundEvent);
}

UI::CButton * UI::CButtonContainer::getChildBtn(int idx) {
	CButton* btn = dynamic_cast<CButton*>(_children[idx]);
	assert(btn && "Expected CButton, received something else");
	return btn;
}
