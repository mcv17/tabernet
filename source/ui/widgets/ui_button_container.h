#pragma once

#include "ui/ui_widget.h"
#include "ui/ui_utils.h"

namespace UI {

	class CButton;

	class CButtonContainer : public CWidget {

	public:

		void update(float dt) override;
		//void init() override;

		void regainFocus();
		void resetFocus();
		void playSwitchSoundEvent();

	private:

		int _focusChild = 0;
		bool _loops = true;
		std::string _prevButtonMapping = "menu_prev";
		std::string _nextButtonMapping = "menu_next";
		std::string _switchSoundEvent = "";

		CButton* getChildBtn(int idx);

		friend class CParser;

	};

}