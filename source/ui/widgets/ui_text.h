#pragma once

#include "ui/ui_widget.h"

namespace UI
{
  class CText : public CWidget
  {
  public:
    void render() override;
	void renderInMenu() override;

  private:
    TTextParams _textParams;

    friend class CParser;
  };
}
