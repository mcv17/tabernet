
#pragma once

#include "mcv_platform.h"
#include "ui/widgets/ui_button.h"
#include "ui/ui_utils.h"
#include "render/textures/texture.h"
#include "engine.h"
#include "ui_button_container.h"

namespace UI
{
  CButton::CButton() {
    _resetRenderConfig = true;
  }

  void CButton::render()
  {
    if (_states.count(_currentState) == 0)
      return;

		renderState(_currentState);
  }

	void CButton::update(float dt) {
		const Vector2 mousePos = EngineInput.mouse().getPosition();
		bool _hasHover = _boundingBox.isPointWithin(mousePos);
		auto& mouseLeftBtn = EngineInput["mouse_left"];
		auto& confirmBtn = EngineInput["menu_confirm"];
		
		if (_hasHover) {
			if (!hasFocus()) {
				CButtonContainer* btnContainer = dynamic_cast<CButtonContainer*>(_parent);
				if (btnContainer)
					btnContainer->resetFocus();
				setFocus(true);
				btnContainer->playSwitchSoundEvent();
			}

			if (mouseLeftBtn.justPressed())
				_isButtonPressed = true;
			if (_isButtonPressed && mouseLeftBtn.isPressed())
				setCurrentState(PRESSED_STATE);
			else
				setCurrentState(HOVER_STATE);

			if (_isButtonPressed && mouseLeftBtn.justReleased()) {
				EngineScripting.envCall("on" + _name + "Clicked", EngineScripting.getUIEnvironment());
				if (!_pressedSoundEvent.empty())
					EngineAudio.playEvent(_pressedSoundEvent);
				_isButtonPressed = false;
			}
		}
		else if (_hasFocus) {
			if (confirmBtn.justPressed())
				_isButtonPressed = true;
			if (_isButtonPressed && confirmBtn.isPressed())
				setCurrentState(PRESSED_STATE);
			else
				setCurrentState(HOVER_STATE);

			if (_isButtonPressed && confirmBtn.justReleased()) {
				EngineScripting.envCall("on" + _name + "Clicked", EngineScripting.getUIEnvironment());
				_isButtonPressed = false;
			}
		} 
		else {
			setCurrentState(NORMAL_STATE);
			_isButtonPressed = false;
		}
	}

	void CButton::init() {
		if (hasInit)
			return;

		Vector2 bbPos = transformVector2(Vector2::Zero, _absolute);
		bbPos += _boundingBox.getPosition();
		Vector2 bbSize = _boundingBox.getSize();

		_boundingBox = TBoundingBox(bbPos, bbSize);

		hasInit = true;
	}

  void CButton::setCurrentState(const std::string& stateName)
  {
		_currentState = stateName;
  }

  void CButton::renderInMenu() {
	  CWidget::renderInMenu();

	  ImGui::Separator();
	  ImGui::Text("Button Widget:");


  }
	void CButton::setFocus(bool focus) {
		_hasFocus = focus;
	}

	void CButton::renderState(const std::string & state) {
		auto& stateRef = _states[state];
		
		if (stateRef.bgParams.texture) {
			MAT44 imageWorld = _pivot * MAT44::CreateScale(stateRef.bgParams.size.x, stateRef.bgParams.size.y, 1.f) * _absolute;
			renderBitmap(imageWorld, stateRef.bgParams.texture, VEC2::Zero, VEC2::One, stateRef.bgParams.color, stateRef.bgParams.additive);
		}
		
		if (!stateRef.textParams.text.empty()) {
			auto& params = stateRef.textParams;
			Vector2 position = _params.pivot + transformVector2(params.position, _absolute);
			if (params.hasOutline)
				renderFontTextWithOutline(params.text, position, params.textColor, params.effectColor, params.scale, params.rotation, params.fontName);
			else if (params.hasDropShadow)
				renderFontTextWithDropShadow(params.text, position, params.textColor, params.effectColor, params.scale, params.rotation, params.fontName);
			else
				renderFontText(params.text, position, params.textColor, params.scale, params.rotation, params.fontName);
		}
		
		if (stateRef.fgParams.texture) {
			MAT44 imageWorld = _pivot * MAT44::CreateScale(stateRef.fgParams.size.x, stateRef.fgParams.size.y, 1.f) * _absolute;
			renderBitmap(imageWorld, stateRef.fgParams.texture, VEC2::Zero, VEC2::One, stateRef.fgParams.color, stateRef.fgParams.additive);
		}
	}
}
