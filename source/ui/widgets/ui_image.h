#pragma once

#include "ui/ui_widget.h"
#include "ui/animations/ui_image_animation.h"

namespace UI
{
  class CImage : public CWidget
  {
  public:

    CImage() {
		origTransform = _params.position;
		origScale = _imageParams.size;
		origColor = _imageParams.color;
	}

	void loadAnimations(json animData);
	void init() override;
	void render() override;
	void update(float dt) override;
	void renderInMenu() override;
	void playAnimation(std::string name, bool loop, float startTime = 0.0f);
	void stopAnimation(std::string name);
	float getCurrentAnimationPercentage();
    TImageParams* getImageParams() override { return &_imageParams; }

  private:

    VEC2 origTransform, origScale;
    VEC4 origColor;

	VEC2 lastTransform, lastScale;
	VEC4 lastColor;

	bool _isMasked = false;
	CRenderToTexture _maskedImage;

    TImageParams _imageParams;
	std::vector<CImageAnimation*> animations;
	CImageAnimation* currentAnimation;
    friend class CParser;
  };
}
