#pragma once

#include "mcv_platform.h"
#include "ui/effects/ui_fx_animate_uv.h"
#include "ui/ui_widget.h"

namespace UI
{
  void CFXAnimateUV::update(float dt)
  {
    assert(_owner);

    TImageParams* imageParams = _owner->getImageParams();
    if (!imageParams)
    {
      return;
    }

    imageParams->minUV += _speed * dt;
    imageParams->maxUV += _speed * dt;
  }

  void CFXAnimateUV::renderInMenu()
  {
	  if (ImGui::TreeNode("UV Animate Effect")) {
		  ImGui::Text("Speed: ");
		  ImGui::SameLine();
		  ImGui::DragFloat2("#drag2uv", &_speed.x, 0.1f, 0.0f, 100.0f, "%.2f");
		  ImGui::TreePop();
	  }
  }

}
