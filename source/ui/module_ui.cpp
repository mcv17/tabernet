#include "mcv_platform.h"
#include "ui/module_ui.h"
#include "ui/ui_parser.h"
#include "ui/ui_widget.h"
#include "ui/ui_utils.h"
#include "ui/ui_controller.h"
#include "ui/widgets/ui_text_font.h"
#include "engine.h"
#include "entity/entity.h"
#include "components/camera/comp_camera.h"
#include "entity/entity_parser.h"

namespace UI
{
  std::unique_ptr<DirectX::SpriteBatch> spriteBatch;
  std::map<std::string, std::unique_ptr<DirectX::SpriteFont>> spriteFonts;
  std::string defaultFont;

  CModuleUI::CModuleUI(const std::string& name)
    : IModule(name)
  {
  }

  bool CModuleUI::start()
  {
    CParser parser;
	json jData = Utils::loadJson("data/ui/ui.json");
    parser.loadFile(jData["widgets_to_load"]);

	defaultFont = jData.value("default_font", "");
	initFonts();

    return true;
  }

  void CModuleUI::update(float dt)
  {
	PROFILE_FUNCTION("ModuleUI");
    for (auto controller : _activeControllers)
    {
      controller->update(dt);
    }

    for (auto widget : _activeWidgets)
    {
      widget->update(dt);
	  if (widget->getIsKillAfterClockFinish() && widget->getCLock()->isFinished())
		  deactivateWidget(widget->getName());
    }

	for (auto widgetToDelete : _toRemoveWidgets) {
		if (widgetToDelete != nullptr)
			delete widgetToDelete;
	}
	_toRemoveWidgets.clear();
		
  }

  void CModuleUI::render()
  {
	PROFILE_FUNCTION("ModuleUI-Render");
	if (EngineUI.getImageWidgetByName("black_screen", "black_screen_image") != nullptr && _activeWidgets.back()->getName() != "black_screen") {
		deactivateWidget("black_screen");
		activateWidget("black_screen");
	}

    for (auto widget : _activeWidgets)
    {
      widget->doRender();
    }
  }

  void CModuleUI::renderInMenu()
  {
	  int current_widget_idx = 0;
	  ImGuiTreeNodeFlags flags = 0;
	  ImGui::Text("UI Editor");
	  ImGui::Separator();
	  if (ImGui::BeginTabBar("UI Tab bar", 0)) {
		  if (ImGui::BeginTabItem("Widgets")) {
			  for (auto& widget : _activeWidgets) {
				  if (ImGui::TreeNode(widget->getName().c_str())) {

					  if (ImGui::Button("Save to File"))
						  UIParser.saveFile(widget);

					  ImGui::Separator();

					  widget->renderInMenu();

					  ImGui::TreePop();
				  }
			  }
			  ImGui::EndTabItem();
		  }
		  if (ImGui::BeginTabItem("Create Widget")) {

			  ImGui::EndTabItem();
		  }
		  ImGui::EndTabBar();
	  }
  }

  void CModuleUI::registerWidget(CWidget* widget)
  {
    assert(_registeredWidgets.find(widget->getName()) == _registeredWidgets.end());

    _registeredWidgets[widget->getName()] = widget;
  }

  CTextFont* CModuleUI::createFloatingDamageWidget(TFloatDamage settings)
  {
	  TTextFontParams textParams;
	  
	  textParams.text = std::wstring(settings.textDamage.begin(), settings.textDamage.end());
	  textParams.fontName = settings.fontName;
	  
	  textParams.textColor.f[0] = settings.textColor.x;
	  textParams.textColor.f[1] = settings.textColor.y;
	  textParams.textColor.f[2] = settings.textColor.z;
	  textParams.textColor.f[3] = settings.textColor.w;
	  
	  textParams.effectColor.f[0] = settings.outlineColor.x;
	  textParams.effectColor.f[1] = settings.outlineColor.y;
	  textParams.effectColor.f[2] = settings.outlineColor.z;
	  textParams.effectColor.f[3] = settings.outlineColor.w;
	  textParams.hasOutline = true;
	  if (CEntity * eCamera = EngineLogicManager.getOutputCamera()) {
		  if (TCompCamera * cameraComp = eCamera->getComponent<TCompCamera>()) {
			  VEC3* screenSpace = new VEC3;
			  cameraComp->getScreenCoordsOfWorldCoord(settings.targetWorldPosition, screenSpace);
			  textParams.position.x = screenSpace->x + settings.offset.x;
			  textParams.position.y = screenSpace->y + settings.offset.y;
		  }
	  }
	  if (settings.isCritical)
		  textParams.scale = VEC2(settings.scale, settings.scale);
	  CTextFont* textFont = new CTextFont;
	  textFont->setTextParams(textParams);
	  textFont->_name = settings.textDamage + std::to_string(fTextIdx);
	  textFont->_alias = settings.textDamage + std::to_string(fTextIdx++);
	  registerAlias(textFont);
	  textFont->updateTransform();
	  registerWidget(textFont);
	  activateWidget(textFont->getName());
	  textFont->loadAnimations(Utils::loadJson("data/ui/animations/FloatingDamageCenter.json"));
	  textFont->loadAnimations(Utils::loadJson("data/ui/animations/FloatingDamageLeft.json"));
	  textFont->loadAnimations(Utils::loadJson("data/ui/animations/FloatingDamageRight.json"));
	  textFont->playAnimation(settings.animationName, false);
	  textFont->getCLock()->setTime(settings.aliveTime);
	  textFont->getCLock()->start();
	  textFont->setIsKillAfterClockFinish(true);
	  
	  return textFont;
  }

  void CModuleUI::registerAlias(CWidget* widget)
  {
    assert(_registeredAlias.find(widget->getAlias()) == _registeredAlias.end());

    _registeredAlias[widget->getAlias()] = widget;
  }
 
  void CModuleUI::activateWidget(const std::string& name)
  {
		auto it = std::find_if(_activeWidgets.begin(), _activeWidgets.end(), [&name](const CWidget* widget) {
			return widget->getName() == name;
		});
		if (it == _activeWidgets.end()) {
			CWidget* widget = getWidgetByName(name);
			if (widget) {
				_activeWidgets.push_back(widget);

				widget->start();
			}
		}
  }

  void CModuleUI::deactivateWidget(const std::string& name)
  {
    auto it = std::find_if(_activeWidgets.begin(), _activeWidgets.end(), [&name](const CWidget* widget)
      {
        return widget->getName() == name;
      });
    if (it != _activeWidgets.end())
    {
      (*it)->stop();

      _activeWidgets.erase(it);
    }
  }

  void CModuleUI::deleteWidget(CWidget* widget)
  {
	  deactivateWidget(widget->getName());
	  _registeredWidgets.erase(widget->getName());
	  _registeredAlias.erase(widget->getName());
	  _toRemoveWidgets.push_back(widget);
  }

  void CModuleUI::setChildWidgetVisibleByTime(const std::string childName, const std::string parentName, bool status, float time)
  {
	  CWidget* parent = getWidgetByName(parentName);

	  if (!parent) return;

	  for (auto& child : parent->getChildren()) {
		  if (child->getName() == childName) {
			  child->getParams()->visible = status;
			  child->startClock(time);
		  }
	  }
  }


  void CModuleUI::registerController(CController* controller)
  {
    _activeControllers.push_back(controller);
  }

  void CModuleUI::unregisterController(CController* controller)
  {
	  auto endIt = std::remove_if(_activeControllers.begin(), _activeControllers.end(), [controller](const UI::CController* con) {
		  return controller == con;
		  });
	  _activeControllers.erase(endIt, _activeControllers.end());
  }

  void CModuleUI::initFonts() {
	  // Prepare the spriteBatch
	  spriteBatch = std::make_unique<DirectX::SpriteBatch>(Render.ctx);

	  // Loop through the fonts folder loading them
	  for (auto& file : std::filesystem::recursive_directory_iterator("data/fonts/")) {

		  // Get each extension and filter them if not .spritefont
		  std::string ext = file.path().extension().generic_string();
		  std::transform(ext.begin(), ext.end(), ext.begin(), ::toupper);
		  if (ext == ".SPRITEFONT") {
			  // Get the path (with wchar too), name of font
			  std::string path = file.path().generic_string();
			  std::wstring wide_path = std::wstring(path.begin(), path.end());
			  const wchar_t* wPath = wide_path.c_str();
			  size_t pos = path.find_last_of('/', path.size());
			  std::string fontName = path.substr(pos + 1, path.size() - pos - 12); // Apart from the .spritefront we need to remove one last '/'

			  // Finally insert the pair to the map (font name, spriteFont)
			  spriteFonts.insert(std::pair<std::string, std::unique_ptr<DirectX::SpriteFont>>(fontName, std::make_unique<DirectX::SpriteFont>(Render.device, wPath)));
		  }
	  }

	  //Set default font if any in the map and the ui.json file
	  if (defaultFont != "")
		  changeDefaultFont(defaultFont); // It will override itself with the same value, but will check if exists
	  else {
		  if (spriteFonts.begin() != spriteFonts.end())
			  changeDefaultFont(spriteFonts.begin()->first); // If default font not defined, first element of the map
		  else
			  Utils::error("[CModuleUI::initFonts] No fonts loaded and defaultFont not set.");
	  }
		  
  }

  void CModuleUI::changeDefaultFont(std::string newDefaultFontName) {
	  if (spriteFonts.find(newDefaultFontName) != spriteFonts.end())
		  defaultFont = newDefaultFontName;
	  else
		  Utils::error(("[CModuleUI::changeDefaultFont] Warning, cannot change default font because it doesn't exist with name " + newDefaultFontName).c_str());
  }

  CWidget* CModuleUI::getWidgetByName(const std::string& name)
  {
    auto it = _registeredWidgets.find(name);
    return it != _registeredWidgets.end() ? it->second : nullptr;
  }

  CWidget* CModuleUI::getWidgetByAlias(const std::string& name)
  {
    auto it = _registeredAlias.find(name);
    return it != _registeredAlias.end() ? it->second : nullptr;
  }

  bool CModuleUI::findWidgetByName(CWidget& widget, const std::string name, CWidget* parent)
  {
	  bool found = false;
	  std::vector<CWidget*> searchArray;

	  if (parent == nullptr)
		  searchArray = _activeWidgets;
	  else
		  searchArray = parent->getChildren();

      for (auto& w : searchArray) {
		if (w->getName() == name) {
			widget = *w;
			return true;
		}
		else {
			if (!w->getChildren().empty()) {
				found = findWidgetByName(widget, name, w);
				if (found) break;
			}
		}
	  }

	  return found;
  }

  CImage* CModuleUI::getImageWidgetByName(std::string parentName, std::string widgetName)
  {
	  CWidget widget;
	  if (findWidgetByName(widget, parentName)) {
		  for (auto& child : widget.getChildren()) {
			  if (CImage * imageChild = dynamic_cast<CImage*>(child)) {
				  if (imageChild->getName() == widgetName) {
					  return imageChild;
				  }
			  }
		  }
	  }
	  return nullptr;
  }

  CButton* CModuleUI::getButtonWidgetByName(std::string parentName, std::string widgetName)
  {
	  CWidget widget;
	  if (findWidgetByName(widget, parentName)) {
		  for (auto& child : widget.getChildren()) {
			  if (CButton * imageChild = dynamic_cast<CButton*>(child)) {
				  if (imageChild->getName() == widgetName) {
					  return imageChild;
				  }
			  }
		  }
	  }
	  return nullptr;
  }

  CTextFont* CModuleUI::getFontWidgetByName(std::string parentName, std::string widgetName)
  {
	  CWidget widget;
	  if (findWidgetByName(widget, parentName)) {
		  for (auto& child : widget.getChildren()) {
			  if (CTextFont * imageChild = dynamic_cast<CTextFont*>(child)) {
				  if (imageChild->getName() == widgetName) {
					  return imageChild;
				  }
			  }
		  }
	  }
	  return nullptr;
  }

  CProgress* CModuleUI::getProgressWidgetByName(std::string parentName, std::string widgetName)
  {
	  CWidget widget;
	  if (findWidgetByName(widget, parentName)) {
		  for (auto& child : widget.getChildren()) {
			  if (CProgress * imageChild = dynamic_cast<CProgress*>(child)) {
				  if (imageChild->getName() == widgetName) {
					  return imageChild;
				  }
			  }
		  }
	  }
	  return nullptr;
  }

}
