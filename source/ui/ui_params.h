#pragma once

#include "mcv_platform.h"
#include "..\\tools\DirectXTK-jun2019\Inc\SpriteFont.h"

namespace UI
{
  struct TParams
  {
    VEC2 pivot = VEC2::Zero;
    VEC2 position = VEC2::Zero;
    VEC2 scale = VEC2::One;
    float rotation = 0.f;
    bool visible = true;
  };

  struct TImageParams
  {
    const CTexture* texture = nullptr;
    const CTexture* mask = nullptr;
    VEC2 size = VEC2::One;
    bool additive = false;
    VEC4 color = VEC4::One;
    VEC2 minUV = VEC2::Zero;
    VEC2 maxUV = VEC2::One;
	Vector2 frameSize = Vector2::One;
	Vector2 nFrames = Vector2::Zero;
	float frameDuration = 1.0f;
	std::vector<std::string> animationPaths;
	float timeRatio = 1.0f;
	float currentTime = 0.0f;
	bool isAnimationLooped = true;
  };

  struct TTextParams
  {
    std::string text;
    const CTexture* texture = nullptr;
    VEC2 size = VEC2::One;
  };

  struct TTextFontParams
  {
	  std::wstring text = L"";
	  std::string fontName = "";
	  Vector2 position = Vector2::Zero;
	  Vector2 scale = Vector2::One;
	  float rotation = 0.0f;
	  DirectX::XMVECTORF32 textColor = DirectX::Colors::White;
	  DirectX::XMVECTORF32 effectColor = DirectX::Colors::Black;
	  bool hasOutline = false;
	  bool hasDropShadow = false;
	  std::vector<std::string> animationPaths;
	  float timeRatio = 1.0f;
	  float currentTime = 0.0f;
	  bool isAnimationLooped = true;
  };

  struct TProgressParams
  {
    float ratio = 1.f;
    //const std::string varName;
  };
}
