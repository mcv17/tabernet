#pragma once

#include "mcv_platform.h"
#include "ui/ui_params.h"
#include "utils/time.h"


namespace UI
{

#define MENUTEXT(text) ImGui::Text(text);

  class CEffect;

  class CWidget
  {

  public:
	  
	CWidget() {}
	CWidget(std::string name) : _name(name) {}

    const std::string& getName() const { return _name; }
    const std::string& getAlias() const { return _alias; }
	const std::vector<CWidget*> getChildren() const { return _children; }
    void doRender();
    void start();
    void stop();

    virtual void update(float dt);
    virtual void render() {}
	virtual void init() {}

    virtual TParams* getParams() { return &_params; }
    virtual TImageParams* getImageParams() { return nullptr; }

    void updateTransform();
    void setParent(CWidget* parent);
    void removeFromParent();
	virtual void renderInMenu();
	void startClock(float time, bool killWidget = false);
	bool getIsKillAfterClockFinish() { return _killAfterClockFinish;}
	void setIsKillAfterClockFinish(bool kill) { _killAfterClockFinish = kill;}
	CClock* getCLock() { return &_aliveClock;}
	bool needsToResetRenderConfig() { return _resetRenderConfig; };

    std::string _name;
    std::string _alias;
  protected:
    void computePivot();
    void computeLocal();
    void computeAbsolute();
	void addEffect(std::string effectType);
	void addChild(std::string name, std::string type);

    TParams _params;
    MAT44 _local;
    MAT44 _pivot;
    MAT44 _absolute;
    CWidget* _parent = nullptr;
    std::vector<CWidget*> _children;
    std::vector<CEffect*> _effects;

	CClock _aliveClock;
	bool _killAfterClockFinish = false;
	bool _resetRenderConfig = false;

    friend class CParser;
  };

 
}
