#pragma once

#include "mcv_platform.h"
#include "ui/ui_utils.h"
#include "render/textures/texture.h"
#include "engine.h"
#include "module_ui.h"

namespace UI
{
  void getColorPalette() {
	  bool open_popup = ImGui::ColorButton("MyColor##3b", color, misc_flags);
  }

  void initPalette() {
	  if (!saved_palette_inited)
		  for (int n = 0; n < IM_ARRAYSIZE(saved_palette); n++)
		  {
			  ImGui::ColorConvertHSVtoRGB(n / 31.0f, 0.8f, 0.8f, saved_palette[n].x, saved_palette[n].y, saved_palette[n].z);
			  saved_palette[n].w = 1.0f; // Alpha
		  }
	  saved_palette_inited = true;
  }
	void renderBitmap(const MAT44& world, const CTexture* texture, const Vector2& minUV, const Vector2& maxUV, const Vector4& color, bool additive, Vector2 frameSize, Vector2 nFrames, float timeRatio /*float frameDuration*/) 
	{
		PROFILE_FUNCTION("renderBitmap");
		
		// The commented line and the line below are equal for 16:9 aspect ratio, but the uncommented line doesn't depend on the aspect ratio
		// MAT44 adjust = MAT44::CreateScale((16.0f / 9.0f) / Render.width, 1.f / Render.height, 1.f);
		MAT44 adjust = MAT44::CreateScale(1.0f / Render.height, 1.0f / Render.height, 1.0f);
		activateObject(world * adjust);

		ctes_ui.UIminUV = minUV;
		ctes_ui.UImaxUV = maxUV;
		ctes_ui.UItint = color;
		ctes_ui.UIframeSize = frameSize;
		ctes_ui.UInframes = nFrames;
		ctes_ui.UItimeRatio = timeRatio;
		ctes_ui.activate();
		ctes_ui.updateGPU();

		const std::string& combinative_tech = "ui.tech";
		const std::string& additive_tech = "ui_additive.tech";
		const std::string& tech_name = additive ? additive_tech : combinative_tech;

		auto* tech = EngineResources.getResource(tech_name)->as<CTechnique>();
		assert(tech);
		tech->activate(true);
		if (texture)
			texture->activate(TS_ALBEDO);
		auto* mesh = EngineResources.getResource("unit_plane_xy.mesh")->as<CMesh>();
		mesh->activateAndRender();
	}

	void renderMasked(const CTexture* texture, const CTexture* mask, const Vector2& minUV, const Vector2& maxUV, const Vector2& minMaskUV, const Vector2& maxMaskUV, const Vector4& color, bool additive, Vector2 frameSize, Vector2 nFrames, float timeRatio) {
		PROFILE_FUNCTION("renderMasked");

		{
			Matrix adjust = Matrix::CreateScale(static_cast<float>(Render.width) / static_cast<float>(Render.height), 1.0f, 1.0f);
			activateObject(adjust);

			ctes_ui.UIminUV = minUV;
			ctes_ui.UImaxUV = maxUV;
			ctes_ui.UItint = color;
			ctes_ui.UIframeSize = frameSize;
			ctes_ui.UInframes = nFrames;
			ctes_ui.UItimeRatio = timeRatio;
			ctes_ui.activate();
			ctes_ui.updateGPU();

			const std::string& combinative_tech = "ui.tech";
			const std::string& additive_tech = "ui_additive.tech";
			const std::string& tech_name = additive ? additive_tech : combinative_tech;

			auto* tech = EngineResources.getResource(tech_name)->as<CTechnique>();
			assert(tech);
			tech->activate(true);
			if (texture)
				texture->activate(TS_ALBEDO);
			auto* mesh = EngineResources.getResource("unit_plane_xy.mesh")->as<CMesh>();
			mesh->activateAndRender();
		}

		{
			ctes_ui.UIminUV = minMaskUV;
			ctes_ui.UImaxUV = maxMaskUV;
			ctes_ui.UItint = Vector4::One;
			ctes_ui.UIframeSize = frameSize;
			ctes_ui.UInframes = nFrames;
			ctes_ui.UItimeRatio = timeRatio;
			ctes_ui.activate();
			ctes_ui.updateGPU();

			auto* tech = EngineResources.getResource("ui_masking.tech")->as<CTechnique>();
			assert(tech);
			tech->activate(true);
			if (mask)
				mask->activate(TS_ALBEDO);
			auto* mesh = EngineResources.getResource("unit_plane_xy.mesh")->as<CMesh>();
			mesh->activateAndRender();
		}
	}

	void renderText(const MAT44& transform, const CTexture* texture, const std::string& text, const VEC2& size)
	{
		PROFILE_FUNCTION("renderText");
		// assuming texture has the characters in ASCII order
		constexpr char firstCh = ' ';
		constexpr int numRows = 8;
		constexpr int numCols = 8;
		constexpr VEC2 cellSize(1.f / numCols, 1.f / numRows);

		MAT44 world = transform;
		MAT44 tr = MAT44::CreateTranslation(size.x, 0.f, 0.f);

		for (const char& ch : text)
		{
			const char cellCh = ch - firstCh;
			const int row = cellCh / numCols;
			const int col = cellCh % numCols;
			const VEC2 minUV(static_cast<float>(col) / static_cast<float>(numCols), static_cast<float>(row) / static_cast<float>(numRows));
			const VEC2 maxUV = minUV + cellSize;

			renderBitmap(world, texture, minUV, maxUV);

			world = world * tr;
		}
	}

	void renderFontTextFinal(std::wstring text, Vector2 screenPos, Vector2 origin, DirectX::XMVECTORF32 textColor, Vector2 scale, float rotation, std::string fontName) {
		PROFILE_FUNCTION("renderFontTextFinal");

		// Render the text
		UI::spriteBatch->Begin();
		UI::spriteFonts[fontName]->DrawString(UI::spriteBatch.get(), text.c_str(), screenPos, textColor, rotation, origin, scale);
		UI::spriteBatch->End();
	}

	void renderFontText(std::wstring text, Vector2 screenPos, DirectX::XMVECTORF32 textColor, Vector2 scale, float rotation, std::string fontName) {
		PROFILE_FUNCTION("renderFontText");

		// Check if font defined, if not use default font
		fontName = checkFontName(fontName);

		// Calculate the origin with the size of the string
		Vector2 outputSize = UI::spriteFonts[fontName]->MeasureString(text.c_str());
		Vector2 origin = Vector2(outputSize.x / 2.0f, outputSize.y / 2.0f);

		renderFontTextFinal(text, screenPos, origin, textColor, scale, rotation, fontName);
	}

	void renderFontTextWithOutline(std::wstring text, Vector2 screenPos, DirectX::XMVECTORF32 textColor, DirectX::XMVECTORF32 outlineColor, Vector2 scale, float rotation, std::string fontName) {
		PROFILE_FUNCTION("renderFontTextWithOutline");

		// Check if font defined, if not use default font
		fontName = checkFontName(fontName);

		// Calculate the origin with the size of the string
		Vector2 outputSize = UI::spriteFonts[fontName]->MeasureString(text.c_str());
		Vector2 origin = Vector2(outputSize.x / 2.0f, outputSize.y / 2.0f);

		// Draw the effect
		renderFontTextFinal(text, screenPos + Vector2(1.f ,  1.f), origin, outlineColor, scale, rotation, fontName);
		renderFontTextFinal(text, screenPos + Vector2(-1.f,  1.f), origin, outlineColor, scale, rotation, fontName);
		renderFontTextFinal(text, screenPos + Vector2(-1.f, -1.f), origin, outlineColor, scale, rotation, fontName);
		renderFontTextFinal(text, screenPos + Vector2(1.f , -1.f), origin, outlineColor, scale, rotation, fontName);

		// Draw the final text
		renderFontTextFinal(text, screenPos, origin, textColor, scale, rotation, fontName);
	}
	void renderFontTextWithDropShadow(std::wstring text, Vector2 screenPos, DirectX::XMVECTORF32 textColor, DirectX::XMVECTORF32 shadowColor, Vector2 scale, float rotation, std::string fontName) {
		PROFILE_FUNCTION("renderFontTextWithDropShadow");

		// Check if font defined, if not use default font
		fontName = checkFontName(fontName);

		// Calculate the origin with the size of the string
		Vector2 outputSize = UI::spriteFonts[fontName]->MeasureString(text.c_str());
		Vector2 origin = Vector2(outputSize.x / 2.0f, outputSize.y / 2.0f);

		// Draw the effect
		renderFontTextFinal(text, screenPos + Vector2(1.f, 1.f), origin, shadowColor, scale, rotation, fontName);
		renderFontTextFinal(text, screenPos + Vector2(-1.f, 1.f), origin, shadowColor, scale, rotation, fontName);

		// Draw the final text
		renderFontTextFinal(text, screenPos, origin, textColor, scale, rotation, fontName);
	}

	std::string checkFontName(std::string fontName) {
		if (fontName == "")
			fontName = defaultFont;
		assert(UI::spriteFonts.find(fontName) != UI::spriteFonts.end() && "This font is not loaded. Does it exists in the bin/data/fonts folder?");
		return fontName;
	}

	Vector2 transformVector2(const Vector2 & v, const Matrix m) {
		Vector3 v3d = Vector3::Transform(Vector3(v.x, v.y, 1.0f), m);

		return Vector2(v3d.x, v3d.y);
	}

	TBoundingBox::TBoundingBox(const Vector2 & position, const Vector2 & size) {
		_startPos = position;
		_endPos = position + size;
	}

	bool TBoundingBox::isPointWithin(const Vector2 point) const {
		return (point >= _startPos && point <= _endPos);
	}
	Vector2 TBoundingBox::getPosition() {
		return _startPos;
	}
	Vector2 TBoundingBox::getSize() {
		return _endPos - _startPos;
	}
}
