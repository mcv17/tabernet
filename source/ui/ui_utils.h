#pragma once

#include "mcv_platform.h"

#include "..\\tools\DirectXTK-jun2019\Inc\SpriteBatch.h"
#include "..\\tools\DirectXTK-jun2019\Inc\SpriteFont.h"

namespace UI
{
	static bool saved_palette_inited = false;
	static ImVec4 saved_palette[32];
	static ImVec4 backup_color;
	static ImVec4 color = ImColor(114, 144, 154, 200);
	static int misc_flags = ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_HSV | ImGuiColorEditFlags_Float  | ImGuiColorEditFlags_AlphaPreviewHalf | ImGuiColorEditFlags_AlphaBar;
	void renderBitmap(const MAT44& world, const CTexture* texture, const Vector2& minUV = Vector2::Zero, const Vector2& maxUV = Vector2::One, const Vector4& color = Vector4::One, bool additive = false, Vector2 frameSize = Vector2::One, Vector2 nFrames = Vector2::Zero, float timeRatio = 1.0f/*float frameDuration = 1.0f*/);
	void renderMasked(const CTexture* texture, const CTexture* mask, const Vector2& minUV = Vector2::Zero, const Vector2& maxUV = Vector2::One, const Vector2& minMaskUV = Vector2::Zero, const Vector2& maxMaskUV = Vector2::One, const Vector4& color = Vector4::One, bool additive = false, Vector2 frameSize = Vector2::One, Vector2 nFrames = Vector2::Zero, float timeRatio = 1.0f);
	void renderText(const MAT44& world, const CTexture* texture, const std::string& text, const VEC2& size);
	void renderFontText(std::wstring text, Vector2 screenPos, DirectX::XMVECTORF32 textColor = DirectX::Colors::White, Vector2 scale = Vector2(1.0f, 1.0f), float rotation = 0.0f, std::string fontName = "");
	void renderFontTextFinal(std::wstring text, Vector2 screenPos, Vector2 origin, DirectX::XMVECTORF32 textColor = DirectX::Colors::White, Vector2 scale = Vector2(1.0f, 1.0f), float rotation = 0.0f, std::string fontName = "");
	void renderFontTextWithOutline(std::wstring text, Vector2 screenPos, DirectX::XMVECTORF32 textColor = DirectX::Colors::White, DirectX::XMVECTORF32 outlineColor = DirectX::Colors::Black, Vector2 scale = Vector2(1.0f, 1.0f), float rotation = 0.0f, std::string fontName = "");
	void renderFontTextWithDropShadow(std::wstring text, Vector2 screenPos, DirectX::XMVECTORF32 textColor = DirectX::Colors::White, DirectX::XMVECTORF32 shadowColor = DirectX::Colors::Black, Vector2 scale = Vector2(1.0f, 1.0f), float rotation = 0.0f, std::string fontName = "");

	std::string checkFontName(std::string fontName);

	Vector2 transformVector2(const Vector2& v, const Matrix m);

	class TBoundingBox {
	public:
		TBoundingBox() {}
		TBoundingBox(const Vector2& position, const Vector2& size);

		bool isPointWithin(const Vector2 point) const;
		Vector2 getPosition();
		Vector2 getSize();

	private:
		Vector2 _startPos;
		Vector2 _endPos;

	};
}
