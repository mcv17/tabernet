#pragma once

#include "mcv_platform.h"
#include "ui/ui_controller.h"

namespace UI
{
  class CButton;

  class CMenuController : public CController
  {
  public:
    using Callback = std::function<void()>;

    void update(float dt) override;

    void registerOption(CButton* button, Callback callback);
    void setCurrentOption(int idx);

  private:
    struct TOption
    {
      CButton* button = nullptr;
      Callback callback = nullptr;
    };
    std::vector<TOption> _options;
    int _currentOption = -1;
  };
}
