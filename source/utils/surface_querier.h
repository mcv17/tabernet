#pragma once

#include <map>
#include <set>

class TCompTags;

/* Contains functions used for identifying surfaces in the game. It uses the TCompTags to read the tags
present in the component and returning a surface if there is one defined for the given combination of tags.
A surface can be defined for a single or a combination of tags (no matter the order). There can not be two surfaces
with the same exact tags.*/

namespace SurfaceQuerier {
	typedef int Tag;
	typedef int SurfaceIndx;
	typedef std::string SurfaceType;

	class SurfaceQuerierManager {
		// Holds the surface id, tags necessary for it to be used.
		struct SurfaceTagInfo {
			SurfaceType surface_id;
			std::set<Tag> tagsNecessaryForSurface;
		};

		// Used when fetching surfaces.
		struct FetchedSurface {
			FetchedSurface(SurfaceIndx indx, int tagsMissing) : fetchedSurfaceIndx(indx), numTagsMissing(tagsMissing){}

			SurfaceIndx fetchedSurfaceIndx;
			int numTagsMissing; // 0 Equals, we have all tags necessary for this.
		};

		static bool SurfaceQuerierManagerLoaded;
		static std::vector<SurfaceTagInfo> surfaces; // Holds all surfaces.
		static std::map<Tag, std::vector<SurfaceIndx>> surfacesPerTag; // Holds all surfaces a tag is present in.

		SurfaceQuerierManager();
	public:
		// Loads all surfaces types from a file whose path is given as argument.
		static void Init(const std::string & surfaceTypesFilePath);
		
		// Returns a string identifying the surface type. Uses the tag component for it. Surface types are defined in
		// surfaces_game.json. Given multiple tags, it will return a surface if it founds one whose required tags are all found
		// in the tcomptags component. If not all required tags are found, it won't return them. If two surfaces have all required
		// tags fulfilled, will return the one that has a bigger number of tags required. For example S1 needs Enemy and S2 needs Enemy and Melee.
		// If both tags are present, S2 will be returned. If two surfaces with the same amount of tags are fullfilled, the first one found is returned.
		// To avoid this, get sure to add specific tags that are not ambiguous (either by using more specific tags or by adding more tags needed)
		// so you don't have two possible results for a given entity.
		static SurfaceType GetSurfaceType(TCompTags * tags);
	};
}