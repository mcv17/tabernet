#pragma once
#include "json.hpp"
#include "murmur3/murmur3.h"
#include "modules/scripting/SOL2/sol.hpp"

using json = nlohmann::json;

namespace Utils {

	struct FilePathData {
		char drive[_MAX_DRIVE];
		char dir[_MAX_DIR];
		char filename[_MAX_FNAME];
		char extension[_MAX_EXT];
		bool error = false;
	};

	bool isPressed(int key);
	
	uint32_t getID(const char* txt);
	uint32_t getID(const void* buff, size_t nbytes);

	json loadJson(const std::string & filename);
	json loadStringJson(const std::string & js);
	std::string loadTextFile(const std::string & filename);
	bool fileExists(const char* filename);
	FilePathData splitPath(const std::string& path);

	void dbg(const char* fmt, ...);
	void dbg(const std::string & txt);
	void dbg(const sol::variadic_args & va);

	void error(const char* fmt, ...);

	bool fatal(const char* fmt, ...);

}


float unitRandom();
float randomFloat(float vmin, float vmax);

#include "named_values.h"
#include "utils/time.h"
#include "utils/file_context.h"
