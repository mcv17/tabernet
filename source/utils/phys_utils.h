#pragma once
#include "modules/module_physics.h"
#include "components/common/comp_transform.h"


namespace PhysUtils {
	// Raycast used for the player shooting.
	// By default checks against all object minus triggers and the player collider.
	physx::PxRaycastBuffer shootPlayerRaycast(const VEC3 & origTransform, const VEC3 & direction, float distance,
		physx::PxU32 filters = CModulePhysics::FilterGroup::All
							^ CModulePhysics::FilterGroup::Trigger
							^ CModulePhysics::FilterGroup::Player
							^ CModulePhysics::FilterGroup::LowPoly
							^ CModulePhysics::FilterGroup::Enemy); //We now have hitboxes, when added all uncomment

	// For collusion Detection.
	physx::PxRaycastBuffer shootCameraRaycast(const VEC3 & origin, const VEC3 & direction, float distance,
		physx::PxU32 filters = CModulePhysics::FilterGroup::All 
							^ CModulePhysics::FilterGroup::Trigger
							^ CModulePhysics::FilterGroup::Player 
							^ CModulePhysics::FilterGroup::Enemy
							^ CModulePhysics::FilterGroup::HighPoly
							^ CModulePhysics::FilterGroup::EnemyHitboxes);

	// Railgun logic raycast
	physx::PxRaycastBuffer railgunRaycast(const VEC3& origin, const VEC3& direction);
	
	// Raycast used for feet. Checks only against scenario static geometry.
	physx::PxRaycastBuffer shootFootRaycast(const VEC3 & origin, const VEC3 & direction, float distance, physx::PxU32 filters = CModulePhysics::FilterGroup::All
		^ CModulePhysics::FilterGroup::Trigger
		^ CModulePhysics::FilterGroup::Player
		^ CModulePhysics::FilterGroup::Enemy
		^ CModulePhysics::FilterGroup::HighPoly
		^ CModulePhysics::FilterGroup::EnemyHitboxes);

	// Raycast used for spawning blood decals.
	physx::PxRaycastBuffer shootBloodSplatterRaycast(const VEC3 & origin, const VEC3 & direction, float distance = 10.0f, physx::PxU32 filters =
		CModulePhysics::FilterGroup::All
		^ CModulePhysics::FilterGroup::Trigger
		^ CModulePhysics::FilterGroup::Player
		^ CModulePhysics::FilterGroup::Enemy
		^ CModulePhysics::FilterGroup::LowPoly
		^ CModulePhysics::FilterGroup::EnemyHitboxes);

	// Default raycast, checks against static geometry.
	physx::PxRaycastBuffer raycast(VEC3 origPosition, VEC3 cameraDirection, float distance, std::string filterName = "all");

	physx::PxSweepBuffer sweep(VEC3 origPosition, VEC3 direction, float distance, physx::PxGeometry & shape, 
														 physx::PxU32 filters = CModulePhysics::FilterGroup::All);

	physx::PxRaycastBuffer raycastDynamic(const VEC3 & origin, const VEC3 & direction, float distance, physx::PxU32 filters);
	
	// Throw a spherical overlap and return the entities hit 
	std::vector<CHandle> sphericalOverlap(TCompTransform* trans, float radius);

	// Check if anything is intersecting between orig and dest
	bool intersects(VEC3 orig, VEC3 dest, float yOffset);
}
