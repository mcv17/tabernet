#pragma once
#include "utils/rng.h"

// ----------------------------------------------------------
// Holds a global with the elapsed/unscaled and current time
struct TElapsedTime {
	float  delta = 0.f;
	double current = 0.f;
	float  scale_factor = 1.0f;
	float  delta_unscaled = 0.f;
	double accumulated_delta = 0.0;
	double accumulated_delta_unscaled = 0.0;

	void set(double new_time) {
		delta_unscaled = (float)(new_time - current);
		current = new_time;
		delta = delta_unscaled * scale_factor;
	}

};

extern TElapsedTime Time;

// ----------------------------------------------------------
// To compute time elapsed between ctor and elapsed()
class CTimer {
  uint64_t time_stamp;
public:
  CTimer() {
    reset();
  }

  // Ticks!
  uint64_t getTimeStamp() const {
    uint64_t now;
    ::QueryPerformanceCounter((LARGE_INTEGER*)&now);
    return now;
  }

  float elapsed() {
    uint64_t now = getTimeStamp();
    uint64_t delta_ticks = now - time_stamp;

    LARGE_INTEGER freq;
    if (::QueryPerformanceFrequency(&freq)) {
      float delta_secs = (float)(delta_ticks) / (float)freq.QuadPart;
      return delta_secs;
    }
    Utils::fatal("QueryPerformanceFrequency returned false!!!\n");
    return 0.f;
  }

  // Reset counter to current timestamp
  void reset() {
    time_stamp = getTimeStamp();
  }

  float elapsedAndReset() {
    float delta = elapsed();
    reset();
    return delta;
  }

};

class IClock {

public:

	virtual void setTime(float time) = 0;
	virtual void start() = 0;
	virtual void stop() = 0;
	virtual void pause() = 0;
	virtual void resume() = 0;

	virtual bool isStarted() const = 0;
	virtual bool isStopped() const = 0;
	virtual bool isPaused() const = 0;
	virtual bool isFinished() const = 0;

	virtual float initialTime() const = 0;
	virtual float elapsed() const = 0;
	virtual float remaining() const = 0;

};

// ----------------------------------------------------------
//	Timer that scales with delta time from Logic Manager
class CClock : public IClock {

public:

	CClock(float goal = 0.0f, bool started = false) {
		setTime(goal);
		if (started)
			start();
	}

	CClock(float minGoal, float maxGoal) {
		assert(minGoal > 0.0f && maxGoal > 0.0f && "The goal values must be positive");
		setRandomTime(minGoal, maxGoal);
	}

	// Change goal
	void setTime(float time) {
		_goal = time;
		_isRandom = false;
	}

	// Change random goal
	void setRandomTime(float minTime, float maxTime) {
		_minGoal = minTime;
		_maxGoal = maxTime;
		_isRandom = true;
		getNewRandomGoal();
	}

	double getTimeStamp() const {
		return Time.accumulated_delta;
	}

	//The various clock actions
	void start() {
		//Start the timer
		_started = true;
		//Unpause the timer
		_paused = false;
		//Get the current clock time
		_startTime = getTimeStamp();
		_pausedTime = 0.0f;

		//If random generate a new random goal
		if (_isRandom)
			getNewRandomGoal();
	}

	void stop() {
		//Stop the timer
		_started = false;
		//Unpause the timer
		_paused = false;
		//Clear tick variables
		_startTime = 0.0;
		_pausedTime = 0.0f;
	}

	void pause() {
		//If the timer is running and isn't already paused
		if (_started && !_paused) {
			//Pause the timer
			_paused = true;

			//Calculate the paused ticks
			_pausedTime = static_cast<float>(getTimeStamp() - _startTime);
			_startTime = 0.0;
		}
	}

	void resume() {
		//If the timer is running and paused
		if (_started && _paused) {
			//Unpause the timer
			_paused = false;
			//Reset the starting ticks
			_startTime = getTimeStamp() - _pausedTime;
			//Reset the paused ticks
			_pausedTime = 0.0f;
		}
	}

	bool isStarted() const { return _started; }
	bool isStopped() const { return !_started; }
	bool isPaused() const { return _paused; }
	bool isFinished() const { return elapsed() > _goal; }

	float getPausedTime() const {
		return _pausedTime;
	}

	// time in seconds the clock has been set to
	float initialTime() const {
		return _goal;
	}

	float elapsed() const {
		//If the timer is running
		if (_started) {
			//If the timer is paused
			if (_paused) {
				//Return the number of ticks when the timer was paused
				return _pausedTime;
			}
			else {
				//Return the current time minus the start time
				return static_cast<float>(getTimeStamp() - _startTime);
			}
		}
		return 0.0f;
	}

	float remaining() const {
		return std::max(0.0f, _goal - elapsed());
	}

private:
	// The goal time at which the clock was programmed
	float _goal;
	// The min and max goal time if we want a random time with each clock.start()
	float _minGoal = 0.0f, _maxGoal = 0.0f;
	bool _isRandom = false;
	//The time at which the clock was started
	double _startTime = 0.0;
	//The amount of time that had passed when the clock was paused
	float _pausedTime = 0.0f;

	//The timer status
	bool _paused = false;
	bool _started = false;

	void getNewRandomGoal() { _goal = _minGoal + RandomNumberGenerator::getGlobalInstance().f(_maxGoal - _minGoal); }
};

/*
*	Timer that doesn't scale with delta time from Logic Manager.
*/
class CClockUnscaled : public IClock {
	//The tick at which the clock was started
	uint64_t _startTicks;
	//The amount of ticks that had passed when the clock was paused
	uint64_t _pausedTicks;
	//The amount of ticks that need to pass for the clock to finish
	uint64_t _goalTicks;

	float _goal;

	//The timer status
	bool _paused;
	bool _started;

public:

	CClockUnscaled(float goal = 0) {
		//Initialize the variables
		_startTicks = 0;
		_pausedTicks = 0;

		setTime(goal);

		_paused = false;
		_started = false;
	}

	// Ticks!
	uint64_t getTimeStamp() const {
		uint64_t now;
		::QueryPerformanceCounter((LARGE_INTEGER*)&now);
		return now;
	}

	// Change goal
	void setTime(float time) {
		_goal = time;
		_goalTicks = secondsToTicks(time);
	}

	//The various clock actions
	void start() {
		//Start the timer
		_started = true;
		//Unpause the timer
		_paused = false;
		//Get the current clock time
		_startTicks = getTimeStamp();
		_pausedTicks = 0;
	}
	void stop() {
		//Stop the timer
		_started = false;
		//Unpause the timer
		_paused = false;
		//Clear tick variables
		_startTicks = 0;
		_pausedTicks = 0;
	}
	void pause() {
		//If the timer is running and isn't already paused
		if (_started && !_paused) {
			//Pause the timer
			_paused = true;

			//Calculate the paused ticks
			_pausedTicks = getTimeStamp() - _startTicks;
			_startTicks = 0;
		}
	}
	void resume() {
		//If the timer is running and paused
		if (_started && _paused) {
			//Unpause the timer
			_paused = false;
			//Reset the starting ticks
			_startTicks = getTimeStamp() - _pausedTicks;
			//Reset the paused ticks
			_pausedTicks = 0;
		}
	}

	bool isStarted() const { return _started; }
	bool isStopped() const { return !_started; }
	bool isPaused() const { return _paused; }
	bool isFinished() const { return elapsedInTicks() >= _goalTicks; }

	// time in seconds the clock has been set to
	float initialTime() const {
		return _goal;
	}

	float elapsed() const {
		return ticksToSeconds(elapsedInTicks());
	}

	float remaining() const {
		return std::max(0.0f, ticksToSeconds(_goalTicks - elapsedInTicks()));
	}

private:
	uint64_t elapsedInTicks() const {
		//If the timer is running
		if (_started) {
			//If the timer is paused
			if (_paused) {
				//Return the number of ticks when the timer was paused
				return _pausedTicks;
			}
			else {
				//Return the current time minus the start time
				uint64_t now = getTimeStamp();
				return now - _startTicks;
			}
		}
		return 0;
	}

	float ticksToSeconds(const uint64_t ticks) const {
		LARGE_INTEGER freq;
		if (::QueryPerformanceFrequency(&freq)) {
			float delta_secs = (float)(ticks) / (float)freq.QuadPart;
			return delta_secs;
		}
		Utils::fatal("QueryPerformanceFrequency returned false!!!\n");
		return 0.f;
	}

	uint64_t secondsToTicks(const float sec) const {
		LARGE_INTEGER freq;
		if (::QueryPerformanceFrequency(&freq))
			return uint64_t(sec * (long double)freq.QuadPart);
		else
			Utils::fatal("QueryPerformanceFrequency returned false!!!\n");
		return 0;
	}

};

class CTimedEvent {

public:

	CTimedEvent() {}

	CTimedEvent(const float time, bool start = true) : _clock(time) {
		if (start)
			_clock.start();
	}

	void setTime(const float time) {
		_clock.setTime(time);
	}

	void start() {
		_clock.start();
	}

	void pause() {
		_clock.pause();
	}

	void resume() {
		_clock.resume();
	}

	bool isStarted() const { return _clock.isStarted(); }
	bool isStopped() const { return _clock.isStopped(); }
	bool isPaused() const { return _clock.isPaused(); }
	bool isFinished() const { return _clock.isFinished(); }

	template <typename T, typename FnType, typename ... Args>
	inline auto doMethodEvent(T * instance, FnType fn, Args && ... args) {
		if (_clock.isFinished()) {
			_clock.start();
			return std::invoke(fn, instance, std::forward<Args>(args) ...);
		}
	}

	template <typename T, typename FnType, typename RetType, typename ... Args>
	inline RetType doMethodFetcher(T * instance, FnType fn, RetType & defaultReturn, Args && ... args) {
		if (_clock.isFinished()) {
			_clock.start();
			return std::invoke(fn, instance, std::forward<Args>(args) ...);
		}
		return defaultReturn;
	}

	template <typename FnType, typename ... Args>
	inline auto doEvent(FnType & fn, Args && ... args) {
		if (_clock.isFinished()) {
			_clock.start();
			return fn(std::forward<Args>(args) ...);
		}
	}

	template <typename FnType, typename RetType, typename ... Args>
	inline RetType doFetcher(FnType fn, RetType & defaultReturn, Args && ... args) {
		if (_clock.isFinished()) {
			_clock.start();
			return fn(std::forward<Args>(args) ...);
		}
		return defaultReturn;
	}

	const CClock& getClock() const {
		return _clock;
	}

private:
	CClock _clock;

};