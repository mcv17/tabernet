#pragma once

#include "mcv_platform.h"

template <typename VAL>
struct TTrack {
	struct TKeyframe {
		float time;
		VAL   value;
	};

private:
	std::vector<TKeyframe>  keyframes;
	bool                    sorted;
	VAL                     defaultValue;

public:
	TTrack() : sorted(false) { }

	void setDefault(VAL value)
	{
		defaultValue = value;
	}

	VAL & getDefault() { return defaultValue; }

	void clearKeyframes() { keyframes.clear(); }

	void set(float t, VAL value) {
		TKeyframe k;
		k.time = t;
		k.value = value;
		keyframes.push_back(k);
		sort();
	}

	void remove(float t) {
		auto endIt = std::remove_if(keyframes.begin(), keyframes.end(), [t](const TKeyframe& keyframe) {
			return keyframe.time == t;
			});
		keyframes.erase(endIt, keyframes.end());
	}

	VAL get(float t) {
		//if (!sorted)
		//  sort();

		if (keyframes.empty()) {
			return defaultValue;
		}
		else if (keyframes.size() == 1) {
			return keyframes[0].value;
		}
		else {
			TKeyframe curr = keyframes[0];
			for (unsigned i = 1; i < keyframes.size(); ++i) {
				const TKeyframe& next = keyframes[i];
				if (next.time > t) {
					float t_ratio = (t - curr.time) / (next.time - curr.time);
					VAL v_ratio = curr.value + (next.value - curr.value) * t_ratio;
					return v_ratio;
				}
				else {
					curr = next;
				}
			}
			return curr.value;
		}
	}

	void sort() {
		auto sorter = [](const TKeyframe& k1, const TKeyframe& k2) {
			return k1.time < k2.time;
		};
		std::sort(keyframes.begin(), keyframes.end(), sorter);
		sorted = true;
	}

	std::vector<TKeyframe>& getKeyframes() {
		return keyframes;
	}

	void commonRenderInMenu() {
		if (ImGui::Button("Sort keyframes by time"))
			sort();
	}

	template<typename T> bool renderInMenu() {}
	template<typename T> bool renderInMenuSizeMode() {}
	template<typename T> bool renderInMenuSizeModeDynamic() {}

	template<>
	bool renderInMenu<Vector4>() {
		bool changed = false;
		commonRenderInMenu();
		ImGui::SameLine();
		if (ImGui::Button("Add keyframe"))
			set(1.0f, Vector4(1.0f, 1.0f, 1.0f, 1.0f));
		ImGui::Separator();
		if (keyframes.size() == 0) {
			changed = ImGui::ColorEdit4("Default value", static_cast<float*>(&defaultValue.x), ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
		}
		else {
			int i = 0;
			for (auto& kf : keyframes) {
				ImGui::PushID(i++);
				if (ImGui::Button("Delete this keyframe"))
					remove(kf.time);
				changed |= (ImGui::DragFloat("Time (0..1)", &kf.time, 0.01f, 0.0f, 1.0f));
				changed |= (ImGui::ColorEdit4("Color", static_cast<float*>(&kf.value.x), ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR));
				ImGui::Separator();
				ImGui::PopID();
			}
		}
		if (changed)
			sort();
		return changed;
	}

	template<>
	bool renderInMenu<VEC3>() {
		bool changed = false;
		commonRenderInMenu();
		ImGui::SameLine();
		if (ImGui::Button("Add keyframe"))
			set(1.0f, VEC3(1.0f, 1.0f, 1.0f));
		ImGui::Separator();
		if (keyframes.size() == 0) {
			ImGui::DragFloat("Default value", &defaultValue.x, 0.01f, 0.0f, 10.0f);
		}
		else {
			int i = 0;
			for (auto& kf : keyframes) {
				ImGui::PushID(i++);
				if (ImGui::Button("Delete this keyframe"))
					remove(kf.time);
				changed |= (ImGui::DragFloat("Time (0..1)", &kf.time, 0.01f, 0.0f, 1.0f));
				changed |= (ImGui::DragFloat("Size", static_cast<float*>(&kf.value.x), 0.01f, 0.0f, 10.0f));
				ImGui::Separator();
				ImGui::PopID();
			}
		}
		if (changed)
			sort();
		return changed;
	}

	template<>
	bool renderInMenuSizeMode<VEC3>() {
		bool changed = false;
		commonRenderInMenu();
		ImGui::SameLine();
		if (keyframes.size() == 0) {
			if (ImGui::Button("Add keyframe"))
				set(1.0f, VEC3(defaultValue.x, defaultValue.y, 1.0f));
		}
		ImGui::Separator();
		if (keyframes.size() == 0) {
			changed |= (ImGui::DragFloat("Starting min random size value", static_cast<float*>(&defaultValue.x), 0.01f, 0.0f, 10.0f));
			changed |= (ImGui::DragFloat("Starting max random size value", static_cast<float*>(&defaultValue.y), 0.01f, 0.0f, 10.0f));
		}
		else {
			int i = 0;
			for (auto& kf : keyframes) {
				ImGui::PushID(i++);
				if (ImGui::Button("Delete this keyframe"))
					remove(kf.time);
				
				if (i != 1)
					changed |= (ImGui::DragFloat("Time (0..1)", &kf.time, 0.01f, 0.0f, 1.0f));
				else{
					changed |= (ImGui::DragFloat("Starting min random size value", static_cast<float*>(&kf.value.x), 0.01f, 0.0f, 10.0f));
					changed |= (ImGui::DragFloat("Starting max random size value", static_cast<float*>(&kf.value.y), 0.01f, 0.0f, 10.0f));
				}
				changed |= (ImGui::DragFloat("Size modifier", static_cast<float*>(&kf.value.z), 0.01f, -100.0f, 100.0f));
				ImGui::Separator();
				ImGui::PopID();
			}
		}
		if (changed)
			sort();
		return changed;
	}

	template<>
	bool renderInMenuSizeModeDynamic<VEC3>() {
		bool changed = false;
		commonRenderInMenu();
		ImGui::SameLine();
		if (keyframes.size() == 0) {
			if (ImGui::Button("Add keyframe"))
				set(1.0f, VEC3(1.0f, 1.0f, 1.0f));
		}
		else {
			if (ImGui::Button("Add keyframe, size change with time."))
				set(1.0f, VEC3(1.0f, 1.0f, 1.0f));
		}
		ImGui::Separator();
		if (keyframes.size() == 0) {
			changed |= (ImGui::DragFloat("Starting min random size value", static_cast<float*>(&defaultValue.x), 0.01f, 0.0f, 10.0f));
			changed |= (ImGui::DragFloat("Starting max random size value", static_cast<float*>(&defaultValue.y), 0.01f, 0.0f, 10.0f));
			changed |= (ImGui::DragFloat("Size modifier with time", static_cast<float*>(&defaultValue.z), 0.01f, 0.0f, 10.0f));
		}
		else {
			int i = 0;
			for (auto& kf : keyframes) {
				ImGui::PushID(i++);
				if (ImGui::Button("Delete this keyframe"))
					remove(kf.time);
				changed |= (ImGui::DragFloat("Time (0..1)", &kf.time, 0.01f, 0.0f, 1.0f));
				changed |= (ImGui::DragFloat("Min random size value", static_cast<float*>(&kf.value.x), 0.01f, 0.0f, 10.0f));
				changed |= (ImGui::DragFloat("Max random size value", static_cast<float*>(&kf.value.y), 0.01f, 0.0f, 10.0f));
				changed |= (ImGui::DragFloat("Size modifier with time", static_cast<float*>(&kf.value.z), 0.01f, 0.0f, 10.0f));
				ImGui::Separator();
				ImGui::PopID();
			}
		}
		if (changed)
			sort();
		return changed;
	}
};

