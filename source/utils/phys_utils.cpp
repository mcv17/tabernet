#include "mcv_platform.h"
#include "phys_utils.h"
#include "engine.h"
#include <components\common\comp_name.h>

namespace PhysUtils {

	// Raycast used for player shooting, checks with both static and dynamic objects. By default checks with all objects minus triggers and the player.
	physx::PxRaycastBuffer PhysUtils::shootPlayerRaycast(const VEC3 & origin, const VEC3 & direction, float distance, physx::PxU32 filters) {
		// Set up filter 
		physx::PxQueryFilterData filterData(physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC);
		filterData.data.word0 = filters;
		const physx::PxScene* scene = EnginePhysics.getScene();
		physx::PxRaycastBuffer hitData;
		if (!scene) return hitData;

		bool hit = scene->raycast(
			VEC3_TO_PXVEC3(origin),
			VEC3_TO_PXVEC3(direction),
			distance,
			hitData,
			physx::PxHitFlag::ePOSITION | physx::PxHitFlag::eNORMAL,
			filterData);

		return hitData;
	}

	// Raycast used for player shooting, checks with both static and dynamic objects. By default checks with all objects minus triggers and the player.
	physx::PxRaycastBuffer PhysUtils::shootCameraRaycast(const VEC3 & origin, const VEC3 & direction, float distance, physx::PxU32 filters) {
		// Set up filter 
		physx::PxQueryFilterData filterData(physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC);
		filterData.data.word0 = filters;
		const physx::PxScene* scene = EnginePhysics.getScene();
		physx::PxRaycastBuffer hitData;
		if (!scene) return hitData;

		bool hit = scene->raycast(
			VEC3_TO_PXVEC3(origin),
			VEC3_TO_PXVEC3(direction),
			distance,
			hitData,
			physx::PxHitFlag::ePOSITION | physx::PxHitFlag::eUV | physx::PxHitFlag::eNORMAL,
			filterData);

		return hitData;
	}

	// Raycast used for player shooting, checks with both static and dynamic objects. By default checks with all objects minus triggers and the player.
	physx::PxRaycastBuffer PhysUtils::shootFootRaycast(const VEC3 & origin, const VEC3 & direction, float distance, physx::PxU32 filters) {
		// Set up filter. For now we only consider dynamics.
		physx::PxQueryFilterData filterData(physx::PxQueryFlag::eSTATIC);
		filterData.data.word0 = filters;
		const physx::PxScene* scene = EnginePhysics.getScene();
		physx::PxRaycastBuffer hitData;
		if (!scene) return hitData;

		bool hit = scene->raycast(
			VEC3_TO_PXVEC3(origin),
			VEC3_TO_PXVEC3(direction),
			distance,
			hitData,
			physx::PxHitFlag::ePOSITION | physx::PxHitFlag::eNORMAL,
			filterData);

		return hitData;
	}

	// Raycast used for spawning blood decals.
	physx::PxRaycastBuffer PhysUtils::shootBloodSplatterRaycast(const VEC3 & origin, const VEC3 & direction, float distance, physx::PxU32 filters) {
		// Set up filter 
		physx::PxQueryFilterData filterData(physx::PxQueryFlag::eSTATIC);
		filterData.data.word0 = filters;
		const physx::PxScene* scene = EnginePhysics.getScene();
		physx::PxRaycastBuffer hitData;
		if (!scene) return hitData;

		bool hit = scene->raycast(
			VEC3_TO_PXVEC3(origin),
			VEC3_TO_PXVEC3(direction),
			distance,
			hitData,
			physx::PxHitFlag::ePOSITION | physx::PxHitFlag::eNORMAL,
			filterData);

		return hitData;
	}

	physx::PxRaycastBuffer railgunRaycast(const VEC3& origin, const VEC3& direction)
	{
		physx::PxScene* scene = EnginePhysics.getScene();
		physx::PxRaycastBuffer hitData;
		if (!scene) return hitData;

		const physx::PxHitFlags outputFlags = physx::PxHitFlag::eDISTANCE | physx::PxHitFlag::ePOSITION | physx::PxHitFlag::eNORMAL;

		// Prepare the raycast info
		physx::PxQueryFilterData filterData = physx::PxQueryFilterData();
		filterData.data.word0 = CModulePhysics::FilterGroup::Wall;
		physx::PxReal checkDistance = 20.0f;
		
		// Shoot ray from enemy hit point seeking a wall within 3m
		bool getEnemyBack = scene->raycast(
			VEC3_TO_PXVEC3(origin),
			VEC3_TO_PXVEC3(direction),
			checkDistance,
			hitData,
			outputFlags,
			filterData);
		
		return hitData;
	}

	physx::PxRaycastBuffer PhysUtils::raycastDynamic(const VEC3 & origin, const VEC3 & direction, float distance, physx::PxU32 filters)
	{
		// Set up filter 
		physx::PxQueryFilterData filterData(physx::PxQueryFlag::eDYNAMIC);
		filterData.data.word0 = filters;
		const physx::PxScene* scene = EnginePhysics.getScene();
		physx::PxRaycastBuffer hitData;
		if (!scene) return hitData;

		bool hit = scene->raycast(
			VEC3_TO_PXVEC3(origin),
			VEC3_TO_PXVEC3(direction),
			distance,
			hitData,
			physx::PxHitFlag::ePOSITION | physx::PxHitFlag::eUV,
			filterData);

		return hitData;
	}

	physx::PxRaycastBuffer PhysUtils::raycast(VEC3 origPosition, VEC3 cameraDirection, float distance, std::string filterName)
	{
		// Set up filter 
		physx::PxQueryFilterData filterData(physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC);
		filterData.data.word0 = EnginePhysics.getFilterByName(filterName);
		const physx::PxScene* scene = EnginePhysics.getScene();
		physx::PxRaycastBuffer hitData;
		if (!scene) return hitData;

		bool hit = scene->raycast(
			VEC3_TO_PXVEC3(origPosition),
			VEC3_TO_PXVEC3(cameraDirection),
			distance,
			hitData,
			physx::PxHitFlag::ePOSITION | physx::PxHitFlag::eUV,
			filterData
		);

		return hitData;
	}

	physx::PxSweepBuffer PhysUtils::sweep(VEC3 origPosition, VEC3 direction, float distance, physx::PxGeometry & shape, physx::PxU32 filters) {
		// Set up filter 
		physx::PxQueryFilterData filterData(physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC);
		filterData.data.word0 = filters;
		const physx::PxScene* scene = EnginePhysics.getScene();
		physx::PxSweepBuffer hitData;
		if (!scene) return hitData;

		auto initialPose = physx::PxTransform(VEC3_TO_PXVEC3(origPosition));

		bool hit = scene->sweep(
			shape,
			initialPose,
			VEC3_TO_PXVEC3(direction),
			distance,
			hitData,
			physx::PxHitFlag::ePOSITION | physx::PxHitFlag::eUV,
			filterData
		);

		return hitData;
	}

	// Spherical query for explosion
	std::vector<CHandle> PhysUtils::sphericalOverlap(TCompTransform* trans, float radius) {

		std::vector<CHandle> hitEntites;
		physx::PxScene* scene = EnginePhysics.getScene();
		if (!scene) return std::vector<CHandle>();

		// Set up filter 
		physx::PxQueryFilterData filterData(physx::PxQueryFlag::eDYNAMIC | physx::PxQueryFlag::eSTATIC);
		filterData.data.word0 = EnginePhysics.getFilterByName("enemy") | EnginePhysics.getFilterByName("scenario");
		
		// Set up buffer
		if (trans->getPosition().y < 0) trans->setPosition(VEC3(trans->getPosition().x, 0.0f, trans->getPosition().z));
		const physx::PxU32 bufferSize = 100;								// [in] size of 'hitBuffer'
		physx::PxOverlapHit hitBuffer[bufferSize];							// [out] User provided buffer for results
		physx::PxOverlapBuffer buf(hitBuffer, bufferSize);					// [out] Blocking and touching hits stored here
		physx::PxSphereGeometry shape = physx::PxSphereGeometry(radius);	// [in] shape to test for overlaps
		shape.radius = radius;						
		physx::PxTransform shapePose = toPxTransform(*trans);				// [in] initial shape pose (at distance=0)

		// Throw query
		bool status_ok = scene->overlap(shape, shapePose, buf, filterData);

		if (status_ok)
			for (physx::PxU32 i = 0; i < buf.nbTouches; i++) {
				// Get all hit entities
				CHandle colHandle;
				colHandle.fromVoidPtr(hitBuffer[i].actor->userData);

				if (colHandle.isValid()) {
					TCompCollider* col = colHandle;
					CHandle entityHandle = CHandle(col).getOwner();
					hitEntites.push_back(entityHandle);
				}
			}

		return hitEntites;

	}
	bool intersects(VEC3 orig, VEC3 dest, float yOffset)
	{
		const physx::PxScene* scene = EnginePhysics.getScene();
		if (!scene) return false;

		physx::PxRaycastBuffer hitData;
		VEC3 dir = dest - orig;
		dir += VEC3(0, yOffset, 0);
		dir.Normalize();

		bool hit = scene->raycast(
			VEC3_TO_PXVEC3(orig),
			VEC3_TO_PXVEC3(dir),
			VEC3::Distance(orig, dest),
			hitData
		);


		return false;
	}

	
}




