#pragma once

#include <map>

/* These classes are used for controlling the value of a given value passed as parameter.
They are mostly used by IK for controlling their effect over bones. */

namespace WeightController {

	/* Weight controllers. */

	/* Interface for weight controllers that can be implemented. */
	class IWeightController {
	protected:
		bool isActive = false;

	public:
		virtual void load(const json & j) = 0;
		virtual void update(const float dt, float & weight_to_modify) = 0;
		virtual void renderInMenu() {};
		virtual void setWeightControllerStatus(bool nActive) { isActive = nActive; }
		bool getWeightControllerStatus() const { return isActive; }
	};

	class TLinearTimeWeightController : public IWeightController {
		bool fadingOut = false;

		float current_time = 0.0f;
		float fade_in = 1.0f;
		float fade_out = 1.0f;

	public:
		void load(const json & j) override;
		void update(const float dt, float & weight_to_modify) override;
		void renderInMenu() override;
		void setWeightControllerStatus(bool nActive) override;
	};

	// TO DO. Controls the weight for a foot ik.
	class TFootIKWeightController : public IWeightController {
	public:
		void load(const json & j) override{}
		void update(const float dt, float & weight_to_modify) override{}
	};

	/* Weight controller factories and manager. */

	class IControllerFactory {
	public:
		virtual IWeightController * create() = 0;
	};

	template<class WeightControllerType>
	class WeightControllerFactory : public IControllerFactory {
	public:
		IWeightController * create() override {
			return new WeightControllerType();
		}
	};


	// Weight controller manager, manages the creation of weight controllers.
	class WeightControllerManager {
		bool loaded = false;
		std::map<std::string, IControllerFactory *> weightControllerFactories;

	public:
		static WeightControllerManager & Instance() {
			static WeightControllerManager instance;
			instance.start();
			return instance;
		}

		~WeightControllerManager() {
			for (auto & weightController : weightControllerFactories) {
				delete weightController.second;
				weightController.second = nullptr;
			}
		}

		IWeightController * createState(const std::string & type);

	private:

		void start();
	};

};