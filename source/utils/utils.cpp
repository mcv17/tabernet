#include "utils.h"
#include "mcv_platform.h"
#include <fstream>
#include "file_context.h"

// utils/time.h
TElapsedTime Time;

// utils/file_context
std::vector< const std::string* > TFileContext::files;

bool Utils::isPressed(int key)
{
	return ::GetAsyncKeyState(key) & 0x8000;
}

void Utils::dbg(const char* format, ...) {
	va_list argptr;
	va_start(argptr, format);
	char dest[1024 * 16];
	_vsnprintf(dest, sizeof(dest), format, argptr);
	va_end(argptr);
	::OutputDebugString(dest);
}

void Utils::dbg(const std::string & txt) {
	::OutputDebugString(txt.c_str());
}

void Utils::dbg(const sol::variadic_args & va) {
	std::string deb = "";
	for (auto& elem : va) {
		deb += elem.as<std::string>();
	}
	deb += "\n";

	::OutputDebugString(deb.c_str());
}

void Utils::error(const char* format, ...) {
	va_list argptr;
	va_start(argptr, format);
	char dest[1024 * 16];
	_vsnprintf(dest, sizeof(dest), format, argptr);
	va_end(argptr);
	::OutputDebugString(dest);

	std::string file_context = TFileContext::getFileContextStack();
	if (!file_context.empty()) {
		strcat(dest, "Files context:\n");
		strcat(dest, file_context.c_str());
	}

#ifdef NDEBUG
	MessageBox(nullptr, dest, "Critical error!", MB_OK);
#else
	strcat(dest, "\n\nContinuing execution may be unsafe\nWould you like to continue execution?");
	if (MessageBox(nullptr, dest, "Critical error!", MB_YESNO) == IDNO)
#endif
		exit(-1);
}

bool Utils::fatal(const char* format, ...) {
  va_list argptr;
  va_start(argptr, format);
  char dest[1024 * 16];
  _vsnprintf(dest, sizeof(dest), format, argptr);
  va_end(argptr);
  ::OutputDebugString(dest);

  std::string file_context = TFileContext::getFileContextStack();
  if (!file_context.empty()) {
    strcat(dest, "Files context:\n");
    strcat(dest, file_context.c_str());
  }

  if (MessageBox(nullptr, dest, "Error!", MB_RETRYCANCEL) == IDCANCEL)
    exit(-1);
  return false;
}

// --------------------------------------------------------
json Utils::loadJson(const std::string& filename) {
	json j;
	PROFILE_FUNCTION_COPY_TEXT(filename.c_str());
	while (true) {
		
		std::ifstream ifs(filename.c_str());
		if (!ifs.is_open()) {
			fatal("Failed to open json file %s\n", filename.c_str());
			continue;
		}

#ifdef NDEBUG
		j = json::parse(ifs, nullptr, false);
		if (j.is_discarded()) {
			ifs.close();
			fatal("Failed to parse json file %s\n", filename.c_str());
			continue;
		}
#else

    try
    {
      // parsing input with a syntax error
      j = json::parse(ifs);
    }
    catch (json::parse_error& e)
    {
      ifs.close();
      // output exception information
      fatal("Failed to parse json file %s\n%s\nAt offset: %d\n"
        , filename.c_str(), e.what(), e.byte);
      continue;
    }

#endif
		// The json is correct, we can leave the while loop
		break;
	}
	
	return j;
}

json Utils::loadStringJson(const std::string & js) {
	json j;

#ifdef NDEBUG
	j = json::parse(js, nullptr, false);
#else
	j = json::parse(js);
#endif

	return j;
}

std::string Utils::loadTextFile(const std::string & filename) {
	std::stringstream strStream;
	while (true) {
		std::ifstream ifs(filename);
		if (!ifs.is_open()) {
			fatal("Failed to open file %s\n", filename.c_str());
			continue;
		}

		strStream << ifs.rdbuf();
		ifs.close();
		break;
	}

	return strStream.str();
}


// generate a hash from the input buffer
uint32_t Utils::getID(const void* buff, size_t nbytes) {
  uint32_t seed = 0;
  uint32_t out_value = 0;
  MurmurHash3_x86_32(buff, (uint32_t)nbytes, seed, &out_value);
  assert(out_value != 0);
  return out_value;
}

uint32_t Utils::getID(const char* txt) {
  return getID(txt, strlen(txt));
}

bool Utils::fileExists(const char* filename) {
  FILE *f = fopen(filename, "rb");
  if (!f)
    return false;
  fclose(f);
  return true;
}

Utils::FilePathData Utils::splitPath(const std::string & path) {
	FilePathData fd;
	errno_t err = _splitpath_s(path.c_str(), fd.drive, _MAX_DRIVE, fd.dir, _MAX_DIR, fd.filename,
														 _MAX_FNAME, fd.extension, _MAX_EXT);
	fd.error = (err == 0);
	return fd;
}

// -------------------------------------------------------------
float unitRandom() {
  return (float)rand() / (float)RAND_MAX;
}

float randomFloat(float vmin, float vmax) {
  return vmin + (vmax - vmin) * unitRandom();
}

