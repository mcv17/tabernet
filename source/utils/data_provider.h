#pragma once
#include "mcv_platform.h"

/* This abstract class is used for the instantiation of classes that
provides data by reading plain old objects. */
class CDataProvider {
public:
	virtual ~CDataProvider() {}
	
	virtual bool isValid() = 0;
	
	virtual void read(void* dest, size_t num_bytes) = 0;
	
	template< class TPOD >
	void read(TPOD & pod) {
		read(&pod, sizeof(TPOD));
	}
};

/* CFileDataProvider is a class allowing for the reading of files by setting
the number of bytes to read and an array to store the data in. */
class CFileDataProvider : public CDataProvider {
	FILE* f = nullptr;

public:
  CFileDataProvider(const char* filename) {
    f = fopen(filename, "rb");
  }

  ~CFileDataProvider() {
    if (f)
      fclose(f);
    f = nullptr;
  }
  bool isValid() override {
    return f != nullptr;
  }
  void read(void* dest, size_t num_bytes) {
    auto nbytes_read = fread(dest, 1, num_bytes, f);
    assert(nbytes_read == num_bytes);
  }

  size_t current() {
    return ftell(f);
  }

  size_t fileSize() {
    fseek(f, 0L, SEEK_END);
    size_t total_sz = ftell(f);
    fseek(f, 0L, SEEK_SET);
    return total_sz;
  }

};

class CMemoryDataProvider : public CDataProvider, public std::vector< uint8_t > {
  size_t offset = 0;
public:
  CMemoryDataProvider(const char* filename) {
    FILE* f = fopen(filename, "rb");
    if (f) {
      size_t sz2 = fseek(f, 0, SEEK_END);
      auto sz = ftell(f);
      resize(sz);
      size_t sz3 = fseek(f, 0, SEEK_SET);
      uint8_t* buf = data();
      auto bytes_read = fread(buf, 1, sz, f);
      assert(sz == bytes_read);
      fclose(f);
    }
  }
  virtual bool isValid() {
    return !empty();
  }
  void read(void* dest, size_t num_bytes) {
    // Do not read beyond the allocated buffer
    assert(offset + num_bytes <= size());
    memcpy(dest, data() + offset, num_bytes);
    offset += num_bytes;
  }
};