#include "mcv_platform.h"
#include "comp_camera_corridor_trigger.h"
#include "components/common/comp_tags.h"
#include "entity/entity_parser.h"

#include "engine.h"

#include "modules/module_camera_mixer.h"
#include "geometry/interpolators.h"

DECL_OBJ_MANAGER("corridor_trigger", TCompCorridorTrigger);

void TCompCorridorTrigger::load(const json& j, TEntityParseContext& ctx) {}

void TCompCorridorTrigger::debugInMenu() {}

void TCompCorridorTrigger::registerMsgs() {
	DECL_MSG(TCompCorridorTrigger, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompCorridorTrigger, TMsgEntityTriggerExit, onTriggerExit);
}

void TCompCorridorTrigger::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	CHandle entityEntered = msg.h_entity;
	CEntity * entity = entityEntered;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags->hasTag(Utils::getID("player"))) {
		EngineLogicManager.SetEntityActive(std::string("Camera-Corridor"), true);
		EngineLogicManager.BlendCamera("Camera-Corridor", 6.0f, new Interpolator::TLinearInterpolator());
	}
}

void TCompCorridorTrigger::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	CHandle entityEntered = msg.h_entity;
	CEntity * entity = entityEntered;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags->hasTag(Utils::getID("player"))) {
		EngineLogicManager.SetEntityActive(std::string("Camera-Corridor"), false);
		EngineLogicManager.BlendCamera("Camera-Player-Output", 1.0f, new Interpolator::TLinearInterpolator());
	}
}