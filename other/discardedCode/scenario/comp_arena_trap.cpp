#include "mcv_platform.h"
#include "comp_arena_trap.h"
#include "components\common\comp_tags.h"
#include "entity/entity_parser.h"
#include "engine.h"

#include "components/common/comp_name.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_health.h"

DECL_OBJ_MANAGER("arena_trap", TCompArenaTrap);

void TCompArenaTrap::load(const json& j, TEntityParseContext& ctx) {
	enemiesToSpawn = j.value("enemies_to_spawn", enemiesToSpawn);
	timeBetweenSpawns = j.value("time_between_spawns", timeBetweenSpawns);
	maxNumEnemiesAlive = j.value("max_enemies_alive", maxNumEnemiesAlive);
	enemies.reserve(enemiesToSpawn);

	// for now we hardcode them as many of these things will probably end up scripted.
	int numSpawns = 5;
	spawnNames.reserve(numSpawns);
	spawns.reserve(numSpawns);
	spawnNames.push_back("SpawnTrampa");
	for (int i = 1; i < numSpawns + 1; i++)
		spawnNames.push_back("SpawnTrampa-" + std::to_string(i));
}

void TCompArenaTrap::debugInMenu() {
	ImGui::DragInt("Enemies to spawn: ", &enemiesToSpawn, 0, 0, 10000);
	ImGui::DragFloat("Time between spawns: ", &timeBetweenSpawns, 0.1f, 0.f, 10000.f);
	ImGui::DragInt("Max Number of Enemies Alive: ", &maxNumEnemiesAlive, 0, 0, 10000);
}

void TCompArenaTrap::registerMsgs() {
	DECL_MSG(TCompArenaTrap, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompArenaTrap, TMsgSpawnStatus, onSpawnStatus);
}

void TCompArenaTrap::update(float dt) {
	if (!active) return;

	// Check if any enemy died. We remove one for update.
	// It will be strange for more than one to die at the same time.
	checkIfAnyEnemiesDied();

	// On spawn time runs out, spawn.
	if (enemiesToSpawn > 0) {
		currentTime += dt;
		if (currentTime > timeBetweenSpawns && maxNumEnemiesAlive > currentEnemiesAlive) {
			currentTime = 0.0f;
			SpawnEnemies();
		}
	}
	else if (currentEnemiesAlive == 0) // All enemies are spawned and dead, and demon is spawned and dead.
		endArena();
}

void TCompArenaTrap::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	if (active) return;

	CEntity * entity = msg.h_entity;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags && tags->hasTag(Utils::getID("player"))) {
		active = true;
		fetchSpawns();
	}
}

void TCompArenaTrap::onSpawnStatus(const TMsgSpawnStatus & msg) {
	if (msg.occupied)
		removeSpawnFromFreeOnes(msg.spawnEntityHandle);
	else
		addSpawnToFreeOnes(msg.spawnEntityHandle);
}

void TCompArenaTrap::addSpawnToFreeOnes(CHandle spawn) {
	freeSpawns.push_back(spawn);
}

void TCompArenaTrap::removeSpawnFromFreeOnes(CHandle spawn) {
	for (auto spawnIt = freeSpawns.begin(); spawnIt != freeSpawns.end(); spawnIt++) {
		if (spawn == (*spawnIt)) {
			freeSpawns.erase(spawnIt);
			break;
		}
	}
}

void TCompArenaTrap::fetchSpawns() {
	// Name of the arena to send to spawns so they know their owner.
	TCompName * arenaName = getComponent<TCompName>();
	if (arenaName == nullptr)
		Utils::fatal("Arena doesn't have a name!");
	std::string sArenaName = arenaName->getName();

	/* Now fetch all spawns. */
	for (auto & name : spawnNames) {
		CHandle spawnHand = getEntityByName(name);
		if (!spawnHand.isValid())
			Utils::fatal("Spawn with name %s not valid.\n", name.c_str());
		CEntity * ent = spawnHand;
		TCompSpawn * spawnComp = ent->getComponent<TCompSpawn>();
		if (spawnComp == nullptr)
			Utils::fatal("Spawn with name %s doesn't have spawn component.\n", name.c_str());
		spawnComp->setSpawnOwner(sArenaName);
		spawns.push_back(spawnHand);
		freeSpawns.push_back(spawnHand);
	}
}

TCompSpawn * TCompArenaTrap::pickRandomSpawn() {
	int spawnIndx = RNG.i(static_cast<int>(spawns.size()));
	CEntity * ent = spawns[spawnIndx];
	return ent->getComponent<TCompSpawn>();
}

void TCompArenaTrap::SpawnEnemies() {
	TCompSpawn * spawn = pickRandomSpawn();
	if (!spawn) return; // All spawns are occupied or no spawn was found.

	enemies.push_back(spawn->spawnRandomEnemy());
	enemiesToSpawn--;
	currentEnemiesAlive++;
}

void TCompArenaTrap::checkIfAnyEnemiesDied() {
	auto endIt = std::remove_if(enemies.begin(), enemies.end(), [this](const CHandle & enemyHandle) {
		if (enemyHandle.isValid()) {
			if (this->checkIfEnemyDead((enemyHandle))) {
				this->currentEnemiesAlive--;
				return true;
			}
			return false;
		}
		this->currentEnemiesAlive--;
		return true;
	});
	enemies.erase(endIt, enemies.end());
}

bool TCompArenaTrap::checkIfEnemyDead(CEntity * e) {
	TCompHealth * health = e->getComponent<TCompHealth>();
	if (health)
		return health->isDead();
	return false;
}

void TCompArenaTrap::endArena() {
	// Destroy the spawns.
	for (auto & spawn : spawns)
		spawn.destroy();

	CHandle(this).destroy(); // We destroy the trigger.
}