#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "modules/module_physics.h"
#include "components/arena/comp_spawn.h"

class TCompSpawn;

class TCompArena3 : public TCompBase {
	DECL_SIBLING_ACCESS();

	// Spawn variables.
	std::vector<std::string> spawnNames;
	std::vector<CHandle> spawns;
	std::vector<CHandle> freeSpawns;

	// Doors.
	std::vector<CHandle> doors;

	// Enemies.
	std::vector<CHandle> enemies;
	CHandle demonHandle;

	// Says if the arena is active. If so, starts spawning
	// enemies. By default the player activates the arena on enters.
	// and deactivates it once all enemies are dead.
	bool active = false;
	
	/* Arena variables. */
	int currentEnemiesAlive = 0;
	int enemiesToSpawn = 40;
	float currentTime = 0.0f;
	float timeBetweenSpawns = 0.5f;
	int maxNumEnemiesAlive = 20;
	
	/* Callbacks. */
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onSpawnStatus(const TMsgSpawnStatus & msg);
	void addSpawnToFreeOnes(CHandle spawn);
	void removeSpawnFromFreeOnes(CHandle spawn);

public:
	void update(float dt);
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();

private:
	/* Creates the doors that block the player path. */
	void createDoors();

	/* Spawn functions. */
	void fetchSpawns();
	TCompSpawn * pickRandomSpawn();
	void SpawnEnemies();
	void SpawnDemon();

	/* Used for removing enemies when they die. */
	void checkIfAnyEnemiesDied();
	bool checkIfEnemyDead(CEntity * e);

	/* Ends the arena. Removes doors and spawns. */
	void endArena();
};