#include "mcv_platform.h"
#include "comp_corrupted_door.h"
#include "components/common/comp_render.h"
#include "components/common/comp_tags.h"

DECL_OBJ_MANAGER("corrupted_door", TCompCorruptedDoor);

void TCompCorruptedDoor::registerMsgs() {
	DECL_MSG(TCompCorruptedDoor, TMsgPlayerInteractionTriggerEnter, showInteractionFeedback);
	DECL_MSG(TCompCorruptedDoor, TMsgPlayerInteractionTriggerExit, hideInteractionFeedback);
	DECL_MSG(TCompCorruptedDoor, TMsgPlayerIsInteracting, onInteract);
}

void TCompCorruptedDoor::update(float dt) {}

void TCompCorruptedDoor::load(const json& j, TEntityParseContext& ctx) {}

void TCompCorruptedDoor::debugInMenu() {

}

void TCompCorruptedDoor::onInteract(const TMsgPlayerIsInteracting & msg) {
	CEntity * player = getEntityByName("Player");
	
	CHandle(this).getOwner().destroy();
}

void TCompCorruptedDoor::showInteractionFeedback(const TMsgPlayerInteractionTriggerEnter & msg) {
	TCompRender * render = getComponent<TCompRender>();
	if (!render) return;

	// TODO: Change after 1st milestone
	_tempColor = render->getColor();
	render->setColor(Vector4(1.0f, 1.0f, 0.0f, 1.0f));
}

void TCompCorruptedDoor::hideInteractionFeedback(const TMsgPlayerInteractionTriggerExit & msg) {
	TCompRender * render = getComponent<TCompRender>();
	if (!render) return;

	render->setColor(_tempColor); // TODO: Change after 1st milestone
}