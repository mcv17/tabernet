#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/player/comp_interactor.h"

class TCompCorruptedDoor : public TCompBase {

public:
	DECL_SIBLING_ACCESS();

	static void registerMsgs();

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();

	void onInteract(const TMsgPlayerIsInteracting & msg);
	void deactivate();

private:
	// TODO: Temporal, change after 1st MS
	Vector4 _tempColor;

	void showInteractionFeedback(const TMsgPlayerInteractionTriggerEnter & msg);
	void hideInteractionFeedback(const TMsgPlayerInteractionTriggerExit & msg);

};