#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "modules/module_physics.h"

class TCompFinalTrigger : public TCompBase {
	DECL_SIBLING_ACCESS();
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);

public:
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
};