#include "mcv_platform.h"
#include "comp_arena2.h"
#include "entity/entity_parser.h"
#include "engine.h"

#include "components/common/comp_name.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_health.h"

DECL_OBJ_MANAGER("arena_2", TCompArena2);

void TCompArena2::load(const json& j, TEntityParseContext& ctx) {
	enemiesToSpawn = j.value("enemies_to_spawn", enemiesToSpawn);
	timeBetweenSpawns = j.value("time_between_spawns", timeBetweenSpawns);
	maxNumEnemiesAlive = j.value("max_enemies_alive", maxNumEnemiesAlive);
	doors.reserve(3);
	enemies.reserve(enemiesToSpawn);

	// for now we hardcode them as many of these things will probably end up scripted.
	int numSpawns = 6;
	spawnNames.reserve(numSpawns);
	spawns.reserve(numSpawns);
	spawnNames.push_back("SpawnArena2");
	for (int i = 1; i < numSpawns + 1; i++)
		spawnNames.push_back("SpawnArena2-" + std::to_string(i));

}

void TCompArena2::debugInMenu() {
	ImGui::DragInt("Enemies to spawn: ", &enemiesToSpawn, 0, 0, 10000);
	ImGui::DragFloat("Time between spawns: ", &timeBetweenSpawns, 0.1f, 0.f, 10000.f);
	ImGui::DragInt("Max Number of Enemies Alive: ", &maxNumEnemiesAlive, 0, 0, 10000);
}

void TCompArena2::registerMsgs() {
	DECL_MSG(TCompArena2, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompArena2::update(float dt) {
	if (!active) return;

	// Erase dead enemies.
	checkIfAnyEnemiesDied();

	// On spawn time runs out, spawn.
	if (enemiesToSpawn > 0) {
		currentTime += dt;
		if (currentTime > timeBetweenSpawns && maxNumEnemiesAlive > currentEnemiesAlive) {
			currentTime = 0.0f;
			SpawnEnemies();
		}
	}
	else if (currentEnemiesAlive == 0) // All enemies are spawned and dead, and demon is spawned and dead.
		endArena();
}

void TCompArena2::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	if (active) return;

	CEntity * entity = msg.h_entity;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags && tags->hasTag(Utils::getID("player"))) {
		active = true;
		createDoors();
		fetchSpawns();
	}
}

void TCompArena2::onSpawnStatus(const TMsgSpawnStatus & msg) {
	if (msg.occupied)
		removeSpawnFromFreeOnes(msg.spawnEntityHandle);
	else
		addSpawnToFreeOnes(msg.spawnEntityHandle);
}

void TCompArena2::addSpawnToFreeOnes(CHandle spawn) {
	freeSpawns.push_back(spawn);
}

void TCompArena2::removeSpawnFromFreeOnes(CHandle spawn) {
	for (auto spawnIt = freeSpawns.begin(); spawnIt != freeSpawns.end(); spawnIt++) {
		if (spawn == (*spawnIt)) {
			freeSpawns.erase(spawnIt);
			break;
		}
	}
}

void TCompArena2::createDoors() {
	TEntityParseContext ctx;
	parseScene("data/prefabs/arenas/arena2doors.json", ctx);

	for (auto & ent : ctx.entities_loaded)
		doors.push_back(ent);
}

void TCompArena2::fetchSpawns() {
	// Name of the arena to send to spawns so they know their owner.
	TCompName * arenaName = getComponent<TCompName>();
	if (arenaName == nullptr)
		Utils::fatal("Arena doesn't have a name!");
	std::string sArenaName = arenaName->getName();

	/* Now fetch all spawns. */
	for (auto & name : spawnNames) {
		CHandle spawnHand = getEntityByName(name);
		if (!spawnHand.isValid())
			Utils::fatal("Spawn with name %s not valid.\n", name.c_str());
		CEntity * ent = spawnHand;
		TCompSpawn * spawnComp = ent->getComponent<TCompSpawn>();
		if (spawnComp == nullptr)
			Utils::fatal("Spawn with name %s doesn't have spawn component.\n", name.c_str());
		spawnComp->setSpawnOwner(sArenaName);
		spawns.push_back(spawnHand);
		freeSpawns.push_back(spawnHand);
	}
}

TCompSpawn * TCompArena2::pickRandomSpawn() {
	int numFreeSpawns = static_cast<int>(freeSpawns.size());
	TCompSpawn * spawn = nullptr;
	if (numFreeSpawns > 0) {
		int spawnIndx = RNG.i(static_cast<int>(numFreeSpawns));
		CEntity * spawnEnt = freeSpawns[spawnIndx];
		spawn = spawnEnt->getComponent<TCompSpawn>();
	}
	return spawn;
}

void TCompArena2::SpawnEnemies() {
	TCompSpawn * spawn = pickRandomSpawn();
	if (!spawn) return; // All spawns are occupied or no spawn was found.

	enemies.push_back(spawn->spawnRandomEnemy());
	enemiesToSpawn--;
	currentEnemiesAlive++;
}

void TCompArena2::checkIfAnyEnemiesDied() {
	auto endIt = std::remove_if(enemies.begin(), enemies.end(), [this](const CHandle & enemyHandle) {
		if (enemyHandle.isValid()) {
			if (this->checkIfEnemyDead((enemyHandle))) {
				this->currentEnemiesAlive--;
				return true;
			}
			return false;
		}
		this->currentEnemiesAlive--;
		return true;
	});
	enemies.erase(endIt, enemies.end());
}

bool TCompArena2::checkIfEnemyDead(CEntity * e) {
	TCompHealth * health = e->getComponent<TCompHealth>();
	if (health)
		return health->isDead();
	return false;
}

void TCompArena2::endArena() {
	// Destroy doors.
	for (auto & door : doors)
		door.destroy();

	// Destroy the spawns.
	for (auto & spawn : spawns)
		spawn.destroy();

	CHandle(this).destroy(); // We destroy the trigger.
}