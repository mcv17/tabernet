#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompHealing : public TCompBase {

	float _healPoints = 20.0f;
	float _addedCorruption = 10.0f;
	float _cooldown = 3.0f;
	float _timeToNextUse = 0;

public:
	DECL_SIBLING_ACCESS();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();
	static void declareInLua();
	void heal();

		
};
