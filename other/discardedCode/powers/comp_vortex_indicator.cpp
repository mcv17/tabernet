#include "mcv_platform.h"
#include "comp_vortex_indicator.h"
#include "components/common/comp_transform.h"
#include "engine.h"
#include "entity/msgs.h"

DECL_OBJ_MANAGER("vortex_indicator", TCompVortexIndicator);

void TCompVortexIndicator::registerMsgs() {
	DECL_MSG(TCompVortexIndicator, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompVortexIndicator, TMsgUpdateIndicator, onUpdateIndicatorMsg);
}

void TCompVortexIndicator::onUpdateIndicatorMsg(const TMsgUpdateIndicator& msg) {
	TCompTransform* c_trans = getComponent<TCompTransform>();
	c_trans->setPosition(msg.position);
	float yaw = vectorToYaw(msg.front);
	c_trans->setAngles(yaw, 0.f, 0.f);
}

void TCompVortexIndicator::update(float dt)
{
}

void TCompVortexIndicator::debugInMenu()
{
	TCompBase::debugInMenu();
}

void TCompVortexIndicator::renderDebug()
{
}


