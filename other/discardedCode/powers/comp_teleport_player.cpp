#include "mcv_platform.h"
#include "comp_teleport_player.h"
#include "entity/entity_parser.h"
#include "entity/msgs.h"
#include "utils/phys_utils.h"
#include "components/controllers/comp_player_character_controller.h"

using namespace physx;

DECL_OBJ_MANAGER("teleport_player", TCompTeleportPlayer);

void TCompTeleportPlayer::load(const json & j, TEntityParseContext & ctx) {
	_powerRange = j.value("powerRange", 10.0f);
	_powerCooldown = j.value("powerCooldown", 2.0f);
	_corruptionCost = j.value("corruptionCost", 15.0f);
}

void TCompTeleportPlayer::update(float dt) {
	if (_remainingCooldown > 0.f)
		_remainingCooldown -= dt;

	if (!_isCasting)
		return;

	if (!_powerIndicator.isValid()) //If we are casting but somehow the entity is invalid
		return;

	updateIndicatorPosition();
}

void TCompTeleportPlayer::updateIndicatorPosition() {
	//Get the camera info
	TCompTransform * playerTransform = getComponent<TCompTransform>(); //Remove

	//First raycast from camera
	PxRaycastBuffer hitData = PhysUtils::raycast(_cameraTransform->getPosition(), _cameraTransform->getFront(), _powerRange, "scenario"); //Filter will be "scenario"

	VEC3 hitPos;
	//If hit something
	if (hitData.hasBlock)
		hitPos = _cameraTransform->getPosition() + _cameraTransform->getFront() * hitData.block.distance;
	else
		hitPos = _cameraTransform->getPosition() + _cameraTransform->getFront() * _powerRange;

	//Second raycast (down)
	PxRaycastBuffer hitData2 = PhysUtils::raycast(hitPos, hitPos.Down, 100.0f, "scenario"); //Filter will be "scenario"

	//Final pos is VEC3.Zero, if different then it will be a valid position, if not, we will not cast it
	VEC3 finalPos;
	finalPos = finalPos.Zero;
	_validPosition = false;

	if (hitData2.hasBlock) {
		finalPos = hitPos + hitPos.Down * hitData2.block.distance;
		_validPosition = true;
	}

	if (!_validPosition) //We 'hide' the indicator
		finalPos = VEC3(0.0f, -1000.0f, 0.0f);

	//Update the indicator
	TMsgUpdateIndicator msg;
	msg.position = finalPos;
	msg.front = playerTransform->getFront();
	_powerIndicator.sendMsg(msg);
	_lastMsg = msg;
}

void TCompTeleportPlayer::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::DragFloat("Power cooldown", &_powerCooldown, 2.0f, 0.5f, 10.f);
	ImGui::DragFloat("Power range", &_powerRange, 15.0f, 1.0f, 30.0f);
	ImGui::DragFloat("Corruption cost", &_corruptionCost, 15.0f, 5.0f, 50.0f);
}

void TCompTeleportPlayer::renderDebug() {}

void TCompTeleportPlayer::registerMsgs() {
	DECL_MSG(TCompTeleportPlayer, TMsgToogleComponent, onToggleComponent);
}

bool TCompTeleportPlayer::startPower(TCompTransform* cameraTransform) {
	_cameraTransform = cameraTransform;
	//If we are already casting, we should not recieve this case from the input player, but we will ignore just in case.
	//Or if the power is in cooldown and we are not able to use it yet.
	if (_isCasting || _remainingCooldown > 0.f)
		return false;

	//We cast the power for the first time
	_isCasting = true;

	TEntityParseContext ctx;
	parseScene("data/prefabs/powerIndicator.json", ctx);
	_powerIndicator = ctx.entities_loaded[0];
	return true;
}

void TCompTeleportPlayer::activatePower() {
	_isCasting = false;
	CEntity * indicatorEntity = _powerIndicator;
	TCompTransform * indicatorTransf = indicatorEntity->getComponent<TCompTransform>();

	if (_validPosition) {
		TCompCorruptionControllerPlayer* playerCorruptionComp = getComponent<TCompCorruptionControllerPlayer>();
		playerCorruptionComp->addCorruption(_corruptionCost);

		//Teleport player
		TCompPlayerCharacterController * playerController = getComponent<TCompPlayerCharacterController>();
		if (playerController) playerController->teleport(_lastMsg.position + VEC3(0.0f, 0.2f, 0.0f));

		//Start the power cooldown
		_remainingCooldown = _powerCooldown;
	}

	//Destroy the power indicator
	if (_powerIndicator.isValid())
		_powerIndicator.destroy();
}

void TCompTeleportPlayer::discardPower() {
	_isCasting = false;
	if (_powerIndicator.isValid())
		_powerIndicator.destroy();
}
