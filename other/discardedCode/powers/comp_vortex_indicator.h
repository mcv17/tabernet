#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompVortexIndicator : public TCompBase {
public:

	static void registerMsgs();
	void onUpdateIndicatorMsg(const TMsgUpdateIndicator & msg);

	void update(float dt);	
	void debugInMenu();
	void renderDebug();

	DECL_SIBLING_ACCESS();
protected:

	
};