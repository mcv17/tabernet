#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_transform.h"

class TCompTeleportPlayer : public TCompBase {

public:
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();

	bool startPower(TCompTransform* cameraTransform);
	void activatePower();
	void discardPower();

	DECL_SIBLING_ACCESS();

private:
	bool _isCasting;
	bool _validPosition;
	CHandle _powerIndicator;
	float _powerCooldown;
	float _remainingCooldown;
	float _powerRange;
	float _corruptionCost;

	TMsgUpdateIndicator _lastMsg;

	TCompTransform* _cameraTransform;

	void updateIndicatorPosition();

};