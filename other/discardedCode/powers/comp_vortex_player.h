#pragma once
#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompVortexPlayer : public TCompBase {

	bool _isCasting;
	bool _validPosition;
	CHandle _vortexIndicator;
	float _powerCooldown;
	float _remainingCooldown;
	float _powerRange;
	float _corruptionCost;

	TMsgUpdateIndicator _lastMsg;

	TCompTransform* _cameraTransform;

	void updateIndicatorPosition();

public:
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();

	bool startPower(TCompTransform* cameraTransform);
	void activatePower();
	void discardPower();

	DECL_SIBLING_ACCESS();
protected:

	
};