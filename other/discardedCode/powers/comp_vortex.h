#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_collider.h"

class TCompVortex : public TCompBase {

	float _timeToLive;
	float _radius;
	float _force;
	std::vector<CHandle> _enemies;

	void onTriggerEnter(const TMsgEntityTriggerEnter & trigger_enter);
	void onTriggerExit(const TMsgEntityTriggerExit & trigger_exit);
	void onUpdateIndicatorMsg(const TMsgUpdateIndicator & msg);
	void moveEntities(float dt);
	bool checkIfEnemyDead(CEntity * e);

public:

	void load(const json & j, TEntityParseContext & ctx);
	static void registerMsgs();

	void update(float dt);	
	void debugInMenu();
	void renderDebug();
	
	DECL_SIBLING_ACCESS();
protected:

	
};
