#include "mcv_platform.h"
#include "comp_corruption_ui_controller.h"
#include "entity/entity_parser.h"
#include "components/player/comp_corruption_controller_player.h"

DECL_OBJ_MANAGER("corruption_ui_controller", TCompCorruptionUIController);

void TCompCorruptionUIController::update(float dt) {
	TCompCorruptionControllerPlayer* corruptComponent = getPlayerCorruptionComponent();
	if (!corruptComponent) return;

	_attribute = "Corruption";
	_curValue = corruptComponent->getCorruptionLevel();
	_maxValue = corruptComponent->getMaxCorruption();

	TCompBarUIController::update(dt);
}

void TCompCorruptionUIController::load(const json& j, TEntityParseContext& ctx) {
	TCompBarUIController::load(j, ctx);
}

void TCompCorruptionUIController::debugInMenu() {
	TCompBarUIController::debugInMenu();
}