#pragma once

#include "comp_bar_ui_controller.h"
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components\player\comp_corruption_controller_player.h"

class TCompCorruptionUIController : public TCompBarUIController {

public:

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();

private:
	DECL_TCOMP_ACCESS("Player", TCompCorruptionControllerPlayer, PlayerCorruptionComponent)

};