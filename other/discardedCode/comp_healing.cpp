#include "mcv_platform.h"
#include "comp_healing.h"
#include "components\common\comp_health.h"
#include "components\player\comp_corruption_controller_player.h"
#include "entity/msgs.h"
#include "engine.h"

DECL_OBJ_MANAGER("healing", TCompHealing);

void TCompHealing::load(const json & j, TEntityParseContext& ctx) {
	_healPoints = j.value("healed_points", _healPoints);
	_addedCorruption = j.value("added_corruption", _addedCorruption);
	_cooldown = j.value("cooldown", _cooldown);
}

void TCompHealing::heal()
{
	TCompCorruptionControllerPlayer * playerCorruptionComp = getComponent<TCompCorruptionControllerPlayer>();
	TCompHealth * playerHealthComp = getComponent<TCompHealth>();

	assert(playerCorruptionComp && playerHealthComp);

	float playerHealth = playerHealthComp->getCurrentHealth();

	if (playerHealth < playerHealthComp->getMaxHealth() && ImGui::GetTime() > _timeToNextUse) {
		
		playerCorruptionComp->addCorruption(_addedCorruption);
		playerHealthComp->setCurrentHealth(playerHealth + _healPoints);
		playerHealthComp->clearTickDamage();

		_timeToNextUse = (float) ImGui::GetTime() + _cooldown;
	}
}

void TCompHealing::debugInMenu()
{
	TCompBase::debugInMenu();
}

void TCompHealing::renderDebug()
{
}

void TCompHealing::registerMsgs() {
	DECL_MSG(TCompHealing, TMsgToogleComponent, onToggleComponent);
}

void TCompHealing::declareInLua() {
	EngineScripting.declareComponent<TCompHealing>
		(
			"TCompHealing",
			"heal", &TCompHealing::heal
		);
}

