#include "mcv_platform.h"
#include "comp_corruption_controller_player.h"
#include "entity/entity_parser.h"
#include "engine.h"

#include <algorithm> 
#include "components\common\comp_health.h"
#include "components\camera\comp_camera_manager.h"
#include "components/weapons/comp_weapons_manager.h"
#include "components/weapons/comp_weapons_controller.h"

DECL_OBJ_MANAGER("corruption_controller_player", TCompCorruptionControllerPlayer);


void TCompCorruptionControllerPlayer::debugInMenu() {
	TCompBase::debugInMenu();

	float newCorruption = currentCorruption;
	if (ImGui::DragFloat("Current Corruption: ", &newCorruption, 0.1f, 0.f, maxCorruption))
		changeCorruption(newCorruption - currentCorruption);

	ImGui::DragFloat("Max corruption: ", &maxCorruption, 0.1f, 0.f, 10000.f);

	ImGui::Text("Current applied penalties: ");
	if (currentIndxCorrArr > 0) {
		ImVec4 textColor = ImVec4(1.f, 0.706f, 0.412f, 1.f);
		for (int i = currentIndxCorrArr; i > 0; i--)
			ImGui::TextColored(textColor, thresholdsCorruptionPenalties[i].corruptionName.c_str());
	}
	else
		ImGui::TextColored(ImVec4(0.f, 1.f, 0.f, 1.f), "No penalties are applied");
}

void TCompCorruptionControllerPlayer::load(const json & j, TEntityParseContext& ctx) {
	currentCorruption = j.value("starting-corruption", 0.0f);
	maxCorruption = j.value("max-corruption", maxCorruption);

	if (currentCorruption > 0.0f)
		checkForPenaltyAddition();

	const json & corruptionPenalties = j["corruption-penalties"];

	// Load values from json.
	thresholdsCorruptionPenalties[0] = { "Nothing", 0, nullptr, nullptr};

	/* Precission penalty */
	thresholdsCorruptionPenalties[1] = { "Precision", corruptionPenalties.value("Precision", 20.0f),
		&TCompCorruptionControllerPlayer::addPrecisionPenalty , &TCompCorruptionControllerPlayer::removePrecisionPenalty };
	_precissionFactorSMG = j.value("precission_factor_smg", _precissionFactorSMG);
	_precissionFactorShotgun = j.value("precission_factor_shotgun", _precissionFactorShotgun);

	/* Vision penalty. */
	visionPenaltyStartThreshold = corruptionPenalties.value("Vision", 40.0f);
	thresholdsCorruptionPenalties[2] = { "Vision", visionPenaltyStartThreshold,
	&TCompCorruptionControllerPlayer::addVisionPenalty , &TCompCorruptionControllerPlayer::removeVisionPenalty };

	/* Insanity penalty. */
	thresholdsCorruptionPenalties[3] = { "Insanity", corruptionPenalties.value("Insanity", 60.0f),
	&TCompCorruptionControllerPlayer::addInsanityPenalty , &TCompCorruptionControllerPlayer::removeInsanityPenalty };

	/* Max life penalty. */
	maxLifePenaltyStartThreshold = corruptionPenalties.value("MaxLife", 80.0f);
	minMaxLifePercentage = j.value("min-max-life-percentage", minMaxLifePercentage);
	thresholdsCorruptionPenalties[4] = { "MaxLife", maxLifePenaltyStartThreshold,
	&TCompCorruptionControllerPlayer::addMaxLifePenalty , &TCompCorruptionControllerPlayer::removeMaxLifePenalty };
		
	// Sort values in case we changed how values are stored.
	std::sort(thresholdsCorruptionPenalties.begin(), thresholdsCorruptionPenalties.end(),
		[](CorruptionPenalty p1, CorruptionPenalty p2) {
		return p1.corruptionThreshold < p2.corruptionThreshold;
	});	

	// Fetch values for the penalties.
	for (int i = 0; i < thresholdsCorruptionPenalties.size(); ++i) {
		auto & penalty = thresholdsCorruptionPenalties[i];
		if(penalty.corruptionName == "Vision") {
			// If it was the last penalty.
			if (i == thresholdsCorruptionPenalties.size() - 1)
				visionPenaltyEndThreshold = maxCorruption;
			else
				visionPenaltyEndThreshold = thresholdsCorruptionPenalties[i + 1].corruptionThreshold;
		}
		else if (penalty.corruptionName == "MaxLife") {
			if (i == thresholdsCorruptionPenalties.size() - 1)
				maxLifePenaltyEndThreshold = maxCorruption;
			else
				maxLifePenaltyEndThreshold = thresholdsCorruptionPenalties[i + 1].corruptionThreshold;
		}
	}

	// Get the camera handle.
	playerCameraH = TCompCameraManager::getCamera(TCompCameraManager::Cameras::Final_Camera);
}


void TCompCorruptionControllerPlayer::onEntityCreated(const TMsgEntityCreated & msg) {
	healthCompH = getComponent<TCompHealth>();
}

void TCompCorruptionControllerPlayer::changeCorruption(float corruptionDelta){
	assert(corruptionDelta != 0 && "Corruption recieved was equal to 0");

	float oldCorruption = currentCorruption;
	currentCorruption += corruptionDelta;
	currentCorruption = Maths::clamp(currentCorruption, 0.0f, maxCorruption);
	
	if (oldCorruption < currentCorruption)
		checkForPenaltyAddition();
	else
		checkForPenaltyRemoval();
}

void TCompCorruptionControllerPlayer::onCorruptionAdded(const TMsgCorruptionAdded & msg){
	changeCorruption(msg.addedCorruption);
}

void TCompCorruptionControllerPlayer::onCorruptionRemoved(const TMsgCorruptionRemoved & msg){
	changeCorruption(-msg.removedCorruption);
}

void TCompCorruptionControllerPlayer::onFullyRestored(const TMsgFullRestore & msg) {
	changeCorruption(-maxCorruption);
}

void TCompCorruptionControllerPlayer::registerMsgs() {
	DECL_MSG(TCompCorruptionControllerPlayer, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCorruptionControllerPlayer, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompCorruptionControllerPlayer, TMsgCorruptionAdded, onCorruptionAdded);
	DECL_MSG(TCompCorruptionControllerPlayer, TMsgCorruptionRemoved, onCorruptionRemoved);
	DECL_MSG(TCompCorruptionControllerPlayer, TMsgFullRestore, onFullyRestored);
}

void TCompCorruptionControllerPlayer::declareInLua() {
	EngineScripting.declareComponent<TCompCorruptionControllerPlayer>
		(
			"TCompCorruptionControllerPlayer",
			"getCorruptionLevel", &TCompCorruptionControllerPlayer::getCorruptionLevel,
			"getMaxCorruption", &TCompCorruptionControllerPlayer::getMaxCorruption,
			"addCorruption", &TCompCorruptionControllerPlayer::addCorruption
		);
}

void TCompCorruptionControllerPlayer::addCorruption(float addedCorruption)
{
	changeCorruption(addedCorruption);
}

void TCompCorruptionControllerPlayer::update(float delta) {}


void TCompCorruptionControllerPlayer::checkForPenaltyAddition() {
	for (int i = currentIndxCorrArr; i < thresholdsCorruptionPenalties.size(); i++) {
		auto & currentCorruptionPenalty = thresholdsCorruptionPenalties[i];

		auto nextCorruptionThreshold = maxCorruption;
		if(i < thresholdsCorruptionPenalties.size()-1)
			nextCorruptionThreshold = thresholdsCorruptionPenalties[i+1].corruptionThreshold;

		// Apply penalty if present.
		auto & penaltyPointer = currentCorruptionPenalty.penaltyFunc;
		if(penaltyPointer)
			(this->*penaltyPointer)();
		
		// If our corruption is more than this corruption threshold, move to next corruption.
		if (currentCorruption > nextCorruptionThreshold) {
			currentIndxCorrArr = i+1;
			continue;
		}
		break;
	}
}

void TCompCorruptionControllerPlayer::checkForPenaltyRemoval() {
	for (int i = currentIndxCorrArr; i > 0; i--) {
		auto & corruptionPenalty = thresholdsCorruptionPenalties[i];
		float corruptionPointsForThreshold = corruptionPenalty.corruptionThreshold;
		
		// If our corruption is less than this corruption threshold we already had, remove the penalty.
		if (currentCorruption <= corruptionPointsForThreshold) {
			(this->*corruptionPenalty.remPenaltyFunc)();
			currentIndxCorrArr = i-1;
			continue;
		}
		else
			(this->*corruptionPenalty.remPenaltyFunc)();
		break;
	}
}

void TCompCorruptionControllerPlayer::addPrecisionPenalty(){
	CEntity * player = CHandle(this).getOwner();
	if (player == nullptr) return;
	TCompWeaponManager * weaponManager = player->getComponent<TCompWeaponManager>();
	if (weaponManager == nullptr) return;
	float corruptionRange = thresholdsCorruptionPenalties[2].corruptionThreshold - thresholdsCorruptionPenalties[1].corruptionThreshold;
	float corr = currentCorruption - thresholdsCorruptionPenalties[1].corruptionThreshold;
	float percentage = corr / corruptionRange;
	percentage = Maths::clamp(percentage, 0.0f, 1.0f);

	TCompWeaponsController * smg = weaponManager->getWeapon(Weapon::SMG);
	TCompWeaponsController * shotgun = weaponManager->getWeapon(Weapon::SHOTGUN);
	
	smg->setMaxDispersion(smg->getBaseMaxDispersion() * (1 + percentage * _precissionFactorSMG));
	shotgun->setMaxDispersion(shotgun->getBaseMaxDispersion() * (1 + percentage * _precissionFactorShotgun));

	// Communicate with the player class or the weapon controller, wherever the accuracy parameter may be stored for the weapon.
	// componentWithTheAccuracyValue.ChangeAccuracyBias(newBias);
}

void TCompCorruptionControllerPlayer::removePrecisionPenalty(){
	CEntity * player = CHandle(this).getOwner();
	if (!player) return;
	TCompWeaponManager * weaponManager = player->getComponent<TCompWeaponManager>();
	if (!weaponManager) return;

	TCompWeaponsController * smg = weaponManager->getWeapon(Weapon::SMG);
	TCompWeaponsController * shotgun = weaponManager->getWeapon(Weapon::SHOTGUN);

	smg->resetMaxDispersion();
	shotgun->resetMaxDispersion();

	// Communicate with the player class or the weapon controller, wherever the accuracy parameter may be stored for the weapon.
	// componentWithTheAccuracyValue.ChangeAccuracyBias(newBias);
}

void TCompCorruptionControllerPlayer::addVisionPenalty(){
	CEntity * cam = playerCameraH.getOwner();
	if (!cam) return;

	float corruptionRange = visionPenaltyEndThreshold - visionPenaltyStartThreshold;
	float corr = currentCorruption - visionPenaltyStartThreshold;
	float percentage = corr / corruptionRange;
	percentage = Maths::clamp(percentage, 0.0f, 1.0f);
	TMsgVisionPenaltyStatusChanged msg = { percentage };
	cam->sendMsg(msg);
}

void TCompCorruptionControllerPlayer::removeVisionPenalty(){
	CEntity * cam = playerCameraH.getOwner();
	if (!cam) return;

	float corruptionRange = visionPenaltyEndThreshold - visionPenaltyStartThreshold;
	float corr = currentCorruption - visionPenaltyStartThreshold;
	float percentage = corr / corruptionRange;
	percentage = Maths::clamp(percentage, 0.0f, 1.0f);
	TMsgVisionPenaltyStatusChanged msg = { percentage };
	cam->sendMsg(msg);
}

void TCompCorruptionControllerPlayer::addInsanityPenalty(){
	// Communicate with the player camera. Get his postprocess component and call the addInsanityPenaltyShader.
}

void TCompCorruptionControllerPlayer::removeInsanityPenalty(){
	// Communicate with the player camera. Get his postprocess component and call the removeInsanityPenaltyShader.
}

void TCompCorruptionControllerPlayer::addMaxLifePenalty(){
	TCompHealth * comp = healthCompH;
	if (!comp) return;

	float maxLife = comp->getMaxHealth();
	float minLife = minMaxLifePercentage * maxLife;
	float lifeThatCanBeLost = maxLife - minLife;
	float corrRange = maxLifePenaltyEndThreshold - maxLifePenaltyStartThreshold;
	float corrRangeActualpoints = currentCorruption - maxLifePenaltyStartThreshold;
	float newMaxLife = maxLife - (corrRangeActualpoints / corrRange) * lifeThatCanBeLost;
	float curHealth = comp->getCurrentHealth();

	if (curHealth > newMaxLife)
		comp->setCurrentHealth(newMaxLife);

	comp->setCurrentMaxHealth(newMaxLife);
}

void TCompCorruptionControllerPlayer::removeMaxLifePenalty(){
	TCompHealth * comp = healthCompH;
	if (!comp) return;

	float maxLife = comp->getMaxHealth();
	float minLife = minMaxLifePercentage * maxLife;
	float lifeThatCanBeLost = maxLife - minLife;
	float corrRange = maxLifePenaltyEndThreshold - maxLifePenaltyStartThreshold;
	float corrRangeActualpoints = currentCorruption - maxLifePenaltyStartThreshold;
	corrRangeActualpoints = std::max(corrRangeActualpoints, 0.0f);
	float newMaxLife = maxLife - (corrRangeActualpoints / corrRange) * lifeThatCanBeLost;
	float curHealth = comp->getCurrentHealth();

	if (curHealth > newMaxLife)
		comp->setCurrentHealth(newMaxLife);

	comp->setCurrentMaxHealth(newMaxLife);
}