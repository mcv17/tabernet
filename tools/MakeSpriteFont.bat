rem Remember that rem is used for comments in bat files
rem Change the font name and the font size here and run the bat to create all the font types
rem and have the output moved into the correct fonts project folder automatically
rem the font name needs the "" !!!! font size the number as is
SET fontName="Rush"
SET fontSize=16

rem the original name
rem ECHO %fontName%

rem replace spaces with underscores
set fileName=%fontName: =_%
rem ECHO %fileName%

rem delete the commas for the command
set fileName=%fileName:~1,-1%
rem ECHO %fileName%

rem prepare the four versions: bold, italic, strikeout and underline
set fileName0=%fileName%_%fontSize%_normal.spritefont
set fileName1=%fileName%_%fontSize%_bold.spritefont
set fileName2=%fileName%_%fontSize%_italic.spritefont
set fileName3=%fileName%_%fontSize%_strikeout.spritefont
set fileName4=%fileName%_%fontSize%_underline.spritefont

rem prepare the fontSize argument
set fontSizeArg=/FontSize:%fontSize%

rem generate the four versions of the fonts
MakeSpriteFont.exe %fontName% %fileName0% %fontSizeArg%
MakeSpriteFont.exe %fontName% %fileName1% %fontSizeArg% /FontStyle:Bold
MakeSpriteFont.exe %fontName% %fileName2% %fontSizeArg% /FontStyle:Italic
MakeSpriteFont.exe %fontName% %fileName3% %fontSizeArg% /FontStyle:Strikeout
MakeSpriteFont.exe %fontName% %fileName4% %fontSizeArg% /FontStyle:Underline

rem move the generated fonts to the bin/data/fonts folder
move *.spritefont ..\bin\data\fonts
pause