#include "SceneParser.h"

#include <fstream>      // std::ofstream
#include <iomanip>      // std::setw

#include <filesystem>

std::string getFileName(const std::string & filepath) {
	std::string file = filepath;
	std::string delimiter = "/";

	size_t pos = 0;
	while ((pos = file.find(delimiter)) != std::string::npos)
		file.erase(0, pos + delimiter.length());

	return file;
}

std::string removeExtension(const std::string & name, const std::string & ext) {
	return name.substr(0, name.find(ext));
}

void SceneParser::start(const std::string & basePath, const std::string & filePath) {
	std::string sceneName = getFileName(filePath);
	sceneName = getFileName(sceneName);
	sceneName = removeExtension(sceneName, ".json");

	// We use the filepath to open the json.
	json scene = Utils::loadJson(filePath);

	// Start with parsing all the prefabs in the scene.
	parseFile(scene);

	// Prefabs files.
	createInstancingPrefabsFile(basePath, sceneName);
}

void SceneParser::parseMaterials(const std::string & folderWhereMaterialsAreAt){
	// For now, can only be used for decals, not going to complicate myself. Very shitty but works.
	if (folderWhereMaterialsAreAt.find("decals") != std::string::npos) {
		// Read all the decals in the engine and prepare the instances for rendering decals.
		createInstancingMaterials(folderWhereMaterialsAreAt);
	}
}

/* This function parses the contents from the scene file. It will return all the prefabs in the scene.
These prefabs will be registered so they can be read and all of it's content registered for instancing. */
bool SceneParser::parseFile(const json & j_scene) {
	assert(j_scene.is_array());

	// For each item in the array...
	for (int i = 0; i < j_scene.size(); ++i) {
		auto& j_item = j_scene[i];

		assert(j_item.is_object());

		if (j_item.count("entity")) {
			auto & j_entity = j_item["entity"];

			// It's a prefab?
			if (j_entity.count("prefab")) {
				// Get the prefab path.
				std::string prefab_src = j_entity["prefab"];
				assert(!prefab_src.empty());

				// Add the prefab to be parsed if not added already.
				if (prefabsAlreadyParsed.count(prefab_src) == 0) {
					prefabsInTheScene.push_back(prefab_src);
					prefabsAlreadyParsed.insert(prefab_src);
				}
			}
		}
		else if (j_item.count("group"))
			parseFile(j_item["group"]);
	}

	return true;
}

// Now, create all the mesh instanced jsons.
void SceneParser::createInstancingPrefabsFile(const std::string & basePath, const std::string & sceneName) {
	json instanced_prefabs;

	std::string instancing_path = basePath + "/bin/data/modules/gpu_culling/instanced_prefabs/";
	std::string filename = instancing_path + sceneName + ".instanced_scene_data";

	instanced_prefabs["desc"] = "Holds all prefabs in the scene, this file gets readed by the instancing module to register all the prefabs in a scene for instancing.";
	instanced_prefabs["prefabs_to_register_for_instancing"] = prefabsInTheScene;

	// Save JSON.
	std::ofstream o(filename);
	o << std::setw(4) << instanced_prefabs << std::endl;
}

// Creates gpu_culled versions of materials for each in their gpu culled instanced version. Used for
void SceneParser::createInstancingMaterials(const std::string & folderWhereMaterialsAreAt) {
	json instancingDecals;

	const int sizeMatSuffix = std::string(".material").size();
	std::string instancing_path = folderWhereMaterialsAreAt;

	if (std::filesystem::exists(folderWhereMaterialsAreAt) == false){
		std::cout << "Directory: " << folderWhereMaterialsAreAt << " doesn't exist. \n";
		return;
	}

	// Create decals instancing entities.
	for (auto & decalPath : std::filesystem::recursive_directory_iterator(instancing_path)) {
		// Ignore dynamic decals, we don't want a new version of them.
		if (decalPath.path().string().find("dynamic") != std::string::npos) continue;

		if (decalPath.status().type() != std::filesystem::file_type::directory) {
			std::string filePath = decalPath.path().string();

			// Load decal Json.
			json instance = Utils::loadJson(filePath);

			json instancedDecal = instance;
			
			if (instancedDecal.count("technique") == 0) continue;
			
			std::string techName = instancedDecal["technique"];
			instancedDecal["technique"] = techName.substr(0, techName.size() - 5) + "_gpu_culled.tech"; // Remove .tech and add _gpu_culled.
			
			filePath = filePath.substr(0, filePath.size() - sizeMatSuffix) + "_instanced_gpu.material";

			// Save instanced decal version.
			std::ofstream o(filePath);
			o << std::setw(4) << instancedDecal << std::endl;

		}
	}

}

// Unused.
/*

bool SceneParser::parseFile(const json & j_scene) {
	assert(j_scene.is_array());

	// For each item in the array...
	for (int i = 0; i < j_scene.size(); ++i) {
		auto& j_item = j_scene[i];

		assert(j_item.is_object());

		if (j_item.count("entity")) {
			auto & j_entity = j_item["entity"];

			// It's a prefab?
			if (j_entity.count("prefab")) {
				// Get the prefab path.
				std::string prefab_src = j_entity["prefab"];
				assert(!prefab_src.empty());

				// Add the prefab to be parsed.
				if (prefabsAlreadyParsed.count(prefab_src))
					prefabsInTheScene.push_back(prefab_src);
			}
			else {
				// Parse the entity information necessary for instancing.

				// If there is a render component we need it for instancing.
				if (j_entity.count("render")) {
					auto & jRender = j_entity["render"];

					// Check if there are multiple meshes.
					if (jRender.count("meshes")) {
						for (int i = 0; i < jRender.size(); ++i) {
							auto & meshAndMaterials = jRender[i];

							std::string meshPath = meshAndMaterials["mesh"];
							std::vector<MaterialPath> materials;

							if (meshAndMaterials.count("materials"))
								materials = meshAndMaterials["materials"].get< std::vector<MaterialPath> >();
							else
								materials.push_back(meshAndMaterials.value("material", "data/materials/plain.material"));

							registerMesh(meshPath, materials);
						}
					}
					else {
						std::string meshPath = jRender["mesh"];
						std::vector<MaterialPath> materials;

						if (jRender.count("materials"))
							materials = jRender["materials"].get< std::vector<MaterialPath> >();
						else
							materials.push_back(jRender.value("material", "data/materials/plain.material"));

						registerMesh(meshPath, materials);
					}
				}
			}
		}
		else if (j_item.count("group"))
			parseFile(j_item["group"]);
	}

	return true;
}

void SceneParser::registerMesh(const std::string & mesh_path, std::vector<MaterialPath> & materials) {
	MeshInstancingID id = getMeshID(mesh_path, materials);

	// Create instance if not present.
	if (!meshesToInstance.count(id)) {

		for (auto & mat : materials)
			mat = removeExtension(mat, ".material") + "_instanced.material";

		MeshInstancing instance = { mesh_path, materials, 0, 0 };
		meshesToInstance[id] = instance;
	}

	// Here we should check if dynamic in the future.
	auto & meshInstance = meshesToInstance[id];
	meshInstance.num_static += 1;
}

SceneParser::MeshInstancingID SceneParser::getMeshID(const std::string & mesh_path, const std::vector<MaterialPath> & materials) {
	std::string result;

	int32_t hashValue = 0.0;
	for (auto & mat : materials) {
		int32_t val = 0.0;
		MurmurHash3_x86_32((void *)mat.c_str(), mat.size(), 10, &val);
		hashValue += val;
	}

	return mesh_path + " - " + std::to_string(hashValue);
}

// Now, create all the mesh instanced jsons.
void SceneParser::createMeshInstanced(const std::string & basePath) {
	json instancedMesh;

	std::string instancing_path = basePath + "/bin/data/meshes/instanced_meshes/";
	std::string suffix = ".instanced_mesh";
	std::string delimiter = "/";

	for (auto & mesh : meshesToInstance) {
		instancedMesh["instanced_id"] = mesh.first;
		instancedMesh["instanced_mesh"] = mesh.second.mesh_path;
		instancedMesh["instances_type"] = "Instance";
		instancedMesh["num_instances_reserved"] = mesh.second.num_static;

		std::string meshName = getFileName(mesh.second.mesh_path);
		meshName = removeExtension(meshName, ".mesh");
		std::string fileName = instancing_path + meshName + suffix;

		// Save JSON.
		std::ofstream o(fileName);
		o << std::setw(4) << instancedMesh << std::endl;
	}
}

// Finally, create the json with the entities that will render the instances.
void SceneParser::createInstancingEntitiesJson(const std::string & basePath, const std::string & sceneName){
	json instancingEntities;

	std::string instancing_path = basePath + "/bin/data/scenes/";

	// Base entity.
	json baseInstanceEntity;
	baseInstanceEntity["entity"]["name"] = "Instances_Meshes";
	instancingEntities.push_back(baseInstanceEntity);

	for (auto & mesh : meshesToInstance) {
		json instanceEntity;

		std::string entityName = getFileName(mesh.second.mesh_path);
		size_t pos = entityName.find(".mesh");
		entityName = entityName.substr(0, pos);

		instanceEntity["entity"]["name"] = entityName;
		instanceEntity["entity"]["desc"] = "This instance registers the draw call to render all the " + entityName + " instances";
		instanceEntity["entity"]["transform"]["pos"] = "0 0 0";

		std::string meshPath = mesh.second.mesh_path;
		meshPath = getFileName(meshPath);
		meshPath = removeExtension(meshPath, ".mesh");
		meshPath += ".instanced_mesh";

		instanceEntity["entity"]["render"]["mesh"] = "data/meshes/instanced_meshes/" + meshPath;
		instanceEntity["entity"]["render"]["materials"] = mesh.second.materials;

		instancingEntities.push_back(instanceEntity);
	}

	std::string filePath = instancing_path + sceneName + "_instancing.json";

	// Save JSON.
	std::ofstream o(filePath);
	o << std::setw(4) << instancingEntities << std::endl;
}

*/