#pragma once
#include "json.hpp"

using json = nlohmann::json;

namespace Utils {
	void dbg(const char* fmt, ...);
	void fatal(const char* fmt, ...);

	json loadJson(const std::string & filename);
	bool fileExists(const char* filename);
}