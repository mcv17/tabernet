#pragma once

#include <iostream>
#include "utils.h"
#include "murmur3/murmur3.h"

#include <set>

// Acts as a material and scene parser for now.
class SceneParser {
	typedef std::string MeshInstancingID; // Combination of mesh and materials, used for id.
	typedef std::string PrefabPath;
	typedef std::string MaterialPath;

	// Prefabs whose files need to be read to find meshes to instance.
	std::vector<PrefabPath> prefabsInTheScene;
	std::set<PrefabPath> prefabsAlreadyParsed;

	/* Functions. */

	/* This function parses the contents from the scene file. */
	bool parseFile(const json & j_scene);

	// Creates a instancing file with all prefabs in the scene that must be registered to be instanced.
	void createInstancingPrefabsFile(const std::string & basePath, const std::string & sceneName);

	void createInstancingMaterials(const std::string & folderWhereMaterialsAreAt);

public:
	// Start the Scene Parser.
	void start(const std::string & basePath, const std::string & filePath);

	// Start the Material Parser.
	void parseMaterials(const std::string & folderWhereMaterialsAreAt);
};

// Unused for now.
/*
	struct MeshInstancing {
		std::string mesh_path; // Path for the mesh.
		std::vector<MaterialPath> materials; // Materials in the mesh.
		int num_static = 0; //How many are static.
		int num_dynamic = 0; //How many are dynamic. Not used, prepared for the future but probably won't use.
	};
	std::map<MeshInstancingID, MeshInstancing> meshesToInstance;

	void registerMesh(const std::string & mesh_path, std::vector<MaterialPath> & materials);
	MeshInstancingID getMeshID(const std::string & mesh_path, const std::vector<MaterialPath> & materials);

	// Now, create all the mesh instanced jsons.
	void createMeshInstanced(const std::string & basePath);

	// Finally, create the json with the entities that will render the instances.
	void createInstancingEntitiesJson(const std::string & basePath, const std::string & sceneName);
*/