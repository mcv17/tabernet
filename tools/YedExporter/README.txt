- Important stuff to keep in mind:
	- It's necessary for the BT to be in a NAMED group when parser is executed.
	- Node X and Y position coordinates are relevant to the parser.
		- Y coord defines which node will be root node, the one with lower Y attribute (upmost node in the group) will be root node.
		- X coord defines order of execution for children from left to right.
	- You CAN give two nodes the same name, in this case IDs will be appended to the name which might make it more difficult to find them in C++ code.
	- Names for nodes will be tranformed to Underscore notation, that means the name will be converted to lower case and any space and end of line will be replaced with "_".
	- Variable names only accept numbers and characters both in uppercase and lowercase.