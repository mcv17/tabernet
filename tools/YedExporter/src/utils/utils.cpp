#include "pch.h"
#include "utils.h"
#include <fstream>
#include <windows.h>

namespace tinyxml2 {
	bool isGroupElement(XMLElement* elem) {
		const char* attrValue = elem->Attribute("yfiles.foldertype");
		return attrValue && strcmp(attrValue, "group") == 0;
	}

	XMLElement* getXMLElementByAttribute(XMLElement* root, const char* elemName, const char* attrName, const char* attrValue) {
		XMLElement* elem = root->FirstChildElement(elemName);
		while (elem) {
			const char* elemAttrValue = elem->Attribute(attrName);
			if (elemAttrValue && strcmp(elemAttrValue, attrValue) == 0) {
				return elem;
			}

			elem = elem->NextSiblingElement(elemName);
		}

		return nullptr;
	}

	XMLElement* getGroup(XMLElement* root, const char* groupName) {
		XMLElement* elem = root->FirstChildElement("node");
		while (elem) {
			if (isGroupElement(elem)) {
				XMLElement* XMLdata = getXMLElementByAttribute(elem, "data", "key", "d6");
				if (XMLdata) {

					XMLElement* XMLlabel = XMLdata->FirstChildElement("y:ProxyAutoBoundsNode");
					XMLlabel = XMLlabel->FirstChildElement("y:Realizers");
					XMLlabel = XMLlabel->FirstChildElement("y:GroupNode");
					XMLlabel = XMLlabel->FirstChildElement("y:NodeLabel");
					XMLNode* label = XMLlabel->FirstChild();
					if (label) {
						const char* groupLabel = label->Value();
						if (strcmp(groupLabel, groupName) == 0) {
							return elem->FirstChildElement("graph");
						}
					}
				}
			}

			elem = elem->NextSiblingElement("node");
		}

		return nullptr;
	}
}

void dbg(const char* format, ...) {
  va_list argptr;
  va_start(argptr, format);
  char dest[1024 * 16];
  _vsnprintf(dest, sizeof(dest), format, argptr);
  va_end(argptr);
  ::OutputDebugString(dest);
}

void fatal(const char* format, ...) {
  va_list argptr;
  va_start(argptr, format);
  char dest[1024 * 16];
  _vsnprintf(dest, sizeof(dest), format, argptr);
  va_end(argptr);
  ::OutputDebugString(dest);

	MessageBox(nullptr, dest, "Error!", MB_OK);
  exit(-1);
}

// --------------------------------------------------------
json loadJson(const std::string& filename) {

  json j;

  while (true) {

    std::ifstream ifs(filename.c_str());
    if (!ifs.is_open()) {
      fatal("Failed to open json file %s\n", filename.c_str());
      continue;
    }

#ifdef NDEBUG

    j = json::parse(ifs, nullptr, false);
    if (j.is_discarded()) {
      ifs.close();
      fatal("Failed to parse json file %s\n", filename.c_str());
      continue;
    }

#else

    try
    {
      // parsing input with a syntax error
      j = json::parse(ifs);
    }
    catch (json::parse_error& e)
    {
      ifs.close();
      // output exception information
      fatal("Failed to parse json file %s\n%s\nAt offset: %d"
        , filename.c_str(), e.what(), e.byte);
      continue;
    }

#endif

    // The json is correct, we can leave the while loop
    break;
  }

  return j;
}
