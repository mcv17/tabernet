#pragma once

namespace tinyxml2 {
	bool isGroupElement(XMLElement* elem);

	XMLElement* getXMLElementByAttribute(XMLElement* root, const char* elemName, const char* attrName, const char* attrValue);

	XMLElement* getGroup(XMLElement* root, const char* groupName);
}

json loadJson(const std::string& filename);

void dbg(const char* fmt, ...);
void fatal(const char* fmt, ...);
