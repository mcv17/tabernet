#include "pch.h"
#include "bt.h"
#include "utils/utils.h"
#include <fstream>
#include <iomanip>
#include <string> 
#include "utils/common.h"

using namespace tinyxml2;

std::string processTextLabel(std::string label) {
	// replace '\n' characters with spaces
	std::replace(label.begin(), label.end(), '\n', ' ');
	// remove extra spaces on both sides
	std::regex sideSpacesRegex(SPACE_REGEX + "(.*)" + SPACE_REGEX);
	std::smatch match;
	std::regex_search(label, match, sideSpacesRegex);
	label = match.str(1);

	return label;
}

std::string toUnderscore(std::string text) {
	// transform string to lowercase
	std::transform(text.begin(), text.end(), text.begin(), ::tolower);
	// replace spaces with underscores
	std::replace(text.begin(), text.end(), ' ', '_');

	return text;
}

XMLElement * getUpmostNode(XMLElement * XMLroot) {
	XMLElement * XMLnode = XMLroot->FirstChildElement("graph")->FirstChildElement("node");
	XMLElement * XMLnodeData;
	XMLElement * XMLupmost = nullptr;
	double upmostPos = 0.0, curYPos = 0.0;
	bool noMaxFound = true;
	while (XMLnode) {
		if (!isGroupElement(XMLnode)) {
			XMLnodeData = getXMLElementByAttribute(XMLnode, "data", "key", "d6");
			curYPos = atof(XMLnodeData->FirstChildElement("y:ShapeNode")->FirstChildElement("y:Geometry")->Attribute("y"));
			if (curYPos < upmostPos || noMaxFound) {
				upmostPos = curYPos;
				XMLupmost = XMLnode;
				noMaxFound = false;
			}
		}

		XMLnode = XMLnode->NextSiblingElement("node");
	}

	return XMLupmost;
}

bool CBTGroup::processNode(CBehaviourTree::TNode & outNode, XMLElement * XMLnode, CBehaviourTree & bt) {
	std::string id, label, color, name;
	float x;

	XMLElement * XMLnodeData = getXMLElementByAttribute(XMLnode, "data", "key", "d6");
	if (!XMLnodeData)
		return false;
	// if XMLnode is not a BT node do not process
	if (!XMLnodeData->FirstChildElement("y:ShapeNode"))
		return false;
	id = XMLnode->Attribute("id");
	label = XMLnodeData->FirstChildElement("y:ShapeNode")->FirstChildElement("y:NodeLabel")->FirstChild()->Value();
	color = XMLnodeData->FirstChildElement("y:ShapeNode")->FirstChildElement("y:Fill")->Attribute("color");
	// get x position to sort transitions array later
	x = static_cast<float>(atof(XMLnodeData->FirstChildElement("y:ShapeNode")->FirstChildElement("y:Geometry")->Attribute("x")));
	name = label;
	outNode.id = id;
	if (bt.namesMap.count(name) > 0 || bt.rootNode.name == name) {
		std::regex idRegex(".*::n([0-9]+)");
		std::smatch match;
		std::regex_search(id, match, idRegex);
		printf("Node with name %s repeated, assigning id %s\n", name.c_str(), match.str(1).c_str());
		name = name + "[id=" + match.str(1) + "]";
	}
	outNode.name = name;
	bt.namesMap.insert(name);

	if (_nodeColorTypes.count(color) == 0)
		fatal("At node %s: Unknown node type with color %s", outNode.name.c_str(), color.c_str());
	outNode.type = _nodeColorTypes[color];
	if (outNode.type == ACTION_NODE_TYPE)
		outNode.params["function"] = processTextLabel(label) ;
	outNode.xPos = x;

	return true;
}

json processPredicateOperand(const std::string & operand, XMLElement * XMLedge, CBehaviourTree & bt, std::string & targetName) {
	json jOperand;
	json jValue;

	std::regex intOperandRegex(INT_REGEX);
	std::regex floatOperandRegex(FLOAT_REGEX);
	std::regex boolOperandRegex(BOOL_REGEX);
	std::regex varOperandRegex(VARIABLE_REGEX);

	if (std::regex_match(operand, intOperandRegex)) {
		jOperand["type"] = "value";
		jValue["type"] = "int";
		jValue["value"] = atoi(operand.c_str());
		jOperand["value"] = jValue;
	}
	else if (std::regex_match(operand, floatOperandRegex)) {
		jOperand["type"] = "value";
		jValue["type"] = "float";
		jValue["value"] = atof(operand.c_str());
		jOperand["value"] = jValue;
	}
	else if (std::regex_match(operand, boolOperandRegex)) {
		jOperand["type"] = "value";
		jValue["type"] = "bool";
		jValue["value"] = (toUnderscore(operand) == "true");
		jOperand["value"] = jValue;
	}
	else if (std::regex_match(operand, varOperandRegex)) {
		jOperand["type"] = "variable";
		jOperand["value"] = operand;
		if (bt.variables.count(operand) == 0)
			fatal("At transition to %s: Variable %s undeclared", targetName.c_str(), operand.c_str());
	}
	else
		fatal("At transition to %s: syntax error for condition predicate", targetName.c_str());

	return jOperand;
}

bool CBTGroup::processTransition(CBehaviourTree::TTransition & outTransition, XMLElement * XMLedge, CBehaviourTree & bt) {
	std::string sourceName = XMLedge->Attribute("source");
	CBehaviourTree::TNode * sourceNode = nullptr;
	
	// get source node, be it root or normal node
	if (sourceName == bt.rootNode.id)
		sourceNode = &bt.rootNode;
	else
		sourceNode = &bt.nodes[XMLedge->Attribute("source")];
	
	// set source and target of transition
	outTransition.source = sourceNode;
	outTransition.target = &bt.nodes.at(XMLedge->Attribute("target"));
	outTransition.sourceName = outTransition.source->name;
	outTransition.targetName = outTransition.target->name;

	//std::regex test("nothing.*");
	//if (std::regex_match(outTransition.targetName, test)) {
	//	int a = 1;
	//}

	// read transition label
	std::string label;
	XMLElement * XMLedgeData = getXMLElementByAttribute(XMLedge, "data", "key", "d10")->FirstChildElement("y:PolyLineEdge");
	if (!XMLedgeData->FirstChildElement("y:EdgeLabel")) // if transition has no label
		label = "";
	else {
		// it is possible that transition has label but is actually empty
		XMLNode * XMLedgeLabel = XMLedgeData->FirstChildElement("y:EdgeLabel")->FirstChild();
		if (XMLedgeLabel->FirstChild()) // hack to detect if label is empty
			label = "";
		else
			label = XMLedgeLabel->Value();
	}

	// we set params to nullptr to make sure it is empty at the start
	outTransition.params = nullptr;
	// process conditions for priority, random and decorator nodes
	if (sourceNode->type == "priority") {
		// priority type source node
		json jCond;
		std::regex funcCondition(REGEX_GROUP( FUNCTION_NAME_REGEX ) + SPACE_REGEX + "\\((.*)\\)");
		std::smatch match;
		if (label.empty()) {
			// default condition (unnamed transition), always return true
			jCond["type"] = "default";
			outTransition.params["condition"] = jCond;
		}
		else if (std::regex_search(label, match, funcCondition) && match.size() > 1 && label == match.str(0)) {
			// function condition (i.e. isPlayerViewable())
			jCond["type"] = "function";
			jCond["name"] = match.str(1);
			outTransition.params["condition"] = jCond;
		}
		else {
			// basic comparation (i.e. life == 0 or isPlayerDead) 
			// first we look if operation is negated, that is if it starts with '!' 
			std::regex isNegated("!.*");
			bool negated = (std::regex_match(label, isNegated));

			const std::string PARENTHESIS_REGEX = "[() ]*";
			// regex for comparing bool variables (i.e. isPlayerDead)
			std::regex boolCondition(NEGATION_REGEX + SPACE_REGEX + REGEX_GROUP( VARIABLE_REGEX ));
			// regex for a generic predicate condition (i.e. a > b, distance >= 5)
			const std::string OPERAND_REGEX = VALUE_REGEX + "|" + VARIABLE_REGEX;
			std::regex predCondition(
				NEGATION_REGEX + PARENTHESIS_REGEX + 
				REGEX_GROUP( OPERAND_REGEX ) + 
				SPACE_REGEX +
				REGEX_GROUP( OPERATOR_REGEX ) + 
				SPACE_REGEX + 
				REGEX_GROUP( OPERAND_REGEX ) + PARENTHESIS_REGEX
			);

			if (std::regex_search(label, match, boolCondition) && match.size() > 1 && label == match.str(0)) {
				json jValue, jLeftOperand, jRightOperand;
				jCond["type"] = "predicate";
				jLeftOperand["type"] = "variable";
				jLeftOperand["value"] = match.str(1);
				if (bt.variables.count(match.str(1)) == 0)
					fatal("At transition to %s: Variable %s undeclared", outTransition.targetName.c_str(), match.str(1).c_str());
				jCond["left_operand"] = jLeftOperand;
				// negate predicate if needed
				if (negated)
					jCond["operator"] = "!=";
				else
					jCond["operator"] = "==";
				jRightOperand["type"] = "value";
				jValue["type"] = "bool";
				jValue["value"] = true;
				jRightOperand["value"] = jValue;
				jCond["right_operand"] = jRightOperand;
			}
			else if (std::regex_search(label, match, predCondition) && match.size() > 1 && label == match.str(0)) {
				std::string leftOperand = match.str(1);
				std::string operat = match.str(2);
				// negate predicate if needed
				if (negated) {
					if (operat == "==")
						operat = "!=";
					else if (operat == ">=")
						operat = "<";
					else if (operat == "<=")
						operat = ">";
					else if (operat == "<")
						operat = ">=";
					else if (operat == ">")
						operat = "<=";
					else if (operat == "!=")
						operat = "==";
				}
				std::string rightOperand = match.str(3);
				jCond["type"] = "predicate";

				jCond["left_operand"] = processPredicateOperand(leftOperand, XMLedge, bt, outTransition.targetName);
				jCond["operator"] = operat;
				jCond["right_operand"] = processPredicateOperand(rightOperand, XMLedge, bt, outTransition.targetName);
			}
			else
				fatal("At transition to %s: syntax error for condition predicate %s", outTransition.targetName.c_str(), label.c_str());

			outTransition.params["condition"] = jCond;
		}
	}
	else if (sourceNode->type == "random") {
		// random type source node
		json jRand;
		// it needs a float number that can be followed by anything else (like a % symbol)
		std::regex randomValueRegex(REGEX_GROUP( FLOAT_REGEX + ) + ".*");
		// or the name of a variable
		std::regex variableRegex(VARIABLE_REGEX);
		std::smatch match;
		if (label.empty()) {
			// evenly distributed random
			jRand["probability"] = 50.0f;
		}
		else if (std::regex_search(label, match, randomValueRegex) && match.size() > 1 && label == match.str(0)) {
			// ponderated value
			jRand["probability"] = static_cast<float>(atof(match.str(1).c_str()));
		}
		else
			fatal("At transition to %s: syntax error for random", outTransition.targetName.c_str());

		outTransition.params["random"] = jRand;
	}
	else if (sourceNode->type == "decorator") {
		// decorator type source node
		json j;
		std::string sourceName = toUnderscore(processTextLabel(sourceNode->name));
		if (sourceName.find("set", 0) == 0) {
			if (label.empty())
				fatal("At transition to %s: Set decorator requires arguments on transition label", outTransition.targetName.c_str());
			std::regex assignRegex(REGEX_GROUP( VARIABLE_REGEX ) + SPACE_REGEX + "=" + SPACE_REGEX + REGEX_GROUP( VALUE_REGEX ));
			std::smatch match;
			if (std::regex_search(label, match, assignRegex) && match.size() > 1 && label == match.str(0)) {
				if (bt.variables.count(match.str(1)) == 0)
					fatal("At transition to %s: Variable %s undeclared", outTransition.targetName.c_str(), match.str(1).c_str());
				std::string varName = match.str(1);
				std::string varType = bt.variables[varName].type;
				std::string varValue = match.str(2);
				j["variable"] = varName;
				if (varType == "int") {
					std::regex intRegex(INT_REGEX);
					if (!std::regex_match(varValue, intRegex))
						fatal("At transition to %s: Incompatible value %s for variable %s of type %s", outTransition.targetName.c_str(), varValue.c_str(), varName.c_str(), varType.c_str());
					j["value"] = atoi(varValue.c_str());
				}
				else if (varType == "float") {
					std::regex floatRegex(FLOAT_REGEX);
					if (!std::regex_match(varValue, floatRegex))
						fatal("At transition to %s: Incompatible value %s for variable %s of type %s", outTransition.targetName.c_str(), varValue.c_str(), varName.c_str(), varType.c_str());
					j["value"] = atof(varValue.c_str());
				}
				else if (varType == "bool") {
					std::regex boolRegex(BOOL_REGEX);
					if (!std::regex_match(varValue, boolRegex))
						fatal("At transition to %s: Incompatible value %s for variable %s of type %s", outTransition.targetName.c_str(), varValue.c_str(), varName.c_str(), varType.c_str());
					j["value"] = (varValue == "true");
				}
				j["type"] = "set";
			}
			else
				fatal("At transition to %s: set decorator arguments not valid", outTransition.targetName.c_str());
		}
		else if (sourceName.find("execute", 0) == 0) {
			if (label.empty())
				fatal("At transition to %s: execute decorator requires arguments on transition label", outTransition.targetName.c_str());
			std::regex funcRegex(REGEX_GROUP( FUNCTION_NAME_REGEX ) + SPACE_REGEX + "(\\(.*\\))?");
			std::smatch match;
			if (std::regex_search(label, match, funcRegex) && match.size() > 1 && label == match.str(0)) {
				j["function"] = processTextLabel(match.str(1));
				j["type"] = "execute";
			}
			else
				fatal("At transition to %s: execute decorator arguments not valid", outTransition.targetName.c_str());
		}
		else 
			fatal("At transition to %s: invalid decorator type in source node", outTransition.targetName.c_str());
		// decorator params are set in transitions
		sourceNode->params["decorator"] = j;
	}

	return true;
}

bool CBTGroup::processVariable(CBehaviourTree::TVariable & outVariable, tinyxml2::XMLElement * XMLnode) {
	std::string label, color;
	
	// read variable node label
	XMLElement * XMLnodeData = getXMLElementByAttribute(XMLnode, "data", "key", "d6");
	if (!XMLnodeData) 
		return false;
	XMLNode * XMLnodeLabel = XMLnodeData->FirstChildElement("y:ShapeNode")->FirstChildElement("y:NodeLabel")->FirstChild();
	if (XMLnodeLabel->FirstChild()) // hack to detect if label is empty
		fatal("At variable in line %i: Variable node is empty", XMLnode->GetLineNum());
	label = XMLnodeLabel->Value();
	color = XMLnodeData->FirstChildElement("y:ShapeNode")->FirstChildElement("y:Fill")->Attribute("color");

	// set variable type from node color
	if (_variableColorTypes.count(color) == 0)
		fatal("At variable in line %i: Unknown variable node type with color %s", XMLnode->GetLineNum(), color.c_str());
	outVariable.type = _variableColorTypes[color];

	// regex for variables that are only declarated
	std::regex onlyDeclRegex(REGEX_GROUP( VARIABLE_REGEX ));
	// regex for variables that are declared and initialized
	std::regex declInitRegex(REGEX_GROUP( VARIABLE_REGEX ) + SPACE_REGEX + "=" + SPACE_REGEX + REGEX_GROUP( VALUE_REGEX ));
	std::smatch match;

	if (std::regex_search(label, match, onlyDeclRegex) && match.size() > 1 && label == match.str(0)) {
		outVariable.name = match.str(1);
		if (outVariable.type == INT_VAR_TYPE || outVariable.type == FLOAT_VAR_TYPE)
			outVariable.value = 0;
		else if (outVariable.type == BOOL_VAR_TYPE)
			outVariable.value = false;
	}
	else if (std::regex_search(label, match, declInitRegex) && match.size() > 1 && label == match.str(0)) {
		outVariable.name = match.str(1);
		if (outVariable.type == INT_VAR_TYPE) {
			std::regex checkValue(INT_REGEX);
			if (!std::regex_match(match.str(2), checkValue))
				fatal("At variable %s: incompatible value for int variable", outVariable.name.c_str());
			outVariable.value = atoi(match.str(2).c_str());
		}
		else if (outVariable.type == FLOAT_VAR_TYPE) {
			std::regex checkValue(FLOAT_REGEX);
			if (!std::regex_match(match.str(2), checkValue))
				fatal("At variable %s: incompatible value for float variable", outVariable.name.c_str());
			outVariable.value = atof(match.str(2).c_str());
		}
		else if (outVariable.type == BOOL_VAR_TYPE) {
			std::regex checkValue(BOOL_REGEX);
			if (!std::regex_match(match.str(2), checkValue))
				fatal("At variable %s: incompatible value for bool variable", outVariable.name.c_str());
			outVariable.value = (toUnderscore(match.str(2)) == "true");
		}
	}

	return true;
}

bool CBTGroup::loadXML(const std::string& filename, const std::string& outputPath, const std::string& outExtension) {
	struct _stat iBuff, oBuff;
	int iResult, oResult;
	std::string label, color;
	std::string rootNodeID;

	// hack to always obtain only name of the file from full path
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	errno_t err;
	err = _splitpath_s(filename.c_str(), drive, _MAX_DRIVE, dir, _MAX_DIR, fname,
										 _MAX_FNAME, ext, _MAX_EXT);
	printf("Processing BT: %s\n", (std::string(fname) + ext).c_str());

	// open XML document
	XMLDocument doc;
	XMLError result = doc.LoadFile(filename.c_str());
	if (result != XML_SUCCESS) {
		return false;
	}

	// start parsing
	XMLElement * XMLgraph = doc.FirstChildElement("graphml")->FirstChildElement("graph");
	XMLElement * XMLgroup = XMLgraph->FirstChildElement("node");
	while (XMLgroup) {
		const bool isGroup = isGroupElement(XMLgroup);

		// parse group as different BT
		if (isGroup) {
			CBehaviourTree bt;

			XMLElement * XMLgroupData = 
				getXMLElementByAttribute(XMLgroup, "data", "key", "d6")
				->FirstChildElement("y:ProxyAutoBoundsNode")
				->FirstChildElement("y:Realizers")->FirstChildElement("y:GroupNode");
			// if bt group has name process it, otherwise get next
			if (XMLgroupData->FirstChildElement("y:NodeLabel")->FirstChild()) {
				label = processTextLabel(XMLgroupData->FirstChildElement("y:NodeLabel")->FirstChild()->Value());
				if (!label.empty()) {
					label = toUnderscore(label);
					iResult = _stat(filename.c_str(), &iBuff);
					oResult = _stat((outputPath + "/" + label + outExtension).c_str(), &oBuff);
					assert(iResult == 0 && "Input file not found!");
					if (oResult == 0 && iBuff.st_mtime < oBuff.st_mtime) {
						printf("File %s doesn't need export\n\n", (std::string(fname) + ext).c_str());
						return true;
					}
					bt.name = label;

					/* NODES */
					// get root node
					XMLElement * XMLnode = getUpmostNode(XMLgroup);
					rootNodeID = XMLnode->Attribute("id");
					if (!processNode(bt.rootNode, XMLnode, bt))
						fatal("At node in line %i: root (upmost) node must be a BT node", XMLnode->GetLineNum());

					// get rest of nodes
					XMLnode = XMLgroup->FirstChildElement("graph")->FirstChildElement("node");
					while (XMLnode) {
						if (XMLnode->Attribute("id") != rootNodeID && !isGroupElement(XMLnode)) {
							CBehaviourTree::TNode btNode;
							if (processNode(btNode, XMLnode, bt))
								bt.nodes[btNode.id] = btNode;
						}

						XMLnode = XMLnode->NextSiblingElement("node"); 
					}
					
					/* VARIABLES */
					XMLnode = XMLgroup->FirstChildElement("graph")->FirstChildElement("node");
					// search for "variables" group
					while (XMLnode) {
						if (isGroupElement(XMLnode)) {
							XMLElement * XMLnodeData = getXMLElementByAttribute(XMLnode, "data", "key", "d6");
							if (!XMLnodeData) continue;
							XMLElement * XMLgroupLabel = XMLnodeData->FirstChildElement("y:ProxyAutoBoundsNode")
								->FirstChildElement("y:Realizers")->FirstChildElement("y:GroupNode")->FirstChildElement("y:NodeLabel");
							if (!XMLgroupLabel) continue;

							label = XMLgroupLabel->FirstChild()->Value();
							if (toUnderscore(label) == "variables") {
								// variables group found!
								XMLElement * XMLvarNode = XMLnode->FirstChildElement("graph")->FirstChildElement("node");
								while (XMLvarNode) {
									CBehaviourTree::TVariable btVar;
									if (processVariable(btVar, XMLvarNode) && bt.variables.count(btVar.name) == 0) {
										if (bt.variables.count(btVar.name) > 0)
											fatal("At variable in line %i: Variable %s already declared", XMLvarNode->GetLineNum(), btVar.name.c_str());
										bt.variables[btVar.name] = btVar;
									}
									
									XMLvarNode = XMLvarNode->NextSiblingElement("node");
								}
							}
						}

						XMLnode = XMLnode->NextSiblingElement("node");
					}

					/* TRANSITIONS */
					// parse first transition
					XMLElement * XMLedge = XMLgraph->FirstChildElement("edge");
					if (XMLedge) {
						CBehaviourTree::TTransition btTransition;
						processTransition(btTransition, XMLedge, bt);
						bt.transitions.push_back(btTransition);

						// parse rest of transitions
						XMLedge = XMLedge->NextSiblingElement("edge");
						while (XMLedge) {
							processTransition(btTransition, XMLedge, bt);
							bt.transitions.push_back(btTransition);

							XMLedge = XMLedge->NextSiblingElement("edge");
						}
						// order transitions by source x position
						auto sortPred = [](const CBehaviourTree::TTransition & a, const CBehaviourTree::TTransition & b) {
							return a.target->xPos < b.target->xPos;
						};
						bt.transitions.sort(sortPred);
					}

					_btGroups.push_back(bt);
				}
			}
		}

		XMLgroup = XMLgroup->NextSiblingElement("node");
	}

	printf("Successfully loaded yEd XML: %s\n\n", filename.c_str());
	return true;
}

bool CBTGroup::saveJSON(const std::string & directory, const std::string & extension) {
	json jData;

	for (auto& bt : _btGroups) {
		/* NODES */
		// root node
		json jRootNode;
		jRootNode["name"] = bt.rootNode.name;
		jRootNode["type"] = bt.rootNode.type;
		if (!bt.rootNode.params.is_null())
			jRootNode["params"] = bt.rootNode.params;
		jData["root_node"] = jRootNode;

		json jNodeArray = json::array();
		for (auto & node : bt.nodes) {
			json jNode;
			jNode["name"] = node.second.name;
			jNode["type"] = node.second.type;
			if (!node.second.params.is_null())
				jNode["params"] = node.second.params;

			jNodeArray.push_back(jNode);
		}
		jData["nodes"] = jNodeArray;

		/* TRANSITIONS */
		json jTransitionArray = json::array();
		for (auto & node : bt.transitions) {
			json jTransition;
			jTransition["source"] = node.sourceName;
			jTransition["target"] = node.targetName;
			jTransition["params"] = node.params;

			jTransitionArray.push_back(jTransition);
		}
		jData["transitions"] = jTransitionArray;

		/* VARIABLES */
		json jVarArray = json::array();
		for (auto & node : bt.variables) {
			json jVar;
			jVar["name"] = node.second.name;
			jVar["type"] = node.second.type;
			jVar["value"] = node.second.value;
			jVarArray.push_back(jVar);
		}
		jData["variables"] = jVarArray;

		// write json file
		std::string filename = directory + "/" + bt.name + extension;
		printf("Writing behaviour tree in %s\n\n", filename.c_str());
		std::ofstream o(filename);
		o << std::setw(4) << jData << std::endl;
	}
	
	_btGroups.clear();
	return true;
}
