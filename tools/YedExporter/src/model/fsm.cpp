#include "pch.h"
#include "fsm.h"
#include "utils/utils.h"
#include <fstream>
#include <iomanip>
#include "utils/common.h"

using namespace tinyxml2;

void CFiniteStateMachine::processPredicate(const std::string& predicate, TTransition& transition) {
	json& jPred = transition.params;
	std::smatch match;

	// regex for comparison between a variable and a value
	std::regex comparisonRegex(
		SPACE_REGEX +
		REGEX_GROUP(VARIABLE_REGEX) +
		SPACE_REGEX +
		REGEX_GROUP(OPERATOR_REGEX) +
		SPACE_REGEX +
		REGEX_GROUP(VALUE_REGEX) +
		SPACE_REGEX
	);
	// regex for predicates that are formed only by a variable (can be negated)
	std::regex onlyVariableRegex(SPACE_REGEX + NEGATION_REGEX + SPACE_REGEX + REGEX_GROUP(VARIABLE_REGEX) + SPACE_REGEX);
	std::string varName, op, value;
	json jVariable;

	// if predicate is a comparison
	if (std::regex_search(predicate, match, comparisonRegex) && match.size() > 1 && predicate == match.str(0)) {
		// example: speed == 0
		
		varName = match.str(1);
		op = match.str(2);
		value = match.str(3);

		// translate operator
		if (op == "==")
			transition.oper = "equal";
		else if (op == "!=")
			transition.oper = "notEqual";
		else if (op == ">")
			transition.oper = "greater";
		else if (op == ">=")
			transition.oper = "greaterEqual";
		else if (op == "<")
			transition.oper = "less";
		else if (op == "<=")
			transition.oper = "lessEqual";

		// build internal "variable" json
		std::string varType;
		jVariable["name"] = varName;
		// check if variable exists
		if (variables.count(varName)) {
			varType = variables[varName].params["type"];
			jVariable["type"] = varType;
		}
		else
			fatal("At predicate \"%s\": Variable %s undeclared", predicate.c_str(), varName.c_str());

		if (varType == INT_VAR_TYPE)
			jVariable["value"] = atoi(value.c_str());
		else if (varType == FLOAT_VAR_TYPE)
			jVariable["value"] = atof(value.c_str());
		else if (varType == BOOL_VAR_TYPE)
			jVariable["value"] = (value == "true" ? true : false);

		jPred["variable"] = jVariable;
	}
	else if (std::regex_search(predicate, match, onlyVariableRegex) && match.size() > 1 && predicate == match.str(0)) {
		// example: !attacking
		
		// check if predicate is negated (starts with !)
		std::regex isNegated(SPACE_REGEX + "!.*");
		bool negated = (std::regex_match(predicate, isNegated));
		std::string varType;

		varName = match.str(1);
		// check if variable exists
		if (variables.count(varName))
			varType = variables[varName].params["type"];
		else
			fatal("At predicate \"%s\": Variable %s undeclared", predicate.c_str(), varName.c_str());

		// check if variable is bool or other for different behaviour
		if (varType == BOOL_VAR_TYPE) {
			transition.oper = "equal";

			jVariable["name"] = match.str(1).c_str();
			jVariable["type"] = BOOL_VAR_TYPE;
			jVariable["value"] = (negated ? false : true);
		}
		else {
			transition.oper = (negated ? "notEqual" : "equal");

			jVariable["name"] = match.str(1).c_str();
			jVariable["type"] = varType;
			jVariable["value"] = 0;
		}

		jPred["variable"] = jVariable;
	}
	else
		fatal("At predicate \"%s\": Invalid predicate", predicate.c_str());

}

void CFiniteStateMachine::processVariable(const std::string& label, const std::string& color, TVariable& variable) {
	json& jVariable = variable.params;

	// regex for variable declaration
	std::regex varDeclRegex(SPACE_REGEX + REGEX_GROUP(VARIABLE_REGEX) + SPACE_REGEX);
	// regex for variable declaration AND initialization
	std::regex varInitRegex(SPACE_REGEX + REGEX_GROUP(VARIABLE_REGEX) + SPACE_REGEX + "=" + SPACE_REGEX + REGEX_GROUP(VALUE_REGEX));
	std::smatch match;
	std::string type;

	// get type from color
	if (_variableColorTypes.count(color))
		type = _variableColorTypes[color];
	else
		fatal("At variable %s: Invalid type color for \"%s\"", variable.name.c_str(), color.c_str());

	if (std::regex_search(label, match, varDeclRegex) && match.size() > 1 && label == match.str(0)) {
		// example: attacking
		variable.name = match.str(1);
		jVariable["type"] = type;
		
		if (type == BOOL_VAR_TYPE)
			jVariable["value"] = false;
		else 
			jVariable["value"] = 0;
	}
	else if (std::regex_search(label, match, varInitRegex) && match.size() > 1 && label == match.str(0)) {
		// example: speed = 10
		variable.name = match.str(1);
		jVariable["type"] = type;

		std::string value = match.str(2);

		// check if value is correct for the type of variable
		if (type == BOOL_VAR_TYPE) {
			if (value == "true")
				jVariable["value"] = true;
			else if (value == "false")
				jVariable["value"] = false;
			else
				fatal("At variable %s: Type %s can't accept \"%s\"", type.c_str(), value.c_str());
		}
		else if (type == INT_VAR_TYPE) {
			std::regex intRegex(INT_REGEX);
			if (std::regex_match(value, intRegex))
				jVariable["value"] = atoi(value.c_str());
			else
				fatal("At variable %s: Type %s can't accept \"%s\"", type.c_str(), value.c_str());
		}
		else if (type == FLOAT_VAR_TYPE) {
			std::regex floatRegex(FLOAT_REGEX);
			if (std::regex_match(value, floatRegex))
				jVariable["value"] = atof(value.c_str());
			else
				fatal("At variable %s: Type %s can't accept \"%s\"", type.c_str(), value.c_str());
		}
	}
	else
		fatal("At variable declaration \"%s\": Invalid syntax", label.c_str());

}

bool CFiniteStateMachine::loadXML(const std::string & filename, const std::string& outputPath, const std::string& outExtension)
{
	struct _stat iBuff, oBuff;
	int iResult, oResult;
	// hack to always obtain only name of the file from full path
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	errno_t err;
	err = _splitpath_s(filename.c_str(), drive, _MAX_DRIVE, dir, _MAX_DIR, fname,
							 _MAX_FNAME, ext, _MAX_EXT);
	name = fname;
	printf("Processing FSM: %s\n", (std::string(fname) + ext).c_str());

  XMLDocument doc;
  XMLError result = doc.LoadFile(filename.c_str());
  if (result != XML_SUCCESS) return false;

  XMLElement* XMLgraph = doc.FirstChildElement("graphml")->FirstChildElement("graph");

	iResult = _stat(filename.c_str(), &iBuff);
	oResult = _stat((outputPath + "/" + std::string(fname) + outExtension).c_str(), &oBuff);
	assert(iResult == 0 && "Input file not found!");
	if (oResult == 0 && iBuff.st_mtime < oBuff.st_mtime) {
		printf("File doesn't need export\n%s\n", (std::string(fname) + outExtension).c_str());
		return false;
	}

	/* Parse FSMs. */

  // STATES
  XMLElement* XMLstate = XMLgraph->FirstChildElement("node");
  while (XMLstate)
	{
		const bool isGroup = isGroupElement(XMLstate);
		
		if (!isGroup)
		{
			XMLElement* XMLnode = getXMLElementByAttribute(XMLstate, "data", "key", "d6")->FirstChildElement("y:UMLClassNode");
			// check if start indicator
			if (XMLnode) {
				XMLNode* XMLlabel = XMLnode->FirstChildElement("y:NodeLabel")->FirstChild();
				if (XMLlabel) {
					TState state;
					state.id = XMLstate->Attribute("id");
					state.name = XMLlabel->Value();

					std::string attributes = XMLnode->FirstChildElement("y:UML")->FirstChildElement("y:AttributeLabel")->FirstChild()->Value();
					json params;
					try {
						params = json::parse(attributes);
					}
					catch (std::exception e) {
						fatal("At state %s: Error parsing json", state.name.c_str());
					}
					state.filename = params["filename"];
					state.type = params["type"];
					state.isCycle = params.value("isCycle", false);

					// parse blenders in state
					if (params.count("blenders")) {
						TBlender blender;
						blender.state = state.name;
						json jBlends = params["blenders"];
						blender.thresholds = json::array();
						for (auto& j : jBlends) {
							for (auto&[key, value] : j.items()) {
								json threshold;
								threshold["animation_name"] = key;
								threshold["threshold"] = value;
								blender.thresholds.push_back(threshold);
							}
						}

						blenders.push_back(std::move(blender));
					}

					states.push_back(std::move(state));
				}
			}
			else {
				XMLnode = getXMLElementByAttribute(XMLstate, "data", "key", "d6")->FirstChildElement("y:ShapeNode");
				XMLNode* XMLlabel = XMLnode->FirstChildElement("y:NodeLabel")->FirstChild();
				if (XMLlabel) {
					std::string label = XMLlabel->Value();
					std::regex startRegex(SPACE_REGEX + "[Ss][Tt][Aa][Rr][Tt]" + SPACE_REGEX);
					if (std::regex_match(label, startRegex)) {
						startNode = XMLstate->Attribute("id");
					}
				}
			}
    }

    XMLstate = XMLstate->NextSiblingElement("node");
  }

  // VARIABLES
  XMLElement* XMLvariables = getGroup(XMLgraph, "variables");
  XMLElement* XMLvar = XMLvariables->FirstChildElement("node");
  while (XMLvar)
  {
    //XMLElement* XMLdata = getXMLElementByAttribute(XMLvar, "data", "key", "d5");
    XMLElement* XMLnodeData = getXMLElementByAttribute(XMLvar, "data", "key", "d6");

    if (XMLnodeData)
    {
			std::string label = XMLnodeData->FirstChildElement("y:ShapeNode")->FirstChildElement("y:NodeLabel")->FirstChild()->Value();
			std::string color = XMLnodeData->FirstChildElement("y:ShapeNode")->FirstChildElement("y:Fill")->Attribute("color");
			TVariable var;
			processVariable(label, color, var);

      variables[var.name] = std::move(var);
    }

    XMLvar = XMLvar->NextSiblingElement("node");
  }

	// TRANSITIONS
	XMLElement* XMLedge = XMLgraph->FirstChildElement("edge");
	while (XMLedge) {
		std::string label;
		XMLElement* XMLdata = getXMLElementByAttribute(XMLedge, "data", "key", "d10")->FirstChildElement("y:PolyLineEdge")->FirstChildElement("y:EdgeLabel");

		if (XMLdata) {
			XMLNode * XMLedgeLabel = XMLdata->FirstChild();
			if (XMLedgeLabel->FirstChild()) // hack to detect if label is empty
				label = "";
			else
				label = XMLedgeLabel->Value();

			std::string sourceId = XMLedge->Attribute("source");
			std::string targetId = XMLedge->Attribute("target");
			if (sourceId != startNode) {
				TState* source = getStateById(sourceId);
				TState* target = getStateById(targetId);

				if (source && target) {
					TTransition tr;
					tr.source = source->name;
					tr.target = target->name;
					processPredicate(label, tr);

					transitions.push_back(std::move(tr));
				}
			}
			else {
				// set start node to the target of the start indicator
				startNode = targetId;
			}
		}
		else {
			std::string sourceId = XMLedge->Attribute("source");
			std::string targetId = XMLedge->Attribute("target");
			if (sourceId == startNode)
				startNode = targetId;
		}

		XMLedge = XMLedge->NextSiblingElement("edge");
	}

	printf("Successfully loaded yEd XML: %s\n", filename.c_str());
	return true;
}

bool CFiniteStateMachine::saveJSON(const std::string & directory, const std::string & extension)
{
  json jData;

  // STATES
  json jStates = json::array();
  for (const auto& st : states)
  {
		if (st.id == startNode)
			jData["start"] = st.name;

		json jState;
    jState["name"] = st.name;
		jState["type"] = st.type;
		jState["filename"] = st.filename;
		jState["isCycle"] = st.isCycle;

    jStates.push_back(jState);
  }
  jData["states"] = jStates;

  // TRANSITIONS
  json jTransitions = json::array();
  for (const auto& tr : transitions)
  {
		json jTransition;
		jTransition["source"] = tr.source;
    jTransition["target"] = tr.target;
		jTransition["operator"] = tr.oper;
		jTransition["variable"] = tr.params["variable"];
		jTransition["type"] = "checkVariable";

    jTransitions.push_back(jTransition);
  }
  jData["transitions"] = jTransitions;

  // VARIABLES
  json jVariables = json::array();
  for (const auto& var : variables)
  {
    json jVar = var.second.params;
    jVar["name"] = var.first;

    jVariables.push_back(jVar);
  }
  jData["variables"] = jVariables;

	// BLENDERS
	json jBlenders = json::array();
	for (auto& blend : blenders) {
		json jBlend;
		jBlend["state"] = blend.state;
		jBlend["thresholds"] = blend.thresholds;

		jBlenders.push_back(jBlend);
	}
	jData["blenders"] = jBlenders;

	std::string filename = directory + "/" + name + extension;
	printf("Writing FSM in %s\n\n", filename.c_str());
	std::ofstream o(filename);
	o << std::setw(4) << jData << std::endl;

	states.clear();
	transitions.clear();
	variables.clear();
	blenders.clear();

	return true;
}

CFiniteStateMachine::TState* CFiniteStateMachine::getStateById(const std::string& id)
{
  auto it = std::find_if(states.begin(), states.end(), [&id](const TState& st) {
    return st.id == id;
  });
  return it != states.end() ? &(*it) : nullptr;
}