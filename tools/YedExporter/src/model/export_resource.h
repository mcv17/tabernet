#pragma once

class IExportResource {
public:
	virtual bool loadXML(const std::string & filename, const std::string& outputPath, const std::string& outExtension) = 0;
	virtual bool saveJSON(const std::string & directory, const std::string & extension) = 0;
};
