#pragma once
#include "export_resource.h"

#define INT_VAR_TYPE				"int"
#define FLOAT_VAR_TYPE			"float"
#define BOOL_VAR_TYPE				"boolean"

class CFiniteStateMachine : public IExportResource
{
public:
  bool loadXML(const std::string& filename, const std::string& outputPath, const std::string& outExtension);
  bool saveJSON(const std::string & directory, const std::string & extension);

  // data model
  struct TState
  {
    std::string id;
    std::string name;
		std::string filename;
		std::string type;
		bool isCycle;
  };
  struct TTransition
  {
    std::string source;
    std::string target;
		std::string oper;
    json params;
  };
  struct TVariable
  {
    std::string name;
		json params;
  };
	struct TBlender
	{
		std::string state;
		json thresholds;
	};

private:
	std::map<std::string, std::string> _variableColorTypes{
		{"#CCFFCC", INT_VAR_TYPE},
		{"#FF9999", FLOAT_VAR_TYPE},
		{"#00CCFF", BOOL_VAR_TYPE}
	};

	std::string name;
	std::string startNode;

  std::vector<TState> states;
  std::vector<TTransition> transitions;
	std::unordered_map<std::string, TVariable> variables;
	std::vector<TBlender> blenders;

  TState* getStateById(const std::string& id);

	void processPredicate(const std::string& predicate, TTransition& transition);
	void processVariable(const std::string& label, const std::string& color, TVariable& variable);
};
