// YedExporter.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include "pch.h"
#include <iostream>
#include "model/config.h"
#include "model/export_resource.h"
#include "model/fsm.h"
#include "model/bt.h"
#include <filesystem>

namespace fs = std::filesystem;

void convertResource(const char * configFile, IExportResource * res, int argc = 0, char *argv[] = nullptr) {
	// load config file
	CConfig cfg(configFile);
	cfg.loadOptions(argc, argv);

	// get files to process
	std::vector<std::string> inputFiles = cfg.fetchFiles();

	printf("Processing %d files\n\n", (int)inputFiles.size());

	// for each file
	for (const auto& inputFile : inputFiles) {
		const std::string outputDir = cfg.getOutputDir(inputFile);

		fs::path iPath(inputFile);
		fs::path oPath(outputDir);
		iPath = fs::canonical(iPath);
		oPath = fs::canonical(oPath);

		
		bool processFile = false;
		if (!cfg.singleFile.empty()) {
			processFile = iPath.stem() == cfg.singleFile;
		}
		else {
			processFile = cfg.processAll || fs::last_write_time(iPath) > fs::last_write_time(oPath);
		}

		if (processFile) {
			// parse XML file
			if (res->loadXML(iPath.generic_string(), oPath.generic_string(), cfg.exportExtension)) {
				// save to JSON file
				res->saveJSON(oPath.generic_string(), cfg.exportExtension);
			}
		}
	}
}

int main(int argc, char *argv[])
{
	CFiniteStateMachine fsm;
	CBTGroup bt;
	// Disabled fsm exporting
	//printf("-------- FSM Exporter --------\n");
	//convertResource("fsm_config.json", &fsm);
	printf("-------- BT Exporter --------\n");
	convertResource("bt_config.json", &bt);
  printf("-------- All done! --------\n");
}

// todo: 
// + check timestamp
// - console arguments (unique_file, new)
// - behavior tree exporter
