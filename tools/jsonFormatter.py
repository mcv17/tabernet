#!/usr/bin/python
import json
import os
from collections import OrderedDict
indent=2
fileTypes = ['.json', '.bt', '.trans_curve', '.curve', 'fsm', '.material', '.particles', '.skeleton']

def main():
	for root, dirs, files in os.walk("../bin/data"):
		for file in files:
			if file.endswith( tuple(fileTypes) ):
				object_pairs_hook = ""
				
				filePath = os.path.join(root, file)
				#Open file and read its contents
				f = open(filePath, 'r')
				data=f.read()
				f.close()

				#Parse the file and pretty format it
				obj = json.loads(data, object_pairs_hook=OrderedDict)
				output = json.dumps(obj, sort_keys=False, indent=indent, separators=(',', ': '))
				
				#Update the file
				f = open(filePath, 'w')
				f.write(output)
				f.close()	
		
main()